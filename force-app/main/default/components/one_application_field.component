<apex:component controller="one_application_field_controller" >
    
    <apex:attribute type="one_application__c" required="true" name="c1" description="Record to draw" />
    <apex:attribute type="String" required="true" name="field_label" description="Field Label to display" />

	<apex:attribute type="String" required="false" default="" name="field_label_afterAsterisk" description="Second part of the field Label (after 'asterisk' char)" />
	<apex:attribute type="String" required="false" default="" name="field_label2_afterAsterisk" description="Second part of the field Label2 (after 'asterisk' char)" />
	<apex:attribute type="String" required="false" default="" name="field_label3_afterAsterisk" description="Second part of the field Label3 (after 'asterisk' char)" />

    <apex:attribute type="String" required="true" name="field_Api_Name" description="Field API name to draw, without initial namespace." />
    <apex:attribute type="String" required="true" name="field_Match" description="Field API name without initial namespace and replacing final '__c' or '__r' by just '__'." />
    
    <apex:attribute type="Object" required="true" name="map_errors" description="Map containing errors" />
    <apex:attribute type="String" required="true" name="mapkeys" description="String containing all map keys separated by comma." />
    
    <apex:attribute type="Boolean" required="true" name="field_required" description="Is Required field." />

    <apex:attribute type="String" required="false" default="" name="label_inline" description="Second label to display in the save line as the inputField." />
    
    <apex:attribute type="Integer" required="false" default="0" name="display_as_radioYesNo" description="Set to 1 to display the question as a Yes/No radio buttons question." />
    <apex:attribute type="one_QuestionUtil" required="false" default="" name="options1" description="Variable of type one_QuestionUtil" />
    <apex:attribute type="Integer" required="false" default="0" name="radio_reset" description="Set to 1 to reset the radio button options." />

    <apex:attribute type="Integer" required="false" default="0" name="display_as_MultipleFields" description="Set to 1 to display the question as multiple fields." />
    
    <apex:attribute type="String" required="false" default="" name="action_function_1" description="ActionFunction to call on an onclick event." />
    
	<apex:attribute type="String" required="false" default="" name="field_label2" description="Field2 Label to display" />
    <apex:attribute type="Boolean" required="false" default="false" name="field_required2" description="Is Required field2." />
    <apex:attribute type="String" required="false" default="" name="label_inline2" description="Second label to display in the save line as the inputField2." />
    <apex:attribute type="String" required="false" name="field_Api_Name2" description="Field2 API name to draw, without initial namespace." />
	<apex:attribute type="String" required="false" name="field_Match2" description="Field2 API name without initial namespace and replacing final '__c' or '__r' by just '__'." />
    
	<apex:attribute type="String" required="false" default="" name="field_label3" description="Field3 Label to display" />
    <apex:attribute type="Boolean" required="false" default="false" name="field_required3" description="Is Required field3." />
    <apex:attribute type="String" required="false" default="" name="label_inline3" description="Second label to display in the save line as the inputField3." />
    <apex:attribute type="String" required="false" name="field_Api_Name3" description="Field3 API name to draw, without initial namespace." />
	<apex:attribute type="String" required="false" name="field_Match3" description="Field3 API name without initial namespace and replacing final '__c' or '__r' by just '__'." />
    
    <apex:attribute type="Integer" required="false" default="0" name="display_as_PicklistWithActionEvent" description="Set to 1 to display the question as a picklist which calls an ActionFunction when 'onChange'." />
        
    <apex:attribute type="Integer" required="false" default="0" name="popUpIconId" description="Used to display icons into the fields" />

    <div class="customfieldcontainer" >
    	<apex:outputText rendered="{!display_as_MultipleFields != 2 && display_as_MultipleFields != 3}" >
	        <div class="customfieldlabelcontainer">
	            <span class="customfieldlabel"><apex:outputText value="{!field_label + IF(field_required,requiredSpan,'') + field_label_afterAsterisk}" escape="false" /></span>
	        </div>
        </apex:outputText>
        
        <apex:outputText rendered="{!(display_as_radioYesNo != 1 && display_as_radioYesNo != 2) && display_as_MultipleFields != 1 && display_as_MultipleFields != 2 && display_as_MultipleFields != 3}" >
            <div class="custominputfieldcontainer">
                <apex:outputText escape="false" value="{!label_inline}" />
                <apex:outputText rendered="false" > <!-- rendered="{!field_required}" -->
                	<div class="custominputRequiredBlock">&nbsp;</div>
                </apex:outputText>
                <apex:outputText rendered="{!display_as_PicklistWithActionEvent == 0}" >
	                <apex:inputField value="{!c1[field_Api_Name]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match)) && LEN(map_errors[LOWER(',' + field_Match)]) > 0, 'custominputfieldwitherrorclass', '')}" >
	                    <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match))}">
	                        <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match)]) > 0}">
	                            <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match)]}</div>
	                        </apex:outputPanel>
	                    </apex:outputPanel>
	                </apex:inputField>
                </apex:outputText>
                <apex:outputText rendered="{!display_as_PicklistWithActionEvent == 1}" >
                    <apex:inputField value="{!c1[field_Api_Name]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match)) && LEN(map_errors[LOWER(',' + field_Match)]) > 0, 'custominputfieldwitherrorclass', '')}" onchange="javascript:{!IF(LEN(action_function_1) > 0, action_function_1 + '();', 'return true;')}" >
                        <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match))}">
                            <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match)]) > 0}">
                                <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match)]}</div>
                            </apex:outputPanel>
                        </apex:outputPanel>
                    </apex:inputField>
                </apex:outputText>
            </div>
        </apex:outputText>
        
        <apex:outputText rendered="{!display_as_radioYesNo == 1 || display_as_radioYesNo == 2}" >
            <div id="container{!field_Api_Name}" class="custominputfieldcontainerextra">
                <div class="custominputfieldradiocontainer {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match)) && LEN(map_errors[LOWER(',' + field_Match)]) > 0, 'custominputfieldradiocontainererror', '')}">
                    <div class="{!IF(field_required, 'custominputfieldradiorequiredblock', '')}" >
                        <apex:selectRadio value="{!c1[field_Api_Name]}" layout="{!IF(display_as_radioYesNo == 1, 'pageDirection', 'lineDirection')}" onclick="javascript:{!IF(LEN(action_function_1) > 0, action_function_1 + '();', 'return true;')}" >
                            <apex:selectOptions value="{!options1.selectOptions1}"/>
                        </apex:selectRadio>
                    </div>
                </div>
                <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match))}">
	                <div>
	                    <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match)]) > 0}">
	                        <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span>&nbsp; --><span class="customerrorlabelclass">{!map_errors[LOWER(',' + field_Match)]}&nbsp;</span></div>
	                    </apex:outputPanel>
	                </div>
                </apex:outputPanel>
            </div>
            <apex:outputText rendered="{!radio_reset == 1}" >
                <script type="text/javascript">
                    function emptyRadios{!field_Api_Name}(){
                        var maindiv = document.getElementById('container{!field_Api_Name}');
                        var radios = maindiv.getElementsByTagName('input');
                        var i = 0;
                        for(i = 0; i < radios.length; i++){
                            if(radios[i].type.toLowerCase() == 'radio'){
                                radios[i].removeAttribute('checked');
                            }
                        }
                    }
                    emptyRadios{!field_Api_Name}();
                </script>
            </apex:outputText>
        </apex:outputText>
        
        <apex:outputText rendered="{!display_as_MultipleFields == 1}" >
            <div class="custominputfieldcontainer custominputfieldcontainerMultipleFields">
                <apex:outputText escape="false" value="{!label_inline}" />
                <apex:outputText rendered="false" > <!-- rendered="{!field_required}" -->
                	<div class="custominputRequiredBlock">&nbsp;</div>
                </apex:outputText>
                <div class="multipleFieldsFieldCont" >
                    <apex:inputField value="{!c1[field_Api_Name]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match)) && LEN(map_errors[LOWER(',' + field_Match)]) > 0, 'custominputfieldwitherrorclass', '')}">
                        <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match))}">
                            <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match)]) > 0}">
                                <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span>  -->{!map_errors[LOWER(',' + field_Match)]}</div>
                            </apex:outputPanel>
                        </apex:outputPanel>
                    </apex:inputField>
                </div>
                <div class="custominputfieldcontainerMultipleFieldsSecondFieldcont" >
                    <apex:selectList value="{!options1.value1}" size="1" >
                        <apex:selectOptions value="{!options1.selectOptions1}"/>
                    </apex:selectList>
                </div>
            </div>
        </apex:outputText>

        <apex:outputText rendered="{!display_as_MultipleFields == 3}" >        
        	<table cellpadding="0" cellspacing="0" class="custominputfieldfield3table">
        		<tr>
        			<td class="threefieldscontainerField1" >
				        <div class="customfieldlabelcontainer">
				            <span class="customfieldlabel"><apex:outputText value="{!field_label + IF(field_required,requiredSpan,'') + IF(popUpIconId == 1,iconPopUp,'') + field_label_afterAsterisk}" escape="false" /></span>
				        </div>        			
			            <div class="custominputfieldcontainer">
			                <apex:outputText escape="false" value="{!label_inline}" />
			                <apex:outputText rendered="false" > <!-- rendered="{!field_required}" -->
			                	<div class="custominputRequiredBlock">&nbsp;</div>
			                </apex:outputText>
			                <apex:inputField value="{!c1[field_Api_Name]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match)) && LEN(map_errors[LOWER(',' + field_Match)]) > 0, 'custominputfieldwitherrorclass', '')}">
			                    <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match))}">
			                        <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match)]) > 0}">
			                            <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match)]}</div>
			                        </apex:outputPanel>
			                    </apex:outputPanel>
			                </apex:inputField>
        				</div>
        			</td>
        			<td class="threefieldscontainerField2" >
				        <div class="customfieldlabelcontainer">
				            <span class="customfieldlabel"><apex:outputText value="{!field_label2 + IF(field_required2,requiredSpan,'') + field_label2_afterAsterisk}" escape="false" /></span>
				        </div>        			
			            <div class="custominputfieldcontainer">
			                <apex:outputText escape="false" value="{!label_inline2}" />
			                <apex:outputText rendered="false" > <!-- rendered="{!field_required2}" -->
			                	<div class="custominputRequiredBlock">&nbsp;</div>
			                </apex:outputText>
			                <apex:inputField value="{!c1[field_Api_Name2]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match2)) && LEN(map_errors[LOWER(',' + field_Match2)]) > 0, 'custominputfieldwitherrorclass', '')}">
			                    <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match2))}">
			                        <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match2)]) > 0}">
			                            <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match2)]}</div>
			                        </apex:outputPanel>
			                    </apex:outputPanel>
			                </apex:inputField>
        				</div>
        			</td>
        			<apex:outputText rendered="{!LOWER(field_label3) != '-notused-'}" >
	        			<td class="threefieldscontainerField3" >
					        <div class="customfieldlabelcontainer">
					            <span class="customfieldlabel"><apex:outputText value="{!field_label3 + IF(field_required3,requiredSpan,'') + field_label3_afterAsterisk}" escape="false" /></span>
					        </div>        			
				            <div class="custominputfieldcontainer">
				                <apex:outputText escape="false" value="{!label_inline3}" />
				                <apex:outputText rendered="false" > <!-- rendered="{!field_required3}" -->
				                	<div class="custominputRequiredBlock">&nbsp;</div>
				                </apex:outputText>
				                <apex:inputField value="{!c1[field_Api_Name3]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match3)) && LEN(map_errors[LOWER(',' + field_Match3)]) > 0, 'custominputfieldwitherrorclass', '')}">
				                    <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match3))}">
				                        <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match3)]) > 0}">
				                            <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match3)]}</div>
				                        </apex:outputPanel>
				                    </apex:outputPanel>
				                </apex:inputField>
	        				</div>
	        			</td>
        			</apex:outputText>
        		</tr>
        	</table>
        </apex:outputText>

        <apex:outputText rendered="{!display_as_MultipleFields == 2}" >        
            <table cellpadding="0" cellspacing="0" class="custominputfieldfield3table">
                <tr>
                    <td class="twofieldscontainerField0" colspan="2">
                        <div class="customfieldlabelcontainer">
                            <span class="customfieldlabel ExtraFieldLabel2"><apex:outputText value="{!field_label + IF(field_required || field_required2,requiredSpan,'') + field_label_afterAsterisk}" escape="false" /></span>
                        </div>                  
                    </td>
                </tr>
                <tr>
                    <td class="twofieldscontainerField1" >
                        <div class="custominputfieldcontainer custominputfieldcontainerextra2">
                            <table cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td class="tdExtraField2class1" >
                                        <span class="labelInline2">
                                            <apex:outputText escape="false" value="{!label_inline}" />
                                        </span>
                                        <apex:outputText rendered="false" > <!-- rendered="{!field_required}" -->
                                            <div class="custominputRequiredBlock">&nbsp;</div>
                                        </apex:outputText>
                                        <apex:inputField value="{!c1[field_Api_Name]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match)) && LEN(map_errors[LOWER(',' + field_Match)]) > 0, 'custominputfieldwitherrorclass', '')}">
                                            <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match))}">
                                                <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match)]) > 0}">
                                                    <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match)]}</div>
                                                </apex:outputPanel>
                                            </apex:outputPanel>
                                        </apex:inputField>                                  
                                    </td>
                                    <td class="tdExtraField2class2" >
                                        <span class="custominputfieldcontainerextra2" >
                                            <apex:outputText value="{!field_label2}" escape="false" />
                                        </span>                                 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td class="twofieldscontainerField2" >
                        <div class="custominputfieldcontainer custominputfieldcontainerextra2">
                            <table cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td class="tdExtraField2class1" >
                                        <span class="labelInline2">
                                            <apex:outputText escape="false" value="{!label_inline2}" />
                                        </span>
                                        <apex:outputText rendered="false" > <!-- rendered="{!field_required2}" -->
                                            <div class="custominputRequiredBlock">&nbsp;</div>
                                        </apex:outputText>
                                        <apex:inputField value="{!c1[field_Api_Name2]}" styleClass="custominputfieldclass {!IF(CONTAINS(mapkeys, LOWER(',' + field_Match2)) && LEN(map_errors[LOWER(',' + field_Match2)]) > 0, 'custominputfieldwitherrorclass', '')}">
                                            <apex:outputPanel layout="none" rendered="{!CONTAINS(mapkeys, LOWER(',' + field_Match2))}">
                                                <apex:outputPanel layout="none" rendered="{!LEN(map_errors[LOWER(',' + field_Match2)]) > 0}">
                                                    <div class="customerrorclass"><!-- <span class="customerrortitleclass">Error:</span> -->{!map_errors[LOWER(',' + field_Match2)]}</div>
                                                </apex:outputPanel>
                                            </apex:outputPanel>
                                        </apex:inputField>
                                    </td>
                                    <td class="tdExtraField2class2" >
                                        <span class="custominputfieldcontainerextra2" >
                                            <apex:outputText value="{!field_label3}" escape="false" />
                                        </span>                         
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </apex:outputText>
                
    </div>

</apex:component>