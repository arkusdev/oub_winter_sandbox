@isTest
public with sharing class Test_batch_Debit_Card_Email 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;

		Branch__c b = getBranch () ;

		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'test'+ i + '.person' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		insert c ;
		
		return c ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a Service
	 * ----------------------------------------------------------------------------------- */
	private static Service__c getService ( Contact c )
	{
		Service__c s = new Service__c () ;
		s.Contact__c = c.ID ;
		s.Issue_Date__c = Date.today ().addDays ( -1 ) ;
		s.Status__c = 'Issued' ;
		s.Type__c = 'Debit Card' ;
		
		insert s ;
		
		return s ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * TEST!
	 * ----------------------------------------------------------------------------------- */
	public static testmethod void testONE ()
	{
		//  Setup
		Contact c1 = getContact ( 1 ) ;
		Service__c s1 = getService ( c1 ) ;

		// Test
		System.debug ( 'TEST - START TEST' ) ;
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		batch_Debit_Card_Email b = new batch_Debit_Card_Email () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
		System.debug ( 'TEST - STOP TEST' ) ;
	}
}