public class FinancialAccountQueue implements Queueable
{
    private list<Automated_Email__c> aeL ;
    private list<Contact> cL ;
    private list<Deposit_Application_Account__c> daaL ;
    private list<Advocate_Reward_Achieved__c> araL ;
	private list<oneOpportunity__c> ooL ;
    private list<Financial_Account__c> faL ;
    
    public FinancialAccountQueue ()
    {
        
    }
    
    public FinancialAccountQueue ( list<Automated_Email__c> xL )
    {
        aeL = xL ;
    }
    
    public FinancialAccountQueue ( list<Advocate_Reward_Achieved__c> xL )
    {
        araL = xL ;
    }
    
    public FinancialAccountQueue ( list<Contact> xL )
    {
        cL = xL ;
    }
    
    public FinancialAccountQueue ( list<Deposit_Application_Account__c> xL )
    {
        daaL = xL ;
    }
    
    public FinancialAccountQueue ( list<oneOpportunity__c> xL )
    {
        ooL = xL ;
    }
    
    public FinancialAccountQueue ( list<Financial_Account__c> xL )
    {
        faL = xL ;
    }
    
    public void execute(QueueableContext context) 
    {
        try
        {
            if ( ( aeL != null ) && ( aeL.size () > 0 ) )
                insert aeL ;
            
            if ( ( araL != null ) && ( araL.size () > 0 ) )
                insert araL ;
            
            if ( ( cL != null ) && ( cL.size () > 0 ) )
                update cL ;
            
            if ( ( daaL != null ) && ( daaL.size () > 0 ) )
                update daaL ;
            
            if ( ( ooL != null ) && ( ooL.size () > 0 ) )
                update ooL ;
            
            if ( ( faL != null ) && ( faL.size () > 0 ) )  
                update faL ;
        }
        catch (Exception e)
        {
            // ?
        }
    }
}