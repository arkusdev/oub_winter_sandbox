public class application_new_transaction_controller {
    
    public Boolean ableToCreate {get;private set;}
    private final Application_Transaction__c appTransaction;
    
    public application_new_transaction_controller(ApexPages.StandardController stdController){
        this.appTransaction = (Application_Transaction__c)stdController.getRecord();
        this.ableToCreate = true;
    }
    
    public void createTransactionBasedOnPreviousPaymentMethod(){
        
        try{
            if(this.ableToCreate){
                USAePay_SOAP_Actions usaepayActions = new USAePay_SOAP_Actions('Settings_Unity_Visa');
                usaepayActions.currentIP = getUserIPAddress();
                usaepayActions.paymentMethod.custNum = this.appTransaction.Customer_Number__c;
                usaepayActions.paymentMethod.methodID = this.appTransaction.Payment_Method_ID__c;
                String amountStr = String.valueOf(this.appTransaction.Settled_Amount__c);
                usaepayActions.transactionParameters.amount = amountStr;
                usaepayActions.transactionParameters.clientIP = usaepayActions.currentIP;
                                            
                USAePay_SOAP_Actions.ResponseObject response = usaepayActions.runCustomerTransaction();
                                            
                if(response.statusCode == 200){
                    //SUCCESS
                    //Creating the Application Transaction record
                    Application_Transaction__c appTransaction = new Application_Transaction__c();
                    appTransaction.Customer_Number__c  = usaepayActions.paymentMethod.custNum;
                    appTransaction.Payment_Method_ID__c  = usaepayActions.paymentMethod.methodID;
                    appTransaction.Transaction_Ref_Num__c  = response.response; //transaction id
                    appTransaction.Amount__c  = this.appTransaction.Amount__c;
                    appTransaction.Settled_Amount__c  = this.appTransaction.Settled_Amount__c;
                    appTransaction.Status__c  = response.status;
                    appTransaction.Transaction_Settled_Date_Time__c = System.now();
                    
                    insert appTransaction;
        
                   	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Transaction successfully created: ' + appTransaction.Transaction_Ref_Num__c));
                    this.ableToCreate = false;
        
                }else{
                    //ERROR
                    String eMsg = response.response;
                    if(String.isEmpty(eMsg)){
                        eMsg = response.msg;
                    }
                    Integer eCode = response.statusCode;
    
                    //query the transaction error msg
                    List<USAePay_Error_Mapping__mdt > transactionErrorMappings = [SELECT Error_Msg__c FROM USAePay_Error_Mapping__mdt  WHERE Error_Code__c = :String.valueOf(eCode) LIMIT 1];
                    if(transactionErrorMappings.size()>0){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,transactionErrorMappings[0].Error_Msg__c));
                    }else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while doing transaction: ' + eMsg));
                    }
                    
                }
            }else{
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You cannot create more than one transaction from the same Application Transaction record.'));
            }
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + ' - line - ' + e.getLineNumber()));
        }
    }
    
    public PageReference cancel(){
        PageReference apptPage = new ApexPages.StandardController(this.appTransaction).view();
        apptPage.setRedirect(true);
        return apptPage;
    }
    
    private String getUserIPAddress() {
        
        string ReturnValue = '';
        
        ReturnValue = ApexPages.currentPage().getHeaders().get('True-Client-IP');
        
        if (ReturnValue == '' || Test.isRunningTest()) {
            ReturnValue = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        } // get IP address when no caching (sandbox, dev, secure urls)
        
        return ReturnValue;
        
    }

}