@isTest
public with sharing class Test_Overdraft_Program {
	
	static testmethod void createControllerNoParameter(){
        
        Test.startTest();
			PageReference myVfPage = Page.Overdraft_Program;
			Test.setCurrentPage(myVfPage);
			Overdraft_Program controller = new Overdraft_Program();
		Test.stopTest();

		System.assertEquals(false, controller.accountsReady);
		System.assertEquals(false, controller.error);
		System.assertEquals(false, controller.success);
		System.assertEquals(false, controller.disabledName);
		
	}

	static testmethod void createControllerWithParameter(){
        
        Contact c = new Contact () ;

        Test.startTest();
	        c.FirstName = 'Test' ;
	        c.LastName = 'User' ;
	        c.Email = 'Test.User@tester.com' ;
	        c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
	        
	        insert c ;

			PageReference myVfPage = Page.Overdraft_Program;
			Test.setCurrentPage(myVfPage);
			ApexPages.currentPage().getParameters().put('id', c.Id);
			Overdraft_Program controller = new Overdraft_Program();

		Test.stopTest();

		System.assertEquals(false, controller.accountsReady);
		System.assertEquals(false, controller.error);
		System.assertEquals(false, controller.success);
		System.assertEquals(true, controller.disabledName);

		System.assertEquals(c.Id, controller.con.Id);
		System.assertEquals(c.FirstName, controller.firstName);
		System.assertEquals(c.LastName, controller.lastName);
		
	}

	static testmethod void listAccountEmptyInputs(){
        
        Contact c = new Contact ();

        Test.startTest();

        	c.FirstName = 'Test' ;
	        c.LastName = 'User' ;
	        c.Email = 'Test.User@tester.com' ;
	        c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;

	        insert c ;

	       	PageReference myVfPage = Page.Overdraft_Program;
			Test.setCurrentPage(myVfPage);
			Overdraft_Program controller = new Overdraft_Program();
			
			controller.firstName = '';
			controller.lastName = '';
			controller.socialSecurityNumber = '';
			controller.dateOfBirth = '';

			controller.listAccounts(); 

		Test.stopTest();

		System.assertEquals(false, controller.accountsReady);
		System.assertEquals(false, controller.error);
		System.assertEquals(false, controller.success);
	}

	static testmethod void listAccountCorrectInputs(){
        
        Contact c = new Contact () ;
        OIS_Status__c ois = new OIS_Status__c ();

        Test.startTest();

        	c.FirstName = 'Test' ;
	        c.LastName = 'User' ;
	        c.Email = 'Test.User@tester.com' ;
	        c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
	        c.TaxID__c = '44444321';
	        c.BirthDate = Date.today().addYears(-25);

	        insert c;
        
        	Financial_Account__c f = new Financial_Account__c () ;
        	f.ACCTNBR__c = '1231231234' ;
        	f.CURRACCTSTATCD__c = 'ACT' ;
        	f.Overdraft_Privilege_Status__c = 'A' ;
        	insert f ;

	        ois.Account_Number__c = f.ACCTNBR__c ;
	        ois.Contact__c = c.Id;
        	ois.Financial_Account__c = f.ID ;
	        //just a placeholder
	        ois.UploadID__c = '21739123';

	        insert ois;
	        
			PageReference myVfPage = Page.Overdraft_Program;
			Test.setCurrentPage(myVfPage);
			Overdraft_Program controller = new Overdraft_Program();

			controller.firstName = c.FirstName;
			controller.lastName = c.LastName;
			controller.socialSecurityNumber = '4321';
			controller.dateOfBirth = Date.today().addYears(-25).format();

			controller.listAccounts(); 

		Test.stopTest();

		System.assertEquals(true, controller.accountsReady);
		System.assertEquals(false, controller.error);
		System.assertEquals(false, controller.success);
		System.assertEquals(1, controller.oisWrapperList.size());
	}

	static testmethod void acceptSelection(){
        
        Contact c = new Contact () ;
        OIS_Status__c ois = new OIS_Status__c ();

        Test.startTest();

        	c.FirstName = 'Test' ;
	        c.LastName = 'User' ;
	        c.Email = 'Test.User@tester.com' ;
	        c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
	        c.SocialSecurityNumber__c = '44444321';
	        c.BirthDate = Date.today().addYears(-25);

	        insert c;

	        ois.Account_Number__c = '1231231234';
	        ois.Contact__c = c.Id;
	        ois.Opt_In_Status__c='Y';
	        //just a placeholder
	        ois.UploadID__c = '21739123';

	        insert ois;
	        
			PageReference myVfPage = Page.Overdraft_Program;
			Test.setCurrentPage(myVfPage);
			Overdraft_Program controller = new Overdraft_Program();

			List<Overdraft_Program.OISWrapper> oisWList = new List<Overdraft_Program.OISWrapper>();
			oisWList.add(new Overdraft_Program.OISWrapper(ois.Account_Number__c,'N',ois.Id));

			controller.oisWrapperList = oisWList;

			controller.acceptSelection(); 

            System.assertEquals(false, controller.accountsReady);
            System.assertEquals(false, controller.error);
            System.assertEquals(true, controller.success);
    
		Test.stopTest();

		List<OIS_Status__c> oisList = [SELECT Account_Number__c, Opt_In_Status__c
										FROM OIS_Status__c
										WHERE Id =:ois.Id];

		System.assertEquals(ois.Id, oisList[0].Id);
		System.assertEquals('N', oisList[0].Opt_In_Status__c);

		List<OIS_Response__c> oisRList = [SELECT Acct_Number__c, Opt_In_Status__c
										FROM OIS_Response__c
										WHERE Contact__c =:c.Id];

		System.assertEquals(ois.Account_Number__c, oisRList[0].Acct_Number__c);
		System.assertEquals('N', oisRList[0].Opt_In_Status__c);
	}


	static testmethod void getOptions(){
        
        List<SelectOption> options = new List<SelectOption>();

        Test.startTest();
	        
			PageReference myVfPage = Page.Overdraft_Program;
			Test.setCurrentPage(myVfPage);
			Overdraft_Program controller = new Overdraft_Program();

			options = controller.getOptions(); 

		Test.stopTest();

		System.assertEquals(3, options.size());
		System.assertEquals('D', options[0].getValue());
		System.assertEquals('Y', options[1].getValue());
		System.assertEquals('N', options[2].getValue());

	}
    
	static testmethod void noEligibleAccounts ()
    {
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'User' ;
        c1.TaxID__c = '111223333' ;
        c1.BirthDate = Date.newInstance ( 1911, 11, 11 ) ;
        c1.Email = 'Test.User@tester.com' ;
        
        insert c1 ;
        
     	Financial_Account__c fa1 = new Financial_Account__c () ;
        fa1.TAXRPTFORPERSNBR__c = c1.ID ;
        fa1.ACCTNBR__c = '1001231234' ;
        fa1.Overdraft_Privilege_Status__c = 'A' ;
        fa1.CURRACCTSTATCD__c = 'CLS' ;
        
        insert fa1 ;
        
     	Financial_Account__c fa2 = new Financial_Account__c () ;
        fa2.TAXRPTFORPERSNBR__c = c1.ID ;
        fa2.ACCTNBR__c = '1001231235' ;
        fa2.Overdraft_Privilege_Status__c = 'S' ;
        fa2.CURRACCTSTATCD__c = 'ACT' ;
        
        insert fa2 ;
        
        OIS_Status__c ois1 = new OIS_Status__c () ;
        ois1.Account_Number__c = fa1.ACCTNBR__c ;
        ois1.Financial_Account__c = fa1.ID ;
        ois1.Contact__c = c1.ID ;
        ois1.First_Name__c = c1.FirstName ;
        ois1.Last_Name__c = c1.LastName ;
        ois1.UploadID__c = c1.TaxID__c + '-' + fa1.ACCTNBR__c ;
        
        insert ois1 ;
        
        OIS_Status__c ois2 = new OIS_Status__c () ;
        ois2.Account_Number__c = fa2.ACCTNBR__c ;
        ois2.Financial_Account__c = fa2.ID ;
        ois2.Contact__c = c1.ID ;
        ois2.First_Name__c = c1.FirstName ;
        ois2.Last_Name__c = c1.LastName ;
        ois2.UploadID__c = c1.TaxID__c + '-' + fa2.ACCTNBR__c ;
        
        insert ois2 ;
        
        Test.startTest () ;
        
        PageReference myVfPage = Page.Overdraft_Program;
        Test.setCurrentPage(myVfPage);
        Overdraft_Program controller = new Overdraft_Program () ;
        
        controller.firstName = c1.FirstName ;
        controller.lastName = c1.LastName ;
        controller.dateOfBirth = c1.Birthdate.format () ;
        controller.socialSecurityNumber = c1.TaxID__c.right ( 4 ) ;
        
        controller.listAccounts () ;
        
		System.assertEquals ( 0, controller.oisWrapperList.size () ) ;
        
        Test.stopTest () ;
    }
}