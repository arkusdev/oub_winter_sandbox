@isTest
public class test_batch_DeDuplication 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Batch_Process_Settings__c insertCustomSetting ()
    {
        Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;

        bps.Name = 'OneUnited_Batch' ;        
        bps.ACH_GL_Account__c = '1001000100' ;
        bps.Application_Cashbox__c = '666' ;
        bps.Batch_Email__c = 'lmattera@oneunited.com' ;
        bps.Collateral_GL_Account__c = '1001000101' ;
        bps.Deposit_ePay_GL_Account__c = '1001000102' ;
        bps.Email_Enabled__c = false ;
        bps.Routing_Number__c = '01100127' ;
        bps.UNITY_Visa_ePay_GL_Account__c = '1001000103' ;
        bps.Batch_DeDeduplication_Count__c = 200 ;
        
        insert bps ;
        
        return bps ;
    }

    private static Account getAccount ( Integer i )
    {
    	Account a = new Account () ;
    	a.name = 'Test' + i + ' Account' + i ;
    	
    	return a ;
    }
    
	private static Contact getContactEmail ( Integer i, String rtID )
	{
		return getContactEmail ( i, rtID, null ) ;
	}
	
    private static Contact getContactEmail ( Integer i, String rtID, Account a )
    {
    	Contact c = new Contact () ;
    	
    	c.FirstName = 'test' + i ;
    	c.LastName = 'user' + i ;
    	c.Email = 'test' + i + '@user' + i + '.com' ;
    	c.RecordTypeID = rtID ;
    	
    	if ( a != null )
	    	c.Account = a ;
    	
    	return c ;
    }

	private static Contact getContactPhone ( Integer i, String rtID )
	{
		return getContactPhone ( i, rtID, null ) ;
	}
	
    private static Contact getContactPhone ( Integer i, String rtID, Account a )
    {
    	Contact c = new Contact () ;
    	
    	c.FirstName = 'test' + i ;
    	c.LastName = 'user' + i ;
    	c.Phone = '00' + i + '-00' + i + '-000' + i ;
    	c.RecordTypeID = rtID ;
    	
    	if ( a != null )
	    	c.Account = a ;
    	
    	return c ;
    }
    
    //  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	public static testmethod void TESTZERO ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
        c1.DeDuplication_Flag__c = true ;
       	cL.add ( c1 ) ; 
        
        Contact c2 = getContactEmail ( 2, prospectRecordTypeID, a1 ) ;
        c2.DeDuplication_Flag__c = true ;
        cL.add ( c2 ) ;

        insert cL ;
        
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
    }
    
	public static testmethod void TESTONE ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Generic Account
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		//  First contact
		Contact cc1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
    	cc1.phone = '00' + 1 + '-00' + 1 + '-000' + 1 ;
        cc1.DeDuplication_Flag__c = true ;
        insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactEmail ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactEmail ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
        insert cp12 ;
        
		//  Second contact
		Contact cc2 = getContactEmail ( 2, customerRecordTypeID, a1 ) ;
        cc2.DeDuplication_Flag__c = true ;
        insert cc2 ;
		
		//  Not so duplicate prospects, second contact
		Contact cp21 = getContactEmail ( 1, prospectRecordTypeID ) ;
		cp21.FirstName = 'Other1' ;
    	cp21.Description = 'Nyet Mergy' ;
        insert cp21 ;
        
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
        
		//  Check to see that stuff merged
		Contact c1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__c, DeDuplication_Flag__c
			FROM Contact
			WHERE ID = :cc1.ID
		] ;
        
        System.assertEquals ( c1.Description, cp11.Description ) ;
        System.assertEquals ( c1.Title, cp12.Title ) ;
        System.assertEquals ( c1.WorkEmail__c, cp11.WorkEmail__c ) ;
        System.assertEquals ( c1.DeDuplication_Flag__c, false ) ;
		
		//  Check to see that stuff did not merge
		Contact c2 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__C, DeDuplication_Flag__c
			FROM Contact
			WHERE ID = :cc2.ID
		] ;
		
        System.assertNotEquals ( c2.Description, cp21.Description ) ;
    }
    
	public static testmethod void TESTTWO ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Generic Account
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		//  First contact
		Contact cc1 = getContactPhone ( 1, customerRecordTypeID, a1 ) ;
        cc1.DeDuplication_Flag__c = true ;
        insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
        insert cp12 ;
        
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
        
		//  Check to see that stuff merged
		Contact c1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__c, Computed_Phone__c, DeDuplication_Flag__c
			FROM Contact
			WHERE ID = :cc1.ID
		] ;
		
        System.assertEquals ( c1.Description, cp11.Description ) ;
        System.assertEquals ( c1.Title, cp12.Title ) ;
        System.assertEquals ( c1.WorkEmail__c, cp11.WorkEmail__c ) ;
        System.assertEquals ( c1.DeDuplication_Flag__c, false ) ;
    }
    
    public static testmethod void TESTTHREE ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Generic Account
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
        list<Contact> cL = new list<Contact> () ;
        
		Contact cc1 = getContactPhone ( 1, customerRecordTypeID, a1 ) ;
        cc1.DeDuplication_Flag__c = true ;
        cL.add ( cc1 ) ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        cL.add ( cp11 ) ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
        cL.add ( cp12 ) ;
        
		Contact cc2 = getContactEmail ( 2, customerRecordTypeID, a1 ) ;
    	cc2.phone = '00' + 2 + '-00' + 2 + '-000' + 1 ;
        cc2.DeDuplication_Flag__c = true ;
        cL.add ( cc2 ) ;
		
		//  Duplicate prospects, first contact
		Contact cp21 = getContactEmail ( 2, prospectRecordTypeID ) ;
    	cp21.WorkEmail__c = 'test' + 2 + '@work' + 2 + '.com' ;
    	cp21.Description = 'Mergy Merge' ;
        cL.add ( cp21 ) ;
		
		//  Duplicate prospects, second contact
		Contact cp22 = getContactEmail ( 2, prospectRecordTypeID ) ;
		cp22.title = 'Tester' ;
        cL.add ( cp22 ) ;
        
        insert cL ;
        
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
    }        
    
    public static testmethod void TESTFOUR ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Generic Account
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
        list<Contact> cL = new list<Contact> () ;
        
    	//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        cp11.DeDuplication_Flag__c = true ;
        cL.add ( cp11 ) ;
 		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		cL.add ( cp12 ) ;

        insert cL ;
        
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
    }
    
    public static testmethod void TESTFIVE ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Generic Account
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
        list<Contact> cL = new list<Contact> () ;
        
    	//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, customerRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        cp11.DeDuplication_Flag__c = true ;
		cL.add ( cp11 ) ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, customerRecordTypeID ) ;
		cp12.title = 'Tester' ;
		cL.add ( cp12 ) ;

        insert cL ;
        
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
    }
    
	static testmethod void TESTSIX ()
	{
        Batch_Process_Settings__c bps = insertCustomSetting () ;

        System.debug ( '----------------------------------- TEST SIX -----------------------------------'  ) ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		Contact cc1 = getContactPhone ( 1, customerRecordTypeID, a1 ) ;
		insert cc1 ;
		
        list<Contact> dL = new list<Contact> () ;
        
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		dL.add ( cp11 ) ;
        
        //  LOTS!
        for ( Integer i = 0 ; i < 10 ; i ++ )
        {
            Contact cpX = getContactPhone ( 1, prospectRecordTypeID ) ;
            cpX.title = 'Tester' ;
            dL.add ( cpX ) ;
        }
		
        //  Insert
		insert dL ;
		
		Test.startTest () ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
        
		Test.stopTest () ;
	}
    
    //  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	public static testmethod void TESTSEVEN ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
        c1.DeDuplication_Flag__c = true ;
       	cL.add ( c1 ) ; 
        
    	//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, customerRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        cp11.DeDuplication_Flag__c = true ;
		cL.add ( cp11 ) ;

        insert cL ;
        
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_DeDuplication b = new batch_DeDuplication () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
    }
}