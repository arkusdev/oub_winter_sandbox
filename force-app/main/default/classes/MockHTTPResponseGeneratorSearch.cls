@isTest
global class MockHTTPResponseGeneratorSearch implements HttpCalloutMock
{
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Optionally, only send a mock response for a specific endpoint and method.
        System.assertEquals ( 'https://mandrillapp.com/api/1.0/messages/search.json', req.getEndpoint () ) ;
        System.assertEquals ( 'POST', req.getMethod () ) ;
        
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        res.setHeader ( 'Content-Type', 'application/json' ) ;
        
        String body = '' ;
		body += '[ { "ts": 1391537268, ' ;
		body += '"state": "sent", ' ;
		body += '"subject": "Thanks for Applying for Your UNITY Visa Card", ' ;
		body += '"email": "lmattera@oneunited.com", ' ;
		body += '"tags": [ "welcome-email" ], ' ;
		body += '"opens": 0, ' ;
		body += '"clicks": 0, ' ;
		body += '"smtp_events": [ { ' ;
		body += '"ts": 1391537269, ' ;
		body += '"type": "sent", ' ;
		body += '"diag": "250 Ok: queued as 0B9CA1622052", ' ;
		body += '"source_ip": "198.2.128.8", ' ;
		body += '"destination_ip": "204.60.84.2", ' ;
		body += '"size": 35959 } ], ' ;
		body += '"subaccount": "UNITYVISAWELCOME", ' ;
		body += '"resends": [ "0" ], ' ;
		body += '"_id": "4faa7cc55db2496fa5505a6bb7bb8877", ' ;
		body += '"sender": "noreplies@unityvisa.com", ' ;
		body += '"template": "unity-visa-thanks-for-applying", ' ;
		body += '"metadata": { ' ;
		body += '"salesforceApplicationId": "a0PP0000001r3mUMAQ", ' ;
		body += '"FISAppId": "2855840", ' ;
		body += '"salesforceContactId": "003P000000fVQ1mIAG" }, ' ;
		body += '"opens_detail": [ { ' ;
        body += '"ts" : 1234, ' ;
        body += '"ip" : "111.222.111.222", ' ;
        body += '"location" : "xyz", ' ;
        body += '"ua" : "abc" } ], ' ;
		body += '"clicks_detail": [ { ' ;
        body += '"ts" : 1234, ' ;
        body += '"url" : "http://foo.bar.com", ' ;
        body += '"ip" : "111.222.111.222", ' ;
        body += '"location" : "xyz", ' ;
        body += '"ua" : "abc" } ] ' ;
        body += '} ]' ;        
        body += '' ;
        
        res.setBody ( body ) ;
        res.setStatusCode ( 200 ) ;
        return res ;
    }
}