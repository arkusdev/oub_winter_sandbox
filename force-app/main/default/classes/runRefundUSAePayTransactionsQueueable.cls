public class runRefundUSAePayTransactionsQueueable implements Queueable, Database.AllowsCallouts 
{
    private List<String> applicationsIds;
    private Map<String,Decimal> amounts;

    public runRefundUSAePayTransactionsQueueable(List<String> applicationsIds,Map<String,Decimal> amountMap) 
    {
        this.applicationsIds = applicationsIds;
        this.amounts = amountMap;
    }
    
    public void execute(QueueableContext context)
    {
        USAePay_SOAP_Actions usaepayActions = new USAePay_SOAP_Actions('Settings_Unity_Visa');
        
        Set<Application_Transaction__c> lapptToUpdate = new Set<Application_Transaction__c>();
        List<Application_Transaction__c> lapptToCreate = new List<Application_Transaction__c>();
        Set<Id> lapptToUpdateIds = new Set<Id>();
        Set<Id> lapptToCreateFromIds = new Set<Id>();
        List<Application_Transaction__c> lappt;
        list<Ticket__c> refundTicketList = new list<Ticket__c> () ;
        
        USAePay_Setting__mdt setting = [SELECT Transaction_Window__c FROM USAePay_Setting__mdt WHERE DeveloperName ='Settings_Unity_Visa' LIMIT 1];
        for(String appId:applicationsIds)
        {
            if(!Test.isRunningTest()){
            	lappt = [SELECT Application__c,CreatedDate,Status__c,Customer_Number__c ,Payment_Method_ID__c,Transaction_Ref_Num__c,Refund_Ticket__c 
                         FROM Application_Transaction__c 
                         WHERE Application__c = :appId 
                         AND Status__c='Settled' 
                         ORDER BY CreatedDate 
                         DESC LIMIT 1];
            }else{
                lappt = [SELECT Application__c,CreatedDate,Status__c,Customer_Number__c ,Payment_Method_ID__c,Transaction_Ref_Num__c,Refund_Ticket__c 
                         FROM Application_Transaction__c 
                         WHERE Application__c= :appId 
                         ORDER BY CreatedDate 
                         DESC LIMIT 1];
            }
            if(lappt.size()>0)
            {
                usaepayActions.transactionParameters.refNum = lappt[0].Transaction_Ref_Num__c;
                
                USAePay_SOAP_Actions.ResponseObject response;
                    
                if(!lapptToUpdateIds.contains(lappt[0].Id))
                {
                    response = usaepayActions.RefundTransaction(); 
                
                    Ticket__c t = null ;
                    
                    if ( lappt[0].Refund_Ticket__c != null ) 
                    {
                        t = new Ticket__c () ;
                        t.ID = lappt[0].Refund_Ticket__c  ;
                        t.Status__c = 'Refund Issued' ;
                    }
                    
                    if(response.statusCode == 200 && !Test.isRunningTest())
                    {
                        lappt[0].Status__c = 'Refunded';
                        lappt[0].Refunded_Amount__c = amounts.get(appId);
                        lappt[0].Transaction_Refunded_Date_Time__c = System.now();
                        
                        lapptToUpdate.add(lappt[0]);
                        lapptToUpdateIds.add(lappt[0].Id);
                        
                        if ( t != null ) 
                            refundTicketList.add ( t ) ;
                    }
                    else
                    {
                        //ERROR
                        lappt[0].Error_Msg__c = 'Error while refunding transaction - ' + response.statusCode + ' - ' + response.response;
                        lappt[0].Transaction_Failed_Date_Time__c = System.now();
                        lappt[0].Status__c = 'Failed';
                        if(!lapptToUpdateIds.contains(lappt[0].Id)){lapptToUpdate.add(lappt[0]);lapptToUpdateIds.add(lappt[0].Id);}
                    }
                }
            }
        }
        
        update new List<Application_Transaction__c>(lapptToUpdate);
        insert lapptToCreate;
        
        if ( ( refundTicketList != null ) && ( refundTicketList.size () > 0 ) )
            update refundTicketList ;
    }
}