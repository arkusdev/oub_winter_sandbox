global class batch_SWMFileTicket implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	private final String INITIAL_STATUS = 'Approved for Refund' ;
	
	private Map<String,String> groupMap ;
	private List<RecordType> rtL ;
	
	global Integer accountCount ;
	global Decimal accountSum ;
	global String ticketId ;
	
	global batch_SWMFileTicket ()
	{
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
		
		//  Reset counters
		accountCount = 0 ;
		accountSum = 0.0 ;
		ticketId = null ;
		
		//  Set up the groups we need for later
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		//  Set up the record types we need for later
		
		List<String> rtNameList = new List<String> () ;
		rtNameList.add ( 'Credit Card Refund' ) ;
		rtNameList.add ( 'CS Batch File' ) ;
		
		rtL = OneUnitedUtilities.getRecordTypeList ( rtNameList ) ; 
	}

	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String recordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Credit Card Refund', 'Ticket__c' ) ;
		String query ;
		
		query = 'SELECT ' ;
		query += 't.ID, t.Approved_Refund_Amount__c ' ;
		query += 'FROM Ticket__c t ' ;
		query += 'WHERE t.Status__c = \'' + INITIAL_STATUS + '\'' ;
		if ( String.isNotBlank ( recordTypeId ) )
			query += 'AND t.RecordTypeId = \'' + recordTypeId + '\' ' ;
		query += 'AND t.SWM_Batch_Ticket__c = NULL ' ;
		query += 'AND t.Approved_Refund_Amount__c > 0 ' ;
		query += 'AND t.ACH_Refund_Exception__c = false ' ;
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Ticket__c> tL )
	{
		if ( tL.size () > 0 )
		{
			String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;
		
			for ( Ticket__c t : tL )
			{
				accountCount ++ ;
				accountSum += t.Approved_Refund_Amount__c ;
			}
			
			Ticket__c batchTicket = new Ticket__c () ;
			
			batchTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
			batchTicket.Status__c = 'New' ;
			batchTicket.Batch_Account_Total__c = accountCount ;
			batchTicket.Batch_Amount_Total__c = accountSum ;
			batchTicket.ownerId = groupMap.get ( 'Credit Card Batch Processing' ) ;
			batchTicket.Batch_Type__c = 'SWM' ;
			
			insert batchTicket ;
			
			List<Ticket__c> bT = new List<Ticket__c> () ;
			for ( Ticket__c t : tL )
			{
				t.SWM_Batch_Ticket__c = batchTicket.id ;
				
				if ( groupId != null )
					t.OwnerId = groupId ;
					
				bT.add ( t ) ;
			}
			
			update bT ;
			
			ticketId = batchTicket.id ;
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
      	OrgWideEmailAddress o = [
      		SELECT ID
      		FROM OrgWideEmailAddress
      		WHERE Address = 'customersupport@oneunited.com'
      	] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage cMail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c,
				'SecureCardRefundACH@OneUnited.com' 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email,
				'SecureCardRefundACH@OneUnited.com' 
			} ;
		}
   
		cMail.setToAddresses ( toAddresses ) ;
		cMail.setSubject ( 'Batch SWM File Job Results :: ' + a.Status ) ;
		
		if ( o != null )
			cMail.setOrgWideEmailAddressId ( o.Id ) ;
		
		String message = '' ;
		message += '<h1>Results from creating the newest SWM batch file:</h1>' ;
		message += '<p>The batch processed ' + a.TotalJobItems +' batches ' ;
		if ( a.NumberOfErrors > 0 )
			message += 'with '+ a.NumberOfErrors + ' failures.' ;
		message += '</p>' ;
		message += '<p>There are ' + accountCount + ' refund account(s) totalling ' + formatMoney ( accountSum ) + ' in the batch.</p>' ;
		message += '<br/>' ;
		
		if ( ( accountCount > 0 ) && ( accountSum > 0.0 ) && ( String.isNotBlank ( ticketId ) ) )
		{
			System.URL sURL = URL.getSalesforceBaseURL () ;
			String urly = sURL.toExternalForm () + '/' + ticketId ;
			message += '<p>You can download the batch from:</p>' ;
			message += '<a href="'+ urly + '">' + urly + '</a>' ;
		}
   
   		cMail.setPlainTextBody ( '' ) ;  // WTB no "null"
		cMail.setHtmlBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { cMail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { cMail } ) ;
			}
		}
	}
	
	private String formatMoney ( Decimal x )
	{
		Decimal dollars;
		Decimal cents;
		dollars = x.intValue();
		cents = x - x.intValue();
		cents = cents.setScale(2);  // it is possible to store repeating decimals in sfdc currency fields…I found out the hard way.
		String amtText = '$' + dollars.format() + cents.toPlainString().substring(1) ;
		return amtText ;		
	}
}