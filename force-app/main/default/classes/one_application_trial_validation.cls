public with sharing class one_application_trial_validation 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security ?!@?
     *  ---------------------------------------------------------------------------------------- */
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public one_application__c o 
 	{ 
 		get; 
 		private set; 
 	}

	//  Step control for panels on page
	public Integer flStepNumber
	{
		get ;
		private set ;
	}

	// Deposit #1
	public String deposit1
	{
		get ;
		set ;
	}

	// Deposit #2
	public String deposit2
	{
		get ;
		set ;
	}

    //  DisclosureURL
    public String ratesURLDisclosure 
    { 
        get ; 
        private set ; 
    }
    
    //  Live Agent
    public String liveAgentButton
    {
    	get ; 
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorDeposit1
    {
    	get ;
    	private set ;
    }
     
    public boolean errorDeposit2
    {
    	get ;
    	private set ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Counters
     *  ---------------------------------------------------------------------------------------- */
    public Integer trialValidationLimit 
    {
    	get ;
    	private set ;
    }
    
    public Integer trialCount
    {
    	get ;
    	private set ;
    }
   
    /*  ----------------------------------------------------------------------------------------
     *  Template Name/etc
     *  ---------------------------------------------------------------------------------------- */
    public String templateName
    {
        get ;
        private set ;
    }
    
    public String pageText
    {
        get ;
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Security
     *  ---------------------------------------------------------------------------------------- */
    private boolean hazDocsAlready ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public one_application_trial_validation ( ApexPages.Standardcontroller stdC )
	{
		//  --------------------------------------------------------------------------------------------------
		//  Setup
		//  --------------------------------------------------------------------------------------------------
		templateName = 'Generic_Application_Template' ;

        //  Default rate disclosure
    	ratesURLDisclosure = one_utils.getDisclosureURL () ;
    	
		//  --------------------------------------------------------------------------------------------------
		//  Live Agent Button
		//  --------------------------------------------------------------------------------------------------
		if ( ( one_settings__c.getInstance('settings').Live_Agent_Enable__c != null ) &&
             ( one_settings__c.getInstance('settings').Live_Agent_Enable__c == true ) )
        {
			liveAgentButton = one_settings__c.getInstance('settings').Live_Agent_Button_ID__c ;
        }
        else
        {
			liveAgentButton = null ;
        }
        
		//  Settings!
		if ( one_settings__c.getInstance('settings').Trial_Deposit_Validation_Limit__c == null )
		{
			trialValidationLimit = 3 ; // Default
		}
		else
		{
			trialValidationLimit = one_settings__c.getInstance('settings').Trial_Deposit_Validation_Limit__c.intValue () ;
		}
		
		//  Controls which panel we display, default of 0
		flStepNumber = 0 ;
		
        //  First check for application in session
        o = (one_application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
        
        hazDocsAlready = false ;
        
 		//  If no session present, use querystring instead
        if ( ( o == null ) || ( o.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
			
			//  Reset object
			o = null ;

			//  Both need to be passed in
			if ( ( aid != null ) && ( kid != null ) )
			{
                o = one_utils.getApplicationFromId ( aid ) ;
                
                //  Check for failure count
                if ( o.Trial_Deposit_Failure_Count__c == null )
                {
                    trialCount = 0 ;
                }
                else
                {
                    trialCount = o.Trial_Deposit_Failure_Count__c.intValue () ;
                }
                
                //  Key must match record on ID passed in
                if ( ! kid.equals ( o.Upload_Attachment_key__c ) )
                {
                    System.debug ( '========== Key does not match ==========' ) ;
                    o = null ;
                }
                
                else if ( ( o.Trial_Deposit_Validated_Datetime__c != null ) ||
                     ( o.Trial_Deposit_Failure_Datetime__c != null ) )
                {
                    System.debug ( '========== Already through ==========' ) ;
                    o = null ;
                }
                
                else if ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P501' ) )
                {
                    System.debug ( '========== Not valid for trial deposits ==========' ) ;
                    o = null ;
                }
			}
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = o.Id ;
			kid = o.Upload_Attachment_Key__c ;
			
			if ( o.Trial_Deposit_Failure_Count__c == null )
			{
				trialCount = 0 ;
			}
			else
			{
				trialCount = o.Trial_Deposit_Failure_Count__c.intValue () ;
			}
 		}
		
		//  Check again for things existing
		if ( ( o != null ) && ( aid != null ) && ( kid != null ) && ( trialCount != null ) )
		{
            System.debug ( '?!? "' + o.Number_of_Applicants__c + '"' ) ;
            
            System.debug ( '========== Override Rate Disclosure ==========' ) ;
            if ( String.isNotBlank ( o.Product__c ) && String.isNotBlank ( o.SubProduct__c ) && String.isNotBlank ( o.Plan__c ) )
	            ratesURLDisclosure = one_utils.getDisclosureURL ( o.Product__c, o.SubProduct__c, o.Plan__c ) ;
            	
            System.debug ( '========== Valid application found ==========' ) ;
            
            if ( String.isNotBlank ( o.RecordType.DeveloperName ) )
            {
                if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'Deposits' ) )
                {
                    pageText = 'You are on the right path to joining the #BankBlack movement!' ;
                    templateName = 'Deposit_Application_template_new' ;
                }
                else if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'UNITY_Visa' ) )
                {
                    pageText = 'You are on the right path to build or rebuild credit!' ;
                    templateName = 'application_template' ; 
                }
            }
            else
            {
                templateName = 'application_template' ; 
            }
            
            //  Check to see they actually have trial balances and are in the right status
            if ( ( o.Trial_Deposit_1__c != null ) && ( o.Trial_Deposit_2__c != null ) && ( o.ACH_Funding_Status__c != null ) )
            {
	            System.debug ( '========== Fields populated ==========' ) ;
	            
	            //  Too many fails, go to fail panel
	            if ( ( trialCount >= trialValidationLimit ) || ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Failed Verification' ) ) )
	            {
	            	flStepNumber = 1500 ;
	            }
            	else if ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
            	{
		            System.debug ( '========== Account in trial deposit! ==========' ) ;
		            
	            	//  Set the step number so the page shows
	            	flStepNumber = 1000 ;
                                
		            System.debug ( '========== DOC Check! ==========' ) ;
                    hazDocsAlready = one_utils.checkIDAttachments ( aid ) ;
            	}
            	else if ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Send Trial Deposit' ) )
            	{
		            System.debug ( '========== Account not ready! ==========' ) ;
		            
	            	//  Set the step number so the page shows
	            	flStepNumber = 10 ;
            	}
            }
            else
            {
	            System.debug ( '========== Application not set up for Trial Deposits ==========' ) ;
            }
		}
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Page actions
     *  ---------------------------------------------------------------------------------------- */
	public PageReference validateDeposit () 
	{
		errorDeposit1 = true ;
		errorDeposit2 = true ;
		
		//  Make sure these are valid
		if ( String.isNotBlank ( deposit1 ) && ( deposit1.isNumeric () ) && ( deposit1.length() == 2 ) )
		{
			System.debug ( '------------ Deposit 1 numeric! ------------- ' ) ;
			errorDeposit1 = false ;
		}
		
		if ( String.isNotBlank ( deposit2 ) && ( deposit2.isNumeric () ) && ( deposit2.length() == 2 ) )
		{
			System.debug ( '------------ Deposit 2 numeric! ------------- ' ) ;
			errorDeposit2 = false ;
		}

		//  Actual Comparison		
		if ( ( ! errorDeposit1 ) && ( ! errorDeposit2 ) )
		{
			System.debug ( '------------ Both inputs pass intial validation ------------- ' ) ;
			
			if ( ( ( Integer.valueOf ( o.Trial_Deposit_1__c ) == Integer.valueOf ( deposit1 ) ) &&
			       ( Integer.valueOf ( o.Trial_Deposit_2__c ) == Integer.valueOf ( deposit2 ) ) ) ||
				 ( ( Integer.valueOf ( o.Trial_Deposit_1__c ) == Integer.valueOf ( deposit2 ) ) &&
			       ( Integer.valueOf ( o.Trial_Deposit_2__c ) == Integer.valueOf ( deposit1 ) ) ) )
			{
                
                trialCount = 0 ;
                flStepNumber = 2000 ;
                
				System.debug ( '------------ Match! ------------- ' ) ;
				
				o.Trial_Deposit_Failure_Count__c = 0 ;
                o.Trial_Deposit_Validated_Datetime__c = System.now () ;
                
                if ( ( o.Photo_ID_Decision__c == null ) && ( o.Photo_ID_Override_Datetime__c == null ) && ( o.Photo_ID_Override_User__c == null ) )
	                o.Photo_ID_Decision__c = 'WARN' ;
                
                //  Both Quiz & Docs
                if ( String.isBlank ( o.Quiz_Outcome__c ) )
                {
                    System.debug ( 'No quiz, redirecting' ) ;
					o.Application_Processing_Status__c = 'Quiz Present' ;
                    o.FIS_Decision_Code__c = 'F501' ;
                    
                    access.updateObject ( o ) ;

                    PageReference pr = Page.one_application_id_authentication ;
                    pr.getParameters().put ( 'aid', o.Id ) ;
                    pr.getParameters().put ( 'kid', o.Upload_Attachment_key__c ) ;
                    pr.getParameters().put ( 'type','QUIZ' ) ;
                    return pr;
                }
                //  Haz quiz, determining docs
                else if ( ( ! o.Quiz_Outcome__c.startsWithIgnoreCase ( 'FAIL' ) ) && 
                          ( ! o.Quiz_Outcome__c.startsWithIgnoreCase ( 'Abandon' ) ) && 
                          ( o.Additional_Doc_Verified_Datetime__c != null ) && 
                          ( o.Additional_Doc_Verified_User__c != null ) &&
                          ( hazDocsAlready == true ) )
                {
                    //  Check for photo ID
                    if ( ( o.Photo_ID_Decision__c != null ) && ( o.Photo_ID_Override_Datetime__c != null ) && ( o.Photo_ID_Override_User__c != null ) )
                    {
                        o.ACH_Funding_Status__c = 'Cleared to Originate' ;
                        o.Trial_Deposit_Failure_Count__c = 0 ;
                        o.Trial_Deposit_Validated_Datetime__c = System.now () ;
                        
                        //  Set up helper object
                        FISHelper fish = new FISHelper () ;
                        fish.setApplication ( o ) ;
                        fish.setApplicant () ;
                        fish.goTrialDepositPass () ;
                        
                        trialCount = 0 ;
                        flStepNumber = 2000 ;
                    }
                    else
                    {
                        System.debug ( 'Quiz and Docs, fall through to final check' ) ;
                        o.FIS_Decision_Code__c = 'F501' ;
                        o.Application_Processing_Status__c = 'Additional Documentation Received' ;
                    }
                    
			        access.updateObject ( o ) ;
                }
                else
                {
                    System.debug ( 'Quiz yes, docs no, go to additional info' ) ;
                    o.FIS_Decision_Code__c = 'F501' ;
			        o.Application_Processing_Status__c = 'Additional Documentation Needed' ;
                    
			        access.updateObject ( o ) ;
                    
                    PageReference pr = Page.application_file_upload ;
                    pr.getParameters().put ( 'aid', o.Id ) ;
                    pr.getParameters().put ( 'kid', o.Upload_Attachment_key__c ) ;
                    pr.getParameters().put ( 'type','QUIZ' ) ;
                    return pr;
                }
			}
			else
			{
				System.debug ( '------------ No Match! ------------- ' ) ;
				
				deposit1 = null ;
				deposit2 = null ;
				trialCount ++ ;
				
	            if ( trialCount >= trialValidationLimit )
	            {
	            	o.ACH_Funding_Status__c = 'Failed Verification' ;
                    o.Trial_Deposit_Failure_Datetime__c = System.now () ;

					o.FIS_Decision_Code__c = 'D501' ; //?
                    flStepNumber = 3000 ;
                }
	            else
	            {
					flStepNumber = 1001 ;
	            }
	            
				o.Trial_Deposit_Failure_Count__c = trialCount ;
		        access.updateObject ( o ) ;
			}
		}
		else
		{
			flStepNumber = 1001 ;
		}
	
		return null ;
	}
}