@isTest
public with sharing class Test_application_refund_check 
{
	/*
	 *  Tests the invalid session code
	 */
	static testmethod void testIsDuplicate ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test11' ;
		c1.LastName = 'User11' ;
		c1.Email = 'test11@user11.com' ;
		
		insert c1 ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c1.Id ;
		fa.CREDITLIMITAMT__c = 1000.00 ;
		
		insert fa ;
		
		one_application__c o = new one_application__c () ;
		
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		
		o.Contact__c = c1.Id;
		
		o.First_Name__c = c1.FirstName ;
		o.Last_Name__c = c1.LastName ;
		o.Email_Address__c = c1.Email ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		
		o.Referred_By_Contact__c = c1.Id;
		o.Entered_By_Contact__c = c1.Id;

		o.FIS_Application_ID__c = '111222333' ;
		o.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		o.FIS_Decision_Code__c = 'F003' ;
		
		o.Funding_Account_Number__c = '12345679' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.Funding_Routing_Number__c = '123456789' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		
		o.Trial_Deposit_1__c = 7 ;
		o.Trial_Deposit_2__c = 11 ;
		o.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
		
		insert o ;
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		Map<String,String> groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Move to Refund' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		t.Application__c = o.Id ;
		t.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		t.OwnerId = groupId ;
		t.Application_Refund_Source__c = 'Holding' ;
		t.Card_Blocked__c = true ;
		
		insert t ;
		
		//  Point to page to test
		PageReference pr1 = Page.application_refund_check ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'id', t.id ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create empty object
		one_application__c app = new one_application__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		//  Feed the standard controller to the page controller
		application_refund_check c = new application_refund_check ( sc ) ;
		
		//  Autorun the action that's supposed to happen on load
		c.checkTicket () ;
		
		//  End of testing
		Test.stopTest () ;
	}

}