@isTest
public class test_loan_application_file_upload 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccountIndividual ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Individuals' ;
        
		return a ;
	}
	
	private static Account getAccountBusiness ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Busineses' ;
        
		return a ;
	}
	/*
	 * Creates a generic application I use for testing.
	 */
	private static loan_application__c getApplicationForTests ()
	{
		loan_application__c la = new loan_application__c () ;
        
        la.Organization_Name__c = 'FooBar' ;
        la.Organization_Email__c = 'foo@bar.com' ;
        la.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ('PPP', 'Loan_Application__c' ) ;
        la.Organization_Employee_Count__c = 10 ;
        la.Organization_Street__c = '1000 Franklin Road' ;
        la.Organization_City__c = 'Boston' ;
        la.Organization_State__c = 'MA' ;
        la.Organization_Zip_Code__c = '02110' ;
        la.Organization_DBA__c = 'Jim The Bob Slocum' ;
        la.Organization_Description__c = 'FooBar\'ed ' ;
        la.Organization_EIN__c = '666' ;
        
        return la ;
    }
        
	@testSetup 
    public static void setup ()
    {	
        Account a1 = getAccountIndividual () ;
        insert a1 ;
        
        Account a2 = getAccountBusiness () ;
        insert a2 ;
    }
    
     /*
	 * Tests with an existing application passed in
	 */	
	public static testmethod void testFileUploadCurrentSession ()
	{
		loan_application__c app = getApplicationForTests () ;
        app.Upload_Attachment_Key__c = loan_application_utils.getRandomString ( 30 ) ;
        app.Application_Processing_Status__c = 'Documentation Needed' ;
        app.Additional_Information_Required__c = 'Government ID Front;Government ID Back;Government ID Selfie;Organizational Documents' ;
        insert app ;
		
		//  Point to page to test
		PageReference pr1 = Page.loan_application_file_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		//  Feed the standard controller to the page controller
		loan_application_file_upload_controller c = new loan_application_file_upload_controller ( sc ) ;
		
        //  useless code coverage crap
        List<SelectOption> throwaway = c.docTypeItemDropDown ;
        
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 1000 ) ;
		System.assertEquals ( c.docTypeListSize, 4 ) ;
		
		//  Trigger the upload, no attachments
		PageReference pr2 = c.upload () ;
		
		//  There should be errors in this case
		System.assertEquals ( c.flStepNumber, 1002 ) ; // Error Step!
		System.assertEquals ( c.errorName, true ) ;
		System.assertEquals ( c.errorFile, true ) ;
		System.assertEquals ( c.docTypeListSize, 4 ) ; // No document size change
        
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Organizational Documents' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = app.id ;
		a1.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a1 ;
		
		//  Trigger the upload with attachment
		PageReference pr3 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // Success step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 3 ) ;  // One less document required!
        
		//  End of testing
		Test.stopTest () ;
    }
    
	/*
	 * Tests the page with the parameters passed in for an existing application
	 */
	public static testmethod void testFileUploadNewSession ()
	{
		loan_application__c app = getApplicationForTests () ;
        app.Upload_Attachment_Key__c = loan_application_utils.getRandomString ( 30 ) ;
        app.Application_Processing_Status__c = 'Documentation Needed' ;
        app.Additional_Information_Required__c = '2019 Payroll Reports;Organizational Documents' ;
        insert app ;
		
		//  Point to page to test
		PageReference pr1 = Page.loan_application_file_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', app.id );
        ApexPages.currentPage().getParameters().put ( 'kid', app.Upload_Attachment_Key__c );

		//  Empty application to pass in
		loan_application__c eApp = new loan_application__c () ;

		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eApp ) ;
		
		//  Feed the standard controller to the page controller
		loan_application_file_upload_controller c = new loan_application_file_upload_controller ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 1000 ) ;
		System.assertEquals ( c.docTypeListSize, 2 ) ;  // Remaining number of required documents
        
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = '2019 Payroll Reports' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = app.id ;
		a1.IsPrivate = false ;
        c.uploadFileName = '2019p.docx' ;

		//  Write attachment to controller like page would		
		c.attachment = a1 ;
		
		//  Trigger the upload with attachment
		PageReference pr2 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // success!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 1 ) ;  // One left

		//  Create new attachment
		Attachment a2 = new Attachment () ;
		a2.Name = 'Organizational Documents' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.ParentId = app.id ;
		a2.IsPrivate = false ;
        c.uploadFileName = 'Organizational Documents.docx' ;

		//  Write attachment to controller like page would		
		c.attachment = a2 ;
		
		//  Trigger the upload with attachment
		PageReference pr3 = c.upload () ;
        
		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 2000 ) ;  // FINAL step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 0 ) ;  // All docs!
        
		//  End of testing
		Test.stopTest () ;
    }
}