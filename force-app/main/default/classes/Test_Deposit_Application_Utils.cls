@isTest
public with sharing class Test_Deposit_Application_Utils 
{
    /* -----------------------------------------------------------------------------------
	 * Account
	 * ----------------------------------------------------------------------------------- */
    private static Account getAccount ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        
        insert a ;
        
        return a ;
    }
    
    public static testmethod void test_ONE ()
    {
    	list<SelectOption> sOL = Deposit_Application_Utils.docTypeItems ;
    	
    	system.assertNotEquals ( soL.size (), 0 ) ;
    }
    
    public static testmethod void test_TWO ()
    {
    	map<String,String> cM = Deposit_Application_Utils.codesAdditionalLanguage ;
    	
    	system.assertNotEquals ( cM.keySet ().size (), 0 ) ;
    }
    
    public static testmethod void test_THREE ()
    {
    	String s = null ;
    	
    	s = Deposit_Application_Utils.translateAdditionalInfoShortToLong ( null ) ;
   	
    	system.assertEquals ( s, null ) ;
    	
    	s = Deposit_Application_Utils.translateAdditionalInfoShortToLong ( 'ID (SS Card/Recent W-2)' ) ;
    	
    	system.assertEquals ( s, 'Identification Information (Social Security Card or recent W-2)' ) ;
    	
    	s = Deposit_Application_Utils.translateAdditionalInfoShortToLong ( 'ID (Driver Lic./State ID)' ) ;
    	
    	system.assertEquals ( s, 'Identification Information (Driver\'s license or State ID)' ) ;
    	
    	s = Deposit_Application_Utils.translateAdditionalInfoShortToLong ( 'Proof Income (Pay Stub/W-2/Other)' ) ;
   	
    	system.assertEquals ( s, 'Proof of income (Pay stub, W-2 or other)' ) ;
    	
    	s = Deposit_Application_Utils.translateAdditionalInfoShortToLong ( 'Utility Bill' ) ;
   	
    	system.assertEquals ( s, 'Utility Bill' ) ;
    }
    
    public static testmethod void test_FOUR ()
    {
        getAccount () ;
    	Deposit_Application__c da = Deposit_Application_Utils.getApplicationWithData ( 1 ) ;
    	
    	String s = Deposit_Application_Utils.getDocUploadQRCode ( da.ID, da.External_Key__c ) ;
    	
    	system.assertNotEquals ( s, null ) ;
    }
    
    public static testmethod void test_FIVE ()
    {
    	String s = Deposit_Application_Utils.getMultiPicklistDocs ( 'IDV' ) ;
    	
    	system.assertEquals ( Deposit_Application_Utils.getAdditionalInfoNeeded ( s ).size () , 2 ) ;
    }
    
    public static testmethod void test_SIX ()
    {
    	map<String,List<String>> cMap = Deposit_Application_Utils.codesAdditionalDocs ;
    	
    	system.assertNotEquals ( cMap, null ) ;
    	
    	String s = Deposit_Application_Utils.getMultiPicklistDocs ( 'INVALID DRIVERS LICENSE FORMAT' ) ;
    	
    	system.assertNotEquals ( s, null ) ;
    }
    
    public static testmethod void test_SEVEN ()
    {
    	map<String,List<String>> cMap = Deposit_Application_Utils.codesAdditionalDocs ;
    	
    	system.assertNotEquals ( cMap, null ) ;
    	
    	String s = Deposit_Application_Utils.getMultiPicklistDocs ( 'I DONT EXIST' ) ;
    	
    	system.assertEquals ( String.isBlank ( s ), true ) ;
    }
}