public class DepositApplicationHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Record Types
	 *  -------------------------------------------------------------------------------------------------- */	
	private static String prospectContactRecordTypeId = null ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Deposit Application tracking
	 *  -------------------------------------------------------------------------------------------------- */	
	public static map <String, boolean> depositApplicationMap ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Contact tracking
	 *  -------------------------------------------------------------------------------------------------- */	
	public static map <String, Contact> contactMap ;
	public static map <String, boolean> updatedContactMap ;
	public static map <String, ID> contactTaxIDMap ;

	/*  --------------------------------------------------------------------------------------------------
	 *  Deposit Application Email tracking
	 *  -------------------------------------------------------------------------------------------------- */	
	public static map <String, boolean> additionalMap ;
	public static map <String, boolean> denyMap ;
	public static map <String, boolean> fcraDenyMap ;

	/*  --------------------------------------------------------------------------------------------------
	 *  Generic checker
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean checkDuplicate ( Map<String,boolean> xMap, String id )
	{
		boolean returnValue = false ;
		
		if ( xMap.containsKey ( id ) == true )
			returnValue = true ;
		else
			xMap.put ( id, true ) ;
		
        System.debug ( 'Deposit Application Helper Duplicate Check : ' + returnValue ) ;
        
		return returnValue ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal data load
	 *  -------------------------------------------------------------------------------------------------- */
	private static void loadData ()
	{
		System.debug ( '----- Deposit Application Helper Loading Data' ) ;
		
		prospectContactRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Get IDs
	 *  -------------------------------------------------------------------------------------------------- */
	public static String getContactProspectRecordTypeId ()
	{
		if ( prospectContactRecordTypeId == null )
			loadData () ;
		
		return prospectContactRecordTypeId ;
	}
}