public class oneTransactionLoadLeadController
{
    /*  ----------------------------------------------------------------------------------------
     *  Security ?!@?
     *  ---------------------------------------------------------------------------------------- */
	private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Workshop
     *  -------------------------------------------------------------------------------------------------- */
    String workshopId = Web_Settings__c.getInstance ( 'Web Forms' ).OneTransaction_Workshop_ID__c ;
    Workshop_Attendee__c wa ;

    /*  --------------------------------------------------------------------------------------------------
     *  Incoming form stuff
     *  -------------------------------------------------------------------------------------------------- */
    private String fN ;
    private String lN ;
    private String eN ;
    private String cL ;
    private String eM ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Looking for
     *  -------------------------------------------------------------------------------------------------- */
    private Contact c ;
    private Lead l ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Extras
     *  -------------------------------------------------------------------------------------------------- */
    private static ID cAccountID = one_settings__c.getInstance('settings').Account_ID__c ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  UTM
     *  -------------------------------------------------------------------------------------------------- */
    private String UTM_Content ;
    private String UTM_Campaign ;
    private String UTM_Medium ;
    private String UTM_Source ;
    private String UTM_Term ;
    private String UTM_VisitorID ;

    /*  --------------------------------------------------------------------------------------------------
     *  Redirection stuff
     *  -------------------------------------------------------------------------------------------------- */
    private PageReference pr ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
	public oneTransactionLoadLeadController ()
    {
        //  -------------------- UTM Stuff --------------------
	    UTM_Content = ApexPages.currentPage().getParameters().get('UTM_Content__c') ;
        UTM_Campaign = ApexPages.currentPage().getParameters().get('UTM_Campaign__c') ;
        UTM_Medium = ApexPages.currentPage().getParameters().get('UTM_Medium__c') ;
        UTM_Source = ApexPages.currentPage().getParameters().get('UTM_Source__c') ;
        UTM_Term = ApexPages.currentPage().getParameters().get('UTM_Term__c') ;
        UTM_VisitorID = ApexPages.currentPage().getParameters().get('UTM_VisitorID__c') ;

        //  -------------------- Check existing --------------------
        fN = ApexPages.currentPage().getParameters().get('fN') ;
        lN = ApexPages.currentPage().getParameters().get('lN') ;
        eN = ApexPages.currentPage().getParameters().get('eN') ;
        cL = ApexPages.currentPage().getParameters().get('cL') ;
        eM = ApexPages.currentPage().getParameters().get('eM') ;
        
        //  Check for prior
        SObject xo = one_utils.getContactOrLead ( fN, lN, eN ) ;

        if ( xo != null )
        {
            //  Contact
            if ( xo instanceof Contact )
            {
                c = (Contact) xo ;
            }
            
            //  Lead
            if ( xo instanceof Lead )
            {
                l = (Lead) xo ;
            }
        }
        
        //  Found either nothing OR just a lead
        if ( c == null )
        {
            c = new Contact () ;
            c.RecordTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Prospect').RecordTypeId ;  
            c.AccountId = cAccountID ;
            
            c.FirstName = fN ;
            c.LastName = lN ;
            c.Email = eN ;

            if ( String.isNotBlank ( eM ) )
	            c.Employer__c = eM ;
        }
        
        c.LeadSource = 'OneTransaction' ;
        c.LeadSource_Change_Date__c = Date.today () ;
        
        if ( String.isNotBlank ( cL ) )
            c.OneTransaction_Focus__c = cL ;
        
        //  UTM CRAP
        one_utils.loadUTMValues ( null, c, UTM_Campaign, UTM_Content, UTM_Medium, UTM_Source, UTM_Term, UTM_VisitorID ) ;
        
        //  Redirect setup
        pr = new PageReference ( 'https://www.oneunited.com/onetransaction/thank-you/' ) ;
        
        //  Set UTM
        if ( ( String.isNotBlank ( UTM_Content ) ) && ( UTM_Content != '-' ) )
	        pr.getParameters().put ( 'UTM_Content', UTM_Content ) ;

        if ( ( String.isNotBlank ( UTM_Campaign ) ) && ( UTM_Campaign != '-' ) && ( UTM_Campaign != '(direct)' ) )
	        pr.getParameters().put ( 'UTM_Campaign', UTM_Campaign ) ;
        
        if ( ( String.isNotBlank ( UTM_Medium ) ) && ( UTM_Medium != '-' ) && ( UTM_Medium != '(none)' ) )
	        pr.getParameters().put ( 'UTM_Medium', UTM_Medium ) ;

        if ( ( String.isNotBlank ( UTM_Source ) ) && ( UTM_Source != '-' ) && ( UTM_Source != '(direct)' ) )
	        pr.getParameters().put ( 'UTM_Source', UTM_Source ) ;

        if ( ( String.isNotBlank ( UTM_Term ) ) && ( UTM_Term != '-' ) )
	        pr.getParameters().put ( 'UTM_Term', UTM_Term ) ;

        if ( ( String.isNotBlank ( UTM_VisitorID ) ) && ( UTM_VisitorID != '-' ) )
	        pr.getParameters().put ( 'UTM_VisitorID', UTM_VisitorID ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  GO!
     *  -------------------------------------------------------------------------------------------------- */
	public PageReference go ()
	{
        try
        {
            if ( c.ID != null )
            {
                System.debug ( 'Found Contact - Updating' ) ;
                
                access.updateObject ( c ) ;
            }
            else
            {
                System.debug ( 'new Contact - Inserting' ) ;
                
                access.insertObject ( c ) ;
            }
            
            if ( ( l != null ) && ( l.ID != null ) )
            {
                System.debug ( 'OLD Lead - Converting' ) ;
                
				LeadStatus convertStatus = null ;
                
                try
                {
                    convertStatus = [
                        SELECT Id, MasterLabel 
                        FROM LeadStatus 
                        WHERE IsConverted=true 
                        LIMIT 1
                    ];
                }
                catch (Exception e )
                {
                    convertStatus = null ;
                }
                
                if ( convertStatus != null )
                {
                    Database.LeadConvert lc = new database.LeadConvert () ;
                    lc.setLeadId ( l.ID ) ;
                    lc.setConvertedStatus ( convertStatus.MasterLabel ) ;
                    lc.setContactId ( c.ID ) ;
                    lc.setAccountId ( cAccountID ) ;
                    
                    try
                    {
                        Database.LeadConvertResult lcr = Database.convertLead ( lc ) ;
                    }
                    catch ( Exception e )
                    {
                        // ?
                    }
                }
            }
        }
        catch ( Exception e )
        {
            // ?
        }
         
        if ( c.ID != null )
        {
            try
            {
                // Workshop
                wa = new Workshop_Attendee__c () ;
            
                wa.First_Name__c = c.FirstName ;
                wa.Last_Name__c = c.LastName ;
                wa.Email_Address__c = c.Email ;
                wa.Phone_Number__c = c.Phone ;
                
                wa.Workshop_ID__c = workshopId ;
                wa.Contact__c = c.id ;
                
                if ( String.isNotBlank ( em ) )
                    wa.Employer__c = em ;
                
                System.debug ( 'Workshop - inserting' ) ;
                access.insertObject ( wa ) ;
            }
            catch ( Exception e )
            {
                //?
            }
        }
            
        return pr ;
    }
}