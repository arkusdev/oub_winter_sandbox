@isTest
public with sharing class TestCaseTrigger 
{
    private static void insertCustomSettings ()
    {
        IBM_Watson_Settings__c iws = new IBM_Watson_Settings__c () ;
        iws.API_IAM_Key__c = 'abc123' ;
        iws.API_Key__c = '123abc' ;
        iws.API_URL__c = 'http://baby.you.and.me/recognize' ;
        iws.API_Callout_Limit__c = 2.0 ;
        
        insert iws ;
    }
    
	private static void sleepInSeconds ( Integer delay )
	{
		Long startingTime = System.now().getTime() ; // Num milliseconds since Jan 1 1970
		Integer delayInMilliseconds = delay * 1000 ; // One-second delay
		
		while ( System.now().getTime() - startingTime < delayInMilliseconds )  
		{
            // Do nothing until desired delay has passed
		}
	}
	
	private static String getRecordType ( String name )
	{
		//  Need the record type
		RecordType rt = [ 
            SELECT r.ID,r.name 
            FROM RecordType r 
            WHERE r.name=:name 
            AND r.SObjectType='Case' 
        ] ;

		return rt.ID ;		
	}
	
	static testMethod void contactTests ()
	{
		//  Create a contact
		Contact ct1 = new Contact () ;
		
		ct1.FirstName = 'c1' ;
		ct1.LastName = 'c2' ;
		ct1.Email = 'c@cemail.com' ;
		
		insert ct1 ;
		
		//  Make sure the loan referral date is null
		System.assertEquals ( ct1.Loan_Referral_Date__c, null ) ;

		// Create a case
		Case c1 = new Case () ;
		c1.RecordTypeId = getRecordType ( 'Loan Referral' ) ;
		c1.Subject = 'Test1' ;
		c1.Status = 'Received';
		c1.ContactId = ct1.ID ;
		
		System.debug ( 'Insert Case 1' ) ;
		insert c1 ;
		
		//  Reselect the contact
		Contact ct2 = [SELECT c.ID, c.Loan_Referral_Date__c FROM Contact c WHERE c.ID = :ct1.Id] ;
		
		//  Loan referral date should not be null
		System.assertNotEquals ( ct2.Loan_Referral_Date__c, null ) ;
		
		//  Zzzz
		sleepInSeconds ( 5 ) ;
		
		//  Update case
		System.debug ( 'Update Case 1' ) ;
		c1.Status = 'Assigned';
		update c1 ;

		//  Reselect the contact
		Contact ct3 = [SELECT c.ID, c.Loan_Referral_Date__c FROM Contact c WHERE c.ID = :ct1.Id] ;
		
		//  Loan referral date should be the same as prior!
		System.assertEquals ( ct3.Loan_Referral_Date__c, ct2.Loan_Referral_Date__c ) ;
		
		//  Zzzz
		sleepInSeconds ( 5 ) ;
		
		// Create a second case
		Case c2 = new Case () ;
		c2.RecordTypeId = getRecordType ( 'Loan Referral' ) ;
		c2.Subject = 'Test2' ;
		c2.Status = 'Received';
		c2.ContactId = ct1.ID ;
		
		System.debug ( 'Insert Case 2' ) ;
		insert c2 ;
		
		//  Reselect the contact
		Contact ct4 = [SELECT c.ID, c.Loan_Referral_Date__c FROM Contact c WHERE c.ID = :ct1.Id] ;
		
		//  Loan referral date should not be the same as prior!
		System.assertNotEquals ( ct4.Loan_Referral_Date__c, ct2.Loan_Referral_Date__c ) ;
	}
	
	static testMethod void leadTests ()
	{
		// Create a Lead
		Lead l1 = new Lead () ;
		l1.FirstName = 'L1' ;
		l1.LastName = 'L2' ;
		l1.Email = 'l@lemail.com' ;
		l1.Company = 'Test Company' ;
		
		insert l1 ;
		
		//  Make sure the loan referral date is null
		System.assertEquals ( l1.Loan_Referral_Date__c, null ) ;
		
		// Create a case
		Case c1 = new Case () ;
		c1.RecordTypeId = getRecordType ( 'Loan Referral' ) ;
		c1.Subject = 'Test1' ;
		c1.Status = 'Received';
		c1.Lead__c = l1.Id ;
		
		System.debug ( 'Insert Case 1' ) ;
		insert c1 ;
		
		//  Reselect the lead
		Lead l2 = [SELECT l.ID, l.Loan_Referral_Date__c FROM Lead l WHERE l.ID = :l1.Id] ;
		
		//  Loan referral date should not be null
		System.assertNotEquals ( l2.Loan_Referral_Date__c, null ) ;
		
		//  Zzzz
		sleepInSeconds ( 5 ) ;
		
		//  Update case
		System.debug ( 'Update Case 1' ) ;
		c1.Status = 'Assigned';
		update c1 ;

		//  Reselect the lead
		Lead l3 = [SELECT l.ID, l.Loan_Referral_Date__c FROM Lead l WHERE l.ID = :l1.Id] ;
		
		//  Loan referral date should be the same as prior!
		System.assertEquals ( l3.Loan_Referral_Date__c, l2.Loan_Referral_Date__c ) ;
		
		//  Zzzz
		sleepInSeconds ( 5 ) ;
		
		// Create a second case
		Case c2 = new Case () ;
		c2.RecordTypeId = getRecordType ( 'Loan Referral' ) ;
		c2.Subject = 'Test2' ;
		c2.Status = 'Received';
		c2.Lead__c = l1.Id ;
		
		System.debug ( 'Insert Case 2' ) ;
		insert c2 ;
		
		//  Reselect the lead
		Lead l4 = [SELECT l.ID, l.Loan_Referral_Date__c FROM Lead l WHERE l.ID = :l1.Id] ;
		
		//  Loan referral date should not be the same as prior!
		System.assertNotEquals ( l4.Loan_Referral_Date__c, l2.Loan_Referral_Date__c ) ;
	} 
    
    static testMethod void ContactSearchTest()
    {
        //  Create a contact
		Contact ct1 = new Contact () ;
		
		ct1.FirstName = 'c1' ;
		ct1.LastName = 'c2' ;
		ct1.Email = 'jmarriner@oneunited.com' ;
		
		insert ct1 ;
        
        Case c1 = new Case () ;
		c1.RecordTypeId = getRecordType ( 'Loan Referral' ) ;
		c1.Subject = 'Test1' ;
		c1.Status = 'Received';
		c1.ContactId = null ;
        c1.SuppliedEmail = 'jmarriner@oneunited.com';
		
		System.debug ( 'Insert Case 1' ) ;
		insert c1 ;
    }
    
	static testMethod void watsonTest ()
	{
        insertCustomSettings () ;
        
        Contact c1 = new Contact () ;
        
		c1.FirstName = 'c1' ;
		c1.LastName = 'c2' ;
		c1.Email = 'jmarriner@oneunited.com' ;
		
		insert c1 ;
        
        Case c = new Case () ;
        c.subject = 'Shoretel' ;
        c.ContactId = c1.ID ;
        
        insert c ;
        
        Attachment a = new Attachment () ;
        a.ParentId = c.ID ;
		a.Name = 'Attach' + c.ID + '.wav' ;
        a.ContentType = 'audio/wav' ;
		a.IsPrivate = false ;

		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a.body = bodyBlob1 ;
        
        insert a ;
        
        IBMWatsonMultiMockResponse mockResponse = new IBMWatsonMultiMockResponse () ;
        Test.setMock ( HttpCalloutMock.class, mockResponse ) ;
            
        //  Go!
        Test.startTest () ;
        
        c.Voicemail_Translation_Status__c = 'Request Translation' ;
        update c ;
        
        Test.stopTest () ;
        
        Case cx = [
            SELECT ID, Voicemail_Translation__c, Voicemail_Translation_Status__c
            FROM Case
            WHERE ID = :c.ID
        ] ;
        
        System.assertNotEquals ( cx.Voicemail_Translation__c, null ) ;
        System.assertNotEquals ( cx.Voicemail_Translation_Status__c, null ) ;
    }
    
    static testmethod void phoneSupportTest ()
    {
        insertCustomSettings () ;
        
        Contact c1 = new Contact () ;
        
		c1.FirstName = 'c1' ;
		c1.LastName = 'c2' ;
		c1.Email = 'jmarriner@oneunited.com' ;
        c1.phone = '562-235-4647' ;
        c1.MobilePhone = '132-987-4657' ;
		
		insert c1 ;
        
        Test.startTest () ;
        
        Case c = new Case () ;
        c.RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Phone Support').RecordTypeId ;
        c.Subject = 'Mitel voice message from +15622354647 for mailbox 4805' ;
        c.Description = 'Foo Bar' ;
        
        insert c ;
        
        Test.stopTest () ;
        
        Case cx = [
            SELECT ID, ContactID
            FROM Case
            WHERE ID = :c.Id
        ] ;
        
        System.assertEquals ( cx.ContactID, c1.ID ) ;
    }
}