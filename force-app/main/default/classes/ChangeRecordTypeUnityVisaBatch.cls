global class ChangeRecordTypeUnityVisaBatch implements Database.Batchable<sObject>{
    
	private String lastModifiedDate ;
    
    global ChangeRecordTypeUnityVisaBatch(String inDate){
        if(String.isEmpty(inDate)){
            lastModifiedDate = 'TODAY';
        }else{
            lastModifiedDate = inDate ;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = '' ;
        query += 'SELECT ID, RecordTypeId, Funding_Options__c, Phone__c, Work_Phone__c, Co_Applicant_Phone__c, Co_Applicant_Work_Phone__c, Gross_Income_Monthly__c, Additional_Income_Amount__c, Co_Applicant_Gross_Income_Monthly__c,Co_Applicant_Additional_Income_Amount__c ' ;
        query += 'FROM one_application__c ' ;
        query += 'WHERE LastModifiedDate<=' + String.valueOf(lastModifiedDate) + ' ';
        if ( ! test.isRunningTest () )
            query += 'AND RecordTypeID != null ' ;
        query += 'ORDER BY CreatedDate DESC';
        
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC,List<one_application__c> scope){
        
        list<one_application__c> listToUpdate = new list<one_application__c> () ;
        
        for(one_application__c applicationRecord : scope)
        {
            if( (String.isEmpty(applicationRecord.RecordTypeId)) || ( Test.isRunningTest () ) )
            {
            	applicationRecord.RecordTypeId = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('UNITY Visa').RecordTypeId;
                
                // Messed up application fixes
                if ( String.isBlank ( applicationRecord.Funding_Options__c ) )
                    applicationRecord.Funding_Options__c = 'Other' ;
                
                if ( String.isNotBlank ( applicationRecord.Phone__c ) && ( applicationRecord.Phone__c.length () < 10 ) )
					fixPhone ( applicationRecord.Phone__c ) ;
                
                if ( String.isNotBlank ( applicationRecord.Work_Phone__c ) && ( applicationRecord.Work_Phone__c.length () < 10 ) )
					fixPhone ( applicationRecord.Work_Phone__c ) ;
                
                if ( String.isNotBlank ( applicationRecord.Co_Applicant_Phone__c ) && ( applicationRecord.Co_Applicant_Phone__c.length () < 10 ) )
					fixPhone ( applicationRecord.Co_Applicant_Phone__c ) ;
                
                if ( String.isNotBlank ( applicationRecord.Co_Applicant_Work_Phone__c ) && ( applicationRecord.Co_Applicant_Work_Phone__c.length () < 10 ) )
					fixPhone ( applicationRecord.Co_Applicant_Work_Phone__c ) ;
                
                Decimal total = 0.0 ;
                
                if ( applicationRecord.Gross_Income_Monthly__c != null )
                    total += applicationRecord.Gross_Income_Monthly__c ;
                
                if ( applicationRecord.Additional_Income_Amount__c != null )
                    total += applicationRecord.Additional_Income_Amount__c ;
                
                if ( applicationRecord.Co_Applicant_Gross_Income_Monthly__c != null )
                    total += applicationRecord.Co_Applicant_Gross_Income_Monthly__c ;
                
                if ( applicationRecord.Co_Applicant_Additional_Income_Amount__c != null )
                    total += applicationRecord.Co_Applicant_Additional_Income_Amount__c ;
                
                if ( total == 0.0 )
                {
                    applicationRecord.Additional_Income_Amount__c = 1.0 ;
                }
            }
            
            listToUpdate.add ( applicationRecord ) ;
        }
		
		if ( ( listToUpdate != null ) && ( listToUpdate.size () >0 ) )
            update listToUpdate ;
    }

    global void finish(Database.BatchableContext BC)
    {
    }
    
    public String fixPhone ( String inString )
    {
        String rString = inString ;
        
        for ( integer i = inString.length () ; i < 10 ; i ++ )
            rString += '0' ;
        
        return rString ;
    }
}