@isTest
public with sharing class TestContactDeDuplication 
{
	static testmethod void deDupeTest ()
	{
		//  --------------------------------------------------
		//  Create dummy contact set 1
		//  --------------------------------------------------
		Contact c11 = new Contact();
		c11.FirstName = 'c1';
		c11.LastName = 'c1';
		c11.Email = 'hey1@emailhey.com';
		c11.UploadID__c = 'INSIGHT-P-007' ;
		c11.WorkEmail__c = 'hey2@emailhey.com' ;
		insert c11;
		
		Contact c12 = new Contact();
		c12.FirstName = 'c1';
		c12.LastName = 'c1';
		c12.Email = 'whey@emailhey.com';
		c12.UploadID__c = 'INSIGHT-P-008' ;
		c12.Phone = '132-987-5797' ;
		insert c12;
		
		//  --------------------------------------------------
		//  Create dummy contact set 2
		//  --------------------------------------------------
		Contact c21 = new Contact();
		c21.FirstName = 'c2';
		c21.LastName = 'c2';
		c21.Email = 'hey2@emailhey.com';
		c21.UploadID__c = 'INSIGHT-P-707' ;
		c21.WorkEmail__c = 'xhey@emailhey.com' ;
		insert c21;
		
		Contact c22 = new Contact();
		c22.FirstName = 'c2';
		c22.LastName = 'c2';
		c22.Email = 'hey2@emailhey.com';
		c22.UploadID__c = 'INSIGHT-P-708' ;
		c22.Phone = '213-987-6546' ;
		insert c22;
		
		Contact c23 = new Contact () ;
		c23.FirstName = 'c2';
		c23.LastName = 'c2';
		c23.Email = 'hey2@emailhey.com';
		c23.UploadID__c = 'INSIGHT-P-709' ;
		c23.MobilePhone='654-987-3211' ;
		insert c23 ;
		
		//  --------------------------------------------------
		//  Create dummy contact set 3
		//  --------------------------------------------------
		Contact c31 = new Contact();
		c31.FirstName = 'c3';
		c31.LastName = 'c3';
		c31.Email = 'hey3@emailhey.com';
		c31.UploadID__c = 'INSIGHT-P-717' ;
		c31.WorkEmail__c = 'xhey@emailhey.com' ;
		insert c31;
		
		Contact c32 = new Contact();
		c32.FirstName = 'c3';
		c32.LastName = 'c3';
		c32.Email = 'hey3@emailhey.com';
		c32.UploadID__c = 'INSIGHT-P-718' ;
		c32.HomePhone = '213-111-9849' ;
		insert c32;
		
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		ContactDeDuplication cdd = new ContactDeDuplication () ;
		Database.executeBatch ( cdd ) ;
		
		Test.stopTest() ;
		
		System.debug ( '----- TEST RESULTS! #1 -----' ) ;
		Contact rc1 = [
			SELECT ID, FirstName, LastName, Email, uploadID__c, WorkEmail__c, Phone, MobilePhone
			FROM Contact
			WHERE uploadID__c = 'INSIGHT-P-007'
		] ;
		
		System.debug ( '1: ' + rc1.id ) ;
		System.debug ( '2: ' + rc1.FirstName ) ;		
		System.debug ( '3: ' + rc1.LastName ) ;
		System.debug ( '4: ' + rc1.Email ) ;
		System.debug ( '5: ' + rc1.UploadID__c ) ;
		System.debug ( '6: ' + rc1.WorkEmail__c ) ;
		System.debug ( '7: ' + rc1.Phone ) ;
		System.debug ( '8: ' + rc1.MobilePhone ) ;
		
		System.debug ( '----- TEST RESULTS! #2 -----' ) ;
		Contact rc2 = [
			SELECT ID, FirstName, LastName, Email, uploadID__c, WorkEmail__c, Phone, MobilePhone
			FROM Contact
			WHERE uploadID__c = 'INSIGHT-P-707'
		] ;
		
		System.debug ( '1: ' + rc2.id ) ;
		System.debug ( '2: ' + rc2.FirstName ) ;		
		System.debug ( '3: ' + rc2.LastName ) ;
		System.debug ( '4: ' + rc2.Email ) ;
		System.debug ( '5: ' + rc2.UploadID__c ) ;
		System.debug ( '6: ' + rc2.WorkEmail__c ) ;
		System.debug ( '7: ' + rc2.Phone ) ;
		System.debug ( '8: ' + rc2.MobilePhone ) ;
	}
}