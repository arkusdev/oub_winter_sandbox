@isTest
public class TestFileCopyActionFunction 
{
    public static testmethod void BOTHtoBOTH ()
    {
        Contact c = new Contact () ;
        c.FirstName = 'Hello' ;
        c.LastName = 'World' ;
        c.Email = 'hello@world.com' ;
        
        insert c ;
        
        Attachment a1 = new Attachment () ;
        a1.Name = 'ID (Driver Lic./State ID)' ;
        a1.ContentType = 'image/jpeg' ;
        a1.Body = Blob.valueOf ( 'yo noid' ) ;
        a1.ParentId = c.ID ;
        a1.Description = 'Yo, Noid!' ;
        a1.IsPrivate = false ;
        
        insert a1 ;
        
        Attachment a2 = new Attachment () ;
        a2.Name = 'RevillaID.pdf' ;
        a2.Body = Blob.valueOf ( 'yo noid' ) ;
        a2.ParentId = c.ID ;
        a2.Description = 'Yo, Noid!' ;
        a2.IsPrivate = false ;
        
        insert a2 ;
        
        ContentVersion cv1 = new ContentVersion () ;
        cv1.ContentLocation = 'S' ;
        cv1.Title = 'Test file 1' ;
        cv1.PathOnClient = 'test1.txt' ;
        cv1.Description = 'Test file 1' ;
        cv1.VersionData = Blob.valueOf ( 'yo noid' ) ;
        
        insert cv1 ;
        
        ContentVersion cv1x = [ SELECT ID, ContentDocumentId FROM ContentVersion WHERE ID = :cv1.ID ] ;
        
        ContentDocumentLink cdl1 = new ContentDocumentLink () ;
        cdl1.ContentDocumentId = cv1x.ContentDocumentId ;
        cdl1.ShareType = 'V' ;
        cdl1.LinkedEntityId = c.ID ;
        cdl1.Visibility = 'AllUsers' ;
        
        insert cdl1 ; 
        
        ContentVersion cv2 = new ContentVersion () ;
        cv2.ContentLocation = 'S' ;
        cv2.Title = 'Test file 2' ;
        cv2.PathOnClient = 'test2.txt' ;
        cv2.Description = 'Test file 2' ;
        cv2.VersionData = Blob.valueOf ( 'yo noid' ) ;
        
        insert cv2 ;
        
        ContentVersion cv2x = [ SELECT ID, ContentDocumentId FROM ContentVersion WHERE ID = :cv2.ID ] ;
        
        ContentDocumentLink cdl2 = new ContentDocumentLink () ;
        cdl2.ContentDocumentId = cv2x.ContentDocumentId ;
        cdl2.ShareType = 'V' ;
        cdl2.LinkedEntityId = c.ID ;
        cdl2.Visibility = 'AllUsers' ;
        
        insert cdl2 ; 
        
        Ticket__c t = new Ticket__c () ;
        t.First_Name__c = 'Hello' ;
        t.Last_Name__c = 'World' ;
        t.Email_Address__c = 'hello@world.com' ;
        
        insert t ;
        
        Test.startTest () ;
        
        list<FileCopyActionFunction.FileCopyRequest> fcaffcrL = new list<FileCopyActionFunction.FileCopyRequest> () ;
        
        FileCopyActionFunction.FileCopyRequest fcaffcr = new FileCopyActionFunction.FileCopyRequest () ;
        fcaffcr.fromID = c.ID ;
        fcaffcr.toID = t.ID ;
        
        fcaffcrL.add ( fcaffcr ) ;
        
        list<FileCopyActionFunction.FileCopyResult> xfcaffcrL = FileCopyActionFunction.copyfiles ( fcaffcrL ) ;
       
        FileCopyActionFunction.FileCopyResult xfcaffcr = xfcaffcrL [ 0 ] ;
        
        System.assertEquals ( xfcaffcr.numberOfFilesCopied, 4 ) ;
        
        Test.stopTest () ;
    }
}