public class ApplicationEmailFunctions 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal
	 *  -------------------------------------------------------------------------------------------------- */	
    private static String emailMethod
    {
        get 
        {
            if ( String.isBlank ( emailMethod ) )
            {
                one_settings__c settings = one_settings__c.getInstance ( 'settings' ) ;
                
                if ( settings != null )
	                emailMethod = settings.Email_Integration_Class__c ;
            }
            
            if ( String.isBlank ( emailMethod ) )
                emailMethod = 'ApplicationEmailFunctionsMandrill' ;
            
            return emailMethod ;
        }
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - USED ON SUCCESS PAGE!
	 *  -------------------------------------------------------------------------------------------------- */
	public static void sendWelcomeEmails ( String sId )
	{
        one_application__c app = getRecordType ( sId ) ;
        
        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
        {
            ApplicationEmailFunctionsMC.sendWelcomeEmails ( sId ) ;
        }
        else
        {
            
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendWelcomeEmails ( sId ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendWelcomeEmails ( sId ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - IDV/OFAC Decline!
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendIDVOFACDeclineEmail ( String sId )
	{
        one_application__c app = getRecordType ( sId ) ;
        
        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
        {
            ApplicationEmailFunctionsMC.sendIDVOFACDeclineEmail ( sId ) ;
        }
        else
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendIDVOFACDeclineEmail ( sId ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendIDVOFACDeclineEmail ( sId ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - USED ON SUCCESS PAGE!
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendTrialDepositEmail ( String sId )
	{
        one_application__c app = getRecordType ( sId ) ;
        
        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
        {
            ApplicationEmailFunctionsMC.sendTrialDepositEmail ( sId ) ;
        }
        else
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendTrialDepositEmail ( sId ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendTrialDepositEmail ( sId ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - USED ON Trial deposit too many failed attempts
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendTrialDepositFailEmails ( String sId )
	{
        one_application__c app = getRecordType ( sId ) ;
        
        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
        {
            ApplicationEmailFunctionsMC.sendTrialDepositFailEmails ( sId ) ;
        }
        else
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendTrialDepositFailEmails ( sId ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendTrialDepositFailEmails ( sId ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - APPROVED
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendApprovedEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( u, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( d, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( d, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
    }
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - FUNDING
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendFundingEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, u, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, d, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, d, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - INFO
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendAdditionalInfoEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, u, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, d, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, d, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - TRIAL
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendTrialDepositEmails ( List<String> sId )
	{   
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, u, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, d, null, null, null, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, d, null, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - Warning
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendWarningEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, u, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, d, null, null, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, d, null, null, null, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - Denial
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendDenialEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, u, null, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, d, null, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, d, null, null, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - Withdrawal
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendWithdrawalEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, u, null, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, null, d, null, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, d, null, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - IDV Decline
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendIDVDeclineEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, u, null, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, null, null, d, null, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, d, null, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - OFAC Decline
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendOFACDeclineEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, u, null, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, null, null, null, d, null, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, d, null, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - FCRA Decline
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendFCRADeclineEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, u, null, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, null, null, null, null, d, null, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, d, null, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - ACH Rejection
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendACHRejectionEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, u, null, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, null, null, null, null, null, d, null, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, d, null, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - Counteroffer
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendCounterOfferEmails ( List<String> sId )
	{
        list<one_application__c> appL = getRecordTypes ( sId ) ;
        
        list<String> u = new list<String> () ;
        list<String> d = new list<String> () ;
        
        for ( one_application__c app : appL )
        {
	        if ( app.RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                u.add ( app.ID ) ;
            else
                d.add ( app.ID ) ;
        }
        
        if ( u.size () > 0 )
        {
            ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, u, null, null, null, null ) ;
        }
        
        if ( d.size () > 0 )
        {
            if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                ApplicationEmailFunctionsMandrill.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, d, null, null, null, null ) ;
            else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, d, null, null, null, null ) ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - ADV401 - Citizenship
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendNotACitizenEmails ( List<String> sId )
	{
        //  Only goes to marketing cloud!
        ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, null, sID, null, null, null ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - ADV403 - Out of Area
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendOutOfAreaEmails ( List<String> sId )
	{
        //  Only goes to marketing cloud!
        ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, null, null, sID, null, null ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - ADV405 - Minor
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendMinorEmails ( List<String> sId )
	{
        //  Only goes to marketing cloud!
        ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, null, null, null, sID, null ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - ADV410 - Young
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendYoungEmails ( List<String> sId )
	{
        //  Only goes to marketing cloud!
        ApplicationEmailFunctionsMC.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, sID ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendEmails ( List<String> approvalId, List<String> fundingId, List<String> additionalId, 
                                    List<String> trialId, List<String> warningId, List<String> denyId, List<String> withdrawalId, 
                                    List<String> idvID, List<String> ofacID, List<String> fcraID, List<String> achRejectID, List<String> counterOfferID,
                                    List<String> notCitizenID, List<String> outOfAreaID, list<String> minorID, list<String> youngID )
	{
		sendEmailsBATCH ( approvalId, fundingId, additionalId, 
                          trialId, warningId, denyId, withdrawalID, 
                          idvID, ofacID, fcraID, achRejectID, counterofferID, 
                          notCitizenID, outOfAreaID, minorID, youngID ) ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - COMBINED
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendEmailsBATCH ( List<String> approvalId, List<String> fundingId, List<String> additionalId, 
                                         List<String> trialId, List<String> warningId, List<String> denyId, List<String> withdrawalId, 
                                         List<String> idvID, List<String> ofacID, List<String> fcraID, List<String> achRejectID, List<String> counterOfferID,
                                         List<String> notCitizenID, List<String> outOfAreaID, list<String> minorID, list<String> youngID )
    {
        list<String> xL = new list<String> () ;
        
        if ( ( approvalId != null ) && ( approvalId.size () > 0 ) )
	        xL.addall ( approvalId ) ;
        
        if ( ( fundingId != null ) && ( fundingId.size () > 0 ) )
	        xL.addall ( fundingId ) ;
        
        if ( ( additionalId != null ) && ( additionalId.size () > 0 ) )
        	xL.addall ( additionalId ) ;

        if ( ( trialId != null ) && ( trialId.size () > 0 ) )
        	xL.addall ( trialId ) ;

        if ( ( warningId != null ) && ( warningId.size () > 0 ) )
        	xL.addall ( warningId ) ;

        if ( ( denyId != null ) && ( denyId.size () > 0 ) )
        	xL.addall ( denyId ) ;

        if ( ( withdrawalId != null ) && ( withdrawalId.size () > 0 ) )
        	xL.addall ( withdrawalId ) ;

        if ( ( idvID != null ) && ( idvID.size () > 0 ) )
	        xL.addall ( idvID ) ;

        if ( ( ofacID != null ) && ( ofacID.size () > 0 ) )
        	xL.addall ( ofacID ) ;

        if ( ( fcraID != null ) && ( fcraID.size () > 0 ) )
        	xL.addall ( fcraID ) ;

        if ( ( achRejectID != null ) && ( achRejectID.size () > 0 ) )
        	xL.addall ( achRejectID ) ;

        if ( ( counterOfferID != null ) && ( counterOfferID.size () > 0 ) )
        	xL.addall ( counterOfferID ) ;

        if ( ( notCitizenID != null ) && ( notCitizenID.size () > 0 ) )
        	xL.addall ( notCitizenID ) ;

        if ( ( outOfAreaID != null ) && ( outOfAreaID.size () > 0 ) )
        	xL.addall ( outOfAreaID ) ;

        if ( ( minorID != null ) && ( minorID.size () > 0 ) )
        	xL.addall ( minorID ) ;

        if ( ( youngID != null ) && ( youngID.size () > 0 ) )
	        xL.addall ( youngID ) ;
        
        list<one_application__c> appL = getRecordTypes ( xL ) ;
        
        map<String,one_application__c> appM = new map<String,one_application__c> () ;
        
        for ( one_application__c app : appL ) 
            appM.put ( app.ID, app ) ;
        
 		map<String,list<String>> approvalM = parseOMatic ( approvalId, appM ) ;
 		map<String,list<String>> fundingM = parseOMatic ( fundingId, appM ) ;
 		map<String,list<String>> additionalM = parseOMatic ( additionalId, appM ) ;
 		map<String,list<String>> trialM = parseOMatic ( trialId, appM ) ;
 		map<String,list<String>> warningM = parseOMatic ( warningId, appM ) ;
 		map<String,list<String>> denyM = parseOMatic ( denyId, appM ) ;
 		map<String,list<String>> withdrawalM = parseOMatic ( withdrawalId, appM ) ;
 		map<String,list<String>> idvM = parseOMatic ( idvID, appM ) ;
 		map<String,list<String>> ofacM = parseOMatic ( ofacID, appM ) ;
 		map<String,list<String>> fcraM = parseOMatic ( fcraID, appM ) ;
 		map<String,list<String>> achRejectM = parseOMatic ( achRejectID, appM ) ;
 		map<String,list<String>> counterOfferM = parseOMatic ( counterOfferID, appM ) ;
 		map<String,list<String>> notCitizenM = parseOMatic ( notCitizenID, appM ) ;
 		map<String,list<String>> outOfAreaM = parseOMatic ( outOfAreaID, appM ) ;
 		map<String,list<String>> minorM = parseOMatic ( minorID, appM ) ;
 		map<String,list<String>> youngM = parseOMatic ( youngID, appM ) ;

		if ( ( approvalM.get ( 'U' ).size () > 0 ) || ( fundingM.get ( 'U' ).size () > 0 ) || ( additionalM.get ( 'U' ).size () > 0 ) || 
             ( trialM.get ( 'U' ).size () > 0 ) || ( warningM.get ( 'U' ).size () > 0 ) || ( denyM.get ( 'U' ).size () > 0 ) || 
             ( withdrawalM.get ( 'U' ).size () > 0 ) || ( idvM.get ( 'U' ).size () > 0 ) || ( ofacM.get ( 'U' ).size () > 0 ) || 
             ( fcraM.get ( 'U' ).size () > 0 ) || ( achRejectM.get ( 'U' ).size () > 0 ) || ( counterOfferM.get ( 'U' ).size () > 0 ) || 
             ( notCitizenM.get ( 'U' ).size () > 0 ) || ( outOfAreaM.get ( 'U' ).size () > 0 ) || ( minorM.get ( 'U' ).size () > 0 ) || 
             ( youngM.get ( 'U' ).size () > 0 ) )
        {
            ApplicationEmailFunctionsMC.sendEmails ( approvalM.get ( 'U' ), fundingM.get ( 'U' ), additionalM.get ( 'U' ), 
                                                     trialM.get ( 'U' ), warningM.get ( 'U' ), denyM.get ( 'U' ), withdrawalM.get ( 'U' ), 
                                                     idvM.get ( 'U' ), ofacM.get ( 'U' ), fcraM.get ( 'U' ), achRejectM.get ( 'U' ), counterOfferM.get ( 'U' ), 
                                                     notCitizenM.get ( 'U' ), outOfAreaM.get ( 'U' ), minorM.get ( 'U' ), youngM.get ( 'U' ) ) ;
        }
        
		if ( ( approvalM.get ( 'D' ).size () > 0 ) || ( fundingM.get ( 'D' ).size () > 0 ) || ( additionalM.get ( 'D' ).size () > 0 ) || 
             ( trialM.get ( 'D' ).size () > 0 ) || ( warningM.get ( 'D' ).size () > 0 ) || ( denyM.get ( 'D' ).size () > 0 ) || 
             ( withdrawalM.get ( 'D' ).size () > 0 ) || ( idvM.get ( 'D' ).size () > 0 ) || ( ofacM.get ( 'D' ).size () > 0 ) || 
             ( fcraM.get ( 'D' ).size () > 0 ) || ( achRejectM.get ( 'D' ).size () > 0 ) || ( counterOfferM.get ( 'D' ).size () > 0 ) || 
             ( notCitizenM.get ( 'D' ).size () > 0 ) || ( outOfAreaM.get ( 'D' ).size () > 0 ) || ( minorM.get ( 'D' ).size () > 0 ) || 
             ( youngM.get ( 'D' ).size () > 0 ) )
        {
            if ( System.isBatch () || System.isFuture() )
            {
                if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                    ApplicationEmailFunctionsMandrill.sendEmailsBatch ( approvalM.get ( 'D' ), fundingM.get ( 'D' ), additionalM.get ( 'D' ), 
                                                                   		trialM.get ( 'D' ), warningM.get ( 'D' ), denyM.get ( 'D' ), withdrawalM.get ( 'D' ), 
                                                                   		idvM.get ( 'D' ), ofacM.get ( 'D' ), fcraM.get ( 'D' ), achRejectM.get ( 'D' ), counterOfferM.get ( 'D' ), 
                                                                   		null, null, null, null ) ;
                else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                    ApplicationEmailFunctionsMC.sendEmailsBatch ( approvalM.get ( 'D' ), fundingM.get ( 'D' ), additionalM.get ( 'D' ), 
                                                             		trialM.get ( 'D' ), warningM.get ( 'D' ), denyM.get ( 'D' ), withdrawalM.get ( 'D' ), 
                                                             		idvM.get ( 'D' ), ofacM.get ( 'D' ), fcraM.get ( 'D' ), achRejectM.get ( 'D' ), counterOfferM.get ( 'D' ), 
                                                             		null, null, null, null ) ;
            }
            else
            {
                if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMandrill' ) )
                    ApplicationEmailFunctionsMandrill.sendEmails ( approvalM.get ( 'D' ), fundingM.get ( 'D' ), additionalM.get ( 'D' ), 
                                                                   trialM.get ( 'D' ), warningM.get ( 'D' ), denyM.get ( 'D' ), withdrawalM.get ( 'D' ), 
                                                                   idvM.get ( 'D' ), ofacM.get ( 'D' ), fcraM.get ( 'D' ), achRejectM.get ( 'D' ), counterOfferM.get ( 'D' ), 
                                                                   null, null, null, null ) ;
                else if ( emailMethod.equalsIgnoreCase ( 'ApplicationEmailFunctionsMC' ) )
                    ApplicationEmailFunctionsMC.sendEmails ( approvalM.get ( 'D' ), fundingM.get ( 'D' ), additionalM.get ( 'D' ), 
                                                             trialM.get ( 'D' ), warningM.get ( 'D' ), denyM.get ( 'D' ), withdrawalM.get ( 'D' ), 
                                                             idvM.get ( 'D' ), ofacM.get ( 'D' ), fcraM.get ( 'D' ), achRejectM.get ( 'D' ), counterOfferM.get ( 'D' ), 
                                                             null, null, null, null ) ;
            }
        }
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Parse-o-foo-ing
	 *  -------------------------------------------------------------------------------------------------- */
    private static map<String,list<String>> parseOmatic ( list<String> xL, map<String,one_application__c> appM )
    {
        list<String> dL = new list<String> () ;
        list<String> uL = new list<String> () ;
        
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            for ( String x : xL )
            {
                if ( appM.containsKey ( x ) )
                {
                    if ( appM.get ( x ).RecordType.DeveloperName.equalsIgnoreCase ( 'unity_visa' ) )
                        uL.add ( x ) ;
                    else
                        dL.add ( x ) ;
                }
                else
                {
                    uL.add ( x ) ;
                }
            }
        }

        //  Always load into map, even if empty
        map<String,list<String>> rM = new map<String,list<String>> ();
        rM.put ( 'D', dL ) ;
        rM.put ( 'U', uL ) ;
        
        return rM ; 
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  JIM SELECT
	 *  -------------------------------------------------------------------------------------------------- */
    private static one_application__c getRecordType ( String x )
    {
        list<String> xL = new list<String> () ;
        xL.add ( x ) ;
        
        list<one_application__c> appL = getRecordTypes ( xL ) ;
        
        return appL [ 0 ] ;
    }
    
    private static list<one_application__c> getRecordTypes ( list<String> xL )
    {
        list<one_application__c> appL ;
        
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            try
            {
                appL = [
                    SELECT ID, RecordType.DeveloperName
                    FROM one_application__c
                    WHERE ID IN :xL 
                ] ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
        else
        {
			appL = new list<one_application__c> () ;            
        }
        
        return appL ;
    }
}