//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)

@isTest
public class fisItUdiUdibrokerMockImpl implements WebServiceMock {
	public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType) {
       
		System.debug(LoggingLevel.INFO, 'fisItUdiUdibrokerMockImpl.doInvoke() - ' +
			'\n request: ' + request +
			'\n response: ' + response +
			'\n endpoint: ' + endpoint +
			'\n soapAction: ' + soapAction +
			'\n requestName: ' + requestName +
			'\n responseNS: ' + responseNS +
			'\n responseName: ' + responseName +
			'\n responseType: ' + responseType);

		if(request instanceOf fisItUdiUdibroker.BrokeredToken_element) {
            fisItUdiUdibroker.BrokeredTokenResponse_element foo = new fisItUdiUdibroker.BrokeredTokenResponse_element () ;
            foo.BrokeredTokenResult = '<PassWordChange.UDIB.OUT TraceId="U01B3171329230404RV"><ResponseCode>00</ResponseCode><Message>Yay</Message><UserID >U00000</UserID><DaysLeft>99</DaysLeft></PassWordChange.UDIB.OUT>' ;
			response.put( 'response_x', foo ) ;
		}
		else if(request instanceOf fisItUdiUdibroker.DirectToPlatform_element) {
			response.put( 'response_x', new fisItUdiUdibroker.DirectToPlatformResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.HealthCheck_element) {
			response.put( 'response_x', new fisItUdiUdibroker.HealthCheckResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.Login_element) {
			response.put( 'response_x', new fisItUdiUdibroker.LoginResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.LoginMulti_element) {
            fisItUdiUdibroker.LoginMultiResponse_element foo = new fisItUdiUdibroker.LoginMultiResponse_element () ;
            foo.LoginMultiResult = '<UniversalLogon.UDIB.OUT ExternalTraceID="ab0931de-e118-4847-82c7-e61e241d6b99" TraceId="P01B0481550210227ZZ"><Response>Connected</Response><SecurityToken>[S:249gh249xughg8gh2348]</SecurityToken><Status><System Name="S"><Code>00</Code><Message /><DaysLeft>254</DaysLeft><MaskingIndicator>Y</MaskingIndicator></System></Status></UniversalLogon.UDIB.OUT>' ;
			response.put( 'response_x', foo );
		}
		else if(request instanceOf fisItUdiUdibroker.Logout_element) {
            fisItUdiUdibroker.LogoutResponse_element foo = new fisItUdiUdibroker.LogoutResponse_element () ;
            foo.LogoutResult = '<LogOff.UDIB.OUT TraceId="123456789"><Response>Disconnected</Response><Status><System Name="S"><Code>00</Code><Message></Message></System></Status></LogOff.UDIB.OUT>' ;
			response.put( 'response_x', foo ) ;
		}
		else if(request instanceOf fisItUdiUdibroker.LookupInfoByAccountNumber_element) {
			response.put( 'response_x', new fisItUdiUdibroker.LookupInfoByAccountNumberResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.NameSearch_element) {
			response.put( 'response_x', new fisItUdiUdibroker.NameSearchResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.ProcessByAcct_element) {
			response.put( 'response_x', new fisItUdiUdibroker.ProcessByAcctResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.ProcessByBin_element) {
            fisItUdiUdibroker.ProcessByBinResponse_element foo = new fisItUdiUdibroker.ProcessByBinResponse_element () ;
            foo.ProcessByBinResult = '<NewAccountAddSpecial.UDIB.OUT><ResponseCode>00</ResponseCode><Message></Message><AccountNumber>4111000000001234</AccountNumber></NewAccountAddSpecial.UDIB.OUT>' ;
			response.put( 'response_x', foo ) ;
		}
		else if(request instanceOf fisItUdiUdibroker.ProcessByCorp_element) {
			response.put( 'response_x', new fisItUdiUdibroker.ProcessByCorpResponse_element());
		}
		else if(request instanceOf fisItUdiUdibroker.Search_x_element) {
			response.put( 'response_x', new fisItUdiUdibroker.SearchResponse_element());
		}
	}
}