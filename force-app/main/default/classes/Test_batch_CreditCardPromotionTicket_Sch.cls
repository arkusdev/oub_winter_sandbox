@isTest
public with sharing class Test_batch_CreditCardPromotionTicket_Sch 
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	static testmethod void something ()
	{
		Test.startTest();

		batch_CreditCardPromotionTicket_Schedule myClass = new batch_CreditCardPromotionTicket_Schedule ();   
		String chron = '0 0 23 * * ?';        
		system.schedule('Test Sched', chron, myClass);
		
		Test.stopTest();
	}
}