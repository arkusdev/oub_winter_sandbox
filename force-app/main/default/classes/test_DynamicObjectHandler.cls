@isTest
public with sharing class test_DynamicObjectHandler 
{
	static testmethod void testUpdatableFields ()
	{
		Contact c = new Contact () ;
		
		List<String> csL = DynamicObjectHandler.getUpdateableFields ( c ) ;
		
		System.assertNotEquals ( csL, null ) ;
	}
	
	static testmethod void testUpdatableFieldQuery ()
	{
		Contact c = new Contact () ;
		
		String w = 'FOO' ;
		String o = 'BAR' ;
		String thingy = DynamicObjectHandler.getUpdateableFieldQuery ( c, w, o ) ;
		
		System.assertNotEquals ( thingy, null ) ;
	}
	
	static testmethod void testUpdatableFieldQueryPlusOther ()
	{
		Contact c = new Contact () ;
		
		String w = 'FOO' ;
		String o = 'BAR' ;
		
		list<String> thingyL = new list<String> () ;
		thingyL.add ( 'Computed_Phone__c' ) ;
		
		String thingy = DynamicObjectHandler.getUpdateableFieldQuery ( c, w, o, thingyL ) ;
		
		System.assertNotEquals ( thingy, null ) ;
	}
    
	static testmethod void testFields ()
	{
		Contact c = new Contact () ;
		
		List<String> csL = DynamicObjectHandler.getFields ( c ) ;
		
		System.assertNotEquals ( csL, null ) ;
	}
	
	
	static testmethod void testFieldQuery ()
	{
		Contact c = new Contact () ;
		
		String w = 'FOO' ;
		String o = 'BAR' ;
		String thingy = DynamicObjectHandler.getFieldQuery ( c, w, o ) ;
		
		System.assertNotEquals ( thingy, null ) ;
	}
}