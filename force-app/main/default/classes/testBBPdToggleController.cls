@isTest
public class testBBPdToggleController {

    @isTest static void testGetPaydayResponses() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        insert fa;
        
        List<Payday_Response__c> pdrs  = new List<Payday_Response__c>();
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        insert pdrs;
        
        List<Payday_Response__c> pdrList = new List<Payday_Response__c>();
        
        test.startTest();
        
   		pdrList = BankBlackPaydayToggleController.getPaydayResponses( fa.Id, rti );
        
        System.assertEquals( pdrList.size(), 3 );
        
        test.stopTest();
    }
    
    @isTest static void testCreatePaydayResponse() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        insert fa;
        
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        
        test.startTest();
        
        BankBlackPaydayToggleController.createPaydayResponse(fa.Id, true,'Email only', fa.ACCTNBR__c);
        
        Payday_Response__c pdr = [SELECT Response_Date__c, Comms_Channels__c, Financial_Account__c, OptIn_Status__c, Contact__c, Account_Number_OB__c, ACCTNBR__c
                                 FROM Payday_Response__c
                                 WHERE Financial_Account__c = :fa.Id AND RecordTypeId = :rti
                                 LIMIT 1];
        
        System.assertEquals( pdr.Response_Date__c.day(), Date.today().day() );
        System.assertEquals( pdr.OptIn_Status__c, 'Pending' );
        System.assertEquals( pdr.Comms_Channels__c, 'Email only' );
        System.assertEquals( pdr.Financial_Account__c, fa.Id );
        System.assertEquals( pdr.Account_Number_OB__c, fa.ACCTNBR__c );
        System.assertEquals( pdr.ACCTNBR__c, fa.ACCTNBR__c );

        rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        
        pdr = [SELECT Response_Date__c, Comms_Channels__c, Financial_Account__c, OptIn_Status__c, Contact__c
                                 FROM Payday_Response__c
                                 WHERE Financial_Account__c = :fa.Id AND RecordTypeId = :rti
                                 LIMIT 1];
        
        System.assertEquals( pdr.Response_Date__c.day(), Date.today().day() );
        System.assertEquals( pdr.Comms_Channels__c, 'Email only' );
        System.assertEquals( pdr.Financial_Account__c, fa.Id );
        
        test.stopTest();
    }
    
    @isTest static void testCreatePaydayCommsResponse() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        insert fa;
        
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        
        test.startTest();
        
        BankBlackPaydayToggleController.createPaydayCommsResponse(fa.Id, 'Email only', fa.acctnbr__c);
        
        Payday_Response__c pdr = [SELECT Response_Date__c, Financial_Account__c, OptIn_Status__c, Contact__c, Comms_Channels__c, acctnbr__c
                                 FROM Payday_Response__c
                                 WHERE Financial_Account__c = :fa.Id AND RecordTypeId = :rti
                                 LIMIT 1];
        
        System.assertEquals( pdr.Response_Date__c.day(), Date.today().day() );
        System.assertEquals( pdr.OptIn_Status__c, 'Pending' );
        System.assertEquals( pdr.Financial_Account__c, fa.Id );
        System.assertEquals( pdr.Comms_Channels__c, 'Email only' );
        System.assertEquals( pdr.acctnbr__c, fa.acctnbr__c );

        test.stopTest();
    }
    
    @isTest static void testProcessToggleEven() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        insert fa;
        
        List<Payday_Response__c> pdrs  = new List<Payday_Response__c>();
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Inactive';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Pending';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        insert pdrs;
        
        test.startTest();
        
        BankBlackPaydayToggleController.processToggleEven(fa.Id); // there are 3
        
        System.assertEquals( 1, [SELECT Id FROM Payday_Response__c WHERE RecordTypeId=:rti AND Financial_Account__c = :fa.Id AND Processing_Status__c = 'New'].size() );
        
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Inactive';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        insert pdr;
        
        BankBlackPaydayToggleController.processToggleEven(fa.Id);
        
        System.assertEquals( 3, [SELECT Id FROM Payday_Response__c WHERE RecordTypeId=:rti AND Financial_Account__c = :fa.Id AND Processing_Status__c = 'Canceled'].size() );
        
        test.stopTest();
    }
    
    @isTest static void testProcessComms() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        insert fa;
        
        List<Payday_Response__c> pdrs  = new List<Payday_Response__c>();
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        pdrs.add(pdr);
        insert pdr;
        System.debug('pdr.ID Insert :: '+ pdr.Id);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Inactive';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        pdrs.add(pdr);
        insert pdr;
        System.debug('pdr.ID Insert :: '+ pdr.Id);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        insert pdr;
        System.debug('pdr.ID Insert :: '+ pdr.Id);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        insert pdr;
        System.debug('pdr.ID Insert :: '+ pdr.Id);
        
        test.startTest();
        
        BankBlackPaydayToggleController.processComms(fa.Id, pdr.Id); // there are 3

        System.assertEquals( 2, [SELECT Id FROM Payday_Response__c WHERE RecordTypeId=:rti AND Financial_Account__c = :fa.Id AND Processing_Status__c = 'Canceled'].size() );
        
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Inactive';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        insert pdr;
        
        BankBlackPaydayToggleController.processComms(fa.Id, pdr.Id);
        
        System.assertEquals( 3, [SELECT Id FROM Payday_Response__c WHERE RecordTypeId=:rti AND Financial_Account__c = :fa.Id AND Processing_Status__c = 'Canceled'].size() );
        
        test.stopTest();
    }
    
    @isTest static void testGetCurrentUserContactId() {
        // Setup user
        Profile testProfile = [SELECT Id 
                           FROM profile
                           WHERE Name = 'OneUnited Advocate Identity User' 
                           LIMIT 1];
        
        Account a = new Account(Name='Test Account');
        insert a;
        
		Contact c = new Contact();
        c.Email = 'test1@ouuser.com';
        c.LastName = 'UserAdvocateProgramDisplayController';
        c.AccountId = a.Id;
        insert c;
        
    	User testUser = new User(
            FirstName = 'Test1',
            LastName = 'UserAdvocateProgramDisplayController',
            Email = 'test1@ouuser.com',
            Username = 'test1user1@ouuser.com',
            Alias = 'dsplctrl',
            CommunityNickname = 'tuser1',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = testProfile.id,
            LanguageLocaleKey = 'en_US',
            IsActive=true,
            ContactId=c.Id ); 

        test.startTest();
        
        String contactId = BankBlackPaydayToggleController.getCurrentUserContactId();
        System.assertEquals( contactId, null );
        
        System.runAs(testUser)
        {
            contactId = BankBlackPaydayToggleController.getCurrentUserContactId();
            System.assertEquals( contactId, c.Id );
        }
        
        test.stopTest();
    }
}