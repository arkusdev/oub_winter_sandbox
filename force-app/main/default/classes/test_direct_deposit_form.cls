@isTest
public with sharing class test_direct_deposit_form {

    /* --------------------------------------------------------------------------------
	 * Setup, Ticket
	 * -------------------------------------------------------------------------------- */
	private static Ticket__c getTicket ( )
	{
        Contact c = new Contact();
        c.LastName = 'last';
        insert c;
        
		Ticket__c t = new Ticket__c() ;
        
        t.RecordTypeId = Schema.SObjectType.Ticket__c.RecordTypeInfosByDeveloperName.get('Direct_Deposit_Form').RecordTypeId;
        t.First_Name__c = 'first';
        t.Last_Name__c = 'last';
        t.Contact__c = c.Id; // Thuy Test
        t.Account_Number__c = '1234567890123456';
        t.Account_Type__c = 'Primary Checking';
        t.Social_Security_Number__c = '111223333';
        t.Company_Name__c = 'OneUnited Bank';
        t.Employee_Id__c = 'employeeid#123';
        t.Direct_Deposit_Amount__c = '$2500';
		
		return t ;
	}
    
    /*
	 *  Tests blank session
	 */
	@isTest static void testDDFormPgBlankSession ()
	{
		//  Point to page to test
		PageReference pr1 = Page.direct_deposit_form ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create empty object
		Ticket__c t = new Ticket__c() ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		direct_deposit_form ddPg = new direct_deposit_form ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( ddPg.t, null ) ; // default
        System.assertEquals ( ApexPages.currentPage().getParameters().get('tid'), null );
		
		//  End of testing
		Test.stopTest () ;
	}
    
    /*
	 *  Tests for valid tid passed
	 */
    @isTest static void testDDFormPgPASS()
	{
		//  Point to page to test
		PageReference pr1 = Page.direct_deposit_form ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Ticket__c testT = getTicket();
        insert testT;
        
        Ticket__c tAfterInsert = [SELECT First_Name__c, Last_Name__c,
                                 Contact_Address__c, Contact_City__c, Contact_State__c, Contact_Zip_Code__c, Contact_Phone__c, 
                                 Account_Number__c, Account_Type__c, Social_Security_Number__c,
                                 Company_Name__c, Employee_Id__c,
                                 Direct_Deposit_Amount__c
                                 FROM Ticket__c
                                 WHERE Id = :testT.Id
                                 LIMIT 1];
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
        // manually pass in mismatched parameters
        ApexPages.currentPage().getParameters().put ( 'tid', testT.Id ) ;
        
		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( tAfterInsert ) ;
		
        direct_deposit_form ddPg = new direct_deposit_form ( sc ) ;
		
        // Verify that ticket is passed
        System.assertEquals ( ApexPages.currentPage().getParameters().get('tid'), testT.Id );
        System.assertEquals( ddPg.t.Id, testT.Id );
        
		//  End of testing
		Test.stopTest () ;
	}
}