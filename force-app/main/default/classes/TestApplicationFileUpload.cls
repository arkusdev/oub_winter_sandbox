@isTest
public with sharing class TestApplicationFileUpload 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
	
    public static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group () ;
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c ();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Referral_Tracking_Hold_Days__c = 7 ;
        s.Rakuten_Tracking_ID__c = 'TESTID' ;
        s.Rates_URL__c = 'https://www.test.com' ;
        
        insert s;
    }
    
	/*
	 * Creates a generic application I use for testing.
	 */
	private static one_application__c getApplicationForTests ()
	{
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		
		one_application__c app = new one_application__c ();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		app.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Unity Visa', 'one_application__c' ) ;
        
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'F003' ;
		
		app.Product__c = 'VSC' ;
		app.SubProduct__c = '000' ;
		app.Plan__c = '000' ;
        
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
		return app ;
	}

	/*
	 * Tests with an existing application passed in
	 */	
	static testmethod void testFileUploadCurrentSession ()
	{
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.application_file_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		one_application__c app = getApplicationForTests () ;
        insert app ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		//  Feed the standard controller to the page controller
		application_file_upload c = new application_file_upload ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 1000 ) ;
		System.assertEquals ( c.docTypeListSize, 5 ) ; // F003 requires 5 docs
		System.assertNotEquals ( c.appField, app.FIS_Application_ID__c ) ;

		//  Create custom settings - can't seem to pick up what already exists
		one_settings__c os = new one_settings__c () ;
		os.Name = 'settings' ;
		os.Case_Owner_ID__c = '00GP0000000PDns' ;
		insert os ;

		//  Trigger the upload, no attachments
		PageReference pr2 = c.upload () ;
		
		//  There should be errors in this case
		System.assertEquals ( c.flStepNumber, 1002 ) ; // Error Step!
		System.assertEquals ( c.errorName, true ) ;
		System.assertEquals ( c.errorFile, true ) ;
		System.assertEquals ( c.docTypeListSize, 5 ) ; // No document size change
		
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Utility Bill' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = app.id ;
		a1.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a1 ;
		
		//  Trigger the upload with attachment
		PageReference pr3 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // Success step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 4 ) ;  // One less document required!
		
		//  End of testing
		Test.stopTest () ;
	}
	
	/*
	 * Tests the page with the parameters passed in for an existing application
	 */
	static testmethod void testFileUploadNewSession ()
	{
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.application_file_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Create the application
		one_application__c app = getApplicationForTests () ;
        app.Additional_Information_Required__c = 'ID (SS Card/Recent W-2);Utility Bill;Phone Bill' ;
        app.FIS_Decision_Code__c = 'F003' ;
        insert app ;

		//  Load attachment
		Attachment a = new Attachment () ;
		a.Name = 'Phone Bill' ;
		Blob bodyBlob = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a.body = bodyBlob ;
		a.ParentId = app.id ;
		a.IsPrivate = false ;
		
		insert a ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', app.id );
        ApexPages.currentPage().getParameters().put ( 'kid', app.Upload_Attachment_Key__c );

		//  Empty application to pass in
		one_application__c eApp = new one_application__c () ;

		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eApp ) ;
		
		//  Feed the standard controller to the page controller
		application_file_upload c = new application_file_upload ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 1000 ) ;
		System.assertEquals ( c.docTypeListSize, 3 ) ;  // Remaining number of required documents
		System.assertNotEquals ( c.appField, app.FIS_Application_ID__c ) ;

		//  Create custom settings - can't seem to pick up what already exists
		one_settings__c os = new one_settings__c () ;
		os.Name = 'settings' ;
		os.Case_Owner_ID__c = '00GP0000000PDns' ;
		insert os ;

		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Utility Bill' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = app.id ;
		a1.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a1 ;
		
		//  Trigger the upload with attachment
		PageReference pr2 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // Success step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 2 ) ;  // One less document required!
		
		//  Create NEXT attachment
		Attachment a2 = new Attachment () ;
		a2.Name = 'ID (SS Card/Recent W-2)' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.ParentId = app.id ;
		a2.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a2 ;
		
		//  Trigger the upload with attachment
		PageReference pr4 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // Success step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 1 ) ;  // One less document required!
		
		//  Create NEXT attachment
		Attachment a3 = new Attachment () ;
		a3.Name = 'Phone Bill' ;
		Blob bodyBlob3 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a3.body = bodyBlob3 ;
		a3.ParentId = app.id ;
		a3.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a3 ;
		
		//  Trigger the upload with attachment
		PageReference pr5 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 2000 ) ;  // FINAL step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 0 ) ;  // All docs!
		
		//  End of testing
		Test.stopTest () ;
	}
	
	/*
	 *  Tests the invalid session code
	 */
	static testmethod void testFileUploadBlankSession ()
	{
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.application_file_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create empty object
		one_application__c app = new one_application__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		//  Feed the standard controller to the page controller
		application_file_upload c = new application_file_upload ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 0 ) ;
		System.assertEquals ( c.docTypeListSize, 0 ) ;

		//  End of testing
		Test.stopTest () ;
	}
}