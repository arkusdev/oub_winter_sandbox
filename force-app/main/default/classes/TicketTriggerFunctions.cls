public class TicketTriggerFunctions
{
	//  Set up STATIC variables for the ticket trigger
	public static Map<String,String> groupMap ;
    public static Map<String,User> activeUserMap ;
	public static List<RecordType> recordTypeList ;
	
    public static map<String,List<String>> uLMap ;
	public static map<String,Map<String, String>> esMapMap ;

    //  Load the groups into the static variable
	public static Map<String,String> getGroupMap ( List<String> iGroupList )
	{
		//  Only call DML if the static variable is empty
		if ( groupMap == null )
		{
			groupMap = OneUnitedUtilities.getGroupMap ( iGroupList ) ;
		}
		
		return groupMap ;
	}

	//  Load the record types into the static variable
	public static List<RecordType> getRecordTypeList ( List<String> iRecordTypeList )
	{
		//  Only call DML if the static variable is empty
		if ( recordTypeList == null )
		{
			recordTypeList = OneUnitedUtilities.getRecordTypeList ( iRecordTypeList ) ;	
		}
		
		return recordTypeList ;
	}
    
    // developername
    private static ID aertid ;
    public static ID getAutomatedEmailRecordTypeID ()
    {
        if ( aertid == null )
            aertid = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('App_Email').RecordTypeId ;
        
        return aertid ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Methods :/
	 *  -------------------------------------------------------------------------------------------------- */
    public static List<String> getUserIdsFromGroupId ( String z )
    {
        List<String> userIdList ;
        if ( uLmap.containsKey ( z ) )
        {
            System.debug ( '1 Found key in uLmap!' ) ;
            userIdList = uLMap.get ( z ) ;
        }
        else
        {
            userIdList = OneUnitedUtilities.getUserIdsFromGroupId ( z ) ;
            ulMap.put ( z, userIdList ) ;
        }
        
        return userIdList ;
	}
    
    public static Map<String, String> getCurrentSubscribers ( String nID )
    {
        Map<String, String> esM ;
        if ( esMapMap.containsKey ( nID ) )
        {
            System.debug ( '1 found ESM in map!' ) ;
            esM = esMapMap.get ( nID ) ;
        }
        else
        {
            esM = OneUnitedUtilities.getCurrentSubscribers ( nID ) ;
            if ( esM == null )
            {
                System.debug ( 'Empty came back, overriding' ) ;
                esM = new Map<String, String> () ;
            }
            
            esMapMap.put ( nID, esM ) ;
        }
        
        return esM ;
    }
}