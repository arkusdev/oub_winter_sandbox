public with sharing class Card_Stock_Order 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	//  Ticket object to upload
	public Ticket__c ticket 
	{
		get 
		{
			if ( ticket == null )
            {
				ticket = new Ticket__c () ;
                ticket.State__c = 'CA' ;
            }
            
			return ticket ;
		}
		
		set ;
	}
     
	//  Step control for panels on page
	public Integer dStepNumber
	{
		get ;
		private set ;
	}

	//  control for debit/atm panels on page
	public Integer dPanel
	{
		get ;
		private set ;
	}

	//  Date for dropdown since inputfield doesn't work with calendar
	public String dateOfBirth
	{
		get ;
		set ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Bleah, debit
     *  ---------------------------------------------------------------------------------------- */
    public list<SelectOption> debitList
    {
        get ;
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Bleah, ATM
     *  ---------------------------------------------------------------------------------------- */
    public list<SelectOption> atmList
    {
        get ;
        private set ;
    }
    
    public Integer rPanel
    {
        get ;
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Bleah, Bleah
     *  ---------------------------------------------------------------------------------------- */
    public String selectCard
    {
        get ;
        set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Internal objects
     *  ---------------------------------------------------------------------------------------- */
    private Web_Settings__c ws ;
    private String recordTypeID ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
	//  Error checking
	public Map<String,String> errorMap
	{
		get ;
		private set ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Final Display
     *  ---------------------------------------------------------------------------------------- */
    public String confirmationNumber
    {
        get ;
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public Card_Stock_Order ( ApexPages.Standardcontroller stdC )
	{
		//  Populate options
		debitList = new list<SelectOption> () ;
        debitList.add ( new SelectOption ( '', '--None--' ) ) ;
        debitList.add ( new SelectOption ( 'Amir', 'Amir' ) ) ;
        debitList.add ( new SelectOption ( 'Black Classic', 'Black Classic' ) ) ;
        debitList.add ( new SelectOption ( 'Classic', 'Classic' ) ) ;
        debitList.add ( new SelectOption ( 'Doonie', 'Doonie' ) ) ;
        debitList.add ( new SelectOption ( 'Justice', 'Justice' ) ) ;
        debitList.add ( new SelectOption ( 'Lady', 'Lady Liberty' ) ) ;
        debitList.add ( new SelectOption ( 'Mona', 'Mona' ) ) ;
        debitList.add ( new SelectOption ( 'King', 'King' ) ) ;
        debitList.add ( new SelectOption ( 'Queen', 'Queen' ) ) ;
        
        atmList = new list<SelectOption> () ;
        atmList.add ( new SelectOption ( '', '--None--' ) ) ;
        atmList.add ( new SelectOption ( 'Amir', 'Amir' ) ) ;
        atmList.add ( new SelectOption ( 'Black Classic', 'Black Classic' ) ) ;
        atmList.add ( new SelectOption ( 'Classic', 'Classic' ) ) ;
        atmList.add ( new SelectOption ( 'Doonie', 'Doonie' ) ) ;
        atmList.add ( new SelectOption ( 'Justice', 'Justice' ) ) ;
        atmList.add ( new SelectOption ( 'Lady', 'Lady Liberty' ) ) ;
        atmList.add ( new SelectOption ( 'Mona', 'Mona' ) ) ;
        atmList.add ( new SelectOption ( 'King', 'King' ) ) ;
        atmList.add ( new SelectOption ( 'Queen', 'Queen' ) ) ;
        
        //  Controls which panel(s) we display
		dStepNumber = 1000 ;
        dPanel = 0 ;
        confirmationNumber = null ;
        
        //  Load settings
        ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;
        
        //  Record Type for ticket
        recordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Card Maintenance Request', 'Ticket__c' ) ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Validate Inputs & Create Ticket
     *  ---------------------------------------------------------------------------------------- */
    public PageReference submitTicket ()
    {
		//  Check for validation, setup
		errorMap = new Map<String, String> () ;
		
		//  Validation
		if ( String.isBlank ( ticket.Street_Address__c ) )
			errorMap.put ( 'Street_Address__c' , 'Please enter your street' ) ;
		else
			errorMap.put ( 'Street_Address__c', '' ) ;
			
		if ( String.isBlank ( ticket.City__c ) )
			errorMap.put ( 'City__c' , 'Please enter your city' ) ;
		else
			errorMap.put ( 'City__c', '' ) ;
		
		if ( String.isBlank ( ticket.State__c ) )
			errorMap.put ( 'State__c' , 'Please enter your state' ) ;
		else
			errorMap.put ( 'State__c', '' ) ;
		
		if ( String.isBlank ( ticket.Zip_Code__c ) )
			errorMap.put ( 'Zip_Code__c' , 'Please enter your zip code' ) ;
		else if ( ! OneUnitedUtilities.isValidZipCode ( ticket.Zip_Code__c ) ) 
			errorMap.put ( 'Zip_Code__c' , 'Please enter a valid zip code' ) ;
		else
		{
			ticket.Zip_Code__c = OneUnitedUtilities.cleanZipCode ( ticket.Zip_Code__c ) ;
			errorMap.put ( 'Zip_Code__c', '' ) ;
		}
		
		if ( String.isBlank ( ticket.First_Name__c ) )
			errorMap.put ( 'First_Name__c' , 'Please enter your first name' ) ;
		else
			errorMap.put ( 'First_Name__c', '' ) ;
		
		if ( String.isBlank ( ticket.Last_Name__c ) )
			errorMap.put ( 'Last_Name__c' , 'Please enter your last name' ) ;
		else
			errorMap.put ( 'Last_Name__c', '' ) ;
				
		if ( String.isBlank ( ticket.Email_Address__c ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter your email address' ) ;
		else if ( ( String.isNotBlank ( ticket.Email_Address__c ) ) && ( ! OneUnitedUtilities.isValidEmail ( ticket.Email_Address__c ) ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter a valid email address' ) ;
		else
			errorMap.put ( 'Email_Address__c', '' ) ;
		
		if ( String.isBlank ( ticket.Card_Type__c ) )
			errorMap.put ( 'Card_Type__c' , 'Please choose the type of card' ) ;
		else
			errorMap.put ( 'Card_Type__c', '' ) ;
				
		if ( String.isBlank ( selectCard ) )
			errorMap.put ( 'Plastic_Type__c' , 'Please choose your plastic' ) ;
		else
        {
            ticket.Plastic_Type__c = selectCard ;
			errorMap.put ( 'Plastic_Type__c', '' ) ;
        }
				
		if ( ( String.isNotBlank ( ticket.Last_4_Card_Number__c ) ) && 
             ( ticket.Last_4_Card_Number__c.length () != 4 ) ) 
			errorMap.put ( 'Last_4_Card_Number__c' , 'Please enter the last four digits on your card.' ) ;
		else if ( ( String.isNotBlank ( ticket.Last_4_Card_Number__c ) ) && 
             ( ticket.Last_4_Card_Number__c.length () == 4 ) && 
             ( ! ticket.Last_4_Card_Number__c.isNumeric () ) ) 
			errorMap.put ( 'Last_4_Card_Number__c' , 'Please enter the last four digits on your card.' ) ;
		else if ( String.isBlank ( ticket.Last_4_Card_Number__c ) )
			errorMap.put ( 'Last_4_Card_Number__c' , 'Please enter the last four digits on your card.' ) ;
		else
			errorMap.put ( 'Last_4_Card_Number__c', '' ) ;
				
		//  Attempt to convert date from string... catch exception to prevent kablooey on page
		Date bDate = null ;
		try
		{
			if ( dateOfBirth != null )
				bDate = Date.parse ( dateOfBirth ) ;
		}
		catch ( TypeException te )
		{
			bDate = null ;	
		}
		
		if ( bDate == null )
			errorMap.put ( 'Date_Of_Birth__c' , 'Please enter your date of birth' ) ;
		else
		{
			ticket.Date_Of_Birth__c = bDate ;
			errorMap.put ( 'Date_Of_Birth__c', '' ) ;
		}

		//  Validation start
		boolean isValid = true ;
		
		//  Non blank entry means an error occurred
		for ( String key : errorMap.keySet () )
		{
			if ( String.isNotEmpty ( errorMap.get ( key ) ) )
                isValid = false ;
		}

		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        dStepNumber = 1001 ;
            
			return null ;
		}
		else
		{
            try 
            {
                System.debug ( '========== Checking for created ticket ==========' ) ;
                
                if ( ticket.ID == null )
                {
                    System.debug ( '========== Setting ticket defaults ==========' ) ;
            
                    ticket.RecordTypeID = recordTypeID ;
					ticket.Status__c = 'Received' ;
                    ticket.Reason_Action_Requested__c = 'Customer Requested' ;
                    ticket.Card_Number__c = 'XXXXXXXXXXXX' + ticket.Last_4_Card_Number__c ;
                    ticket.Maintenance_Action__c = 'Re-Issue Card' ;
                    ticket.Website_Request__c = true ;
                    ticket.Card_Maintenance_Type__c = 'Consumer Debit - Maintenance' ;
                    
                    System.debug ( '========== Checking for a contact ==========' ) ;
                    Contact c = getContact ( ticket.First_Name__c, ticket.Last_Name__c, ticket.Email_Address__c ) ;
                    
                    if ( c != null )
                    {
                        System.debug ( 'Found Contact :: "' + c.ID + '", Setting.' ) ;
                        ticket.Contact__c = c.ID ;
                    }
                    else
                    {
                        
                        if ( ( ws != null ) && ( ws.Card_Stock_Order_Default_Contact_ID__c != null ) )
                        {
	                        System.debug ( 'Setting default Contact' ) ;
                            ticket.Contact__c = ws.Card_Stock_Order_Default_Contact_ID__c ;
                        }
                        else
                        {
                            System.debug ( 'No default contact found?!?' ) ;
                        }
                    }
 			
                    System.debug ( '========== Inserting Ticket ==========' ) ;
		        	insert ticket ;
                    
                    //  Set panel to success upload
                    dStepNumber = 2000 ;
            
                    if ( ( ticket != null ) && ( ticket.ID != null ) )
                    {
                        System.debug ( '========== Reload! ==========' ) ;
                        list<Ticket__c> nTL = null ;
                        Ticket__c nT = null ;
                        
                        try
                        {
                            nTL = [
                                SELECT ID, Name
                                FROM Ticket__c 
                                WHERE ID = :ticket.ID 
                            ] ;
                        }
                        catch ( DMLException e )
                        {
                            for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
                                System.debug ( e.getDmlMessage ( i ) ) ; 
                        }
                        
                        if ( ( nTL != null ) && ( nTL.size () > 0 ))
                        {
                            nT = nTL [ 0 ] ;
                            confirmationNumber = nT.Name ;
                        }
                    }
                }
                else
                {
                    System.debug ( '========== Found Ticket ID ' + ticket.ID + ' ==========' ) ;
                }
            }
            catch ( DMLException e )
            {
                System.debug ( '========== Error Inserting Ticket! ==========' ) ;
                dStepNumber = 1200 ;
                
                for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
                    System.debug ( e.getDmlMessage ( i ) ) ; 
    
                return null;
            }
            finally 
            {
                System.debug ( '========== Final stuff ==========' ) ;
                
                //  Clear errors if any
				errorMap = new Map<String, String> () ;
             
                //  Reset attachment
                ticket = new Ticket__c () ; 
            }
        } 
        
    	return null ;
    }
    
    private Contact getContact ( String fN, String lN, String eM )
    {
        Contact c = null ;
        list<Contact> cL = null ;
        
        try
        {
            cL = [
                SELECT ID
                FROM Contact
                WHERE FirstName = :fN
                AND LastName = :lN
                AND Email = :eM
                AND RecordType.Name = 'Customer'
				ORDER BY ID
            ] ;
        }
        catch ( DMLException e )
        {
            for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
                System.debug ( e.getDmlMessage ( i ) ) ; 
            
            c = null ;
        }
        
        if ( ( cL != null ) && ( cL.size () > 0 ) )
        {
            c = cL [ 0 ] ;
        }
        
        return c ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Flippy flip flip
     *  ---------------------------------------------------------------------------------------- */
    public void flipThing ()
	{
        System.debug ( 'CT :: ' + ticket.Card_Type__c ) ;
        
        if ( String.isBlank ( ticket.Card_Type__c ) )
            dPanel = 0 ;
        else if ( ticket.Card_Type__c.equalsIgnoreCase ( 'Debit' ) )
            dPanel = 1000 ;
        else if ( ticket.Card_Type__c.equalsIgnoreCase ( 'ATM' ) )
            dPanel = 2000 ;
    }
}