public with sharing class one_application_send{
    
    private one_application__c app;

    public one_application_send(ApexPages.StandardController controller){
        this.app = (one_application__c)controller.getRecord();
        this.app = one_utils.getApplicationFromId(app.Id);
    }
    
    public pageReference sendData(){
        one_utils.sendApplicationForm(app, (app.FIS_Application_Source_Code__c != null && app.FIS_Application_Source_Code__c != '') ? app.FIS_Application_Source_Code__c : null);
        pageReference pr = new pageReference('/' + app.Id);
        pr.setRedirect(true);
        return pr;
    }

}