public class ApplicationLeadConvertQueueable implements Queueable 
{
    private LeadStatus convertStatus ;
    private list<ID> cL ;
    private list<ID> lL ;
    
    public ApplicationLeadConvertQueueable ( ID x, ID y )
    {
        cL = new list<ID> () ;
        cL.add ( x ) ;
        
        lL = new list<ID> () ;
        lL.add ( y ) ;
    }
    
    public ApplicationLeadConvertQueueable ( list<ID> xL, list<ID> yL )
    {
        cL = xL ;
        lL = yL ;
    }

    public void execute(QueueableContext context) 
    {
        try
        {
			convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        }
        catch (Exception e )
        {
            convertStatus = null ;
        }
        
        if ( ( cL != null ) && ( cL.size () > 0 ) )
        {
            for ( integer i = 0 ; i < cL.size () ; i++ )
            {
                ID c = cL.get ( i ) ;
                ID l = lL.get ( i ) ;
                
                Database.LeadConvert lc = new database.LeadConvert () ;
                lc.setLeadId ( l ) ;
                lc.setConvertedStatus ( convertStatus.MasterLabel ) ;
                lc.setContactId ( c ) ;
                lc.setAccountId ( one_settings__c.getInstance('settings').Account_ID__c ) ;
                
                try
                {
                	Database.LeadConvertResult lcr = Database.convertLead ( lc ) ;
                }
                catch ( Exception e )
                {
                    // ?
                }
            }
        }
    }
}