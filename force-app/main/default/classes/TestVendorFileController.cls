@isTest
public with sharing class TestVendorFileController 
{
	private static User getUser ()
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test1' ;
		u.lastName = 'User1' ;
		u.email = 'test1@user.com' ;
		u.username = 'test1user1@user.com' ;
		u.alias = 'tuser1' ;
		u.CommunityNickname = 'tuser1' ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		insert u ;
		
		return u ;
	}
	
	private static PermissionSet getPermissionSet ( String name )
	{
		PermissionSet ps = [
	        select Id
	        from PermissionSet
	        where Name = :name
	    ];
	    
		return ps ;
	}
	
	private static PermissionSetAssignment setPermissionSetAssignment ( PermissionSet ps, User u )
	{
		PermissionSetAssignment psa = new PermissionSetAssignment () ;
		psa.AssigneeId = u.Id ;
		psa.PermissionSetId = ps.Id ;
		
		insert psa ;
		
		return psa ;
	}
	
	private static User getUserWithPermissionSet ( String name )
	{
		User u = getUser () ;
		PermissionSet ps = getPermissionSet ( name ) ;
		PermissionSetAssignment psa = setPermissionSetAssignment ( ps, u ) ;
		
		return u ;
	}
	
	private static User getUserWithPermissionSetNEW ( String name )
	{
		User u = getUser () ;
		
		PermissionSet ps = new PermissionSet () ;
		ps.Name = name ;
		ps.Label = 'Test' ;
		insert ps ;
		
		PermissionSetAssignment psa = setPermissionSetAssignment ( ps, u ) ;
		
		return u ;
	}
	
	static testmethod void testOne_BLANK ()
	{
		//  --------------------------------------------------
		//  User & permissions
		//  --------------------------------------------------
		User u1 = getUserWithPermissionSetNEW ( 'VM_Test_BLANK' ) ;
		
		//  --------------------------------------------------
		//  Object Setup
		//  --------------------------------------------------
		Account a = new Account () ;
		a.Name = 'Test Account' ;
		
		insert a ;
		
		Vendor_Management__c vm = new Vendor_Management__c () ;
		vm.Relationship__c = a.Id ;
		insert vm ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.GLBA_Vendor_Files ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		System.runAs ( u1 )
		{
			
			//  Create standard controller object from the application
			ApexPages.StandardController sc = new ApexPages.StandardController ( vm ) ;
	        
			//  Feed the standard controller to the page controller
			VendorFileController vfc = new VendorFileController ( sc ) ;
			
			System.assertEquals ( vfc.mode, 500 ) ;
		}
				
		Test.stopTest() ;
	}
	
	static testmethod void testTwo_ADMIN ()
	{
		//  --------------------------------------------------
		//  User & permissions
		//  --------------------------------------------------
		User u1 = getUserWithPermissionSetNEW ( 'VM_TEST_ADMIN' ) ;
		
		//  --------------------------------------------------
		//  Object Setup
		//  --------------------------------------------------
		Account a = new Account () ;
		a.Name = 'Test Account' ;
		
		insert a ;
		
		Vendor_Management__c vm = new Vendor_Management__c () ;
		vm.Relationship__c = a.Id ;
		vm.GLBA_Vendor_ID__c = 123 ;
		insert vm ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.GLBA_Vendor_Files ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		System.runAs ( u1 )
		{
			
			//  Create standard controller object from the application
			ApexPages.StandardController sc = new ApexPages.StandardController ( vm ) ;
	        
			//  Feed the standard controller to the page controller
			VendorFileController vfc = new VendorFileController ( sc ) ;
			
			System.assertEquals ( vfc.mode, 0 ) ;
			System.assertEquals ( vfc.isAdministrator, 'Y' ) ;
		}
		
		Test.stopTest() ;
	}
	
	static testmethod void testThree_NORMAL ()
	{
		//  --------------------------------------------------
		//  User & permissions
		//  --------------------------------------------------
		User u1 = getUserWithPermissionSetNEW ( 'VM_TEST_NORMAL' ) ;
		
		//  --------------------------------------------------
		//  Object Setup
		//  --------------------------------------------------
		Account a = new Account () ;
		a.Name = 'Test Account' ;
		
		insert a ;
		
		Vendor_Management__c vm = new Vendor_Management__c () ;
		vm.Relationship__c = a.Id ;
		vm.GLBA_Vendor_ID__c = 123 ;
		insert vm ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.GLBA_Vendor_Files ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		System.runAs ( u1 )
		{
			
			//  Create standard controller object from the application
			ApexPages.StandardController sc = new ApexPages.StandardController ( vm ) ;
	        
			//  Feed the standard controller to the page controller
			VendorFileController vfc = new VendorFileController ( sc ) ;
			
			System.assertEquals ( vfc.mode, 0 ) ;
			System.assertEquals ( vfc.isAdministrator, 'N' ) ;
		}
		
		Test.stopTest() ;
	}
}