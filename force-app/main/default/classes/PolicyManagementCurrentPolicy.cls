public with sharing class PolicyManagementCurrentPolicy 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page Objects
     *  ---------------------------------------------------------------------------------------- */
    public Attachment displayBDA
    {
    	public get ;
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public PolicyManagementCurrentPolicy ( ApexPages.Standardcontroller stdC )
    {
    	displayBDA = null ;
    	
		//  Get the object from the page
		Policy_Management__c pm = (Policy_Management__c) stdC.getRecord () ;

		//  Attachments!
		list<Attachment> aL = [
			SELECT ID, Name, Description, ContentType, CreatedDate, LastModifiedDate
			FROM Attachment
			WHERE ParentID = :pm.ID
			ORDER BY LastModifiedDate DESC
		] ;
		
		if ( ( aL != null ) && ( aL.size () > 0 ) )
		{
			for ( Attachment a : aL )
			{
				System.debug ( 'Checking -- ' + a.Name + ' --> ' + a.ContentType ) ;
				if ( ( a.Name.contains ( 'BDA' ) ) && ( a.ContentType == 'application/pdf' ) )
				{
					System.debug ( 'FOUND!!' ) ;
					displayBDA = a ;
					break ;
				}
			}
		}
	}
	
	public PageReference loadFile ()
	{
		PageReference goAttachment = null ;
		
		if ( displayBDA != null )
		{
		    goAttachment = new PageReference ( '/servlet/servlet.FileDownload?file=' + displayBDA.id ) ;
			goAttachment.setRedirect ( true ) ;
		}
	
		return goAttachment ;
	}
}