@isTest
public class TestApplicationLOSOverride {

	public static one_application__c genericTestApp() {
		// generic application - single applicant
		one_application__c app = new one_application__c();
        app.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Unity Visa', 'one_application__c' );
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = 'First' ;
		app.Last_Name__c = 'Last' ;
		app.Email_Address__c = 'gentestapp@gentestapp.com' ;
        app.DOB__c = System.today().addYears(-28);
        app.SSN__c = '001-01-0001' ;
        app.Phone__c = one_utils.formatPhone ( '1112223333' ) ;
        
        app.Street_Address__c = '111 Test St.' ;
        app.City__c = 'Test City' ;
        app.State__c = 'CO' ;
        app.ZIP_Code__c = '54321' ;
        app.Mailing_Address_Same__c = 'Yes' ;

        app.Gross_Income_Monthly__c = 3000.00;
        app.Over_18__c = true;

		return app;
	}

	@testSetup
	public static void setup(){
		// my unique user name and used for email also
		String myUserName = 'testuser' + DateTime.now().getTime() + '@testme000.com';

		// system administrator profile
		Id profileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;

		// create system admin user
		User testuser =  new User(FirstName = 'Test', LastName = 'User', Alias = 'tstuser', UserName = myUserName, Email = myUserName,
							EmailEncodingKey = 'UTF-8', ProfileId = profileId, LanguageLocaleKey='en_US',
							LocaleSidKey='en_US',TimeZoneSidKey='America/Denver');
		insert testuser;

		// create additional user here if needed
	}

	@isTest
	public static void myTest1(){

		User testuser = [SELECT Id FROM User WHERE Alias='tstuser' LIMIT 1];

		System.runAs(testuser){
			List<String> reasonList = new List<String>();
			one_application__c myApp;

			one_application__c app = genericTestApp();
            app.Credit_Decline_Reasons__c = 'Bankruptcy on credit file	';
			insert app;

			Test.startTest();

			reasonList = ApplicationLOSOverride.getDeclineReasons();
			myApp = ApplicationLOSOverride.getApplication(app.Id);

			Test.stopTest();

			System.assertEquals(myApp.Id, app.Id);
		}
	}

	@isTest
	public static void myTest2(){

		User testuser = [SELECT Id FROM User WHERE Alias='tstuser' LIMIT 1];

		System.runAs(testuser){
			Boolean result = false;

			one_application__c app = genericTestApp();
			app.FIS_Decision_Code__c = null;
            app.Pre_Bureau_Disposition__c = 'Warn';
            app.Pre_Bureau_Override_Datetime__c = null;
            app.Pre_Bureau_Override_User__c = null;

            insert app;

			Test.startTest();
			result = ApplicationLOSOverride.isPreBureauOverride(app);
			Test.stopTest();

			System.assert(result, 'The result was not true!!!');
		}
	}

	@isTest
	public static void myTest3(){

		User testuser = [SELECT Id FROM User WHERE Alias='tstuser' LIMIT 1];

		System.runAs(testuser){

			one_application__c app = genericTestApp();
			insert app;

			Test.startTest();

			LOSHelper testLOS = new LOSHelper();
			testLOS.setApplication(app);

			ApplicationLOSOverride.overrideApplicationPreBureau(app, 'override pre reason');
			ApplicationLOSOverride.declineApplicationPreBureau(app, 'decline pre reason');

			Test.stopTest();

		}
	}

	@isTest
	public static void myTest4(){

		User testuser = [SELECT Id FROM User WHERE Alias='tstuser' LIMIT 1];

		System.runAs(testuser){
			Decimal result = 0.0;

			one_application__c app = genericTestApp();
			app.FIS_Decision_Code__c = null;
            app.Post_Bureau_Disposition__c = 'Warn';
            app.Post_Bureau_Override_Datetime__c = null;
            app.Post_Bureau_Override_User__c = null;

            insert app;

			Test.startTest();
			result = ApplicationLOSOverride.isPostBureauOverride(app);
			Test.stopTest();

			System.assert ( result!=-1, 'The result was not true!!!' ) ;
		}
	}

	@isTest
	public static void myTest5(){

		User testuser = [SELECT Id FROM User WHERE Alias='tstuser' LIMIT 1];

		System.runAs(testuser){
			Decimal creditLimit = 10000.00;

			one_application__c app = genericTestApp();
			insert app;

			Test.startTest();

			LOSHelper testLOS = new LOSHelper();
			testLOS.setApplication(app);

			ApplicationLOSOverride.overrideApplicationPostBureau(app, 'override post reason', creditLimit);
			ApplicationLOSOverride.declineApplicationPostBureau(app, 'decline post reason');

			Test.stopTest();

		}
	}

}