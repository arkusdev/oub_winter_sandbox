@isTest
public class Test_Deposit_Account_Online_Banking 
{
    /* --------------------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------------------- */
    private static one_settings__c insertCustomSetting ()
    {	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = '3297thuipwgyb8gfh314putgh24' ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = false ;

        s.Trial_Deposit_Validation_Limit__c = 3 ;
        s.ACH_Validation_Threshold__c = 1000.00 ;
        s.Deposit_ACH_Validation_Threshold__c = 1000.00 ;
        
        //  FIS STUFF
        s.FIS_Aquirer_ID__c = '059187' ;
        s.FIS_Authentication_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/authen' ;
        s.FIS_IDA_Config_Key__c = 'idaaliaskey' ;
        s.FIS_IDA_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/ida' ;
        s.FIS_Location_ID__c = 'UNITY Visa Application' ;
        s.FIS_Staging_Flag__c = true ;
        s.FIS_User_ID__c = '02280828' ;
        s.FIS_User_Password__c = 'Z26ZjcZdZAiZXaZ' ;
        
        // Stuff I care about for this
        s.Andera_Deposit_Application_URL__c = 'https://www.cnn.com' ;
        s.Andera_Deposit_Application_FIID__c = 'ABC123' ;
        s.Online_Banking_Login_URL__c = 'https://www.nytimes.com' ;
        s.Percentage_Deposit_Application_Redirect__c = 50 ;
        s.Current_Customer_Redirect__c = true ;
        
        insert s ;
        
        return s ;
    }

	public static testmethod void testOne ()
    {
        one_settings__c settings = insertCustomSetting () ;
        
        PageReference pr = new PageReference ( 'https://www.oneunited.com' ) ;
        pr.getParameters().put ( 'x', '123' ) ;
        pr.getParameters().put ( 'y', '456' ) ;
        
		Test.setCurrentPage ( pr ) ;
		        
        Test.startTest () ;
        
        Deposit_Account_Online_Banking daob = new Deposit_Account_Online_Banking () ;
        
        Test.stopTest () ;
    }
}