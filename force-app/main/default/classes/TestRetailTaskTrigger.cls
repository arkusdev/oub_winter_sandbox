@isTest
public with sharing class TestRetailTaskTrigger 
{
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		
		insert b ;
		
		return b ;
	}
	
	private static Contact getContactEmail ()
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'testuser@test.com' ;
		
		insert c ;
		
		return c ;
	}
	
	private static Contact getContactNoEmail ()
	{
		Contact c = new Contact () ;
		
		c.firstName = 'Test2' ;
		c.LastName = 'User2' ;
		
		insert c ;
		
		return c ;
	}
	
	private static RecordType getRetailTask ()
	{
		RecordType rt = [
			SELECT ID FROM RecordType WHERE Name = 'Retail Task'
		] ;

		return rt ;		
	}
	
	static testmethod void testTaskTriggerFinancialAccount ()
	{
		//  Get the contact
		Contact c = getContactEmail () ;
		
		//  Get the branch
		Branch__c b = getBranch () ;

		//  Financial Account record type
		String dRT = OneUnitedUtilities.getRecordTypeId ( 'Deposit' ) ;
		
		//  Set up the financial account
		Financial_Account__c f = new Financial_Account__c () ;
		f.TAXRPTFORPERSNBR__c = c.id ;
		f.ACCTNBR__c = '123456798' ;
		f.MJACCTTYPCD__c = 'SAV' ;
		f.CURRMIACCTTYPCD__c = 'UESP' ;
		f.BRANCHORGNBR__c = b.ID ;
		f.CURRMIACCTTYPCD__c = 'QB01' ;
		f.RecordTypeId = dRT ;
		
		
		insert f ;
		
		RecordType rt = getRetailTask () ;
		
		//  Insert the first task
		Task t1 = new Task () ;
		
		t1.RecordTypeId = rt.Id ;
		t1.Send_Customer_Communication__c = true ;
		t1.Type = 'Call' ;
		t1.Interested_in_New_Account__c = 'Other Investments' ;
		t1.WhatId = f.id ;
		
		insert t1 ;
		
		//  Insert the second task
		Task t2 = new Task () ;
		
		t2.RecordTypeId = rt.Id ;
		t2.Send_Customer_Communication__c = true ;
		t2.Type = 'Call' ;
		t2.Interested_in_New_Account__c = 'Left Message' ;
		t2.WhatId = f.id ;
		
		insert t2 ;
	}
	
	static testmethod void testTaskTriggerOpportunity ()
	{
		//  Get the contact
		Contact c = getContactEmail () ;
		
		//  Set up the opportunity
		RecordType rtO = [
			SELECT ID FROM RecordType WHERE Name = 'Deposit Opportunity'
		] ;
		
		oneOpportunity__c o = new oneOpportunity__c () ;
		
		o.Home_Phone__c = '123-456-7890' ;
		o.Interested_in_Product__c = 'Savings' ;
		o.Opportunity_Contact__c = c.id ;
		o.Opportunity_Source__c = 'Branch' ;
		o.Potential__c = 100.00 ;
		o.RecordTypeId = rtO.Id ;
		o.Type__c = 'New Customer' ;
		
		insert o ;
		
		RecordType rt = getRetailTask () ;
		
		//  Insert the first task
		Task t1 = new Task () ;
		
		t1.RecordTypeId = rt.Id ;
		t1.Send_Customer_Communication__c = true ;
		t1.Type = 'Call' ;
		t1.Interested_in_New_Account__c = 'Not Enough Liquidity' ;
		t1.WhatId = o.id ;
		
		insert t1 ;
		
		//  Insert the second task
		Task t2 = new Task () ;
		
		t2.RecordTypeId = rt.Id ;
		t2.Send_Customer_Communication__c = true ;
		t2.Type = 'Call' ;
		t2.Interested_in_New_Account__c = 'Left Message' ;
		t2.WhatId = o.id ;
		
		insert t2 ;
	}
	
	static testmethod void testTaskTriggerWorkshopAttendee ()
	{
        //  Setup
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.Workshop_Contact_Record_Type__c = 'Prospect' ;
        insert ws ;
        
		// Workshops are at a branch
		Branch__c b = new Branch__c () ;
		b.Name = 'Test Branch' ;
		b.Address_A__c = '123 Test Street' ;
		b.City__c = 'Test City' ;
		b.State_CD__c = 'TS' ;
		b.Zip__c = '10001' ;
		
		insert b ;
		
		//  Create the workshop referring to the branch
		Workshop__c w = new Workshop__c () ;
		
		w.Name = 'Test Workshop' ;
		w.Branch__c = b.id ; 
		
		insert w ;

		Contact c = getContactEmail () ;
		
		//  Workshop attendee at the workshop we created
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		wa.Workshop_ID__c = w.id ;
		wa.Contact__c = c.id ;
		
		insert wa ;
		
		RecordType rt = getRetailTask () ;
		
		//  Insert the first task
		Task t1 = new Task () ;
		
		t1.RecordTypeId = rt.Id ;
		t1.Send_Customer_Communication__c = true ;
		t1.Type = 'Call' ;
		t1.Interested_in_New_Account__c = 'Only Invest Through Quickrate' ;
		t1.WhatId = wa.id ;
		
		insert t1 ;
		
		//  Insert the second task
		Task t2 = new Task () ;
		
		t2.RecordTypeId = rt.Id ;
		t2.Send_Customer_Communication__c = true ;
		t2.Type = 'Call' ;
		t2.Interested_in_New_Account__c = 'Left Message' ;
		t2.WhatId = wa.id ;
		
		insert t2 ;
	}
	
	static testmethod void testTaskTriggerContact ()
	{
		Contact c = getContactEmail () ;
		
		RecordType rt = getRetailTask () ;
		
		List<Task> tL = new List<Task> () ;
		
		//  Set up the first task
		Task t1 = new Task () ;
		
		t1.RecordTypeId = rt.Id ;
		t1.Send_Customer_Communication__c = true ;
		t1.Type = 'Call' ;
		t1.Interested_in_New_Account__c = 'Not Interested' ;
		t1.whoid = c.id ;
		
		tL.add ( t1 ) ;
		
		//  Set up the second task
		Task t2 = new Task () ;
		
		t2.RecordTypeId = rt.Id ;
		t2.Send_Customer_Communication__c = true ;
		t2.Type = 'Call' ;
		t2.Interested_in_New_Account__c = 'Left Message' ;
		t2.whoId = c.id ;
		
		tL.add ( t2 ) ;
		
		//  Insert all the tasks
		insert tL ;
	}
	
	static testmethod void testTaskTriggerOpportunityNoEmail ()
	{
		//  Get the contact
		Contact c = getContactNoEmail () ;
		
		//  Set up the opportunity
		RecordType rtO = [
			SELECT ID FROM RecordType WHERE Name = 'Deposit Opportunity'
		] ;
		
		oneOpportunity__c o = new oneOpportunity__c () ;
		
		o.Email__c = 'none@none.com' ;
		o.Home_Phone__c = '123-456-7890' ;
		o.Interested_in_Product__c = 'Savings' ;
		o.Opportunity_Contact__c = c.id ;
		o.Opportunity_Source__c = 'Branch' ;
		o.Potential__c = 100.00 ;
		o.RecordTypeId = rtO.Id ;
		o.Type__c = 'New Customer' ;
		
		insert o ;
		
		RecordType rt = getRetailTask () ;
		
		//  Insert the first task
		Task t1 = new Task () ;
		
		t1.RecordTypeId = rt.Id ;
		t1.Send_Customer_Communication__c = true ;
		t1.Type = 'Call' ;
		t1.Interested_in_New_Account__c = 'Not Interested' ;
		t1.WhatId = o.id ;
		
		insert t1 ;
		
		//  Insert the second task
		Task t2 = new Task () ;
		
		t2.RecordTypeId = rt.Id ;
		t2.Send_Customer_Communication__c = true ;
		t2.Type = 'Call' ;
		t2.Interested_in_New_Account__c = 'Left Message' ;
		t2.WhatId = o.id ;
		
		insert t2 ;
	}
}