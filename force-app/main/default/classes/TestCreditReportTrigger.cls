@isTest
public class TestCreditReportTrigger 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;

	/* --------------------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------------------- */
    private static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = false ;

        s.Trial_Deposit_Validation_Limit__c = 3 ;
        s.ACH_Validation_Threshold__c = 1000.00 ;
        s.Deposit_ACH_Validation_Threshold__c = 1000.00 ;
        
        //  FIS STUFF
        s.FIS_Aquirer_ID__c = '059187' ;
        s.FIS_Authentication_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/authen' ;
        s.FIS_IDA_Config_Key__c = 'idaaliaskey' ;
        s.FIS_IDA_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/ida' ;
        s.FIS_Location_ID__c = 'UNITY Visa Application' ;
        s.FIS_Staging_Flag__c = true ;
        s.FIS_User_ID__c = '02280828' ;
        s.FIS_User_Password__c = 'Z26ZjcZdZAiZXaZ' ;
        
        insert s;
    }

    private static void insertFakeCert ()
    {
		one_CertificateApplication__c oc = new one_CertificateApplication__c();
		oc.Certificate__c = 'xxxxxx';
		oc.Active__c = true;
		oc.Password__c = '88889999';
        
        insert oc ;
    }

	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Creates a generic application I use for testing.
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, String rtID )
	{
		one_application__c app = new one_application__c ();
        app.RecordTypeId = rtID ;
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = '666-01-1111' ;
        app.Phone__c = c1.Phone ;
        app.Over_18__c = true ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
        app.RequestedCreditLimit__c = 250.00 ;
		
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = '666-02-1111' ;
            app.Co_Applicant_Phone__c = c2.Phone ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
            
            app.Co_Applicant_Gross_Income_Monthly__c = 1000.0 ;
            app.Co_Applicant_Employment_Status__c = 'Retired' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Funding_Options__c = 'E- Check (ACH)' ;
		app.Gross_Income_Monthly__c = 10000.00 ;
        app.Employment_Status__c = 'Retired' ;

		app.FIS_Application_ID__c = null ;
		app.FIS_Decision_Code__c = null ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        app.IDV_Applicant_Deceased__c = false ;
        app.IDV_Applicant_SSN_Before_Birth__c = false ;
        app.IDV_Applicant_Underage__c  = false ;
        
        //  Address - MINFraud
        app.Visitor_IP__c = '1.2.3.4' ;
        app.Visitor_User_Agent__c = 'Mozilla' ;
        app.Visitor_Accept_Language__c = 'YES' ;
		
		return app ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Creates a generic credit checker I use for testing.
	 * -------------------------------------------------------------------------------- */
    public static creditchecker__Credit_Report__c getCreditReport ( one_application__c app )
    {
        creditchecker__Credit_Report__c cccr = new creditchecker__Credit_Report__c () ;
        cccr.Application__c = app.ID ;
        cccr.creditchecker__Applicants_First_Name__c = app.First_Name__c ;
        cccr.creditchecker__Applicants_Last_Name__c = app.Last_Name__c ;
        cccr.creditchecker__Applicants_SSN__c = app.SSN__c ;
        cccr.creditchecker__Current_City__c = app.City__c ;
        cccr.creditchecker__Current_Country__c = 'USA' ;
        cccr.creditchecker__Current_StateCode__c = app.State__c ;
        cccr.creditchecker__Current_Zip_Code__c = app.ZIP_Code__c ;
        cccr.creditchecker__Current_Line__c = '1' ;
        
        return cccr ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Test SUCCESS
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TEST_Success ()
    {
        //  Setup
        insertCustomSetting () ;
        insertFakeCert () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        
        one_Application__c app = getApplicationForTests ( c1, c2, rtID ) ;
        insert app ;
        
        Test.startTest () ;
       
        creditchecker__Credit_Report__c cccr = getCreditReport ( app ) ;
        cccr.creditchecker__Status__c = 'Processing' ;
        
        insert cccr ;
        
        cccr.creditchecker__Status__c = 'Completed' ;
        update cccr ;
        
        Test.stopTest () ;
        
        one_Application__c appX = [
            SELECT ID, FIS_Decision_Code__C
            FROM one_application__c
            WHERE ID = :app.ID
            LIMIT 1
        ] ;
        
        //  Should return no hit because I've left the credit report mostly empty
        System.assertEquals ( appX.FIS_Decision_Code__c, 'P066' ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Test FAIL
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TEST_Failure ()
    {
        //  Setup
        insertCustomSetting () ;
        insertFakeCert () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        
        one_Application__c app = getApplicationForTests ( c1, c2, rtID ) ;
        insert app ;
        
        Test.startTest () ;
       
        creditchecker__Credit_Report__c cccr = getCreditReport ( app ) ;
        cccr.creditchecker__Status__c = 'Processing' ;
        
        insert cccr ;
        
        cccr.creditchecker__Status__c = 'Request Failed' ;
        update cccr ;
        
        Test.stopTest () ;
        
        one_Application__c appX = [
            SELECT ID, FIS_Decision_Code__C
            FROM one_application__c
            WHERE ID = :app.ID
            LIMIT 1
        ] ;
        
        //  Should return no hit because I've left the credit report mostly empty
        System.assertEquals ( appX.FIS_Decision_Code__c, 'E950' ) ;
    }
}