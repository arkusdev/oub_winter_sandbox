public with sharing class SurveyResponseController 
{
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    /*  ----------------------------------------------------------------------------------------
     *  Page controller(s)
     *  ---------------------------------------------------------------------------------------- */
    public Integer step { public get; private set; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Page display stuff
     *  ---------------------------------------------------------------------------------------- */
	public OneSurvey__c os { public get; private set; }
    public boolean textResponseRequired { public get ; private set ; }
    public boolean closedAccountQuestion { public get ; private set ; }
	 
    /*  ----------------------------------------------------------------------------------------
     *  For display
     *  ---------------------------------------------------------------------------------------- */
    public Survey_Response__c sr { public get; public set; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Input overrides due to edit access removal
     *  ---------------------------------------------------------------------------------------- */
    public string feedback { public get; public set; }
    public boolean testimonialDisclosure { public get; public set; }
    
    public string rating_1_5_select { public get ; public set ; }
 	public List<SelectOption> rating_1_5
    {
        get
        {
            List<SelectOption> options = new List<SelectOption> () ;
            options.add ( new SelectOption ( '1','1' ) ) ;
            options.add ( new SelectOption ( '2','2' ) ) ;
            options.add ( new SelectOption ( '3','3' ) ) ;
            options.add ( new SelectOption ( '4','4' ) ) ;
            options.add ( new SelectOption ( '5','5' ) ) ;
            return options;
        }
        
		private set ;
    }

    public string rating_1_10_select { public get ; public set ; }
    public List<SelectOption> rating_1_10
    {
        get
        {
            List<SelectOption> options = new List<SelectOption> () ;
            options.add ( new SelectOption ( '1','1' ) ) ;
            options.add ( new SelectOption ( '2','2' ) ) ;
            options.add ( new SelectOption ( '3','3' ) ) ;
            options.add ( new SelectOption ( '4','4' ) ) ;
            options.add ( new SelectOption ( '5','5' ) ) ;
            options.add ( new SelectOption ( '6','6' ) ) ;
            options.add ( new SelectOption ( '7','7' ) ) ;
            options.add ( new SelectOption ( '8','8' ) ) ;
            options.add ( new SelectOption ( '9','9' ) ) ;
            options.add ( new SelectOption ( '10','10' ) ) ;
            return options;
        }
        
		private set ;
    }

    public string rating_y_n_select { public get ; public set ; }
 	public List<SelectOption> rating_y_n
    {
        get
        {
            List<SelectOption> options = new List<SelectOption> () ;
            options.add ( new SelectOption ( 'Yes','Yes' ) ) ;
            options.add ( new SelectOption ( 'No','No' ) ) ;
            return options;
        }
        
		private set ;
    }

    public string rating_text { public get ; public set ; }

    public string survey_response_select { public get ; public set ; }
    public list<SelectOption> survey_response
    {
        get
        {
            return loadStupidObjectOptionSelectList () ;
        }
        
		private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Possible reload?
     *  ---------------------------------------------------------------------------------------- */
    private Web_Settings__c ws ;
    private String method ;

    /*  ----------------------------------------------------------------------------------------
     *  UTM-ick-ish
     *  ---------------------------------------------------------------------------------------- */
    private Boolean UTM_are_values_loaded ;
    
    public String UTM_Campaign { public get ; public set ; }
    public String UTM_Content { public get ; public set ; }
    public String UTM_Medium { public get ; public set ; }
    public String UTM_Source { public get ; public set ; }
    public String UTM_Term { public get ; public set ; }
    public String UTM_VisitorID { public get ; public set ; }

    public String visitor_browser { public get ; public set ; }
    public String visitor_browser_version { public get ; public set ; }
    public String visitor_os { public get ; public set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Internals
     *  ---------------------------------------------------------------------------------------- */
    private String q = 'SELECT ID FROM Survey_Response__c WHERE ' ;

    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public SurveyResponseController ( ApexPages.Standardcontroller stdC )
	{
		//  SETUP - ASS-U-ME no survey response coming in
		step = 1000 ;
		method = 'INSERT' ;
		UTM_are_values_loaded = false ;
        textResponseRequired = true ;
        closedAccountQuestion = false ;
		
		//  Default survey to override to as well as possible survey responses
		ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;

		//  Set up properly?		
		if ( ( ws != null ) && ( String.isNotBlank ( ws.Survey_Answers__c ) ) )
		{
			Map<String,String> pMap = ApexPages.currentPage().getParameters () ;
			
			if ( pMap != null )
			{
				for ( String p : pMap.keyset () )
				{
					//  If the parameter contains the survey response, set step to update & refresh
					if ( ws.Survey_Answers__c.contains ( p ) )
						step = 0 ;
				}
			}
		}

		//  No incoming response, set up the survey object and skip straight to the form
		if ( step == 1000 )
		{
			String s = ApexPages.currentPage().getParameters().get ( 'Survey__c' ) ;
			os = getSurvey ( s ) ;
			
			if ( os != null )
			{
				sr = new Survey_Response__c () ;
				sr.Survey__c = os.ID ;
			}
			else
			{
				System.debug ( 'No survey found?!?' ) ;
                step = 9999 ;
			}
		}
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  DMLish Crap, can't do in constructor :-/
     *  ---------------------------------------------------------------------------------------- */
	public PageReference setupSurveyResponse ()
	{
        System.debug ( 'Parsing Incoming response!' ) ;
        
		//  INCOMING
		Integer result = parseSurveyResponse () ;
		
		if ( ( result == 0 ) && ( os != null ) )
		{
			//  Set field to default
			sr.Testimonial_Disclosure__c = true ;
            
            //  Set display fields
            rating_1_5_select = sr.Rating_1_5__c ;
            rating_1_10_select = sr.Rating_1_10__c ;
            rating_Y_N_select = sr.Rating_Yes_No__c ;
			
			if ( os.Disable_Survey_Edit__c == false )
			{
                //  Display page
                step = 2000 ;
                method = 'UPDATE' ;
            }
            else
            {
                //  Skip straight to end
                step = 5000 ;
                method = 'NYET' ;
            }
		}
		else
		{
			//  No survey found
			System.debug ( 'Parsing is incorrect, going to nope page.' ) ;
			step = 9999 ;
		}
		
		return null ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Submit survey update
     *  ---------------------------------------------------------------------------------------- */
	public PageReference submit ()
	{
		loadUTMValues () ;
		
        if ( String.isNotBlank ( feedback ) )
            sr.Feedback__c = feedback ;
        
        if ( String.isNotBlank ( survey_response_select ) )
	        sr.Closed_Account_Survey_Response__c = survey_response_select ;
        
        if ( testimonialDisclosure != null )
            sr.Testimonial_Disclosure__c = testimonialDisclosure ;
        
        if ( String.isNotBlank ( rating_1_5_select ) )
	        sr.Rating_1_5__c = rating_1_5_select ;
        
        if ( String.isNotBlank ( rating_1_10_select ) )
	        sr.Rating_1_10__c = rating_1_10_select ;
        
        if ( String.isNotBlank ( rating_y_n_select ) )
	        sr.Rating_Yes_No__c = rating_y_n_select ;
        
        if ( String.isNotBlank ( rating_text ) )
	        sr.Rating_Text__c = rating_text ;
        
		try
		{
			if ( method.equalsIgnoreCase ( 'INSERT' ) )
				//insert sr ;
            	access.insertObject(sr);
				
			if ( method.equalsIgnoreCase ( 'UPDATE' ) )
				//update sr ;
            	access.updateObject(sr);
		}
		catch ( Exception e )
		{
			System.debug ( 'Unable to ' + method + ' survey response?!?' ) ;
		}
		
		step = 5000 ;
		
		return null ;
	} 
     
    /*  ----------------------------------------------------------------------------------------
     *  Save & Set up survey response
     *  ---------------------------------------------------------------------------------------- */
	private Integer parseSurveyResponse () 
	{
		Integer result = 0 ;
        sr = new Survey_Response__c () ;
		sr.Testimonial_Disclosure__c = false ;
                
        String landingURL = calculateURL () ;
        String landingURLShort = calculateURLShort () ;
                
        boolean hazPrior = false ;
        Survey_Response__c osr = getPriorSurveyResponse ( landingURLShort ) ;
        
        if ( osr != null )
        {
            sr.ID = osr.ID ;
            hazPrior = true ;
        }
        else
        {
            hazPrior = false ;
        }
        
        sr.Landing_URL__c = landingURL ;
        sr.Landing_URL_Short__c = landingURLShort ;
        
        //  Insert basically empty object with just the query string, survey ID, and the incoming ID
        String sID = ApexPages.currentPage().getParameters().get ( 'Survey__c' ) ;
        String uID = ApexPages.currentPage().getParameters().get ( 'uid' ) ;
        
        //  Check to see if survey actually exists - return default if it doesn't
        os = getSurvey ( sID ) ;
        
        if ( os != null )
        {
            if ( ! hazPrior )
	            sr.Survey__c = os.ID ; // can't override master detail!
        }
        else
        {
            result = 1 ;
        }
        
        if ( String.isNotBlank ( uID ) )
        {
            System.debug ( 'Incoming ID :: ' + uID ) ;
            sr.Response_ID__c = uID ;
        }
        
        loadUTMValues () ;
        
		try
		{
			if ( result == 0 )
            {
                if ( hazPrior )
                    //update sr ;
                	access.updateObject(sr);
                else
                    //insert sr ;
                	access.insertObject(sr);
            }
		}
		catch ( Exception e )
		{
			System.debug ( 'Unable to insert/update blank survey response?!?' ) ;
			result = 1 ;
		}
		
		if ( result == 0 )
		{
			//  Update check
			boolean hazUpdate = false ;
			
			//  Attempt to update the object with the results from the string
	        Map<String, Schema.SobjectField> iFields = Survey_Response__c.getSObjectType().getDescribe().fields.getMap();
	        
	        for ( String s : iFields.keySet () ) 
	        {
	            if  ( iFields.get ( s ).getDescribe(). isAccessible () ) 
	            {
	            	if ( ( ! s.equalsIgnoreCase ( 'Survey__c' ) ) && ( ! s.equalsIgnoreCase ( 'uid' ) ) )
	            	{
		                String x = ApexPages.currentPage().getParameters().get ( s ) ;
		                
		                if ( String.isNotBlank ( x ) )
		                {
			                sr.put ( s, x ) ;
			                hazUpdate = true ;
			                
			                System.debug ( 'Found field = "' + s + '" value = "' + x + '"' ) ;
		                }
		                else
		                {
			                System.debug ( 'Found field = "' + s + '" NO VALUE' ) ;
		                }

	            	}
	            	else
	            	{
	            		System.debug ( 'Skipping "' + s + '", we have it.' ) ;
	            	}
	            }
	            else
	            {
	            	System.debug ( 'Not allowed to touch ' + s + ' ?' ) ;
	            }
	        }
			
			try
			{
				if ( hazUpdate )
				{
					System.debug ( 'Updating survey response object' ) ;
					//update sr ;
                    access.updateObject(sr);
				}
				else
				{
					System.debug ( 'No update required' ) ;
				}
				
			}
			catch ( Exception e )
			{
				// ?
				System.debug ( 'Unable to update survey response ?!?' ) ;
			}
		}
		else
		{
			System.debug ( 'Initial Insert failed, skipping update' ) ;
		}
		
		return result ;
	}
	
	//  Returns survey ID if valid, otherwise default
	private OneSurvey__c getSurvey ( String sID )
	{
		System.debug ( 'Searching for = ' + sID ) ;
		
		list<OneSurvey__c> osL = null ;
		OneSurvey__c os = null ;
		
		try
		{
			if ( String.isNotBlank ( sID ) )
			{
				osL = [
					SELECT ID, Feedback_Type__c, Banner_URL__c,
					Header__c, Description__c, Question__c, Closed_Account_Survey_Question__c,
					Response_Description__c, Response_Question__c, Response_Header__c,
                    Feedback_Text_Not_Requred__c, Disable_Survey_Header__c, Disable_Survey_Edit__c,
                    Thank_You_Message__c
					FROM OneSurvey__c
					WHERE ID = :sID
					AND Active__c = true
					AND Start_Date__c <= TODAY
					AND End_Date__c >= TODAY
				] ;
			}
		}
		catch ( Exception e )
		{
			System.debug ( 'Error selecting survey?!?' ) ;
		}
		
		if ( ( osL != null ) && ( osL.size () > 0 ) )
		{
			System.debug ( 'Valid and active survey found.' ) ;
			
			//  Grab the first one
			os = osL [ 0 ] ;
            
            if ( String.isNotBlank ( os.Closed_Account_Survey_Question__c ) )
                closedAccountQuestion = true ;
		}
		else
		{
			System.debug ( 'Survey ' + sID + ' not found, attempting to default.' ) ;

			if ( ws != null )
			{
				if ( String.isNotBlank ( ws.Default_Survey_ID__c ) )
				{
					try
					{
						osL = [
							SELECT ID, Feedback_Type__c, Banner_URL__c,
							Header__c, Description__c, Question__c, Closed_Account_Survey_Question__c, 
							Response_Description__c, Response_Question__c, Response_Header__c,
                            Feedback_Text_Not_Requred__c, Disable_Survey_Header__c, Disable_Survey_Edit__c,
                            Thank_You_Message__c
							FROM OneSurvey__c
							WHERE ID = :ws.Default_Survey_ID__c
							AND Active__c = true
							AND Start_Date__c <= TODAY
							AND End_Date__c >= TODAY
						] ;
					}
					catch ( Exception e )
					{
						System.debug ( 'Error selecting survey?!?' ) ;
					}
					
					if ( ( osL != null ) && ( osL.size () > 0 ) )
					{
						System.debug ( 'Valid and active survey found.' ) ;
						
						//  Grab the first one
						os = osL [ 0 ] ;
                        
                        if ( String.isNotBlank ( os.Closed_Account_Survey_Question__c ) )
                            closedAccountQuestion = true ;
					}
					else
					{
						System.debug ( 'No default survey found (SOQL). :(' ) ;
					}
				}
				else
				{
					System.debug ( 'No default survey found (Setting). :(' ) ;
				}
			}
			else
			{
				System.debug ( 'No web settings found.' ) ;
			}
		}
		
		return os ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  UTM
     *  ---------------------------------------------------------------------------------------- */
	private void loadUTMValues ()
	{
		if ( UTM_are_values_loaded == false )
		{
			UTM_are_values_loaded = true ;
			
			sr.UTM_Campaign__c = UTM_Campaign ;
			sr.UTM_Content__c = UTM_Content ;
			sr.UTM_Medium__c = UTM_Medium ;
			sr.UTM_Source__c = UTM_Source ;
			sr.UTM_Term__c = UTM_Term ;
			sr.UTM_Visitor_ID__c = UTM_VisitorID ;
			
			sr.Visitor_Operating_System__c = visitor_os;
			sr.Visitor_Browser__c = visitor_browser;
			sr.Visitor_Browser_version__c = visitor_browser_version;
		}
	}
    
    /*  ----------------------------------------------------------------------------------------
     *  URL, Assemble!
     *  ---------------------------------------------------------------------------------------- */
    private String calculateURL ()
    {
        String s = '' ;
        
        s = URL.getCurrentRequestUrl().toExternalForm() ; // only the start
        
        //  Load the rest
        Map<String,String> pMap = ApexPages.currentPage().getParameters () ;
        
        if ( pMap != null )
        {
            String x = '?' ;
            for ( String p : pMap.keyset () )
            {
                //  Exclude salesforce internal stuff
                if ( ( ! p.contains ( 'j_id') ) && ( ! p.contains ( 'ViewState' ) ) )
                {
                    s += x + p + '=' + pMap.get ( p ) ;
                    x = '&' ;
                }
            }
        }
        
        return s ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  URL, Assemble! & SHRINK
     *  ---------------------------------------------------------------------------------------- */
    private String calculateURLShort ()
    {
        String s = calculateURL () ;
        s = s.replace ( 'Rating_1_5__c=1', 'Rating_1_5__c=X' ) ;
        s = s.replace ( 'Rating_1_5__c=2', 'Rating_1_5__c=X' ) ;
        s = s.replace ( 'Rating_1_5__c=3', 'Rating_1_5__c=X' ) ;
        s = s.replace ( 'Rating_1_5__c=4', 'Rating_1_5__c=X' ) ;
        s = s.replace ( 'Rating_1_5__c=5', 'Rating_1_5__c=X' ) ;
        
        return s.left ( 250 ) ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Prior?!?
     *  ---------------------------------------------------------------------------------------- */
    private Survey_Response__c getPriorSurveyResponse ( String s )
    {
        System.debug ( 'S = ' + s ) ;
        
        list<Survey_Response__c> srL = null ;
        Survey_Response__c sr = null ;
        
        try
        {
            srL = [
                SELECT ID
                FROM Survey_Response__c
                WHERE Landing_URL_Short__c = :s
                AND CreatedDate = TODAY
                ORDER BY CreatedDate DESC
            ] ;
        }
        catch ( Exception e )
        {
            sr = null ;
        }
        
       	if ( ( srL != null ) && ( srL.size () > 0 ) )
            sr = srL [ 0 ] ;
        
        return sr ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Prior?!?
     *  ---------------------------------------------------------------------------------------- */
    private list<SelectOption> loadStupidObjectOptionSelectList ()
    {
        list<SelectOption> stupidSelect = new list<SelectOption> () ;
        
        Schema.DescribeFieldResult fieldResult = survey_response__c.Closed_Account_Survey_Response__c.getDescribe () ;
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues () ;
        
        for( Schema.PicklistEntry pickListVal : ple)
        {
            stupidSelect.add ( new SelectOption ( picklistVal.getValue (), picklistVal.getLabel () ) ) ;
        }
        
        return stupidSelect ;
    }
}