public with sharing class PromotionIncludeController 
{
	public String promoCode { public get ; private set ; }
	public boolean validPromoCode { public get ; private set ; }
	
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public PromotionIncludeController ()
	{
		one_referral oneR = new one_referral () ;
		
		oneR.loadThirdPartyData () ;
		
		one_referral.PromotionData pd = oneR.loadPromoCode () ;
		promoCode = pd.promoCode ;
		validPromoCode = pd.validPromotionCode ;
	}
}