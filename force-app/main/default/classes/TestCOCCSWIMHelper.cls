@isTest
public class TestCOCCSWIMHelper 
{
	private static Batch_Process_Settings__c insertCustomSetting ()
    {
        Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;

        bps.Name = 'OneUnited_Batch' ;        
        bps.ACH_GL_Account__c = '1001000100' ;
        bps.Application_Cashbox__c = '666' ;
        bps.Batch_Email__c = 'lmattera@oneunited.com' ;
        bps.Collateral_GL_Account__c = '1001000101' ;
        bps.Deposit_ePay_GL_Account__c = '1001000102' ;
        bps.Email_Enabled__c = false ;
        bps.Routing_Number__c = '01100127' ;
        bps.UNITY_Visa_ePay_GL_Account__c = '1001000103' ;
        
        insert bps ;
        
        return bps ;
    }

    public static testmethod void testSingleONE ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
        Test.startTest () ;
        
        COCCSWIMHelper csh = new COCCSWIMHelper () ;
        
        csh.createSWIMRecord ( COCCSWIMHelper.isDeposit (), bps.Application_Cashbox__c, bps.Collateral_GL_Account__c, 250.25, '5000000', 'Test SWIM' ) ;
        csh.createSWIMRecord ( COCCSWIMHelper.isWithdrawal (), bps.Application_Cashbox__c, bps.UNITY_Visa_ePay_GL_Account__c, 250.25, '5000000', 'Test SWIM' ) ;
        
        String swmThingy = csh.getSWIMFile () ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void testSingleTWO ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
        Test.startTest () ;
        
        COCCSWIMHelper csh = new COCCSWIMHelper () ;
        
        csh.createSWIMRecord ( COCCSWIMHelper.isDeposit (), bps.Collateral_GL_Account__c, 250.25, '5000000', 'Test SWIM' ) ;
        csh.createSWIMRecord ( COCCSWIMHelper.isWithdrawal (), bps.UNITY_Visa_ePay_GL_Account__c, 250.25, '5000000', 'Test SWIM' ) ;
        
        String swmThingy = csh.getSWIMFile () ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void testDoubleONE ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
        Test.startTest () ;
        
        COCCSWIMHelper csh = new COCCSWIMHelper () ;
        
        csh.createSWIMDoubleRecord ( bps.Collateral_GL_Account__c, bps.UNITY_Visa_ePay_GL_Account__c, bps.Application_Cashbox__c, 250.25, '5000000', 'Test SWIM' ) ;
        
        String swmThingy = csh.getSWIMFile () ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void testDoubleTWO ()
    {
        Batch_Process_Settings__c bps = insertCustomSetting () ;
        
        Test.startTest () ;
        
        COCCSWIMHelper csh = new COCCSWIMHelper () ;
        
        csh.createSWIMDoubleRecord ( bps.Collateral_GL_Account__c, bps.UNITY_Visa_ePay_GL_Account__c, 250.25, '5000000', 'Test SWIM' ) ;
        
        String swmThingy = csh.getSWIMFile () ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void testMisc ()
    {
       	System.assertEquals ( COCCSWIMHelper.isDescriptiveDeposit (), 'DEPD' ) ;
       	System.assertEquals ( COCCSWIMHelper.isExternalDeposit (), 'XDEP' ) ;
    }
}