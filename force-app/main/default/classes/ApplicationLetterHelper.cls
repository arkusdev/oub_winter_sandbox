public class ApplicationLetterHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Double map tracking
	 *  -------------------------------------------------------------------------------------------------- */	
    public static map <String, boolean> letterMap ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Generic checker
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean checkDuplicate ( Map<String,boolean> xMap, String id )
	{
		boolean returnValue = false ;
		
		if ( xMap.containsKey ( id ) == true )
			returnValue = true ;
		else
			xMap.put ( id, true ) ;
		
        System.debug ( 'Returning : ' + returnValue ) ;
        
		return returnValue ;
	}
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Get crap
	 *  -------------------------------------------------------------------------------------------------- */	
    public static map<ID,creditchecker__Credit_Report__c> getCreditReport ( List<ID> appIDL )
    {
        map<ID,creditchecker__Credit_Report__c> crM = new map<ID,creditchecker__Credit_Report__c> () ;
        
        try
        {
            list<creditchecker__Credit_Report__c> crL = [
                SELECT ID, Application__c, creditchecker__Date_Completed__c, creditchecker__Average_Score__c, creditchecker__Score_Tier__c,
				(Select Id, creditchecker__Credit_Score__c, creditchecker__Factor_Code__c, creditchecker__Whose__c, creditchecker__Model_Name_Description__c
                 From creditchecker__Score_Models__r)
                FROM creditchecker__Credit_Report__c
                WHERE Application__c IN :appIDL
            ] ;
            
            if ( ( crL != null ) && ( crL.size () > 0 ) )
            {
                for ( creditchecker__Credit_Report__c cr : crL )
                {
                    crM.put ( cr.Application__c, cr ) ;
                }
            }
        }
        catch ( DMLException e )
        {
            OneUnitedUtilities.parseStackTrace ( e ) ;
        }
        
        return crM ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ADV406 - Counteroffer
	 *  -------------------------------------------------------------------------------------------------- */	
    public static Automated_Email__c createCounterofferLetter ( one_application__c app, creditchecker__Credit_Report__c cr )
    {
        Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByName.get('Batch Letter').RecordTypeId ;
        ae.Template_Name__c = 'ADV406' ;
        ae.Application__c = app.ID ;
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
	        ae.Application_ID__c = app.FIS_Application_ID__c ;
        else
            ae.Application_ID__c = app.Name ;
        ae.Status__c = 'Processing' ;
        
        ae.Contact__c = app.Contact__c ;
        ae.First_Name__c = app.First_Name__c ;
        ae.Last_Name__c = app.Last_Name__c ;
        
        ae.Mailing_Address__c = app.Street_Address__c ;
        ae.Mailing_City__c = app.City__c ;
        ae.Mailing_State__c = app.State__c ;
        ae.Mailing_Zip__c = app.Mailing_ZIP_Code__c ;
        
        ae.Credit_Decline_Reasons__c = app.Credit_Decline_Reasons__c ;
        ae.Counter_Offer_Credit_Limit__c = app.Counter_Offer_Limit__c ;
        ae.Credit_Report_Date__c = cr.creditchecker__Date_Completed__c ;
        ae.Credit_Report_Source__c = 'Experian' ;
        
        if ( cr.creditchecker__Score_Tier__c == 'No Score Available' )
            ae.Credit_Score__c = 0.0 ;
        else
	        ae.Credit_Score__c = cr.creditchecker__Average_Score__c ;
        
        for (creditchecker__Score_Model__c sm: cr.creditchecker__Score_Models__r) 
        {
            if ( sm.creditchecker__Whose__c == 'Borrower' && sm.creditchecker__Model_Name_Description__c == 'ExperianVantageScore' )
            {
                ae.Credit_Score_Reasons__c = sm.creditchecker__Factor_Code__c ;
            }
			else if ( sm.creditchecker__Whose__c == 'Co-Borrower' &&  sm.creditchecker__Model_Name_Description__c == 'ExperianVantageScore' && sm.creditchecker__Credit_Score__c != null ) 
            {
                ae.Co_Applicant_Credit_Score__c = sm.creditchecker__Credit_Score__c ;
                ae.Co_Applicant_Credit_Score_Reasons__c = sm.creditchecker__Factor_Code__c ;
            }
        }
        
        return ae ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ADV407 - Approved
	 *  -------------------------------------------------------------------------------------------------- */	
    public static Automated_Email__c createApprovalLetter ( one_application__c app, creditchecker__Credit_Report__c cr )
    {
        Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByName.get('Batch Letter').RecordTypeId ;
        ae.Template_Name__c = 'ADV407' ;
        ae.Application__c = app.ID ;
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
	        ae.Application_ID__c = app.FIS_Application_ID__c ;
        else
            ae.Application_ID__c = app.Name ;
        ae.Status__c = 'Processing' ;
        
        ae.Contact__c = app.Contact__c ;
        ae.First_Name__c = app.First_Name__c ;
        ae.Last_Name__c = app.Last_Name__c ;
        
        ae.Mailing_Address__c = app.Street_Address__c ;
        ae.Mailing_City__c = app.City__c ;
        ae.Mailing_State__c = app.State__c ;
        ae.Mailing_Zip__c = app.Mailing_ZIP_Code__c ;
        
        return ae ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ADV411 - Declined
	 *  -------------------------------------------------------------------------------------------------- */	
    public static Automated_Email__c createDeclineLetter ( one_application__c app, creditchecker__Credit_Report__c cr )
    {
        Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByName.get('Batch Letter').RecordTypeId ;
        ae.Template_Name__c = 'ADV411' ;
        ae.Application__c = app.ID ;
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
	        ae.Application_ID__c = app.FIS_Application_ID__c ;
        else
            ae.Application_ID__c = app.Name ;
        ae.Status__c = 'Processing' ;
        
        ae.Contact__c = app.Contact__c ;
        ae.First_Name__c = app.First_Name__c ;
        ae.Last_Name__c = app.Last_Name__c ;
        
        ae.Mailing_Address__c = app.Street_Address__c ;
        ae.Mailing_City__c = app.City__c ;
        ae.Mailing_State__c = app.State__c ;
        ae.Mailing_Zip__c = app.Mailing_ZIP_Code__c ;
        
        ae.Credit_Decline_Reasons__c = app.Credit_Decline_Reasons__c ;
        ae.Credit_Report_Date__c = cr.creditchecker__Date_Completed__c ;
        ae.Credit_Report_Source__c = 'Experian' ;
        
        if ( cr.creditchecker__Score_Tier__c == 'No Score Available' )
            ae.Credit_Score__c = 0.0 ;
        else
	        ae.Credit_Score__c = cr.creditchecker__Average_Score__c ;
        
        for (creditchecker__Score_Model__c sm: cr.creditchecker__Score_Models__r) 
        {
            if ( sm.creditchecker__Whose__c == 'Borrower' && sm.creditchecker__Model_Name_Description__c == 'ExperianVantageScore' )
            {
                ae.Credit_Score_Reasons__c = sm.creditchecker__Factor_Code__c ;
            }
			else if ( sm.creditchecker__Whose__c == 'Co-Borrower' &&  sm.creditchecker__Model_Name_Description__c == 'ExperianVantageScore' && sm.creditchecker__Credit_Score__c != null ) 
            {
                ae.Co_Applicant_Credit_Score__c = sm.creditchecker__Credit_Score__c ;
                ae.Co_Applicant_Credit_Score_Reasons__c = sm.creditchecker__Factor_Code__c ;
            }
        }
        
        return ae ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ADV412 - Fraud
	 *  -------------------------------------------------------------------------------------------------- */	
    public static Automated_Email__c createFraudLetter ( one_application__c app, creditchecker__Credit_Report__c cr )
    {
        Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByName.get('Batch Letter').RecordTypeId ;
        ae.Template_Name__c = 'ADV412' ;
        ae.Application__c = app.ID ;
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
	        ae.Application_ID__c = app.FIS_Application_ID__c ;
        else
            ae.Application_ID__c = app.Name ;
        ae.Status__c = 'Processing' ;
        
        ae.Contact__c = app.Contact__c ;
        ae.First_Name__c = app.First_Name__c ;
        ae.Last_Name__c = app.Last_Name__c ;
        
        ae.Mailing_Address__c = app.Street_Address__c ;
        ae.Mailing_City__c = app.City__c ;
        ae.Mailing_State__c = app.State__c ;
        ae.Mailing_Zip__c = app.Mailing_ZIP_Code__c ;
        
        return ae ;
    }
}