public class oneTicketTab {   
    public List<Ticket__c> recentTickets {get; set;}
   
    public oneTicketTab(){
       recentTickets = [SELECT ID, 
                    LastViewedDate,  
                    Name,  
                    RecordType.Name,  
                    Status__c,  
                    Subject__c,  
                    Priority__c,  
                    Ticket_Age__c,  
                    Days_Since_Change__c,  
                    Owner.Name
             FROM Ticket__c
            WHERE LastViewedDate != NULL
         ORDER BY LastViewedDate DESC];
    }
   
}