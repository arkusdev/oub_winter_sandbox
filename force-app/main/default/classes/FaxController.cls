public class FaxController {
    
    //page Object's
    public String displayattachment
    {
    	public get ;
    	private set ;
    }
    
    public ContentVersion versionObject
    {
        public get ;
        private set ;
    }
    
    
	//controller
    public FaxController ( ApexPages.Standardcontroller stdC ) {
        
        //  Get the object from the page
		Ticket__c ticket = (Ticket__c) stdC.getRecord () ;

       //  Attachments!
       list<ContentDocumentLink> cd1 = [
            SELECT ID, ContentDocumentId
            FROM ContentDocumentLink
            WHERE LinkedEntityId = :ticket.ID
        ] ;  
     
        if(cd1 !=null && cd1.size() >0){
            list<ContentVersion> contentversion = [
                SELECT ID, ContentDocumentId, CreatedDate, FileExtension, Title, VersionData, LastModifiedDate, ContentBodyId 
                FROM ContentVersion
            	WHERE ContentDocumentId = :cd1[0].ContentDocumentId
                ORDER BY LastModifiedDate DESC
            ];
            
            
            System.debug(contentversion[0]);
            
            if( contentversion != null && contentversion.size() >0){
              if( contentversion[0].FileExtension=='pdf'){
                       
                  	   String imageurl = '/sfc/servlet.shepherd/version/renditionDownload?rendition=SVGZ&versionId=' + contentversion[0].ID ;
                  	   
                  	   versionObject = contentversion[0];
                       displayattachment = imageurl;
                       
                       System.debug(displayattachment);
                   }
               }  
            }    
        }
	}