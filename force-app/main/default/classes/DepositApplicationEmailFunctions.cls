public class DepositApplicationEmailFunctions 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Settings
	 *  -------------------------------------------------------------------------------------------------- */	
	private static final String API_KEY = 'o-zuUl0_dDiq6OT87S1Agg' ;
	
	private static final String ONEUNITED_FROM_EMAIL = 'noreplies@oneunited.com' ;
	private static final String ONEUNITED_FROM_NAME = 'OneUnited Bank' ;
	private static final String SETTINGS = 'Andera' ;
	
	private static deposit_application_settings__c das ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Deposit Application Additional Info message
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void sendDepositApplicationAdditionalInfoMessage ( Deposit_Application__c [] daL )
	{
		System.debug ( '========== Creating JSON Object ==========' ) ;
		
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
		
		//  Create the object
		g.writeStartObject () ;
		
		//  API KEY
		g.writeStringField ( 'key', API_KEY ) ;
		
		//  Template
		g.writeStringField ( 'template_name', 'andera-address-verification-1' ) ;
		
		g.writeFieldName ( 'template_content' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'OneUnited Bank Template' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;

		//  Start of message		
		g.writeFieldName ( 'message' ) ;
		g.writeStartObject () ;

		//  Message contents
		g.writeStringField ( 'from_email', ONEUNITED_FROM_EMAIL ) ;
		g.writeStringField ( 'from_name', ONEUNITED_FROM_NAME ) ;
		
		//  To stuff, always an array!
		g.writeFieldName ( 'to' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'email', da.Email__c ) ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( String.isNotEmpty ( da.Co_Applicant_Email__c ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'email', da.Co_Applicant_Email__c ) ;
				g.writeEndObject () ;
			}
		}
		
		g.writeEndArray () ;
		
		g.writeStringField ( 'subject', 'Additional Information Required to Process Your Application' ) ;
		g.writeStringField ( 'subaccount', 'DEPOSITAPPLICATIONIDV' ) ;
		
		// GLOBAL Merge vars
		g.writeFieldName ( 'global_merge_vars' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:COMPANY' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:Description' ) ;
		g.writeStringField ( 'content', '' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;

		// LOCAL Merge vars
		g.writeFieldName ( 'merge_vars' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			String appIDMessageText = ' ' ;
			if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
				appIDMessageText = ' # ' + da.Confirmation_Number__c ;
			
			String additionalInfo = '' ;
			list<String> additionalDocs = Deposit_Application_Utils.getAdditionalInfoNeeded ( da.Additional_Information_Required__c ) ;
			
			if ( ( additionalDocs != null ) && ( additionalDocs.size () > 0 ) )
			{
				for ( String doc : additionalDocs )
					additionalInfo += '<li>' +  doc + '</li>' ;
			}
			
			String days = 'today' ;
		 	date cd = da.CreatedDate.date () ;
			Integer diff = Math.abs ( cd.daysBetween ( Date.today () ) ) ;
			Integer timeLeft = 15 - diff ;

			if ( timeLeft > 0 )
				days = String.valueOf ( timeLeft ) + ' day(s)' ;
			
			System.debug ( 'DAYS: ' + days ) ;
			
			//  Applicant!
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', da.Email__c ) ;
			g.writeFieldName ( 'vars' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeApplicationID' ) ;
			g.writeStringField ( 'content', appIDMessageText ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'First_Name__c' ) ;
			g.writeStringField ( 'content', da.First_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'Last_Name__c' ) ;
			g.writeStringField ( 'content', da.Last_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeSalesforceApplicationID' ) ;
			g.writeStringField ( 'content', da.Id ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeSalesforceKeyID' ) ;
			g.writeStringField ( 'content', da.External_Key__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeMissingInformation' ) ;
			g.writeStringField ( 'content', additionalInfo ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeDaysRemaining' ) ;
			g.writeStringField ( 'content', days ) ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( String.isNotBlank ( da.Co_Applicant_Email__c ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'rcpt', da.Co_Applicant_Email__c ) ;
				g.writeFieldName ( 'vars' ) ;
				g.writeStartArray () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeApplicationID' ) ;
				g.writeStringField ( 'content', appIDMessageText ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'FirstName' ) ;
				g.writeStringField ( 'content', da.Co_Applicant_First_Name__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'LastName' ) ;
				g.writeStringField ( 'content', da.Co_Applicant_Last_Name__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeSalesforceApplicationID' ) ;
				g.writeStringField ( 'content', da.Id ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeSalesforceKeyID' ) ;
				g.writeStringField ( 'content', da.External_Key__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeMissingInformation' ) ;
				g.writeStringField ( 'content', additionalInfo ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeDaysRemaining' ) ;
				g.writeStringField ( 'content', days ) ;
				g.writeEndObject () ;
				g.writeEndArray () ;
				g.writeEndObject () ;
			}
		}
		g.writeEndArray () ;
		
		//  Campaign
		g.writeStringField ( 'google_analytics_campaign', 'Deposit Application Additional Information Required' ) ;
		
		//  Tags
		g.writeFieldName ( 'tags' ) ;
		g.writeStartArray () ;
		g.writeString ( 'welcome-email' ) ;
		g.writeEndArray () ;

		//  Googly
		g.writeFieldName ( 'google_analytics_domains' ) ;
		g.writeStartArray () ;
		g.writeString ( 'oneunited.com' ) ;
		g.writeString ( 'www.oneunited.com' ) ;
		g.writeEndArray () ;

		//  Recipient Meta data
		g.writeFieldName ( 'recipient_metadata' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', da.Email__c ) ;
			g.writeFieldName ( 'values' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'salesforceApplicationId', da.Id ) ;
			if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
				g.writeStringField ( 'AppId', da.Confirmation_Number__c ) ;
			if ( da.Tax_Reported_Contact__c != null )
				g.writeStringField ( 'salesforceContactId', da.Tax_Reported_Contact__c ) ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( String.isNotBlank ( da.Co_Applicant_Email__c ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'rcpt', da.Co_Applicant_Email__c ) ;
				g.writeFieldName ( 'values' ) ;
				g.writeStartArray () ;
				g.writeStartObject () ;
				g.writeStringField ( 'salesforceApplicationId', da.Id ) ;
				if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
					g.writeStringField ( 'AppId', da.Confirmation_Number__c ) ;
				if ( da.Co_Applicant_Tax_Reported_Contact__c != null )
					g.writeStringField ( 'salesforceContactId', da.Co_Applicant_Tax_Reported_Contact__c ) ;
				g.writeEndObject () ;
				g.writeEndArray () ;
				g.writeEndObject () ;
			}
		}
		
		g.writeEndArray () ;
		
		//  End of Message
		g.writeEndObject () ;
		
		//  End of object
		g.writeEndObject () ;
		
		System.debug ( '========== JSON OUTPUT START ==========' ) ;
		System.debug ( g.getAsString () ) ;
		System.debug ( '========== JSON OUTPUT END ==========' ) ;
		
        sendHTTPRequest ( g.getAsString () ) ;
		
        System.debug ( '========== Done! ==========' ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  UNITY Visa Declined message
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void sendDepositApplicationDenialMessage ( Deposit_Application__c [] daL )
	{
		System.debug ( '========== Creating JSON Object ==========' ) ;
		
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
		
		//  Create the object
		g.writeStartObject () ;
		
		//  API KEY
		g.writeStringField ( 'key', API_KEY ) ;
		
		//  Template
		g.writeStringField ( 'template_name', 'andera-non-fcra' ) ;
		
		g.writeFieldName ( 'template_content' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'OneUnited Bank Template' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;

		//  Start of message		
		g.writeFieldName ( 'message' ) ;
		g.writeStartObject () ;

		//  Message contents
		g.writeStringField ( 'from_email', ONEUNITED_FROM_EMAIL ) ;
		g.writeStringField ( 'from_name', ONEUNITED_FROM_NAME ) ;
		
		//  To stuff, always an array!
		g.writeFieldName ( 'to' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'email', da.Email__c ) ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( ( da.Co_Applicant_Email__c != null ) && ( da.Co_Applicant_Email__c != '' ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'email', da.Co_Applicant_Email__c ) ;
				g.writeEndObject () ;
			}
		}
		
		g.writeEndArray () ;
		
		g.writeStringField ( 'subject', 'Update from OneUnited Bank' ) ;
		g.writeStringField ( 'subaccount', 'DEPOSITAPPLICATIONDENIAL' ) ;
		
		// GLOBAL Merge vars
		g.writeFieldName ( 'global_merge_vars' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:COMPANY' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:Description' ) ;
		g.writeStringField ( 'content', '' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;

		// LOCAL Merge vars
		g.writeFieldName ( 'merge_vars' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			String appIDMessageText = ' ' ;
			if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
				appIDMessageText = ' # ' + da.Confirmation_Number__c ;
			
			//  Applicant!
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', da.Email__c ) ;
			g.writeFieldName ( 'vars' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeApplicationID' ) ;
			g.writeStringField ( 'content', appIDMessageText ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'FirstName' ) ;
			g.writeStringField ( 'content', da.First_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'LastName' ) ;
			g.writeStringField ( 'content', da.Last_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'Applicant_Name' ) ;
			g.writeStringField ( 'content', da.First_Name__c + ' ' + da.Last_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeSalesforceApplicationID' ) ;
			g.writeStringField ( 'content', da.Id ) ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( ( da.Co_Applicant_Email__c != null ) && ( da.Co_Applicant_Email__c != '' ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'rcpt', da.Co_Applicant_Email__c ) ;
				g.writeFieldName ( 'vars' ) ;
				g.writeStartArray () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeApplicationID' ) ;
				g.writeStringField ( 'content', appIDMessageText ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'FirstName' ) ;
				g.writeStringField ( 'content', da.Co_Applicant_First_Name__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'LastName' ) ;
				g.writeStringField ( 'content', da.Co_Applicant_Last_Name__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeSalesforceApplicationID' ) ;
				g.writeStringField ( 'content', da.Id ) ;
				g.writeEndObject () ;
				g.writeEndArray () ;
				g.writeEndObject () ;
			}
		}
		g.writeEndArray () ;
		
		//  Campaign
		g.writeStringField ( 'google_analytics_campaign', 'Deposit Application Decline Message' ) ;
		
		//  Tags
		g.writeFieldName ( 'tags' ) ;
		g.writeStartArray () ;
		g.writeString ( 'welcome-email' ) ;
		g.writeEndArray () ;

		//  Googly
		g.writeFieldName ( 'google_analytics_domains' ) ;
		g.writeStartArray () ;
		g.writeString ( 'oneunited.com' ) ;
		g.writeString ( 'www.oneunited.com' ) ;
		g.writeEndArray () ;

		//  Recipient Meta data
		g.writeFieldName ( 'recipient_metadata' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', da.Email__c ) ;
			g.writeFieldName ( 'values' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'salesforceApplicationId', da.Id ) ;
			if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
				g.writeStringField ( 'AppId', da.Confirmation_Number__c ) ;
			if ( da.Tax_Reported_Contact__c != null )
				g.writeStringField ( 'salesforceContactId', da.Tax_Reported_Contact__c ) ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( ( da.Co_Applicant_Email__c != null ) && ( da.Co_Applicant_Email__c != '' ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'rcpt', da.Co_Applicant_Email__c ) ;
				g.writeFieldName ( 'values' ) ;
				g.writeStartArray () ;
				g.writeStartObject () ;
				g.writeStringField ( 'salesforceApplicationId', da.Id ) ;
				if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
					g.writeStringField ( 'AppId', da.Confirmation_Number__c ) ;
				if ( da.Co_Applicant_Tax_Reported_Contact__c != null )
					g.writeStringField ( 'salesforceContactId', da.Co_Applicant_Tax_Reported_Contact__c ) ;
				g.writeEndObject () ;
				g.writeEndArray () ;
				g.writeEndObject () ;
			}
		}
		
		g.writeEndArray () ;
		
		//  End of Message
		g.writeEndObject () ;
		
		//  End of object
		g.writeEndObject () ;
		
		System.debug ( '========== JSON OUTPUT START ==========' ) ;
		System.debug ( g.getAsString () ) ;
		System.debug ( '========== JSON OUTPUT END ==========' ) ;
		
        sendHTTPRequest ( g.getAsString () ) ;
		
        System.debug ( '========== Done! ==========' ) ;		
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  UNITY Visa Declined message
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void sendDepositApplicationFCRADenialMessage ( Deposit_Application__c [] daL )
	{
		System.debug ( '========== Creating JSON Object ==========' ) ;
		
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
		
		//  Create the object
		g.writeStartObject () ;
		
		//  API KEY
		g.writeStringField ( 'key', API_KEY ) ;
		
		//  Template
		g.writeStringField ( 'template_name', 'andera-decline-fcra' ) ;
		
		g.writeFieldName ( 'template_content' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'OneUnited Bank Template' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;

		//  Start of message		
		g.writeFieldName ( 'message' ) ;
		g.writeStartObject () ;

		//  Message contents
		g.writeStringField ( 'from_email', ONEUNITED_FROM_EMAIL ) ;
		g.writeStringField ( 'from_name', ONEUNITED_FROM_NAME ) ;
		
		//  To stuff, always an array!
		g.writeFieldName ( 'to' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'email', da.Email__c ) ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( ( da.Co_Applicant_Email__c != null ) && ( da.Co_Applicant_Email__c != '' ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'email', da.Co_Applicant_Email__c ) ;
				g.writeEndObject () ;
			}
		}
		
		g.writeEndArray () ;
		
		g.writeStringField ( 'subject', 'Update from OneUnited Bank' ) ;
		g.writeStringField ( 'subaccount', 'DEPOSITAPPLICATIONFCRADENIAL' ) ;
		
		// GLOBAL Merge vars
		g.writeFieldName ( 'global_merge_vars' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:COMPANY' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:Description' ) ;
		g.writeStringField ( 'content', '' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;

		// LOCAL Merge vars
		g.writeFieldName ( 'merge_vars' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			String appIDMessageText = ' ' ;
			if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
				appIDMessageText = ' # ' + da.Confirmation_Number__c ;
			
			//  Applicant!
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', da.Email__c ) ;
			g.writeFieldName ( 'vars' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeApplicationID' ) ;
			g.writeStringField ( 'content', appIDMessageText ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'FirstName' ) ;
			g.writeStringField ( 'content', da.First_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'LastName' ) ;
			g.writeStringField ( 'content', da.Last_Name__c ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeSalesforceApplicationID' ) ;
			g.writeStringField ( 'content', da.Id ) ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( ( da.Co_Applicant_Email__c != null ) && ( da.Co_Applicant_Email__c != '' ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'rcpt', da.Co_Applicant_Email__c ) ;
				g.writeFieldName ( 'vars' ) ;
				g.writeStartArray () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeApplicationID' ) ;
				g.writeStringField ( 'content', appIDMessageText ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'FirstName' ) ;
				g.writeStringField ( 'content', da.Co_Applicant_First_Name__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'LastName' ) ;
				g.writeStringField ( 'content', da.Co_Applicant_Last_Name__c ) ;
				g.writeEndObject () ;
				g.writeStartObject () ;
				g.writeStringField ( 'name', 'mergeSalesforceApplicationID' ) ;
				g.writeStringField ( 'content', da.Id ) ;
				g.writeEndObject () ;
				g.writeEndArray () ;
				g.writeEndObject () ;
			}
		}
		g.writeEndArray () ;
		
		//  Campaign
		g.writeStringField ( 'google_analytics_campaign', 'Deposit Application Decline Message' ) ;
		
		//  Tags
		g.writeFieldName ( 'tags' ) ;
		g.writeStartArray () ;
		g.writeString ( 'welcome-email' ) ;
		g.writeEndArray () ;

		//  Googly
		g.writeFieldName ( 'google_analytics_domains' ) ;
		g.writeStartArray () ;
		g.writeString ( 'oneunited.com' ) ;
		g.writeString ( 'www.oneunited.com' ) ;
		g.writeEndArray () ;

		//  Recipient Meta data
		g.writeFieldName ( 'recipient_metadata' ) ;
		g.writeStartArray () ;
		
		for ( Deposit_Application__c da : daL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', da.Email__c ) ;
			g.writeFieldName ( 'values' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'salesforceApplicationId', da.Id ) ;
			if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
				g.writeStringField ( 'AppId', da.Confirmation_Number__c ) ;
			if ( da.Tax_Reported_Contact__c != null )
				g.writeStringField ( 'salesforceContactId', da.Tax_Reported_Contact__c ) ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
			
			//  Co-applicant
			if ( ( da.Co_Applicant_Email__c != null ) && ( da.Co_Applicant_Email__c != '' ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'rcpt', da.Co_Applicant_Email__c ) ;
				g.writeFieldName ( 'values' ) ;
				g.writeStartArray () ;
				g.writeStartObject () ;
				g.writeStringField ( 'salesforceApplicationId', da.Id ) ;
				if ( ( da.Confirmation_Number__c != null ) && ( da.Confirmation_Number__c != '' ) )
					g.writeStringField ( 'AppId', da.Confirmation_Number__c ) ;
				if ( da.Co_Applicant_Tax_Reported_Contact__c != null )
					g.writeStringField ( 'salesforceContactId', da.Co_Applicant_Tax_Reported_Contact__c ) ;
				g.writeEndObject () ;
				g.writeEndArray () ;
				g.writeEndObject () ;
			}
		}
		
		g.writeEndArray () ;
		
		//  End of Message
		g.writeEndObject () ;
		
		//  End of object
		g.writeEndObject () ;
		
		System.debug ( '========== JSON OUTPUT START ==========' ) ;
		System.debug ( g.getAsString () ) ;
		System.debug ( '========== JSON OUTPUT END ==========' ) ;
		
        sendHTTPRequest ( g.getAsString () ) ;
		
        System.debug ( '========== Done! ==========' ) ;		
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Sends the email
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void sendHTTPRequest ( String js )
	{
		if ( das == null )
			das = deposit_application_settings__c.getInstance ( SETTINGS ) ;
		
    	if ( ( das != null ) && ( das.Email_Callout_Functionality__c != null ) && ( das.Email_Callout_Functionality__c == true ) )
    	{
			System.debug ( '========== Setting up HTTP Request Object ==========' ) ;
			HttpRequest request = new HttpRequest() ;
		
			request.setEndPoint ( 'https://mandrillapp.com/api/1.0/messages/send-template.json' ) ;
			request.setMethod ( 'POST' ) ;
			request.setHeader ( 'Content-Type', 'application/json' ) ;
			request.setBody ( js ) ;
	
		    System.debug ( '========== Calling method ==========' ) ;
		        
		    Http http = new Http () ;
		    HTTPResponse res = null ;
		    try
		    {
				res = http.send ( request ) ;
		    }
		    catch ( System.CalloutException e )
		    {
		     	System.debug ( 'HTTP Request fail. :(' ) ;
		    }
		
		    System.debug ( '========== RETURN START ==========' ) ;
		    if ( res != null )
				System.debug ( res.getBody () ) ;
	        System.debug ( '========== RETURN END ==========' ) ;
    	}
    	else
    	{
    		System.debug ( 'WTF Callout disabled, how did we get here?' ) ;
    	}
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - INFO
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendDepositApplicationAdditionalInfoEmails ( List<String> sId )
	{
		sendDepositApplicationEmails ( sId, null, null ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - Decline
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendDepositApplicationDenialEmails ( List<String> sId )
	{
		sendDepositApplicationEmails ( null, sId, null ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - FCRA Decline
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void sendDepositApplicationFCRADenialEmails ( List<String> sId )
	{
		sendDepositApplicationEmails ( null, null, sid ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails - COMBINED
	 *  ORDER - Additional, Denied
	 *  -------------------------------------------------------------------------------------------------- */	
	@future ( callout=true ) // <- MAJOR Headaches!
	public static void sendDepositApplicationEmails ( List<String> additionalId, List<String> denyId, List<String> fcraDenyId )
	{
		Deposit_Application__c [] daL = null ;
		
		/*  --------------------------------------------------------------------------------------------------
		 *   Additional Info Emails
		 *  -------------------------------------------------------------------------------------------------- */	
		if ( ( additionalId != null ) && ( additionalId.size () > 0 ) )
		{
			System.debug ( '========== 	DEPOSIT Additional Info Emails :: ' + additionalId.size () +' Found ==========' ) ;
			
            try
            {
            	daL = [
		    		SELECT ID, CreatedDate,
		    		Confirmation_Number__c, External_Key__c, 
		    		Application_Status__c, Additional_Information_Required__c,
		    		First_Name__c, Last_Name__c,
		    		Email__c, Tax_Reported_Contact__c, 
 					Co_Applicant_First_Name__c, Co_Applicant_Last_Name__c,		    		
		    		Co_Applicant_Email__c, Co_Applicant_Tax_Reported_Contact__c,
 					Beneficiary_First_Name__c, Beneficiary_Last_Name__c,		    		
		    		Beneficiary_Email__c, Beneficiary_Tax_Reported_Contact__c
		    		FROM Deposit_Application__c WHERE Id IN :additionalId
            	] ;
            }
            catch ( QueryException e )
            {
                System.debug ( '========== QUERY ERROR ==========' ) ;
                System.debug ( e ) ;
                System.debug ( '========== QUERY ERROR ==========' ) ;
            }
            
            if ( ( daL != null ) && ( daL.size () > 0 ) ) 
            {
				if ( das == null )
					das = deposit_application_settings__c.getInstance ( SETTINGS ) ;
				
		    	if ( ( das != null ) && ( das.Email_Callout_Functionality__c != null ) && ( das.Email_Callout_Functionality__c == true ) )
		    	{
	                System.debug ( '===== Sending ' + daL.size () + ' Additional Info Required Email(s) =====' ) ;
	                
	                sendDepositApplicationAdditionalInfoMessage ( daL ) ;
	                
	                System.debug ( '========== Updating Date sent ==========' ) ;
	                
	                List<Deposit_Application__c> oD = [
	                	SELECT ID
	                	FROM Deposit_Application__c 
	                	WHERE ID IN :additionalId 
	                ] ;
	                
	                if ( ( oD != null ) && ( od.size () > 0 ) )
	                {
	                    for ( Deposit_Application__c o : oD )
	                    {
	                        o.Last_Additional_Info_Email_Date__c = Date.today () ;
	                    }
	                    
	                    update oD ;
	                }
		    	}
		    	else
		    	{
		    		System.debug ( 'Additional Info Email - Callout disabled!' ) ;
		    	}
            }
		}
		else
		{
			System.debug ( '========== No Deposit Application Additional Info Emails Found ==========' ) ;
		}
		
		/*  --------------------------------------------------------------------------------------------------
		 *   Decline Emails
		 *  -------------------------------------------------------------------------------------------------- */	
		if ( ( denyId != null ) && ( denyId.size () > 0 ) )
		{
			System.debug ( '========== Deposit Application Denial Emails :: ' + denyId.size () +' Found ==========' ) ;
			
            try
            {
            	daL = [
		    		SELECT ID, CreatedDate, 
		    		Confirmation_Number__c, External_Key__c, 
		    		Application_Status__c, Additional_Information_Required__c,
		    		First_Name__c, Last_Name__c,
		    		Email__c, Tax_Reported_Contact__c, 
 					Co_Applicant_First_Name__c, Co_Applicant_Last_Name__c,		    		
		    		Co_Applicant_Email__c, Co_Applicant_Tax_Reported_Contact__c,
 					Beneficiary_First_Name__c, Beneficiary_Last_Name__c,		    		
		    		Beneficiary_Email__c, Beneficiary_Tax_Reported_Contact__c
		    		FROM Deposit_Application__c 
		    		WHERE Id IN :denyId
		    		AND Denial_Email_Sent__c = false
            	] ;
            }
            catch ( QueryException e )
            {
                System.debug ( '========== QUERY ERROR ==========' ) ;
                System.debug ( e ) ;
                System.debug ( '========== QUERY ERROR ==========' ) ;
            }
            
            if ( ( daL != null ) && ( daL.size () > 0 ) ) 
            {
				if ( das == null )
					das = deposit_application_settings__c.getInstance ( SETTINGS ) ;
				
		    	if ( ( das != null ) && ( das.Email_Callout_Functionality__c != null ) && ( das.Email_Callout_Functionality__c == true ) )
		    	{
	                System.debug ( '===== Sending ' + daL.size () + ' Deny Email(s) =====' ) ;
	                sendDepositApplicationDenialMessage ( daL ) ;
	                
	                System.debug ( '========== Flipping Denial Email Flag ==========' ) ;
	                
	                List<Deposit_Application__c> oD = [
	                	SELECT ID 
	                	FROM Deposit_Application__c 
	                	WHERE ID IN :denyId 
	                ] ;
	                
	                if ( ( oD != null ) && ( od.size () > 0 ) )
	                {
	                    for ( Deposit_Application__c o : oD )
	                    {
	                        o.Denial_Email_Sent__c = true ;
	                    }
	                    
	                    update oD ;
	                }
		    	}
		    	else
		    	{
		    		System.debug ( 'Decline Email - Callout disabled!' ) ;
		    	}
            }
		}
		else
		{
			System.debug ( '========== No Deposit Application Denial Emails Found ==========' ) ;
		}
		
		/*  --------------------------------------------------------------------------------------------------
		 *   FCRA Decline Emails
		 *  -------------------------------------------------------------------------------------------------- */	
		if ( ( fcraDenyId != null ) && ( fcraDenyId.size () > 0 ) )
		{
			System.debug ( '========== Deposit Application FCRA Denial Emails :: ' + fcraDenyId.size () +' Found ==========' ) ;
			
            try
            {
            	daL = [
		    		SELECT ID, CreatedDate, 
		    		Confirmation_Number__c, External_Key__c, 
		    		Application_Status__c, Additional_Information_Required__c,
		    		First_Name__c, Last_Name__c,
		    		Email__c, Tax_Reported_Contact__c, 
 					Co_Applicant_First_Name__c, Co_Applicant_Last_Name__c,		    		
		    		Co_Applicant_Email__c, Co_Applicant_Tax_Reported_Contact__c,
 					Beneficiary_First_Name__c, Beneficiary_Last_Name__c,		    		
		    		Beneficiary_Email__c, Beneficiary_Tax_Reported_Contact__c
		    		FROM Deposit_Application__c 
		    		WHERE Id IN :fcraDenyId
		    		AND Denial_Email_Sent__c = false
            	] ;
            }
            catch ( QueryException e )
            {
                System.debug ( '========== QUERY ERROR ==========' ) ;
                System.debug ( e ) ;
                System.debug ( '========== QUERY ERROR ==========' ) ;
            }
            
            if ( ( daL != null ) && ( daL.size () > 0 ) ) 
            {
				if ( das == null )
					das = deposit_application_settings__c.getInstance ( SETTINGS ) ;
				
		    	if ( ( das != null ) && ( das.Email_Callout_Functionality__c != null ) && ( das.Email_Callout_Functionality__c == true ) )
		    	{
	                System.debug ( '===== Sending ' + daL.size () + ' Deny Email(s) =====' ) ;
	                sendDepositApplicationFCRADenialMessage ( daL ) ;
	                
	                System.debug ( '========== Flipping Denial Email Flag ==========' ) ;
	                
	                List<Deposit_Application__c> oD = [
	                	SELECT ID 
	                	FROM Deposit_Application__c 
	                	WHERE ID IN :fcraDenyId 
	                ] ;
	                
	                if ( ( oD != null ) && ( od.size () > 0 ) )
	                {
	                    for ( Deposit_Application__c o : oD )
	                    {
	                        o.Denial_Email_Sent__c = true ;
	                    }
	                    
	                    update oD ;
	                }
		    	}
		    	else
		    	{
		    		System.debug ( 'Decline FCRA Email - Callout disabled!' ) ;
		    	}
            }
		}
		else
		{
			System.debug ( '========== No Deposit Application Denial Emails Found ==========' ) ;
		}
	}
}