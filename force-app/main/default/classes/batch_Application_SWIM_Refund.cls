global class batch_Application_SWIM_Refund implements Database.Batchable<SObject>, Database.Stateful
{
    //  Settings
	private Batch_Process_Settings__c bps ;
	private final String INITIAL_STATUS = 'Approved for Refund' ;
	
    //  Groups & RecordType
	private Map<String,String> groupMap ;
	private List<RecordType> rtL ;
	
	//  DEBIT
    global Integer dAccountCount ;
	global Decimal dAccountSum ;
	global String dTicketId ;
	
	//  ACH
    global Integer aAccountCount ;
	global Decimal aAccountSum ;
	global String aTicketId ;
	
	global batch_Application_SWIM_Refund ()
    {
        //  Custom settings
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
        
        //  Groups
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		//  Set up the record types we need for later
		List<String> rtNameList = new List<String> () ;
		rtNameList.add ( 'Credit Card Refund' ) ;
		rtNameList.add ( 'CS Batch File' ) ;
		
		rtL = OneUnitedUtilities.getRecordTypeList ( rtNameList ) ; 
        
        // Sums and crap
        dAccountCount = 0 ;
        dAccountSum = 0.0 ;
        
        aAccountCount = 0 ;
        aAccountSum = 0.0 ;
	}
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String recordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Credit Card Refund', 'Ticket__c' ) ;
		String query = '' ; // NO "NULL"!
		
		query += 'SELECT ID, ' ;
		query += 'Approved_Refund_Amount__c, Application_Refund_Source__c, Contact__c, ' ;
        query += 'Application__r.ID, Application__r.Funding_Options__c, Application__r.Application_ID__c, Application__r.Last_Name__c ' ;
		query += 'FROM Ticket__c ' ;
		query += 'WHERE Status__c = \'' + INITIAL_STATUS + '\'' ;
		if ( String.isNotBlank ( recordTypeId ) )
			query += 'AND RecordTypeId = \'' + recordTypeId + '\' ' ;
		query += 'AND SWM_Batch_Ticket__c = NULL ' ;
		query += 'AND Approved_Refund_Amount__c > 0 ' ;
		query += 'AND ACH_Refund_Exception__c = false ' ;

        query += '' ;
		
		return Database.getQueryLocator ( query ) ;
    }
    
	global void execute ( Database.Batchablecontext bc, List<Ticket__c> tL )
	{
        //  Ticket setup
        list<Ticket__c> aTL = new list<Ticket__c> () ;
        list<Ticket__c> dTL = new list<Ticket__c> () ;
        list<String> oL = new list<String> () ;
        
        //  Trackback
        map<ID,ID> appTicketLookupMap = new map<ID,ID> () ;
        
        //  SUM & Sort
		if ( tL.size () > 0 )
		{
			String groupId = groupMap.get ( 'Credit Card Batch Processing' ) ;
            
			for ( Ticket__c t : tL )
			{
                if ( String.isNotBlank ( t.Application__r.Funding_Options__c ) )
                {
                    if ( t.Application__r.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) )
                    {
                        aAccountCount ++ ;
                        aAccountSum += t.Approved_Refund_Amount__c ;
                        
                        aTL.add ( t ) ;
                    }
                    
                    if ( t.Application__r.Funding_Options__c.equalsIgnoreCase ( 'Debit Card' ) )
                    {
                        dAccountCount ++ ;
                        dAccountSum += t.Approved_Refund_Amount__c ;
                        
                        dTL.add ( t ) ;
                    }
                    
                    oL.add ( t.Application__r.ID ) ;
                    appTicketLookupMap.put ( t.Application__r.ID, t.ID ) ;
                }
			}
        }
        
        //  Select transactions
        list<Application_Transaction__c> oTL = null ;
        try
        {
            oTL = [
            	SELECT ID, Application__c
                FROM Application_Transaction__c
                WHERE Application__c IN :oL
                AND Status__c = 'Settled'
                AND SWIM_Settlement_Ticket__c != NULL
                AND SWIM_Refund_Ticket__c = NULL
                ORDER BY CreatedDate DESC
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        //  Transactions, parsing
        map<ID,Application_Transaction__c> atMap = new map<ID,Application_Transaction__c> () ;
        list<Application_Transaction__c> uTL = new list<Application_Transaction__c> () ;
        
        for ( Application_Transaction__c at : oTL )
        {
            if ( ! atMap.containsKey ( at.Application__c ) )
                atMap.put ( at.Application__c, at ) ;
            
            //  Update transaction with refund ticket for later lookup
			Application_Transaction__c nt = new Application_Transaction__c () ;
            nt.ID = at.ID ;
            nt.Refund_Ticket__c = appTicketLookupMap.get ( at.Application__c ) ;
            
            uTL.add ( nT ) ;
        }
        
        // ACH
        if ( ( aTL != null ) && ( aTL.size () > 0 ) )
        {
            aTicketId = createTicket ( aTL, aAccountCount, aAccountSum, 'ACH', atMap ) ;
        }
        
        // DEBIT
        if ( ( dTL != null ) && ( dTL.size () > 0 ) )
        {
            dTicketId = createTicket ( dTL, dAccountCount, dAccountSum, 'DEBIT', atMap ) ;
        }
        
        //  Update transactions
        if ( ( uTL != null ) && ( uTL.size () > 0 ) )
            update uTL ;
    }
    
    //  Create batch ticket, update associated tickets and create attachment.
    private ID createTicket ( list<Ticket__c> tL, Integer count, Decimal sum, String type, map<ID,Application_Transaction__c> atMap )
    {
        String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;
        
        COCCSWIMHelper csh = new COCCSWIMHelper () ;
        csh.setCashboxFromSettings () ;
        
        for ( Ticket__c t : tL )
        {
            one_Application__c o = t.Application__r ;
            
            String fixName = o.Last_Name__c.replace ( '\'', '-' ) ;
            String description = 'REFUND VISA ' + o.Application_ID__c + ' -- ' + fixName ;
            if ( o.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) && ( t.Application_Refund_Source__c.equalsIgnoreCase ( 'Holding' ) ) )
	            csh.createSWIMDoubleRecord ( bps.ACH_GL_Account__c, bps.ACH_Outgoing_Account__c, t.Approved_Refund_Amount__c, o.Application_ID__c, description ) ;
            else if ( o.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) && ( t.Application_Refund_Source__c.equalsIgnoreCase ( 'Collateral' ) ) )
	            csh.createSWIMDoubleRecord ( bps.Collateral_GL_Account__c, bps.ACH_Outgoing_Account__c, t.Approved_Refund_Amount__c, o.Application_ID__c, description ) ;
            else if ( o.Funding_Options__c.equalsIgnoreCase ( 'Debit Card' ) )
	            csh.createSWIMDoubleRecord ( bps.Collateral_GL_Account__c, bps.UNITY_Visa_ePay_GL_Account__c, t.Approved_Refund_Amount__c, o.Application_ID__c, description ) ;
        }
        
        //  File name
        Datetime dt = System.now () ;
        String fileName = '1218_UVREFUND' + type + '_' + dt.format ( 'yyyyMMddHHmmss' ) + '.SWM' ;
        
        //  Create the overall ticket
        Ticket__c batchTicket = new Ticket__c () ;
        
        batchTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
        batchTicket.Status__c = 'New' ;
        batchTicket.Batch_Account_Total__c = count ;
        batchTicket.Batch_Amount_Total__c = sum ;
        batchTicket.ownerId = groupMap.get ( 'Credit Card Batch Processing' ) ;
        batchTicket.Is_Batch_Refund__c = true ;
        batchTicket.Batch_Type__c = 'SWM ' + type ;
        batchTicket.Batch_File_Name__c = fileName ;
        batchTicket.Description__c = 'BATCH UNITY Visa SWIM Refund' ;
        
        insert batchTicket ;
        
        //  Attachment ?!
        Attachment a = new Attachment () ;
        a.ParentId = batchTicket.ID ;
        a.Name = fileName ;
        a.ContentType = 'Text/txt' ;
        a.Body = Blob.valueOf ( csh.getSWIMFile ().escapeHTML4 () ) ;
        
        insert a ;
        
        List<Ticket__c> bTL = new List<Ticket__c> () ;
        List<Application_Transaction__c> atL = new List<Application_Transaction__c> () ;
        
        for ( Ticket__c t : tL )
        {
            t.SWM_Batch_Ticket__c = batchTicket.id ;

            if ( groupId != null )
                t.OwnerId = groupId ;            
            
            bTL.add ( t ) ;
            
            if ( atMap.containsKey ( t.Application__c ) )
            {
            	Application_Transaction__c ot = atMap.get ( t.Application__c ) ;
                
                Application_Transaction__c nt = new Application_Transaction__c () ;
                nt.ID = ot.ID ;
                nt.SWIM_Refund_Ticket__c = batchTicket.ID ;
                atL.add ( nt ) ;
            }
        }
        
        if ( ( bTL != null ) && ( bTL.size () > 0 ) )
	        update bTL ;
        
        if ( ( atL != null ) && ( atL.size () > 0 ) )
	        update atL ;
        
        return batchTicket.id ;
    }
    
   	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
      	OrgWideEmailAddress o = [
      		SELECT ID
      		FROM OrgWideEmailAddress
      		WHERE Address = 'customersupport@oneunited.com'
      	] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage cMail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c,
				'helpdesk@OneUnited.com' 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email,
				'helpdesk@OneUnited.com' 
			} ;
		}
   
		cMail.setToAddresses ( toAddresses ) ;
		cMail.setSubject ( 'Batch SWM File Job Results :: ' + a.Status ) ;
		
		if ( o != null )
			cMail.setOrgWideEmailAddressId ( o.Id ) ;
		
		String message = '' ;
		message += '<h1>Results from creating the newest refund SWM batch file:</h1>' ;
		message += '<p>The batch processed ' + a.TotalJobItems +' batches ' ;
		if ( a.NumberOfErrors > 0 )
			message += 'with '+ a.NumberOfErrors + ' failures.' ;
		message += '<br/></p>' ;
		
		//  ACH
        if ( ( aAccountCount > 0 ) && ( aAccountSum > 0.0 ) && ( String.isNotBlank ( aTicketId ) ) )
		{
            message += '<p>There are ' + aAccountCount + ' ACH refund(s) totalling ' + formatMoney ( aAccountSum ) + ' in the batch.</p>' ;
            
			System.URL sURL = URL.getSalesforceBaseURL () ;
			String urly = sURL.toExternalForm () + '/' + aTicketId ;
			message += '<p>You can see the ACH batch at:</p>' ;
			message += '<a href="'+ urly + '">' + urly + '</a>' ;
		}
        else
        {
            message += '<p>There are no new ACH refunds in the batch.</p>' ;
        }
   
		//  DEBIT
        if ( ( dAccountCount > 0 ) && ( dAccountSum > 0.0 ) && ( String.isNotBlank ( dTicketId ) ) )
		{
            message += '<p>There are ' + dAccountCount + ' Debit refund(s) totalling ' + formatMoney ( dAccountSum ) + ' in the batch.</p>' ;
            
			System.URL sURL = URL.getSalesforceBaseURL () ;
			String urly = sURL.toExternalForm () + '/' + dTicketId ;
			message += '<p>You can see the debit card batch at:</p>' ;
			message += '<a href="'+ urly + '">' + urly + '</a>' ;
		}
        else
        {
            message += '<p>There are no new debit card refunds in the batch.</p>' ;
        }
   
   		cMail.setPlainTextBody ( '' ) ;  // WTB no "null"
		cMail.setHtmlBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { cMail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { cMail } ) ;
			}
		}
	}
	
	private String formatMoney ( Decimal x )
	{
		Decimal dollars;
		Decimal cents;
		dollars = x.intValue();
		cents = x - x.intValue();
		cents = cents.setScale(2);  // it is possible to store repeating decimals in sfdc currency fields…I found out the hard way.
		String amtText = '$' + dollars.format() + cents.toPlainString().substring(1) ;
		return amtText ;		
	}
}