public without sharing virtual class OneUnitedWebDMLHelper
{
    protected OneUnitedWebDMLHelper() {}

    @TestVisible private static OneUnitedWebDMLHelper instance = null;

    public static OneUnitedWebDMLHelper getInstance () 
    {
        if (instance == null) 
        {
            instance = new OneUnitedWebDMLHelper () ;
        }
        return instance ;
    }
    
	public void insertObject ( SObject o )
    {
        list<SObject> oL = new list<SObject> () ;
        oL.add ( o ) ;
        
        insertObjects ( oL ) ;
    }
    
    public void insertObjects ( list<SObject> oL )
    {
        insert oL ;
    }

    public void updateObject ( SObject o )
    {
        list<SObject> oL = new list<SObject> () ;
        oL.add ( o ) ;
        
        updateObjects ( oL ) ;
    }
    
    public void updateObjects ( list<SObject> oL )
    {
        update oL ;
    }
}