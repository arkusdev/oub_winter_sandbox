public class ContactHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Double mapping tracking
	 *  -------------------------------------------------------------------------------------------------- */	
	public static map <String, boolean> closedSurveyMap ; 
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Generic checker
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean checkDuplicate ( Map<String,boolean> xMap, String id )
	{
		boolean returnValue = false ;
		
		if ( xMap.containsKey ( id ) == true )
			returnValue = true ;
		else
			xMap.put ( id, true ) ;
		
        System.debug ( 'Returning : ' + returnValue ) ;
        
		return returnValue ;
	}
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Record types
	 *  -------------------------------------------------------------------------------------------------- */
    private static string customerRTID ;
    
    public static string getCustomerRTID ()
    {
        if ( customerRTID == null )
            customerRTID = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Customer').RecordTypeId ;
        
        return customerRTID ;
    }
    
    private static ID automatedEmailRTID ;
    
    public static ID getAutomatedEmailRTID ()
    {
        if ( automatedEmailRTID == null )
            automatedEmailRTID = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        
        return automatedEmailRTID ;
    }

	/*  --------------------------------------------------------------------------------------------------
	 *  Survey
	 *  -------------------------------------------------------------------------------------------------- */
    private static ID surveyID = null ;

	public static ID getSurveyID ()
    {
        if ( surveyID == null )
        {
            try
            {
                oneSurvey__c s = [
                    SELECT ID
                    FROM oneSurvey__c
                    WHERE Name = 'Closed Account Survey'
                    AND Active__c = true
                    LIMIT 1
                ] ;
                
	            surveyID = s.ID ;
            }
            catch ( Exception e )
            {
                // ?
            }
        }
        
        return surveyID ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
     * DE-FAULT
	 *  -------------------------------------------------------------------------------------------------- */
    private static ID alsDefaultID = null ;
    
    public static ID getAdvocateDefaultID () 
    {
        if ( alsDefaultID == null )
        {
            AdvocateLevelSetting__mdt als = null ;
            
            try
            {
                als = [ 
                    SELECT AdvocateId__c 
                    FROM AdvocateLevelSetting__mdt 
                    WHERE DeveloperName='Default' 
                    LIMIT 1 
                ] ;
            }
            catch ( Exception e )
            {
                als = null ;
            }
            
            if ( als != null )
                alsDefaultID = als.AdvocateId__c ;
        }
        
        return alsDefaultID ;
    }

    /* -----------------------------------------------------------------------------------
     * GONE?!?
     * ----------------------------------------------------------------------------------- */
    private static ID alQuitID = null ;
    
    public static ID getAdvocateQuitID () 
    {
        if ( alQuitID == null )
        {
            Advocate_Level__c al = null ;
            
            try
            {
                al = [ 
                    SELECT ID 
                    FROM Advocate_Level__c 
                    WHERE Name='Non Participant' 
                    LIMIT 1 
                ] ;
            }
            catch ( Exception e )
            {
                al = null ;
            }
            
            if ( al != null )
                alQuitID = al.ID ;
        }
        
        return alQuitID ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Campaigns for stuffing people into
     * ----------------------------------------------------------------------------------- */
    private static map<String,OneCampaign__c> ocM = null ;
    
    public static map<String,OneCampaign__c> getCampaignMap ()
    {
        if ( ocM == null )
        {
            ocM = new map<String,OneCampaign__c> () ;
            
            list<OneCampaign__c> ocL = new list<OneCampaign__c> () ;
            
            try
            {
                ocL = [
                    SELECT ID, Name
                    FROM OneCampaign__c
                    WHERE Active__c = true
                ] ;
            }
            catch ( DMLException e )
            {
                ocL = null ;
            }
            
            if ( ( ocL != null ) && ( ocL.size () > 0 ) )
            {
                for ( OneCampaign__c oc : ocL )
                {
                    ocM.put ( oc.name.toLowerCase (), oc ) ;
                }
            }
            else
            {
                ocM = null ;
            }
        }
        
        return ocM ;
    }
}