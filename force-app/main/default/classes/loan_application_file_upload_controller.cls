public with sharing class loan_application_file_upload_controller
{
    /*  ----------------------------------------------------------------------------------------
     *  Security
     *  ---------------------------------------------------------------------------------------- */
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public loan_application__c o 
 	{ 
 		get; 
 		private set; 
 	}
 	
 	//  QR Code to allow site transfer to mobile device
 	public String qrCode 
 	{
 		get; 
 		private set; 
 	}
 	
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
    
    public string uploadFileName
    {
        get ;
		set ;            
    }
 
	//  Step control for panels on page
	public Integer flStepNumber
	{
		get ;
		private set ;
	}

	//  List for document type drop down
    public List<SelectOption> docTypeItemDropDown
    {
        get
        {
	        return loan_application_utils.docTypeItems ;
        }
        
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeListDisplay
    {
        get ;
		private set ;
    }
    
    // Count of needed documents
    public Integer docTypeListSize
    {
    	get
    	{
    		Integer dtl = 0 ;
    		
    		if ( docTypeList != null )
    			dtl = docTypeList.size () ;
    			
    		return dtl ;
    	}
    	private set ;
    }

    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public loan_application_file_upload_controller ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		flStepNumber = 0 ;
		
        //  First check for application in session
        o = (loan_application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
 
 		//  If no session present, use querystring instead
        if ( ( o == null ) || ( o.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
			
			o = null ;

			//  Both need to be passed in
			if ( ( aid != null ) && ( kid != null ) )
			{
                list<loan_application__c> oL = null ;
                
				try
                {
                    oL = [ 
                        SELECT ID, Application_ID__c, Name, Additional_Information_Required__c, Additional_Info_Special_Instructions__c, Loan_ID__c, Upload_Attachment_key__c
                        FROM loan_application__c 
                        WHERE ID = :aid
                        AND Upload_Attachment_key__c = :kid
                        ORDER BY ID
                        LIMIT 1
                    ] ;
				}
				catch ( DmlException e )
				{
					//  Defaults are already set
					for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
					{
				        System.debug ( e.getDmlMessage ( i ) ) ;
					}				
			    }
                
                if ( ( oL != null ) && ( oL.size () > 0 ) )
                {
					//  Grab the first one
					o = oL [ 0 ] ;
                }
				else
				{
		            System.debug ( '========== No Data found for ID & Key ==========' ) ;
				}
			}
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = o.Id ;
			kid = o.Upload_Attachment_Key__c ;
		}
        
		//  Check again for things existing
		if ( ( o != null ) && ( aid != null ) && ( kid != null ) )
		{
            System.debug ( '========== Valid application found ==========' ) ;
            
 			//  Set up QR Code, need the full URL
			qrCode = loan_application_utils.getDocUploadQRCode ( aid, kid, 125, 125 ) ;

            System.debug ( 'INFO?? ' + o.Additional_Information_Required__c ) ;

            //  Additional documentation, old vs new
			if ( o.Additional_Information_Required__c != null )
            {
                //  New way, stored in picklist
				docTypeList = loan_application_utils.getAdditionalInfoNeeded ( o.Additional_Information_Required__c ) ;
                docTypeListDisplay = loan_application_utils.getAdditionalInfoNeededDISPLAY ( o.Additional_Information_Required__c ) ;
            }
			
			if ( docTypeList != null )
			{
/*                
				//  Load attachments separately
				Attachment[] aL = null ;
				
				try
				{
					aL = [
						SELECT Name 
						FROM Attachment 
						WHERE ParentID = :aid 
						] ;
				}
				catch ( Exception e )
				{
					System.debug ( e.getMessage () ) ;
				}
				
				//  Check the attachments (if any!)
				if ( aL != null )
				{
					for ( Attachment oA : aL )
					{
						Integer j = 0 ;
						while ( j < docTypeList.size () )
						{
							if ( docTypeList [j].equalsIgnoreCase ( oA.Name ) )
								docTypeList.remove ( j ) ;
                            
							j++ ;
						}
                        
						j = 0 ;
						while ( j < docTypeListDisplay.size () )
						{
							if ( docTypeListDisplay [j].containsIgnoreCase ( oA.Name ) )
								docTypeListDisplay.remove ( j ) ;
                            
							j++ ;
						}
					}
                    
                    System.debug ( 'Final DocTypeList Size = ' + docTypeList.size () ) ;
				}
*/				
				//  Set the step number
				if ( ( docTypeList != null ) && ( docTypeList.size () > 0 ) )
                {
					flStepNumber = 1000 ;
                }
                else if ( ( docTypeList != null ) && ( docTypeList.size () == 0 ) )
                {
                    flStepNumber = 2000 ;
                }
			}
			else
			{
				System.debug ( 'No documents found for this code! :: "' + o.Additional_Information_Required__c + '" ?' ) ;
			}
        }
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Validate & Upload the file
     *  ---------------------------------------------------------------------------------------- */
	public PageReference upload () 
	{
        // Defaults
		boolean isValid = true ;
		errorName = false ;
		errorFile = false ;
		
		//  Start with validation
		if ( String.isBlank ( attachment.name ) )
		{
			errorName = true ;
			isValid = false ;
		}
		
		if ( attachment.body == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        flStepNumber = 1002 ;
			return null ;
		}
		
		attachment.OwnerId = UserInfo.getUserId () ;
		attachment.ParentId = o.Id ;    // the record the file is attached to
		attachment.IsPrivate = false ;  // Or the guest user can't query it!
        
        if ( String.isNotBlank ( uploadFileName ) )
        {
            list<String> thingy = uploadFileName.split ( '\\.' ) ;
            attachment.name += '.' + thingy.get ( thingy.size () -1 ) ;
        }
        
        if ( String.isNotBlank ( uploadFileName ) )
	        attachment.Description = uploadFileName + ' - ' + attachment.Description ; // add the file name so we don't lose it
 
		try 
		{
	        System.debug ( '========== Inserting Attachment ==========' ) ;
			access.insertObject ( attachment ) ;
			
	        System.debug ( '========== Flipping application status ==========' ) ;
	        o.Application_Processing_Status__c = 'Documentation Received' ;
	        o.Additional_Information_Required__c = removeFoundDoc ( o.Additional_Information_Required__c, attachment.name ) ;
            
            access.updateObject ( o ) ;
		}
		catch (DMLException e)
		{
			System.debug ( '========== Error Inserting! ==========' ) ;

			return null;
		}
		finally 
		{
	        System.debug ( '========== Final stuff ==========' ) ;
	        
	        //  Set panel to success upload
			flStepNumber = 1001 ;
            
            list<String> thingy = attachment.name.split ( '\\.' ) ;
			
			//  Remove document from list
			Integer j = 0 ;
			while ( j < docTypeList.size () )
			{
				if ( docTypeList [j].equalsIgnoreCase ( thingy.get ( 0 ) ) )
					docTypeList.remove ( j ) ;
                
				j++ ;
			}
            
            j = 0 ;
			while ( j < docTypeListDisplay.size () )
			{
				if ( docTypeListDisplay [j].containsIgnoreCase ( thingy.get ( 0 ) ) )
					docTypeListDisplay.remove ( j ) ;
                
				j++ ;
			}
			
			//  If all documents are gone, go to overall success
			if ( docTypeList.size () == 0 )
				flStepNumber = 2000 ;
			
			//  Clear errors if any
			errorName = false ;
    		errorFile = false ;
            
            //  Reset attachment
			attachment = new Attachment () ; 
		}
		
		return null;
	}

    /*  ----------------------------------------------------------------------------------------
     *  Clear docs as they're added
     *  ---------------------------------------------------------------------------------------- */
	private String removeFoundDoc ( String docList, String doc )
	{
		String returnList = '' ;
		
		list<String> dL = docList.split ( ';' ) ;
		list<String> xL = new list<String> () ;
        
        list<String> thingy = doc.split ( '\\.' ) ;
		
		for ( String s : dL )
		{
			if ( ! s.equalsIgnoreCase ( thingy.get ( 0 ) ) )
				xL.add ( s ) ;
		}
		
		boolean start = true ;
		for ( String s : xL )
		{
			if ( start )
				start = false ;
			else
				returnList = returnList + ';' ;
				
			returnList = returnList + s ;
		}
		
		return returnList ;
	}
}