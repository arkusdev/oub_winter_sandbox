global class batch_DepositApplication_Update implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	
    global batch_DepositApplication_Update ()
    {
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query ;
		query = '' ;
		query += 'SELECT ID ' ;
		query += 'FROM Deposit_Application__c ' ;
		query += 'WHERE ' ;
		query += '( Application_Status__c <> \'DECLINED\' AND Application_Status__c <> \'APPROVED\' AND Application_Status__c <> \'VOID\' ) ' ;
		query += 'AND ' ;
		query += '( Last_Updated__c = NULL OR Last_Updated__c < YESTERDAY ) ' ;
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}

	global void execute ( Database.Batchablecontext bc, list<Deposit_Application__c> daL )
	{
		System.debug ( '------------------ BATCH EXCUTE START ------------------' ) ;
		
		if ( ( daL != null ) && ( daL.size () > 0 ) )
		{
			for ( Deposit_Application__c da : daL )
			{
				da.Last_Updated__c = Date.today () ;
			}
			
			try
			{
				update daL ;
			}
			catch ( DMLException e )
			{
				System.debug ( 'Exception updating :: ' + e.getMessage () ) ;
			}
		}
		else
		{
			System.debug ( 'No deposit applications found to update.' ) ;
		}
		
		System.debug ( '------------------ BATCH EXCUTE END ------------------' ) ;
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Deposit Application Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}