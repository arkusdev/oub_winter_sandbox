public with sharing class ApplicationOverride 
{
    /*  --------------------------------------------------------------------------------------------------
	 *  UGH
	 *  -------------------------------------------------------------------------------------------------- */	
    class AppData
    {
        public one_application__c app ;
        public map<String,boolean> ov ;
    }

	/*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */	
    public ApplicationOverride ()
    {
        // Empty
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Setup
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static one_application__c getApplication ( ID appID )
    {
        one_application__c app = one_utils.getApplicationFromId ( appID ) ;
        
        return app ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Override Calculations - ALL OF THEM!
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static map<String,boolean> getOverrides ( one_application__c app )
    {
        map<String,boolean> oMap = new map<String,boolean> () ;
        oMap.put ( 'canDecline', false ) ;
        
		//        
        //  Check for LOS
        //  
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
	        oMap.put ( 'isLOS', true ) ;
        else
	        oMap.put ( 'isLOS', false ) ;
	        
		//
        //  Minfraud, Setup
        //
        oMap.put ( 'mfAppOverride', false ) ;
        
		//
        //  Minfraud, Application
        //
        if ( ( String.isNotBlank ( app.minFraud_Disposition__c ) ) &&
            ( app.minFraud_Disposition__c.equalsIgnoreCase ( 'MANUAL_REVIEW' ) ) &&
            ( app.minFraud_Override_Datetime__c == null ) &&
            ( app.minFraud_Override_User__c == null ) &&
            ( ! app.Application_Status__c.equalsIgnoreCase ( 'DECLINED' ) ) )
        {
            //  Always decline
            oMap.put ( 'canDecline', true ) ;
            
            //  Do they have a valid ID to override?
            if ( String.isNotBlank ( app.minFraud_ID__c ) )
            {
		        oMap.put ( 'mfAppOverride', true ) ;
            }
        }
        
		//
        //  OUB, Setup
        //
        oMap.put ( 'oubAppOverride', false ) ;
        
		//
        //  OUB, Application
        //
        if ( ( String.isNotBlank ( app.OUB_Fraud_Disposition__c ) ) &&
            ( ( app.OUB_Fraud_Disposition__c.equalsIgnoreCase ( 'WARN' ) ) || ( app.OUB_Fraud_Disposition__c.equalsIgnoreCase ( 'NONE' ) ) ) &&
            ( app.OUB_Fraud_Override_Datetime__c == null ) &&
            ( app.OUB_Fraud_Override_User__c == null ) )
        {
	        oMap.put ( 'oubAppOverride', true ) ;
            oMap.put ( 'canDecline', true ) ;
        }
        
		//
        //  OFAC, Setup
        //
        oMap.put ( 'ofacAppOverride', false ) ;
        oMap.put ( 'ofacCoAppOverride', false ) ;
        oMap.put ( 'ofacBothOverride', false ) ;

        //
        //  OFAC, Applicant
        //
        if ( ( String.isNotBlank ( app.OFAC_Decision__c ) ) &&
            ( app.OFAC_Decision__c.equalsIgnoreCase ( 'FAIL' ) ) &&
            ( app.OFAC_Override_Datetime__c == null ) &&
            ( app.OFAC_Override_User__c == null ) )
        {
            //  Do they have a valid ID to override?
            if ( ( String.isNotBlank ( app.OFAC_Transaction_ID__c ) ) &&
                ( ! app.OFAC_Transaction_ID__c.equalsIgnoreCase ( '0' ) ) )
            {
		        oMap.put ( 'ofacAppOverride', true ) ;
            }
            else
            {
		        oMap.put ( 'canDecline', true ) ;
            }
        }
        
        //
        //  OFAC, Co-Applicant
        //
        if ( ( app.Number_of_Applicants__c.equalsIgnoreCase ( 'I will be applying jointly with another person' ) ) &&
            ( String.isNotBlank ( app.OFAC_CoApplicant_Decision__c ) ) &&
            ( app.OFAC_CoApplicant_Decision__c.equalsIgnoreCase ( 'FAIL' ) ) &&
            ( app.OFAC_CoApp_Override_Datetime__c == null ) &&
            ( app.OFAC_CoApp_Override_User__c == null ) )
        {
            //  Do they have a valid ID to override?
            if ( ( String.isNotBlank ( app.OFAC_CoApplicant_Transaction_ID__c ) ) &&
                ( ! app.OFAC_CoApplicant_Transaction_ID__c.equalsIgnoreCase ( '0' ) ) )
            {
		        oMap.put ( 'ofacCoAppOverride', true ) ;
            }
            else
            {
		        oMap.put ( 'canDecline', true ) ;
            }
        }
        
		//
        //  OFAC, Both
        //
        if ( oMap.get ( 'ofacAppOverride' ) && oMap.get ( 'ofacCoAppOverride' ) )
            oMap.put ( 'ofacBothOverride', true ) ;
        
		//
        //  OFAC, Decline
        //
        if ( oMap.get ( 'ofacAppOverride' ) || oMap.get ( 'ofacCoAppOverride' ) )
            oMap.put ( 'canDecline', true ) ;

        //
        //  IDV, Setup
        //
        oMap.put ( 'idvAppOverride', false ) ;
        oMap.put ( 'idvCoAppOverride', false ) ;
        oMap.put ( 'idvBothOverride', false ) ;

        //
        //  IDV, Applicant
        //
        if ( ( String.isNotBlank ( app.IDV_Decision__c ) ) &&
            ( ( app.IDV_Decision__c.equalsIgnoreCase ( 'FAIL' ) ) || ( app.IDV_Decision__c.equalsIgnoreCase ( 'NONE' ) ) ) &&
            ( app.IDV_Override_Datetime__c == null ) &&
            ( app.IDV_Override_User__c == null ) )
        {
            //  Do they have a valid ID to override?
            if ( ( String.isNotBlank ( app.IDV_Transaction_ID__c ) ) &&
                ( ! app.IDV_Transaction_ID__c.equalsIgnoreCase ( '0' ) ) )
            {
		        oMap.put ( 'idvAppOverride', true ) ;
            }
            else
            {
		        oMap.put ( 'canDecline', true ) ;
            }
        }
        
		//
        //  IDV, Co-Applicant
        //
        if ( ( app.Number_of_Applicants__c.equalsIgnoreCase ( 'I will be applying jointly with another person' ) ) &&
            ( String.isNotBlank ( app.IDV_CoApplicant_Decision__c ) ) &&
            ( ( app.IDV_CoApplicant_Decision__c.equalsIgnoreCase ( 'FAIL' ) ) || ( app.IDV_CoApplicant_Decision__c.equalsIgnoreCase ( 'NONE' ) ) ) &&
            ( app.IDV_CoApplicant_Override_Datetime__c == null ) &&
            ( app.IDV_CoApplicant_Override_User__c == null ) )
        {
            //  Do they have a valid ID to override?
            if ( ( String.isNotBlank ( app.IDV_CoApplicant_Transaction_ID__c ) ) &&
                ( ! app.IDV_CoApplicant_Transaction_ID__c.equalsIgnoreCase ( '0' ) ) )
            {
		        oMap.put ( 'idvCoAppOverride', true ) ;
            }
            else
            {
		        oMap.put ( 'canDecline', true ) ;
            }
        }
        
		//
        //  IDV, Both
        //
        if ( oMap.get ( 'idvAppOverride' ) && oMap.get ( 'idvCoAppOverride' ) )
            oMap.put ( 'idvBothOverride', true ) ;
        
		//
        //  IDV, Decline
        //
        if ( oMap.get ( 'idvAppOverride' ) || oMap.get ( 'idvCoAppOverride' ) )
            oMap.put ( 'canDecline', true ) ;

        //
        //  Qualfile, Setup
        //
        oMap.put ( 'qfAppOverride', false ) ;
        oMap.put ( 'qfCoAppOverride', false ) ;
        oMap.put ( 'qfBothOverride', false ) ;

        //
        //  QF, Applicant
        //
        if ( ( String.isNotBlank ( app.QualiFile_Decision__c ) ) &&
            ( app.QualiFile_Decision__c.equalsIgnoreCase ( 'Review' ) ) &&
            ( app.Qualifile_Override_User__c == null ) &&
            ( app.Qualifile_Override_Datetime__c == null ) )
        {
            oMap.put ( 'qfAppOverride', true ) ;
        }
        else
        {
            oMap.put ( 'canDecline', true ) ;
        }

		//
        //  QF, Co-Applicant
        //

        if ( ( app.Number_of_Applicants__c.equalsIgnoreCase ( 'I will be applying jointly with another person' ) ) &&
            ( String.isNotBlank ( app.QualiFile_CoApp_Decision__c ) ) &&
            ( app.QualiFile_CoApp_Decision__c.equalsIgnoreCase ( 'Review' ) ) &&
            ( app.Qualifile_CoApplicant_Override_User__c == null ) &&
            ( app.Qualifile_CoApplicant_Override_Datetime__c == null ) )
        {
            oMap.put ( 'qfCoAppOverride', true ) ;
        }
        else
        {
            oMap.put ( 'canDecline', true ) ;
        }

		//
        //  QF, Both
        //
        if ( oMap.get ( 'qfAppOverride' ) && oMap.get ( 'qfCoAppOverride' ) )
            oMap.put ( 'qfBothOverride', true ) ;

        //
        //  QF, Decline
        //
        if ( oMap.get ( 'qfAppOverride' ) || oMap.get ( 'qfCoAppOverride' ) )
            oMap.put ( 'canDecline', true ) ;
        
        //
        //  Photo, Setup
        //
        oMap.put ( 'photoAppOverride', false ) ;
        
        //
        //  Photo, Applicant
        //
        if ( ( String.isNotBlank ( app.Photo_ID_Decision__c ) ) &&
            ( ( app.Photo_ID_Decision__c.equalsIgnoreCase ( 'WARN' ) ) || ( app.Photo_ID_Decision__c.equalsIgnoreCase ( 'NONE' ) ) ) &&
            ( app.Photo_ID_Override_Datetime__c == null ) &&
            ( app.Photo_ID_Override_User__c == null ) )
        {
	        oMap.put ( 'photoAppOverride', true ) ;
            oMap.put ( 'canDecline', true ) ;
        }

        //
        //  Return
        //
        return oMap ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Minfraud override
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void runMinfraudOverride ( one_application__c app, String overrideReason )
    {
        if ( String.isNotEmpty ( overrideReason ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  Override MinFRaud
            fish.overrideMinFraud ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline
     *  Minfraud = D050
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationMinfraud ( one_application__c app, String declineReason )
    {
        declineApplication ( app, declineReason, 'D050' ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  OUB override
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void runOUBOverride ( one_application__c app, String overrideReason )
    {
        if ( String.isNotEmpty ( overrideReason ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideOUBVelocity ( overrideReason ) ;
                        
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline
     *  OUB = ?!? (It depends)
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationOUB ( one_application__c app, String declineReason )
    {
        //  Replace to ... something else?
        String newCode = app.FIS_Decision_Code__c.replace ( 'D', 'F' ) ;
        
        declineApplication ( app, declineReason, newCode ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  OFAC override
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void runOFACOverride ( one_application__c app, String overrideReason, String whichApp )
    {
        if ( whichApp.equalsIgnoreCase ( 'A' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        else if ( whichApp.equalsIgnoreCase ( 'C' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            fish.setCoApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;

            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        else if ( whichApp.equalsIgnoreCase ( 'B' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;
            
            //  run the co-applicant
            fish.setCoApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline
     *  OFAC = D004
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationOFAC ( one_application__c app, String declineReason )
    {
        declineApplication ( app, declineReason, 'D004' ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  IDV override
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void runIDVOverride ( one_application__c app, String overrideReason, String whichApp )
    {
        if ( whichApp.equalsIgnoreCase ( 'A' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideIDV ( false, overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        else if ( whichApp.equalsIgnoreCase ( 'C' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            fish.setCoApplicant () ;
            fish.overrideIDV ( false, overrideReason ) ;

            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        else if ( whichApp.equalsIgnoreCase ( 'B' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideIDV ( false, overrideReason ) ;
            
            //  run the co-applicant
            fish.setCoApplicant () ;
            fish.overrideIDV ( false, overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline
     *  IDV = D006
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationIDV ( one_application__c app, String declineReason )
    {
        declineApplication ( app, declineReason, 'D006' ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  QF override
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void runQFOverride ( one_application__c app, String overrideReason, String whichApp )
    {
        if ( whichApp.equalsIgnoreCase ( 'A' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        else if ( whichApp.equalsIgnoreCase ( 'C' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            fish.setCoApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;

            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        else if ( whichApp.equalsIgnoreCase ( 'B' ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  run the co-applicant
            fish.setCoApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline
     *  QF = D911
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationQF ( one_application__c app, String declineReason )
    {
        declineApplication ( app, declineReason, 'D911' ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Minfraud override
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void runPhotoOverride ( one_application__c app, String overrideReason )
    {
        if ( String.isNotEmpty ( overrideReason ) )
        {
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  Override Photo
            fish.setApplicant () ;
            fish.overrideOUBPhoto ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline
     *  Photo = D501
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationPhoto ( one_application__c app, String declineReason )
    {
        declineApplication ( app, declineReason, 'D501' ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Generic Decline
     *  -------------------------------------------------------------------------------------------------- */
    private static void declineApplication ( one_application__c app, String declineReason, String decisionCode )
    {
        try
        {
            app.Decline_Reason__c = declineReason ;
            app.FIS_Decision_Code__c = decisionCode ;
            System.debug('Update app debugging 7:');
            update app ;
        }
        catch ( Exception e )
        {
            System.debug ( 'WTF?!?' ) ;
        }
    }
}