@isTest
public with sharing class TestUNITYVisaPassthrough 
{
	static testmethod void testController ()
	{
		Test.startTest () ;
		
		//  Testing...
		UNITYVisaPassthrough uvp = new UNITYVisaPassthrough () ;
		
		uvp.go () ;
		
		Test.stopTest () ;
	}
}