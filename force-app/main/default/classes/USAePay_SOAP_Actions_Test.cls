@isTest
public class USAePay_SOAP_Actions_Test {
    
    testMethod static void test1(){
        
        USAePay_SOAP_Actions usaepayActions = new USAePay_SOAP_Actions('Settings_Unity_Visa');
        
        usaepayActions.customer.billingAddress.city = 'Test City';
        usaepayActions.customer.billingAddress.country = 'USA';
        usaepayActions.customer.billingAddress.email = 'test@email.com';
        usaepayActions.customer.billingAddress.firstName = 'Test';
        usaepayActions.customer.billingAddress.lastName =  'Test';
        usaepayActions.customer.billingAddress.phone = '1234554321';
        usaepayActions.customer.billingAddress.state = 'TE';
        usaepayActions.customer.billingAddress.street = 'Test'; 
        usaepayActions.customer.billingAddress.zip = '12345';
        
        usaepayActions.customer.amount =  String.valueOf(250);
        USAePay_SOAP_Actions.ResponseObject response = usaepayActions.addCustomer();
        
        System.assert(response.statusCode == 200);
        System.assert(response.response == '1111111');
        
        usaepayActions.customer.custNum = response.response;
        usaepayActions.paymentMethod.custNum = response.response;
        usaepayActions.paymentMethod.methodName = 'Debit Deposit';
        usaepayActions.paymentMethod.secondarySort = '1';
        usaepayActions.paymentMethod.cardExpiration = '0979';
        usaepayActions.paymentMethod.cardCode = '999';
        usaepayActions.paymentMethod.cardNumber = '1111111111111111';
        response = usaepayActions.addCustomerPaymentMethod();
        
        System.assert(response.statusCode == 200);
        System.assert(response.response == '111');
        usaepayActions.paymentMethod.methodID = response.response; 
        
        usaepayActions.transactionParameters.billingAddress = usaepayActions.customer.billingAddress;
        usaepayActions.transactionParameters.cardExpiration = '0979';
        usaepayActions.transactionParameters.cardCode = '999';
        usaepayActions.transactionParameters.cardNumber = '1111111111111111';
        usaepayActions.transactionParameters.amount = String.valueOf(250);
        
        response = usaepayActions.runTransaction();
        response = usaepayActions.runCustomerTransaction();
        System.assertEquals(response.statusCode, 200);
        System.assertEquals(response.response, '3254239166'); 
        
        //debit cards errors
        usaepayActions.paymentMethod.cardExpiration = '0910';
        response = usaepayActions.addCustomerPaymentMethod();
        System.assert(response.status == 'DEBIT_CARD_INFO_FORMAT_ERROR');
        usaepayActions.paymentMethod.cardExpiration = '01' + String.valueOf(System.today().year()).substring(2);
        response = usaepayActions.addCustomerPaymentMethod();
        System.assert(response.status == 'DEBIT_CARD_INFO_FORMAT_ERROR');
        usaepayActions.transactionParameters.cardExpiration = '0910';
        response = usaepayActions.runTransaction();
        System.assert(response.status == 'DEBIT_CARD_INFO_FORMAT_ERROR');
        usaepayActions.transactionParameters.cardExpiration = '01' + String.valueOf(System.today().year()).substring(2);
        response = usaepayActions.runTransaction();
        System.assert(response.status == 'DEBIT_CARD_INFO_FORMAT_ERROR');
        
    }
    
    testMethod static void test2(){
        
        USAePay_SOAP_Actions usaepayActions = new USAePay_SOAP_Actions('Settings_Unity_Visa');
        
        USAePay_SOAP_Actions.ResponseObject response = usaepayActions.deleteCustomer();
        
        System.assert(response.statusCode == 200);
        
        response = usaepayActions.deleteCustomerPaymentMethod();
        
        System.assert(response.statusCode == 200);
        response = usaepayActions.voidTransaction();
        
        System.assert(response.statusCode == 200);  
        
        response = usaepayActions.refundTransaction();
        
        System.assert(response.statusCode == 200);
        System.assert(response.response == '11111111');
        
        Application_Transaction__c apt = new Application_Transaction__c();
        apt.Customer_Number__c = '132456424';
        apt.Payment_Method_ID__c = 'dfgadhsfdr34';
        apt.Amount__c = 200;
        insert apt;
        
        application_refund_transaction controller = new application_refund_transaction(new ApexPages.StandardController(apt));
        controller.amount = '200';
        Test.setCurrentPageReference(new PageReference('Page.application_refund_transaction')); 
		System.currentPageReference().getParameters().put('transactionId', 'hgjhkdjsha455');
        controller.refund();
        
        application_new_transaction_controller controller2 = new application_new_transaction_controller(new ApexPages.StandardController(apt));
        
        controller2.createTransactionBasedOnPreviousPaymentMethod();
        //shouldn't be able to create now
        controller2.createTransactionBasedOnPreviousPaymentMethod();
        controller2.cancel();
        
        usaepayActions.testCoverageForSendRequest();
        
    }
    
}