global class batch_ACHFileTicket implements Database.Batchable<SObject>, Database.Stateful
{
	private Map<String,String> groupMap ;
	private List<RecordType> rtL ;
	private Batch_Process_Settings__c bps ;
	
	global batch_ACHFileTicket ()
	{
		//  Settings for what to do with the batch
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
		
		//  Set up the groups we need for later
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		//  Set up the record types we need for later
		
		List<String> rtNameList = new List<String> () ;
		rtNameList.add ( 'Credit Card Refund' ) ;
		rtNameList.add ( 'CS Batch File' ) ;
		
		rtL = OneUnitedUtilities.getRecordTypeList ( rtNameList ) ; 
	}

	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query ;
		
		query  = 'SELECT ' ;
		query += 't.ID, t.Approved_Refund_Amount__c ' ;
		query += 'FROM Ticket__c t ' ;
		query += 'WHERE t.ACH_Batch_Ticket__c = NULL ' ;
		query += 'AND t.SWM_Batch_Ticket__r.Batch_Type__c = \'SWM\' ' ;
		query += 'AND t.SWM_Batch_Ticket__r.Status__c = \'Processed\' ' ;
		query += 'AND t.Approved_Refund_Amount__c > 0 ' ;
		query += 'AND t.ACH_Refund_Exception__c = false ' ;
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Ticket__c> tL )
	{
		String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;
		
		if ( tL.size () > 0 )
		{
			Integer accountCount = 0 ;
			double accountSum = 0.0 ;
			
			for ( Ticket__c t : tL )
			{
				accountCount ++ ;
				accountSum += t.Approved_Refund_Amount__c ;
			}
			
			Ticket__c batchTicket = new Ticket__c () ;
			
			batchTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
			batchTicket.Status__c = 'New' ;
			batchTicket.Batch_Account_Total__c = accountCount ;
			batchTicket.Batch_Amount_Total__c = accountSum ;
			batchTicket.ownerId = groupMap.get ( 'Credit Card Batch Processing' ) ;
			batchTicket.Batch_Type__c = 'ACH' ;
			
			insert batchTicket ;
			
			List<Ticket__c> bT = new List<Ticket__c> () ;
			for ( Ticket__c t : tL )
			{
				t.ACH_Batch_Ticket__c = batchTicket.id ;
				
				if ( groupId != null )
					t.OwnerId = groupId ;
					
				bT.add ( t ) ;
			}
			
			update bT ;
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Batch ACH File Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}