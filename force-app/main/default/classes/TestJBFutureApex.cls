public class TestJBFutureApex 
{
    @future ( callout=true ) // <- Headaches!
	public static void goEmail ( String cID )
    {
        Contact c = [ 
            SELECT ID, FirstName, LastName, Email
            FROM Contact 
            WHERE ID = :cID
            LIMIT 1
        ] ;
        
        Automated_Email__C ae = new Automated_Email__C () ;
        ae.First_Name__c = c.FirstName ;
        ae.Last_Name__c = c.LastName ;
        ae.Email_Address__c = c.Email ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Template_Name__c = 'deposit-application-thanks-for-applying' ;
        ae.application_id__c = '123456' ;
        ae.Contact__c = c.ID ;
        ae.Contact_OR_Account__c = 'Contact' ;
        
        insert ae ;
        
        TestJBFutureQueue q = new TestJBFutureQueue ( c.ID ) ;
        System.enqueueJob(q);
    }
}