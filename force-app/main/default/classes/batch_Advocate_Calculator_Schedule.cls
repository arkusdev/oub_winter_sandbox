global class batch_Advocate_Calculator_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
        batch_Advocate_Calculator b = new batch_Advocate_Calculator () ;
        Database.executeBatch ( b ) ;
   }
}