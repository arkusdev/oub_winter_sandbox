public with sharing class SmartMoneyWorkshop 
{
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    /*  ----------------------------------------------------------------------------------------
     *  Page Type variable
     *  ---------------------------------------------------------------------------------------- */
    public Integer eventType { get; set; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Page input variables
     *  ---------------------------------------------------------------------------------------- */
    public String cEmail { get; set; }
    
    public Workshop_Attendee__c wa { get; set; }
    public String eventDate { get; set; }
    public String eventStart { get; set; }
    public String eventEnd { get; set; }

    public String howDidYouHear { get; set; }
    
    public List<SelectOption> getItems ()
    {
    	List<SelectOption> options = new List<SelectOption> () ;
    	options.add ( new SelectOption ( 'Please Select', '' ) ) ;
    	options.add ( new SelectOption ( 'Email', 'Email' ) ) ;
    	options.add ( new SelectOption ( 'Google', 'Google' ) ) ;
    	options.add ( new SelectOption ( 'Other Website', 'Other Website' ) ) ;
    	options.add ( new SelectOption ( 'Newspaper or Magazine', 'Newspaper or Magazine' ) ) ;
    	options.add ( new SelectOption ( 'Radio', 'Radio' ) ) ;
    	options.add ( new SelectOption ( 'Family Member or Friend', 'Family Member or Friend' ) ) ;
    	options.add ( new SelectOption ( 'Bank Employee', 'Bank Employee' ) ) ;
    	options.add ( new SelectOption ( 'Facebook', 'Facebook' ) ) ;
    	options.add ( new SelectOption ( 'Instagram', 'Instagram' ) ) ;
    	options.add ( new SelectOption ( 'Twitter', 'Twitter' ) ) ;
    	options.add ( new SelectOption ( 'Black Lives Matter', 'Black Lives Matter' ) ) ;
    	options.add ( new SelectOption ( 'The Killer Mike Challenge', 'The Killer Mike Challenge') ) ;
    	options.add ( new SelectOption ( 'Other', 'Other' ) ) ;
    	
    	return options ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Tracking stuff
     *  ---------------------------------------------------------------------------------------- */
    public String utmCampaign { get; set; }
    public String utmContent { get; set; }
    public String utmMedium { get; set; }
    public String utmSource { get; set; }
    public String utmTerm { get; set; }
    public String utmVisitorId { get; set; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Internal variables and controls
     *  ---------------------------------------------------------------------------------------- */
   	public Integer fStepNumber { get; private set; }
   	
    public Workshop__c w { get ; private set ; }
   	private List<Workshop__c> wL { private get; private set; } 
   	
   	private RecordType rt { private get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public Boolean e_firstName { get; private set ; }
    public Boolean e_lastName { get; private set; }
    public Boolean e_email1 { get; private set; }
    public Boolean e_emailDiff { get; private set; }
    public Boolean e_phone { get; private set; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Confirmation
     *  ---------------------------------------------------------------------------------------- */
    public String confirmationNumber { get; private set; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public SmartMoneyWorkshop ( ApexPages.Standardcontroller stdC )
	{
		//  Get the recordtype out of the settings object
		String crt = Web_Settings__c.getInstance ( 'Web Forms' ).Workshop_Contact_Record_Type__c ;
		
		//  Load the record type for assignment
		rt = [ SELECT ID FROM RecordType WHERE Name=:crt AND SobjectType = 'Contact' ] ;
		
		//  Assume not found until proven otherwise
		fStepNumber = 1500 ;
		wL = new List<Workshop__c> () ;
		
		//  Incoming date, time and location
		String wid = ApexPages.currentPage().getParameters().get('wid') ;
		
		System.debug ( 'WID: ' + wid ) ;
		
		//  Confirm variables passed in
		if ( String.isNotBlank ( wid ) ) 
		{
			//  Check for event
			wL = [
				SELECT ID, 
				Title__c, Description__c,
				Event_Start__c, Event_End__c,
				Event_Start_Date__c, Event_Start_Time__c, 
				Event_Sponsorship__c, Type__c,
				Branch_Name__c, Address__c,
				Branch__r.State_CD__c,
				Bank_Event_Photo__c,
				Third_Party_Event_Host__c,
				Third_Party_Event_Location__c,
				Third_Party_Event_Link__c
				FROM Workshop__c
				WHERE ID = :wid
			] ;
			
			//  Event exists!
			if ( ( wL != null ) && ( wL.size () > 0 ) )
			{
				//  Grab the first one, set for output
				w = wL.get ( 0 ) ;
				
				Branch__c b = w.Branch__r ;
				
				//  Set up display date/times
				if ( w.Event_Start__c != null )
				{
					eventStart = getTimeBasedOnBranch ( w.Event_Start__c, b ) ;
					eventDate = w.Event_Start__c.format ( 'MM/dd/yyyy' ) ;
				}
				
				if ( w.Event_End__c != null )
				{
					eventEnd = getTimeBasedOnBranch ( w.Event_End__c, b ) ;
				}
				
				//  Activate the initial panel
				fStepNumber = 1000 ;
				
				//  Set the page type
				setPageType ( w.Type__c ) ;
				
				//  Initialize variable
				wa = new Workshop_Attendee__c () ;
			}
		}
	}

    /*  ----------------------------------------------------------------------------------------
     *  Submit the form
     *  ---------------------------------------------------------------------------------------- */
	public PageReference submit () 
	{
		//  Reset step number
		fStepNumber = 1000 ;
		
		//  Reset list
		e_firstName = false ;
	    e_lastName = false ;
	    e_email1 = false ;
	    e_emailDiff = false ;
	    e_phone = false ;
		
		//  Error check
		if ( String.isEmpty ( wa.First_Name__c ) )
		{
			e_firstName = true ;
			fStepNumber = 1001 ;
		}

		if ( String.isEmpty ( wa.Last_Name__c ) )
		{
			e_lastName = true ;
			fStepNumber = 1001 ;
		}
		
		if ( ( ! String.isEmpty ( wa.Email_Address__c ) ) && ( ! validateEmail ( wa.Email_Address__c ) ) )
		{
			e_email1 = true ;
			fStepNumber = 1001 ;
		}
		
		if ( ( ! String.isEmpty ( wa.Email_Address__c ) ) && ( ( ! String.isEmpty ( cEmail ) ) ) && ( ! cEmail.equalsIgnoreCase ( wa.Email_Address__c ) ) )
		{
			e_emailDiff = true ;
			fStepNumber = 1001 ;
		}
		
		if ( String.isEmpty ( wa.Phone_Number__c ) )
		{
			e_phone = true ;
			fStepNumber = 1001 ;
		}
		else
		{
	        String PhoneRegEx = '(^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$)';
	        Pattern PhonePattern = Pattern.compile ( PhoneRegEx ) ;
	        Matcher PhoneMatcher;
	        
			PhoneMatcher = PhonePattern.matcher ( wa.Phone_Number__c ) ;
			
			if ( ! PhoneMatcher.matches () )
			{
				e_phone = true ;
				fStepNumber = 1001 ;
			}
			else
			{
				wa.Phone_Number__c = formatPhone ( wa.Phone_Number__c ) ;
			}
		}
		
		//  Processing logic if no errors
		if ( fStepNumber != 1001 )
		{
			System.debug ( 'No Errors, proceding to logic' ) ;
			
			System.debug ( 'UTM1 "' + utmCampaign + '"' ) ;
			System.debug ( 'UTM2 "' + utmContent + '"' ) ;
			System.debug ( 'UTM3 "' + utmMedium + '"' ) ;
			System.debug ( 'UTM4 "' + utmSource + '"' ) ;
			System.debug ( 'UTM5 "' + utmTerm + '"' ) ;
			System.debug ( 'UTM6 "' + utmVisitorId + '"' ) ;
			
			Contact c = OneUnitedUtilities.lookupContact ( wa.Email_Address__c ) ;
			
			if ( c != null )
			{
				System.debug ( 'Found Contact(s) - updating!' ) ;
				
				boolean isUpdate = false ;
				
				if ( String.isNotBlank ( utmCampaign ) )
				{
					c.UTM_Campaign__c = utmCampaign ;
					isUpdate = true ;
				}
				
				if ( String.isNotBlank ( utmContent ) )
				{
					c.UTM_Content__c = utmContent ;
					isUpdate = true ;
				}
				
				if ( String.isNotBlank ( utmMedium ) )
				{
					c.UTM_Medium__c = utmMedium ;
					isUpdate = true ;
				}
				
				if ( String.isNotBlank ( utmSource ) )
				{
					c.UTM_Source__c = utmSource ;
					isUpdate = true ;
				}
				
				if ( String.isNotBlank ( utmTerm ) )
				{
					c.UTM_Term__c = utmTerm ;
					isUpdate = true ;
				}
				
				if ( String.isNotBlank ( utmVisitorId ) )
				{
					c.UTM_VisitorID__c = utmVisitorId ;
					isUpdate = true ;
				}
				
				//  Only update if UTM passed in
				if ( isUpdate )
				{
					System.debug ( 'Updating contact with UTM' ) ;
					//update c ;
                    access.updateObject(c);
				}
			}
			else
			{
				System.debug ( 'Create special contact!' ) ;
				
				c = new Contact () ;
				c.FirstName = wa.First_Name__c ;
				c.LastName = wa.Last_Name__c ;
				
				if ( String.isNotBlank ( wa.Email_Address__c ) )
					c.Email = wa.Email_Address__c ;
				
				c.Phone = wa.Phone_Number__c ;
				
				c.Referred_By__c = howDidYouHear ;
				c.LeadSource = 'OUB Website Workshop' ;
				
				if ( String.isNotBlank ( utmCampaign ) )
				{
					c.UTM_Campaign__c = utmCampaign ;
				}
				
				if ( String.isNotBlank ( utmContent ) )
				{
					c.UTM_Content__c = utmContent ;
				}
				
				if ( String.isNotBlank ( utmMedium ) )
				{
					c.UTM_Medium__c = utmMedium ;
				}
				
				if ( String.isNotBlank ( utmSource ) )
				{
					c.UTM_Source__c = utmSource ;
				}
				
				if ( String.isNotBlank ( utmTerm ) )
				{
					c.UTM_Term__c = utmTerm ;
				}
				
				if ( String.isNotBlank ( utmVisitorId ) )
				{
					c.UTM_VisitorID__c = utmVisitorId ;
				}
				
				if ( String.isNotBlank ( rt.ID ) ) 
					c.RecordTypeID = rt.ID ;
				
				//insert c ;
                access.insertObject(c);
			}
			
			System.debug ( 'Contact ID = ' + c.ID ) ;
			System.debug ( 'Creating workshop attendee' ) ;
			
			//  Update workshop attendee
			wa.Contact__c = c.ID ;
			wa.Referred_By__c = howDidYouHear ;
			wa.Workshop_ID__c = w.ID ;
			
			//insert wa ;
            access.insertObject(wa);
			
			//  Set default blank
			confirmationNumber = null ;
			
			//  Re-select record to get the number
			System.debug ( 'Getting confirmation number' ) ;
			if ( wa != null )
			{
				Workshop_Attendee__c wa2 = [ SELECT ID, Name FROM Workshop_Attendee__c WHERE ID = :wa.ID ] ;
				
				if ( wa2 != null )
				{
					confirmationNumber = wa2.Name ;
				}
			}
			
			System.debug ( '# - ' + confirmationNumber ) ;
			
			//  Go to final panel!
			fStepNumber = 2000 ;
		}
		
		//  Done!
		return null ;
	}
	
	private boolean validateEmail ( String email )
	{
		//  Pattern email addresses must match
		Pattern p = Pattern.compile ( '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))');
		
	    Matcher m = p.matcher ( email ) ;

		//  Assume valid
		boolean isValid = true ;

		if ( ! m.matches () ) 
		{
			//  Doesn't match, not a valid email address
			isValid = false ;
		}
		else
		{
			//  Garbage in check
			isValid = isValidEmail ( email ) ;
		}
		
		return isValid ;
	}
	
	private String formatPhone ( String phone )
	{
		if(phone == null) return '';
		
		Set<String> nums = new Set<String>{'+', '0','1','2','3','4','5','6','7','8','9'};
		String c = '';
		String nphone = ' ';
		Integer i = 0;
		for(i = 0; i < phone.length(); i++){
			c = phone.substring(i, i + 1);
			if(nums.contains(c)){
				nphone += c;
			}
		}
		
		nphone = nphone.trim();
		return nphone;
	}
	
	private boolean isValidEmail ( String email )
	{
		//  Ignore these email addresses as obvious fakes
		if ( email.equalsIgnoreCase ( 'none@none.com' ) )
			return false ;
			
		if ( email.equalsIgnoreCase ( 'foo@bar.com' ) )
			return false ;
			
		if ( email.contains ( 'none@' ) )
			return false ;
		
		if ( email.contains ( 'noemail@' ) )
			return false ;
		
		if ( email.contains ( '@noemail.' ) )
			return false ;
		
		if ( email.contains ( '@none.' ) )
			return false ;
			
		if ( email.contains ( 'not_provided' ) )
			return false ;
			
		return true ;
	}
	
	private String getTimeBasedOnBranch ( Datetime dt, Branch__c b )
	{
		String foo = '' ;
		
		String state = b.State_CD__c ;
		
		if ( String.isNotBlank ( b.State_CD__c ) )
		{
    		if ( state.equalsIgnoreCase ( 'MA' ) )
	    		foo = dt.format ( 'hh:mm a', 'America/New_York' ) ;
    		else if ( state.equalsIgnoreCase ( 'CA' ) )
	    		foo = dt.format ( 'hh:mm a', 'America/Los_Angeles' ) ;
    		else if ( state.equalsIgnoreCase ( 'FL' ) )
	    		foo = dt.format ( 'hh:mm a', 'America/New_York' ) ;
		}
		else
		{
			foo = dt.format ( 'hh:mm a' ) ;
		}
		
		return foo ;
	}
	
	private void setPageType ( String thingy )
	{
		//  DEFAULT
		eventType = 1000 ;
		
		if ( String.isNotBlank ( thingy ) )
		{
			if ( thingy.equalsIgnoreCase ( 'At Branch, Third Party, Registration Required' ) )
				eventType = 2000 ;
			else if ( thingy.equalsIgnoreCase ( 'At Branch, Third Party, Registration Not Required' ) )
				eventType = 3000 ;
			else if ( thingy.equalsIgnoreCase ( 'Not At Branch, Third Party, Registration Required' ) )
				eventType = 4000 ;
			else if ( thingy.equalsIgnoreCase ( 'Not At Branch, Third Party, Registration Not Required' ) )
				eventType = 5000 ;
			else if ( thingy.equalsIgnoreCase ( 'Not At Branch, Third Party, CLOSED' ) )
				eventType = 6000 ;
		}
	}
}