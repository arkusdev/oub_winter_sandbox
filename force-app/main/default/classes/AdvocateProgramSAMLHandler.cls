global class AdvocateProgramSAMLHandler implements Auth.SamlJitHandler
{
	/* --------------------------------------------------------------------------------
	 *  Globals
	 * -------------------------------------------------------------------------------- */
    private final String PROFILE_NAME = 'OneUnited Advocate Apps User' ;
    
	/* --------------------------------------------------------------------------------
	 * Handle user and contact
	 * -------------------------------------------------------------------------------- */
    private void handleJit ( Boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion ) 
    {
        /*
        System.debug ( '------------------------- Various? ----------------------' ) ;
        System.debug ( 'Provider   : ' + samlSsoProviderId ) ;
        System.debug ( 'Community  : ' + communityId ) ;
        System.debug ( 'Portal     : ' + portalId ) ;
        System.debug ( 'Federation : ' + federationIdentifier ) ;
        System.debug ( 'Assertion  : ' + assertion ) ;
        
        System.debug ( '------------------------- attributes? ----------------------' ) ;
        
        for ( String x : attributes.keySet() )
        {
            system.debug ( x + ' --> ' + attributes.get ( x ) ) ;
        }
        
        System.debug ( '------------------------- END ----------------------' ) ;
		*/
        
/*      for ( String x : attributes.keySet () )
        {
            if ( x.equalsIgnoreCase ( 'KEEP_ALIVE_URL' ) )
            {
				Cache.OrgPartition orgPart = Cache.Org.getPartition ( 'local.Community' ) ;
                String thingy = EncodingUtil.convertToHex ( Crypto.generateDigest ( 'SHA1', Blob.valueOf ( federationIdentifier + 'keepAliveURL' ) ) ) ;
				orgPart.put ( thingy, attributes.get ( x ) ) ;
            }
        } */
        
        Contact c = null ;
        
        if ( String.isNotBlank ( federationIdentifier ) )
        {
            try
            {
                list<Contact> cL = [
                    SELECT ID, FirstName, LastName, TaxID__c,
                    Phone, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode
                    FROM Contact
                    WHERE TaxID__c = :federationIdentifier
                ] ;
                
                if ( ( cL != null ) && ( cL.size () > 0 ) )
                    c = cL [ 0 ] ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
        else
        {
            System.debug ( 'No Federation ID passed in, can not proceed.' ) ;
        }
        
        if ( c != null ) 
        {
            Profile p = null ;
            
            if ( create )
            {
                try
                {
                    List<Profile> pL = [
                        SELECT ID
                        FROM Profile
                        WHERE Name = :PROFILE_NAME
                    ] ;
                    
                    if ( ( pL != null ) && ( pL.size () > 0 ) )
                        p = pL [ 0 ] ;
                }
                catch ( Exception e )
                {
                    p = null ;
                }
                
                if ( p != null )
                    u.ProfileId = p.ID ;
                
                u.LocaleSidKey = 'en_US' ;
                u.EmailEncodingKey = 'UTF-8' ;
                u.LanguageLocaleKey = 'en_US' ;
                u.TimeZoneSidKey = 'GMT' ;
                
                u.FederationIdentifier = federationIdentifier ;
                u.ContactId = c.ID ;
            
                String alias = '';
                if ( c.FirstName == null ) 
                    alias = c.LastName;
                else 
                    alias = c.FirstName.charAt(0) + c.LastName ;
    
                if ( alias.length() > 5 )
                    alias = alias.substring(0, 5);
                
                u.Alias = alias;
                
                try
                {
                    list<Referral_Code__c> rL = [
                        SELECT Name , CreatedDate
                        FROM Referral_Code__c
                        WHERE Contact__c  = :c.ID
                        ORDER BY CreatedDate DESC
                    ] ;
                    
                    if ( ( rL != null ) && ( rL.size () > 0 ) )
                        u.CommunityNickname = rL [ 0 ].Name ;
                    else
                		u.CommunityNickname = c.FirstName + c.LastName.charAt(0) ;
                    
                    list<User> uL = [
                        SELECT ID, CommunityNickname
                        FROM User
                        WHERE CommunityNickname = :u.CommunityNickname
                    ] ;
                    
                    if ( ( uL != null ) && ( uL.size () > 0 ) )
                    {
                		u.CommunityNickname = c.FirstName + c.LastName.charAt(0) + one_utils.getRandomString ( 4 ) ;
                        
                        String x = u.CommunityNickname ;
                        if ( x.length () > 40 )
                            u.CommunityNickname = x.left ( 40 ) ;
                    }

                }
                catch ( DMLException e )
                {
                	u.CommunityNickname = c.FirstName + c.LastName.charAt(0) ;
                   
                    // ?
                }
                
                
                if ( String.isNotBlank ( c.Email ) )
	                u.Username = c.email + '.' + c.ID + '.communityuser' ;
                else
                    u.Username = c.FirstName + c.LastName + '@' + c.ID + '.communityuser' ;
            }
            
            u.IsActive = true ;
            
            u.FirstName = c.FirstName ;
            u.LastName = c.LastName ;
            
            u.Street = c.MailingStreet ;
            u.City = c.MailingCity ;
            u.State = c.MailingState ;
            u.PostalCode = c.MailingPostalCode ;

            if ( String.isNotBlank ( c.Email ) )
	            u.Email = c.Email ;
            
            u.Phone = c.Phone ;
        }
        
        if ( ! create )
        {
            try
            {
                update u ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
    
	/* --------------------------------------------------------------------------------
	 * Merge the two and check for a user regardless
	 * -------------------------------------------------------------------------------- */
    private User xUser ( Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion )
    {
        User u = null ;
        boolean create = false ;
        
        try
        {
         	list<User> uL = [
                SELECT ID, FederationIdentifier, ProfileID
                FROM User
                WHERE FederationIdentifier = :federationIdentifier
            ] ;
            
            if ( ( uL != null ) && ( uL.size () > 0 ) )
                u = uL [ 0 ] ;
        }
        catch ( Exception e )
        {
            u = null ;
        }
        
        if ( u == null )
        {
            u = new User () ;
            create = true ;
        }
        
        handleJit ( create, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion ) ;
        
        return u ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Required for implement
	 * -------------------------------------------------------------------------------- */
    global User createUser ( Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion ) 
    {
		User u = xUser ( samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion ) ;
        
        return u ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Required for implement
	 * -------------------------------------------------------------------------------- */
	global void updateUser ( Id userId, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion ) 
    {
        //  Yes, we are ignoring the user ID !
		xUser ( samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion ) ;
	}
}