@isTest (SeeAllData=true)
public with sharing class Test_one_application_batch_download 
{
	static testmethod void downloadSWIMTest ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test11' ;
		c1.LastName = 'User11' ;
		c1.Email = 'test11@user11.com' ;
		
		insert c1 ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c1.Id ;
		fa.CREDITLIMITAMT__c = 1000.00 ;
		
		insert fa ;
		
		one_application__c o = new one_application__c () ;
		
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		
		o.Contact__c = c1.Id;
		
		o.First_Name__c = c1.FirstName ;
		o.Last_Name__c = c1.LastName ;
		o.Email_Address__c = c1.Email ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		
		o.Referred_By_Contact__c = c1.Id;
		o.Entered_By_Contact__c = c1.Id;

		o.FIS_Application_ID__c = '111222333' ;
		o.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		o.FIS_Decision_Code__c = 'F003' ;
		
		o.Funding_Account_Number__c = '12345679' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.Funding_Routing_Number__c = '123456789' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		
		o.Trial_Deposit_1__c = 7 ;
		o.Trial_Deposit_2__c = 11 ;
		o.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
		
		insert o ;
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		Map<String,String> groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Move to Refund' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		t.Application__c = o.Id ;
		t.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		t.OwnerId = groupId ;
		t.Application_Refund_Source__c = 'Holding' ;
		t.Card_Blocked__c = true ;
		
		insert t ;
		
		String batchTicketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
		String batchGroupId = groupMap.get ( 'Credit Card Batch Processing' ) ;

		Ticket__c bt = new Ticket__c () ;
		bt.Status__c = 'New' ;
		bt.recordTypeId = batchTicketRecordTypeId ;
		bt.Batch_Type__c = 'SWM' ;
		bt.ownerId = batchGroupId ;
		
		insert bt ;
		
		t.SWM_Batch_Ticket__c = bt.id ;
		
		update t ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_batch_download ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( bT ) ;
        
		//  Feed the standard controller to the page controller
		one_application_batch_download tv = new one_application_batch_download ( sc ) ;
		
		tv.goDownload () ;
		
		Test.stopTest() ;
	}

	static testmethod void downloadACHTest ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test11' ;
		c1.LastName = 'User11' ;
		c1.Email = 'test11@user11.com' ;
		
		insert c1 ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c1.Id ;
		fa.CREDITLIMITAMT__c = 1000.00 ;
		
		insert fa ;
		
		one_application__c o = new one_application__c () ;
		
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		
		o.Contact__c = c1.Id;
		
		o.First_Name__c = c1.FirstName ;
		o.Last_Name__c = c1.LastName ;
		o.Email_Address__c = c1.Email ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		
		o.Referred_By_Contact__c = c1.Id;
		o.Entered_By_Contact__c = c1.Id;

		o.FIS_Application_ID__c = '111222333' ;
		o.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		o.FIS_Decision_Code__c = 'F003' ;
		
		o.Funding_Account_Number__c = '12345679' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.Funding_Routing_Number__c = '123456789' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		
		o.Trial_Deposit_1__c = 7 ;
		o.Trial_Deposit_2__c = 11 ;
		o.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
		
		insert o ;
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		Map<String,String> groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Move to Refund' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		t.Application__c = o.Id ;
		t.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		t.OwnerId = groupId ;
		t.Application_Refund_Source__c = 'Holding' ;
		t.Card_Blocked__c = true ;
		
		insert t ;
		
		String batchTicketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
		String batchGroupId = groupMap.get ( 'Credit Card Batch Processing' ) ;

		Ticket__c bt = new Ticket__c () ;
		bt.Status__c = 'New' ;
		bt.recordTypeId = batchTicketRecordTypeId ;
		bt.Batch_Type__c = 'ACH' ;
		bt.ownerId = batchGroupId ;
		
		insert bt ;
		
		t.SWM_Batch_Ticket__c = bt.id ;
		
		update t ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_batch_download ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( bT ) ;
        
		//  Feed the standard controller to the page controller
		one_application_batch_download tv = new one_application_batch_download ( sc ) ;
		
		tv.goDownload () ;
		
		Test.stopTest() ;
	}
	
	static testmethod void downloadFISTest ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
    	oneOffers__c oo = new oneOffers__c () ;
    	
    	oo.Name = 'Test Offer' ;
    	oo.External_Offer_ID__c = 'abc,xyz,pbq' ;
    	oo.Dollar_Amount__c = 25.00 ;
    	oo.Promotion_Code__c = 'UGET25' ;
    	oo.Start_Date__c = Date.Today () - 1 ;
    	oo.End_Date__c = Date.Today () + 1 ;
    	oo.Active__c = true ;
    	oo.Plan__c = '999' ;
    	oo.Product__c = 'XTZ' ;
    	oo.SubProduct__c = '000' ;
        oo.Terms_ID__c = 'ANOTHER' ;
        oo.Bill_Code__c = 'LAST MINUTE' ;
        oo.Offer_Confirm_Text__c = 'BLAH THANKS JIM' ;
    	
    	insert oo ;
    	
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test11' ;
		c1.LastName = 'User11' ;
		c1.Email = 'test11@user11.com' ;
		
		insert c1 ;
		
		one_application__c o = new one_application__c () ;
		
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		
		o.Contact__c = c1.Id;
		
		o.First_Name__c = c1.FirstName ;
		o.Last_Name__c = c1.LastName ;
		o.Email_Address__c = c1.Email ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		
		o.Referred_By_Contact__c = c1.Id;
		o.Entered_By_Contact__c = c1.Id;

		o.FIS_Application_ID__c = '111222333' ;
		o.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		o.FIS_Decision_Code__c = 'F003' ;
		
		o.Funding_Account_Number__c = '12345679' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.Funding_Routing_Number__c = '123456789' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		
		o.Trial_Deposit_1__c = 7 ;
		o.Trial_Deposit_2__c = 11 ;
		
		o.Application_Approved_Date__c = DateTime.now () ;
		
		o.FIS_Application_ID__c = '3211321' ;
		o.FIS_Decision_Code__c = 'A001' ;
		o.FIS_Credit_Score__c = '980' ;
		o.FIS_Decision_Letter__c = 'A' ;
		o.FIS_Credit_Limit__c = 2500.00 ;
		
		o.Unknown_Location__c = true ;
		o.Invalid_Email_Address__c = true ;
		o.Invalid_Co_Applicant_Email_Address__c = true ;
		
		o.Offer__c = oo.ID ;
		o.Promotion_Code__c = oo.Promotion_Code__c ;
		
		insert o ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;

		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		
		fa.Credit_Card_Application__c = o.ID ;
		fa.OPENDATE__c = Date.Today () - 100 ;
		fa.CONTRACTDATE__c = Date.Today () - 100 ;
		fa.TAXRPTFORPERSNBR__c = c1.ID ;
		fa.Past_Due_History__c = 'ZZZZZZZZZ000' ;
		
		insert fa ;
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		Map<String,String> groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		String batchTicketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
		String batchGroupId = groupMap.get ( 'Credit Card Batch Processing' ) ;

		Ticket__c bt = new Ticket__c () ;
		bt.Status__c = 'New' ;
		bt.recordTypeId = batchTicketRecordTypeId ;
		bt.Batch_Type__c = 'FIS' ;
		bt.ownerId = batchGroupId ;
		
		insert bt ;
		
		fa.Promotion_Batch_Ticket__c = bt.ID ;
		
		update fa ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_batch_download ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( bT ) ;
        
		//  Feed the standard controller to the page controller
		one_application_batch_download tv = new one_application_batch_download ( sc ) ;
		
		tv.goDownload () ;
		
		Test.stopTest() ;
	}
}