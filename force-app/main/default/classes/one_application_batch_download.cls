public with sharing class one_application_batch_download 
{
    private final String HOLDING_ACCTNBR = '1002999283' ;
    private final String COLLATERAL_ACCTNBR = '1003004750' ;
    private final String OUTBOUND_ACCTNBR = '1003087300' ; // YAY!
    private final String LUIGI_ACCTNBR = '1002888527' ;  // HACK!
    private final String RTXNDESC = 'UNITY VISA Refund' ;
    private final String ROUTINGNBR = '01100127' ;
    private final String CASHBOX = '663' ; // ?
    
    private list<Ticket__c> swmTicketList ;
    private list<Financial_Account__c> fcPromoList ;
    private Ticket__c batchTicket ;
    
    public boolean externalFileDownload { public get ; private set ; }
    
    public list<String> fileLineList { public get ; private set ; }
    public String fileURL { public get ; private set ; }

    //  Controls the page
    public String contentType { public get ; private set ; }
    public String ticketType { public get ; private set ; }
    public String applyHTMLTag { public get ; private set ; }
    public String applyBodyTag { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public one_application_batch_download ( ApexPages.Standardcontroller stdC )
    {
    	externalFileDownload = false ;
    	
        //  Get the ID from the ticket
        ApexPages.Standardcontroller theController = stdC ;
        String tId = ((Ticket__c) theController.getRecord ()).id ;
        
        if ( String.IsNotBlank ( tId ) )
        {
            List<Ticket__c> xT ;
            
            xT = [
                SELECT t.Id, t.Batch_Type__c, t.ACH_Batch_URL__c
                FROM Ticket__c t
                WHERE Id = :tId
            ] ;
            
            if ( ( xT != null ) && ( xT.size () > 0 ) )
                batchTicket = xT [ 0 ] ;
            
            ticketType = batchTicket.Batch_Type__c ;

            if ( batchTicket.Batch_Type__c == 'SWM' )
            {
                swmTicketList = [
                    SELECT t.Id, t.Approved_Refund_Amount__c, t.Name, t.Application_Refund_Source__c, t.Contact__c
                    FROM Ticket__c t
                    WHERE SWM_Batch_Ticket__c = :batchTicket.Id
                ] ;
                
                Date d = Date.today () ;
                setSWIMPage ( d.year () + '' + d.month () + '' + d.day () ) ;
            }
            else if ( batchTicket.Batch_Type__c == 'FIS' )
            {
            	fcPromoList = [
            		SELECT ID, ACCTNBR__c, 
            		Credit_Card_Application__r.Offer__r.Dollar_Amount__c,
            		Credit_Card_Application__r.Offer__r.Promotion_Code__c, 
            		Credit_Card_Application__r.Offer__r.Name
            		FROM Financial_Account__c
            		WHERE Promotion_Batch_Ticket__c = :batchTicket.Id
            	] ;
            	
                Date d = Date.today () ;
                setFISPage ( d.year () + '' + d.month () + '' + d.day () ) ;
            }
            else if ( batchTicket.Batch_Type__c == 'ACH' )
            {
                fileURL = batchTicket.ACH_Batch_URL__c ;
                
                setACHPage () ;
            }
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Action
     *  -------------------------------------------------------------------------------------------------- */   
    public PageReference goDownload ()
    {
        PageReference pageRef = null ;
        
        if ( batchTicket.Batch_Type__c == 'SWM' )
        {
            Date d = Date.today () ;
            
            String sMonth = String.valueof(d.month());
            String sDay = String.valueof(d.day());
            
            if (sMonth.length()==1)
                sMonth = '0' + sMonth;
    
            if(sDay.length()==1)
                sDay = '0' + sDay;
                
            String sToday = String.valueof ( d.year () ) + sMonth + sDay ;
            
            if ( swmTicketList.size () > 0 )
            {
                fileLineList = new List<String> () ;
                
                for ( Ticket__c t : swmTicketList )
                {
                    /*  --------------------------------------------------------------------------------------------------
                     *  PART 1 - INTO GL
                     *  -------------------------------------------------------------------------------------------------- */   
                    String l1 = '' ;
                    
                    //  Account number
                    if ( t.Application_Refund_Source__c.equalsIgnoreCase ( 'Holding' ) )
                        l1 += HOLDING_ACCTNBR.leftPad ( 17 ).replace ( ' ', '0' ) ;
                    else if ( t.Application_Refund_Source__c.equalsIgnoreCase ( 'Collateral' ) )
                        l1 += COLLATERAL_ACCTNBR.leftPad ( 17 ).replace ( ' ', '0' ) ;
                        
                    //  transaction code
                    l1 += 'GLD ' ;
                    
                    //  Amount
                    l1 += getNumberWithCents ( t.Approved_Refund_Amount__c ).leftPad ( 10 ).replace ( ' ', '0' ) ;
                    
                    //  Number of items
                    l1 += '000001' ;
                    
                    //  Effective Date of Transaction
                    l1 += sToday ;
                    
                    //  Transaction Description
                    l1 += (RTXNDESC+'-'+t.Name).rightPad ( 45 ) ;
                    
                    //  Trace Number
                    l1 += routingNbr + t.Name.leftPad ( 15-ROUTINGNBR.length() ).replace ( ' ', '0' ) ;
                    
                    //  Cashbox
                    l1 += CASHBOX.leftPad ( 10 ).replace ( ' ', '0' ) ;
                    
                    //  Retirement Code
                    l1 += '    ' ;
                    
                    //  Retirement Year
                    l1 += '    ' ;
                    
                    //  Fund Type Code
                    l1 += 'EL  ' ;
                    
                    //  Fund Type Detail
                    l1 += 'INTR' ;
                    
                    //  Clearing Category Code
                    l1 += 'IMED' ;
                    
                    //  Check Number
                    l1 += '          ' ;
                    
                    //  Balance Category Code
                    l1 += '    ' ;
                    
                    //  Balance Type Code
                    l1 += '    ' ;
                    
                    //  Reverse Paid in Effect
                    l1 += ' ' ;
                    
                    //  END OF LINE
                    l1 += '\r\n' ;
                    
                    fileLineList.add ( l1 ) ;
                    
                    /*  --------------------------------------------------------------------------------------------------
                     *  PART 2 - OUT OF GL
                     *  -------------------------------------------------------------------------------------------------- */   
                    String l2 = '' ;
                    
                    //  Account number
                    l2 += OUTBOUND_ACCTNBR.leftPad ( 17 ).replace ( ' ', '0' ) ;
                    
                    //  transaction code
                    l2 += 'GLR ' ;
                    
                    //  Amount
                    l2 += getNumberWithCents ( t.Approved_Refund_Amount__c ).leftPad ( 10 ).replace ( ' ', '0' ) ;
                    
                    //  Number of items
                    l2 += '000001' ;
                    
                    //  Effective Date of Transaction
                    l2 += sToday ;
                    
                    //  Transaction Description
                    l2 += (RTXNDESC+'-'+t.Name).rightPad ( 45 ) ;
                    
                    //  Trace Number
                    l2 += routingNbr + t.Name.leftPad ( 15-ROUTINGNBR.length() ).replace ( ' ', '0' ) ;
                    
                    //  Cashbox
                    l2 += CASHBOX.leftPad ( 10 ).replace ( ' ', '0' ) ;
                    
                    //  Retirement Code
                    l2 += '    ' ;
                    
                    //  Retirement Year
                    l2 += '    ' ;
                    
                    //  Fund Type Code
                    l2 += 'EL  ' ;
                    
                    //  Fund Type Detail
                    l2 += 'INTR' ;
                    
                    //  Clearing Category Code
                    l2 += 'IMED' ;
                    
                    //  Check Number
                    l2 += '          ' ;
                    
                    //  Balance Category Code
                    l2 += '    ' ;
                    
                    //  Balance Type Code
                    l2 += '    ' ;
                    
                    //  Reverse Paid in Effect
                    l2 += ' ' ;
                    
                    //  END OF LINE
                    l2 += '\n' ;
                    
                    fileLineList.add ( l2 ) ;
                }
            }
        }
        else if ( batchTicket.Batch_Type__c == 'FIS' )
        {
            Date d = Date.today () ;
            
            String sMonth = String.valueof(d.month());
            String sDay = String.valueof(d.day());
            
            if (sMonth.length()==1)
                sMonth = '0' + sMonth;
    
            if(sDay.length()==1)
                sDay = '0' + sDay;
                
            String sToday = sMonth + '/' + sDay + '/' + String.valueof ( d.year () ) ;
            
			if ( ( fcPromoList != null ) && ( fcPromoList.size () > 0 ) )
			{
                fileLineList = new List<String> () ;
                
                //  HEADER
                fileLineList.add ( 'Account Number,Reason Code,Description Code,Description,Transaction Date,Check,Transaction Amount\r\n' ) ;
                
                for ( Financial_Account__c fa : fcPromoList )
                {
                    /*  --------------------------------------------------------------------------------------------------
                     *  Single record per entry
                     *  -------------------------------------------------------------------------------------------------- */   
                	String l = '' ;
                	
                    //  Account number
                	l += fa.ACCTNBR__c ;
                	l += ',' ;
                	                	
                	//  Reason Code ???
                	l += '79' ;
                	l += ',' ;

                	//  Description Code ???
                	l += '' ;
                	l += ',' ;

                	//  Description - Promotion Code
                	l += fa.Credit_Card_Application__r.Offer__r.Promotion_Code__c ;
                	l += ',' ;
                	
                	// Transaction Date
                	l += sToday + '' ;
                	l += ',' ;
                	
                	// Check ???
                	l += '' ;
                	l += ',' ;
                	
                	// Transaction Amount
                	l += fa.Credit_Card_Application__r.Offer__r.Dollar_Amount__c ;
//                	l += ',' ;
                	
                    //  END OF LINE
                    l += '\r\n' ;
                    
                	fileLineList.add ( l ) ;
                }
			}        	
        }
        else if ( batchTicket.Batch_Type__c == 'ACH' )
        {
        	//  DO nothing!
        }

        batchTicket.Batch_Download__c = true ;
        batchTicket.Batch_Download_Date__c = System.now () ;
        batchTicket.Batch_Download_User__c = UserInfo.getUserId () ;
        batchTicket.Is_Batch_Refund__c = true ;
        batchTicket.Status__c = 'Downloaded' ;
        
        update batchTicket ;
        
        return pageRef ;
    }
    
    private String getNumberWithCents ( Decimal x )
    {
        //  Convert to integer to get the whole amount
        Integer y = x.intValue () ;
        
        //  Remainder
        Decimal z = x - y ;
        
        //  Convert to String
        String foo = String.valueof ( y ) ;
        
        //  Default to trailing zeroes
        String bar = '00' ;
        
        //  Convert remainder to String and override zeroes
        if ( z > 0 )
        {
            bar = String.valueOf ( (z * 100).intValue () ) ;
            
			if ( bar.length () == 1 )
			{
				bar = '0' + bar ;
			}
        }
        
        //  Combine!
        return foo + bar ;
    }
    
    private void setSWIMPage ( String dt )
    {
        contentType = 'text/txt#SWM_REFUND_' + dt + '.txt' ;
        applyHTMLTag = 'false' ;
        applyBodyTag = 'false' ;
    	externalFileDownload = false ;
    }
    
    private void setFISPage ( String dt )
    {
        contentType = 'text/csv#FIS_FLAT_FILE_' + dt + '.csv' ;
        applyHTMLTag = 'false' ;
        applyBodyTag = 'false' ;
    	externalFileDownload = false ;
    }
    
    private void setACHPage ()
    {
        contentType = 'text/html' ;
        applyHTMLTag = 'true' ;
        applyBodyTag = 'true' ;
    	externalFileDownload = true ;
    }
}