@isTest
public class TestEverfiProfileTrigger 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Individuals' ;
        
		return a ;
	}
	
    public static testmethod void testContactExists ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test1' ;
        c1.LastName = 'User1' ;
        c1.Email = 'Test1@user1.com' ;
        insert c1 ;
        
        Contact c2 = new Contact () ;
        c2.FirstName = 'Test2' ;
        c2.LastName = 'User2' ;
        c2.Email = 'Test2@user1.com' ;
        insert c2 ;
        
        list<Everfi_Profile__c> epL = new list<Everfi_Profile__c> () ;

        Everfi_Profile__c ep1 = new Everfi_Profile__c () ;
        ep1.First_Name__c = c1.FirstName ;
        ep1.Last_Name__c = c1.LastName ;
        ep1.Email_Address__c = c1.Email ;
        epL.add ( ep1 ) ;
        
        Everfi_Profile__c ep2 = new Everfi_Profile__c () ;
        ep2.First_Name__c = c2.FirstName ;
        ep2.Last_Name__c = c2.LastName ;
        ep2.Email_Address__c = c2.Email ;
        epL.add ( ep2 ) ;
        
        insert epL ;
    }
    
    public static testmethod void testContactNew ()
    {
        Account a = getAccount () ;
        insert a ;

        list<Everfi_Profile__c> epL = new list<Everfi_Profile__c> () ;

        Everfi_Profile__c ep1 = new Everfi_Profile__c () ;
        ep1.First_Name__c = 'Test1' ;
        ep1.Last_Name__c = 'User1' ;
        ep1.Email_Address__c = 'test1@user1.com' ;
        epL.add ( ep1 ) ;
        
        Everfi_Profile__c ep2 = new Everfi_Profile__c () ;
        ep2.First_Name__c = 'Test2' ;
        ep2.Last_Name__c = 'User2' ;
        ep2.Email_Address__c = 'test2@user2.com' ;
        epL.add ( ep2 ) ;
        
        insert epL ;
    }
}