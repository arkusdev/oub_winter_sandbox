@isTest
global class TestUNITYVisaHeatmap 
{
	/*
	 * Creates a generic application I use for testing.
	 */
	private static one_application__c getApplicationForTests ()
	{
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		
		one_application__c app = new one_application__c ();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'F003' ;
		
		insert app;
		
		return app ;
	}

	static testmethod void testHeatmap ()
	{
		//  Point to page to test
		PageReference pr1 = Page.UNITY_Visa_heatmap ;
		Test.setCurrentPage ( pr1 ) ;
		
		one_application__c app = getApplicationForTests () ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		//  Feed the standard controller to the page controller
		UNITY_Visa_Heatmap c = new UNITY_Visa_Heatmap ( sc ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
}