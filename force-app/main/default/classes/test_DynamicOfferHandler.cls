@isTest
public with sharing class test_DynamicOfferHandler 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		
		insert b ;
		
		return b ;
	}
	
	private static Contact getSimpleContact ( Integer x )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
		
		insert c ;
		
		return c ;
	}
	
	private static Financial_Account__c getFA ( Contact c, Branch__c b )
	{
		String checkingRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = checkingRecordTypeId ;
		fa.MJACCTTYPCD__c = 'CK' ;
		fa.TAXRPTFORPERSNBR__c = c.Id ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.BRANCHORGNBR__c = b.Id ;
		fa.NOTEBAL__c = 250.00 ;
		
		insert fa ;
		
		return fa ;
	}
	
	private static oneOffers__c setUpContactCheckingOfferOne ()
	{
		oneOffers__c o = new oneOffers__c () ;
		o.Name = 'Test Offer' ;
		o.Offer_Target_Object__c = 'Contact' ;
		o.Opportunity_Product__c = 'Checking' ;
		o.Offer_Sales_Coaching__c = 'Blah Blah Blah' ;
		o.Offer_Order__c = 1 ;
		o.Offer_Criteria_Object__c = 'Financial_Account__c' ;
		o.Related_To_Field__c = 'TAXRPTFORPERSNBR__c' ;
		o.Active__c = true ;
		o.Start_Date__c = Date.today ().addDays ( -7 ) ;
		o.End_Date__c = Date.today ().addDays ( 7 ) ;
		
		insert o ;
		
		Offer_Filter_Criteria__c o1 = new Offer_Filter_Criteria__c () ;
		o1.oneOffer__c = o.Id ;
		o1.Field__c = 'MJACCTTYPCD__c' ;
		o1.Operator__c = '=' ;
		o1.Value__c = '\'CK\'' ;
		
		insert o1 ;
		
		Offer_Filter_Criteria__c oo1 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o1.Id
		] ;
		
		Offer_Filter_Criteria__c o2 = new Offer_Filter_Criteria__c () ;
		o2.oneOffer__c = o.Id ;
		o2.Field__c = 'CURRACCTSTATCD__c' ;
		o2.Operator__c = 'IN' ;
		o2.Value__c = '( \'ACT\', \'IACT\', \'DORM\' )' ;
		
		insert o2 ;
		
		Offer_Filter_Criteria__c oo2 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o2.Id
		] ;
		
		o.Filter_Criteria__c = '{' + oo1.Name + '} AND {' + oo2.Name + '}' ;
		update o ;
		
		return o ;
	}
	
	private static oneOffers__c setUpContactCheckingOfferTwo ()
	{
		oneOffers__c o = new oneOffers__c () ;
		o.Name = 'Test Offer' ;
		o.Offer_Target_Object__c = 'Contact' ;
		o.Opportunity_Product__c = 'Checking' ;
		o.Offer_Sales_Coaching__c = 'Blah Blah Blah' ;
		o.Offer_Order__c = 1 ;
		o.Offer_Criteria_Object__c = 'Financial_Account__c' ;
		o.Related_To_Field__c = 'TAXRPTFORPERSNBR__c' ;
		o.Active__c = true ;
		o.Start_Date__c = Date.today ().addDays ( -7 ) ;
		o.End_Date__c = Date.today ().addDays ( 7 ) ;
		
		insert o ;
		
		Offer_Filter_Criteria__c o1 = new Offer_Filter_Criteria__c () ;
		o1.oneOffer__c = o.Id ;
		o1.Field__c = 'MJACCTTYPCD__c' ;
		o1.Operator__c = '=' ;
		o1.Value__c = '\'CK\'' ;
		
		insert o1 ;
		
		Offer_Filter_Criteria__c oo1 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o1.Id
		] ;
		
		Offer_Filter_Criteria__c o2 = new Offer_Filter_Criteria__c () ;
		o2.oneOffer__c = o.Id ;
		o2.Field__c = 'CURRACCTSTATCD__c' ;
		o2.Operator__c = 'IN' ;
		o2.Value__c = '( \'ACT\', \'IACT\', \'DORM\' )' ;
		
		insert o2 ;
		
		Offer_Filter_Criteria__c oo2 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o2.Id
		] ;
		
		o.Filter_Criteria__c = '{' + oo1.Name + '} AND {' + oo2.Name + '}' ;
		update o ;
		
		Offer_Filter_Criteria__c o3 = new Offer_Filter_Criteria__c () ;
		o3.oneOffer__c = o.Id ;
		o3.Field__c = 'Personal_Savings_Balance__c' ;
		o3.Operator__c = '>' ;
		o3.Value__c = '0' ;
		
		insert o3 ;
		
		Offer_Filter_Criteria__c oo3 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o3.Id
		] ;
		
		Offer_Filter_Criteria__c o4 = new Offer_Filter_Criteria__c () ;
		o4.oneOffer__c = o.Id ;
		o4.Field__c = 'Personal_Savings_Accounts__c' ;
		o4.Operator__c = '>' ;
		o4.Value__c = '0' ;
		
		insert o4 ;
		
		Offer_Filter_Criteria__c oo4 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o4.Id
		] ;
		
		o.Target_Filter_Critera__c = '{' + oo3.Name + '} AND {' + oo4.Name + '}' ;
		update o ;
		
		return o ;
	}
	
	private static oneOffers__c setUpContactLoanOfferOne ()
	{
		oneOffers__c o = new oneOffers__c () ;
		o.Name = 'Test Offer' ;
		o.Offer_Target_Object__c = 'Contact' ;
		o.Opportunity_Product__c = 'Loan' ;
		o.Offer_Sales_Coaching__c = 'Blah Blah Blah' ;
		o.Offer_Order__c = 2 ;
		o.Offer_Criteria_Object__c = 'Financial_Account__c' ;
		o.Related_To_Field__c = 'TAXRPTFORPERSNBR__c' ;
		o.Active__c = true ;
		o.Start_Date__c = Date.today ().addDays ( -7 ) ;
		o.End_Date__c = Date.today ().addDays ( 7 ) ;
		
		insert o ;
		
		Offer_Filter_Criteria__c o1 = new Offer_Filter_Criteria__c () ;
		o1.oneOffer__c = o.Id ;
		o1.Field__c = 'MJACCTTYPCD__c' ;
		o1.Operator__c = '=' ;
		o1.Value__c = '\'MTG\'' ;
		
		insert o1 ;
		
		Offer_Filter_Criteria__c oo1 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o1.Id
		] ;
		
		Offer_Filter_Criteria__c o2 = new Offer_Filter_Criteria__c () ;
		o2.oneOffer__c = o.Id ;
		o2.Field__c = 'CURRACCTSTATCD__c' ;
		o2.Operator__c = 'IN' ;
		o2.Value__c = '( \'ACT\', \'NPFM\' )' ;
		
		insert o2 ;
		
		Offer_Filter_Criteria__c oo2 = [
			SELECT ID, Name
			FROM Offer_Filter_Criteria__c
			WHERE ID = :o2.Id
		] ;
		
		o.Filter_Criteria__c = '{' + oo1.Name + '} AND {' + oo2.Name + '}' ;
		update o ;
		
		return o ;
	}
	
	private static void setupPriorOffers ( Contact c, oneOffers__c oo, String offer )
	{
		Offer_Response__c o = new Offer_Response__c () ;
		o.Contact__c = c.Id ;
		o.Offer__c = oo.Id ;
		o.Response__c = offer ;
		
		insert o ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  TESTS
	//  --------------------------------------------------------------------------------------
	static testmethod void testContactOneEmpty ()
	{
		DynamicOfferHandler doh = new DynamicOfferHandler () ;
		
		Contact c = new Contact () ;
		
		String foo = doh.setObjectType ( c ) ; 
		
		// Random gets
		List<oneOffers__c> oL = doh.getOfferList () ;
		
		String b1 = doh.branchId ;
		String d1 = doh.departmentId ;
	}
	
	static testmethod void testContactTwo ()
	{
		Branch__c b = getBranch () ;
		
		Contact c99 = getSimpleContact ( 99 ) ;
		
		Financial_Account__c fa = getFa ( c99, b ) ;
		
		oneOffers__c ckOO = setUpContactCheckingOfferOne () ;
		oneOffers__c mtgOO = setUpContactLoanOfferOne () ;
		
		setupPriorOffers ( c99, mtgOO, 'Yes' ) ;
		setupPriorOffers ( c99, mtgOO, 'No' ) ;
		
		DynamicOfferHandler doh = new DynamicOfferHandler () ;
		
		doh.obj = c99 ;
		
		// Random gets
		String x1 = doh.getObjectType () ;
		List<oneOffers__c> oL = doh.getOfferList () ;
		
		String b1 = doh.branchId ;
		String d1 = doh.departmentId ;
	}
	
	static testmethod void testContactThree ()
	{
		Branch__c b = getBranch () ;
		
		Contact c99 = getSimpleContact ( 99 ) ;
		
		Financial_Account__c fa = getFa ( c99, b ) ;
		
		oneOffers__c ckOO = setUpContactCheckingOfferTwo () ;
		oneOffers__c mtgOO = setUpContactLoanOfferOne () ;
		
		setupPriorOffers ( c99, mtgOO, 'Yes' ) ;
		setupPriorOffers ( c99, mtgOO, 'No' ) ;
		
		DynamicOfferHandler doh = new DynamicOfferHandler () ;
		
		doh.obj = c99 ;
	}
	
	static testmethod void testAccountOneEmpty ()
	{
		DynamicOfferHandler doh = new DynamicOfferHandler () ;
		
		Account a = new Account () ;
		
		String foo = doh.setObjectType ( a ) ; 
	}
}