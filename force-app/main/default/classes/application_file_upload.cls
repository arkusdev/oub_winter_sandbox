public with sharing class application_file_upload 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security ?!@?
     *  ---------------------------------------------------------------------------------------- */
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public one_application__c o 
 	{ 
 		get; 
 		private set; 
 	}
 	
 	//  QR Code to allow site transfer to mobile device
 	public String qrCode 
 	{
 		get; 
 		private set; 
 	}
 	
 	public String appField
 	{
 		get;
 		private set;
 	}
 	
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
 
    public string uploadFileName
    {
        get ;
		set ;            
    }
 
	//  Step control for panels on page
	public Integer flStepNumber
	{
		get ;
		private set ;
	}

	//  Additional language based on decision code
    public String codeAdditionalLanguage
    {
        get ;
		private set ;
    }
    
	//  List for document type drop down
    public List<SelectOption> docTypeItemDropDown
    {
        get
        {
	        return one_utils.docTypeItems ;
        }
        
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
    // Count of needed documents
    public Integer docTypeListSize
    {
    	get
    	{
    		Integer dtl = 0 ;
    		
    		if ( docTypeList != null )
    			dtl = docTypeList.size () ;
    			
    		return dtl ;
    	}
    	private set ;
    }
    
    //  DisclosureURL
    public String ratesURLDisclosure 
    { 
        get ; 
        private set ; 
   	}
    
    /*  ----------------------------------------------------------------------------------------
     *  Display cart for checkout tracking
     *  ---------------------------------------------------------------------------------------- */
    public boolean displayCartCheckout { public get ; private set ; }
    public String cartCheckoutJSON { public get ; private set ; }
    public string recordUserJSON { public get; private set; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Live Agent
     *  ---------------------------------------------------------------------------------------- */
    public String liveAgentButton
    {
    	get ; 
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Rakuten tracking pixel
     *  ---------------------------------------------------------------------------------------- */
    public String rakutenTrackingPixel
    {
    	get ; 
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Template Name / Success template
     *  ---------------------------------------------------------------------------------------- */
    public String templateName
    {
        get ;
        private set ;
    }
    
    private Integer successTemplate ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Photo ID stuff
     *  ---------------------------------------------------------------------------------------- */
    private list<String> photoIDL = new list<String> () ;

    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public application_file_upload ( ApexPages.Standardcontroller stdC )
	{
		//  --------------------------------------------------------------------------------------------------
		//  JIM cart checkout for google tag tracking
		//  --------------------------------------------------------------------------------------------------
		displayCartCheckout = false ;
        Cookie jCookie = ApexPages.currentPage().getCookies().get ( 'j_Cookie' ) ;
        
		//  --------------------------------------------------------------------------------------------------
		//  Photo ID setup
		//  --------------------------------------------------------------------------------------------------
		photoIDL.add ( 'Non Exp ID (Driver Lic./State ID) F' ) ;
		photoIDL.add ( 'Non Exp ID (Driver Lic./State ID) B' ) ;
		photoIDL.add ( 'Non Exp ID (Driver Lic./State ID) S' ) ;
        
		//  --------------------------------------------------------------------------------------------------
		//  Default template
		//  --------------------------------------------------------------------------------------------------
		templateName = 'Generic_Application_Template' ;
        successTemplate = 2000 ;
        
		//  --------------------------------------------------------------------------------------------------
		//  Live Agent Button
		//  --------------------------------------------------------------------------------------------------
		if ( ( one_settings__c.getInstance('settings').Live_Agent_Enable__c != null ) &&
             ( one_settings__c.getInstance('settings').Live_Agent_Enable__c == true ) )
        {
			liveAgentButton = one_settings__c.getInstance('settings').Live_Agent_Button_ID__c ;
        }
        else
        {
			liveAgentButton = null ;
        }
        
		//  Default rate disclosure
    	ratesURLDisclosure = one_utils.getDisclosureURL () ;
		
		//  Controls which panel we display, default of 0
		flStepNumber = 0 ;
		
        //  First check for application in session
        o = (one_application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
 
 		//  If no session present, use querystring instead
        if ( ( o == null ) || ( o.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
			
			o = null ;

			//  Both need to be passed in
			if ( ( aid != null ) && ( kid != null ) )
			{
                list<one_application__c> oL = null ;
                
				try
                {
                    oL = [ 
                        SELECT ID, Name, Application_ID__c, First_Name__c, Last_Name__c, Email_Address__c, DOB__c,
                        Phone__c, City__c, State__c, Zip_Code__c,
                        FIS_Application_ID__c,FIS_Decision_Code__c,Upload_Attachment_key__c,RequestedCreditLimit__c,
                        Reservation_Vendor_ID__c, Reservation_Vendor_ID__r.Name,  Reservation_Vendor_ID__r.Tracking_Pixel__c,
                        Reservation_Affiliate_Vendor__c, Reservation_Affiliate_Vendor__r.Name,  Reservation_Affiliate_Vendor__r.Tracking_Pixel__c,
                        Product__c, SubProduct__c, Plan__c, Additional_Information_Required__c, RecordType.DeveloperName
                        FROM one_application__c 
                        WHERE ID = :aid
                        AND Upload_Attachment_key__c = :kid
                        ORDER BY ID
                        LIMIT 1
                    ] ;
				}
				catch ( DmlException e )
				{
					//  Defaults are already set
					for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
					{
				        System.debug ( e.getDmlMessage ( i ) ) ;
					}				
			    }
                
                if ( ( oL != null ) && ( oL.size () > 0 ) )
                {
					//  Grab the first one
					o = oL [ 0 ] ;
                }
				else
				{
		            System.debug ( '========== No Data found for ID & Key ==========' ) ;
				}
			}
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = o.Id ;
			kid = o.Upload_Attachment_Key__c ;
		}
		
		//  Check again for things existing
		if ( ( o != null ) && ( aid != null ) && ( kid != null ) )
		{
            //  --------------------------------------------------------------------------------------------------
            //  JIM user for google tag tracking
			//  --------------------------------------------------------------------------------------------------
            recordUserJSON = one_utils.generateUserJSON ( o ) ;
            
            //  --------------------------------------------------------------------------------------------------
            //  JIM cart checkout for google tag tracking
			//  --------------------------------------------------------------------------------------------------
            if ( jCookie != null )
            {
	            System.debug ( '========== Checking cookie ==========' ) ;
                String jFoo = jCookie.getValue() ;
                
                if ( jFoo.equalsIgnoreCase ( o.ID ) )
                    displayCartCheckout = true ;
                
                //  Remove the cookie
                Cookie jNYETCookie = new Cookie ( 'j_Cookie', '', null, 0, false ) ;
                ApexPages.currentPage().setCookies ( new Cookie[] { jNYETCookie } ) ;
            }
            
			if ( displayCartCheckout )
                cartCheckoutJSON = one_utils.generateCartCheckoutJSON ( o ) ;
		
            System.debug ( '========== Override Rate Disclosure ==========' ) ;
            if ( String.isNotBlank ( o.Product__c ) && String.isNotBlank ( o.SubProduct__c ) && String.isNotBlank ( o.Plan__c ) )
	            ratesURLDisclosure = one_utils.getDisclosureURL ( o.Product__c, o.SubProduct__c, o.Plan__c ) ;
            	
            System.debug ( '========== Tracking Pixel ==========' ) ;
            
			Account a1 = o.Reservation_Vendor_ID__r ;
			Account a2 = o.Reservation_Affiliate_Vendor__r ;
			
			if ( ( a1 != null ) && ( a1.Tracking_pixel__c != null ) )
			{
				if ( a1.Tracking_Pixel__c )
				{
					one_referral oneR = new one_referral () ;
					oneR.loadThirdPartyData ( o.id ) ;
					rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( o.Name ) ;
				}
			}
			else if ( ( a2 != null ) && ( a2.Tracking_pixel__c != null ) )
			{
				if ( a2.Tracking_Pixel__c )
				{
					one_referral oneR = new one_referral () ;
					oneR.loadThirdPartyData ( o.id ) ;
					rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( o.Name ) ;
				}
			}
			
            System.debug ( '========== Valid application found ==========' ) ;
            
            if ( String.isNotBlank ( o.RecordType.DeveloperName ) )
            {
                if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'Deposits' ) )
                {
                    templateName = 'Deposit_Application_template_new' ;
			        successTemplate = 2001 ;
                }
                else if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'UNITY_Visa' ) )
                {
                    templateName = 'application_template' ; 
			        successTemplate = 2000 ;
                }
            }
            else
            {
                templateName = 'application_template' ; 
            }
            
			//  Set up QR Code, need the full URL
			qrCode = one_utils.getDocUploadQRCode ( aid, kid, 125, 125 ) ;
			
			//  Application ID for page
            appField = o.Application_ID__c ;
				
			//  Get the additional language needed
			codeAdditionalLanguage = one_utils.codesAdditionalLanguage.get ( o.FIS_Decision_Code__c ) ;
			
			//  Additional documentation, old vs new
			if ( o.Additional_Information_Required__c != null )
            {
                //  New way, stored in picklist
				docTypeList = one_utils.getAdditionalInfoNeeded ( o.Additional_Information_Required__c ) ;
            }
            else
            {
                //  Old way, via the decision code.  Need to update the picklist as well.
                String docList = one_utils.getMultiPicklistDocs ( o.FIS_Decision_Code__c ) ;
                o.Additional_Information_Required__c = docList ;
				docTypeList = one_utils.getAdditionalInfoNeeded ( docList ) ;
            }
			
			if ( docTypeList != null )
			{
/*
				//  Load attachments separately
				Attachment[] aL = null ;
				
				try
				{
					aL = [
						SELECT Name 
						FROM Attachment 
						WHERE ParentID = :aid 
						] ;
				}
				catch ( Exception e )
				{
					System.debug ( e.getMessage () ) ;
				}
				
				//  Check the attachments (if any!)
				if ( aL != null )
				{
					for ( Attachment oA : aL )
					{
						Integer j = 0 ;
						while ( j < docTypeList.size () )
						{
                            String longName = one_utils.translateAdditionalInfoShortToLong ( oA.name ) ;
                            System.debug ( 'Comparing - "' + docTypeList [j] + '" == "' + longName + '" ?' ) ;
							if ( docTypeList [j].equalsIgnoreCase ( longName ) )
								docTypeList.remove ( j ) ;
							j++ ;
						}
					}
                    
                    System.debug ( 'Final DocTypeList Size = ' + docTypeList.size () ) ;
				}
*/
				//  Set the step number
				if ( ( docTypeList != null ) && ( docTypeList.size () > 0 ) )
                {
					flStepNumber = 1000 ;
                }
                else if ( ( docTypeList != null ) && ( docTypeList.size () == 0 ) )
                {
                    flStepNumber = 2000 ;
                    
                    if ( String.isNotBlank ( o.RecordType.DeveloperName ) )
            		{
                      	if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'Deposits' ) )
                        {
                            flStepNumber = 2001 ;
                        }
                    }
                }
			}
			else
			{
				System.debug ( 'No documents found for this code! :: "' + o.Additional_Information_Required__c + '" ?' ) ;
			}
		}
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Validate & Upload the file
     *  ---------------------------------------------------------------------------------------- */
	public PageReference upload () 
	{
        // Do NOT want this firing more than once!
        displayCartCheckout = false ;
        
        // Defaults
		boolean isValid = true ;
		errorName = false ;
		errorFile = false ;
		
		//  Start with validation
		if ( String.isBlank ( attachment.name ) )
		{
			errorName = true ;
			isValid = false ;
		}
		
		if ( attachment.body == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        flStepNumber = 1002 ;
			return null ;
		}
		
//		attachment.OwnerId = UserInfo.getUserId () ;
		attachment.ParentId = o.Id ;    // the record the file is attached to
		attachment.IsPrivate = false ;  // Or the guest user can't query it!
 
        if ( String.isNotBlank ( uploadFileName ) )
        {
            list<String> thingy = uploadFileName.split ( '\\.' ) ;
            attachment.name += '.' + thingy.get ( thingy.size () -1 ) ;
        }
        
        if ( String.isNotBlank ( uploadFileName ) )
	        attachment.Description = uploadFileName + ' - ' + attachment.Description ; // add the file name so we don't lose it
        
		try 
		{
	        System.debug ( '========== Inserting Attachment ==========' ) ;
			access.insertObject ( attachment ) ;
			
	        System.debug ( '========== Flipping application status ==========' ) ;
	        o.Application_Processing_Status__c = 'Additional Documentation Received' ;
            list<String> thingy = attachment.name.split ( '\\.' ) ;
	        o.Additional_Information_Required__c = removeFoundDoc ( o.Additional_Information_Required__c, thingy.get ( 0 ) ) ;
	        access.updateObject ( o ) ;
		}
		catch (DMLException e)
		{
			System.debug ( '========== Error Inserting! ==========' ) ;

			return null;
		}
		finally 
		{
	        System.debug ( '========== Final stuff ==========' ) ;
	        
	        //  Set panel to success upload
			flStepNumber = 1001 ;
			
            list<String> thingy = attachment.name.split ( '\\.' ) ;
            
			//  Remove document from list
			Integer j = 0 ;
			while ( j < docTypeList.size () )
			{
				System.debug ( '-- ' + docTypeList [j] + ' --> ' + thingy.get ( 0 ) + ' ?? ' + one_Utils.translateAdditionalInfoShortToLong ( thingy.get ( 0 ) ) ) ;
				
				if ( docTypeList [j].equalsIgnoreCase ( one_Utils.translateAdditionalInfoShortToLong ( thingy.get ( 0 ) ) ) )
					docTypeList.remove ( j ) ;
				j++ ;
			}
			
			//  If all documents are gone, go to overall success
			if ( docTypeList.size () == 0 )
				flStepNumber = successTemplate ;
			
			//  Clear errors if any
			errorName = false ;
    		errorFile = false ;
            
			//  Remove document from photoList
			Integer k = 0 ;
			while ( k < photoIDL.size () )
			{
				System.debug ( '-- ' + photoIDL [k] + ' --> ' + thingy.get ( 0 ) ) ;
				
				if ( photoIDL [k].equalsIgnoreCase ( thingy.get ( 0 ) ) )
					photoIDL.remove ( k ) ;
				k++ ;
			}

            //  Reset attachment
			attachment = new Attachment () ; 
            
			//  If all photo documents are gone, do other thing
			if ( photoIDL.size () == 0 )
            {
                checkPhotoID () ;
            }
		}
		
		return null;
	}

    /*  ----------------------------------------------------------------------------------------
     *  Clear docs as they're added
     *  ---------------------------------------------------------------------------------------- */
	private String removeFoundDoc ( String docList, String doc )
	{
		String returnList = '' ;
		
		list<String> dL = docList.split ( ';' ) ;
		list<String> xL = new list<String> () ;
		
		for ( String s : dL )
		{
			if ( ! s.equalsIgnoreCase ( doc ) )
				xL.add ( s ) ;
		}
		
		boolean start = true ;
		for ( String s : xL )
		{
			if ( start )
				start = false ;
			else
				returnList = returnList + ';' ;
				
			returnList = returnList + s ;
		}
		
		return returnList ;
	}
    
    /*  ----------------------------------------------------------------------------------------
     *  Photo ID thingy
     *  ---------------------------------------------------------------------------------------- */
    private void checkPhotoID ()
    {
 		try 
		{
            System.debug ( '========== Flipping photo stuff ==========' ) ;
            o.Photo_ID_Decision__c = 'WARN' ;
            
 	        access.updateObject ( o ) ;
		}
		catch (DMLException e)
		{
			System.debug ( '========== Photo Error Updating?!? ==========' ) ;
		}
    }
}