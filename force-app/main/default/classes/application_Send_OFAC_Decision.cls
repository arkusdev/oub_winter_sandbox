public with sharing class application_Send_OFAC_Decision 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Input
     *  -------------------------------------------------------------------------------------------------- */
    public String overrideReason { public get ; public set ; }
    public String overrideReasonError { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Results
     *  -------------------------------------------------------------------------------------------------- */
    public boolean canOverride { public get ; private set ; }
    public boolean applicantOverrideButton { public get ; private set ; }
    public boolean coApplicantOverrideButton { public get ; private set ; }
    public boolean bothOverrideButton { public get ; private set ; }
    public boolean declineButton { public get ; private set ; }
    public boolean LOSOverride { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Application
     *  -------------------------------------------------------------------------------------------------- */
    private one_application__c app;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public application_Send_OFAC_Decision ( ApexPages.StandardController controller )
    {
        //  DEFAULT
        overrideReason = 'Approved by Compliance' ;
        overrideReasonError = null ;

        canOverride = false ;
        applicantOverrideButton = false ;
        coApplicantOverrideButton = false ;
        bothOverrideButton = false ;
        declineButton = false ;
        LOSOverride = false ;
        
        //  Get (possibly incomplete) application 
        this.app = (one_application__c) controller.getRecord () ;
        
        //  Reload application
        this.app = one_utils.getApplicationFromId ( app.Id ) ;
        
                //  Check for LOS
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
        {
            LOSOverride = true ;
        }
        else
        {
            //  Have they been overridden?
            if ( ( String.isNotBlank ( app.OFAC_Decision__c ) ) &&
                ( app.OFAC_Decision__c.equalsIgnoreCase ( 'FAIL' ) ) &&
                ( app.OFAC_Override_Datetime__c == null ) &&
                ( app.OFAC_Override_User__c == null ) )
            {
                //  Do they have a valid ID to override?
                if ( ( String.isNotBlank ( app.OFAC_Transaction_ID__c ) ) &&
                    ( ! app.OFAC_Transaction_ID__c.equalsIgnoreCase ( '0' ) ) )
                {
                    canOverride = true ;
                    applicantOverrideButton = true ;
                }
                else
                {
                    declineButton = true ;
                }
            }
            
            //  Have they been overridden?
            if ( ( app.Number_of_Applicants__c.equalsIgnoreCase ( 'I will be applying jointly with another person' ) ) &&
                ( String.isNotBlank ( app.OFAC_CoApplicant_Decision__c ) ) &&
                ( app.OFAC_CoApplicant_Decision__c.equalsIgnoreCase ( 'FAIL' ) ) &&
                ( app.OFAC_CoApp_Override_Datetime__c == null ) &&
                ( app.OFAC_CoApp_Override_User__c == null ) )
            {
                //  Do they have a valid ID to override?
                if ( ( String.isNotBlank ( app.OFAC_CoApplicant_Transaction_ID__c ) ) &&
                    ( ! app.OFAC_CoApplicant_Transaction_ID__c.equalsIgnoreCase ( '0' ) ) )
                {
                    canOverride = true ;
                    coApplicantOverrideButton = true ;
                }
                else
                {
                    declineButton = true ;
                }
            }
            
            //  Override one, override both
            if ( applicantOverrideButton && coApplicantOverrideButton )
                bothOverrideButton = true ;
            
            //  Override one, allow for decline
            if ( applicantOverrideButton || coApplicantOverrideButton )
                declineButton = true ;
        }
    }

    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for applicant
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runApplicantOverride ()
    {
        overrideReasonError = null ;
        
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for co-applicant
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runCoApplicantOverride ()
    {
        overrideReasonError = null ;
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            fish.setCoApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;

            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for both
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runBothApplicantOverride ()
    {
        overrideReasonError = null ;
        
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;
            
            //  run the co-applicant
            fish.setCoApplicant () ;
            fish.overrideOFAC ( false, overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Decline
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference declineApplication ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        try
        {
            app.FIS_Decision_Code__c = 'D004' ;
            System.debug('Update app debugging 3:');
            update app ;
        }
        catch ( Exception e )
        {
            System.debug ( 'WTF?!?' ) ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Cancel ?!?
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference cancel ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        return pr ;
    }
}