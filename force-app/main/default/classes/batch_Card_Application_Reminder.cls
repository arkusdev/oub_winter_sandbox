global class batch_Card_Application_Reminder implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	private list<RecordType> rtL ;
	
    global batch_Card_Application_Reminder ()
    {
    	list<String> inRTL = new list<String> () ;
    	inRTL.add ( 'ATM Card Request' ) ;
    	inRTL.add ( 'Business Debit Card Application' ) ;
    	inRTL.add ( 'Card Maintenance Request' ) ;
    	inRTL.add ( 'Consumer Debit Card Application' ) ;
    	
    	rtL = OneUnitedUtilities.getRecordTypeList ( inRTL ) ;
    	
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		System.debug ( '------------------ batch_Card_Application_Reminder START ------------------' ) ;
		
		String rt1 = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'ATM Card Request', 'Ticket__c' ) ; 
		String rt2 = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Business Debit Card Application', 'Ticket__c' ) ; 
		String rt3 = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Card Maintenance Request', 'Ticket__c' ) ; 
		String rt4 = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Consumer Debit Card Application', 'Ticket__c' ) ; 
		
		String query = '' ;
		query += 'SELECT ID, Name, RecordType.Name, Status__c, Days_Since_Last_Status_Change__c, ' ; 
		query += 'OwnerID ' ; 
		query += 'FROM Ticket__c ' ; 
		query += 'WHERE RecordTypeID IN ( \'' + rt1 + '\', \'' + rt2 +  '\', \'' + rt3 +  '\', \'' + rt4 + '\' ) ' ; 
		query += 'AND Status__c = \'Clarification\' ' ; 
		
		if ( ! Test.isRunningTest () )
			query += 'AND Days_Since_Last_Status_Change__c > 2 ' ;

		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
    
	global void execute ( Database.Batchablecontext bc, List<Ticket__c> tL )
	{
		System.debug ( '------------------ batch_Card_Application_Reminder EXECUTE START ------------------' ) ;
		
		Map<String,User> uMap = OneUnitedUtilities.getActiveUsers () ;
		List<Messaging.SingleEmailMessage> mL = new List<Messaging.SingleEmailMessage> () ;
		
		if ( ( tL != null ) && ( tL.size () > 0 ) )
		{
			for ( Ticket__c t : tL )
			{
				//  Create message for each user
				Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage () ;
	
				List<String> toAddresses = new List<String> () ;
				
				User u = uMap.get ( t.OwnerID ) ;
				
				boolean foundUser = false ;
				if ( u != null )
				{
					System.debug ( 'PRIMARY : ' + u.Email ) ;
					
					foundUser = true ;
					toAddresses.add ( u.Email ) ;
					
					if ( u.ManagerID != null )
					{
						User uM = uMap.get ( u.ManagerID ) ;
						
						if ( uM != null )
						{
							System.debug ( 'MANAGER : ' + uM.Email ) ;
							toAddresses.add ( uM.Email ) ;
						}
					}
				}

				if ( foundUser )
				{
					m.setToAddresses ( toAddresses ) ;
					
					m.setSubject ( 'REMINDER: Your ' + t.RecordType.Name + ' ticket is in Clarification Status.' ) ;
					
					String message ;
					message  = '' ;			
					message += '<html>' ;
					message += '<body>' ;
					
					message += '<p>Please note - your ticket has been in the  Clarification status for ' + t.Days_Since_Last_Status_Change__c.intValue () + ' days.</p>' ;
					message += 'To avoid negatively impacting the customer, please update the ticket : <a href="' + URL.getSalesforceBaseUrl ().toExternalForm () + '/' + t.ID + '">' + t.Name + '</a> .' ;
					message += '' ;
					
					message += '</body>' ;
					message += '</html>' ;
					m.setHTMLBody ( message ) ;
					mL.add ( m ) ;
				}
			}
			
			//  Don't email if running test
			if ( ( ! Test.isRunningTest () ) && ( mL.size () > 0 ) )
				Messaging.sendEmail ( mL ) ;
		}
		else
		{
			System.debug ( 'No Records found!' ) ;
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Card Maintenance Clarification Reminder Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}