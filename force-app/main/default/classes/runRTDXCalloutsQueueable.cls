public class runRTDXCalloutsQueueable implements Queueable, Database.AllowsCallouts
{
	private List<String> applicationsIds;
    
    public runRTDXCalloutsQueueable ( List<String> applicationsIds ) 
	{
        this.applicationsIds = applicationsIds;
    }
    
    public void execute(QueueableContext context) 
	{
        List<one_application__c> apps = [SELECT ID, name, RecordTypeID, RecordType.Name, Joint_Application__c,
                Application_ID__c, number_of_applicants__c, FIS_Decision_Code__c, FIS_Application_ID__c,
                Application_Status__c, Application_Processing_Status__c,Override_Person_in_COCC__c,
                Monthly_Rent_Payment__c,Co_Applicant_Monthly_Rent_Payment__c,Co_Applicant_Email_Address__c,prior_employer_city__c,co_applicant_middle_name__c,
                primid_state__c,co_applicant_employer_city__c,createdbyid,street_address__c,street_address_2__c,middle__c,
                co_applicant_employment_how_long_yrs__c,co_applicant_prior_employment_yrs__c,co_applicant_previous_address__c,
                prior_self_employed__c,employment_how_long_mths__c,mailing_state__c,self_employed__c,
                primid_co_applicant_issue_date__c,employer_state__c,isdeleted,mailing_zip_code__c,previous_city__c,
                systemmodstamp,prior_employer_zip_code__c,currently_employed__c,prior_position_occupation__c,
                createddate,ownerid,co_applicant_additional_income_y_n__c,co_applicant_prior_employer__c,
                co_applicant_employment_status__c,co_applicant_prior_employer_city__c,co_applicant_employer__c,
                co_applicant_previous_state__c,co_applicant_currently_employed__c,transfer_balance_credit_card_number__c,
                employer_address__c,mailing_city__c,co_applicant_self_employed__c,primid_expiration_date__c,
                co_applicant_previous_how_long_yrs__c,lastmodifiedbyid,previous_state__c,primid_co_applicant_expiration_date__c,
                co_applicant_position_occupation__c,how_long_yrs__c,co_applicant_employer_state__c,primid_issue_date__c,
                co_applicant_other_housing_status__c,funding_account_number__c,funding_options__c,
                co_applicant_prior_self_employed__c,co_applicant_phone__c,phone__c,credit_report_authorization__c,
                co_applicant_prior_position_occupation__c,co_applicant_street_address__c,co_applicant_street_address_2__c,gross_income_monthly__c,
                employment_status__c,co_applicant_last_name__c,mailing_street_address__c,mailing_street_address_2__c,primid_co_applicant_type__c,
                is_co_app_same_current_address__c,previous_address__c,position_occupation__c,funding_routing_number__c,
                work_phone__c,email_address__c,ssn__c,other_housing_status__c,additional_income_amount__c,
                co_applicant_zip_code__c,previous_how_long_yrs__c,co_applicant_additional_income_type__c,
                prior_employer_address__c,co_applicant_own_rent_other__c,dob__c,zip_code__c,how_long_mths__c,
                co_applicant_how_long_yrs__c,employer_city__c,co_applicant_prior_employer_address__c,prior_employer__c,
                co_applicant_previous_zip__c,co_applicant_how_long_mths__c,referred_by__c,funding_bank_name__c,
                co_applicant_mothersmaidenname__c,prior_employment_gross_income_monthly__c,prior_employed__c,
                co_applicant_employment_status_other__c,additional_income_type__c,primid_type__c,
                co_applicant_employment_how_long_mths__c,primid_co_applicant_number__c,prior_employment_how_long_yrs__c,
                co_applicant_dob__c,reservationid__c,co_applicant_ssn__c,mailing_address_same__c,last_name__c,first_name__c,
                transfer_balance_opt_in__c,over_18__c,employment_status_other__c,primid_number__c,
                co_credit_report_authorization__c,transfer_balance_amount__c,funding_expiration_date__c,
                co_applicant_previous_city__c,co_applicant_first_name__c,co_applicant_state__c,previous_zip__c,
                co_applicant_employer_address__c,mothersmaidenname__c,disclosures__c,co_applicant_prior_employment_gross_inc__c,
                lastmodifieddate,co_applicant_gross_income_monthly__c,own_rent_other__c,
                prior_employment_how_long_months__c,employer__c,state__c,co_applicant_employer_zip_code__c,
                co_applicant_contact__c,co_applicant_prior_employer_state__c,previous_how_long_mths__c,contact__c,
                co_applicant_city__c,co_applicant_additional_income_amount__c,co_applicant_work_phone__c,
                funding_security_code__c,primid_type_formula__c,employment_how_long_yrs__c,
                co_applicant_previous_how_long_mths__c,co_applicant_prior_employment_mths__c,employer_zip_code__c,
                prior_employer_state__c,requestedcreditlimit__c,primid_co_applicant_state__c,additional_income_y_n__c,city__c,
                co_applicant_prior_employer_zip_code__c,is_co_app_same_previous_address__c, FIS_Application_Source_Code__c,
                Promotion_Code__c, Product__c, SubProduct__c, Plan__c, Offer__c, Bill_Code__c, Terms_ID__c,
                IDV_Run_DateTime__c, IDV_Transaction_ID__c, IDV_Override_Datetime__c, IDV_Override_User__c, IDV_Decision__c,
                IDV_CoApplicant_Run_DateTime__c, IDV_CoApplicant_Transaction_ID__c, IDV_CoApplicant_Override_Datetime__c, IDV_CoApplicant_Override_User__c,  IDV_CoApplicant_Decision__c,
                QualiFile_Run_Datetime__c, QualiFile_Acceptance_Text__c, QualiFile_Score__c, QualiFile_Product_Offer__c, QualiFile_Decision__c,
                QualiFile_CoApp_Run_Datetime__c, QualiFile_CoApp_Acceptance_Text__c, QualiFile_CoApp_Score__c, QualiFile_CoApp_Product_Offer__c, QualiFile_CoApp_Decision__c,
                OFAC_Run_Datetime__c, OFAC_Transaction_ID__c, OFAC_Override_Datetime__c, OFAC_Override_User__c, OFAC_Decision__c,
                OFAC_CoApplicant_Run_Datetime__c, OFAC_CoApplicant_Transaction_ID__c, OFAC_CoApp_Override_Datetime__c, OFAC_CoApp_Override_User__c, OFAC_CoApplicant_Decision__c,
                Quiz_Outcome__c,
                Reservation_Vendor_ID__c, Reservation_Vendor_ID__r.Name,  Reservation_Vendor_ID__r.Tracking_Pixel__c,
                Reservation_Affiliate_Vendor__c, Reservation_Affiliate_Vendor__r.Name,  Reservation_Affiliate_Vendor__r.Tracking_Pixel__c,
                Upload_Attachment_Key__c,FundSourceCd__c,
                Insight_Applicant_Number__c, Insight_Co_Applicant_Number__c,
                Advocate_Referral_Code__c, Advocate_Referral_Code_Text__c,
                Credit_Score__c, FIS_Credit_Limit__c,
                Pre_Bureau_Override_Datetime__c, Post_Bureau_Override_Datetime__c
                FROM one_application__c 
				WHERE Id IN :applicationsIds];
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        List<String> appIDList = new List<String> () ;
        
        String uvRTID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;

        //  Set up and log in
        RTDXHelper rh = new RTDXHelper () ;
        RTDXHelper.CardData rhcdL = rh.logIN ( '1234' ) ;
        
        for ( one_application__c app : apps )
        {
            //  Skip deposit applications
            if ( ( String.isNotBlank ( app.RecordType.Name ) ) && ( ! app.RecordType.Name.equalsIgnoreCase ( 'Deposits' ) ) )
            {
                //  Success on login response of 00
                if ( ( rhcdL != null ) && ( rhcdL.responseCode.equalsIgnoreCase ( '00' ) ) )
                {
                    RTDXHelper.CardData rhcd = rh.sendApplication ( app ) ;
                    
                    if ( rhcd != null )
                    {
                        //  Success on number returned
                        if ( String.isNotBlank ( rhcd.accountNumber ) )
                        {
                            //  Override in Bereau means A002
                            if ( ( app.Pre_Bureau_Override_Datetime__c == null ) && ( app.Post_Bureau_Override_Datetime__c == null ) )
	                            app.FIS_Decision_Code__c = 'A001';
                            else
	                            app.FIS_Decision_Code__c = 'A002';
                                
                            app.Application_Processing_Status__c = 'Card Issuance Complete' ;
                            app.RTDX_Success_Datetime__c = System.now () ;
                            
                            appIDList.add ( app.ID ) ;
                            
                            Financial_Account__c fa = new Financial_Account__c () ;
                            fa.RecordTypeId = uvRTID ;
                            
                            fa.ACCTNBR__c = rhcd.accountNumber ;
                            fa.MJACCTTYPCD__c = 'VISA' ;
                            fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
                            fa.CURRACCTSTATCD__c = 'APPR' ;
                            fa.TAXRPTFORPERSNBR__c = app.Contact__c ;
                            fa.Credit_Card_Application__c = app.ID ;
                            
                            faL.add ( fa ) ;
                        }
                        else
                        {
                            app.FIS_Decision_Code__c = 'E922' ;
                        }
                        
                        //  Always update application with response
                        app.RTDX_Response_Code__c = rhcd.responseCode ;
                        app.RTDX_Message__c = rhcd.message ;
                    }
                }
                else
                {
                    app.FIS_Decision_Code__c = 'E922' ;
                    app.RTDX_Response_Code__c = rhcdL.responseCode ;
                    app.RTDX_Message__c = rhcdL.message ;
                }
            }
        }
        
        //  Log out - DISABLED because they think it'll help
//        rh.logOUT () ;
        
        //  Application update
        update apps;

        //  NOT FUTURE!
        if ( ( appIDList != null ) && ( appIDList.size() > 0 ))
            ApplicationEmailFunctions.sendApprovedEmails ( appIDList ) ;
        
        if ( ( faL != null ) && ( faL.size () > 0 ) )
	        insert faL ;
    }
}