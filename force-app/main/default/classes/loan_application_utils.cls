public class loan_application_utils 
{
	// ---------------------------------------------------------------------------------------------------------------
	// Shorty / Longy
	// ---------------------------------------------------------------------------------------------------------------
	private static list<Loan_Application_Additional_Information__mdt> laaiL ;
    
    private static void loadAdditionalInformation ()
    {
        try
        {
            laaiL = [
                SELECT Label, Display_Name__c, Description__c
                FROM Loan_Application_Additional_Information__mdt
                ORDER BY Label
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
    }
    
    // ---------------------------------------------------------------------------------------------------------------
	//  List for document type drop down
    // ---------------------------------------------------------------------------------------------------------------
    public static List<SelectOption> docTypeItems
    {
        get
        {
	        if ( docTypeItems == null )
	        {
                if ( laaiL == null )
                    loadAdditionalInformation () ;
                
	        	docTypeItems = new List<SelectOption> () ;
		        docTypeItems.add ( new SelectOption ( '','' ) ) ;
                
                for ( Loan_Application_Additional_Information__mdt laai : laaiL )
                {
			        docTypeItems.add ( new SelectOption ( laai.Label, laai.Display_Name__c ) ) ;
                }
	        }
	        
	        return docTypeItems ;
        }
        
		set ;
    }
    
    // ---------------------------------------------------------------------------------------------------------------
	// Doc MESS
	// ---------------------------------------------------------------------------------------------------------------
	public static list<String> getAdditionalInfoNeeded ( String s )
	{
        if ( laaiL == null )
            loadAdditionalInformation () ;
        
		list<String> sL = new list<String> () ;
		
        System.debug ( 'OLD S : ' + s ) ;
        
        if ( String.isNotEmpty ( s ) )
		{
			sL = s.split ( ';' ) ;
		}
		
		sL.sort();
		return sL ;
	}
    
    // ---------------------------------------------------------------------------------------------------------------
	// Doc MESS
	// ---------------------------------------------------------------------------------------------------------------
	public static list<String> getAdditionalInfoNeededDISPLAY ( String s )
	{
        if ( laaiL == null )
            loadAdditionalInformation () ;
        
		list<String> sL = new list<String> () ;
		
        System.debug ( 'OLD S : ' + s ) ;
        
        if ( String.isNotEmpty ( s ) )
		{
			list<String> xL = s.split ( ';' ) ;
            
            for ( String xs : xL )
            {
                for ( Loan_Application_Additional_Information__mdt laai : laaiL )
                {
                    if ( xs.equalsIgnoreCase ( laai.Label ) )
                    {
                        if ( String.isNotBlank ( laai.Description__c ) )
                        {
	                        sL.add ( '<b>'+xs + '</b><p>' + laai.Description__c +'</p>') ;
                        }
                        else
                        {
                            sL.add ( '<b>'+xs+ '</b>' ) ;
                        }
                    }
                }
            }
		}
		sL.sort();
		return sL ;
	}
    
	// ---------------------------------------------------------------------------------------------------------------
	// Returns a QR Code for the page passed in
	// ---------------------------------------------------------------------------------------------------------------
	public static String getDocUploadQRCode ( String aid, String key )
	{
		return getDocUploadQRCode ( aid, key, 175, 175 ) ;
	}
	 
	public static String getDocUploadQRCode ( String aid, String key, Integer x, Integer y )
	{
	    String fullUrl;
		if ( String.isNotEmpty ( Site.getBaseUrl () ) )
		 	fullUrl = Site.getBaseUrl ()+'/'  ;
		else
			fullUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/apex/' ;
	    	
		String url = fullUrl + 'loan_application_file_upload?aid=' + aid + '&kid=' + key ;
		
		String qrCode = 'https://chart.googleapis.com/chart?cht=qr&chs=' + x + 'x' + y + '&chl=' + EncodingUtil.urlEncode ( url, 'UTF-8' ) ;

		return qrCode ;
	}

	// ---------------------------------------------------------------------------------------------------------------
	// Generates random key for application that is used in uploading attachments.
	// ---------------------------------------------------------------------------------------------------------------
	private static final List<String> RANDOM_CHARS = new List<String> { 
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
		} ;

	public static String getRandomString ( Integer len )
	{
		String retVal = '';
		
		if ( len != null && len >= 1 )
		{
			Integer chars = 0 ;
			Integer random ;
			do
			{
				random = Math.round ( Math.random () * Integer.valueOf ( RANDOM_CHARS.size () - 1 ) ) ;
				
				// Should never happen any longer, but just in case ...
				if ( random < 0 ) random = 0 ;
				if ( random > 62) random = 62 ;
				
				retVal += RANDOM_CHARS [ random ] ;
				chars++ ;
			} while ( chars < len ) ;
		}
		
		return retVal ;
	}
	
}