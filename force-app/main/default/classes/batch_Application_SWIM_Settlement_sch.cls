global class batch_Application_SWIM_Settlement_sch implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Application_SWIM_Settlement b = new batch_Application_SWIM_Settlement () ;
		Database.executeBatch ( b ) ;
   }    
}