public class applicationCheckStatusHelper 
{
    private static Map<String, Map<String, application_check_status__c>> acsRT ;
    private static final String SEPARATOR = '=' ;

    private static void loadData ()
    {
		System.debug ( '----- Loading Data' ) ;
        
        //  Load results for parsing later
        try
        {
            List<application_check_status__c> acsL = [
                SELECT ID, Name, Title__c, Body__c, Application_Record_Type__c,
                Application_Status__c, Application_Processing_Status__c, ACH_Funding_Status__c, 
                Active_Additional_Info__c, Active_Trial_Deposit__c, Order_By__c
                FROM application_check_status__c
                WHERE Active__c = true
                ORDER BY Application_Record_Type__c, Order_By__c
            ] ;
            
            Map<String, application_check_status__c> acsM = null ;
            String lastRT = 'XXX' ;
            acsRT = new Map<String, Map<String, application_check_status__c>> () ;
            
            if ( ( acsL <> null ) && ( acsL.size () > 0 ) )
            {
                for ( application_check_status__c acs : acsL )
                {
                    if ( ! acs.Application_Record_Type__c.equalsIgnoreCase ( lastRT ) )
                    {
                        if ( ! lastRT.equalsIgnoreCase ( 'XXX' ) )
                            acsRT.put ( lastRT, acsM ) ;
                        
                        lastRT = acs.Application_Record_Type__c ;
		                acsM = new Map <String, application_check_status__c> () ;
                    }
                    
                    acsM.put ( acs.Application_Status__c + SEPARATOR + acs.Application_Processing_Status__c + SEPARATOR + acs.ACH_Funding_Status__c, acs ) ;
                }
                
                //  Save the last one
                acsRT.put ( lastRT, acsM ) ;
            }
        }
        catch ( Exception e )
        {
            System.debug ( 'Unable to load results?!?' ) ;
            acsRT = null ;
        }
    }
    
    public application_check_status__c getStatus ( String s1, String s2, String s3 )
    {
        return getStatus ( 'UNITY_Visa', s1, s2, s3 ) ;
    }
    
    public application_check_status__c getStatus ( String rt, String s1, String s2, String s3 )
    {
        if ( acsRT == null )
            loadData () ;
        
        application_check_status__c acs = null ;
        
        String check1 = getSeparator ( s1, s2, s3 ) ;
        System.debug ( 'Look up #1 :: "' + check1 + '"' ) ;
        
        if ( acsRT != null )
        {
            Map<String, application_check_status__c> acsM = acsRT.get ( rt ) ;
            
            if ( acsM != null )
            {
                if ( acsM.containsKey ( check1 ) )
                {
                    acs = acsM.get ( check1 ) ;
                }
	            else
                {
                    String check2 = getSeparator ( s1, 'ALL', s3 ) ;
                    System.debug ( 'Look up #2 :: "' + check2 + '"' ) ;
                    
                    if ( acsM.containsKey ( check2 ) )
                    {
                        acs = acsM.get ( check2 ) ;
                    }
                    else
                    {
                        String check3 = getSeparator ( s1, s2, 'ALL' ) ;
                        System.debug ( 'Look up #3 :: "' + check3 + '"' ) ;
                        
                        if ( acsM.containsKey ( check3 ) )
                        {
                            acs = acsM.get ( check3 ) ;
                        }
                        else
                        {
                            String check4 = getSeparator ( s1, 'ALL', 'ALL' ) ;
                            System.debug ( 'Look up #4 :: "' + check4 + '"' ) ;
                            
                            if ( acsM.containsKey ( check4 ) )
                            {
                                acs = acsM.get ( check4 ) ;
                            }
                            else
                            {
                                System.debug ( '-ERROR 1 !-' ) ;
                                acs = null ;
                            }
                        }
                    }
                }
            }
            else
            {
	            System.debug ( '-ERROR 2 !-' ) ;
                acs = null ;
            }
        }
        else
        {
            System.debug ( '-ERROR 3 !-' ) ;
            acs = null ;
        }
        
        return acs ;
    }
    
    private String getSeparator ( String s1, String s2, String s3 )
    {
        String thingy = '' ;
        if ( String.isNotBlank ( s1 ) )
            thingy = thingy + s1 ;
        else
        	thingy = thingy + 'ALL' ;
        
        if ( String.isNotBlank ( s2 ) )
            thingy = thingy + SEPARATOR + s2 ;
        else
        	thingy = thingy + SEPARATOR + 'ALL' ;
        
        if ( String.isNotBlank ( s3 ) )
            thingy = thingy + SEPARATOR + s3 ;
        else
        	thingy = thingy + SEPARATOR + 'ALL' ;
        
		return thingy ;
    }
}