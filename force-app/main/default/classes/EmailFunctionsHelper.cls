public class EmailFunctionsHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Double mapping tracking
	 *  -------------------------------------------------------------------------------------------------- */	
	public static map <String, boolean> approvalMap ; 
	public static map <String, boolean> fundingMap ; 
	public static map <String, boolean> additionalMap ; 
	public static map <String, boolean> trialMap ; 
	public static map <String, boolean> warningMap ; 
	public static map <String, boolean> denyMap ;
    public static map <String, boolean> withdrawalMap ;
    public static map <String, boolean> declineIDVMap ;
    public static map <String, boolean> declineOFACMap ;
    
    public static map <String, boolean> declineFCRAMap ;
    public static map <String, boolean> achRejectionMap ;
    public static map <String, boolean> counterOfferMap ;
    
    public static map <String, boolean> notCitizenMap ;
    public static map <String, boolean> outOfAreaMap ;
    public static map <String, boolean> underageMap ;
    public static map <String, boolean> youngMap ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Generic checker
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean checkDuplicate ( Map<String,boolean> xMap, String id )
	{
		boolean returnValue = false ;
		
		if ( xMap.containsKey ( id ) == true )
			returnValue = true ;
		else
			xMap.put ( id, true ) ;
		
        System.debug ( 'Returning : ' + returnValue ) ;
        
		return returnValue ;
	}
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Time-based Additional Info Email
	 *  -------------------------------------------------------------------------------------------------- */
    public static boolean sendTimeBasedAdditionalInfoEmail ( one_application__c o )
    {
    	return sendTimeBasedAdditionalInfoEmail ( o, null ) ;
    }
    
    public static boolean sendTimeBasedAdditionalInfoEmail ( one_application__c o, DateTime lastDate )
    {
        boolean sendEmail = false ;
        
		if ( ( String.isNotBlank ( o.Application_Status__c) ) &&
             ( ( o.Application_Status__c.equalsIgnoreCase ( 'pending' ) ) || ( o.Application_Status__c.equalsIgnoreCase ( 'fraud' ) ) || ( o.Application_Status__c.equalsIgnoreCase ( 'pending funding' ) ) ) )
        {
            if ( ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                 ( o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
            {
                if ( o.CreatedDate != null )
                {
                    date cd = o.CreatedDate.date () ;
                    
                    Integer diff = Math.abs ( cd.daysBetween ( Date.today () ) ) ;
                    Long remainder = Math.mod ( diff, 4 ) ;
                    
                    System.debug ( 'Diff : ' + diff + ' Remainder : ' + remainder ) ;
                    
                    if ( ( remainder == 0 ) && ( diff <= 15 ) && ( ! cd.isSameDay ( Date.today () ) ) )
                    {
                        if ( ( lastDate == null ) || ( ( lastDate != null ) && ( ! lastDate.isSameDay ( Date.today () ) ) ) )
                        {
                            System.debug ( 'Additional Email - Status change time - ADD' ) ;
                            sendEmail = true ;
                        }
                    }
                }
            }
        }
        
        return sendEmail ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Time-based trial deposit Email
	 *  -------------------------------------------------------------------------------------------------- */
    public static boolean sendTimeBasedTrialEmail ( one_application__c o )
    {
    	return sendTimeBasedTrialEmail ( o, null ) ;
    }
    
    public static boolean sendTimeBasedTrialEmail ( one_application__c o, DateTime lastDate )
    {
        boolean sendEmail = false ;
        
		if ( ( String.isNotBlank ( o.Application_Status__c) ) &&
             ( ( o.Application_Status__c.equalsIgnoreCase ( 'pending' ) ) || ( o.Application_Status__c.equalsIgnoreCase ( 'fraud' ) ) || ( o.Application_Status__c.equalsIgnoreCase ( 'pending funding' ) ) ) )
        {
            if ( String.isNotBlank ( o.Funding_Options__c ) )
            {
                if ( o.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) )
                {
                    if ( ( o.FIS_Decision_Code__c != null ) && ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P501' ) ) )
                    {
                        if ( o.CreatedDate != null )
                        {
                            date cd = o.CreatedDate.date () ;
                            
                            Integer diff = Math.abs ( cd.daysBetween ( Date.today () ) ) ;
                            Long remainder = Math.mod ( diff, 4 ) ;
                            
                            System.debug ( 'Diff : ' + diff + ' Remainder : ' + remainder ) ;
                            
                            if ( ( remainder == 0 ) && ( ! cd.isSameDay ( Date.today () ) ) )
                            {
                                if ( String.isNotBlank ( o.ACH_Funding_Status__c ) && 
                                    ( ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) ) ||
                                     ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Send Trial Deposit' ) ) )
                                   )
                                {
                                    System.debug ( 'Trial Deposit Email - Status change time - ADD' ) ;
                                    sendEmail = true ;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return sendEmail ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Time-based Funding Email
	 *  -------------------------------------------------------------------------------------------------- */
    public static boolean sendTimeBasedFundingEmail ( one_application__c o )
    {
    	return sendTimeBasedFundingEmail ( o, null ) ;
    }
    
    public static boolean sendTimeBasedFundingEmail ( one_application__c o, DateTime lastDate )
    {
        boolean sendEmail = false ;
        
		if ( ( String.isNotBlank ( o.Application_Status__c) ) &&
             ( ( o.Application_Status__c.equalsIgnoreCase ( 'pending' ) ) || ( o.Application_Status__c.equalsIgnoreCase ( 'fraud' ) ) || ( o.Application_Status__c.equalsIgnoreCase ( 'pending funding' ) ) ) )
        {
            if ( String.isNotBlank ( o.Funding_Options__c ) )
            {
                if ( ( ! o.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) &&
                     ( ! o.Funding_Options__c.equalsIgnoreCase ( 'Debit Card' ) ) )
                {
                    if ( ( o.FIS_Decision_Code__c != null ) &&
                        ( ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) ||
                         ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) || 
                         ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) )
                    {
                        if ( o.CreatedDate != null )
                        {
                            date cd = o.CreatedDate.date () ;
                            
                            Integer diff = Math.abs ( cd.daysBetween ( Date.today () ) ) ;
                            Long remainder = Math.mod ( diff, 4 ) ;
                            
                            System.debug ( 'Diff : ' + diff + ' Remainder : ' + remainder ) ;
                            
                            if ( ( remainder == 0 ) && ( ! cd.isSameDay ( Date.today () ) ) )
                            {
                                if ( ( lastDate == null ) || ( ( lastDate != null ) && ( ! lastDate.isSameDay ( Date.today () ) ) ) )
                                {
                                    System.debug ( 'Funding Email - Status change time - ADD' ) ;
                                    sendEmail = true ;
                                }
                            }
                        }
                    }
                }
            }
        }
        
		return sendEmail ;
    }
}