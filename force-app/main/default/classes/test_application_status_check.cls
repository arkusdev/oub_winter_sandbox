@isTest
public class test_application_status_check 
{
	/*
	 * Creates a generic application I use for testing.
	 */
	private static one_application__c getApplicationForTests ()
	{
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'test1';
		c1.LastName = 'user1';
		c1.Email = 'hey1@emailhey1.com';
		insert c1;
        
		Contact c2 = new Contact();
		c2.FirstName = 'test2';
		c2.LastName = 'user2';
		c2.Email = 'hey2@emailhey2.com';
		insert c2;
		
		one_application__c app = new one_application__c ();
		app.Number_of_Applicants__c = 'I will be applying invidually' ;
		
		app.Contact__c = c1.Id;
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.SSN__c = '123-45-6789' ;
		app.DOB__c = Date.newInstance ( 1960, 2, 17 ) ;
        app.Phone__c = '321-987-9879' ;
        app.Street_Address__c = 'Test Street' ;
        app.City__c = 'Test City' ;
        app.State__c = 'CA' ;
        app.ZIP_Code__c = '90210' ;
        app.MothersMaidenName__c = 'User' ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		app.Co_applicant_SSN__c = '234-56-7890' ;
		app.Co_Applicant_DOB__c = Date.newInstance ( 1960, 2, 17 ) ;
        app.Co_Applicant_Phone__c = '321-987-9879' ;
        app.Co_Applicant_Street_Address__c = 'Test Street' ;
        app.Co_Applicant_City__c = 'Test City' ;
        app.Co_Applicant_State__c = 'CA' ;
        app.Co_Applicant_ZIP_Code__c = '90210' ;
        app.Co_Applicant_MothersMaidenName__c = 'User' ;
        
		app.Gross_Income_Monthly__c = 10000.00 ;
        app.Promotion_Code__c = 'NYET' ;
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'A001' ;
		
		app.Trial_Deposit_1__c = 7 ;
		app.Trial_Deposit_2__c = 11 ;
        
		app.Application_Processing_Status__c = 'Approved Pending' ;
        app.ACH_Funding_Status__c = 'No' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
		
		insert app ;
		
		return app ;
	}

	public static testmethod void testOne ()
    {
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
        
        //  Load settings
        one_test_new.insertCustomSetting () ;
        
        //  Load helper
        test_applicationStatusCheckHelper.loadRT1ACSData () ;
        
        //  Load application
        one_application__c o = getApplicationForTests () ;
        
        //  Re-get, need the name
        one_application__c oX = [
            SELECT ID, Name
            FROM one_application__c
            WHERE ID = :o.ID
        ] ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;

		//  Load class
		application_status_check a = new application_status_check ( stdC ) ;
        
        //  Initial setup test
        System.assertEquals ( a.scStep, 1000 ) ;

		//  Submit blank        
        a.submitAppInfo () ;
        
        //  Check for errors
        System.assertEquals ( a.errorAppID, true ) ;
        System.assertEquals ( a.errorMothersMaidenName, true ) ;

		//  Step?
        System.assertEquals ( a.scStep, 1000 ) ;
		
        //  Fail data test
        a.appID = 'Application' ;
        a.mothersmaidenname = 'BAD' ;
        
		//  Submit data        
        a.submitAppInfo () ;
        
        //  Check for NOT errors
        System.assertEquals ( a.errorAppID, false ) ;
        System.assertEquals ( a.errorMothersMaidenName, false ) ;
		
		//  Step?
        System.assertEquals ( a.scStep, 9000 ) ;
        
        //  Pass data test
        a.appID = oX.name ;
        a.mothersmaidenname = 'User' ;
        
		//  Submit data        
        a.submitAppInfo () ;
        
		//  Step?
        System.assertEquals ( a.scStep, 5000 ) ;
        System.assertNotEquals ( a.title, null ) ;
        System.assertNotEquals ( a.Body, null ) ;
        
		//  End of testing
		Test.stopTest () ;
    }
}