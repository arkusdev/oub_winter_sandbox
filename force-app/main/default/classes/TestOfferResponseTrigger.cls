@isTest
public class TestOfferResponseTrigger 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Branch__c getBranch ( Integer i ) 
    {
        Branch__c b = new Branch__c () ;
        b.Name = 'Branch' + i ;
        
        return b ;
    }

    private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.name = 'Test' + i ;
        
        return a ;
    }
    
	private static Contact getContact ( Account a, Integer i, Branch__c b )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'User' + i ;
		c.Email = 'test' + i + '@user' + i + '.com' ;
        c.AccountId = a.ID ;
        c.Branch__c = b.ID ;
		
		return c ;
	}
    
    private static oneOffers__c getOffer ( Integer i )
    {
        oneOffers__c o = new oneOffers__c () ;
        o.Name = 'Offer' + i ;
        
        return o ;
    }
    
    private static Referral_Code__c getReferralCode ( Contact c )
    {
        Referral_Code__c rc = new Referral_Code__c () ;
        rc.Contact__c = c.ID ;
        rc.Name = c.FirstName + c.LastName ;
        
        return rc ;
    }
	
	//  --------------------------------------------------------------------------------------
	//  ONE
	//  --------------------------------------------------------------------------------------
    public static testmethod void ONE ()
    {
        Branch__c b1 = getBranch ( 1 ) ;
        insert b1 ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1, b1 ) ;
        insert c1 ;
        
        Referral_Code__c rc1 = getReferralCode ( c1 ) ;
        insert rc1 ;
        
        oneOffers__c o1 = getOffer ( 1 ) ;
        o1.Offer_Criteria_Object__c = 'Referral_Code__c' ;
        insert o1 ;
        
        Test.startTest () ;
        
        Offer_Response__c of1 = new Offer_Response__c () ;
        of1.Contact__c = c1.ID ;
        of1.Branch__c = b1.ID ;
        of1.Offer__c = o1.ID ;
        
        insert of1 ;
        
        list<Offer_Response__c> ofL = [
            SELECT ID, Referral_Code__c
            FROM Offer_Response__c
            WHERE Offer__c = :o1.ID
        ] ;
        
        System.assertEquals ( ofL [ 0 ].Referral_Code__c, rc1.ID ) ;
        
        Test.stopTest () ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  TWO
	//  --------------------------------------------------------------------------------------
    public static testmethod void TWO ()
    {
        Branch__c b1 = getBranch ( 1 ) ;
        insert b1 ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1, b1 ) ;
        insert c1 ;
        
        list<Referral_Code__c> rcL = new list<Referral_Code__c> () ;
        
        Referral_Code__c rc1 = getReferralCode ( c1 ) ;
        rc1.Name += '1' ;
        rcL.add ( rc1 ) ;
        
        Referral_Code__c rc2 = getReferralCode ( c1 ) ;
        rc2.Name += '2' ;
        rcL.add ( rc2 ) ;
        
        insert rcL ;
        
        list<Referral_Code_Financial_Account__c> rfcaL = new list<Referral_Code_Financial_Account__c> () ;
        
        Referral_Code_Financial_Account__c rcfa1 = new Referral_Code_Financial_Account__c () ;
        rcfa1.Advocate_Referral_Code__c = rcL[0].ID ;
        rfcaL.add ( rcfa1 ) ;
        
        Referral_Code_Financial_Account__c rcfa2 = new Referral_Code_Financial_Account__c () ;
        rcfa2.Advocate_Referral_Code__c = rcL[1].ID ;
        rfcaL.add ( rcfa2 ) ;
        
        Referral_Code_Financial_Account__c rcfa3 = new Referral_Code_Financial_Account__c () ;
        rcfa3.Advocate_Referral_Code__c = rcL[1].ID ;
        rfcaL.add ( rcfa3 ) ;
        
        insert ( rfcaL ) ;
        
        oneOffers__c o1 = getOffer ( 1 ) ;
        o1.Offer_Criteria_Object__c = 'Referral_Code__c' ;
        insert o1 ;
        
        Test.startTest () ;
        
        Offer_Response__c of1 = new Offer_Response__c () ;
        of1.Contact__c = c1.ID ;
        of1.Branch__c = b1.ID ;
        of1.Offer__c = o1.ID ;
        insert of1 ;
        
        list<Offer_Response__c> ofL = [
            SELECT ID, Referral_Code__c
            FROM Offer_Response__c
            WHERE Offer__c = :o1.ID
        ] ;
        
        System.assertEquals ( ofL [ 0 ].Referral_Code__c, rcL[1].ID ) ;
        
        Test.stopTest () ;
    }
}