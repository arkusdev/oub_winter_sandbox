global class DeDuplicationActionFunctionExpanded 
{
	private static final String DELIMITER = '~' ;
    private static final Integer DEDUPE_MERGE_MAX = 2 ;
	
	private static String customerRecordTypeID ;
	private static String prospectRecordTypeID ;
    
  	private static boolean debugCRAP = false ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void dedupeContactCustomer ( List<ID> xL )
	{
		customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
		
		Contact emptyC = new Contact () ;
		List<String> emptyContactFields = DynamicObjectHandler.getUpdateableFields ( emptyC ) ;
		
		String cLs = DynamicObjectHandler.joinListQuoted ( xL, ',' ) ;
		
		String c1 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyC, 'ID IN ( ' + cLs + ' ) AND recordTypeID = \'' + customerRecordTypeID + '\'' , 'ID' ) ;
		
        if ( debugCRAP ) 
            System.debug ( 'C1: ' + c1 ) ;
		
		list<String> sEL = new list<String> () ;
		list<String> sPL = new list<String> () ;
		list<Contact> cL = Database.query ( c1 ) ;
		
		map<String,Contact> cEMap = new Map<String,Contact> () ;
		
		System.debug ( 'Contact list size: ' + cL.size () ) ;
		
		if ( ( cL != null ) && ( cL.size () > 0 ) )
		{
			for ( Contact c : cL )
			{
				if ( String.isNotBlank ( getKeyByEmail ( c ) ) )
				{
					cEMap.put ( getKeyByEmail ( c ), c ) ;
					sEL.add ( c.Email ) ;
				}
				
				if ( String.isNotBlank ( getKeyByPhone ( c ) ) )
				{
					cEMap.put ( getKeyByPhone ( c ), c ) ;
					sPL.add ( c.Phone ) ;
				}
			} 
			
			String oELs = null ;
			String oPLs = null ;
			
			// JIM's crazy Prospect check
			String thingy = ' (  recordTypeID = \'' + prospectRecordTypeID + '\' AND UploadID__c = NULL AND TaxID__c = NULL ) ' ;
			boolean foundData = false ;
			
			if ( ( sEL.size () > 0 ) && ( sPL.size () > 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += 'AND ( Email IN ( ' + oELs + ' ) OR Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () > 0 ) && ( sPL.size () == 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				
				thingy += 'AND ( Email IN ( ' + oELs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () == 0 ) && ( sPL.size () > 0 ) )
			{
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += 'AND ( Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			
			map<ID,Contact> contactMapToUpdate = new map<ID,Contact> () ;
			map<ID,Contact> masterContactMap = new map<ID,Contact> () ;
			map<ID,List<Contact>> duplicateContactMap = new map<ID,List<Contact>> () ;
			
			if ( foundData == true )
			{
				
				String c2 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyC, thingy, 'ID' ) ;
		
          		if ( debugCRAP )
					System.debug ( 'C2: ' + c2 ) ;
				
				list<Contact> oL = Database.query ( c2 ) ;
				
				if ( ( oL != null ) && ( oL.size () > 0 ) )
				{
					for ( Contact c : oL )
					{
						boolean found = false ;
						
						System.debug ( 'Checking for :: ' + getKeyByEmail ( c ) ) ;
						
						if ( cEMap.containsKey ( getKeyByEmail ( c ) ) )
						{
							System.debug ( 'Email Key FOUND!' ) ;
							
							Contact cc = cEMap.get ( getKeyByEmail ( c ) ) ;
							
							if ( cc.ID != c.ID )
							{
								for ( String s : emptyContactFields )
								{
									if ( ( cc.get ( s ) == null ) && ( c.get ( s ) != null ) )
									{
									 	System.debug ( 'Setting "' + s + '", found difference' ) ;
									 	
									 	found = true ;
									 	cc.put ( s, c.get ( s ) ) ;
									}
								}
								
								if ( found == true )
								{
									if ( ! contactMapToUpdate.containsKey ( cc.ID ) )
										contactMapToUpdate.put ( cc.ID, cc ) ;
								}
								
								if ( ! masterContactMap.containsKey ( cc.ID ) )
									masterContactMap.put ( cc.ID, cc ) ;
								
								if ( ! duplicateContactMap.containsKey ( cc.ID ) )
									duplicateContactMap.put ( cc.ID, new list<Contact> () ) ;	
			
								duplicateContactMap.get ( cc.ID ).add ( c ) ;
							}
							else
							{
								System.debug ( 'Duplicate Master ID found, skipping' ) ;
							}
						}
						else
						{
							System.debug ( 'Email Key Not Found, checking phone!' ) ;
							
							System.debug ( 'Checking for :: ' + getKeyByPhone ( c ) ) ;
						
							if ( cEMap.containsKey ( getKeyByPhone ( c ) ) )
							{
								System.debug ( 'Phone Key FOUND!' ) ;
								
								Contact cc = cEMap.get ( getKeyByPhone ( c ) ) ;
								
								if ( cc.ID != c.ID )
								{
									for ( String s : emptyContactFields )
									{
										if ( ( cc.get ( s ) == null ) && ( c.get ( s ) != null ) )
										{
										 	System.debug ( 'Setting "' + s + '", found difference' ) ;
										 	
										 	found = true ;
										 	cc.put ( s, c.get ( s ) ) ;
										}
									}
									
									if ( found == true )
									{
										if ( ! contactMapToUpdate.containsKey ( cc.ID ) )
											contactMapToUpdate.put ( cc.ID, cc ) ;
									}
									
									if ( ! masterContactMap.containsKey ( cc.ID ) )
										masterContactMap.put ( cc.ID, cc ) ;
									
									if ( ! duplicateContactMap.containsKey ( cc.ID ) )
										duplicateContactMap.put ( cc.ID, new list<Contact> () ) ;	
				
									duplicateContactMap.get ( cc.ID ).add ( c ) ;
								}
								else
								{
									System.debug ( 'Duplicate Master ID found, skipping' ) ;
								}
							}
							else
							{
								System.debug ( 'Key Not Found!' ) ;
							}
						}
					}
				}
				else
				{
					System.debug ( 'No prospect records found to merge!' ) ;
				}
			}
			else
			{
				System.debug ( 'No valid emails or phone numbers found to merge on!' ) ;
			}
	
			if ( contactMapToUpdate.keyset ().size () > 0 )
			{
				list<Contact> contactListToUpdate = new list<Contact> () ;
				
				for ( String cID : contactMapToUpdate.keyset () )
					contactListToUpdate.add ( contactMapToUpdate.get ( cID ) ) ;
				
				try
				{
					System.debug ( 'Updating ' + contactListToUpdate.size () + ' contact records' ) ;
				
					update contactListToUpdate ;
				}
				catch ( DMLException e )
				{
					System.debug ( 'DML : ' + e.getMessage () ) ;
				}
			}
				
			if ( masterContactMap.keyset ().size () > 0 )
			{
				System.debug ( 'Merging into ' + masterContactMap.keyset ().size () + ' contact records' ) ;
				
				for ( ID x : masterContactMap.keyset () )
				{
                    list<Contact> dcL = duplicateContactMap.get ( x ) ;
                    
			        List<Contact> thingyL = new List<Contact> () ;
                    for ( Contact c : dcL )
                    {
                        thingyL.add ( c ) ;
		                if ( thingyL.size () == DEDUPE_MERGE_MAX )
                        {
                            System.debug ( 'MERGE MAX of ' + DEDUPE_MERGE_MAX + ', running' ) ;
                            Database.MergeResult[] results = Database.merge ( masterContactMap.get ( x ), thingyL, false ) ;
                            
                            for ( Database.MergeResult res : results ) 
                            {
                                if ( res.isSuccess () ) 
                                {
                                    System.debug ( 'MERGE SUCCESS!' ) ;
                                }
                                else
                                {
                                    for ( Database.Error err : res.getErrors () )
                                        System.debug ( err.getMessage () );
                                }
                            }
                            
                            thingyL = new list<Contact> () ;
                        }
                    }
                    
                    if ( thingyL.size () > 0 )
                    {
                        System.debug ( 'MERGE REMAINDER, running' ) ;
                        Database.MergeResult[] results = Database.merge ( masterContactMap.get ( x ), thingyL, false ) ;
                        
                        for ( Database.MergeResult res : results ) 
                        {
                            if ( res.isSuccess () ) 
                            {
                                System.debug ( 'MERGE SUCCESS!' ) ;
                            }
                            else
                            {
                                for ( Database.Error err : res.getErrors () )
                                    System.debug ( err.getMessage () );
                            }
                        }
                    }
				}
			}
		}
		else
		{
			System.debug ( 'No customer contacts found to merge' ) ;
		}
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal
	 *  -------------------------------------------------------------------------------------------------- */	
	public static void dedupeContactProspect ( List<ID> xL )
	{
		customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
		
		Contact emptyC = new Contact () ;
		List<String> emptyContactFields = DynamicObjectHandler.getUpdateableFields ( emptyC ) ;
		
		String cLs = DynamicObjectHandler.joinListQuoted ( xL, ',' ) ;
		
		String c1 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyC, 'ID IN ( ' + cLs + ' ) AND recordTypeID = \'' + prospectRecordTypeID + '\'' , 'ID' ) ;
		
        if ( debugCRAP )
			System.debug ( 'C1: ' + c1 ) ;
		
		list<String> sEL = new list<String> () ;
		list<String> sPL = new list<String> () ;
        list<ID> sIL = new list<ID> () ;
		list<Contact> cL = Database.query ( c1 ) ;
		
		map<String,Contact> cEMap = new Map<String,Contact> () ;
		
		System.debug ( 'Contact list size: ' + cL.size () ) ;
		
		if ( ( cL != null ) && ( cL.size () > 0 ) )
		{
			for ( Contact c : cL )
			{
				if ( String.isNotBlank ( getKeyByEmail ( c ) ) )
				{
					cEMap.put ( getKeyByEmail ( c ), c ) ;
					sEL.add ( c.Email ) ;
				}
				
				if ( String.isNotBlank ( getKeyByPhone ( c ) ) )
				{
					cEMap.put ( getKeyByPhone ( c ), c ) ;
					sPL.add ( c.Phone ) ;
				}
                
                sIL.add ( c.ID ) ;
			} 
			
			String oELs = null ;
			String oPLs = null ;
			
			// JIM's crazy Prospect check
			String thingy = ' (  recordTypeID = \'' + prospectRecordTypeID + '\' AND UploadID__c = NULL AND TaxID__c = NULL ) ' ;
			boolean foundData = false ;
			
			if ( ( sEL.size () > 0 ) && ( sPL.size () > 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += 'AND ( Email IN ( ' + oELs + ' ) OR Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () > 0 ) && ( sPL.size () == 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				
				thingy += 'AND ( Email IN ( ' + oELs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () == 0 ) && ( sPL.size () > 0 ) )
			{
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += 'AND ( Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			
			map<ID,Contact> contactMapToUpdate = new map<ID,Contact> () ;
			map<ID,Contact> masterContactMap = new map<ID,Contact> () ;
			map<ID,List<Contact>> duplicateContactMap = new map<ID,List<Contact>> () ;
			
			if ( foundData == true )
			{
                // FINAL
                String iDL = DynamicObjectHandler.joinListQuoted ( sIL, ',' ) ;
                thingy += 'AND ( ID NOT IN ( ' + iDL + ' ) )' ;
                
				String c2 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyC, thingy, 'ID' ) ;
		
		        if ( debugCRAP )
					System.debug ( 'C2: ' + c2 ) ;
				
				list<Contact> oL = Database.query ( c2 ) ;
				
				if ( ( oL != null ) && ( oL.size () > 0 ) )
				{
					for ( Contact c : oL )
					{
						boolean found = false ;
						
						System.debug ( 'Checking for :: ' + getKeyByEmail ( c ) ) ;
						
						if ( cEMap.containsKey ( getKeyByEmail ( c ) ) )
						{
							System.debug ( 'Email Key FOUND!' ) ;
							
							Contact cc = cEMap.get ( getKeyByEmail ( c ) ) ;
							
							if ( cc.ID != c.ID )
							{
								for ( String s : emptyContactFields )
								{
									if ( ( cc.get ( s ) == null ) && ( c.get ( s ) != null ) )
									{
									 	System.debug ( 'Setting "' + s + '", found difference' ) ;
									 	
									 	found = true ;
									 	cc.put ( s, c.get ( s ) ) ;
									}
								}
								
								if ( found == true )
								{
									if ( ! contactMapToUpdate.containsKey ( cc.ID ) )
										contactMapToUpdate.put ( cc.ID, cc ) ;
								}
								
								if ( ! masterContactMap.containsKey ( cc.ID ) )
									masterContactMap.put ( cc.ID, cc ) ;
								
								if ( ! duplicateContactMap.containsKey ( cc.ID ) )
									duplicateContactMap.put ( cc.ID, new list<Contact> () ) ;	
			
								duplicateContactMap.get ( cc.ID ).add ( c ) ;
							}
							else
							{
								System.debug ( 'Duplicate Master ID found, skipping' ) ;
							}
						}
						else
						{
							System.debug ( 'Email Key Not Found, checking phone!' ) ;
							
							System.debug ( 'Checking for :: ' + getKeyByPhone ( c ) ) ;
						
							if ( cEMap.containsKey ( getKeyByPhone ( c ) ) )
							{
								System.debug ( 'Phone Key FOUND!' ) ;
								
								Contact cc = cEMap.get ( getKeyByPhone ( c ) ) ;
								
								if ( cc.ID != c.ID )
								{
									for ( String s : emptyContactFields )
									{
										if ( ( cc.get ( s ) == null ) && ( c.get ( s ) != null ) )
										{
										 	System.debug ( 'Setting "' + s + '", found difference' ) ;
										 	
										 	found = true ;
										 	cc.put ( s, c.get ( s ) ) ;
										}
									}
									
									if ( found == true )
									{
										if ( ! contactMapToUpdate.containsKey ( cc.ID ) )
											contactMapToUpdate.put ( cc.ID, cc ) ;
									}
									
									if ( ! masterContactMap.containsKey ( cc.ID ) )
										masterContactMap.put ( cc.ID, cc ) ;
									
									if ( ! duplicateContactMap.containsKey ( cc.ID ) )
										duplicateContactMap.put ( cc.ID, new list<Contact> () ) ;	
				
									duplicateContactMap.get ( cc.ID ).add ( c ) ;
								}
								else
								{
									System.debug ( 'Duplicate Master ID found, skipping' ) ;
								}
							}
							else
							{
								System.debug ( 'Key Not Found!' ) ;
							}
						}
					}
				}
				else
				{
					System.debug ( 'No prospect records found to merge!' ) ;
				}
			}
			else
			{
				System.debug ( 'No valid emails or phone numbers found to merge on!' ) ;
			}
	
			if ( contactMapToUpdate.keyset ().size () > 0 )
			{
				list<Contact> contactListToUpdate = new list<Contact> () ;
				
				for ( String cID : contactMapToUpdate.keyset () )
					contactListToUpdate.add ( contactMapToUpdate.get ( cID ) ) ;
				
				try
				{
					System.debug ( 'Updating ' + contactListToUpdate.size () + ' contact records' ) ;
				
					update contactListToUpdate ;
				}
				catch ( DMLException e )
				{
					System.debug ( 'DML : ' + e.getMessage () ) ;
				}
			}
				
			if ( masterContactMap.keyset ().size () > 0 )
			{
				System.debug ( 'Merging into ' + masterContactMap.keyset ().size () + ' contact records' ) ;
				
				for ( ID x : masterContactMap.keyset () )
				{
                    list<Contact> dcL = duplicateContactMap.get ( x ) ;
                    
			        List<Contact> thingyL = new List<Contact> () ;
                    for ( Contact c : dcL )
                    {
                        thingyL.add ( c ) ;
		                if ( thingyL.size () == DEDUPE_MERGE_MAX )
                        {
                            System.debug ( 'MERGE MAX of ' + DEDUPE_MERGE_MAX + ', running' ) ;
                            Database.MergeResult[] results = Database.merge ( masterContactMap.get ( x ), thingyL, false ) ;
                            
                            for ( Database.MergeResult res : results ) 
                            {
                                if ( res.isSuccess () ) 
                                {
                                    System.debug ( 'MERGE SUCCESS!' ) ;
                                }
                                else
                                {
                                    for ( Database.Error err : res.getErrors () )
                                        System.debug ( err.getMessage () );
                                }
                            }
                            
                            thingyL = new list<Contact> () ;
                        }
                    }
                    
                    if ( thingyL.size () > 0 )
                    {
                        System.debug ( 'MERGE REMAINDER, running' ) ;
                        Database.MergeResult[] results = Database.merge ( masterContactMap.get ( x ), thingyL, false ) ;
                        
                        for ( Database.MergeResult res : results ) 
                        {
                            if ( res.isSuccess () ) 
                            {
                                System.debug ( 'MERGE SUCCESS!' ) ;
                            }
                            else
                            {
                                for ( Database.Error err : res.getErrors () )
                                    System.debug ( err.getMessage () );
                            }
                        }
                    }
				}
			}
		}
		else
		{
			System.debug ( 'No customer contacts found to merge' ) ;
		}
	}
    
	//  Key mapping from contact
	private static String getKeyByEmail ( Contact c )
	{
		String x = '' ;
		
		if ( ( String.isNotBlank ( c.Email ) ) &&
		     ( String.isNotBlank ( c.FirstName ) ) &&
		     ( String.isNotBlank ( c.LastName ) ) )
		{
			x = c.Email.toUpperCase () + DELIMITER + c.FirstName.toUpperCase () + DELIMITER + c.LastName.toUpperCase () ;
		}
		
		return x ;
	}
	
	//  Key mapping from contact
	private static String getKeyByPhone ( Contact c )
	{
		String x = '' ;
		
		if ( ( String.isNotBlank ( c.Phone ) ) &&
		     ( String.isNotBlank ( c.FirstName ) ) &&
		     ( String.isNotBlank ( c.LastName ) ) )
		{
			x = c.Phone.toUpperCase () + DELIMITER + c.FirstName.toUpperCase () + DELIMITER + c.LastName.toUpperCase () ;
		}
		
		return x ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
/*	@InvocableMethod ( label='DeDuplicate Contacts' )
	global static List<DeDuplicationActionFunctionExpandedResult> deduplicateContact ( List<DeDuplicationActionFunctionExpandedRequest> requests ) 
	{
		System.debug ( '------------------------- OLD DEDUPE STARTING ------------------------------' ) ;
		
		//  Not used?
		list<DeDuplicationActionFunctionExpandedResult> rL = new list<DeDuplicationActionFunctionExpandedResult> () ;
		
		if ( ( requests != null ) && ( requests.size () > 0 ) )
		{
			//  Get IDs out of Object
			list<ID> incLC = new List<ID> () ;
			list<ID> incLP = new List<ID> () ;
			
			for ( DeDuplicationActionFunctionExpandedRequest d : requests )
			{
                if ( String.isBlank ( d.cType ) )
                {
					incLC.add ( d.cID ) ;
                }
                else
                {
                    if ( d.cType.startsWithIgnoreCase ( 'C' ) )
						incLC.add ( d.cID ) ;
                        
                    else if ( d.cType.startsWithIgnoreCase ( 'P' ) )
						incLP.add ( d.cID ) ;
                }
			}
			
            if ( incLC.size () > 0 )
				dedupeContactCustomer ( incLC ) ;
            
            if ( incLP.size () > 0 )
				dedupeContactProspect ( incLP ) ;
		}
		
		System.debug ( '------------------------- OLD DEDUPE ENDING ------------------------------' ) ;
		return rL ;
	} */
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='DeDuplicate Contacts' )
	global static List<DeDuplicationActionFunctionExpandedResult> deduplicateContact ( List<DeDuplicationActionFunctionExpandedRequest> requests ) 
	{
		System.debug ( '------------------------- DEDUPE SETUP STARTING ------------------------------' ) ;
		
		//  Not used?
		list<DeDuplicationActionFunctionExpandedResult> rL = new list<DeDuplicationActionFunctionExpandedResult> () ;
		
		if ( ( requests != null ) && ( requests.size () > 0 ) )
		{
			//  Get IDs out of Object
			list<Contact> cL = new List<Contact> () ;
			
			for ( DeDuplicationActionFunctionExpandedRequest d : requests )
			{
                Contact c = new Contact () ;
                c.ID = d.cID ;
                c.DeDuplication_Flag__c = true ;
                
                cL.add ( c ) ;
			}
            
            if ( ( cL != null ) && ( cL.size () > 0 ) )
            {
                try
                {
                    update cL ;
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }
		}
		
		System.debug ( '------------------------- DEDUPE SETUP ENDING ------------------------------' ) ;
		return rL ;
	}
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class DeDuplicationActionFunctionExpandedRequest
	{
	    @InvocableVariable ( label='Contact ID' required=true )
	    public ID cID ;
        
	    @InvocableVariable ( label='Type' required=false )
	    public String cType ;
	}
	
	global class DeDuplicationActionFunctionExpandedResult
	{
	    @InvocableVariable
	    public ID cID ;
	}
}