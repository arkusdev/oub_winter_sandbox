public class USAePay_SOAP_Test_Mocks {
    
    public static HTTPResponse getAddCustomerResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:addCustomerResponse>'
            +   '<addCustomerReturn xsi:type="xsd:integer">1111111</addCustomerReturn>'
            +   '</ns1:addCustomerResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
    
    public static HTTPResponse getDeleteCustomerResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:deleteCustomerResponse>'
            + '<deleteCustomerReturn xsi:type="xsd:boolean">true</deleteCustomerReturn>'
            + '</ns1:deleteCustomerResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
    
    public static HTTPResponse getAddCustomerPaymentMethodResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:addCustomerPaymentMethodResponse>'
            + '<addCustomerPaymentMethodReturn xsi:type="xsd:integer">111</addCustomerPaymentMethodReturn>'
            + '</ns1:addCustomerPaymentMethodResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
    
    public static HTTPResponse getDeleteCustomerPaymentMethodResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:deleteCustomerPaymentMethodResponse>'
            + '<deleteCustomerPaymentMethodReturn xsi:type="xsd:boolean">true</deleteCustomerPaymentMethodReturn>'
            + '</ns1:deleteCustomerPaymentMethodResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
    
    public static HTTPResponse getRunTransactionResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:runTransactionResponse>'
         	+ '<runTransactionReturn xsi:type="ns1:TransactionResponse">'
            + '<AcsUrl xsi:type="xsd:string"/>'
            + '<AuthAmount xsi:type="xsd:double">250</AuthAmount>'
            + '<AuthCode xsi:type="xsd:string">513575</AuthCode>'
            + '<AvsResult xsi:type="xsd:string">Address: Match &amp; 5 Digit Zip: Match</AvsResult>'
            + '<AvsResultCode xsi:type="xsd:string">YYY</AvsResultCode>'
            + '<BatchNum xsi:type="xsd:integer">1</BatchNum>'
            + '<BatchRefNum xsi:type="xsd:integer">239166</BatchRefNum>'
            + '<CardCodeResult xsi:type="xsd:string">Match</CardCodeResult>'
            + '<CardCodeResultCode xsi:type="xsd:string">M</CardCodeResultCode>'
            + '<CardLevelResult xsi:type="xsd:string">Visa Traditional</CardLevelResult>'
            + '<CardLevelResultCode xsi:type="xsd:string">A</CardLevelResultCode>'
            + '<ConversionRate xsi:type="xsd:double">0</ConversionRate>'
            + '<ConvertedAmount xsi:type="xsd:double">0</ConvertedAmount>'
            + '<ConvertedAmountCurrency xsi:type="xsd:string">840</ConvertedAmountCurrency>'
            + '<CustNum xsi:type="xsd:integer">0</CustNum>'
            + '<Error xsi:type="xsd:string">Approved</Error>'
            + '<ErrorCode xsi:type="xsd:integer">0</ErrorCode>'
            + '<isDuplicate xsi:type="xsd:boolean">false</isDuplicate>'
            + '<Payload xsi:type="xsd:string"/>'
            + '<RefNum xsi:type="xsd:integer">3254239166</RefNum>'
            + '<Result xsi:type="xsd:string">Approved</Result>'
            + '<ResultCode xsi:type="xsd:string">A</ResultCode>'
            + '<Status xsi:type="xsd:string">Pending</Status>'
            + '<StatusCode xsi:type="xsd:string">P</StatusCode>'
            + '<VpasResultCode xsi:type="xsd:string"/>'
            + '</runTransactionReturn>'
            + '</ns1:runTransactionResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
    
     public static HTTPResponse getRunCustomerTransactionResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:runCustomerTransactionResponse>'
         	+ '<runCustomerTransactionReturn xsi:type="ns1:TransactionResponse">'
            + '<AcsUrl xsi:type="xsd:string"/>'
            + '<AuthAmount xsi:type="xsd:double">250</AuthAmount>'
            + '<AuthCode xsi:type="xsd:string">513575</AuthCode>'
            + '<AvsResult xsi:type="xsd:string">Address: Match &amp; 5 Digit Zip: Match</AvsResult>'
            + '<AvsResultCode xsi:type="xsd:string">YYY</AvsResultCode>'
            + '<BatchNum xsi:type="xsd:integer">1</BatchNum>'
            + '<BatchRefNum xsi:type="xsd:integer">239166</BatchRefNum>'
            + '<CardCodeResult xsi:type="xsd:string">Match</CardCodeResult>'
            + '<CardCodeResultCode xsi:type="xsd:string">M</CardCodeResultCode>'
            + '<CardLevelResult xsi:type="xsd:string">Visa Traditional</CardLevelResult>'
            + '<CardLevelResultCode xsi:type="xsd:string">A</CardLevelResultCode>'
            + '<ConversionRate xsi:type="xsd:double">0</ConversionRate>'
            + '<ConvertedAmount xsi:type="xsd:double">0</ConvertedAmount>'
            + '<ConvertedAmountCurrency xsi:type="xsd:string">840</ConvertedAmountCurrency>'
            + '<CustNum xsi:type="xsd:integer">0</CustNum>'
            + '<Error xsi:type="xsd:string">Approved</Error>'
            + '<ErrorCode xsi:type="xsd:integer">0</ErrorCode>'
            + '<isDuplicate xsi:type="xsd:boolean">false</isDuplicate>'
            + '<Payload xsi:type="xsd:string"/>'
            + '<RefNum xsi:type="xsd:integer">3254239166</RefNum>'
            + '<Result xsi:type="xsd:string">Approved</Result>'
            + '<ResultCode xsi:type="xsd:string">A</ResultCode>'
            + '<Status xsi:type="xsd:string">Pending</Status>'
            + '<StatusCode xsi:type="xsd:string">P</StatusCode>'
            + '<VpasResultCode xsi:type="xsd:string"/>'
            + '</runCustomerTransactionReturn>'
            + '</ns1:runCustomerTransactionResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
	
    public static HTTPResponse getVoidTransactionResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:voidTransactionResponse>'
         	+ '<voidTransactionReturn xsi:type="xsd:boolean">true</voidTransactionReturn>'
      		+ '</ns1:voidTransactionResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
    }
    
    public static HTTPResponse getRefundTransactionResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:refundTransactionResponse>'
         	+ '<refundTransactionReturn xsi:type="ns1:TransactionResponse">'
            + '<AcsUrl xsi:type="xsd:string"/>'
            + '<AuthAmount xsi:type="xsd:double">250</AuthAmount>'
            + '<AuthCode xsi:type="xsd:string">513575</AuthCode>'
            + '<AvsResult xsi:type="xsd:string">Address: Match &amp; 5 Digit Zip: Match</AvsResult>'
            + '<AvsResultCode xsi:type="xsd:string">YYY</AvsResultCode>'
            + '<BatchNum xsi:type="xsd:integer">1</BatchNum>'
            + '<BatchRefNum xsi:type="xsd:integer">239166</BatchRefNum>'
            + '<CardCodeResult xsi:type="xsd:string">Match</CardCodeResult>'
            + '<CardCodeResultCode xsi:type="xsd:string">M</CardCodeResultCode>'
            + '<CardLevelResult xsi:type="xsd:string">Visa Traditional</CardLevelResult>'
            + '<CardLevelResultCode xsi:type="xsd:string">A</CardLevelResultCode>'
            + '<ConversionRate xsi:type="xsd:double">0</ConversionRate>'
            + '<ConvertedAmount xsi:type="xsd:double">0</ConvertedAmount>'
            + '<ConvertedAmountCurrency xsi:type="xsd:string">840</ConvertedAmountCurrency>'
            + '<CustNum xsi:type="xsd:integer">0</CustNum>'
            + '<Error xsi:type="xsd:string">Approved</Error>'
            + '<ErrorCode xsi:type="xsd:integer">0</ErrorCode>'
            + '<isDuplicate xsi:type="xsd:boolean">false</isDuplicate>'
            + '<Payload xsi:type="xsd:string"/>'
            + '<RefNum xsi:type="xsd:integer">11111111</RefNum>'
            + '<Result xsi:type="xsd:string">Approved</Result>'
            + '<ResultCode xsi:type="xsd:string">A</ResultCode>'
            + '<Status xsi:type="xsd:string">Pending</Status>'
            + '<StatusCode xsi:type="xsd:string">P</StatusCode>'
            + '<VpasResultCode xsi:type="xsd:string"/>'
            + '</refundTransactionReturn>'
            + '</ns1:refundTransactionResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
        
    }
    
    public static HTTPResponse getCaptureTransactionResponseMock(){
        
        HTTPResponse response = new HTTPResponse();
        response.setStatus('SUCCESS');
        response.setStatusCode(200);
        
        String responseBody = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:usaepay" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'
            + '<SOAP-ENV:Body>'
            + '<ns1:captureTransactionResponse>'
         	+ '<captureTransactionReturn xsi:type="ns1:TransactionResponse">'
            + '<AcsUrl xsi:type="xsd:string"/>'
            + '<AuthAmount xsi:type="xsd:double">250</AuthAmount>'
            + '<AuthCode xsi:type="xsd:string">513575</AuthCode>'
            + '<AvsResult xsi:type="xsd:string">Address: Match &amp; 5 Digit Zip: Match</AvsResult>'
            + '<AvsResultCode xsi:type="xsd:string">YYY</AvsResultCode>'
            + '<BatchNum xsi:type="xsd:integer">1</BatchNum>'
            + '<BatchRefNum xsi:type="xsd:integer">239166</BatchRefNum>'
            + '<CardCodeResult xsi:type="xsd:string">Match</CardCodeResult>'
            + '<CardCodeResultCode xsi:type="xsd:string">M</CardCodeResultCode>'
            + '<CardLevelResult xsi:type="xsd:string">Visa Traditional</CardLevelResult>'
            + '<CardLevelResultCode xsi:type="xsd:string">A</CardLevelResultCode>'
            + '<ConversionRate xsi:type="xsd:double">0</ConversionRate>'
            + '<ConvertedAmount xsi:type="xsd:double">0</ConvertedAmount>'
            + '<ConvertedAmountCurrency xsi:type="xsd:string">840</ConvertedAmountCurrency>'
            + '<CustNum xsi:type="xsd:integer">0</CustNum>'
            + '<Error xsi:type="xsd:string">Approved</Error>'
            + '<ErrorCode xsi:type="xsd:integer">0</ErrorCode>'
            + '<isDuplicate xsi:type="xsd:boolean">false</isDuplicate>'
            + '<Payload xsi:type="xsd:string"/>'
            + '<RefNum xsi:type="xsd:integer">11111111</RefNum>'
            + '<Result xsi:type="xsd:string">Approved</Result>'
            + '<ResultCode xsi:type="xsd:string">A</ResultCode>'
            + '<Status xsi:type="xsd:string">Pending</Status>'
            + '<StatusCode xsi:type="xsd:string">P</StatusCode>'
            + '<VpasResultCode xsi:type="xsd:string"/>'
            + '</captureTransactionReturn>'
            + '</ns1:captureTransactionResponse>'
            + '</SOAP-ENV:Body>'
            + '</SOAP-ENV:Envelope>';
        
        response.setBody(responseBody);
        
        return response;
        
        
    }

}