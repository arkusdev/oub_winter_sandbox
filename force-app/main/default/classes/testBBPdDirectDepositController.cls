@isTest
public class testBBPdDirectDepositController {

    /* --------------------------------------------------------------------------------
	 * Setup, Ticket
	 * -------------------------------------------------------------------------------- */
	private static Ticket__c getTicket ( )
	{
		Ticket__c t = new Ticket__c() ;
        
        t.RecordTypeId = Schema.SObjectType.Ticket__c.RecordTypeInfosByDeveloperName.get('Direct_Deposit_Form').RecordTypeId; // '0123B000000c3o9QAA';
        t.First_Name__c = 'first';
        t.Last_Name__c = 'last';
        t.Account_Number__c = '1234567890123456';
        t.Account_Type__c = 'Primary Checking';
        t.Social_Security_Number__c = '111223333';
        t.Company_Name__c = 'OneUnited Bank';
        t.Employee_Id__c = 'employeeid#123';
        t.Direct_Deposit_Amount__c = '$2500';
		
		return t ;
	}
    
    /* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
    
    @isTest static void testGenerateTicket() {
        Contact c = getContact(1);
        insert c;
        
        Map<String, Object> info = new Map<String, Object>();
        
        info.put('companyName', 'lalala Company');
		info.put('employeeId', '123456');
        info.put('amountChoice', '100%');
        info.put('acctNum', '1234567890123456');
        info.put('acctType', 'checking');
        info.put('signature', true);
        
        test.startTest();
        
        String tid = BankBlackPaydayDirectDepositController.generateTicket(info);
        System.assertEquals( String.isBlank(tid), true );
        
        info.put('contactId', c.Id);
        tid = BankBlackPaydayDirectDepositController.generateTicket(info);
        System.assertEquals( String.isBlank(tid), false );
        
        test.stopTest(); 
    }
    
    @isTest static void testGetCurrentUserContactId() {
        // Setup user
        Profile testProfile = [SELECT Id 
                           FROM profile
                           WHERE Name = 'OneUnited Advocate Identity User' 
                           LIMIT 1];
        
        Account a = new Account(Name='Test Account');
        insert a;
        
		Contact c = new Contact();
        c.Email = 'test1@ouuser.com';
        c.LastName = 'UserAdvocateProgramDisplayController';
        c.AccountId = a.Id;
        insert c;
        
    	User testUser = new User(
            FirstName = 'Test1',
            LastName = 'UserAdvocateProgramDisplayController',
            Email = 'test1@ouuser.com',
            Username = 'test1user1@ouuser.com',
            Alias = 'dsplctrl',
            CommunityNickname = 'tuser1',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = testProfile.id,
            LanguageLocaleKey = 'en_US',
            IsActive=true,
            ContactId=c.Id ); 
        
        Test.startTest();
        
        String userContactId = BankBlackPaydayDirectDepositController.getCurrentUserContactId();
        
        System.assertEquals( userContactId, null );
        
        System.runAs(testUser)
        {
            userContactId = BankBlackPaydayDirectDepositController.getCurrentUserContactId();
            System.assertEquals( userContactId, c.Id );
        }
        
        Test.stopTest();
    }
    
    @isTest static void testGetContactById() {
        Contact c = getContact(1);
        insert c;
        
        Test.startTest();
        
        Contact c2 = BankBlackPaydayDirectDepositController.getContactById('123');
        Contact c3 = BankBlackPaydayDirectDepositController.getContactById(c.Id);
        
        System.assertEquals( c2, null );
        System.assertEquals( c3.Id, c.Id );
        
        Test.stopTest();
    }
    
    @isTest static void testGetFinancialAccountById() {
        Contact c = getContact(1);
        insert c;
        
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        fa.TAXRPTFORPERSNBR__c = c.Id;
        fa.MJACCTTYPCD__c = 'CK';
        insert fa;
        
        Test.startTest();
        
        Financial_Account__c fa2 = BankBlackPaydayDirectDepositController.getFinancialAccountById('123');
        Financial_Account__c fa3 = BankBlackPaydayDirectDepositController.getFinancialAccountById(fa.Id);
        
        System.assertEquals( fa2, null );
        System.assertEquals( fa3.ACCTNBR__c, '1234567890123456' );
        System.assertEquals( fa3.TAXRPTFORPERSNBR__c, c.Id );
        System.assertEquals( fa3.TAXRPTFORPERSNBR__r.TaxID__c, '666011111' );
        System.assertEquals( fa3.MJACCTTYPCD__c, 'CK' );
        
        Test.stopTest();
    }
    
    @isTest static void testGetFinancialAccountByAcctNum() {
        Contact c = getContact(1);
        insert c;
        
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        fa.TAXRPTFORPERSNBR__c = c.Id;
        fa.MJACCTTYPCD__c = 'CK';
        insert fa;
        
        Test.startTest();
        
        Financial_Account__c fa2 = BankBlackPaydayDirectDepositController.getFinancialAccountByAcctNum('123');
        Financial_Account__c fa3 = BankBlackPaydayDirectDepositController.getFinancialAccountByAcctNum('1234567890123456');
        
        System.assertEquals( fa2, null );
        System.assertEquals( fa3.ACCTNBR__c, '1234567890123456' );
        System.assertEquals( fa3.TAXRPTFORPERSNBR__c, c.Id );
        System.assertEquals( fa3.TAXRPTFORPERSNBR__r.TaxID__c, '666011111' );
        System.assertEquals( fa3.MJACCTTYPCD__c, 'CK' );
        
        Test.stopTest();
    }
    
    @isTest static void testGetExistingTicket() {
        Contact c = getContact(1);
        insert c;
        
        Ticket__c t = getTicket();
        t.Contact__c = c.Id;
        insert t;
        
        Test.startTest();
        
        Ticket__c t2 = BankBlackPaydayDirectDepositController.getExistingTicket( c.Id, '123' );
        Ticket__c t3 = BankBlackPaydayDirectDepositController.getExistingTicket( c.Id, '1234567890123456' );
        
        System.assertEquals( t2, null );
        System.assertEquals( t3.Id, t.Id );
        System.assertEquals( t3.RecordTypeId, Schema.SObjectType.Ticket__c.RecordTypeInfosByDeveloperName.get('Direct_Deposit_Form').RecordTypeId );
        System.assertEquals( t3.First_Name__c, 'first' );
        System.assertEquals( t3.Last_Name__c, 'last' );
        System.assertEquals( t3.Account_Type__c, 'Primary Checking' );
        System.assertEquals( t3.Social_Security_Number__c, '111223333' );
        System.assertEquals( t3.Company_Name__c, 'OneUnited Bank' );
        System.assertEquals( t3.Employee_Id__c, 'employeeid#123' );
        System.assertEquals( t3.Direct_Deposit_Amount__c, '$2500' );
        
        Test.stopTest();
    }
    
}