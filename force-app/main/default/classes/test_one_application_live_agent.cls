@isTest
public class test_one_application_live_agent 
{
	static testmethod void testOne ()
    {
        //  SETTINGS!
		one_test_new.insertCustomSetting () ;
        
		one_application_live_agent oala = new one_application_live_agent () ;
        one_settings__c settings = one_settings__c.getInstance ( 'settings' ) ;
        
        //  CONFIRM ?!?
        system.assertEquals ( settings.Live_Agent_Enable__c, true ) ;
        system.assertEquals ( settings.Live_Agent_Chat_URL__c, oala.liveAgentChatURL ) ;
        system.assertEquals ( settings.Live_Agent_JS_URL__c, oala.liveAgentJSURL ) ;
        system.assertEquals ( settings.Live_Agent_Company_ID__c, oala.liveAgentCompanyID ) ;
        system.assertEquals ( settings.Live_Agent_Deployment_ID__c, oala.liveAgentDeploymentID ) ;
    }
}