public with sharing class one_application_success_controller 
{

	public String typeParameter{get;set;}
	public String appId{get;set;}
	public String appField{set;get;}
	public one_application__c app{get;set;}
	
	public boolean rerenderOne{set;get;}
	public boolean rerenderTwo{set;get;}
	public boolean rerenderThree{set;get;}
	public boolean rerenderFour{set;get;}
	
	public boolean rerenderAppId{set;get;}

	public boolean allowSendEmail{get;set;}
	public boolean allowSendEmailCoAppl{get;set;}
	
	public String entCnt_email{get;set;}
	public String refCnt_email{get;set;}
	
	private Double trialLimit ;
	
 	//  QR Code to allow site transfer to mobile device
 	public String qrCode 
 	{ 
 		get; 
 		private set; 
 	}
 	
	//  Additional language based on decision code
    public String codeAdditionalLanguage
    {
        get ;
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
    //  Link to additional downloads
    public PageReference attachmentDownloadLink
    {
    	get ;
    	private set ;
    }
    
	public one_application_success_controller()
	{
		
		trialLimit = one_settings__c.getInstance('settings').ACH_Validation_Threshold__c ;
		
		entCnt_email = null;
		refCnt_email = null;
		
		allowSendEmail = false;
		allowSendEmailCoAppl = false;
		
		app = null;

		system.debug('---- ' + Apexpages.currentPage().getHeaders().get('Referer'));
		system.debug('---- ' + (Apexpages.currentPage().getHeaders().get('Referer') + '').toLowerCase().endsWith('one_application_end'));
		
		rerenderOne = false;
		rerenderTwo = false;
		rerenderThree = false ;
		rerenderFour = false ;
		
		rerenderAppId = false;
		if(system.currentPageReference().getParameters().get('type') != null){	
			typeParameter = System.currentPageReference().getParameters().get('type');
		}
		if(system.currentPageReference().getParameters().get('aid') != null){	
			appId = System.currentPageReference().getParameters().get('aid');
		}
		if(appId != null && appId != ''){
			List<one_application__c> appAuxs = new List<one_application__c>();
			try{
				appAuxs = [
				select FIS_Application_ID__c, FIS_Decision_Code__c, FIS_Credit_Limit__c, Upload_Attachment_key__c, 
				Email_Address__c, Co_Applicant_Email_Address__c, Entered_By_Contact__c, Entered_By_Contact__r.Email, Referred_By_Contact__c, Referred_By_Contact__r.Email 
				from one_application__c where id = : appId limit 1
				];
			}
			catch(Exception e)
			{appAuxs = new List<one_application__c>();}
			
			if(appAuxs.size() > 0){
				app = appAuxs[0];
				if(app.Entered_By_Contact__c != null){
					entCnt_email = app.Entered_By_Contact__r.Email;
				}			
				if(app.Referred_By_Contact__c != null){
					refCnt_email = app.Referred_By_Contact__r.Email;
				}			
			}else{
				app = null;
			}
			if(app != null){
				appField = app.FIS_Application_ID__c;
				rerenderAppId = true;

				if((Apexpages.currentPage().getHeaders().get('Referer') + '').toLowerCase().endsWith('one_application_end')
					|| (Apexpages.currentPage().getHeaders().get('Referer') + '').toLowerCase().endsWith('one_application_start')
				){	
					//if(app.FIS_Application_ID__c != null && app.FIS_Application_ID__c != ''){
						if(app.Email_Address__c != null && app.Email_Address__c != ''){
							allowSendEmail = true;
						}
						if(app.Co_Applicant_Email_Address__c != null && app.Co_Applicant_Email_Address__c != ''){
							allowSendEmailCoAppl = true;
						}
					//}
				}
			}
		}
		
		if(typeParameter != null)
		{
			if(appId != null && appId != '')
			{
				System.debug ( app.FIS_Application_ID__c + ' == ' + typeParameter ) ;
				System.debug ( app.FIS_Credit_Limit__c + ' == ' + trialLimit ) ;
				
				//  Check for Additional Information
				if ( typeParameter.equalsIgnoreCase ( 'idv' ) )
				{
					rerenderOne = false ;
					rerenderTwo = false ;
					rerenderThree = true ;
					rerenderFour = false ;
					
					// QR code for display
					qrCode = one_utils.getDocUploadQRCode ( appId, app.Upload_Attachment_key__c, 125, 125 ) ;

					//  Get the additional language needed
					codeAdditionalLanguage = one_utils.codesAdditionalLanguage.get ( app.FIS_Decision_Code__c ) ;
					
					//  Get the list of documents to display - assumes at this point they haven't uploaded any!
					docTypeList = one_utils.getDocTypeList ( app.FIS_Decision_Code__c ) ;
					
					//  Set up the page reference used for the link
		    		PageReference pr = Page.one_application_file_upload ;
			        pr.getParameters().put ('aid', appId ) ;
			        pr.getParameters().put ('kid', app.Upload_Attachment_key__c ) ;
		    		
		    		attachmentDownloadLink = pr ;
				}
				//  Check for over the trial limit
				else if ( ( app.FIS_Application_ID__c == typeParameter ) && ( app.FIS_Credit_Limit__c >= trialLimit ) )
				{
					system.debug ( '----> Trial deposit' ) ;
					rerenderOne = false;
					rerenderTwo = false;
					rerenderThree = false ;
					rerenderFour = true ;
				}
				//  Original logic here
				else
				{
					system.debug ( '----> Original Logic' ) ;
					rerenderOne = (app.FIS_Application_ID__c == typeParameter ? false : true);
					rerenderTwo = (app.FIS_Application_ID__c == typeParameter ? true : false);
					rerenderThree = false ;
					rerenderFour = false ;
				}
			}
		}

		entCnt_email = null;
		refCnt_email = null;		
		allowSendEmail = false;
		allowSendEmailCoAppl = false;

		system.debug('@@ appField '+ appField);
		
		//  A hacking we will go! - to show a specific panel, uncomment the below
		//  rerenderOne = true ;
		//  rerenderTwo = true ;
		//  rerenderThree = true ;
		//  rerenderFour = true ;
	}
	
	public void removeEmailScript()
	{
		if(appId != null && appId != '')
		{
			try
			{
				EmailFunctions.sendUNITYVisaWelcomeEmails(appId);
			}
			catch(Exception e)
			{
				system.debug('=== ERROR sendUNITYVisaWelcomeEmails: ' + e.getMessage());
			}
		}

		allowSendEmail = false;
		allowSendEmailCoAppl = false;
		entCnt_email = '';
		refCnt_email = '';
	}
}