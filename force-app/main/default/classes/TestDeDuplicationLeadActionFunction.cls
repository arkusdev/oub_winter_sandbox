@isTest
public class TestDeDuplicationLeadActionFunction 
{
	private static Lead getLeadEmail ( Integer i )
	{
    	Lead l = new Lead () ;
    	
        l.Company = 'Nyet' ;
    	l.FirstName = 'test' + i ;
    	l.LastName = 'user' + i ;
    	l.Email = 'test' + i + '@user' + i + '.com' ;
    	
    	return l ;
    }
    
	private static Lead getLeadPhone ( Integer i )
	{
    	Lead l = new Lead () ;
    	
        l.Company = 'Nyet' ;
    	l.FirstName = 'test' + i ;
    	l.LastName = 'user' + i ;
    	l.Phone = '00' + i + '-00' + i + '-000' + i ;
    	
    	return l ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, NADA
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Zero ()
	{
		System.debug ( '----------------------------------- TEST ZERO -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Main list to send in
		list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> dafrL = new list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> () ;
		
		//  Run (empty) function!
		DeDuplicationLeadActionFunction.deduplicateLead ( dafrL ) ;
		
		Test.stopTest () ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Single Lead
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Zero_One ()
	{
		System.debug ( '----------------------------------- TEST ONE -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Main list to send in
		list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> dafrL = new list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> () ;
		
		//  First Lead
		Lead cc1 = getLeadEmail ( 1 ) ;
		insert cc1 ;
		
        // ?!?
        cc1.phone = '00' + 1 + '-00' + 1 + '-000' + 1 ;
        update cc1 ;
        
		//  Add to list
		DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest dafr1 = new DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest () ;
		dafr1.xID = cc1.ID ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Run function!
		DeDuplicationLeadActionFunction.deduplicateLead ( dafrL ) ;
	}

    //  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Lead with Email
	//  --------------------------------------------------------------------------------------
	static testmethod void test_One ()
	{
		System.debug ( '----------------------------------- TEST ONE -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Main list to send in
		list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> dafrL = new list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> () ;
		
		//  First Lead
		Lead cc1 = getLeadEmail ( 1 ) ;
    	cc1.phone = '00' + 1 + '-00' + 1 + '-000' + 1 ;
		insert cc1 ;
		
		//  Duplicate Lead
		Lead cp11 = getLeadEmail ( 1 ) ;
    	cp11.Work_Email__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate lead, second
		Lead cp12 = getLeadEmail ( 1 ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest dafr1 = new DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest () ;
		dafr1.xID = cc1.ID ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Second lead
		Lead cc2 = getLeadEmail ( 2 ) ;
		insert cc2 ;
		
		//  Not so duplicate, second lead
		Lead cp21 = getLeadEmail ( 1 ) ;
		cp21.FirstName = 'GoWay' ;
    	cp21.Description = 'Nyet Mergy' ;
		insert cp21 ;

		//  Add to list
		DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest dafr2 = new DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest () ;
		dafr2.xID = cc2.ID ;
		
		dafrL.add ( dafr2 ) ;
		
		//  Run function!
		DeDuplicationLeadActionFunction.deduplicateLead ( dafrL ) ;
		
		Test.stopTest () ;
		
		//  Check to see that stuff merged
		Lead l1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, Work_Email__c
			FROM Lead
			WHERE ID = :cc1.ID
		] ;
		
		if ( l1 != null )
		{
			System.assertEquals ( l1.Description, cp11.Description ) ;
			System.assertEquals ( l1.Title, cp12.Title ) ;
			System.assertEquals ( l1.Work_Email__c, cp11.Work_Email__c ) ;
		}
		
		//  Check to see that stuff did not merge
		Lead l2 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, Work_Email__c
			FROM Lead
			WHERE ID = :cc2.ID
		] ;
		
		if ( l2 != null )
		{
			System.assertNotEquals ( l2.Description, cp21.Description ) ;
		}
	}
        
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Lead with Phone
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Two ()
	{
		System.debug ( '----------------------------------- TEST TWO -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Main list to send in
		list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> dafrL = new list<DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest> () ;
		
		//  First Lead
		Lead cc1 = getLeadPhone ( 1 ) ;
		insert cc1 ;
		
		//  Duplicate lead
		Lead cp11 = getLeadPhone ( 1 ) ;
		cp11.Work_Email__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Lead cp12 = getLeadPhone ( 1 ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest dafr1 = new DeDuplicationLeadActionFunction.DeDuplicationLeadActionFunctionRequest () ;
		dafr1.xID = cc1.ID ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Run function!
		DeDuplicationLeadActionFunction.deduplicateLead ( dafrL ) ;
		
		Test.stopTest () ;
		
		//  Check to see that stuff merged
		Lead l1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, Work_Email__c, Phone
			FROM Lead
			WHERE ID = :cc1.ID
		] ;
		
		if ( l1 != null )
		{
			System.assertEquals ( l1.Description, cp11.Description ) ;
			System.assertEquals ( l1.Title, cp12.Title ) ;
			System.assertEquals ( l1.Work_Email__c, cp11.Work_Email__c ) ;
		}
	}
}