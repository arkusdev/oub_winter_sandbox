@isTest
public class TestEverFiCalloutSchedule 
{
	/* --------------------------------------------------------------------------------
	 * Setup, Settings
	 * -------------------------------------------------------------------------------- */
    private static void setCustomSettings ()
    {
        EverFi_Settings__c efs = new EverFi_Settings__c () ;

        efs.Grant_Type__c = 'foo' ;
        efs.Client_ID__c = 'abc123' ;
        efs.Client_Secret__c = 'youandme' ;
        efs.Login_URL__c = 'https://www.oneunited.com/token' ;

        efs.User_URL__c = 'https://www.oneunited.com/users' ;
        efs.Location_URL__c = 'https://www.oneunited.com/locations' ;
        efs.Program_User_URL__c = 'https://www.oneunited.com/program_users' ;
        
        efs.Email__c = 'test@user.com' ;

        insert efs ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Defaults
	 * -------------------------------------------------------------------------------- */
    private static void setDefaults ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        insert a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Account
	 * -------------------------------------------------------------------------------- */
    private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.Name = 'Test' + i ;
        return a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Account a, Integer i )
	{
		Contact c = new Contact () ;
        
        c.AccountId = A.ID ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
    
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
	static testmethod void something ()
	{
        setCustomSettings () ;
        setDefaults () ; // Required because TRIGGER
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        
		Test.startTest();

		EverFiCalloutSchedule myClass = new EverFiCalloutSchedule ();   
		String chron = '0 0 23 * * ?';        
		system.schedule('Test Sched', chron, myClass);
		
		Test.stopTest();
	}
}