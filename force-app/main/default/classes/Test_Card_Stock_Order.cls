@isTest
public with sharing class Test_Card_Stock_Order 
{
	static testmethod void testOneTicketEmpty ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Card_Stock_Order cso = new Card_Stock_Order ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( cso.dStepNumber, 1000 ) ;
		
		//  Empty ticket
		Ticket__c t = cso.ticket ;
		
		//  Submit
		cso.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( cso.dStepNumber, 1001 ) ;		
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testTwoTicketBad ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Card_Stock_Order cso = new Card_Stock_Order ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( cso.dStepNumber, 1000 ) ;
		
		//  Load the ticket with bad data
		Ticket__c t = cso.ticket ;
		t.Zip_Code__c = 'xyzab' ;
		t.Email_Address__c = 'none@none.com' ;
        cso.dateOfBirth = 'blah blah blah' ;
        t.Last_4_Card_Number__c = '1234567890' ;
        
		//  Submit
		cso.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( cso.dStepNumber, 1001 ) ;		
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testThreeTicketGoodNoContact ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Card_Stock_Order cso = new Card_Stock_Order ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( cso.dStepNumber, 1000 ) ;
		
		//  Load the ticket
		Ticket__c t = cso.ticket ;
		
		t.Street_Address__c = '123 Test Street' ;
		t.City__c = 'Test City' ;
		t.State__c = 'CA' ;
		t.Zip_Code__c = '13245-6597' ;
		
		t.First_Name__c = 'Test' ;
		t.Last_Name__c = 'User' ;
		t.Email_Address__c = 'test@user.com' ;
        
		cso.dateOfBirth = '12/31/2095' ;
        t.Last_4_Card_Number__c = '9876' ;
        t.Card_Type__c = 'Debit' ;
        cso.flipThing () ;
        System.assertEquals (cso.dPanel, 1000) ;
        cso.selectCard = 'Mona' ;
        
		//  Submit
		cso.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( cso.dStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void testFourTicketGoodContact ()
	{
        Contact c = new Contact () ;
        c.FirstName = 'Test' ;
        c.LastName = 'User' ;
        c.Email = 'Test.User@tester.com' ;
        c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
        
        insert c ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Card_Stock_Order cso = new Card_Stock_Order ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( cso.dStepNumber, 1000 ) ;
		
		//  Load the ticket
		Ticket__c t = cso.ticket ;
		
		t.Street_Address__c = '123 Test Street' ;
		t.City__c = 'Test City' ;
		t.State__c = 'CA' ;
		t.Zip_Code__c = '13245-6597' ;
		
		t.First_Name__c = c.FirstName ;
		t.Last_Name__c = c.LastName ;
		t.Email_Address__c = c.Email ;
        
		cso.dateOfBirth = '12/31/2095' ;
        t.Last_4_Card_Number__c = '9876' ;
        t.Card_Type__c = 'ATM' ;
        cso.flipThing () ;
        System.assertEquals (cso.dPanel, 2000) ;
        cso.selectCard = 'Mona' ;
        
		//  Submit
		cso.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( cso.dStepNumber, 2000 ) ;
        System.assertNotEquals ( cso.confirmationNumber, null ) ;
        
		//  End of testing
		Test.stopTest () ;
	}
}