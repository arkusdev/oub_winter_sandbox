public without sharing virtual class ApplicationHelper 
{
    // -----------------------------------------------------------------------
    // SETUP
    // -----------------------------------------------------------------------
    protected ApplicationHelper() {}

    @TestVisible private static ApplicationHelper instance = null;

    // -----------------------------------------------------------------------
    // INSTANCE
    // -----------------------------------------------------------------------
    public static ApplicationHelper getInstance () 
    {
        if (instance == null) 
        {
            instance = new ApplicationHelper () ;
        }
        return instance ;
    }
    
    // -----------------------------------------------------------------------
    // GENERICS
    // -----------------------------------------------------------------------
    public void insertObject ( SOBject x )
    {
        list<SObject> xL = new list<SOBject> () ;
        xL.add ( x ) ;
        
        insertObjects ( xL ) ;
    }

    public void insertObjects ( list<SObject> xL )
    {
        insert xL ;
    }
    
    public void updateObject ( SOBject x )
    {
        list<SObject> xL = new list<SOBject> () ;
        xL.add ( x ) ;
        
        updateObjects ( xL ) ;
    }

    public void updateObjects ( list<SObject> xL )
    {
        update xL ;
    }
    
    public void deleteObject ( SOBject x )
    {
        list<SObject> xL = new list<SOBject> () ;
        xL.add ( x ) ;
        
        deleteObjects ( xL ) ;
    }

    public void deleteObjects ( list<SObject> xL )
    {
        delete xL ;
    }
    
    // -----------------------------------------------------------------------
    // SPECIFICS
    // -----------------------------------------------------------------------
    public void upsertApplication ( one_application__c x )
    {
        list<one_application__c> xL = new list<one_application__c> () ;
        xL.add ( x ) ;
        
        upsertApplications ( xL ) ;
    }

    public void upsertApplications ( list<one_application__c> xL )
    {
        upsert xL ;
    }
    
    public void upsertLead ( Lead x )
    {
        list<Lead> xL = new list<Lead> () ;
        xL.add ( x ) ;
        
        upsertLeads ( xL ) ;
    }

    public void upsertLeads ( list<Lead> xL )
    {
        upsert xL ;
    }
    
    public void upsertOnlineDepositAccount ( Online_Deposit_Application_Account__c odaa )
    {
     	list<Online_Deposit_Application_Account__c> odaaL = new list<Online_Deposit_Application_Account__c> () ;
        odaaL.add ( odaa ) ;
        
        upsertOnlineDepositAccounts ( odaaL ) ;
    }
    
    public void upsertOnlineDepositAccounts ( list<Online_Deposit_Application_Account__c> odaaL )
    {
        upsert odaaL ;
    }
}