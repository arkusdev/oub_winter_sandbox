@isTest
public with sharing class TestPromotionInclude 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
	
    public static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group () ;
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c ();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Referral_Tracking_Hold_Days__c = 7 ;
        s.Rakuten_Tracking_ID__c = 'TESTID' ;
        s.Rates_URL__c = 'https://www.test.com' ;
        
        insert s;
    }
    
    private static oneOffers__c getOffer ( String thingy, String promoCode )
    {
    	oneOffers__c oo = new oneOffers__c () ;
    	oo.Name = 'Test Offer' ;
    	oo.External_Offer_ID__c = 'abc,xyz,' + thingy + ',pbq' ;
    	oo.Dollar_Amount__c = 25.00 ;
    	oo.Promotion_Code__c = promoCode ;
    	oo.Start_Date__c = Date.Today () - 1 ;
    	oo.End_Date__c = Date.Today () + 1 ;
    	oo.Active__c = true ;
    	oo.Plan__c = '999' ;
    	oo.Product__c = 'XTZ' ;
    	oo.SubProduct__c = '000' ;
    	
    	insert oo ;
    	return oo ;
    }

	static testmethod void testPromotionINCLUDE ()
	{
		insertCustomSetting () ;
		
		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		
		Test.startTest () ;
		
		//  Parameters
		ApexPages.currentPage().getParameters().put ( 'prcd', 'JIMTEST' ) ;

		//  Testing...
		PromotionIncludeController pic = new PromotionIncludeController () ;
		
		//  asserts
		System.assertEquals ( pic.validPromoCode, true ) ;
		System.assertEquals ( pic.promoCode, 'JIMTEST' ) ;
		
		Test.stopTest () ;
	}
}