global class batch_CreditCardPromotionTicket_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_CreditCardPromotionTicket b = new batch_CreditCardPromotionTicket () ;
		Database.executeBatch ( b ) ;
   }
}