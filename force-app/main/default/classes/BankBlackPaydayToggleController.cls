public with sharing class BankBlackPaydayToggleController {
    
    // Returns Payday Response objects associated with a particular Financial Account
    // only pulls records of a certain type that were created today and weren't processed
    public static List<Payday_Response__c> getPaydayResponses(String faId, String type) {
        try
        {
            Date dt = Date.today();
            
            List<Payday_Response__c> responseList = new List<Payday_Response__c>();
            responseList = [SELECT Id, OptIn_Status__c, Processing_Status__c, Response_Date__c, Financial_Account__c, Comms_Channels__c 
                           FROM Payday_Response__c
                           WHERE Financial_Account__c = :faId
                           AND Day_Only(Response_Date__c) = :dt 
                           AND Processing_Status__c = 'New'
                           AND RecordTypeId = :type 
                           ORDER BY CreatedDate Desc, ID Desc ]; // in order to quickly determine most recent toggle or comms channel value
            
            return responseList;
        }
        catch ( Exception e )
        {
            System.debug('toggle --- getPaydayResponses --- error: ' + e);
            return null;
        }
    }
    
    @AuraEnabled
    public static void createPaydayResponse(String faId, Boolean check, String commsOption, String acctNbr) {
        try
        {
            
            System.debug('Current sf fa record: ' + faId);
            System.debug('Current sf commsOption: ' + commsOption);
            
            Payday_Response__c pdr = new Payday_Response__c();
            pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId; 
            pdr.Response_Date__c = Datetime.now();
            pdr.Account_Number_OB__c = acctnbr ;
            if ( faId != null && String.isNotBlank(faId) )
            {
            	pdr.Financial_Account__c = faId;
            }
            pdr.Contact__c = getCurrentUserContactId();
            
            if (String.isNotBlank(commsOption)) {
                pdr.Comms_Channels__c = commsOption;
            }
            
            if ( check )
            {
                String foo = 'Pending' ;
                
                if ( String.isNotBlank ( faId ) )
                {
                    try
                    {
                        Financial_Account__c fa = [
                            SELECT ID, BankBlack_Payday_Comms_OptIn__c, BankBlack_Payday_OptIn__c
                            FROM Financial_Account__c
                            WHERE ID = :faId
                            LIMIT 1
                        ] ;
                        
                        if ( String.isNotBlank ( fa.BankBlack_Payday_OptIn__c ) && ( fa.BankBlack_Payday_OptIn__c.equalsIgnoreCase ( 'Active' ) ) )
                            foo = 'Active' ;
                    }
                    catch ( DMLException e )
                    {
                        // ?
                    }
                }
                    
                pdr.OptIn_Status__c = foo ;
            }
            else if ( !check )
            {
                pdr.OptIn_Status__c = 'Inactive';
                pdr.Comms_Channels__c = 'No notifications';
            }
            
            insert pdr;
            
            if (String.isNotBlank(pdr.Comms_Channels__c)  && 
                String.isNotBlank(faId) && 
                String.isNotBlank(pdr.OptIn_Status__c) && 
                (! pdr.OptIn_Status__c.equalsIgnoreCase('Inactive') ) ) {
                createPaydayCommsResponse(faId,pdr.Comms_Channels__c,acctNbr);
            }
        }
        catch ( Exception e )
        {
            System.debug('toggle --- createPaydayResponse --- error: ' + e);
        }
    }
    
    // If the user has toggled the switch an even # of times today,
    // i.e. indicates no changes need to be made to BankBlack_Payday_OptIn__c on Financial_Account__c object to Luigi
    // Handles even toggles by changing the Processing_Status__c field on Payday_Response__c objects to "Processed"
    @AuraEnabled
    public static void processToggleEven(String faId) {
        try
        {
            //Boolean shouldProcess = false;
            List<Payday_Response__c> paydayResponses = getPaydayResponses(faId, Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId ); // Payday OptIn default type for toggle
    		
            /*
            if ( ( paydayResponses.size() > 0 ) && ( ( math.mod( paydayResponses.size(),2 ) == 0 ) ) )
            {
                shouldProcess = true;
            }
            
            if ( shouldProcess )
            {
			*/
            Integer i=0;
                for ( Payday_Response__c pdr : paydayResponses )
                {
                    if (i !=0 ) {
                        pdr.Processing_Status__c = 'Canceled';
                    }
                    i++;
                }
                
                update paydayResponses;
            //}
        }
        catch ( Exception e )
        {
            System.debug('toggle --- processToggleEven --- error: ' + e);
        }
    }
    
    public static String getCurrentUserContactId() {
        try
        {
            User user = [SELECT ContactID FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
            return user.ContactId;
        }
        catch ( Exception e )
        {
            System.debug('toggle --- getCurrentUserContactId --- error: ' + e);
            return '';
        }
    }

    // don't judge me, Luigi
    // these were originally in 2 different components, and I didn't want to put more work into combining them into 1 method
    @AuraEnabled
    public static String createPaydayCommsResponse(String faId, String commsOption, String acctNbr) {
        try
        {
            Payday_Response__c pdr = new Payday_Response__c();
            pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId; // default is Payday OptIn (for the toggle)
            pdr.OptIn_Status__c = 'Pending';
            pdr.Response_Date__c = Datetime.now();
            pdr.Account_Number_OB__c = acctnbr ;
            if ( faId != null && String.isNotBlank(faId) )
            {
            	pdr.Financial_Account__c = faId;
            }
            pdr.Comms_Channels__c = commsOption;
            pdr.Contact__c = getCurrentUserContactId();
            insert pdr;
            
            if ( faId != null && String.isNotBlank(faId) &&  pdr.Id != null )
            {
            	processComms(faId, pdr.Id);
            }
            
            return pdr.Id;
        }
        catch ( Exception e )
        {
            System.debug('toggle --- createPaydayCommsResponse --- error: ' + e);
            return '';
        }
    }
    
    // Changes Processing_Status__c field to 'Processed' for all other Payday Comms Responses
    // except for the most recently created one
    @AuraEnabled
    public static void processComms(String faId, String pdrId) {
        try
        {
            List<Payday_Response__c> paydayResponses = getPaydayResponses( faId, Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId ); // only the comms optIn record type responses
            
            Integer i=0;
            for ( Payday_Response__c pdr : paydayResponses )
            {
                if (( pdr.Id != pdrId ) && (i!=0))
                {
                    pdr.Processing_Status__c = 'Canceled';
                    System.debug('Setting to Canceled :: ' + pdr.Id);
                }
                i++;
            }
            
            update paydayResponses;
        }
        catch ( Exception e )
        {
            System.debug('toggle --- processComms --- error: ' + e);
        }
    }
}