@isTest
public class TestReferralCodeGenerator 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.name = 'Test' + i ;
        
        return a ;
    }
    
	private static Contact getContact ( Account a, Integer i )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'User' + i ;
		c.Email = 'test' + i + '@user' + i + '.com' ;
        c.AccountId = a.ID ;
		
		return c ;
	}
	
	static testmethod void ONE ()
    {
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;

		list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( a1 , 1 ) ;
        cL.add ( c1 ) ;
        
        Contact c2 = getContact ( a1 , 2 ) ;
        cL.add ( c2 ) ;
        
        Contact c3 = getContact ( a1 , 3 ) ;
        c3.FirstName += 'XXX' ;
        cL.add ( c3 ) ;
        
        Contact c4 = getContact ( a1 , 4 ) ;
        c4.LastName += 'XXX' ;
        cL.add ( c4 ) ;
        
        insert cL ;
        
        Test.startTest () ;
        
        ReferralCodeGenerator.generateReferralCodes ( cL ) ;
        
        Test.stopTest () ;
    }
    
	static testmethod void TWO ()
    {
        Test.startTest () ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;

        Contact c1 = getContact ( a1 , 1 ) ;
        c1.FirstName += 'X' ;
        c1.LastName += 'X' ;

        insert c1 ;
        
        ReferralCodeGenerator.generateReferralCode ( c1 ) ;
        ReferralCodeGenerator.generateReferralCode ( c1 ) ; // REPEAT!
        
        list<Referral_Code__c> rcL = [
            SELECT ID, Name
            FROM Referral_Code__c
            WHERE Contact__c = :c1.ID
        ] ;
        
        System.assertEquals ( rcL.size (), 2 ) ;
        System.assertNotEquals ( rcL [ 0 ].Name, rcL [ 1 ].Name ) ;
        System.assertEquals ( rcL [ 0 ].Name.length (), 6 ) ;
        System.assertEquals ( rcL [ 1 ].Name.length (), 6 ) ;
        
        Test.stopTest () ;
    }
    
	static testmethod void DUPE ()
    {
        Test.startTest () ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;

        list<contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( a1 , 1 ) ;
        c1.FirstName += 'Xand' ;
        c1.LastName += 'Xor' ;
        c1.TaxID__c = '111223333' ;
        cL.add ( c1 ) ;
        
        Contact c2 = getContact ( a1 , 1 ) ;
        c2.FirstName += 'XAND' ;
        c2.LastName += 'XOR' ;
        c2.TaxID__c = '222334444' ;
        cL.add ( c2 ) ;

        insert cL ;
        
        ReferralCodeGenerator.generateReferralCodes ( cL ) ;
        
        Referral_Code__c rc1 = [
            SELECT ID, Name
            FROM Referral_Code__c
            WHERE Contact__c = :c1.ID
        ] ;
        
        Referral_Code__c rc2 = [
            SELECT ID, Name
            FROM Referral_Code__c
            WHERE Contact__c = :c2.ID
        ] ;
        
        System.assertNotEquals ( rc1.Name, rc2.Name ) ;
        
        Test.stopTest () ;
    }
}