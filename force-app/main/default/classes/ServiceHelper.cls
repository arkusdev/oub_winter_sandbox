public class ServiceHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Record types
	 *  -------------------------------------------------------------------------------------------------- */
    private static string agreementRTID ;
    
    public static string getAgreementRTID ()
    {
        if ( agreementRTID == null )
            agreementRTID = Schema.SObjectType.Service__c.RecordTypeInfosByName.get('Agreement').RecordTypeId ;
        
        return agreementRTID ;
    }
    
    private static string onlineservicesRTID ;
    
    public static string getOnlineservicesRTID ()
    {
        if ( onlineservicesRTID == null )
            onlineservicesRTID = Schema.SObjectType.Service__c.RecordTypeInfosByName.get('Online Services').RecordTypeId ;
        
        return onlineservicesRTID ;
    }
}