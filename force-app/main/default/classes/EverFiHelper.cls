public class EverFiHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Inputs
	 *  -------------------------------------------------------------------------------------------------- */	
    private Datetime since ;
    private String sinceThing ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Settings
	 *  -------------------------------------------------------------------------------------------------- */	
    EverFi_Settings__c efs = null ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Internals
	 *  -------------------------------------------------------------------------------------------------- */	
    private EverFiAuth efa = null ;

    private String errorMessage ;
    private Integer statusCode ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Internals, Maps
	 *  -------------------------------------------------------------------------------------------------- */	
    private map<String,EverFi_Profile__c> epM ;
    private map<Integer,Everi_Module__c> emM ;
    private Map<String,Module__c> mM ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Counters
	 *  -------------------------------------------------------------------------------------------------- */	
    private Integer userCount ;
    private Integer totalUserCount ;
    private Integer profileCount ;
    private Integer totalProfileCount ;
    private Integer programUserCount ;
    private Integer totalProgramUserCount ;
    private Integer moduleCount ;
    private Integer locationCount ;
    
    private Integer calloutCount ;

    /*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */	
    public EverFiHelper ()
    {
        //  reset Counters
        resetVariables () ;
        
        //  Load settings
        efs = EverFi_Settings__c.getOrgDefaults () ;
            
        //  Default to 24 hours prior if not found
        if ( ( efs != null ) && ( efs.Last_Timestamp__c != null ) )
            since = efs.Last_Timestamp__c ;
        else
	        since = System.now () .addDays ( -1 ) ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Reset
	 *  -------------------------------------------------------------------------------------------------- */	
    public void resetVariables ()
    {
        //  Counters
        userCount = 0 ;
        totalUserCount = 0 ;
        profileCount = 0 ;
        totalProfileCount = 0 ;
        programUserCount = 0 ;
        totalProgramUserCount = 0 ;
        moduleCount = 0 ;
        locationCount = 0 ;
        
        calloutCount = 1 ;
        
        //  Defaults
        errorMessage = '' ;
        statusCode = 200 ;
        efa = null ;
        
        epM = new map<String,EverFi_Profile__c> () ;
        emM = new map<Integer,Everi_Module__c> () ;
        mM = new map<String,Module__c> () ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Update last since date
	 *  -------------------------------------------------------------------------------------------------- */
    public boolean updateSince ()
    {
        Datetime foo = Datetime.now () ;
        
        if ( ( String.isNotBlank ( sinceThing ) ) && ( ! sinceThing.equalsIgnoreCase ( 'null' ) ) )
            foo = EverfiProfileHelper.convertISO8601 ( sinceThing ) ;
        else
            sinceThing = EverfiProfileHelper.convertDateTimeToString ( foo ) ;
        
        return updateSince ( foo ) ;
    }
    
    public boolean updateSince ( Datetime dt )
    {
        boolean success = false ;
        
        System.debug ( 'Updating SINCE :: ' + dt ) ;
        
        try
        {
	        efs.Last_Timestamp__c = dt ;
            update efs ;
            
            success = true ;
        }
        catch ( Exception e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            success = false ;
        }
        
        return success ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal methods
	 *  -------------------------------------------------------------------------------------------------- */	
    private boolean oauthLogin ( String loginUri, String clientId, String clientSecret )
    {
        boolean success = false ;
        
        HttpRequest req = new HttpRequest () ; 
 
        req.setMethod ( 'POST' ) ;
        req.setEndpoint ( loginUri ) ;
        req.setBody ( 'grant_type=' + efs.Grant_Type__c + '&client_id=' + clientId + '&client_secret=' + clientSecret );
    
        Http http = new Http () ;
        HTTPResponse res = null ;
  
        try
        {
	        res = http.send ( req ) ;
            statusCode = res.getStatusCode () ;
            
            success = true ;
        }
        catch ( Exception e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            success = false ;
        }
        
        if ( res != null )
        {
            JSONParser parser = JSON.createParser ( res.getBody () ) ;
            
            while ( parser.nextToken() != null )
            {
                if ( parser.getCurrentToken() == JSONToken.START_OBJECT ) 
                {
                    efa = (EverFiAuth) parser.readValueAs ( EverFiAuth.class ) ;
                }
            }
        }
        else
        {
            success = false ;
        }
        
        return success ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Login public interface
	 *  -------------------------------------------------------------------------------------------------- */	
    public boolean everFiLogin ()
    {
       return oauthLogin ( efs.Login_URL__c, efs.Client_ID__c, efs.Client_Secret__c ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Users
	 *  -------------------------------------------------------------------------------------------------- */
	private String getUsers ()
    {
        HttpRequest req = new HttpRequest () ; 
 
        req.setMethod ( 'GET' ) ;
        req.setHeader ( 'Host', 'api.fifoundry.net' ) ;
        req.setHeader ( 'Authorization', 'Bearer ' + efa.access_token ) ;
        req.setHeader ( 'Accept', 'application/json' ) ;
        req.setEndpoint ( efs.User_URL__c ) ;
        
        String body = '' ;
        
        if ( String.isNotBlank ( body ) )
	        req.setBody ( body ) ;
        
        Http http = new Http () ;
        HTTPResponse res ;
  
        try
        {
	        res = http.send ( req ) ;
            statusCode = res.getStatusCode () ;
        }
        catch ( Exception e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            res = null ;
        }
        
        String returnString = '' ;
        
        if ( res != null )
	        returnString =  res.getBody () ;
        
        return returnString ;
    }
    
    public String loadUsers ()
    {
        String response = getUsers () ;
        
        String x = '' ;
        
        if ( String.isNotBlank ( response ) )
        {
            JSONParser parser = JSON.createParser ( response ) ;
        
            x = response ;
        }
        
        return x ;
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Locations
	 *  -------------------------------------------------------------------------------------------------- */
	private String getLocations ()
    {
        HttpRequest req = new HttpRequest () ; 
 
        req.setMethod ( 'GET' ) ;
        req.setHeader ( 'Host', 'api.fifoundry.net' ) ;
        req.setHeader ( 'Authorization', 'Bearer ' + efa.access_token ) ;
        req.setHeader ( 'Accept', 'application/json' ) ;
        req.setEndpoint ( efs.Location_URL__c ) ;
        
        Http http = new Http () ;
        HTTPResponse res ;
        
        try
        {
	        res = http.send ( req ) ;
            statusCode = res.getStatusCode () ;
        }
        catch ( Exception e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            res = null ;
        }
        
        String returnString = '' ;
        
        if ( res != null )
	        returnString =  res.getBody () ;
        
        return returnString ;
    }
    
    public String loadLocations ()
    {
        String response = getLocations () ;
        
        String x = '' ;
        
        if ( String.isNotBlank ( response ) )
        {
            JSONParser parser = JSON.createParser ( response ) ;
        
            x = response ;
        }
        
        return x ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Program Users
	 *  -------------------------------------------------------------------------------------------------- */
	private String getProgramUsers ( Datetime since )
    {
        String xSince = EverfiProfileHelper.convertDateTimeToString ( since ) ;
        return getProgramUsers ( xSince, null ) ;
    }
    
	private String getProgramUsers ( String since, String scrollID )
    {
        HttpRequest req = new HttpRequest () ; 
 
        req.setMethod ( 'GET' ) ;
        req.setHeader ( 'Host', 'api.fifoundry.net' ) ;
        req.setHeader ( 'Authorization', 'Bearer ' + efa.access_token ) ;
        req.setHeader ( 'Accept', 'application/json' ) ;
        
        String endPoint = efs.Program_User_URL__c ;
        endPoint += '?since=' + since ;
        
        if ( String.isNotBlank ( scrollID ) )
            endPoint += '&scroll_id=' + scrollID ;
        
        if ( efs.Scroll_Size__c != null )
            endPoint += '&scroll_size=' + String.valueOf ( efs.Scroll_Size__c ) ;
        
        req.setEndpoint ( endPoint ) ;
        
        String body = '' ;
        
        if ( String.isNotBlank ( body ) )
	        req.setBody ( body ) ;
        
        Http http = new Http () ;
        HTTPResponse res ;
        
        try
        {
	        res = http.send ( req ) ;
            statusCode = res.getStatusCode () ;
            calloutCount ++ ;
        }
        catch ( Exception e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            res = null ;
        }
        
        String returnString = '' ;
        
        if ( res != null )
	        returnString =  res.getBody () ;
        
        return returnString ;
    }
    
    public boolean loadProgramUsersSimple ()
    {
        return loadProgramUsersSimple ( since ) ;
    }
    
    public boolean loadProgramUsersSimple ( Datetime since )
    {
		EverFiProgramUserSimple efpus ;
        boolean success = true ;
        
	    String jsonResponse = getProgramUsers ( since ) ;
        calloutCount ++ ;
        
        String scrollID = '' ;
        
        if ( String.isNotBlank ( jsonResponse ) )
        {
            efpus = EverFiProgramUserSimple.parse ( jsonResponse ) ;
            
            EverFiProgramUserSimple.Next efpusn = efpus.Next ;
            
            if ( ( efpus.Next == null ) || ( String.isBlank ( efpusn.scroll_id ) ) )
            {
	            parseEverFiData ( efpus ) ;
            }
            else
            {
	            parseEverFiData ( efpus ) ;
                
                scrollID = efpusn.scroll_id ;
                sinceThing = efpusn.since ;
                
                if ( String.isNotBlank ( scrollID ) && String.isNotBlank ( sinceThing ) )
                {
	                System.debug ( 'SCROLL ID ::' + scrollID ) ;
	                System.debug ( 'THING ?!? ::' + sinceThing ) ;
                    
                    while ( String.isNotBlank ( scrollID ) && String.isNotBlank ( sinceThing ) )
                    {
                        jsonResponse = getProgramUsers ( sinceThing, scrollID ) ;
                        
                        if ( String.isNotBlank ( jsonResponse ) )
                        {
                            efpus = EverFiProgramUserSimple.parse ( jsonResponse ) ;
                            efpusn = efpus.Next ;
                            
                            if ( efpusn != null )
                            {
                                scrollID = efpusn.scroll_id ;
								sinceThing = efpusn.since ;
                            }
                            else
                                scrollID = '' ;
                            
                            System.debug ( 'NEXT SCROLL ID ::' + scrollID ) ;
			                System.debug ( 'NEXT THING ?!? ::' + sinceThing ) ;
                            parseEverFiData ( efpus ) ;
                        }
                    }
                }
            }
        }
        else
        {
            if ( ( statusCode <> 200 ) && ( statusCode <> 204 ) )
	            success = false ;
        }
        
        return success ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ERROR
	 *  -------------------------------------------------------------------------------------------------- */	
    public string getErrorMessage ()
    {
        return errorMessage ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Numbahs
	 *  -------------------------------------------------------------------------------------------------- */
    public EverFiReporting getReporting ()
    {
        EverFiReporting efr = new EverFiReporting () ;
        
        efr.userCount = userCount ;
        efr.totalUserCount = totalUserCount ;
        efr.profileCount = profileCount ;
        efr.programUserCount = programUserCount ;
        efr.totalProgramUserCount = totalProgramUserCount ;
        efr.moduleCount = moduleCount ;
        efr.locationCount = locationCount ;
        
        efr.calloutCount = calloutCount ;
        
        efr.email = efs.Email__c ;
        efr.since = since ;
        
        if ( ( String.isNotBlank ( sinceThing ) ) && ( ! sinceThing.equalsIgnoreCase ( 'null' ) ) )
	        efr.sinceThing = EverfiProfileHelper.convertISO8601 ( sinceThing ) ;
        
        return efr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Load Everfi Objects
	 *  -------------------------------------------------------------------------------------------------- */	
    private boolean parseEverFiData ( EverFiProgramUserSimple efpus )
    {
        boolean success = false ;
        errorMessage = '' ;
        
        if ( efpus != null )
        {
            //  Everything in data
            List<EverFiProgramUserSimple.data> efpusDL = efpus.data ;
            
            if ( ( efpusDL != null ) && ( efpusDL.size () > 0 ) )
            {
                //  multiple data per batch
                totalUserCount += efpusDL.size () ;
                
                for ( EverFiProgramUserSimple.data efpusD : efpusDL )
                {
                    EverFiProgramUserSimple.User efpusU = efpusD.User ;
                    
                    //  Map registered user to everfi profile
                    if ( String.isNotBlanK ( efpusU.email ) )
                    {
                        EverFi_Profile__c ep = new EverFi_Profile__c () ;
                        ep.Name = String.valueOf ( efpusU.ID ) ;
                        ep.First_Name__c = efpusU.first_name ;
                        ep.Last_Name__c =  efpusU.last_name ;
                        ep.Email_Address__c = efpusU.email ;
                        
                        String thingy = efpusU.first_name + '-' + efpusU.last_name + '-' + efpusU.email ;
                        if ( ! epM.containsKey ( thingy ) )
                            epM.put ( thingy, ep ) ;
                        
                        list<EverFiProgramUserSimple.User_content> efpusucL = efpusD.User_content ;
                        totalProgramUserCount += efpusucL.size () ;
                        
                        //  Use user results as a course list since every user has every course
                        for ( EverFiProgramUserSimple.User_content efpusuc : efpusucL )
                        {
                            if ( ! emM.containsKey ( efpusuc.ID ) )
                            {
                                Everi_Module__c em = new Everi_Module__c () ;
                                em.Module_ID__c = String.valueOf ( efpusuc.ID ) ;
                                em.Name = efpusuc.content_name ;
                                emM.put ( efpusuc.ID, em ) ;
                            }
                            
                            //  Combine unique userID + course ID for key to chain lookup object
                            String combinedID = efpusU.ID + '-' + efpusuc.ID ;
                            
                            if ( ! mM.containsKey ( combinedID ) )
                            {
                                Module__c m = new Module__c () ;
                                m.Name = combinedID ;
                                m.Course_Status__c = efpusuc.state ;
                                
                                mM.put ( combinedID, m ) ;
                            }
                        }
                    }
                    else
                    {
                        list<EverFiProgramUserSimple.User_content> efpusucL = efpusD.User_content ;
                        totalProgramUserCount += efpusucL.size () ;
                    }
                }
            }
        }
        
        return success ;
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Load Everfi Objects
	 *  -------------------------------------------------------------------------------------------------- */	
    public boolean saveEverFiData ()
    {
        boolean success = false ;
        errorMessage = '' ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  Contact lookup
         *  -------------------------------------------------------------------------------------------------- */	
        if ( epM.keySet().size () > 0 )
        {
            list<Contact> cL = null ;
            
            try
            {
                // map key is contact lookup reference field
                cL = [
                    SELECT ID, Email, WorkEmail__c, Name_Email_Lookup__c
                    FROM Contact
                    WHERE Name_Email_Lookup__c IN :epM.keySet ()
                ] ;
            }
            catch ( DMLException e )
            {
                cL = null ;
                success = false ;
                errorMessage = 'Unable to do contact lookup?!?  ' + OneUnitedUtilities.parseStackTrace ( e ) ;
            }
            
            //  Map contact IDs back to map
            if ( ( cL != null ) && ( cL.size () > 0 ) )
            {
                for ( Contact c : cL )
                {
                    if ( epM.containsKey ( c.Name_Email_Lookup__c ) )
                        epM.get ( c.Name_Email_Lookup__c ).Contact__c = c.ID ;
                }
            }
        
            /*  --------------------------------------------------------------------------------------------------
             *  Data load #1, will need IDs for later
             *  -------------------------------------------------------------------------------------------------- */	
            try
            {
                upsert epM.values () Name ;
                profileCount = epM.keyset () .size () ;
                
                upsert emM.values () Module_ID__c ;
                moduleCount = emM.keyset ().size () ;
            }
            catch ( DMLException e )
            {
                success = false ;
                errorMessage = 'Unable to update everfi profiles and/or modules?!?  ' + OneUnitedUtilities.parseStackTrace ( e ) ;
            }
            
            /*  --------------------------------------------------------------------------------------------------
             *  Map conversion from first-last-email to unique ID from everfi
             *  -------------------------------------------------------------------------------------------------- */
	        map<String,EverFi_Profile__c> epM2 = new map<String,EverFi_Profile__c> () ;
            
            for ( String thingy : epM.keyset () )
            {
                if ( ! epM2.containsKey ( epM.get ( thingy ).Name ) )
                    epM2.put ( epM.get ( thingy ).Name, epM.get ( thingy ) ) ;
            }
            
            /*  --------------------------------------------------------------------------------------------------
             *  Use third map with first two cross references to build list!
             *  -------------------------------------------------------------------------------------------------- */
            list<Module__c> mL = new list<Module__c> () ;
            
            for ( String combinedID : mM.keySet () )
            {
                String[] keys = combinedID.split ( '-' ) ;
                
                Module__c m = mM.get ( combinedID ) ;
                
                if ( epM2.containsKey ( keys [0] ) )
                    m.Everfi_Profile__c = epM2.get ( keys [ 0 ] ). ID ;
                
                if ( emM.containsKey ( Integer.valueof ( keys [1] ) ) )
                    m.Everfi_Module__c = emM.get ( Integer.valueof ( keys [ 1 ] ) ).ID ;
                
                if ( ( m.Everfi_Module__c != null ) && ( m.Everfi_Profile__c != null ) )
                    mL.add ( m ) ;
            }
            
            /*  --------------------------------------------------------------------------------------------------
             *  Data load #2
             *  -------------------------------------------------------------------------------------------------- */	
            try
            {
                upsert mL Name ;
                programUserCount = mL.size () ;
            }
            catch ( DMLException e )
            {
                success = false ;
                errorMessage = 'Unable to update everfi modules?!?  ' + OneUnitedUtilities.parseStackTrace ( e ) ;
            }
            
            success = true ;
        }
        else
        {
            success = true ;
            errorMessage = 'No Data to load' ;
        }
        
        return success ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Classes for Interfaces
	 *  -------------------------------------------------------------------------------------------------- */	
    public class EverFiAuth
    {
        public String access_token ;
        public String token_type ;
        public Integer expires_in ;
        public Integer created_at ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Classes for Numbahs
	 *  -------------------------------------------------------------------------------------------------- */
    public class EverFiReporting
    {
        public String email ;
        public Datetime since ;
        public Datetime sinceThing ;
        public Integer userCount ;
        public Integer totalUserCount ;
        public Integer profileCount ;
        public Integer totalProfileCount ;
        public Integer programUserCount ;
        public Integer totalProgramUserCount ;
        public Integer moduleCount ;
        public Integer locationCount ;
        
        public Integer calloutCount ;
    }
}