@isTest
global class MockHTTPEverFiHelper implements HttpCalloutMock
{
    public static Integer scrollNUM = 0 ;
    public static String since = '2019-05-07T18:12:16.306174Z' ;
    
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        
		// Endpoint can change ...
		if ( req.getEndpoint().contains ( 'token' ) )
		{
            res.setHeader ( 'Content-Type', 'application/json' ) ;
            res.setBody ( '{ "access_token":"foo.bar.com","token_type":"bearer","expires_in":3600,"created_at":1529690880 }' ) ;
            res.setStatusCode ( 200 ) ;
        }
        else if ( req.getEndpoint().contains ( 'program_users' ) )
        {
            String scrollID = '' ;
            if ( scrollNUM <> 0 )
            {
                scrollID = String.valueOf ( scrollNUM ) ;
                scrollNUM -- ;
            }
            res.setHeader ( 'Content-Type', 'application/json' ) ;
            res.setBody ( '{ "next": { "since": "' + since + '", "scroll_id": "'+scrollID+'", "href": "https://api.fifoundry.net/v1/progress/program_users?since=2019-02-21T15%3A02%3A57.388313Z" },"data": [{"id": 1262308, "user": {"id": 1254975, "type": "registered learner", "email": "test@test.com", "last_name": "User", "first_name": "Test", "foundry_user_id": "abc123"}, "deleted": false, "program": {"id": 742, "name": "OneUnited Bank Customer Program"}, "responses": [{"id": 123456, "question_id":654987, "question_text": "who are yuo?", "question_answer":"yo"}], "updated_at": "2019-02-20T15:18:12.386055+00:00", "user_content": [{"id": 27156871, "state": "started", "opened_at": "2019-02-20T15:17:53.920Z", "started_at": "2019-02-20T15:18:10.399Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Developing a Business Plan"}, {"id": 27156893, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Emergency Savings"}, {"id": 27156892, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Monthly Budget Tool"}, {"id": 27156891, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Overdraft"}, {"id": 27156890, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Credit Cards"}, {"id": 27156889, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Mobile Payments"}, {"id": 27156888, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Mortgages"}, {"id": 27156887, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Considering Home Ownership"}, {"id": 27156886, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Mortgage Refinancing"}, {"id": 27156885, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Debt Payoff"}, {"id": 27156884, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Checking Accounts"}, {"id": 27156883, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Identity Protection"}, {"id": 27156882, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Taxes"}, {"id": 27156881, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Build Emergency Savings"}, {"id": 27156880, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Credit Scores and Reports"}, {"id": 27156879, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "When to Collect Social Security"}, {"id": 27156878, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Retirement 101"}, {"id": 27156877, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Estate Planning"}, {"id": 27156876, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Savings"}, {"id": 27156875, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Your Retirement Lifestyle"}, {"id": 27156874, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Preventing Elder Fraud"}, {"id": 27156873, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Expectations of a Financial Caregiver"}, {"id": 27156872, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "How Businesses Obtain Credit"}, {"id": 27156870, "state": "unopened", "opened_at": "2019-02-21T14:04:36.124Z", "started_at": "2019-02-21T14:04:36.124Z", "completed_at": "2019-02-21T14:04:36.124Z", "content_name": "Business Financial Statements"}], "incentive_activities": [{"id": 999, "incentive_id" : 999, "incentive_title":"unknown", "incentive_playlist_id":1234, "incentive_playlist_name":"abc123", "status":"non_started", "start_datetime": "2019-02-21T14:04:36.124Z","completion": "2019-02-21T14:04:36.124Z"}]}] }' );
            res.setStatusCode ( 200 ) ;
        }
        else if ( req.getEndpoint().contains ( 'locations' ) )
        {
            res.setHeader ( 'Content-Type', 'application/json' ) ;
            res.setBody ( '{"data":[],"links":{},"meta":{"total_count":0,"cursor_id":null}}' ) ;
            res.setStatusCode ( 200 ) ;
        }
        else if ( req.getEndpoint().contains ( 'users' ) )
        {
            res.setHeader ( 'Content-Type', 'application/json' ) ;
            res.setBody ( '{"data":[{"id":"x-y-z-a-b","type":"users","attributes":{"active":true,"administrative_area":{"organization":{"country":null,"level_1":null,"level_2":null,"state_iso_code":null,"country_iso_code":null},"home":{"country":null,"level_1":null,"level_2":null},"work":{"country":null,"level_1":null,"level_2":null}},"bounced_email":false,"business_lines":["at-work"],"managed_business_lines":[],"created_at":"2018-10-25T07:33:10.196Z","current_sign_in_at":"2018-10-25T07:33:10.225Z","email":"foo@yahoo.com","employee_id":null,"external_attributes":{},"first_name":"A Carmelle","home_address_formatted":null,"home_address_room":null,"is_learner?":true,"is_manager?":false,"last_name":"Hasan","last_sign_in_at":"2018-10-25T07:33:10.225Z","location_id":null,"organization_id":"a-b-c-c-d","organization_logo_url":"https://everfi-adminifi.s3.amazonaws.com/production/mediafi/media/foo.png","organization_name":"OneUnited Bank","organization_demo":false,"organization_slug":"oneunitedbank","organization_uuid":"a-b-c-d-e","sign_in_count":1,"sso_id":null,"status":"pending","student_id":null,"updated_at":"2018-11-05T11:25:20.695Z","user_types":{"Financial Education Student":"Learner"},"work_address_formatted":null,"work_address_room":null},"relationships":{"organization":{"data":null},"location":{"data":null},"home_address":{"data":null},"work_address":{"data":null},"custom_grouping_values":{"data":[]},"sso_attributes":{"data":[]}}}],"links":{},"meta":{"total_count":1,"cursor_id":null}}' ) ;
            res.setStatusCode ( 200 ) ;
        }
        
        // return
        return res ;
    }
}