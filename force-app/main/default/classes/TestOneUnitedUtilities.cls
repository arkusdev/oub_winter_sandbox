@isTest
public with sharing class TestOneUnitedUtilities 
{
	private static User getUser ()
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test1' ;
		u.lastName = 'User1' ;
		u.email = 'test1@user.com' ;
		u.username = 'test1user1@user.com' ;
		u.alias = 'tuser1' ;
		u.CommunityNickname = 'tuser1' ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		insert u ;
		
		return u ;
	}
	
	public static testmethod void testGetRecordTypeId ()
	{
		Test.startTest () ;
		
		//  Should exist
		String recordTypeIdYes = OneUnitedUtilities.getRecordTypeId ( 'Retail Task' ) ;
		System.assertNotEquals ( recordTypeIdYes, '' ) ;
		
		// Should not exist!
		String recordTypeIdNo = OneUnitedUtilities.getRecordTypeId ( 'NO SUCH THING' ) ;
		System.assertEquals ( recordTypeIdNo, '' ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testGetRecordTypeIdForObject ()
	{
		Test.startTest () ;
		
		//  Should exist
		String recordTypeIdYes = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Event' ) ;
		System.assertNotEquals ( recordTypeIdYes, '' ) ;
		
		// Should not exist!
		String recordTypeIdNo = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Contact' ) ;
		System.assertEquals ( recordTypeIdNo, '' ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testOtherGetRecordTypeIdMethod ()
	{
		Test.startTest () ;
		
		List<String> foo = new List<String> () ;
		foo.add ( 'Community Room' ) ;
		foo.add ( 'Social Media Alert' ) ;
		
		List<RecordType> rtL = OneUnitedUtilities.getRecordTypeList ( foo ) ;
		
		String cmId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Community Room', 'Ticket__c' ) ;
		System.assertNotEquals ( cmId, '' ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testGetGroupId ()
	{
		Test.startTest () ;
		
		Group g = new Group () ;
		g.Name = 'Test Group' ;
		
		insert g ;
		
		//  Exists
		String groupIdYes = OneUnitedUtilities.getGroupId ( 'Test Group' ) ;
		System.assertNotEquals ( groupIdYes, '' ) ;
		
		//  Doesn't exist
		String groupIdNo = OneUnitedUtilities.getGroupId ( 'Fake Group' ) ;
		System.assertEquals ( groupIdNo, '' ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testGetGroupMap ()
	{
		Test.startTest () ;
		
		Group g1 = new Group () ;
		g1.Name = 'Test Group 1' ;
		insert g1 ;
		
		Group g2 = new Group () ;
		g2.Name = 'Test Group 2' ;
		insert g2 ;
		
		Map<String,String> gM = OneUnitedUtilities.getGroupMap ( new List<String> { g1.Name, g2.Name } ) ;
		
		System.assertEquals ( gM.size (), 2 ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testUserMap ()
	{
		Test.startTest () ;
		
		User u1 = getUser () ;
		
		Map<String,User> uMap = OneUnitedUtilities.getActiveUsers () ;
		
		System.assertNotEquals ( uMap.get ( u1.Id ), null ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testUserMapByEmail ()
	{
		Test.startTest () ;
		
		User u1 = getUser () ;
		
		Map<String,User> uMap = OneUnitedUtilities.getActiveUsersByEmail () ;
		
		System.assertNotEquals ( uMap.get ( u1.email ), null ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testIsUser ()
	{
		Test.startTest () ;
		
		User u = getUser () ;
			
		System.assertEquals ( OneUnitedUtilities.isUser ( u.id ), true ) ;
		System.assertNotEquals ( OneUnitedUtilities.isGroup ( u.id ), true ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testIsGroup ()
	{
		Test.startTest () ;
		
		Group g = new Group () ;
		g.Name = 'Test Group' ;
		
		insert g ;
		
		System.assertNotEquals ( OneUnitedUtilities.isUser ( g.id ), true ) ;
		System.assertEquals ( OneUnitedUtilities.isGroup ( g.id ), true ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testEmailValidation ()
	{
		Test.startTest () ;
		
		String email ;
		
		email = 'junk' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = '--junk' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = 'foo--@' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = 'foo,junk@bar.com' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = 'none@none.com' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = 'na@na.com' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = 'user.name@microsoft.com' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), true ) ;

		email = '1@2.3' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = 'xyz100@gmail.com' ;
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), true ) ;
		
		email = 'maynow71@gmail.com';
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), true ) ;
		
		email = '1@gmail.com';
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
		email = '991@gmail.com';
		System.assertEquals ( OneUnitedUtilities.isValidEmail ( email ), false ) ;
		
        Test.stopTest () ;
	}
	
	public static testmethod void testPhoneValidation ()
	{
		Test.startTest () ;
		
		String phoneNumber ;
		
		phoneNumber = 'xyz' ;
		System.assertequals ( OneUnitedUtilities.isValidPhoneNumber ( phoneNumber ), false ) ;
		
		phoneNumber = '343-323-2131' ;
		System.assertequals ( OneUnitedUtilities.isValidPhoneNumber ( phoneNumber ), true ) ;
		
		phoneNumber = '+1 (343) 323-2131' ;
		System.assertequals ( OneUnitedUtilities.isValidPhoneNumber ( phoneNumber ), true ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testPhoneCleaning ()
	{
		Test.startTest () ;
		
		String phoneNumber ;
		
		phoneNumber = '(323) 290-6546' ;
		System.assertequals ( OneUnitedUtilities.cleanPhoneNumber ( phoneNumber ), '323-290-6546' ) ;
		
		phoneNumber = '1 (323) 290-6546' ;
		System.assertequals ( OneUnitedUtilities.cleanPhoneNumber ( phoneNumber ), '323-290-6546' ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testZipCodeValidation ()
	{
		Test.startTest () ;
		
		String zipCode ;
		
		zipCode = 'xyz' ;
		System.assertequals ( OneUnitedUtilities.isValidZipCode ( zipCode ), false ) ;
		
		zipCode = '12345' ;
		System.assertequals ( OneUnitedUtilities.isValidZipCode ( zipCode ), true ) ;
		
		zipCode = '12345-4324' ;
		System.assertequals ( OneUnitedUtilities.isValidZipCode ( zipCode ), true ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testZipCodeCleaning ()
	{
		Test.startTest () ;
		
		String zipCode ;
		
		zipCode = '12345' ;
		System.assertequals ( OneUnitedUtilities.cleanZipCode ( zipCode ), '12345' ) ;
		
		zipCode = '12345x6789' ;
		System.assertequals ( OneUnitedUtilities.cleanZipCode ( zipCode ), '12345-6789' ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void lookupContact ()
	{
		Test.startTest () ;
		
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test9' ;
		c1.LastName = 'User9' ;
		c1.Email = 'test9@user9.com' ;
		
		insert c1 ;
		
		Contact c2 ;
		
		c2 = OneUnitedUtilities.lookupContact ( 'test9@user9.com' ) ;
		System.assertEquals ( c2.Id, c1.Id ) ;
		
		c2 = OneUnitedUtilities.lookupContact ( 'other9@user9.com' ) ;
		System.assertEquals ( c2, null ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void lookupContacts ()
	{
		Test.startTest () ;
		
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test1' ;
		c1.LastName = 'User1' ;
		c1.Email = 'test1@user1.com' ;
		
		insert c1 ;
		
		Contact c2 = new Contact () ;
		c2.FirstName = 'Test2' ;
		c2.LastName = 'User2' ;
		c2.Email = 'test2@user2.com' ;
		
		insert c2 ;
		
		List<String> eL = new List<String> () ;
		eL.add ( c1.Email ) ;
		eL.add ( c2.Email ) ;
		eL.add ( 'not@here.com' ) ;
		
		Map<String,Contact> thingy = OneUnitedUtilities.lookupContactMap ( eL ) ;
		
		System.assertEquals ( thingy.containsKey ( c1.Email ), true ) ;
		System.assertEquals ( thingy.containsKey ( c2.Email ), true ) ;
		System.assertEquals ( thingy.containsKey ( 'not@here.com' ), false ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testDayOfWeek ()
	{
		Test.startTest () ;
		
		Date d ;
		
		d = Date.parse ( '02/08/2014' ) ;
		System.assertEquals ( OneUnitedUtilities.getDayOfWeek ( d ), 6 ) ;
		
		d = Date.parse ( '02/15/2014' ) ;
		System.assertEquals ( OneUnitedUtilities.getDayOfWeek ( d ), 6 ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testGuestUser ()
	{
		Test.startTest () ;
		
		User u = [
			SELECT ID
			FROM User
			WHERE Name = 'One United Web Site Guest User'
		] ;
	
		String id = u.Id ;
		
		System.assertEquals ( OneUnitedUtilities.isGuestUser ( Id ), true ) ;

		//  DML TEST		
		System.assertEquals ( OneUnitedUtilities.isGuestUser ( Id ), true ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testAPIUser ()
	{
		Test.startTest () ;
		
		User u = [
			SELECT ID
			FROM User
			WHERE Name = 'SalesForce Automation'
		] ;
	
		String id = u.Id ;

		System.assertEquals ( OneUnitedUtilities.isAPIUser ( Id ), true ) ;
		
		// DML TEST
		System.assertEquals ( OneUnitedUtilities.isAPIUser ( Id ), true ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testGetUserIdListFromGroup ()
	{
		Test.startTest () ;
		
		List<String> userIdList = OneUnitedUtilities.getUserIdsFromGroupName ( 'Social Media Alert' ) ;
		
		System.assertNotEquals ( userIdList, null ) ;

		Test.stopTest () ;
	}
	
	public static testmethod void testGetPriorSubscribers ()
	{
		Test.startTest () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'TestC' ;
		c.LastName = 'UserC' ;
		c.Email = 'testC@userC.com' ;
		
		insert c ;
		
		User u = getUser () ;
		
		EntitySubscription es = new EntitySubscription () ;
		es.ParentId = c.Id ;
		es.SubscriberId = u.Id ;
		
		insert es ;
		
		Map<String,String> subList = OneUnitedUtilities.getCurrentSubscribers ( c.Id ) ;
		
		System.assertNotEquals ( subList, null ) ;
		System.assertEquals ( subList.containsKey ( u.Id ), true ) ;

		Test.stopTest () ;
	}
    
	public static testmethod void testGetPriorSubscribersBULK ()
	{
		Test.startTest () ;

		User u1 = getUser () ;
		
        Contact c1 = new Contact () ;
		c1.FirstName = 'TestC' ;
		c1.LastName = 'User1C' ;
		c1.Email = 'test1C@user1C.com' ;
		
		insert c1 ;
		
		EntitySubscription es1 = new EntitySubscription () ;
		es1.ParentId = c1.Id ;
		es1.SubscriberId = u1.Id ;
		
		insert es1 ;
		
        Contact c2 = new Contact () ;
		c2.FirstName = 'Test2C' ;
		c2.LastName = 'User2C' ;
		c2.Email = 'test2C@user2C.com' ;
		
		insert c2 ;
		
		EntitySubscription es2 = new EntitySubscription () ;
		es2.ParentId = c2.Id ;
		es2.SubscriberId = u1.Id ;
		
		insert es2 ;
        
        list<String> cL = new list<String> () ;
        cL.add ( c1.ID ) ;
        cL.add ( c2.ID ) ;
		
		Map<String,Map<String,String>> subSubMap = OneUnitedUtilities.getCurrentSubscribers ( cL ) ;
		
		System.assertNotEquals ( subSubMap, null ) ;
		System.assertEquals ( subSubMap.containsKey ( c1.Id ), true ) ;
		System.assertEquals ( subSubMap.containsKey ( c2.Id ), true ) ;
        
        Map<String,String> subMap1 = subSubMap.get ( c1.ID ) ;
		System.assertNotEquals ( subMap1, null ) ;
		System.assertEquals ( subMap1.containsKey ( u1.Id ), true ) ;

        Map<String,String> subMap2 = subSubMap.get ( c2.ID ) ;
		System.assertNotEquals ( subMap2, null ) ;
		System.assertEquals ( subMap2.containsKey ( u1.Id ), true ) ;

		Test.stopTest () ;
	}
    
    public static testmethod void testGetContactByNameAndEmail ()
    {
        Contact c = new Contact () ;
        c.FirstName = 'Test' ;
        c.LastName = 'User' ;
        c.Email = 'testWTF@userWTF.com' ;
        
        insert c ;
        
        Test.startTest () ;
        
        Contact xc = OneUnitedUtilities.getContactByNameAndEmail ( c.FirstName, c.LastName, c.Email ) ;
        
        System.assertEquals ( c.ID, xc.ID ) ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void getDefaultAccounts ()
    {
        Account a1 = new Account () ;
        a1.name = 'Insight Customers - Individuals' ;
        insert a1 ;
        
        Account a2 = new Account () ;
        a2.name = 'Insight Customers - Busineses' ;
        insert a2 ;
        
        Test.startTest () ;
        
        Account a = OneUnitedUtilities.getDefaultAccount () ;
        
        System.assertNotEquals ( a, null ) ;
        
        Account ab = OneUnitedUtilities.getDefaultBusinessAccount () ;
        
        System.assertNotEquals ( ab, null ) ;
        
        Test.stopTest() ;
    }
    
    public static testmethod void getExceptionErrorThingy ()
    {
        one_application__c o = new one_application__c () ;
        
        try
        {
            insert o ;
        }
        catch ( Exception e )
        {
            String foo = OneUnitedUtilities.parseStackTrace ( e ) ;
            
            System.assertNotEquals ( foo, null ) ;
            
            System.debug ( foo ) ;
        }
    }
    
    private static testmethod void sendTestEmail ()
    {
        OneUnitedUtilities.sendBatchHTMLEmail ( 'user@acme.com', 'Test', 'Test' ) ;
    }
}