@isTest
public with sharing class test_application_upload 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Branch__c b = getBranch () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'x' + i + 'test'+ i + 'person@testing.com' ;
		c.phone = '00' + i + '-00' + i + '-000' + i ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		
		insert c ;
		
		return c ;
	}
    
	public static testmethod void testExistingContact ()
	{
		//  Point to page to test
		PageReference pr1 = Page.application_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Load a contact
		Contact c1 = getContact ( 1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		application_upload au = new application_upload ( sc ) ;
		
		//  Ready to start
		system.assertEquals ( au.dStepNumber, 1000 ) ;
		
		//  No data
		au.uploadFile () ;
		
		//  Check Empty Results
		system.assertEquals ( au.dStepNumber, 1001 ) ;
		system.assertEquals ( au.errorFirstName, true ) ;
		system.assertEquals ( au.errorLastName, true ) ;
		system.assertEquals ( au.errorEmail, true ) ;
		system.assertEquals ( au.errorPhone, true ) ;
		system.assertEquals ( au.errorFile, true ) ;
		
		//  Check bad some results
		au.Email = 'nyet@nyet.nyet@com' ;
		au.Phone = 'notaphone' ;
		
		//  Bad data
		au.uploadFile () ;
		
		system.assertEquals ( au.errorEmail, true ) ;
		system.assertEquals ( au.errorPhone, true ) ;
		
		//  Bad attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Test Thingy' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.IsPrivate = false ;
		a1.contentType = 'Music/MP3' ;

		//  Write attachment to controller like page would		
		au.attachment = a1 ;
		
		//  Attachment test
		au.uploadFile () ;
		
		system.assertEquals ( au.errorFile, true ) ;
		
		//  Good Data
		au.firstName = c1.FirstName ;
		au.LastName = c1.LastName ;
		au.Email = c1.Email ;
		au.Phone = c1.Phone ;
		
		//  Good attachment
		Attachment a2 = new Attachment () ;
		a2.Name = 'Test Thingy.pdf' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.IsPrivate = false ;
		a2.contentType = 'application/pdf' ;

		//  Write attachment to controller like page would		
		au.attachment = a2 ;
		
		//  Attachment test
		au.uploadFile () ;
		
		system.assertEquals ( au.dStepNumber, 2000 ) ;
	}
	
	public static testmethod void testNoContact ()
	{
		//  Point to page to test
		PageReference pr1 = Page.application_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		application_upload au = new application_upload ( sc ) ;
		
		//  Ready to start
		system.assertEquals ( au.dStepNumber, 1000 ) ;
		
		//  Good Data
		au.firstName = 'Test99' ;
		au.LastName = 'User99' ;
		au.Email = 'x99Test99User@Testing.com' ;
		au.Phone = '099-099-0099' ;
		
		//  Good attachment
		Attachment a2 = new Attachment () ;
		a2.Name = 'Test Thingy.pdf' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.IsPrivate = false ;
		a2.contentType = 'application/pdf' ;

		//  Write attachment to controller like page would		
		au.attachment = a2 ;
		
		//  Attachment test
		au.uploadFile () ;
		
		system.assertEquals ( au.dStepNumber, 2000 ) ;
	}
	
	public static testmethod void testMismatchedContact ()
	{
		//  Point to page to test
		PageReference pr1 = Page.application_upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Load a contact
		Contact c1 = getContact ( 1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		application_upload au = new application_upload ( sc ) ;
		
		//  Ready to start
		system.assertEquals ( au.dStepNumber, 1000 ) ;
		
		//  No data
		au.uploadFile () ;
		
		//  Check Empty Results
		system.assertEquals ( au.dStepNumber, 1001 ) ;
		system.assertEquals ( au.errorFirstName, true ) ;
		system.assertEquals ( au.errorLastName, true ) ;
		system.assertEquals ( au.errorEmail, true ) ;
		system.assertEquals ( au.errorPhone, true ) ;
		system.assertEquals ( au.errorFile, true ) ;
		
		//  Check bad some results
		au.Email = 'nyet@nyet.nyet@com' ;
		au.Phone = 'notaphone' ;
		
		//  Bad data
		au.uploadFile () ;
		
		system.assertEquals ( au.errorEmail, true ) ;
		system.assertEquals ( au.errorPhone, true ) ;
		
		//  Good Data
		au.firstName = c1.FirstName + 'X' ;
		au.LastName = c1.LastName + 'Y' ;
		au.Email = c1.Email ;
		au.Phone = c1.Phone ;
		
		//  Good attachment
		Attachment a2 = new Attachment () ;
		a2.Name = 'Test Thingy.pdf' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.IsPrivate = false ;
		a2.contentType = 'application/pdf' ;

		//  Write attachment to controller like page would		
		au.attachment = a2 ;
		
		//  Attachment test
		au.uploadFile () ;
		
		system.assertEquals ( au.dStepNumber, 2000 ) ;
	}
}