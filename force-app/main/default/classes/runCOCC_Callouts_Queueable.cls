public class runCOCC_Callouts_Queueable implements Queueable, Database.AllowsCallouts{
    
    private List<String> applicationsIds;
    
    public runCOCC_Callouts_Queueable(List<String> applicationsIds) {
        this.applicationsIds = applicationsIds;
    }
    
    public void execute(QueueableContext context) {
        
        List<one_application__c> apps = [SELECT ID, name, RecordTypeID, RecordType.Name,
                Application_ID__c, number_of_applicants__c, FIS_Decision_Code__c, FIS_Application_ID__c,
                Application_Status__c, Application_Processing_Status__c,Override_Person_in_COCC__c,
                Monthly_Rent_Payment__c,Co_Applicant_Monthly_Rent_Payment__c,Co_Applicant_Email_Address__c,prior_employer_city__c,co_applicant_middle_name__c,
                primid_state__c,co_applicant_employer_city__c,createdbyid,street_address__c,street_address_2__c,middle__c,
                co_applicant_employment_how_long_yrs__c,co_applicant_prior_employment_yrs__c,co_applicant_previous_address__c,
                prior_self_employed__c,employment_how_long_mths__c,mailing_state__c,self_employed__c,
                primid_co_applicant_issue_date__c,employer_state__c,isdeleted,mailing_zip_code__c,previous_city__c,
                systemmodstamp,prior_employer_zip_code__c,currently_employed__c,prior_position_occupation__c,
                createddate,ownerid,co_applicant_additional_income_y_n__c,co_applicant_prior_employer__c,
                co_applicant_employment_status__c,co_applicant_prior_employer_city__c,co_applicant_employer__c,
                co_applicant_previous_state__c,co_applicant_currently_employed__c,transfer_balance_credit_card_number__c,
                employer_address__c,mailing_city__c,co_applicant_self_employed__c,primid_expiration_date__c,
                co_applicant_previous_how_long_yrs__c,lastmodifiedbyid,previous_state__c,primid_co_applicant_expiration_date__c,
                co_applicant_position_occupation__c,how_long_yrs__c,co_applicant_employer_state__c,primid_issue_date__c,
                co_applicant_other_housing_status__c,funding_account_number__c,funding_options__c,
                co_applicant_prior_self_employed__c,co_applicant_phone__c,phone__c,credit_report_authorization__c,
                co_applicant_prior_position_occupation__c,co_applicant_street_address__c,co_applicant_street_address_2__c,gross_income_monthly__c,
                employment_status__c,co_applicant_last_name__c,mailing_street_address__c,mailing_street_address_2__c,primid_co_applicant_type__c,
                is_co_app_same_current_address__c,previous_address__c,position_occupation__c,funding_routing_number__c,
                work_phone__c,email_address__c,ssn__c,other_housing_status__c,additional_income_amount__c,
                co_applicant_zip_code__c,previous_how_long_yrs__c,co_applicant_additional_income_type__c,
                prior_employer_address__c,co_applicant_own_rent_other__c,dob__c,zip_code__c,how_long_mths__c,
                co_applicant_how_long_yrs__c,employer_city__c,co_applicant_prior_employer_address__c,prior_employer__c,
                co_applicant_previous_zip__c,co_applicant_how_long_mths__c,referred_by__c,funding_bank_name__c,
                co_applicant_mothersmaidenname__c,prior_employment_gross_income_monthly__c,prior_employed__c,
                co_applicant_employment_status_other__c,additional_income_type__c,primid_type__c,
                co_applicant_employment_how_long_mths__c,primid_co_applicant_number__c,prior_employment_how_long_yrs__c,
                co_applicant_dob__c,reservationid__c,co_applicant_ssn__c,mailing_address_same__c,last_name__c,first_name__c,
                transfer_balance_opt_in__c,over_18__c,employment_status_other__c,primid_number__c,
                co_credit_report_authorization__c,transfer_balance_amount__c,funding_expiration_date__c,
                co_applicant_previous_city__c,co_applicant_first_name__c,co_applicant_state__c,previous_zip__c,
                co_applicant_employer_address__c,mothersmaidenname__c,disclosures__c,co_applicant_prior_employment_gross_inc__c,
                lastmodifieddate,co_applicant_gross_income_monthly__c,own_rent_other__c,
                prior_employment_how_long_months__c,employer__c,state__c,co_applicant_employer_zip_code__c,
                co_applicant_contact__c,co_applicant_prior_employer_state__c,previous_how_long_mths__c,contact__c,
                co_applicant_city__c,co_applicant_additional_income_amount__c,co_applicant_work_phone__c,
                funding_security_code__c,primid_type_formula__c,employment_how_long_yrs__c,
                co_applicant_previous_how_long_mths__c,co_applicant_prior_employment_mths__c,employer_zip_code__c,
                prior_employer_state__c,requestedcreditlimit__c,primid_co_applicant_state__c,additional_income_y_n__c,city__c,
                co_applicant_prior_employer_zip_code__c,is_co_app_same_previous_address__c, FIS_Application_Source_Code__c,
                Promotion_Code__c, Product__c, SubProduct__c, Plan__c, Offer__c, Bill_Code__c, Terms_ID__c,
                IDV_Run_DateTime__c, IDV_Transaction_ID__c, IDV_Override_Datetime__c, IDV_Override_User__c, IDV_Decision__c,
                IDV_CoApplicant_Run_DateTime__c, IDV_CoApplicant_Transaction_ID__c, IDV_CoApplicant_Override_Datetime__c, IDV_CoApplicant_Override_User__c,  IDV_CoApplicant_Decision__c,
                QualiFile_Run_Datetime__c, QualiFile_Acceptance_Text__c, QualiFile_Score__c, QualiFile_Product_Offer__c, QualiFile_Decision__c,
                QualiFile_CoApp_Run_Datetime__c, QualiFile_CoApp_Acceptance_Text__c, QualiFile_CoApp_Score__c, QualiFile_CoApp_Product_Offer__c, QualiFile_CoApp_Decision__c,
                OFAC_Run_Datetime__c, OFAC_Transaction_ID__c, OFAC_Override_Datetime__c, OFAC_Override_User__c, OFAC_Decision__c,
                OFAC_CoApplicant_Run_Datetime__c, OFAC_CoApplicant_Transaction_ID__c, OFAC_CoApp_Override_Datetime__c, OFAC_CoApp_Override_User__c, OFAC_CoApplicant_Decision__c,
                Quiz_Outcome__c,
                Reservation_Vendor_ID__c, Reservation_Vendor_ID__r.Name,  Reservation_Vendor_ID__r.Tracking_Pixel__c,
                Reservation_Affiliate_Vendor__c, Reservation_Affiliate_Vendor__r.Name,  Reservation_Affiliate_Vendor__r.Tracking_Pixel__c,
                Upload_Attachment_Key__c,FundSourceCd__c,
                Insight_Applicant_Number__c, Insight_Co_Applicant_Number__c,
                Advocate_Referral_Code__c, Advocate_Referral_Code_Text__c,
                Pre_Bureau_Disposition__c,Pre_Bureau_Override_Datetime__c,Pre_Bureau_Override_User__c,
                Post_Bureau_Disposition__c,Post_Bureau_Override_Datetime__c,Post_Bureau_Override_User__c,
                RTDX_Response_Code__c, RTDX_Message__c
                FROM one_application__c 
				WHERE Id IN :applicationsIds];
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        list<Online_Deposit_Application_Account__c> odaaL = new list<Online_Deposit_Application_Account__c> () ;
        List<String> appIDList = new List<String> () ;
        
        String faRtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit', 'Financial_Account__c' ) ;
        
        for(one_application__c app : apps)
        {
            COCC_Helper coccHelper = new COCC_Helper(app);
            
            if ( ( String.isNotBlank ( app.RecordType.Name ) ) && ( ! app.RecordType.Name.equalsIgnoreCase ( 'Deposits' ) ) )
            {
                // JIM SAID TO IGNORE THE RESPONSE!
                COCC_Helper.COCC_Response res = coccHelper.createPersonsUnityVisa();
                
                //  PUsh to FIS Card
                if ( LOSHelper.isNewLOS ( app ) )
                {
                    //  Update application status - must board card
                    app.FIS_Decision_Code__c = 'P922';
                    appIDList.add ( app.ID ) ;
                }
            }
            else
            {
                COCC_Helper.COCC_Response res = coccHelper.createAccounts();
                
                if(res.status == 0)
                {
                    //  Update application status
                    app.FIS_Decision_Code__c = 'P089';
                    appIDList.add ( app.ID ) ;
    
                    //  Stub finacial accounts
                    if ( ( coccHelper.getListOfAccounts () != null ) && ( coccHelper.getListOfAccounts ().size () > 0 ) )
                    {
                        for ( Online_Deposit_Application_Account__c odaa : coccHelper.getListOfAccounts () )
                        {
                            Financial_Account__c fa = new Financial_Account__c () ;
                            fa.RecordTypeId = faRtID ;
                            
                            fa.ACCTNBR__c = odaa.COCC_Account_Number__c ;
                            fa.MJACCTTYPCD__c = odaa.Major_Account_Type_Code__c ;
                            fa.CURRMIACCTTYPCD__c = odaa.Minor_Account_Type_Code__c ;
                            fa.PRODUCT__c = odaa.Product_Name__c ;
                            fa.Credit_Card_Application__c = app.ID ;
                            fa.CURRACCTSTATCD__c = 'APPR' ;
                            fa.TAXRPTFORPERSNBR__c = app.Contact__c ;
                            
                            if ( String.isNotBlank ( app.Advocate_Referral_Code_Text__c ) )
                                fa.Advocate_Referral_Code_Text__c = app.Advocate_Referral_Code_Text__c ;
                            
                            if ( app.Advocate_Referral_Code__c != null )
                                fa.Advocate_Referral_Code__c = app.Advocate_Referral_Code__c ;
                            
                            faL.add ( fa ) ;
                            odaaL.add ( odaa ) ;
                        }
                    }
                }
            }
        }
        
        update apps;

        //  NOT FUTURE!
        if ( ( appIDList != null ) && ( appIDList.size() > 0 ))
            ApplicationEmailFunctions.sendApprovedEmails ( appIDList ) ;
        
        if ( ( faL != null ) && ( faL.size () > 0 ) )
	        insert faL ;
        
        //  Map results
        Map<String,Financial_Account__c> faM = new map<String,Financial_Account__c> () ;
        list<Online_Deposit_Application_Account__c> odaaX = new list<Online_Deposit_Application_Account__c> () ;
        
        for ( Financial_Account__c fa : faL )
        {
            faM.put ( fa.ACCTNBR__c, fa ) ;
        }
        
        if ( ( odaaL != null ) && ( odaaL.size () > 0 ) )
        {
            for ( Online_Deposit_Application_Account__c odaa : odaaL )
            {
                if ( faM.containsKey ( odaa.COCC_Account_Number__c ) )
                {
                    odaa.Financial_Account__c = faM.get ( odaa.COCC_Account_Number__c ).ID ; 
                    odaaX.add ( odaa ) ;
                }
            }
        }
        
        if ( ( odaaX != null ) && ( odaaX.size () > 0 ) )
            update odaaX ;
        
        list<Referral_Code_Financial_Account__c> rcfaL = new list<Referral_Code_Financial_Account__c> () ;
        
        if ( ( faL != null ) && ( faL.size () > 0 ) )
        {
            for ( Financial_Account__c fa : faL )
            {
                if ( fa.Advocate_Referral_Code__c != null )
                {
                    Referral_Code_Financial_Account__c rcfa = new Referral_Code_Financial_Account__c () ;
                    rcfa.Financial_Account__c = fa.ID ;
                    rcfa.Advocate_Referral_Code__c = fa.Advocate_Referral_Code__c ;
                    
                    rcfaL.add ( rcfa ) ;
                }
            }
            
            if ( ( rcfaL != null ) && ( rcfaL.size () > 0 ) )
                insert rcfaL ;
        }
    }
}