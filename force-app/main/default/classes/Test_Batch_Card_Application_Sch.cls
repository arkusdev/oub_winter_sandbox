@isTest
public with sharing class Test_Batch_Card_Application_Sch 
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	static testmethod void something ()
	{
		Test.startTest();

		batch_Card_Application_Reminder_Sch myClass = new batch_Card_Application_Reminder_Sch ();   
		String chron = '0 0 23 * * ?';        
		system.schedule('Test Sched', chron, myClass);
		
		Test.stopTest();
	}
}