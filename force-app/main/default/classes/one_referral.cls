public with sharing class one_referral 
{
	//  Amount of time to hold referral info
	private static Integer cookieAgeInDays ;
	
	//  Referral info
	private static Datetime applicationAge ;
	private static String reservationID ;
	private static String reservationVendorID ;
	private static String affiliate ;
	private static String affiliateVendorId ;
	
	//  Rakuten specific
	private static String offerID ;
	private static String creativeID ;
	private static String creativeType ;
	private static String rakutenTrackingID ;
	
	//  Promo code
	private static String promotionCode ;
    
    //  Advocate
    private static String advocateCode ;
	
	/* --------------------------------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------------------------------- */
	public one_referral ()
	{
		applicationAge = DateTime.now () ;
		cookieAgeInDays = -1 ;
		
		one_settings__c settings = one_settings__c.getInstance ( 'settings' ) ;
		 
		if ( settings != null )
		{
			if ( settings.Referral_Tracking_Hold_Days__c != null )
				cookieAgeInDays = settings.Referral_Tracking_Hold_Days__c.intValue () ;
				
			if ( settings.Rakuten_Tracking_ID__c != null )
				rakutenTrackingID = settings.Rakuten_Tracking_ID__c ;
		}

		System.debug ( 'Default Age = ' + cookieAgeInDays ) ;
	}
	
	/* --------------------------------------------------------------------------------------------------
	 * Cookies & third party
	 * -------------------------------------------------------------------------------------------------- */
	public void loadThirdPartyData ()
	{
		boolean newDTCookie = false ;
		boolean newCIDCookie = false ;
		boolean newCVIDCookie = false ;
		boolean newAAIDCookie = false ;
		
		//  Rakuten again
		boolean newOFIDCookie = false ;
		boolean newCRIDCookie = false ;
		boolean newCTIDCookie = false ;
		
		//  Promotion
		boolean newPromoCookie = false ;
        
        //  Advocate
        boolean newAdvocateCookie = false ;
		
		//  --------------------------------------------------------------------------------------------------
		//  Promotion
		//  --------------------------------------------------------------------------------------------------
		promotionCode = ApexPages.currentPage().getParameters().get ('prcd') ;
        String promoCookieCode = checkForCookieValue ( 'prcd_Cookie' ) ;

		if ( promotionCode == null || promotionCode == 'null' )
		{
			promotionCode = checkForCookieValue ( 'prcd_Cookie' ) ;
		}
		else
		{
            System.debug ( 'Compare: |' + promotionCode + '| -- |' + promoCookieCode + '|' ) ;
			
			//  New promotion code, wipe everything
			if ( ( String.isBlank ( promoCookieCode ) ) || 
                 ( ( String.isNotBlank ( promoCookieCode ) ) && ( ! promotionCode.equalsIgnoreCase ( promoCookieCode ) ) ) )
			{
                System.debug ( 'Clearing prior data - NEW promo' ) ;
                
                reservationID = '' ;
                reservationVendorID = '' ;
                affiliate = '' ;
                affiliateVendorId = '' ;
                
                //  Rakuten specific
                offerID = '' ;
                creativeID = '' ;
                creativeType = '' ;
                rakutenTrackingID = '' ;
                
				removeAllCookies () ;
                
            	newPromoCookie = true ;
			}
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Advocate
		//  --------------------------------------------------------------------------------------------------
		advocateCode = ApexPages.currentPage().getParameters().get ('apcd') ;
        String advocateCookieCode = checkForCookieValue ( 'apcd_Cookie' ) ;

        if ( String.isNotBlank ( advocateCode ) )
            advocateCode = advocateCode.toUpperCase () ;
        
        if ( String.isNotBlank ( advocateCookieCode ) )
            advocateCookieCode = advocateCookieCode.toUpperCase () ;
        
		if ( advocateCode == null || advocateCode == 'null' )
		{
			advocateCode = checkForCookieValue ( 'apcd_Cookie' ) ;
            
            if ( String.isNotBlank ( advocateCode ) )
                advocateCode = advocateCode.toUpperCase () ;
		}
		else
		{
            System.debug ( 'Compare: |' + advocateCode + '| -- |' + advocateCookieCode + '|' ) ;
			
			//  New advocate code, wipe everything
            System.debug ( 'Clearing prior data' ) ;
            
            reservationID = '' ;
            reservationVendorID = '' ;
            affiliate = '' ;
            affiliateVendorId = '' ;
            
            //  Rakuten specific
            offerID = '' ;
            creativeID = '' ;
            creativeType = '' ;
            rakutenTrackingID = '' ;
            
            removeAllCookies () ;
            
            newAdvocateCookie = true ;
		}
        
		//  --------------------------------------------------------------------------------------------------
		//  Campaign ID (Third party referral)
		//  --------------------------------------------------------------------------------------------------
		reservationID = ApexPages.currentPage().getParameters().get ('cid') ;
        String reservationIDCookie = checkForCookieValue ( 'cid_Cookie' ) ;
		
		if ( reservationID == null || reservationID == 'null' )
		{
			reservationID = checkForCookieValue ( 'cid_Cookie' ) ;
		}
		else
		{
            System.debug ( 'Compare: |' + reservationID + '| -- |' + reservationIDCookie + '|' ) ;
            
			//  New reservation ID, wipe everything
			if ( ( String.isBlank ( reservationIDCookie ) ) || 
                 ( ( String.isNotBlank ( reservationIDCookie ) ) && ( ! reservationID.equalsIgnoreCase ( reservationIDCookie ) ) ) )
			{
                //  Clear all old Cookies & Data, new CID!
                System.debug ( 'Clearing prior data - NEW reservation ID' ) ;
                
                reservationVendorID = '' ;
                affiliate = '' ;
                affiliateVendorId = '' ;
                
                //  Rakuten specific
                offerID = '' ;
                creativeID = '' ;
                creativeType = '' ;
                rakutenTrackingID = '' ;
                
                //  Don't touch the promotion - it's handled above!
                removeALLCookiesExceptPromotion () ;
                
	            newCIDCookie = true ;
            }
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Start time of application (based on campaign id )
		//  --------------------------------------------------------------------------------------------------
		applicationAge = System.now () ;
		Cookie dtCookie = ApexPages.currentPage().getCookies().get ( 'dt_Cookie' ) ;
		
		if ( ( dtCookie == null ) || ( newCIDCookie == true ) || ( newPromoCookie == true ) )
		{
			newDTCookie = true ;
		}
		else
		{
            if ( String.isNotBlank ( dtCookie.getValue () ) )
				applicationAge = Datetime.valueofGMT ( dtCookie.getValue () ) ;
           	else
                newDTCookie = true ;
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Campaign Vendor ID
		//  --------------------------------------------------------------------------------------------------
		reservationVendorID = ApexPages.currentPage().getParameters().get ('cvid') ;

		if ( reservationVendorID == null || reservationVendorID == 'null' )
		{
			reservationVendorID = checkForCookieValue ( 'cvid_Cookie' ) ;
			
			//  Check to see if we have an ID and if that ID is actually valid
			if ( ( String.isNotBlank ( reservationVendorID ) ) && ( ! one_utils.isValidAccount ( reservationVendorID ) ) )
				reservationVendorID = null ;
		}
		else
		{
			newCVIDCookie = true ;
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Affiliate 
		//  --------------------------------------------------------------------------------------------------
		affiliate = ApexPages.currentPage().getParameters().get('aaid');
		
		if(affiliate == null || affiliate == 'null')
		{
			affiliate = checkForCookieValue ( 'aaid_Cookie' ) ;
		}
		else
		{
			newAAIDCookie = true ;
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Rakuten #1
		//  --------------------------------------------------------------------------------------------------
		offerId = ApexPages.currentPage().getParameters().get ('ofid') ;
		
		if ( offerId == null || offerId == 'null' )
		{
			offerId = checkForCookieValue ( 'ofid_Cookie' ) ;
		}
		else
		{
			newOFIDCookie = true ;
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Rakuten #2
		//  --------------------------------------------------------------------------------------------------
		creativeId = ApexPages.currentPage().getParameters().get ('crid') ;
		
		if ( creativeId == null || creativeId == 'null' )
		{
			creativeId = checkForCookieValue ( 'crid_Cookie' ) ;
		}
		else
		{
			newCRIDCookie = true ;
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Rakuten #3
		//  --------------------------------------------------------------------------------------------------
		creativeType = ApexPages.currentPage().getParameters().get ('ctid') ;
		
		if ( creativeType == null || creativeType == 'null' )
		{
			creativeType = checkForCookieValue ( 'ctid_Cookie' ) ;
		}
		else
		{
			newCTIDCookie = true ;
		}
		
		//  --------------------------------------------------------------------------------------------------
		//  Look up default campaign vendor ID 
		//  --------------------------------------------------------------------------------------------------
		if ( String.isBlank ( reservationVendorID ) )
			reservationVendorID = one_utils.getDefaultCampaignID () ;
		
		//  --------------------------------------------------------------------------------------------------
		//  Look up affiliate vendor ID 
		//  --------------------------------------------------------------------------------------------------
		if ( ( String.isNotBlank ( affiliate ) ) && ( String.isNotBlank ( reservationVendorID ) ) )
			affiliateVendorId = one_utils.getAffiliateAccountLookup ( affiliate, reservationVendorID ) ;
		
		//  --------------------------------------------------------------------------------------------------
		//  Figure out how long to hold the data based on all of the above 
		//  --------------------------------------------------------------------------------------------------
		calculateHoldDuration () ;
		
		System.debug ( 'Updated Age = ' + cookieAgeInDays ) ;
		
		//  --------------------------------------------------------------------------------------------------
		//  Set ALL Cookies after hold duration
		//  --------------------------------------------------------------------------------------------------
		if ( newDTCookie )
		{
			System.debug ( 'Creating DT cookie' ) ;
			dtCookie = new Cookie ( 'dt_Cookie', String.valueOfGmt ( applicationAge ), null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { dtCookie } ) ;
		}
		
		if ( newCIDCookie )
		{
			System.debug ( 'Creating CID cookie' ) ;
			Cookie cidCookie = new Cookie ( 'cid_Cookie', reservationID, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { cidCookie } ) ;
		}
		
		if ( newCVIDCookie )
		{
			System.debug ( 'Creating CVID cookie' ) ;
			Cookie cvidCookie = new Cookie ( 'cvid_Cookie', reservationVendorID, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { cvidCookie } ) ;				
		}
		
		if ( newAAIDCookie )
		{
			System.debug ( 'Creating AAID cookie' ) ;
			Cookie aaidCookie = new Cookie ( 'aaid_Cookie', affiliate, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { aaidCookie } ) ;
		}
		
		if ( newOFIDCookie )
		{
			System.debug ( 'Creating OFID cookie' ) ;
			Cookie ofidCookie = new Cookie ( 'ofid_Cookie', offerID, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { ofidCookie } ) ;
		}
		
		if ( newCRIDCookie )
		{
			System.debug ( 'Creating CRID cookie' ) ;
			Cookie cridCookie = new Cookie ( 'crid_Cookie', creativeID, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { cridCookie } ) ;
		}
		
		if ( newCTIDCookie )
		{
			System.debug ( 'Creating CTID cookie' ) ;
			Cookie ctidCookie = new Cookie ( 'ctid_Cookie', creativeType, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { ctidCookie } ) ;
		}
		
		if ( newPromoCookie )
		{
			System.debug ( 'Creating Promo cookie' ) ;
			Cookie prcdCookie = new Cookie ( 'prcd_Cookie', promotionCode, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { prcdCookie } ) ;
		}
        
		if ( newAdvocateCookie )
		{
			System.debug ( 'Creating Advocate cookie' ) ;
			Cookie apcdCookie = new Cookie ( 'apcd_Cookie', advocateCode, null, cookieAgeInDays * 24 * 60 * 60, false ) ;
			ApexPages.currentPage().setCookies ( new Cookie[] { apcdCookie } ) ;
		}
	}
	
	/* --------------------------------------------------------------------------------------------------
	 * Setter, non-application pages were we have to reload the data
	 * -------------------------------------------------------------------------------------------------- */
	public void loadThirdPartyData ( string aid )
	{
		try
		{
			//  Select into a list in the infetesimly small chance we get more than one.
			List<one_application__c> oL = [
				SELECT ID, Name,
				Application_Landing_Date__c,
				Creative_ID__c, Creative_Type__c,
				Offer_ID__c,
				ReservationID__c,
				Reservation_Affiliate_Vendor__c,
				Reservation_Affiliate_Vendor_ID__c,
				Reservation_Vendor_ID__c 
				FROM one_application__c
				WHERE ID = :aid
			] ;
			
			if ( ( oL != null ) && ( oL.size () > 0 ) ) 
			{
				//  Pull the first one
				one_application__c o = oL [ 0 ] ;
				
				//  Load the variables
				applicationAge = o.Application_Landing_Date__c ;
				reservationID = o.ReservationID__c ;
				reservationVendorID = o.Reservation_Vendor_ID__c ;
				affiliate = o.Reservation_Affiliate_Vendor_ID__c ;
				affiliateVendorId = o.Reservation_Affiliate_Vendor__c ;
	
				//  Rakuten specific
				offerID = o.Offer_ID__c ;
				creativeID = o.Creative_ID__c ;
				creativeType = o.Creative_Type__c ;
			}
		}
		catch ( Exception e )
		{
			// do nothing?
			System.debug ( 'Error selecting application' ) ;
		}
	}
	
	/* --------------------------------------------------------------------------------------------------
	 * Getter(s), affiliates
	 * -------------------------------------------------------------------------------------------------- */
	public Datetime getApplicationAge ()
	{
		return applicationAge ;
	}
	
	public String getReservationId ()
	{
		return reservationID ;
	}
	
	public String getReservationVendorId ()
	{
		return reservationVendorID ;
	}
	
	public String getAffiliateCode ()
	{
		return affiliate ;
	}
	
	public String getAffiliateVendorId ()
	{
		return affiliateVendorId ;
	}
	
	public String getOfferID ()
	{
		return offerID ;
	}
	
	public String getCreativeID ()
	{
		return creativeID ;
	}
	
	public String getCreativeType ()
	{
		return creativeType ;
	}
	
	/* --------------------------------------------------------------------------------------------------
	 * Internal
	 * -------------------------------------------------------------------------------------------------- */
	private void calculateHoldDuration ()
	{
		List<String> lS = new List<String> () ;
		
		if ( String.isNotBlank ( reservationVendorID ) )
			lS.add ( reservationVendorID ) ;
			
		if ( String.isNotBlank ( affiliateVendorID ) )
			lS.add ( affiliateVendorId ) ;
			
		System.debug ( 'SIZE :: ' + lS.size () ) ;

		if ( lS.size () > 0 )
		{
			Map<String, Account> aMap = new Map<String, Account> ( [
				SELECT ID, Referral_Tracking_Hold_Days__c, Tracking_Pixel__c
				FROM Account
				WHERE ID IN :lS
			] ) ;
			
			if ( ( String.isNotBlank ( affiliateVendorId ) ) && ( aMap.containsKey ( affiliateVendorId ) ) )
			{
				Account a = aMap.get ( affiliateVendorId ) ;
				
				if ( a.Referral_Tracking_Hold_Days__c != null )
					cookieAgeInDays = a.Referral_Tracking_Hold_Days__c.intValue () ;
					
				System.debug ( 'Using AAID to set age : ' + cookieAgeInDays ) ;
			}
			else if ( ( String.isNotBlank ( reservationVendorID ) ) && ( aMap.containsKey ( reservationVendorID ) ) )
			{
				Account a = aMap.get ( reservationVendorID ) ;
				
				if ( a.Referral_Tracking_Hold_Days__c != null )
					cookieAgeInDays = a.Referral_Tracking_Hold_Days__c.intValue () ;
					
				System.debug ( 'Using CVID to set age : ' + cookieAgeInDays ) ;
			}
		}
	}
	
	/* --------------------------------------------------------------------------------------------------
	 * Check promo code - uses data from cookies!
	 * -------------------------------------------------------------------------------------------------- */
	public PromotionData loadPromoCode ()
	{
		PromotionData pd = new PromotionData () ;
		
		//  Always save the promotion code
		pd.validPromotionCode = false ;
		pd.promoCode = promotionCode ;
		
        System.debug ( 'Promocode : "' + promotionCode + '"' ) ;
        
		//  DUH
		if ( String.isNotBlank ( promotionCode ) )
		{
			//  Part one, affiliate
			if ( String.isNotBlank ( offerID ) )
			{
				String thingy = '%' + offerID + '%' ;
				try
				{
					List<oneOffers__c> ooL = [
						SELECT ID, Plan__c, Product__c, SubProduct__c, Bill_Code__c, Terms_ID__c, Offer_Confirm_Text__c
						FROM oneOffers__c
						WHERE Active__c = true
						AND Start_Date__c <= TODAY
						AND End_Date__c >= TODAY
						AND Promotion_Code__c = :promotionCode
						AND External_Offer_ID__c LIKE :thingy
						ORDER BY ID
					] ;
					
					if ( ( ooL != null ) && ( ooL.size () > 0 ) )
					{
						pd.validPromotionCode = true ;
						pd.offerObj = ooL [ 0 ] ;
					}
				}
				catch ( Exception e )
				{
					pd.validPromotionCode = false ;
				}
			}
			else
			{
				try
				{
					List<oneOffers__c> ooL = [
						SELECT ID, Plan__c, Product__c, SubProduct__c, Bill_Code__c, Terms_ID__c, Offer_Confirm_Text__c
						FROM oneOffers__c
						WHERE Active__c = true
						AND Start_Date__c <= TODAY
						AND End_Date__c >= TODAY
						AND Promotion_Code__c = :promotionCode
						ORDER BY ID
					] ;
					
					if ( ( ooL != null ) && ( ooL.size () > 0 ) )
					{
						pd.validPromotionCode = true ;
						pd.offerObj = ooL [ 0 ] ;
						pd.promoCode = promotionCode ;
					}
				}
				catch ( Exception e )
				{
					pd.validPromotionCode = false ;
				}
			}
		}
		return pd ;
	}
    
	/* --------------------------------------------------------------------------------------------------
	 * Check advocate code - uses data from cookies!
	 * -------------------------------------------------------------------------------------------------- */
	public AdvocateProgramData loadAdvocateProgramCode ()
	{
		AdvocateProgramData apd = new AdvocateProgramData () ;
		
		//  Always save the promotion code
		apd.validAdvocateCode = false ;
		apd.advocateProgramCode = advocateCode ;
        
        if ( String.isNotBlank ( apd.advocateProgramCode ) )
            apd.advocateProgramCode = apd.advocateProgramCode.toUpperCase () ;
		
		if ( String.isNotBlank ( advocateCode ) )
		{
            list<Referral_Code__c> rcL = null ;
            
            try
            {
                rcL = [
                    SELECT ID, Name
                    FROM Referral_Code__c
                    WHERE Name = :advocateCode.toUpperCase ()
                    ORDER BY CreatedDate DESC
                ] ;
            }
            catch ( DMLException e )
            {
                rcL = null ;
            }
            
            if ( ( rcL != null ) && ( rcL.size () > 0 ) )
            {
	            apd.validAdvocateCode = true ;
                
                apd.referralObj = rcL [ 0 ] ;
            }
        }
        
        return apd ;
    }
    
	/* --------------------------------------------------------------------------------------------------
	 *  Clear out cookies after
	 * -------------------------------------------------------------------------------------------------- */
	public void removeALLCookies ()
	{
		System.debug ( 'Removing Prior Cookies' ) ;
		
		Cookie dtCookie = new Cookie ( 'dt_Cookie', '', null, 0, false ) ;
		Cookie cidCookie = new Cookie ( 'cid_Cookie', '', null, 0, false ) ;
		Cookie cvidCookie = new Cookie ( 'cvid_Cookie', '', null, 0, false ) ;
		Cookie aaidCookie = new Cookie ( 'aaid_Cookie', '', null, 0, false ) ;
		Cookie ofidCookie = new Cookie ( 'ofid_Cookie', '', null, 0, false ) ;
		Cookie cridCookie = new Cookie ( 'crid_Cookie', '', null, 0, false ) ;
		Cookie ctidCookie = new Cookie ( 'ctid_Cookie', '', null, 0, false ) ;
		Cookie prcdCookie = new Cookie ( 'prcd_Cookie', '', null, 0, false ) ;
		Cookie apcdCookie = new Cookie ( 'apcd_Cookie', '', null, 0, false ) ;
		
		ApexPages.currentPage().setCookies ( new Cookie[] { dtCookie, cidCookie, cvidCookie, aaidCookie, ofidCookie, cridCookie, ctidCookie, prcdCookie, apcdCookie } ) ;
	}
	
	public void removeALLCookiesExceptPromotion ()
	{
		System.debug ( 'Removing Prior Cookies - NO PROMO!' ) ;
		
		Cookie dtCookie = new Cookie ( 'dt_Cookie', '', null, 0, false ) ;
		Cookie cidCookie = new Cookie ( 'cid_Cookie', '', null, 0, false ) ;
		Cookie cvidCookie = new Cookie ( 'cvid_Cookie', '', null, 0, false ) ;
		Cookie aaidCookie = new Cookie ( 'aaid_Cookie', '', null, 0, false ) ;
		Cookie ofidCookie = new Cookie ( 'ofid_Cookie', '', null, 0, false ) ;
		Cookie cridCookie = new Cookie ( 'crid_Cookie', '', null, 0, false ) ;
		Cookie ctidCookie = new Cookie ( 'ctid_Cookie', '', null, 0, false ) ;
		Cookie apcdCookie = new Cookie ( 'apcd_Cookie', '', null, 0, false ) ;
		
		ApexPages.currentPage().setCookies ( new Cookie[] { dtCookie, cidCookie, cvidCookie, aaidCookie, ofidCookie, cridCookie, ctidCookie, apcdCookie } ) ;
	}
	
	/* --------------------------------------------------------------------------------------------------
	 *  Rakuten tracking pixel link
	 * -------------------------------------------------------------------------------------------------- */
	public String getRakutenTrackingLinkUNITYVisa ( String orderId )
	{
		return getRakutenTrackingLink ( orderId, 'Application', '1', '0', 'USD' ) ;
	}
	
	public String getRakutenTrackingLink ( String orderId, String sku, String q, String amt, String cur )
	{
		return getRakutenTrackingLink ( orderId, sku, q, amt, cur, '' ) ;
	}
	
	public String getRakutenTrackingLink ( String orderId, String sku, String q, String amt, String cur, String name )
	{
		String s = '' ;
		
		if ( String.isNotBlank ( rakutenTrackingID ) )
		{
			s = s + '<img src="https://track.linksynergy.com/eventnvppixel' ;
			
			s = s + '?mid='+ rakutenTrackingID ;
			s = s + '&ord=' + orderId ;
			
			if ( String.IsNotBlank ( reservationID ) )
				s = s + '&tr=' + reservationID ;
				
			if ( applicationAge != null )
				s = s + '&land=' + applicationAge.formatGMT ( 'yyyyMMdd_hhmm' ) ;
			
			s = s + '&skulist=' + sku ;
			s = s + '&qlist=' + q ;
			s = s + '&amtlist=' + amt ;
			s = s + '&cur=' + cur ;
			
			if ( String.isNotBlank ( name ) )
				s = s + '&namelist=' + name ;
			
	//		s = s + '&' ;
			s = s + '">' ;
		}
		
		return s ;
	}
	
	/* --------------------------------------------------------------------------------------------------
	 * Cookie function
	 * -------------------------------------------------------------------------------------------------- */
	private String checkForCookieValue ( String cookieName )
	{
		System.debug ( 'Checking against ' + cookieName ) ;
		
		Cookie xCookie = ApexPages.currentPage().getCookies().get ( cookieName ) ;
		
		if ( xCookie == null )
		{
			System.debug ( cookieName + ' Cookie is empty, doing nothing' ) ;
			return null ;
		}
		else
		{
			System.debug ( 'Getting value from ' + cookieName + '!' ) ;
			return xCookie.getValue () ;
		}
	}
	
    
	/* --------------------------------------------------------------------------------------------------
	 * Promo class to get around ... issues
	 * -------------------------------------------------------------------------------------------------- */
	public class PromotionData
	{
		public boolean validPromotionCode ;
		public String promoCode ;
		
		public oneOffers__c offerObj ;
	}
    
	/* --------------------------------------------------------------------------------------------------
	 * advocate class to get around ... issues
	 * -------------------------------------------------------------------------------------------------- */
	public class AdvocateProgramData
	{
		public boolean validAdvocateCode ;
		public String advocateProgramCode ;
		
		public Referral_Code__c referralObj ;
	}
}