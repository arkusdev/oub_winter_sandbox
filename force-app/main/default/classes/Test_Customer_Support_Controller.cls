@isTest
public class Test_Customer_Support_Controller 
{
    // --------------------------------------------------------------------------------
    // Test setup
    // --------------------------------------------------------------------------------
    @testSetup
    private static void setup ()
    {
        list<Case_Topic__c> ctL = new list<Case_Topic__c> () ;
        
        Case_Topic__c ct1 = new Case_Topic__c () ;
        ct1.Name = 'Advocate Link' ;
        ct1.Classification__c = 'Advocate_Link' ;
        ct1.Active__c = true ;
        ct1.Message__c = 'Yo Noid!' ;
        ct1.Status__c = 'Proposed Response' ;
        ct1.Order_By__c = 1 ;
        
        ctL.add ( ct1 ) ;
        
        Case_Topic__c ct2 = new Case_Topic__c () ;
        ct2.Name = 'ATM Deposit' ;
        ct2.Classification__c = 'ATM_Deposit' ;
        ct2.Active__c = true ;
        ct2.Message__c = 'Yo Noid!' ;
        ct2.Status__c = 'Proposed Response' ;
        ct2.Order_By__c = 2 ;
        
        ctL.add ( ct2 ) ;
        
        Case_Topic__c ct3 = new Case_Topic__c () ;
        ct3.Name = 'Other' ;
        ct3.Classification__c = 'Other' ;
        ct3.Active__c = true ;
        ct3.Status__c = 'Needs Agent' ;
        ct3.Order_By__c = 3 ;
        
        ctL.add ( ct3 ) ;
        
        insert ctL ;
        
        Account a = new Account () ;
        a.name = 'Insight Customers - Individuals' ;
        
        insert a ;
    }
    
    // --------------------------------------------------------------------------------
	// Setup, Contact
	// --------------------------------------------------------------------------------
	private static Contact getContact ( Integer i, Account a )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '13245600' + String.valueOf ( x ) ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        c.AccountId = a.ID ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}

    // --------------------------------------------------------------------------------
	// Go!
	// --------------------------------------------------------------------------------
    public static testmethod void ONE ()
    {
        Case c = new Case () ;
		ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
        
        Customer_Support_Controller csc = new Customer_Support_Controller ( sc ) ;
        
        Test.startTest () ;
        
        //  Empty
        csc.validateClassificationChoice () ;
        
        csc.validateClassificationChoice () ;
        
        List<SelectOption> ccdd = csc.caseClassificationDropDown ;
        
        csc.selectedOption = ccdd.get ( 1 ).getValue () ; // 0 is not picked
        
        csc.validateClassificationChoice () ;

        csc.selectedOption = ccdd.get ( 2 ).getValue () ; // 0 is not picked
        
        csc.validateClassificationChoice () ;
        
        csc.goYES () ;

        csc.goNO () ;
        
        csc.firstName = 'Test' ;
        csc.lastName = 'User' ;
        csc.emailAddress = 'test@user.com' ;
        csc.phoneNumber = '1234567980' ;
        
        csc.address = '100 franklin street' ;
        csc.city = 'Boston' ;
        csc.stateOption = 'MA' ;
        csc.zipCode = '02110' ;
        
        csc.description = 'Yo Noid!' ;
        
        csc.goSubmit () ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void TWO ()
    {
        Account a = [SELECT ID FROM Account WHERE Name = 'Insight Customers - Individuals' LIMIT 1 ] ;
        
        Contact c1 = getContact ( 1, a ) ;
        insert c1 ;
        
        Case c = new Case () ;
		ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
        
        Customer_Support_Controller csc = new Customer_Support_Controller ( sc ) ;
        
        Test.startTest () ;
        
        //  Empty
        csc.validateClassificationChoice () ;
        
        csc.validateClassificationChoice () ;
        
        List<SelectOption> ccdd = csc.caseClassificationDropDown ;
        
        csc.selectedOption = ccdd.get ( 3 ).getValue () ; // 0 is not picked
        
        csc.validateClassificationChoice () ;
        
        csc.goNO () ;
        
        csc.firstName = c1.FirstName ;
        csc.lastName = c1.LastName ;
        csc.emailAddress = c1.Email ;
        csc.phoneNumber = c1.Phone ;
        
        csc.address = '100 franklin street' ;
        csc.city = 'Boston' ;
        csc.stateOption = 'MA' ;
        csc.zipCode = '02110' ;
        
        csc.description = 'Yo Noid!' ;
        
        csc.goSubmit () ;
        
        csc.subject = 'Yes' ;
        
        csc.goSubmit () ;
        
        Test.stopTest () ;
    }
}