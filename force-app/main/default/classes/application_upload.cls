public with sharing class application_upload 
{
    /*  ----------------------------------------------------------------------------------------
     *  Preliminary Inputs
     *  ---------------------------------------------------------------------------------------- */
    //  First Name
    public String firstName 
    { 
        get ; 
        set ; 
   	}
   	
   	//  Last Name
    public String lastName 
    { 
        get ; 
        set ; 
   	}
    
   	//  Email
    public String email 
    { 
        get ; 
        set ; 
   	}
    
   	//  Phone
    public String phone 
    { 
        get ; 
        set ; 
   	}
    
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	//  Ticket object to upload
	public Ticket__c ticket 
	{
		get 
		{
			if ( ticket == null )
				ticket = new Ticket__c () ;
			return ticket ;
		}
		
		set ;
	}
	
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
 
	//  Step control for panels on page
	public Integer dStepNumber
	{
		get ;
		private set ;
	}
	
	//  Checking to see if they have a contact already
	private Contact c ;

    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorFirstName
    {
    	get ;
    	private set ;
    }
    
    public boolean errorLastName
    {
    	get ;
    	private set ;
    }
    
    public boolean errorEmail
    {
    	get ;
    	private set ;
    }
    
    public boolean errorPhone
    {
    	get ;
    	private set ;
    }
    
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public application_Upload ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		dStepNumber = 1000 ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Upload
     *  ---------------------------------------------------------------------------------------- */
    public PageReference uploadFile ()
    {
		boolean isValid = true ;
    	errorFirstName = false ;
    	errorLastName = false ;
    	errorEmail = false ;
    	errorPhone = false ;
    	
		//  Validation
		if ( String.isBlank ( firstName ) )
		{
			System.debug ( 'First Name not valid!' ) ;
			errorFirstName = true ;
			isValid = false ;
		}
		
		if ( String.isBlank ( lastName ) )
		{
			System.debug ( 'Last Name not valid!' ) ;
			errorLastName = true ;
			isValid = false ;
		}
		
		if ( ( String.isBlank ( email ) ) || 
		     ( ! OneUnitedUtilities.isValidEmail ( email ) ) )
		{
			System.debug ( 'Email not valid!' ) ;
			errorEmail = true ;
			isValid = false ;
		}
		
		if ( ( String.isBlank ( phone ) ) ||  
		     ( ! OneUnitedUtilities.isValidPhoneNumber ( phone ) ) )
		{
			System.debug ( 'Phone not valid!' ) ;
			errorPhone = true ;
			isValid = false ;
		}
		
		if ( attachment.body == null )
		{
			System.debug ( 'Attachment body empty!' ) ;
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ( String.isNotBlank ( attachment.name ) ) && ( String.isNotBlank ( attachment.ContentType ) ) ) 
		{
			System.debug ( 'NAME : ' + attachment.name ) ;
			System.debug ( 'TYPE : ' + attachment.ContentType ) ;
			
			//  Assume wrong unless we find otherwise
			boolean foundValid = false ;
			
			if ( ( attachment.name.endsWithIgnoreCase ( 'gif' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/gif' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'jpg' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/jpeg' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'jpeg' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/jpeg' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'jpg' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/pjpeg' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'jpeg' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/pjpeg' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'pdf' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'application/pdf' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'png' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/png' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'tiff' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/tiff' ) ) )
			{
				foundValid = true ;
			}
			else if ( ( attachment.name.endsWithIgnoreCase ( 'tiff' ) ) && ( attachment.ContentType.equalsIgnoreCase ( 'image/x-tiff' ) ) )
			{
				foundValid = true ;
			}
			
			if ( ! foundValid )
			{
				System.debug ( 'Did not find an acceptable attachment.') ;
				errorFile = true ;
				isValid = false ;
			}
		}
		else
		{
			System.debug ( 'Attachment not valid!' ) ;
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        attachment = new Attachment () ;  //  Safari mobile bug?
	        dStepNumber = 1001 ;
		}
		else
		{
	        System.debug ( '========== Inputs Valid, creating objects! ==========' ) ;
			dStepNumber = 2000 ;
						
			// --------------------------------------
			// Handle contact
			// --------------------------------------
			try
			{
				System.debug ( 'Contact Search!' ) ;
				
				//  Search for contact
				Contact pc = OneUnitedUtilities.lookupContact ( email ) ;

				if ( pc != null )
				{
					if ( firstName.equalsIgnoreCase ( pc.FirstName ) &&
					     lastName.equalsIgnoreCase ( pc.LastName ) )
					{
						System.debug ( 'Found a matching pre-existing contact!' ) ;
						c = pc ;
					}
					else
					{
						System.debug ( 'Name does not match' ) ;
						System.debug ( '"' + pc.FirstName + '" -- "' + firstName + '"' ) ;
						System.debug ( '"' + pc.LastName + '" -- "' + lastName + '"' ) ;
						
						System.debug ( 'Creating prospect contact!' ) ;
						c = new Contact () ;
						c.FirstName = firstName ;
						c.LastName = lastName ;
						c.Phone = phone ;
						c.Email = email ;
						c.RecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
						
						insert c ;
					}
				}
				else
				{
					System.debug ( 'Creating prospect contact!' ) ;
					c = new Contact () ;
					c.FirstName = firstName ;
					c.LastName = lastName ;
					c.Phone = phone ;
					c.Email = email ;
					c.RecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
					
					insert c ;
				}
	    	}
	    	catch ( DMLException e )
	    	{
	    		System.debug ( 'Error Creating contact!' ) ;
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
			        System.debug ( e.getDmlMessage ( i ) ) ; 
	    		
	    		dStepNumber = 1002 ;
	    		return null ;
	    	}
			
			// --------------------------------------
			// Insert ticket
			// --------------------------------------
			try
			{
				System.debug ( 'Creating ticket!' ) ;
				ticket.First_Name__c = firstName ;
				ticket.Last_Name__c = lastName ;
				ticket.Email_Address__c = email ;
				ticket.Phone__c = phone ;
				ticket.Contact__c = c.ID ;
				ticket.Subject__c = 'Pre-filled Application' ;
				ticket.RecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Support', 'Ticket__c' ) ;
				insert ticket ;			
	    	}
	    	catch ( DMLException e )
	    	{
	    		System.debug ( 'Error Creating ticket!' ) ;
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
			        System.debug ( e.getDmlMessage ( i ) ) ; 
	    		
	    		dStepNumber = 1002 ;
	    		return null ;
	    	}
			
			// --------------------------------------
			// Insert attachment
			// --------------------------------------
			try
			{
				System.debug ( 'Creating attachment!' ) ;
				attachment.ParentID = ticket.ID ;
				insert attachment ;
	    	}
	    	catch ( DMLException e )
	    	{
	    		System.debug ( 'Error Creating attachment!' ) ;
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
			        System.debug ( e.getDmlMessage ( i ) ) ; 
	    		
	    		dStepNumber = 1002 ;
	    		return null ;
	    	}
		}
		
		return null ;
    }
   
}