global class EverFiCalloutSchedule implements Schedulable 
{
	global void execute ( SchedulableContext sc ) 
    {
        EverFiCalloutBatch b = new EverFiCalloutBatch () ;
        Database.executeBatch ( b ) ;
    }
}