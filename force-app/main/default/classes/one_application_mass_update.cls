public with sharing class one_application_mass_update
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public List<oneData> oDL
 	{ 
 		get; 
 		private set; 
 	}
 	
 	public Integer recordCount
 	{
 		get ;
 		private set ;
 	}

    /*  ----------------------------------------------------------------------------------------
     *  Controller objects
     *  ---------------------------------------------------------------------------------------- */
    private List<one_application__c> oL ;
     
    /*  ----------------------------------------------------------------------------------------
     *  Load Data
     *  ---------------------------------------------------------------------------------------- */
	private void loadData ()
	{
		//  Load data
		oL = [ 
			SELECT
			Id, Name, FIS_Decision_Code__c, FIS_Prior_Decision_Code__c,
			( SELECT OldValue FROM Histories WHERE Field='FIS_Decision_Code__c' ORDER BY CreatedDate DESC LIMIT 1 ) 
			FROM one_application__c 
			WHERE FIS_Prior_Decision_Code__c = NULL
			ORDER BY Name
			LIMIT :recordCount
		] ;
		
		oDL = new List<oneData> () ;
		
		//  Create display object
		for ( one_application__c o : oL )
		{
			if ( ( o.Histories != null ) && ( o.Histories.size () > 0 ) )
			{
				one_application__history h = o.Histories ;
				
				if ( String.isNotEmpty ( String.valueOf ( h.OldValue ) ) )
				{
					oneData oD = new oneData ();
					oD.oneId = o.Id ;
					oD.appId = o.Name ;
					oD.currentCode = o.FIS_Decision_Code__c ;
					oD.priorCode = o.FIS_Prior_Decision_Code__c ;
					
					oD.historyCode = String.valueOf ( h.OldValue ) ;
					
					if ( String.isEmpty ( o.FIS_Prior_Decision_Code__c ) )
						o.FIS_Prior_Decision_Code__c = String.valueOf ( h.OldValue ) ;
					
					System.debug ( 'Adding record ' + o.Name ) ;
					oDL.add ( oD ) ;
				}
			}
		}
		
		//  Remove things that don't need updating
		Integer j = 0 ;

		while ( j < oL.size () )
		{
			if ( String.isBlank ( oL.get ( j ).FIS_Prior_Decision_Code__c ) )
			{
				System.debug ( 'Removing - ' + oL.get ( j ).name ) ;
				oL.remove ( j ) ;
			}
			else
			{
				System.debug ( 'Keeping - ' + oL.get ( j ).name ) ;
				j++;
			}
		}
	}
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public one_application_mass_update ( ApexPages.Standardcontroller stdC )
	{
		//  Get number of records to handle
		if ( one_settings__c.getInstance('settings').Mass_Update_Record_Count__c != null )
        	recordCount = Integer.valueOf ( one_settings__c.getInstance('settings').Mass_Update_Record_Count__c ) ;
        else
        	recordCount = 1 ;
		
		//  Initial data load
		loadData () ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Do the deed
     *  ---------------------------------------------------------------------------------------- */
    public PageReference updateData ()
    {
    	//  Tracking variables
    	Integer sCount = 0 ;
    	Integer fCount = 0 ;
    	
		//  Update records
		Database.SaveResult[] osr = Database.update ( oL, false ) ;

		//  Check results
		for ( Database.SaveResult sr : osr )
		{
			if ( !sr.isSuccess () )
			{
				fCount ++ ;
				
				for ( Database.Error e : sr.getErrors () )
				{
					ApexPages.addMessage ( new ApexPages.Message ( ApexPages.Severity.ERROR, e.getMessage () ) ) ;    	
				}
			}
			else
			{
				sCount++ ;
			}
		}

		//  Summary messages
		if ( sCount > 0 )
			ApexPages.addMessage ( new ApexPages.Message ( ApexPages.Severity.INFO, sCount + ' Records Successfully Updated' ) ) ;
			
		if ( fCount > 0 )
			ApexPages.addMessage ( new ApexPages.Message ( ApexPages.Severity.INFO, fCount + ' Records Failed to Update' ) ) ;

		//  Reload Data
    	loadData () ;
    	
    	return null ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Summary classes for display purposes
     *  ---------------------------------------------------------------------------------------- */
	public class oneData
	{
		public String oneId { get; set; }
		public String appId { get; set; }
		public String priorCode { get; set; }
		public String currentCode { get; set; }
		public String historyCode { get; set; }
	}
}