public class DepositAccountHelper 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Variables
     *  -------------------------------------------------------------------------------------------------- */
    private one_settings__c settings ;
    
    private String anderaURL ;
    private String anderaFIID ;
    private boolean redirectExistingCustomer ;
    private String onlineBankingLoginURL ;
    private Double percentDepositAppRedirect ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public DepositAccountHelper ()
    {
		//  Andera defaults
        anderaURL = 'https://secure.andera.com/index.cfm' ;
        anderaFIID = 'C64475A74197426784B89B3F1E14783C' ;
        
		//  redirect default
		onlineBankingLoginURL = 'https://www.oneunited.com/personal-banking/online-banking/online-banking-login/' ;
        
        //  Default interstitial OFF
        redirectExistingCustomer = false ;
        
        //  Default percentage 0!
        percentDepositAppRedirect = 0 ;
        
        //  Settings
        settings = one_settings__c.getInstance('settings') ;

        if ( settings != null )
        {
            if ( String.isNotBlank ( settings.Andera_Deposit_Application_URL__c ) )
                anderaURL = settings.Andera_Deposit_Application_URL__c ;
            
            if ( String.isNotBlank ( settings.Andera_Deposit_Application_FIID__c ) )
                anderaFIID = settings.Andera_Deposit_Application_FIID__c ;
            
            if ( String.isNotBlank ( settings.Online_Banking_Login_URL__c ) )
                onlineBankingLoginURL = settings.Online_Banking_Login_URL__c ;
            
            if ( settings.Current_Customer_Redirect__c != null )
                redirectExistingCustomer = settings.Current_Customer_Redirect__c ;
            
            if ( settings.Percentage_Deposit_Application_Redirect__c != null )
                percentDepositAppRedirect = settings.Percentage_Deposit_Application_Redirect__c ; 
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Getters
     *  -------------------------------------------------------------------------------------------------- */
    public String getAnderaURL ()
    {
        return anderaURL ;
    }
    
    public String getAnderaFIID ()
    {
        return anderaFIID ;
    }
    
    public String getOnlineBankingLoginURL ()
    {
        return onlineBankingLoginURL ;
    }
    
    public boolean isRedirectExistingCustomer ()
    {
        return redirectExistingCustomer ;
    }
        
    /*  --------------------------------------------------------------------------------------------------
     *  Luigi special application randomization
     *  -------------------------------------------------------------------------------------------------- */
    public boolean sendToDepositApplication ()
    {
        //  Don't redirect unless proven otherwise
        boolean goDepositApp = false ;
        
        if ( percentDepositAppRedirect == 0 )
        {
            //  ZERO never redirect
            goDepositApp = false ;
        }
        else if ( percentDepositAppRedirect >= 100 )
        {
            //  ALWAYS redirect!
			goDepositApp = true ;
        }
        else
        {
            //  Calculate
            Double xRandom = math.random () * 100 ;
            
            System.debug ( xRandom + ' < ?!? = ' + percentDepositAppRedirect ) ;
            
            if ( xRandom <= percentDepositAppRedirect )
                goDepositApp = true ;
        }
        
        return goDepositApp ;
    }
}