@isTest
public class TestAdvocateProgramAwardInvocationFunct 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Contact getContact ( Integer x )
	{
        Account a = new Account();
        a.Name= 'Account'+x;
        insert a;
        
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
        c.AccountID=a.ID;
		
		return c ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  Activity
	//  --------------------------------------------------------------------------------------
    public static testmethod void TEST_Activity ()
    {
        list<Advocate_Activity__c> aaL = new list<Advocate_Activity__c> () ;
        
        Advocate_Activity__c aa1a = new Advocate_Activity__c () ;
        aa1a.API_Name__c = 'ACTIVITY1' ;
        aa1a.Activity_Points__c = 100 ;
        aa1a.Name = 'Activity 1' ;
        aa1a.Description__c = 'ABC123' ;
        
        aaL.add ( aa1a ) ;
        
        Advocate_Activity__c aa1b = new Advocate_Activity__c () ;
        aa1b.API_Name__c = 'ACTIVITY1' ;
        aa1b.Activity_Points__c = 100 ;
        aa1b.Name = 'Activity 1' ;
        aa1b.Description__c = 'ABC123' ;
        
        aaL.add ( aa1b ) ;
        
        Advocate_Activity__c aa2 = new Advocate_Activity__c () ;
        aa2.API_Name__c = 'ACTIVITY2' ;
        aa2.Activity_Points__c = 1000 ;
        aa2.Name = 'Activity 1' ;
        aa2.Description__c = 'ABC123' ;
        
        aaL.add ( aa2 ) ;
        
        insert aaL ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;

        Contact c2 = getContact ( 2 ) ;
        insert c2 ;

        Test.startTest () ;
        
        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar1 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar1.contactID = c1.ID ;
        apaafapar1.requestName = 'ACTIVITY1' ;
        apaafapar1.requestType = 'Activity' ;
        
        apaafaparL.add ( apaafapar1 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar2 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar2.contactID = c2.ID ;
        apaafapar2.requestName = 'ACTIVITY2' ;
        apaafapar2.requestType = 'Activity' ;
        
        apaafaparL.add ( apaafapar2 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL ) ;
        
        List<Advocate_Activity_Achieved__c> aaaL1 = [
            SELECT ID, Advocate_Activity_Points__c 
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( aaaL1.size (), 1 ) ;
        System.assertEquals ( aaaL1 [ 0 ].Advocate_Activity_Points__c, 100 ) ; // SET BY PROCESS!
        
        List<Advocate_Activity_Achieved__c> aaaL2 = [
            SELECT ID, Advocate_Activity_Points__c
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c2.ID 
        ] ;
        
        System.assertEquals ( aaaL2.size (), 1 ) ;
        System.assertEquals ( aaaL2 [ 0 ].Advocate_Activity_Points__c, 1000 ) ; // SET BY PROCESS!
        
        Test.stopTest () ;
    }

	//  --------------------------------------------------------------------------------------
	//  Badgers
	//  --------------------------------------------------------------------------------------
    public static testmethod void TEST_Badges ()
    {
        list<Advocate_Badge__c> abL = new list<Advocate_Badge__c> () ;
        
        Advocate_Badge__c aa1 = new Advocate_Badge__c () ;
        aa1.API_Name__c = 'BADGE1' ;
        aa1.Description__c = 'XYZ123' ;
        aa1.Badge_Points__c = 100 ;
        
        abL.add ( aa1 ) ;
        
        Advocate_Badge__c aa2 = new Advocate_Badge__c () ;
        aa2.API_Name__c = 'BADGE2' ;
        aa2.Description__c = 'XYZ123' ;
        aa2.Badge_Points__c = 1000 ;
        
        abL.add ( aa2 ) ;
        
        insert abL ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;

        Contact c2 = getContact ( 2 ) ;
        insert c2 ;

        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar11 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar11.contactID = c1.ID ;
        apaafapar11.requestName = 'BADGE1' ;
        apaafapar11.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar11 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar12 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar12.contactID = c1.ID ;
        apaafapar12.requestName = 'BADGE1' ;
        apaafapar12.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar12 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar2 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar2.contactID = c2.ID ;
        apaafapar2.requestName = 'BADGE2' ;
        apaafapar2.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar2 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL ) ;
        
        List<Advocate_Badge_Awarded__c> abaL1 = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( abaL1.size (), 1 ) ;
        
        List<Advocate_Badge_Awarded__c> abaL2 = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c2.ID 
        ] ;
        
        System.assertEquals ( abaL2.size (), 1 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar3 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar3.contactID = c1.ID ;
        apaafapar3.requestName = 'BADGE1' ;
        apaafapar3.requestType = 'Badge' ;
        
        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL2 = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        apaafaparL2.add ( apaafapar3 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL2 ) ;
        
        List<Advocate_Badge_Awarded__c> abaL1D = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( abaL1D.size (), 1 ) ;
    }

	//  --------------------------------------------------------------------------------------
	//  Rewards
	//  --------------------------------------------------------------------------------------
    public static testmethod void TEST_Rewards ()
    {
        list<Advocate_Reward__c> arL = new list<Advocate_Reward__c> () ;
        
        Advocate_Reward__c ar1 = new Advocate_Reward__c () ;
        ar1.API_Name__c = 'REWARD1' ;
        ar1.Description__c = 'XYZ123' ;
        
        arL.add ( ar1 ) ;
        
        Advocate_Reward__c ar2 = new Advocate_Reward__c () ;
        ar2.API_Name__c = 'REWARD2' ;
        ar2.Description__c = 'XYZ123' ;
        
        arL.add ( ar2 ) ;
        
        insert arL ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;

        Contact c2 = getContact ( 2 ) ;
        insert c2 ;

        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar1 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar1.contactID = c1.ID ;
        apaafapar1.requestName = 'REWARD1' ;
        apaafapar1.requestType = 'Reward' ;
        
        apaafaparL.add ( apaafapar1 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar2 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar2.contactID = c2.ID ;
        apaafapar2.requestName = 'REWARD2' ;
        apaafapar2.requestType = 'Reward' ;
        
        apaafaparL.add ( apaafapar2 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL ) ;
        
        List<Advocate_Reward_Achieved__c> araL1 = [
            SELECT ID
            FROM Advocate_Reward_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( araL1.size (), 1 ) ;
        
        List<Advocate_Reward_Achieved__c> araL2 = [
            SELECT ID
            FROM Advocate_Reward_Achieved__c
            WHERE Contact__c = :c2.ID 
        ] ;
        
        System.assertEquals ( araL2.size (), 1 ) ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  BLANK
	//  --------------------------------------------------------------------------------------
    public static testmethod void TEST_Blank ()
    {
        Advocate_Reward__c ar1 = new Advocate_Reward__c () ;
        ar1.API_Name__c = 'REWARD1' ;
        ar1.Description__c = 'XYZ123' ;
        
        insert ar1 ;
        
        Advocate_Badge__c ab1 = new Advocate_Badge__c () ;
        ab1.API_Name__c = 'BADGE1' ;
        ab1.Description__c = 'XYZ123' ;
        ab1.Badge_Points__c = 100 ;
        
        insert ab1 ;
        
        Advocate_Activity__c aa1a = new Advocate_Activity__c () ;
        aa1a.API_Name__c = 'ACTIVITY1' ;
        aa1a.Activity_Points__c = 100 ;
        aa1a.Name = 'Activity 1' ;
        aa1a.Description__c = 'ABC123' ;
        
        insert aa1a ;
        
        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar1 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar1.contactID = null ;
        apaafapar1.requestName = 'ACTIVITY1' ;
        apaafapar1.requestType = 'Activity' ;
        
        apaafaparL.add ( apaafapar1 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar2 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar2.contactID = null ;
        apaafapar2.requestName = 'BADGE1' ;
        apaafapar2.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar2 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar3 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar3.contactID = null ;
        apaafapar3.requestName = 'REWARD1' ;
        apaafapar3.requestType = 'Reward' ;
        
        apaafaparL.add ( apaafapar3 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL ) ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Go Away
	//  --------------------------------------------------------------------------------------
    public static testmethod void TEST_NOT_Participating ()
    {
        Advocate_Level__c al1 = new Advocate_Level__c () ;
        al1.Name = 'Non Participant' ;
        al1.Public_Use__c = false ;
        
        insert al1 ;
        
        Advocate_Reward__c ar1 = new Advocate_Reward__c () ;
        ar1.API_Name__c = 'REWARD1' ;
        ar1.Description__c = 'XYZ123' ;
        
        insert ar1 ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.Advocate_Level__c = al1.ID ;
        insert c1 ;
        
        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar1 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar1.contactID = c1.ID ;
        apaafapar1.requestName = 'REWARD1' ;
        apaafapar1.requestType = 'Reward' ;
        
        apaafaparL.add ( apaafapar1 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL ) ;
        
        List<Advocate_Reward_Achieved__c> araL1 = [
            SELECT ID
            FROM Advocate_Reward_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( araL1.size (), 0 ) ;
    }
}