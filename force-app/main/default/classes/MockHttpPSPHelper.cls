@isTest
global class MockHttpPSPHelper implements HTTPCalloutMock {
    global HTTPResponse respond (HTTPRequest request) {
        HTTPResponse resp = new HTTPResponse();
        
       	resp.setStatusCode(200);
        
        if ( request.getEndpoint().containsIgnoreCase('segments') )
        {
            // copied from documentation Segment: Get All
            resp.setBody ('[' +
                          '{' +
                          '"hidden": false,' +
                          '"hidden_at": null,' +
                          '"name": "first_name",' +
                          '"id": "55f2f386636f6e34a4020000",' +
                          '"users_count": 5,' +
                          '"groups": [' +
                          '{' +
                          '"join_type": "+",' +
                          '"rules": [' +
                          '{' +
                          '"time_frame": "",' +
                          '"match_value_type": "",' +
                          '"match_type": "equal",' +
                          '"match_value": "michal",' +
                          '"match_end_value": "",' +
                          '"event_resource_type": "",' +
                          '"event_resource_id": "",' +
                          '"type": "first_name",' +
                          '"in_app_event_name": "",' +
                          '"locations": null' +
                          '}' +
                          ']' +
                          '}' +
                          ']' +
                          '},' +
                          '{' +
                          '"hidden": false,' +
                          '"hidden_at": null,' +
                          '"name": "email contains Joachim",' +
                          '"id": "5591aebd636f6e75070d0000",' +
                          '"users_count": 0,' +
                          '"groups": [' +
                          '{' +
                          '"join_type": "+",' +
                          '"rules": [' +
                          '{' +
                          '"time_frame": "",' +
                          '"match_value_type": "",' +
                          '"match_type": "contains",' +
                          '"match_value": "joachim",' +
                          '"match_end_value": "",' +
                          '"event_resource_type": "",' +
                          '"event_resource_id": "",' +
                          '"type": "first_name",' +
                          '"in_app_event_name": "",' +
                          '"locations": null' +
                          '}' +
                          ']' +
                          '}' +
                          ']' +
                          '}' +
                          ']');
        }
        else if ( request.getEndpoint().containsIgnoreCase('groups') )
        {
            // copied from documentation Groups: Get One
            resp.setBody ('{' +
                          '"hidden": false,' +
                          '"hidden_at": null,' +
                          '"name": "groupXX",' +
                          '"resource_type": "Mobile::App::Campaign",' +
                          '"resource_ids": [' +
                          '"58591aec636f6e9a51c00300",' +
                          '"58591acc636f6e9a51ae0300",' +
                          '"58591a9e636f6e9a51a80300",' +
                          '"58591a6d636f6e9a51a50300",' +
                          '"5873927a636f6ef6a9ab0000",' +
                          '"58739219636f6ef6a9a80000"' +
                          '],' +
                          '"id": "587384b3636f6ea5f5110000"' +
                          '}');
        }
        else if ( request.getEndpoint().containsIgnoreCase('rules') )
        {
            // copied from documentation for Segments: Get Available Rules
            resp.setBody ('{' +
                          '"rules": [' +
                          '{' +
                          '"type": "email",' +
                          '"data_type": "string",' +
                          '"match_types": [' +
                          '"equal",' +
                          '"not_equal",' +
                          '"starts_with",' +
                          '"ends_with",' +
                          '"contains"' +
                          ']' +
                          '},' +
                          '{' +
                          '"type": "first_name",' +
                          '"data_type": "string",' +
                          '"match_types": [' +
                          '"equal",' +
                          '"not_equal",' +
                          '"starts_with",' +
                          '"ends_with",' +
                          '"contains"' +
                          ']' +
                          '}' +
                          ']' +
                          '}');
        }
        else if ( request.getEndpoint().containsIgnoreCase('campaigns') )
        {
            if ( request.getMethod().equalsIgnoreCase('POST') )
            {
            	resp.setStatusCode(201);
            }
            
            // copied from documentation for Campaigns: Get All
            resp.setBody ('[' +
                          '{' +
                          '"end_at": null,' +
                          '"guid": "s3BuOq4W",' +
                          '"hidden": false,' +
                          '"hidden_at": null,' +
                          '"name": "Phelim - Home",' +
                          '"start_at": "2016-01-05T17:44:08+00:00",' +
                          '"type": "CardPushNotificationCampaign",' +
                          '"id": "568c00e8636f6e75187e0000",' +
                          '"subscriptions_count": 1,' +
                          '"state": "active"' +
                          '},' +
                          '{' +
                          '"end_at": null,' +
                          '"guid": "HL7TOgE9",' +
                          '"hidden": false,' +
                          '"hidden_at": null,' +
                          '"name": "Hi",' +
                          '"start_at": "2015-12-09T12:39:18+00:00",' +
                          '"type": "CardPushNotificationCampaign",' +
                          '"id": "566820f6636f6e41e47b0000",' +
                          '"subscriptions_count": 0,' +
                          '"state": "active"' +
                          '}' +
                          ']');            
        }
        else if ( request.getEndpoint().containsIgnoreCase('users') )
        {
            // copied from documentation for Users: Get One
            resp.setBody ( '{' +
                          '"age": null,' +
                          '"alias": "mcksnk",' +
                          '"control_group_campaign_ids": [],' +
                          '"dedupe_devices": [' +
                          '{' +
                          '"active": true,' +
                          '"app_version": "2.9.0RC2",' +
                          '"bluetooth_state": "off",' +
                          '"language": "en",' +
                          '"os_version": "6.0.1",' +
                          '"sdk_version": "2.9.0RichNotification",' +
                          '"timezone": "GMT+01:00",' +
                          '"token": "sample_token",' +
                          '"type": "android",' +
                          '"uninstalled": false' +
                          '}' +
                          '],' +
                          '"firstName": "sample_first",' +
                          '"gender": "male",' +
                          '"has_reversed_location": true,' +
                          '"lastName": "sample_last",' +
                          '"uninstalled_at": null,' +
                          '"id": "5c20c37511807a687ee40100"' +
                          '}');
        }
        else if ( request.getEndpoint().containsIgnoreCase('push_opt_outs') )
        {
            // copied from documentation for Push Opt Outs: Get All
            resp.setBody( '[' +
                         '{' +
                         '"alias": "test",' +
                         '"device_guids": ["test-guid-1", "test-guid-2"],' +
                         '"last_opt_out_at": "2017-01-01T08:00:00+00:00",' +
                         '},' +
                         '{' +
                         '"alias": "test-2",' +
                         '"device_guids": ["test-guid-3"],' +
                         '"last_opt_out_at": "2017-01-02T08:00:00+00:00",' +
                         '},' +
                         ']');
        }
        else if ( request.getEndpoint().containsIgnoreCase('deeplinks') )
        {
            resp.setBody( '[' +
                         '{' +
                         '"created_at": "2016-08-16T10:11:38.758Z",' +
                         '"nickname": "nickname_1",' +
                         '"target": "target_1",' +
                         '"id": "57b2e6da636f6e39b80b0500"' +
                         '},' +
                         '{' +
                         '"created_at": "2016-09-22T11:11:45.067Z",' +
                         '"nickname": "nickname_2",' +
                         '"target": "target_2",' +
                         '"id": "57e3bc71636f6eae948a0400"' +
                         '}' +
                         ']');
        }
        
        return resp;
    }
}