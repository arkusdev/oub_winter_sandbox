public with sharing class VendorFileController 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public String isAdministrator
 	{ 
 		get; 
 		private set; 
 	}
 	
 	public String url
 	{
 		get ;
 		private set ;
 	}
 	
 	public Integer mode
 	{
 		get ;
 		private set ;
 	}
 	
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public VendorFileController ( ApexPages.Standardcontroller stdC )
	{
		//  Get the object from the page
		Vendor_Management__c vm = (Vendor_Management__c) stdC.getRecord () ;
		
		//  Set the defaults
		isAdministrator = 'N' ;
		url = 'https://portal.oneunited.local/intranet/GLBA_Vendors/vendorFiles.asp' ;
		mode = 999 ;
		
		//  Permission set to check against
		List<PermissionSetAssignment> psaL = null ;
		
		//  Check for ID
		if ( vm.GLBA_Vendor_ID__c == null )
		{
			mode = 500 ;
		}
		else
		{
			//  Check for administrator permission
			if ( Test.isRunningTest () )
			{
				psaL = [
					SELECT PermissionSetId 
					FROM PermissionSetAssignment 
					WHERE AssigneeId= :UserInfo.getUserId () 
					AND PermissionSet.Name = 'VM_TEST_ADMIN'
				] ;
			}
			else
			{
				psaL = [
					SELECT PermissionSetId 
					FROM PermissionSetAssignment 
					WHERE AssigneeId= :UserInfo.getUserId () 
					AND PermissionSet.Name = 'Vendor_Management_Full_Access'
				] ;
			}
			
			if ( ( psaL != null ) && ( psaL.size () > 0 ) )
			{
				isAdministrator = 'Y' ;
				mode = 0 ;
			}
			else
			{
				//  Check for read only permission
				if ( Test.isRunningTest () )
				{
					psaL = [
						SELECT PermissionSetId 
						FROM PermissionSetAssignment 
						WHERE AssigneeId= :UserInfo.getUserId () 
						AND PermissionSet.Name = 'VM_TEST_NORMAL'
					] ;
				}
				else
				{
					psaL = [
						SELECT PermissionSetId 
						FROM PermissionSetAssignment 
						WHERE AssigneeId= :UserInfo.getUserId () 
						AND PermissionSet.Name = 'Vendor_Management_Basic_Access'
					] ;
				}
					
				if ( ( psaL != null ) && ( psaL.size () > 0 ) )
				{
					isAdministrator = 'N' ;
					mode = 0 ;
				}
			}
		}		
	}
}