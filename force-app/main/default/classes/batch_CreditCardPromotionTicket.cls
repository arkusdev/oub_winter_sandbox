global class batch_CreditCardPromotionTicket implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	private Map<String,String> groupMap ;
	private List<RecordType> rtL ;
	
    global batch_CreditCardPromotionTicket ()
    {
    	System.debug ( '----------------------------- CONSTRUCTOR -----------------------------' ) ;
    	
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
		
		//  Set up the groups we need for later
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		//  Set up the record types we need for later
		
		List<String> rtNameList = new List<String> () ;
		rtNameList.add ( 'Credit Card Refund' ) ;
		rtNameList.add ( 'CS Batch File' ) ;
		
		rtL = OneUnitedUtilities.getRecordTypeList ( rtNameList ) ; 
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
    	System.debug ( '----------------------------- START -----------------------------' ) ;
    	
		String recordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		String query = '' ;
		
		query += 'SELECT ID,  ' ;
		query += 'MJACCTTYPCD__c, CURRACCTSTATCD__c, Open_Date__c, ' ; 
		query += 'Past_Due_History__c, ' ;
		query += 'Credit_Card_Application__r.Promotion_Code__c, ' ;
		query += 'Credit_Card_Application__r.Offer__r.Dollar_Amount__c ' ;
		query += 'FROM Financial_Account__c ' ;
		query += 'WHERE MJACCTTYPCD__c = \'VISA\' ' ;
		query += 'AND ( NOT ( Past_Due_History__c LIKE \'%1%\' ) ) ' ;
		query += 'AND Past_Due_History__c NOT IN ( \'ZZZZZZZZZZ00\', \'ZZZZZZZZZZ0Z\', \'ZZZZZZZZZZZ0\', \'ZZZZZZZZZZZZ\' ) ' ;
		query += 'AND Credit_Card_Application__r.Promotion_Code__c <> NULL ' ;
		query += 'AND CURRACCTSTATCD__c = \'ACT\' ' ;
		query += 'AND Open_Date__c < LAST_90_DAYS ' ;
		query += 'AND Promotion_Batch_Ticket__c = NULL ' ;
		
		if ( String.isNotBlank ( recordTypeId ) )
			query += 'AND RecordTypeId = \'' + recordTypeId + '\' ' ;

//		query += 'ORDER BY Open_Date__c ' ;
		query += 'LIMIT 100 ' ;
		
			
		query += '' ;
		
		System.debug ( 'QQ :: ' + query ) ;
    
		return Database.getQueryLocator ( query ) ;
	}
    
	global void execute ( Database.Batchablecontext bc, List<Financial_Account__c> faL )
	{
		System.debug ( '------------------ BATCH EXECUTE START ------------------' ) ;
		
		if ( ( faL != null ) && ( faL.size () > 0 ) )
		{
			Integer accountCount = 0 ;
			double accountSum = 0.0 ;
			
			for ( Financial_Account__c fa : faL )
			{
				accountCount ++ ;
				accountSum += fa.Credit_Card_Application__r.Offer__r.Dollar_Amount__c ;
			}
			
			Ticket__c batchTicket = new Ticket__c () ;
			
			batchTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
			batchTicket.Status__c = 'New' ;
			batchTicket.Batch_Account_Total__c = accountCount ;
			batchTicket.Batch_Amount_Total__c = accountSum ;
			batchTicket.ownerId = groupMap.get ( 'Credit Card Batch Processing' ) ;
			batchTicket.Batch_Type__c = 'FIS' ;
			
			insert batchTicket ;
			
			list<Financial_Account__c> faN = new list<Financial_Account__c> () ;
			for ( Financial_Account__c fa : faL )
			{
				Financial_Account__c faX = new Financial_Account__c () ;
				faX.ID = fa.ID ;
				faX.Promotion_Batch_Ticket__c = batchTicket.ID ;
				
				faN.add ( faX ) ;
			}
			
			try
			{
				if ( faN.size () > 0 )
					update faN ;
			}
			catch ( Exception e )
			{
				System.debug ( 'Exception error: ' + e.getMessage() ) ;
			}
			
			System.debug ( 'Found :: ' + accountCount + ' accounts for ' + accountSum + ' amount.' ) ;
		}
		else
		{
			System.debug ( 'No new promotions found' ) ;
		}

		System.debug ( '------------------ BATCH EXECUTE END ------------------' ) ;
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Credit Card Promotion Ticket Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}