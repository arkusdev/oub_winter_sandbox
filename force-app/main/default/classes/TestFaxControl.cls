@isTest
public with sharing class TestFaxControl 
{
	public static testmethod void ONE ()
	{
		Ticket__c ticket = new Ticket__c () ;
	//	ticket.Name = 12456768 ;
	//	ticket.Id = xyza121aw2 ;
		
		insert ticket ;
        
       //Create ContentVersion For VersionId feeded to parent object ContentDocument
        ContentVersion cv1 = new ContentVersion();
        cv1.Title = 'File';
        cv1.PathOnClient = 'File.pdf';
        cv1.VersionData = blob.valueOf('body1');
		insert cv1;

       
       //Getting the document created by ContentVersion
        List<ContentDocument> documentList1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
       
        //Assigning Document Id to Con..LinkId
        ContentDocumentLink cd1 = new ContentDocumentLink();
        cd1.LinkedEntityId = ticket.Id;
        cd1.ContentDocumentId = documentList1[0].Id;
        cd1.ShareType = 'V'; 
        cd1.Visibility = 'AllUsers';
        
        insert cd1;
        
        
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pagereference = Page.Fax_page ;
		Test.setCurrentPage ( pagereference ) ;
		
		Test.startTest () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController fc = new ApexPages.StandardController ( ticket ) ;
        
		//  Feed the standard controller to the page controller
		FaxController ffc = new FaxController ( fc ) ;
		
		System.assertNotEquals ( ffc.displayattachment, null ) ;
		
		Test.stopTest () ;
	}
}