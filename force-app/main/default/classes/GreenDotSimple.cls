public class GreenDotSimple 
{
/*
    public class options
    {
        public String name { get ; set ; }
        public String value { get ; set ; }
    }
*/
    public String store_name { get ; set; }
    public String store_address1 { get ; set; }
    public String store_address2 { get ; set; }
    public String store_city { get ; set; }
    public String store_state { get ; set; } 
    public String store_zip { get ; set; }

//    public list<options> options { get ; set ; }
    
    public static GreenDotSimple parse ( String json ) 
    {
        return (GreenDotSimple) System.JSON.deserialize ( json, GreenDotSimple.class ) ;
	}
}