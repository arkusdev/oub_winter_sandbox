@isTest
public with sharing class Test_batch_VM_ALL 
{
	static private void setSettings ( Date d )
	{
		Vendor_Management_Settings__c vms = new Vendor_Management_Settings__c () ;
		
		vms.Name = 'VM_Settings' ;
		vms.Email_CC__c = 'test@test.com' ;
		vms.Email_Override__c = 'test@test.com' ;
		vms.Landing_ID__c = 'abc123' ;
		vms.Monthly_Notifications__c = true ;
		vms.Monthly_Notification_Day__c = d.day () ;
		vms.Monthly_Upcoming_Warnings__c = true ;
		vms.Monthly_Upcoming_Warning_Day__c = d.day () ;
		vms.Status_Notification__c = true ;
		vms.Status_Notification_Day__c = d.day () ;
		vms.Yearly_Reminder_Date__c = d ;
		vms.Yearly_Reminder_Enabled__c = true ;
		vms.Contract_Expiration__c = true ;
		vms.Contract_Notification__c = true ;
		vms.Contract_Notification_Days__c = String.valueOf ( d.day () ) + ',' + String.valueOf ( d.day () + 15 ) ;
		vms.Batch_Exclude_Status__c = 'Closed' ;

		insert vms ;
	}
	
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
	private static User getUser ( String thingy )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		insert u ;
		
		return u ;
	}
	
	private static Vendor_Management__c getVM ( String thingy, User u, Date d )
    {
        return getVM ( thingy, u, d, null ) ;
    }
    
	private static Vendor_Management__c getVM ( String thingy, User u, Date d, String recordTypeID )
	{
		Vendor_Management__c vm = new Vendor_Management__c () ;
		vm.Name = 'Test Vendor ' + thingy ;
		vm.OwnerID = u.Id ;
		vm.Vendor_Status__c = 'Open' ;
        
        if ( recordTypeID != null )
            vm.RecordTypeId = recordTypeID ;
		
		insert vm ;
		
		return vm ;
	}
	
	private static Vendor_Contract__c getVC ( Vendor_Management__c vm, User u, Date d )
	{
		Vendor_Contract__c vc = new Vendor_Contract__c () ;
		vc.Vendor_Management__c = vm.ID ;
		vc.Notification_Date__c = d.addDays ( 1 ) ;
		vc.Expiration_Date__c = d.addDays ( 1 ) ;
		
		insert vc ;
		
		return vc ;
	}
	
	static testmethod void batchContractExpiration ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Date d = Date.today () ;
		setSettings ( d ) ;
		setBatchSettings () ;
		
		User u1 = getUser ( '1' ) ;
		
		Vendor_Management__c vm1 = getVM ( 'A', u1, d ) ;
		Vendor_Contract__c vc1 = getVC ( vm1, u1, d.addDays ( -5 ) ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_VM_Contract_Expiration b = new batch_VM_Contract_Expiration () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}
	
	static testmethod void batchContractNotification ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Date d = Date.today () ;
		setSettings ( d ) ;
		setBatchSettings () ;
		
		User u1 = getUser ( '1' ) ;
		
		Vendor_Management__c vm1 = getVM ( 'A', u1, d ) ;
		Vendor_Contract__c vc1 = getVC ( vm1, u1, d.addDays ( 5 ) ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_VM_Contract_Notification b = new batch_VM_Contract_Notification () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}
	
	static testmethod void batchMonthlyNotification ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Date d = Date.today () ;
		setSettings ( d ) ;
		setBatchSettings () ;
		
		User u1 = getUser ( '1' ) ;
		
		Vendor_Management__c vm1 = getVM ( 'A', u1, d ) ;
		Vendor_Contract__c vc1 = getVC ( vm1, u1, d ) ; 
		Vendor_Contract__c vc2 = getVC ( vm1, u1, d ) ; 
		
		User u2 = getUser ( '2' ) ;
		
		Vendor_Management__c vm2 = getVM ( 'B', u2, d ) ;
		Vendor_Contract__c vc3 = getVC ( vm2, u2, d ) ; 
		Vendor_Contract__c vc4 = getVC ( vm2, u2, d ) ; 
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_VM_Monthly_Notification b = new batch_VM_Monthly_Notification () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}
	
	static testmethod void batchMonthlyExpiration ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Date d = Date.today () ;
		setSettings ( d ) ;
		setBatchSettings () ;
		
		User u1 = getUser ( '1' ) ;
		
		Vendor_Management__c vm1 = getVM ( 'A', u1, d ) ;
		Vendor_Contract__c vc1 = getVC ( vm1, u1, d ) ; 
		Vendor_Contract__c vc2 = getVC ( vm1, u1, d ) ; 
		
		User u2 = getUser ( '2' ) ;
		
		Vendor_Management__c vm2 = getVM ( 'B', u2, d ) ;
		Vendor_Contract__c vc3 = getVC ( vm2, u2, d ) ; 
		Vendor_Contract__c vc4 = getVC ( vm2, u2, d ) ; 
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_VM_Monthly_Expiration b = new batch_VM_Monthly_Expiration () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}

	static testmethod void batchStatusNotification ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
        String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Vendor', 'Vendor_Management__c' ) ;
        
		Date d = Date.today () ;
		setSettings ( d ) ;
		setBatchSettings () ;
		
		User u1 = getUser ( '1' ) ;
		
		Vendor_Management__c vm11 = getVM ( 'A', u1, d ) ;
		Vendor_Management__c vm12 = getVM ( 'B', u1, d, rt ) ;
		Vendor_Contract__c vc11 = getVC ( vm11, u1, d ) ; 
		Vendor_Contract__c vc12 = getVC ( vm11, u1, d ) ; 
        Vendor_Contract__c vc13 = getVC ( vm12, u1, d ) ; 

		User u2 = getUser ( '2' ) ;
		
		Vendor_Management__c vm21 = getVM ( 'B', u2, d ) ;
		Vendor_Management__c vm22 = getVM ( 'B', u2, d, rt ) ;
		Vendor_Contract__c vc21 = getVC ( vm21, u2, d ) ; 
		Vendor_Contract__c vc22 = getVC ( vm21, u2, d ) ;
        Vendor_Contract__c vc23 = getVC ( vm22, u2, d ) ; 

		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_VM_Status_Notification b = new batch_VM_Status_Notification () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}

	static testmethod void batchYearlyReminder ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Date d = Date.today () ;
		setSettings ( d ) ;
		setBatchSettings () ;
		
		User u1 = getUser ( '1' ) ;
		
		Vendor_Management__c vm1 = getVM ( 'A', u1, d ) ;
        
        String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Vendor', 'Vendor_Management__c' ) ;
		Vendor_Management__c vm2 = getVM ( 'B', u1, d, rt ) ;

        //  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_VM_Yearly_Reminder b = new batch_VM_Yearly_Reminder () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}
}