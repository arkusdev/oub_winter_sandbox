@isTest
public class TestoneOpportunityHelper 
{
    // -----------------------------------------------------------------------------------
    // Bleah
    // -----------------------------------------------------------------------------------
    @testSetup public static void testSetup ()
    {
        Advocate_Level__c al = new Advocate_Level__c () ;
        al.Name = 'Non Participant' ;
        insert al ;
    }
    
    // -----------------------------------------------------------------------------------
    // Load and return a account
    // -----------------------------------------------------------------------------------
    private static Account getAccount ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        
        return a ;
    }
    
    private static Account getBusinessAccount ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Busineses' ;
        
        return a ;
    }
    
    // -----------------------------------------------------------------------------------
    // Load and return a branch
    // -----------------------------------------------------------------------------------
    private static Branch__c getBranch ()
    {
        Branch__c b = new Branch__c () ;
        b.Name = 'TEST Branch' ;
        b.On_boarding_Journey__c = false ;
        b.State_CD__c = 'MA' ;
        
        return b ;
    }
    
    // --------------------------------------------------------------------------------
    // Setup, Contact
    // --------------------------------------------------------------------------------
    private static Contact getContact ( Integer i )
    {
        Branch__c b = getBranch () ;
        insert b ;
        
        Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
        c.FirstName = 'Test' + x + 'T' ;
        c.LastName = 'Applicant' + x + 'A' ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
        c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
        
        c.Branch__c = b.ID ;
        
        return c ;
    }
    
    // --------------------------------------------------------------------------------
    // Statics
    // --------------------------------------------------------------------------------
    public static testmethod void ONE ()
    {
        String foo1 = oneOpportunityHelper.getLoanTypeId () ;
        Map<String,Branch__c> foo2 = oneOpportunityHelper.getBranchIDMap () ;
        Map<String,Branch__c> foo3 = oneOpportunityHelper.getBranchNameMap () ;
        Map<String, String> foo4 = oneOpportunityHelper.getGroupMap () ;
        String foo5 = oneOpportunityHelper.getDepositTypeId () ;
    }
    
    // --------------------------------------------------------------------------------
    // Other
    // --------------------------------------------------------------------------------
    public static testmethod void TWO ()
    {
        Account a1 = getAccount () ;
        insert a1 ;
        
        Account a2 = getBusinessAccount () ;
        insert a2 ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Mortgagebot_Web_ID__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        
        insert la ;
        
        Test.startTest () ;
        
        list<String> laL = new list<String> () ;
        laL.add ( la.Mortgagebot_Web_ID__c ) ;
        
        map<String,Loan_Application__c> laM = oneOpportunityHelper.getPossibleLoanAppByID ( laL ) ;
        
        Test.stopTest () ;
    }
}