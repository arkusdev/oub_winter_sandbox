@isTest
public class testBBPaydayPushQueue {
    private static void setCustomSettings() {
        PSP_Settings__c psps = new PSP_Settings__c () ;
        
        psps.Name = 'PSP Settings';
        psps.App_Id__c = 'hereisanappid';
        psps.Base_URL__c  = 'www.fakeyfakeyurl.com/' ;
        psps.Token__c  = 'hereisatoken' ;

        insert psps ;
    }
    
    private static void setup() {
        List<Automated_Email__c> aeList = new List<Automated_Email__c>();
        
        for (Integer i = 0; i < 5; i++) {
            Automated_Email__c ae = new Automated_Email__c();
            ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Push_Notification').RecordTypeId;
            ae.Status__c = 'Processing';
            ae.Email_Sent__c = false;
            
            ae.Contact__c = createContactWithService(i);
            ae.Last_Name__c = ae.Contact__r.LastName;
            ae.First_Name__c = ae.Contact__r.FirstName;
            ae.Email_Address__c = ae.Contact__r.Email;
            ae.Merge_Var_Name_1__c = 'ddAmount';
            ae.Merge_Var_Value_1__c = String.valueOf( 50 * i);
            ae.Merge_Var_Name_2__c = 'payorName';
            ae.Merge_Var_Value_2__c = 'company' + i;
            ae.Merge_Var_Name_3__c = 'userAlias';
            ae.Merge_Var_Value_3__c = 'username' + i;
            ae.Merge_Var_Name_4__c = 'mergeN4';
            ae.Merge_Var_Value_4__c = 'mergeV4-' + i;
            ae.Merge_Var_Name_5__c = 'mergeN5';
            ae.Merge_Var_Value_5__c = 'mergeV5-' + i;
            
            aeList.add(ae);
        }
        
        insert aeList;
    }
    
    private static String createContactWithService(Integer i) {
        Contact c = new Contact();
        c.FirstName = 'first' + i;
        c.LastName = 'last' + i;
        c.Email = 'email' + i + '@abc.com';
        c.GUUID__c = '000054654654654654654654654';
        insert c;
        
        Service__c s = new Service__c();
        s.Status__c = 'Active';
        s.Type__c = 'Online Banking';
        s.Alternate_Login_ID__c = 'username' + i;
        s.Contact__c = c.Id;
        insert s;

        return c.Id;
    }
    
    @isTest
    static void testQueueable() {
        setCustomSettings();
        setup();
        
        String rti = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Push_Notification').RecordTypeId;
        List<Automated_Email__c> aeList = [SELECT Id, Status__c, Email_Sent__c, Contact__c, Last_Name__c, First_Name__c, Email_Address__c,
                                           		Merge_Var_Name_1__c, Merge_Var_Value_1__c, Merge_Var_Name_2__c, Merge_Var_Value_2__c, Merge_Var_Name_3__c, Merge_Var_Value_3__c,
                                           		CreatedDate
                                           FROM Automated_Email__c
                                           WHERE RecordTypeId = :rti];
        System.debug(aeList);
        
        Test.setMock ( HttpCalloutMock.class, new MockHttpPSPHelper() ) ;
        
        Test.startTest();
        
        BBPaydayPushQueue pushQ = new BBPaydayPushQueue(aeList);
        System.enqueueJob(pushQ);
        
        Test.stopTest();
        
        System.assertEquals(5, [SELECT count() FROM Automated_Email__c WHERE Status__c = 'Completed']);
    }
}