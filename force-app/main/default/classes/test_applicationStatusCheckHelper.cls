@isTest
public class test_applicationStatusCheckHelper 
{
    public static void loadRT1ACSData ()
    {
        // Setup
        application_check_status__c acs1 = new application_check_status__c () ;
        acs1.Active__c = true ;
        acs1.Name = 'U Test One' ;
        acs1.Title__c = 'Title' ;
        acs1.Body__c = 'True' ;
        acs1.Application_Status__c = 'APPROVED' ;
        acs1.Application_Processing_Status__c = 'Yes' ;
        acs1.ACH_Funding_Status__c = 'No' ;
        acs1.Order_By__c = 1.0 ;
        acs1.Application_Record_Type__c = 'UNITY_Visa' ;
        
        insert acs1 ;
        
        // Setup
        application_check_status__c acs2 = new application_check_status__c () ;
        acs2.Active__c = true ;
        acs2.Name = 'U Test Two' ;
        acs2.Title__c = 'Title' ;
        acs2.Body__c = 'True' ;
        acs2.Application_Status__c = 'APPROVED' ;
        acs2.Application_Processing_Status__c = 'No' ;
        acs2.ACH_Funding_Status__c = 'ALL' ;
        acs2.Order_By__c = 1.0 ;
        acs2.Application_Record_Type__c = 'UNITY_Visa' ;
        
        insert acs2 ;
        
        // Setup
        application_check_status__c acs3 = new application_check_status__c () ;
        acs3.Active__c = true ;
        acs3.Name = 'U Test Three' ;
        acs3.Title__c = 'Title' ;
        acs3.Body__c = 'True' ;
        acs3.Application_Status__c = 'APPROVED' ;
        acs3.Application_Processing_Status__c = 'ALL' ;
        acs3.ACH_Funding_Status__c = 'Maybe' ;
        acs3.Order_By__c = 1.0 ;
        acs3.Application_Record_Type__c = 'UNITY_Visa' ;
        
        insert acs3 ;
        
        // Setup
        application_check_status__c acs4 = new application_check_status__c () ;
        acs4.Active__c = true ;
        acs4.Name = 'U Test Four' ;
        acs4.Title__c = 'Title' ;
        acs4.Body__c = 'True' ;
        acs4.Application_Status__c = 'APPROVED' ;
        acs4.Application_Processing_Status__c = 'ALL' ;
        acs4.ACH_Funding_Status__c = 'ALL' ;
        acs4.Order_By__c = 1.0 ;
        acs4.Application_Record_Type__c = 'UNITY_Visa' ;
        
        insert acs4 ;
        
        // Setup
        application_check_status__c acs5 = new application_check_status__c () ;
        acs5.Active__c = true ;
        acs5.Name = 'U Test Five' ;
        acs5.Title__c = 'Title' ;
        acs5.Body__c = 'True' ;
        acs5.Application_Status__c = 'ALL' ;
        acs5.Application_Processing_Status__c = 'ALL' ;
        acs5.ACH_Funding_Status__c = 'ALL' ;
        acs5.Order_By__c = 1.0 ;
        acs5.Application_Record_Type__c = 'UNITY_Visa' ;
        
        insert acs5 ;
    }
    
    public static void loadRT2ACSData ()
    {
        // Setup
        application_check_status__c acs1 = new application_check_status__c () ;
        acs1.Active__c = true ;
        acs1.Name = 'D Test One' ;
        acs1.Title__c = 'Title' ;
        acs1.Body__c = 'True' ;
        acs1.Application_Status__c = 'APPROVED' ;
        acs1.Application_Processing_Status__c = 'Yes' ;
        acs1.ACH_Funding_Status__c = 'No' ;
        acs1.Order_By__c = 1.0 ;
        acs1.Application_Record_Type__c = 'Deposits' ;
        
        insert acs1 ;
        
        // Setup
        application_check_status__c acs2 = new application_check_status__c () ;
        acs2.Active__c = true ;
        acs2.Name = 'D Test Two' ;
        acs2.Title__c = 'Title' ;
        acs2.Body__c = 'True' ;
        acs2.Application_Status__c = 'APPROVED' ;
        acs2.Application_Processing_Status__c = 'No' ;
        acs2.ACH_Funding_Status__c = 'ALL' ;
        acs2.Order_By__c = 1.0 ;
        acs2.Application_Record_Type__c = 'Deposits' ;
        
        insert acs2 ;
        
        // Setup
        application_check_status__c acs3 = new application_check_status__c () ;
        acs3.Active__c = true ;
        acs3.Name = 'D Test Three' ;
        acs3.Title__c = 'Title' ;
        acs3.Body__c = 'True' ;
        acs3.Application_Status__c = 'APPROVED' ;
        acs3.Application_Processing_Status__c = 'ALL' ;
        acs3.ACH_Funding_Status__c = 'Maybe' ;
        acs3.Order_By__c = 1.0 ;
        acs3.Application_Record_Type__c = 'Deposits' ;
        
        insert acs3 ;
        
        // Setup
        application_check_status__c acs4 = new application_check_status__c () ;
        acs4.Active__c = true ;
        acs4.Name = 'D Test Four' ;
        acs4.Title__c = 'Title' ;
        acs4.Body__c = 'True' ;
        acs4.Application_Status__c = 'APPROVED' ;
        acs4.Application_Processing_Status__c = 'ALL' ;
        acs4.ACH_Funding_Status__c = 'ALL' ;
        acs4.Order_By__c = 1.0 ;
        acs4.Application_Record_Type__c = 'Deposits' ;
        
        insert acs4 ;
        
        // Setup
        application_check_status__c acs5 = new application_check_status__c () ;
        acs5.Active__c = true ;
        acs5.Name = 'D Test Five' ;
        acs5.Title__c = 'Title' ;
        acs5.Body__c = 'True' ;
        acs5.Application_Status__c = 'ALL' ;
        acs5.Application_Processing_Status__c = 'ALL' ;
        acs5.ACH_Funding_Status__c = 'ALL' ;
        acs5.Order_By__c = 1.0 ;
        acs5.Application_Record_Type__c = 'Deposits' ;
        
        insert acs5 ;
    }
    
	public static testmethod void testOne ()
    {
        loadRT1ACSData () ;
        loadRT2ACSData () ;
        
        Test.startTest () ;
        
        applicationCheckStatusHelper acsh = new applicationCheckStatusHelper () ;
        
        application_check_status__c acs ;
        
        acs = acsh.getStatus ( 'UNITY_Visa', 'APPROVED', 'Yes', 'No' ) ;
        System.assertEquals ( acs.Name, 'U Test One' ) ;
        
        acs = acsh.getStatus ( 'Deposits', 'APPROVED', 'Yes', 'No' ) ;
        System.assertEquals ( acs.Name, 'D Test One' ) ;
        
        acs = acsh.getStatus ( 'UNITY_Visa', 'APPROVED', 'No', 'I hate you' ) ;
        System.assertEquals ( acs.Name, 'U Test Two' ) ;
        
        acs = acsh.getStatus ( 'Deposits', 'APPROVED', 'No', 'I hate you' ) ;
        System.assertEquals ( acs.Name, 'D Test Two' ) ;
        
        acs = acsh.getStatus ( 'UNITY_Visa', 'APPROVED', 'No I dont', 'Maybe' ) ;
        System.assertEquals ( acs.Name, 'U Test Three' ) ;
        
        acs = acsh.getStatus ( 'Deposits', 'APPROVED', 'No I dont', 'Maybe' ) ;
        System.assertEquals ( acs.Name, 'D Test Three' ) ;
        
        acs = acsh.getStatus ( 'UNITY_Visa', 'APPROVED', 'What', 'The' ) ;
        System.assertEquals ( acs.Name, 'U Test Four' ) ;
        
        acs = acsh.getStatus ( 'Deposits', 'APPROVED', 'What', 'The' ) ;
        System.assertEquals ( acs.Name, 'D Test Four' ) ;
        
        acs = acsh.getStatus ( 'UNITY_Visa', null, null, null ) ;
        System.assertEquals ( acs.Name, 'U Test Five' ) ;
        
        acs = acsh.getStatus ( 'Deposits', null, null, null ) ;
        System.assertEquals ( acs.Name, 'D Test Five' ) ;
        
        acs = acsh.getStatus ( null, null, null ) ;
        System.assertEquals ( acs.Name, 'U Test Five' ) ;
        
        Test.stopTest () ;
    }
}