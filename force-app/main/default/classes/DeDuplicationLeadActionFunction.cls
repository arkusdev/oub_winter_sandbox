global class DeDuplicationLeadActionFunction 
{
	private static final String DELIMITER = '~' ;
    private static final Integer DEDUPE_MERGE_MAX = 2 ;

  	private static boolean debugCRAP = false ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void dedupeLead ( List<ID> xL )
	{
		Lead emptyL = new Lead () ;
		List<String> emptyLeadFields = DynamicObjectHandler.getUpdateableFields ( emptyL ) ;
		
		String lLs = DynamicObjectHandler.joinListQuoted ( xL, ',' ) ;
		
		String l1 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyL, 'ID IN ( ' + lLs + ' ) ' , 'ID' ) ;
		
        if ( debugCRAP ) 
            System.debug ( 'L1: ' + l1 ) ;
		
		list<String> sEL = new list<String> () ;
		list<String> sPL = new list<String> () ;
		list<Lead> lL = Database.query ( l1 ) ;
		
		map<String,Lead> lEMap = new Map<String,Lead> () ;
		
		System.debug ( 'Lead list size: ' + lL.size () ) ;
        
		if ( ( lL != null ) && ( lL.size () > 0 ) )
		{
			for ( Lead l : lL )
			{
				if ( String.isNotBlank ( getKeyByEmail ( l ) ) )
				{
					lEMap.put ( getKeyByEmail ( l ), l ) ;
					sEL.add ( l.Email ) ;
				}
				
				if ( String.isNotBlank ( getKeyByPhone ( l ) ) )
				{
					lEMap.put ( getKeyByPhone ( l ), l ) ;
					sPL.add ( l.Phone ) ;
				}
			}
            
			String oELs = null ;
			String oPLs = null ;
			
			//  SETUP
			String thingy = 'IsConverted = FALSE AND ' ;
			boolean foundData = false ;
			
			if ( ( sEL.size () > 0 ) && ( sPL.size () > 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += '( Email IN ( ' + oELs + ' ) OR Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () > 0 ) && ( sPL.size () == 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				
				thingy += '( Email IN ( ' + oELs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () == 0 ) && ( sPL.size () > 0 ) )
			{
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += '( Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			
			map<ID,Lead> mapToUpdate = new map<ID,Lead> () ;
			map<ID,Lead> masterMap = new map<ID,Lead> () ;
			map<ID,List<Lead>> duplicateMap = new map<ID,List<Lead>> () ;
            
			if ( foundData == true )
			{
				
				String l2 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyL, thingy, 'ID' ) ;
		
          		if ( debugCRAP )
					System.debug ( 'L2: ' + l2 ) ;
				
				list<Lead> oL = Database.query ( l2 ) ;
				
				if ( ( oL != null ) && ( oL.size () > 0 ) )
				{
					for ( Lead l : oL )
					{
						boolean found = false ;
						
						System.debug ( 'Checking for :: ' + getKeyByEmail ( l ) ) ;
						
						if ( lEMap.containsKey ( getKeyByEmail ( l ) ) )
						{
							System.debug ( 'Email Key FOUND!' ) ;
							
							Lead lc = lEMap.get ( getKeyByEmail ( l ) ) ;
							
							if ( lc.ID != l.ID )
							{
								for ( String s : emptyLeadFields )
								{
									if ( ( lc.get ( s ) == null ) && ( l.get ( s ) != null ) )
									{
									 	System.debug ( 'Setting "' + s + '", found difference' ) ;
									 	
									 	found = true ;
									 	lc.put ( s, l.get ( s ) ) ;
									}
								}
								
								if ( found == true )
								{
									if ( ! mapToUpdate.containsKey ( lc.ID ) )
										mapToUpdate.put ( lc.ID, lc ) ;
								}
								
								if ( ! masterMap.containsKey ( lc.ID ) )
									masterMap.put ( lc.ID, lc ) ;
								
								if ( ! duplicateMap.containsKey ( lc.ID ) )
									duplicateMap.put ( lc.ID, new list<Lead> () ) ;	
			
								duplicateMap.get ( lc.ID ).add ( l ) ;
							}
							else
							{
								System.debug ( 'Duplicate Master ID found, skipping' ) ;
							}
						}
						else
						{
							System.debug ( 'Email Key Not Found, checking phone!' ) ;
							
							System.debug ( 'Checking for :: ' + getKeyByPhone ( l ) ) ;
						
							if ( lEMap.containsKey ( getKeyByPhone ( l ) ) )
							{
								System.debug ( 'Phone Key FOUND!' ) ;
								
								Lead lc = lEMap.get ( getKeyByPhone ( l ) ) ;
								
								if ( lc.ID != l.ID )
								{
									for ( String s : emptyLeadFields )
									{
										if ( ( lc.get ( s ) == null ) && ( l.get ( s ) != null ) )
										{
										 	System.debug ( 'Setting "' + s + '", found difference' ) ;
										 	
										 	found = true ;
										 	lc.put ( s, l.get ( s ) ) ;
										}
									}
									
									if ( found == true )
									{
										if ( ! mapToUpdate.containsKey ( lc.ID ) )
											mapToUpdate.put ( lc.ID, lc ) ;
									}
									
									if ( ! masterMap.containsKey ( lc.ID ) )
										masterMap.put ( lc.ID, lc ) ;
									
									if ( ! duplicateMap.containsKey ( lc.ID ) )
										duplicateMap.put ( lc.ID, new list<Lead> () ) ;	
				
									duplicateMap.get ( lc.ID ).add ( l ) ;
								}
								else
								{
									System.debug ( 'Duplicate Master ID found, skipping' ) ;
								}
							}
							else
							{
								System.debug ( 'Key Not Found!' ) ;
							}
						}
					}
				}
				else
				{
					System.debug ( 'No leads found to merge!' ) ;
				}
			}
			else
			{
				System.debug ( 'No valid emails or phone numbers found to merge on!' ) ;
			}
	
			if ( mapToUpdate.keyset ().size () > 0 )
			{
				list<Lead> listToUpdate = new list<Lead> () ;
				
				for ( String xID : mapToUpdate.keyset () )
					listToUpdate.add ( mapToUpdate.get ( xID ) ) ;
				
				try
				{
					System.debug ( 'Updating ' + listToUpdate.size () + ' Lead records' ) ;
				
					update listToUpdate ;
				}
				catch ( DMLException e )
				{
					System.debug ( 'DML : ' + e.getMessage () ) ;
				}
			}
				
			if ( masterMap.keyset ().size () > 0 )
			{
				System.debug ( 'Merging into ' + masterMap.keyset ().size () + ' lead records' ) ;
				
				for ( ID x : masterMap.keyset () )
				{
                    list<Lead> dcL = duplicateMap.get ( x ) ;
                    
			        List<Lead> thingyL = new List<Lead> () ;
                    for ( Lead l : dcL )
                    {
                        thingyL.add ( l ) ;
		                if ( thingyL.size () == DEDUPE_MERGE_MAX )
                        {
                            System.debug ( 'MERGE MAX of ' + DEDUPE_MERGE_MAX + ', running' ) ;
                            Database.MergeResult[] results = Database.merge ( masterMap.get ( x ), thingyL, false ) ;
                            
                            for ( Database.MergeResult res : results ) 
                            {
                                if ( res.isSuccess () ) 
                                {
                                    System.debug ( 'MERGE SUCCESS!' ) ;
                                }
                                else
                                {
                                    for ( Database.Error err : res.getErrors () )
                                        System.debug ( err.getMessage () );
                                }
                            }
                            
                            thingyL = new list<Lead> () ;
                        }
                    }
                    
                    if ( thingyL.size () > 0 )
                    {
                        System.debug ( 'MERGE REMAINDER, running' ) ;
                        Database.MergeResult[] results = Database.merge ( masterMap.get ( x ), thingyL, false ) ;
                        
                        for ( Database.MergeResult res : results ) 
                        {
                            if ( res.isSuccess () ) 
                            {
                                System.debug ( 'MERGE SUCCESS!' ) ;
                            }
                            else
                            {
                                for ( Database.Error err : res.getErrors () )
                                    System.debug ( err.getMessage () );
                            }
                        }
                    }
				}
			}
		}
		else
		{
			System.debug ( 'No leads found to merge' ) ;
		}
    }
    
	//  Key mapping from lead
	private static String getKeyByEmail ( Lead l )
	{
		String x = '' ;
		
		if ( String.isNotBlank ( l.Email ) )
		{
			x = l.Email.toUpperCase () ;
		}
		
		return x ;
	}
	
	//  Key mapping from lead
	private static String getKeyByPhone ( Lead l )
	{
		String x = '' ;
		
		if ( ( String.isNotBlank ( l.Phone ) ) &&
		     ( String.isNotBlank ( l.FirstName ) ) &&
		     ( String.isNotBlank ( l.LastName ) ) )
		{
			x = l.Phone.toUpperCase () + DELIMITER + l.FirstName.toUpperCase () + DELIMITER + l.LastName.toUpperCase () ;
		}
		
		return x ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='DeDuplicate Leads' )
	global static List<DeDuplicationLeadActionFunctionResult> deduplicateLead ( List<DeDuplicationLeadActionFunctionRequest> requests ) 
	{
		System.debug ( '------------------------- DEDUPE STARTING ------------------------------' ) ;
		
		//  Not used?
		list<DeDuplicationLeadActionFunctionResult> rL = new list<DeDuplicationLeadActionFunctionResult> () ;
		
		if ( ( requests != null ) && ( requests.size () > 0 ) )
		{
			//  Get IDs out of Object
			list<ID> incL = new List<ID> () ;
			
			for ( DeDuplicationLeadActionFunctionRequest d : requests )
			{
                incL.add ( d.xID ) ;
			}
			
            dedupeLead ( incL ) ;
		}
		
		System.debug ( '------------------------- DEDUPE ENDING ------------------------------' ) ;
		return rL ;
	}
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class DeDuplicationLeadActionFunctionRequest
	{
	    @InvocableVariable ( label='Lead ID' required=true )
	    public ID xID ;
	}
	
	global class DeDuplicationLeadActionFunctionResult
	{
	    @InvocableVariable
	    public ID xID ;
	}
}