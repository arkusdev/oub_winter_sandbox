public with sharing class Case_Classification_Controller 
{
    //  ----------------------------------------------------------------------------------------
    //  Security
    //  ----------------------------------------------------------------------------------------
	private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;

    //  ----------------------------------------------------------------------------------------
    //  Page objects
    //  ----------------------------------------------------------------------------------------
 	public Case c 
 	{ 
 		get; 
 		private set; 
 	}
 	
	//  Step control for panels on page
	public Integer cStepNumber
	{
		get ;
		private set ;
	}
    
    public boolean askForDetails
    {
        get ; 
        private set ;
    }
    
    public String firstName 
    { 
    	get ; 
        set ;
    }
    
    public boolean errorFirstName
    {
        get ;
        private set ;
    }

    public String lastName 
    { 
    	get ;
        set ;
    }

    public boolean errorLastName
    {
        get ;
        private set ;
    }

    public String phoneNumber 
    { 
    	get ; 
        set ;
    }

    public boolean errorPhone
    {
        get ;
        private set ;
    }

    public String emailAddress
    { 
    	get ; 
        set ;
    }

    public boolean errorEmail
    {
        get ;
        private set ;
    }

	//  List for document type drop down
    public List<SelectOption> caseClassificationDropDown
    {
        get
        {
            return Case_Classification_Helper.getCaseClassificationList () ;
        }
        
		private set ;
    }
    
    public String selectedOption
    {
        get ;
        set ;
    }
    
    public String message
    {
        get ;
        private set ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Main Controller
    //  ----------------------------------------------------------------------------------------
    public Case_Classification_Controller ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		cStepNumber = 0 ;
        
        //  Extra details?
        askForDetails = false ;
        
        //  First check for application in session
        c = (Case) stdC.getRecord () ;
        
        String cid = null ;
        
 		//  If no session present, use querystring instead
        if ( ( c == null ) || ( c.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			cid = ApexPages.currentPage().getParameters().get('cid') ;  // Can't use ID, salesforce picks it up internally!
            
			c = null ;

			//  Both need to be passed in
			if ( cid != null )
			{
                list<Case> cL = null ;
                
                try
                {
                    cL = [
                        SELECT Id, CaseNumber, Case_Topic__c, Status, Case_Topic__r.Classification__c, ContactID, Lead__c, SuppliedName, SuppliedEmail, SuppliedPhone
                        FROM Case 
                        WHERE RecordType.Name IN ( 'Email Correspondence', 'Web Correspondence', 'Phone Support' )
                        AND Id = :cid 
                    ] ;
                }
                catch ( DMLException e )
                {
                    cL = null ;
                }
                
                if ( ( cL != null ) && ( cL.size () > 0 ) ) 
                    c = cL [ 0 ] ;
			}
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			cid = c.Id ;
		}
        
		//  Check again for things existing
		if ( ( c != null ) && ( cid != null ) )
		{
            System.debug ( '========== Valid case found ==========' ) ;
            
            System.debug ( '========== Checking for details ==========' ) ;
            if ( ( c.ContactID == null ) && ( c.Lead__c == null ) )
                askForDetails = true ;
            
            System.debug ( '========== Topic setup ==========' ) ;
            if ( String.IsBlank ( c.Case_Topic__c ) )
            {
                cStepNumber = 1000 ;
            }
            else
            {
                Case_Topic__c ccMDT = Case_Classification_Helper.getClassificationInfo ( c.Case_Topic__r.Classification__c ) ;
                
                if ( ccMDT != null )
                {
                    if ( String.isNotBlank ( ccMDT.Message__c ) )
                    {
                        cStepNumber = 1001 ;
                
                        message = ccMDT.Message__c ;
                		selectedOption = c.Case_Topic__r.Classification__c;
                        
                    }
                    else
                    {
                        cStepNumber = 1200 ;
                		selectedOption = c.Case_Topic__r.Classification__c;
                    }
                }
                else
                {
                    cStepNumber = 1000 ;
                }
                
                System.debug ( 'STEP :: ' + cStepNumber ) ;
            }
        }
        else
        {
            cStepNumber = 3000 ;
        }
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Validate the dropdown
    //  ----------------------------------------------------------------------------------------
	public PageReference validateClassificationChoice () 
	{
        errorFirstName = false ;
        errorLastName = false ;
        errorEmail = false ;
        errorPhone = false ;
        
        if ( askForDetails == true )
        {
            String fullName = '' ;
            
            if ( String.isBlank ( firstName ) )
                errorFirstName = true ;
            else
                fullName = firstName ;
            
            if ( String.isBlank ( lastName ) )
                errorLastName = true ;
            else
                fullName += lastName ;
            
            if ( String.isNotBlank ( fullName ) )
            {
                if ( String.isBlank ( c.SuppliedName ) )
                    c.SuppliedName = fullName ;
                else if ( ! c.SuppliedName.equalsIgnoreCase ( fullName ) )
                        c.Additional_Name__c = fullName ;
            }
            
            if ( String.isBlank ( emailAddress ) || ( ! OneUnitedUtilities.isValidEmail ( emailAddress ) ) )
            {
                errorEmail = true ;
            }
            else
            {
                if ( String.isBlank ( c.SuppliedEmail ) )
                    c.SuppliedEmail = emailAddress ;
                else if ( ! c.SuppliedEmail.equalsIgnoreCase ( emailAddress ) )
                    c.Additional_Email__c = emailAddress ;
            }
            
            if ( String.isBlank ( phoneNumber ) || ( ! OneUnitedUtilities.isValidPhoneNumber ( phoneNumber ) ) )
            {
                errorPhone = true ;
            }
            else
            {
                if ( String.isBlank ( c.SuppliedPhone ) )
                    c.SuppliedPhone = phoneNumber ;
                else if ( ! c.SuppliedPhone.equalsIgnoreCase ( phoneNumber ) )
                    c.Additional_Phone__c = phoneNumber ;
            }
        }
        
        if ( ( ! errorFirstName ) && ( ! errorLastName ) && ( ! errorEmail ) && ( ! errorPhone ) && ( askForDetails ) )
        {
            askforDetails = false ;
            
            ID xID = Case_Classification_Helper.getContact ( firstName, lastName, emailAddress ) ;
            
            if ( xID != null )
                c.ContactId = xID ;
        }
        
        Case_Topic__c ccMDT = Case_Classification_Helper.getClassificationInfo ( selectedOption ) ;
        
        if ( ccMDT != null )
        {
            if ( String.isNotBlank ( ccMDT.Message__c ) )
            {
		        cStepNumber = 1001 ;
        
                message = ccMDT.Message__c ;
                
                c.Case_Topic__c = ccMDT.ID ;
                c.Status = ccMDT.Status__c ;
                
                access.updateObject ( c ) ;
            }
            else
            {
		        cStepNumber = 1002 ;
                
                message = 'Thank you for your feedback.' ;
                
                c.Case_Topic__c = ccMDT.ID ;
                c.Status = ccMDT.Status__c ;
                
                access.updateObject ( c ) ;
            }
        }
        else
        {
            c.Case_Topic__c = null ;
            c.Status = 'Waiting on Customer' ;
            access.updateObject ( c ) ;
            cStepNumber = 1000 ;
        }
        
        System.debug ( 'STEP :: ' + cStepNumber ) ;
        return null ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  YAY
    //  ----------------------------------------------------------------------------------------
    public PageReference goYES ()
    {
        cStepNumber = 1100 ;
        
        c.Status = 'Closed' ;
        
        access.updateObject ( c ) ;
        
        return null ;
    }

    //  ----------------------------------------------------------------------------------------
    //  NEIGH
    //  ----------------------------------------------------------------------------------------
    public PageReference goNO ()
    {
        cStepNumber = 1200 ;
        
        c.Status = 'Needs Agent' ;
        
        access.updateObject ( c ) ;
        
        return null ;
    }
}