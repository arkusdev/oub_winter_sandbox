@isTest
public class Test_batch_Application_SWIM_Refund 
{
	/* --------------------------------------------------------------------------------
	 * Custom settings
	 * -------------------------------------------------------------------------------- */
	private static Batch_Process_Settings__c insertBatchCustomSetting ()
    {
        Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;

        bps.Name = 'OneUnited_Batch' ;
        bps.ACH_GL_Account__c = '1001000100' ;
        bps.ACH_Outgoing_Account__c = '1001000101' ;
        bps.Application_Cashbox__c = '666' ;
        bps.Batch_Email__c = 'lmattera@oneunited.com' ;
        bps.Collateral_GL_Account__c = '1001000102' ;
        bps.Deposit_ePay_GL_Account__c = '1001000103' ;
        bps.Email_Enabled__c = false ;
        bps.Routing_Number__c = '01100127' ;
        bps.UNITY_Visa_ePay_GL_Account__c = '1001000104' ;
        
        insert bps ;
        
        return bps ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Setup, application
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, String fundingSource )
	{
		one_application__c app = new one_application__c ();
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = '666-01-1111' ;
        app.Phone__c = c1.Phone ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
		
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = '666-02-1111' ;
            app.Co_Applicant_Phone__c = c2.Phone ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

        app.Funding_Account_Type__c = fundingSource ;
        if ( fundingSource.equalsIgnoreCase ( 'E- Check (ACH)' ) )
        {
            app.Funding_Account_Number__c = '1101100110' ;
            app.Funding_Account_Type__c = 'Checking' ;
            app.Funding_Routing_Number__c = '011001001' ;
        }
        
		app.Funding_Options__c = fundingSource ;
        
		app.Gross_Income_Monthly__c = 10000.00 ;

        Decimal amount = Math.random () * 1000 + 250 ;
		app.FIS_Application_ID__c = '111222333' ;
		app.FIS_Decision_Code__c = 'A001' ;
        app.RequestedCreditLimit__c = amount ;
        app.FIS_Credit_Limit__c = amount ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
		
		return app ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Setup, application transaction
	 * -------------------------------------------------------------------------------- */
    private static Application_Transaction__c getApplicationTransaction ( one_application__c o, Ticket__c t )
    {
        Application_Transaction__c at = new Application_Transaction__c () ;
        at.Application__c = o.ID ;
        at.Status__c = 'Settled' ;
        at.SWIM_Settlement_Ticket__c = t.ID ;
        
        return at ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, financial account
	 * -------------------------------------------------------------------------------- */
    private static Financial_Account__c getFinancialAccount ( Contact c, String rt, Decimal a )
    {
        Financial_Account__c fa = new Financial_Account__c () ;
        
		fa.recordTypeId = rt ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c.Id ;
		fa.CREDITLIMITAMT__c = a ;
		
        return fa ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, ticket
	 * -------------------------------------------------------------------------------- */
    private static Ticket__c getRefundTicket ( Financial_Account__c fa, one_Application__c o, String source )
    {
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		Map<String,String> groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Approved for Refund' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		t.Application__c = o.Id ;
		t.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		t.OwnerId = groupId ;
		t.Application_Refund_Source__c = source ;
		t.Card_Blocked__c = true ;
        
        return t ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Test, nothing
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testNothing ()
    {
        insertBatchCustomSetting () ;
        
        Test.startTest () ;
        
        batch_Application_SWIM_Refund b = new batch_Application_SWIM_Refund () ;
        Database.executeBatch ( b ) ;
        
        Test.stopTest () ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Test, ACH, Holding
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testACHHolding ()
    {
        insertBatchCustomSetting () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, 'E- Check (ACH)' ) ;
        insert app1 ;
        
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
        Financial_Account__c f1 = getFinancialAccount ( c1, creditCardRecordTypeId, app1.FIS_Credit_Limit__c ) ;
        insert f1 ;
        
		Ticket__c t1 = getRefundTicket ( f1, app1, 'Holding' ) ;
		insert t1 ;
		        
        Test.startTest () ;
        
        batch_Application_SWIM_Refund b = new batch_Application_SWIM_Refund () ;
        Database.executeBatch ( b ) ;
        
        Test.stopTest () ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Test, ACH, Collateral
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testACHCollateral ()
    {
        insertBatchCustomSetting () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, 'E- Check (ACH)' ) ;
        insert app1 ;
        
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
        Financial_Account__c f1 = getFinancialAccount ( c1, creditCardRecordTypeId, app1.FIS_Credit_Limit__c ) ;
        insert f1 ;
        
		Ticket__c t1 = getRefundTicket ( f1, app1, 'Collateral' ) ;
		insert t1 ;
		        
        Test.startTest () ;
        
        batch_Application_SWIM_Refund b = new batch_Application_SWIM_Refund () ;
        Database.executeBatch ( b ) ;
        
        Test.stopTest () ;
	}

	/* --------------------------------------------------------------------------------
	 * Test, Debit Card
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testDebitCard ()
    {
        insertBatchCustomSetting () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, 'Debit Card' ) ;
        insert app1 ;
        
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
        Financial_Account__c f1 = getFinancialAccount ( c1, creditCardRecordTypeId, app1.FIS_Credit_Limit__c ) ;
        insert f1 ;
        
		Ticket__c t1 = getRefundTicket ( f1, app1, 'Debit Card' ) ;
		insert t1 ;
        
        Application_Transaction__c at1 = getApplicationTransaction ( app1, t1 ) ;
        insert at1 ;
		        
        Test.startTest () ;
        
        batch_Application_SWIM_Refund b = new batch_Application_SWIM_Refund () ;
        Database.executeBatch ( b ) ;
        
        Test.stopTest () ;
        
        Application_Transaction__c at2 = [ SELECT ID, Refund_Ticket__c FROM Application_Transaction__c WHERE ID = :at1.ID ] ;
        System.assertEquals ( at2.Refund_Ticket__c, t1.ID ) ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Test, Combined
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testCombined ()
    {
        insertBatchCustomSetting () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, 'Debit Card' ) ;
        insert app1 ;
        
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
        Financial_Account__c f1 = getFinancialAccount ( c1, creditCardRecordTypeId, app1.FIS_Credit_Limit__c ) ;
        insert f1 ;
        
		Ticket__c t1 = getRefundTicket ( f1, app1, 'Debit Card' ) ;
		insert t1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, 'E- Check (ACH)' ) ;
        insert app2 ;
        
        Financial_Account__c f2 = getFinancialAccount ( c2, creditCardRecordTypeId, app2.FIS_Credit_Limit__c ) ;
        insert f2 ;
        
		Ticket__c t2 = getRefundTicket ( f2, app2, 'Collateral' ) ;
		insert t2 ;
		        
        Test.startTest () ;
        
        batch_Application_SWIM_Refund b = new batch_Application_SWIM_Refund () ;
        Database.executeBatch ( b ) ;
        
        Test.stopTest () ;
	}
}