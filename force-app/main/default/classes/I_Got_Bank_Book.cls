public with sharing class I_Got_Bank_Book 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security
     *  ---------------------------------------------------------------------------------------- */
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Settings
     *  ---------------------------------------------------------------------------------------- */
    private Web_Settings__c ws ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Display objects
     *  ---------------------------------------------------------------------------------------- */
    public String iGotBankBanner { public get ; private set ; }

    public Date contestEndDate { public get ; private set ; }
    public String contestEndDateFormat { public get ; private set ; }

    private Date contestWinnerDate { private get ; private set ; }
    public String contestWinnerDateFormat { public get ; private set ; }
    
    public String contestWinnerNumber { public get ; private set ; }
    public String contestWinnerAmount { public get ; private set ; }
    
	//  Step control for panels on page
	public Integer dStepNumber { get ; private set ; }

    /*  ----------------------------------------------------------------------------------------
     *  Inputs
     *  ---------------------------------------------------------------------------------------- */
    public String firstName { get ; set ; }
    public String lastName { get ; set ; }
    public String email { get ; set ; }
    public String mobilePhone { get ; set ; }
    
	//  Error checking
	public Map<String,String> errorMap { get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public I_Got_Bank_Book ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 2000 - No Contest!
		dStepNumber = 2000 ;
        
        //  Load settings
        ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;
        
        //  Active contest?
        if ( ws != null )
        {
            contestEndDate = ws.I_Got_Bank_Contest_End_Date__c ;
            
            if ( ( contestEndDate != null ) && ( contestEndDate >= Date.today () ) )
                dStepNumber = 1000 ;
            
            if ( ws.I_Got_Bank_Banner__c != null )
                iGotBankBanner = ws.I_Got_Bank_Banner__c ;
            
            if ( ws.I_Got_Bank_Contest_Winner_Date__c != null )
                contestWinnerDate = ws.I_Got_Bank_Contest_Winner_Date__c ;
            else
                contestWinnerDate = contestEndDate.addDays ( 90 ) ;
            
            if ( contestWinnerDate <= contestEndDate )
                contestWinnerDate = contestEndDate.addDays ( 90 ) ;
            
            Datetime dte = contestEndDate.addDays ( 1 ) ;
            contestEndDateFormat = dte.format ( 'M/d/yyyy' ) ;
            
            Datetime dtw = contestWinnerDate.addDays ( 1 ) ;
            contestWinnerDateFormat = dtw.format ( 'M/d/yyyy' ) ;
            
            if ( String.isNotBlank ( ws.I_Got_Bank_Contest_Winner_Number__c ) )
                contestWinnerNumber = ws.I_Got_Bank_Contest_Winner_Number__c ;
            else
                contestWinnerNumber = 'Three (3)' ;
            
            if ( String.isNotBlank ( ws.I_Got_Bank_Contest_Winner_Award__c ) )
                contestWinnerAmount = ws.I_Got_Bank_Contest_Winner_Award__c ;
            else
                contestWinnerAmount = '$1,000' ;
        }
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Validate entry
     *  ---------------------------------------------------------------------------------------- */
	public PageReference submitInformation () 
	{
		//  Check for validation, setup
		boolean isValid = true ;
		errorMap = new Map<String, String> () ;
        
		//  Validation
		//  We dump blanks in if no error found so the page doesn't crash and burn

        //  -------------------- PARENT --------------------
		if ( String.isEmpty ( firstName ) )
			errorMap.put ( 'First_Name__c' , 'Please your first name' ) ;
		else
			errorMap.put ( 'First_Name__c', '' ) ;
		
		if ( String.isEmpty ( lastName ) )
			errorMap.put ( 'Last_Name__c' , 'Please enter your last name' ) ;
		else
			errorMap.put ( 'Last_Name__c', '' ) ;
		
		if ( String.isEmpty ( email ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter your email address' ) ;
        else if ( ! OneUnitedUtilities.isValidEmail ( email ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter a valid email address' ) ;
		else
			errorMap.put ( 'Email_Address__c', '' ) ;
		
		if ( String.isEmpty ( mobilePhone ) )
			errorMap.put ( 'Phone__c' , 'Please enter a mobile phone' ) ;
        else if ( String.isEmpty ( one_utils.formatPhone ( mobilePhone ) ) )
			errorMap.put ( 'Phone__c' , 'Please enter a mobile phone' ) ;
		else
        {
			errorMap.put ( 'Phone__c', '' ) ;
            mobilePhone = one_utils.formatPhone ( mobilePhone ) ;
        }
        
		//  Non blank entry means an error occurred
		for ( String key : errorMap.keySet () )
		{
			if ( String.isNotEmpty ( errorMap.get ( key ) ) )
				isValid = false ;
		}

        if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        dStepNumber = 1001 ;
		}
        else
        {
            dStepNumber = 1100 ;
            
            System.debug ( '========== Checking for existing contact ==========' ) ;
            Contact c = OneUnitedUtilities.getContactByNameAndEmail ( firstName, lastName, email ) ;
            
            if ( c == null )
            {
            	System.debug ( '========== Contact not found, creating ==========' ) ;
                c = new Contact () ;
                
                c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
                
                c.FirstName = firstName ;
                c.LastName = lastName ;
                c.Email = email ;
                c.MobilePhone = mobilePhone ;
                c.Phone = c.MobilePhone ;
                c.LeadSource = 'I Got Bank' ;
                c.I_Got_Bank_Book_Request__c = System.now () ;
                
                Account a = OneUnitedUtilities.getDefaultAccount () ;
                if ( a != null )
                	c.AccountId = a.ID ;
                
                try
                {
                	access.insertObject ( c ) ;
                }
                catch ( Exception e )
                {
                    // ?
                }
            }
            else
            {
                c.I_Got_Bank_Book_Request__c = System.now () ;
                
                try
                {
                	access.updateObject ( c ) ;
                }
                catch ( Exception e )
                {
                    // ?
                }
            }
            
            //  Automated email ?
            if ( c != null )
            {
                Automated_Email__c ae = new Automated_Email__c () ;
                ae.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Survey Email', 'automated_email__c' ) ;
                ae.Contact__c = c.ID ;
                ae.Contact_OR_Account__c = 'Contact' ;
                ae.Is_A_Survey__c = 'N' ;
                ae.Template_Name__c = 'i-got-bank-book-request' ;
                ae.First_Name__c = firstName ;
                ae.Last_Name__c = lastName ;
                ae.Email_Address__c = email ;
                
                try
                {
                    access.insertObject ( ae ) ;
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }
            else
            {
                System.debug ( 'No contact found to send email ?!?' ) ;
            }
        }
            
        return null ;
    }
}