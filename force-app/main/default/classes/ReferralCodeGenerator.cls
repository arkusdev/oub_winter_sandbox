public class ReferralCodeGenerator 
{
	//  --------------------------------------------------------------------------------
	//  INTERNAL
	//  --------------------------------------------------------------------------------
	private static final List<String> RANDOM_NUMBERS = new List<String> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' } ;
    private static final List<String> RANDOM_LETTERS = new List<String> { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' } ;

	//  --------------------------------------------------------------------------------
	//  GENERATE!
	//  --------------------------------------------------------------------------------
	public static void generateReferralCode ( Contact c )
    {
        List<Contact> cL = new list<Contact> () ;
        cL.add ( c ) ;
            
        generateReferralCodes ( cL ) ;
    }
    
    public static void generateReferralCodes ( list<Contact> cL )
    {
        Map<String,list<String>> codeMap = new map<String,list<String>> () ;
        list<String> masterCodeList = new list<String> () ;
        
        for ( Contact c : cL )
        {
	        list<String> codeList = new list<String> () ;
            String x ;
            
            String uFirstName = stripAndFix ( c.FirstName ) ;
            
            if ( String.isNotBlank ( uFirstName ) )
            {
                if ( uFirstName.length () >= 6 )
                {
                    x = uFirstName.left ( 6 ) ;
                    codeList.add ( x ) ;
                }
                else
                {
                    x = uFirstName ;
                    codeList.add ( x ) ;
                }
            }

            String uLastName = stripAndFix ( c.LastName ) ;
                
            if ( String.isNotBlank ( uLastName ) )
            {
                if ( uLastName.length () >= 6 )
                {
                    x = uLastName.left ( 6 ) ;
                    codeList.add ( x ) ;
                }
                else
                {
                    x = uLastName ;
                    codeList.add ( x ) ;
                }
            }
                
            if ( String.isNotBlank ( uFirstName ) )
            {
                if ( uFirstName.length () >= 5 )
                {
                    for ( integer i = 1 ; i <= 9 ; i ++ )
                    {
                        x = uFirstName.left ( 5 ) + i ;
                        codeList.add ( x ) ;
                    }
                }
                
                if ( uFirstName.length () >= 4 )
                {
                    for ( integer i = 1 ; i <= 9 ; i ++ )
                    {
                        x = uFirstName.left ( 4 ) + i ;
                        codeList.add ( x ) ;
                    }
                }
                
                if ( uFirstName.length () >= 3 )
                {
                    for ( integer i = 0 ; i <= 9 ; i ++ )
                    {
                        x = uFirstName.left ( 3 ) + getRandomThingys ( RANDOM_NUMBERS, 2 ) ;
                        codeList.add ( x ) ;
                    }
                }
            }
            
            if ( String.isNotBlank ( uLastName ) )
            {
                if ( uLastName.length () >= 5 )
                {
                    for ( integer i = 1 ; i <= 9 ; i ++ )
                    {
                        x = uLastName.left ( 5 ) + i ;
                        codeList.add ( x ) ;
                    }
                }
                
                if ( uLastName.length () >= 4 )
                {
                    for ( integer i = 1 ; i <= 9 ; i ++ )
                    {
                        x = uLastName.left ( 4 ) + i ;
                        codeList.add ( x ) ;
                    }
                }
                
                if ( uLastName.length () >= 3 )
                {
                    for ( integer i = 0 ; i <= 9 ; i ++ )
                    {
                        x = uLastName.left ( 3 ) + getRandomThingys ( RANDOM_NUMBERS, 2 ) ;
                        codeList.add ( x ) ;
                    }
                }
            }
            
            if ( ( String.isNotBlank ( uFirstName ) ) && ( String.isNotBlank ( uLastName ) ) )
            {
                if ( uLastName.length () >= 5 )
                {
                    x = uFirstName.left ( 1 ) + uLastName.left ( 5 ) ;
                    codeList.add ( x ) ;
                }
                
                if ( uLastName.length () >= 4 )
                {
                    for ( integer i = 1 ; i <= 9 ; i ++ )
                    {
                        x = uFirstName.left ( 1 ) + uLastName.left ( 4 ) + i ;
                        codeList.add ( x ) ;
                    }
                }
                
                if ( uLastName.length () >= 3 )
                {
                    for ( integer i = 0 ; i <= 9 ; i ++ )
                    {
                        x = uFirstName.left ( 1 ) + uLastName.left ( 3 ) + getRandomThingys ( RANDOM_NUMBERS, 2 ) ;
                        codeList.add ( x ) ;
                    }
                }
            }
            
            String bb ;
            for ( integer i = 0 ; i <= 9 ; i ++ )
            {
                bb = 'BB' + getRandomThingys ( RANDOM_NUMBERS, 2 ) ;
                codeList.add ( bb ) ;
            }
            
            for ( integer i = 0 ; i <= 9 ; i ++ )
            {
                bb = 'BB' + getRandomThingys ( RANDOM_NUMBERS, 3 ) ;
                codeList.add ( bb ) ;
            }
            
            //  Add to map
            codeMap.put ( c.ID, codeList ) ;
            
            //  Load to master
            masterCodeList.addAll ( codeList ) ;
        }
        
		//  --------------------------------------- LOOK UP Existing -----------------------------------------
        map<String, boolean> rcM = new map<String, boolean> () ;
        
        try
        {
            list<Referral_Code__c> rcL = [
                SELECT ID, Name
                FROM Referral_Code__c
                WHERE Name IN :masterCodeList
            ] ;
            
            if ( ( rcL != null ) && ( rcL.size () > 0 ) )
            {
                for ( Referral_Code__c rc : rcL )
                {
                    rcM.put ( rc.Name, true ) ;
                }
            }
        }
        catch ( Exception e )
        {
            rcM = null ;
        }
        
		//  --------------------------------------- CHECK FOR Existing -----------------------------------------
		list<Referral_Code__c> rcL = new list<Referral_Code__c> () ;
        
        for ( Contact c : cL )
        {
            if ( codeMap.containsKey ( c.ID ) )
            {
            	list<String> codeList = codeMap.get ( c.ID ) ;
                
                boolean found = false ;
                for ( String code : codeList )
                {
                    if ( ( ! rcM.containsKey ( code ) ) && ( ! found ) )
                    {
                        Referral_Code__c rc = new Referral_Code__c () ;
                        rc.Name = code ;
                        rc.Contact__c = c.ID ;
                        
                        rcL.add ( rc ) ;
                        found = true ;
                        rcM.put ( code, true ) ; // insert so we don't repeat
                    }
                }
            }
        }
        
		//  --------------------------------------- CREATE! -----------------------------------------
        if ( ( rcL != null ) && ( rcL.size () > 0 ) )
        {
            try
            {
                insert rcL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
    
    private static string getRandomThingys ( List<String> thingy, Integer len )
    {
		String retVal = '';
		
		if ( len != null && len >= 1 )
		{
			Integer chars = 0 ;
			Integer random ;
			do
			{
				random = Math.round ( Math.random () * Integer.valueOf ( thingy.size () - 1 ) ) ;
				
				// Should never happen any longer, but just in case ...
				if ( random < 0 ) random = 0 ;
				if ( random > thingy.size () ) random = thingy.size () ;
				
				retVal += thingy [ random ] ;
				chars++ ;
			} while ( chars < len ) ;
		}
		
		return retVal ;
    }
    
    private static string stripAndFix ( String x )
    {
        Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        
        String y = x.toUpperCase ().replaceAll ( '[^a-zA-Z0-9]', '' ) ;
        
        return y ;
    }
}