public with sharing class Deposit_Application_Utils 
{
	/*
	 *  List for document type drop down
	 */
    public static List<SelectOption> docTypeItems
    {
        get
        {
	        if ( docTypeItems == null )
	        {
	        	docTypeItems = new List<SelectOption> () ;
		        docTypeItems.add ( new SelectOption ( '','' ) ) ;
		        docTypeItems.add ( new SelectOption ( 'ID (Driver Lic./State ID)','Identification Information (Driver\'s license or State ID)' ) ) ;
		        docTypeItems.add ( new SelectOption ( 'Proof Income (Pay Stub/W-2/Other)','Proof of income (Pay stub, W-2 or other)' ) ) ;
		        docTypeItems.add ( new SelectOption ( 'Utility Bill','Utility Bill' ) ) ;
	        }
	        
	        return docTypeItems ;
        }
        
		set ;
    }
    
	/*
	 *  List for additional language for doc types
	 */
	public static map<String,String> codesAdditionalLanguage
	{
		get
		{
			if ( codesAdditionalLanguage == null )
			{
				codesAdditionalLanguage = new map<String,String> () ;
				
				//  Borrower codes
				codesAdditionalLanguage.put ( 'INVALID DRIVERS LICENSE FORMAT', 'verify your information' ) ;
				codesAdditionalLanguage.put ( 'INQ NAME DOESNT MATCH DL NAME', 'verify your information' ) ;
				codesAdditionalLanguage.put ( 'ADDRESS ALERT: NONRESIDENTIAL', 'verify your address' ) ;
				codesAdditionalLanguage.put ( 'DL NOT ON STATE FILE', 'verify your information' ) ;
				codesAdditionalLanguage.put ( 'INQ DOB DOESNT MATCH DL DOB', 'verify your information' ) ;
				codesAdditionalLanguage.put ( 'IDV', 'verify your information' ) ;
				codesAdditionalLanguage.put ( 'Review', 'verify your information' ) ;
			}
			
			return codesAdditionalLanguage ;
		}
		
		set ;
	}
	
	/*
	 * Documents required for those codes
	 */
	public static map<String,list<String>> codesAdditionalDocs
	{
		get
		{
			if ( codesAdditionalDocs == null )
			{
				codesAdditionalDocs = new map<String,list<String>> () ;
				
				// Applicant
				codesAdditionalDocs.put ( 'INVALID DRIVERS LICENSE FORMAT', new list<String> { 'ID (Driver Lic./State ID)' } ) ;
				codesAdditionalDocs.put ( 'INQ NAME DOESNT MATCH DL NAME', new list<String>  { 'ID (Driver Lic./State ID)' } ) ;
				codesAdditionalDocs.put ( 'ADDRESS ALERT: NONRESIDENTIAL', new list<String> { 'Utility Bill' } ) ;
				codesAdditionalDocs.put ( 'DL NOT ON STATE FILE', new list<String> { 'ID (Driver Lic./State ID)' } ) ;
				codesAdditionalDocs.put ( 'INQ DOB DOESNT MATCH DL DOB', new list<String> { 'ID (Driver Lic./State ID)' } ) ;
				codesAdditionalDocs.put ( 'IDV', new list<String> { 'ID (Driver Lic./State ID)', 'Utility Bill' } ) ;
			}
			
			return codesAdditionalDocs ;
		}
		set ;
	}
	
	public static String getMultiPicklistDocs ( String x )
	{
		String y = '' ;
		
		list<String> sL = codesAdditionalDocs.get ( x ) ;
		
		if ( sL != null )
		{
			boolean start = true ;
			for ( String s : sL )
			{
				if ( start )
					start = false ;
				else
					y = y + ';' ;
					
				y = y + s ;
			}
		}
		
		return y ;
	}
	
	public static String translateAdditionalInfoShortToLong ( String x )
	{
		String y = null ;
		
		if ( String.isNotBlank ( x ) )
		{
			if ( x.equalsIgnoreCase ( 'ID (SS Card/Recent W-2)' ) )
				y = 'Identification Information (Social Security Card or recent W-2)' ;
			
			if ( x.equalsIgnoreCase ( 'ID (Driver Lic./State ID)' ) )
				y = 'Identification Information (Driver\'s license or State ID)' ;

			if ( x.equalsIgnoreCase ( 'Proof Income (Pay Stub/W-2/Other)' ) )
				y = 'Proof of income (Pay stub, W-2 or other)' ;
			
			if ( x.equalsIgnoreCase ( 'Utility Bill' ) )
				y = 'Utility Bill' ;
		}
				
		return y ;
	}
	
	/*
	 * Returns a QR Code for the page passed in
	 */
	public static String getDocUploadQRCode ( String aid, String key )
	{
		return getDocUploadQRCode ( aid, key, 175, 175 ) ;
	}
	 
	public static String getDocUploadQRCode ( String aid, String key, Integer x, Integer y )
	{
	    String fullUrl;
		if ( String.isNotEmpty ( Site.getBaseUrl () ) )
		 	fullUrl = Site.getBaseUrl ()  ;
		else
			fullUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/apex/' ;
	    	
		String url = fullUrl + 'Deposit_Application_Additional_Info?aid=' + aid + '&kid=' + key ;
		
		String qrCode = 'https://chart.googleapis.com/chart?cht=qr&chs=' + x + 'x' + y + '&chl=' + EncodingUtil.urlEncode ( url, 'UTF-8' ) ;

		return qrCode ;
	}
	
	private static final List<String> RANDOM_CHARS = new List<String> { 
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
		} ;

	/*
	 * Generates random key for application that is used in uploading attachments.
	 */
	public static String getRandomString ( Integer len )
	{
		String retVal = '';
		
		if ( len != null && len >= 1 )
		{
			Integer chars = 0 ;
			Integer random ;
			do
			{
				random = Math.round ( Math.random () * Integer.valueOf ( RANDOM_CHARS.size () - 1 ) ) ;
				
				// Should never happen any longer, but just in case ...
				if ( random < 0 ) random = 0 ;
				if ( random > 62) random = 62 ;
				
				retVal += RANDOM_CHARS [ random ] ;
				chars++ ;
			} while ( chars < len ) ;
		}
		
		return retVal ;
	}
	
	public static list<String> getAdditionalInfoNeeded ( String s )
	{
		list<String> sL = null ;
		
		if ( String.isNotEmpty ( s ) )
		{
			sL = new list<String> () ;
			
			list<String> tL = s.split ( ';' ) ;
			
			for ( String t : tL )
			{
				String x = translateAdditionalInfoShortToLong ( t ) ;
				
				if ( String.isNotBlank ( x ) ) 
					sL.add ( translateAdditionalInfoShortToLong ( t ) ) ;
			}
		}
		
		return sL ;
	}

	public static Deposit_Application__c getApplicationWithData ( Integer i )
	{
		Deposit_Application__c da = new Deposit_Application__c () ;
	
		da.First_Name__c = 'Test' + i ;
		da.Middle_Name__c = 'Er' ;
		da.Last_Name__c = 'User' + i ;
		da.Email__c = 'Test' + i + '@User' + i + '.com' ;
		
		da.Address__c = '1313 Testingbird Lane' ;
		da.City__c = 'Boston' ;
		da.State__c = 'MA' ;
		da.Zip__c = '02110' ;
		da.Country__c = 'USA' ;
		
		da.Daytime_Phone__c = '617-555-1111' ;
		da.Day_Phone_Extension__c = '4357' ;
		da.Evening_Phone__c = '857-555-2222' ;
		da.Confirmation_Number__c = 'XYZ123' ;
		
		da.External_Key__c = getRandomString ( 30 ) ;
		
		insert da ;
		
		return da ;
	}
}