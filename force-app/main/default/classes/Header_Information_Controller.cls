public with sharing class Header_Information_Controller 
{
    public list<String> headers { public get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public Header_Information_Controller ()
	{
        headers = new list<String> () ;
        
        map<String,String> foo = ApexPages.currentPage().getHeaders () ;
        
        for ( String key : foo.keyset () )
        {
            headers.add ( key + ' --> ' + foo.get ( key ) ) ;
        }
    }
}