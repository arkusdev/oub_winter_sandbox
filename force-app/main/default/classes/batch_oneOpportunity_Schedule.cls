global class batch_oneOpportunity_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_oneOpportunity b = new batch_oneOpportunity () ;
		Database.executeBatch ( b ) ;
   }
}