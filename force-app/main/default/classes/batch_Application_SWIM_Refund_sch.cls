global class batch_Application_SWIM_Refund_sch implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Application_SWIM_Refund b = new batch_Application_SWIM_Refund () ;
		Database.executeBatch ( b ) ;
   }    
}