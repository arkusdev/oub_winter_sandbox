global class EmailActionFunction 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='Load Email for Marketing Cloud' )
	global static List<SendOnlineBankingResult> sendOnlineBankingEmail ( List<SendOnlineBankingRequest> requests ) 
	{
		//  ---------------------------------------------------------------------------------------        
        //  Run date/time
		//  ---------------------------------------------------------------------------------------        
        DateTime dt = DateTime.now () ;

		//  Not used?
		List<SendOnlineBankingResult> rL = new List<SendOnlineBankingResult> () ;
		
        //  ---------------------------------------------------------------------------------------        
        //  Create contact objects to save for sync
        //  ---------------------------------------------------------------------------------------        
        list<Automated_Email__c> aeL = new list<Automated_Email__c> () ;
        String surveyRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Survey Email', 'Automated_Email__c' ) ;
        
		//  ---------------------------------------------------------------------------------------        
		//  Sorting for Contact/Accounts
		//  ---------------------------------------------------------------------------------------        
		map<ID,SendOnlineBankingRequest> sobrM = new map<ID,SendOnlineBankingRequest> () ;
        list<ID> ciL = new list<ID> () ;
        list<ID> aiL = new list<ID> () ;
        
		for ( SendOnlineBankingRequest r : requests )
		{
        	if ( ! sobrM.containsKey ( r.contactID ) ) 
                sobrM.put ( r.contactID, r ) ;
            
            if ( String.isNotBlank ( r.idType ) )
            {
                if ( r.idType.equalsIgnoreCase ( 'Contact' ) )
                    ciL.add ( r.contactID ) ;
                else
                    aiL.add ( r.contactID ) ;
            }
            else
            {
                ciL.add ( r.contactID ) ;
            }
        }
        
		//  ---------------------------------------------------------------------------------------        
		//  Contact info and parsing
		//  ---------------------------------------------------------------------------------------        
		list<Contact> cL = new list<Contact> () ;
        
        try
        {
            cL = [
                SELECT ID, FirstName, LastName, Email, Available_for_Survey__c
                FROM Contact 
                WHERE ID IN :ciL
            ] ;
        }
        catch ( exception e )
        {
            cL = null ;
        }
        
		list<Contact> cxL1 = new list<Contact> () ;
		list<Contact> cxL2 = new list<Contact> () ;
        
        if ( ( cL != null ) && ( cL.size () > 0 ) ) 
        {
            for ( Contact c : cL )
            {
                SendOnlineBankingRequest sobr = sobrM.get ( c.ID ) ;
                String surveyYN = 'N' ;
                if ( String.isNotBlank ( sobr.surveyYN ) )
                    surveyYN = sobr.surveyYN ;
                
                boolean aYN = true ;
                if ( c.Available_for_Survey__c != null )
                    aYN = c.Available_for_Survey__c ;
                
                if ( surveyYN.equalsIgnoreCase ( 'N' ) )
                {
                    cxL1.add ( c ) ;
                }
                else if ( ( surveyYN.equalsIgnoreCase ( 'Y' ) ) && ( aYN == true ) )
                {
                    cxL1.add ( c ) ;
                    cxL2.add ( c ) ;
                }
            }
            
            if ( ( cxL1 != null ) && ( cxL1.size () <> 0 ) )
            {
                for ( Contact c : cxL1 )
                {
                    SendOnlineBankingRequest r = sobrM.get ( c.ID ) ;
        
                    Automated_Email__c ae = new Automated_Email__c () ;
                    ae.recordTypeID = surveyRecordTypeID ;
                    ae.First_Name__c = c.FirstName ;
                    ae.Last_Name__c = c.LastName ;
                    ae.Email_Address__c = c.Email ;
                    
                    if ( String.isNotBlank ( r.idType ) )
                    {
                        ae.Contact_OR_Account__c = r.idType ;
                        
                        if ( r.idType.equalsIgnoreCase ( 'Contact' ) )
                            ae.Contact__c = r.contactID ;
                        else
                            ae.Relationship__c = r.contactID ;
                    }
                    else
                    {
                        ae.Contact_OR_Account__c = 'Contact' ;
                        ae.Contact__c = r.contactID ;
                    }
                    
                    ae.Template_Name__c = r.TemplateName ;
                    ae.Is_A_Survey__c = r.surveyYN ;
                    
                    if ( String.isNotBlank ( r.mergeVarName1 ) && String.isNotBlank ( r.mergeVarValue1 ) )
                    {
                        ae.Merge_Var_Name_1__c = r.mergeVarName1 ;
                        ae.Merge_Var_Value_1__c = r.mergeVarValue1 ;
                    }
                    
                    if ( String.isNotBlank ( r.mergeVarName2 ) && String.isNotBlank ( r.mergeVarValue2 ) )
                    {
                        ae.Merge_Var_Name_2__c = r.mergeVarName2 ;
                        ae.Merge_Var_Value_2__c = r.mergeVarValue2 ;
                    }
        
                    if ( String.isNotBlank ( r.mergeVarName3 ) && String.isNotBlank ( r.mergeVarValue3 ) )
                    {
                        ae.Merge_Var_Name_3__c = r.mergeVarName3 ;
                        ae.Merge_Var_Value_3__c = r.mergeVarValue3 ;
                    }
        
                    if ( String.isNotBlank ( r.mergeVarName4 ) && String.isNotBlank ( r.mergeVarValue4 ) )
                    {
                        ae.Merge_Var_Name_4__c = r.mergeVarName4 ;
                        ae.Merge_Var_Value_4__c = r.mergeVarValue4 ;
                    }
        
                    if ( String.isNotBlank ( r.mergeVarName5 ) && String.isNotBlank ( r.mergeVarValue5 ) )
                    {
                        ae.Merge_Var_Name_5__c = r.mergeVarName5 ;
                        ae.Merge_Var_Value_5__c = r.mergeVarValue5 ;
                    }
                    
                    aeL.add ( ae ) ;
                }
            }
        }
		
		//  ---------------------------------------------------------------------------------------        
		//  Account data and parsing
		//  ---------------------------------------------------------------------------------------        
		list<Account> aL = new list<Account> () ;
        
        try
        {
            aL = [
                SELECT ID, Name, Business_Email__c, Available_for_Survey__c
                FROM Account 
                WHERE ID IN :aiL
            ] ;
        }
        catch ( exception e )
        {
            aL = null ;
        }
        
		list<Account> axL1 = new list<Account> () ;
		list<Account> axL2 = new list<Account> () ;
        
        if ( ( aL != null ) && ( aL.size () > 0 ) ) 
        {
            for ( Account a : aL )
            {
                SendOnlineBankingRequest sobr = sobrM.get ( a.ID ) ;
                String surveyYN = 'N' ;
                if ( String.isNotBlank ( sobr.surveyYN ) )
                    surveyYN = sobr.surveyYN ;
                
                boolean aYN = true ;
                if ( a.Available_for_Survey__c != null )
                    aYN = a.Available_for_Survey__c ;
                
                if ( surveyYN.equalsIgnoreCase ( 'N' ) )
                {
                    axL1.add ( a ) ;
                }
                else if ( ( surveyYN.equalsIgnoreCase ( 'Y' ) ) && ( aYN == true ) )
                {
                    axL1.add ( a ) ;
                    axL2.add ( a ) ;
                }
            }
                    
            //  ---------------------------------------------------------------------------------------        
            //  Create account objects to save for sync
            //  ---------------------------------------------------------------------------------------
            if ( ( axL1 != null ) && ( axL1.size () <> 0 ) )
            {
                for ( Account a : axL1 )
                {
                    SendOnlineBankingRequest r = sobrM.get ( a.ID ) ;
        
                    Automated_Email__c ae = new Automated_Email__c () ;
                    ae.recordTypeID = surveyRecordTypeID ;
                    ae.First_Name__c = a.Name ;
                    ae.Email_Address__c = a.Business_Email__c ;
                    
                    if ( String.isNotBlank ( r.idType ) )
                    {
                        ae.Contact_OR_Account__c = r.idType ;
                        
                        if ( r.idType.equalsIgnoreCase ( 'Contact' ) )
                            ae.Contact__c = r.contactID ;
                        else
                            ae.Relationship__c = r.contactID ;
                    }
                    else
                    {
                        ae.Contact_OR_Account__c = 'Contact' ;
                        ae.Contact__c = r.contactID ;
                    }
                    
                    ae.Template_Name__c = r.TemplateName ;
                    ae.Is_A_Survey__c = r.surveyYN ;
                    
                    if ( String.isNotBlank ( r.mergeVarName1 ) && String.isNotBlank ( r.mergeVarValue1 ) )
                    {
                        ae.Merge_Var_Name_1__c = r.mergeVarName1 ;
                        ae.Merge_Var_Value_1__c = r.mergeVarValue1 ;
                    }
                    
                    if ( String.isNotBlank ( r.mergeVarName2 ) && String.isNotBlank ( r.mergeVarValue2 ) )
                    {
                        ae.Merge_Var_Name_2__c = r.mergeVarName2 ;
                        ae.Merge_Var_Value_2__c = r.mergeVarValue2 ;
                    }
        
                    if ( String.isNotBlank ( r.mergeVarName3 ) && String.isNotBlank ( r.mergeVarValue3 ) )
                    {
                        ae.Merge_Var_Name_3__c = r.mergeVarName3 ;
                        ae.Merge_Var_Value_3__c = r.mergeVarValue3 ;
                    }
        
                    if ( String.isNotBlank ( r.mergeVarName4 ) && String.isNotBlank ( r.mergeVarValue4 ) )
                    {
                        ae.Merge_Var_Name_4__c = r.mergeVarName4 ;
                        ae.Merge_Var_Value_4__c = r.mergeVarValue4 ;
                    }
        
                    if ( String.isNotBlank ( r.mergeVarName5 ) && String.isNotBlank ( r.mergeVarValue5 ) )
                    {
                        ae.Merge_Var_Name_5__c = r.mergeVarName5 ;
                        ae.Merge_Var_Value_5__c = r.mergeVarValue5 ;
                    }
                    
                    aeL.add ( ae ) ;
                }
            }
        }
        
		//  ---------------------------------------------------------------------------------------
		//  Update new record to sync to marketing cloud 
		//  ---------------------------------------------------------------------------------------        
        if ( ( aeL != null ) && ( aeL.size () > 0 ) )
        {
            try
            {
                insert aeL ;
            }
            catch ( Exception e )
            {
                // ?!?
            }
        }
        
		//  ---------------------------------------------------------------------------------------        
        //  Update Contacts
		//  ---------------------------------------------------------------------------------------        
		if ( ( cxL2 != null ) && ( cxL2.size () > 0 ) )
        {
            for ( Contact c : cxL2 )
                c.Last_Survey_Sent__c = dt ;

            update cxL2 ;
        }
        
		//  ---------------------------------------------------------------------------------------        
        //  Accounts
		//  ---------------------------------------------------------------------------------------        
		if ( ( axL2 != null ) && ( axL2.size () > 0 ) )
        {
            for ( Account a : axL2 )
                a.Last_Survey_Sent__c = dt ;

            update axL2 ;
        }
        
		//  ---------------------------------------------------------------------------------------        
        //  Done
		//  ---------------------------------------------------------------------------------------        
        return rL ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class SendOnlineBankingRequest 
	{
	    @InvocableVariable ( label='Object ID' required=true )
	    public ID contactID ;
	    
	    @InvocableVariable ( label='Template Name' required=true )
	    public String TemplateName ;
	    
	    @InvocableVariable ( label='Contact OR Account' required=false )
	    public String idType ;
	    
	    @InvocableVariable ( label='Is A Survey' required=false )
	    public String surveyYN ;
	    
	    @InvocableVariable ( label='Merge Var Name 1' required=false )
	    public String mergeVarName1 ;
	    
	    @InvocableVariable ( label='Merge Var Value 1' required=false )
	    public String mergeVarValue1 ;
	    
	    @InvocableVariable ( label='Merge Var Name 2' required=false )
	    public String mergeVarName2 ;
	    
	    @InvocableVariable ( label='Merge Var Value 2' required=false )
	    public String mergeVarValue2 ;
	    
	    @InvocableVariable ( label='Merge Var Name 3' required=false )
	    public String mergeVarName3 ;
	    
	    @InvocableVariable ( label='Merge Var Value 3' required=false )
	    public String mergeVarValue3 ;

	    @InvocableVariable ( label='Merge Var Name 4' required=false )
	    public String mergeVarName4 ;
	    
	    @InvocableVariable ( label='Merge Var Value 4' required=false )
	    public String mergeVarValue4 ;

	    @InvocableVariable ( label='Merge Var Name 5' required=false )
	    public String mergeVarName5 ;
	    
	    @InvocableVariable ( label='Merge Var Value 5' required=false )
	    public String mergeVarValue5 ;
	}	
	
	global class SendOnlineBankingResult 
	{
	    @InvocableVariable
	    public ID contactID ;
	}
}