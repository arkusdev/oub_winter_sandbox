public with sharing class direct_deposit_form {
    @TestVisible private Ticket__c t = null; // Idk... after moving VF page to Community, the page will not render the ticket fields. (show up as null although ticket != null) Code below is workaround
    public String contact { public get; private set; }
    
    public String name { public get; private set; }
    public String address { public get; private set; }
    public String city { public get; private set; }
    public String state { public get; private set; }
    public String zip { public get; private set; }
    public String phone { public get; private set; }
    public String ssnFormatted { public get; private set; }
    public String acctNum { public get; private set; }
    public String acctNumSpaced { public get; private set; }
    public String acctType { public get; private set; }
    public String companyName { public get; private set; }
    public String eid { public get; private set; }
    public String amount { public get; private set; }
    public Boolean signature { public get; private set; }
    
    public direct_deposit_form(ApexPages.Standardcontroller stdC) {

        String tid;
        contact = '';
        name = '';
        address = '';
        city = '';
        state = '';
        zip = '';
        phone = '';
        ssnFormatted = '';
        acctNum = '';
        acctNumSpaced = '';
        acctType = '';
        companyName = '';
        eid = '';
        amount = '';
        
        if ( ApexPages.currentPage().getParameters().get('tid') != null )
        {
            tid = ApexPages.currentPage().getParameters().get('tid');
        }
        
        if ( ( tid != null ) && ( !tid.equals('null') ) )
        {
            System.debug('====== tid is not null. ======');
            System.debug('tid: ' + tid);
            
            t = [SELECT First_Name__c, Last_Name__c, Contact__c,
                 Contact_Address__c, Contact_City__c, Contact_State__c, Contact_Zip_Code__c, Contact_Phone__c, 
                 Account_Number__c, Account_Type__c, Social_Security_Number__c, 
                 Company_Name__c, Employee_Id__c, Electronic_Sig_Consent__c,
                 Direct_Deposit_Amount__c
                 FROM Ticket__c
                 WHERE Id = :tid
                 LIMIT 1];
            
            if ( t != null )
            {
                System.debug('====== Found associated ticket. ======');
                System.debug(t);
                name = t.First_Name__c + ' ' + t.Last_Name__c;
                address = t.Contact_Address__c;
                city = t.Contact_City__c;
                state = t.Contact_State__c;
                zip = t.Contact_Zip_Code__c;
                phone = t.Contact_Phone__c;      
                acctNum = t.Account_Number__c;
                acctType = t.Account_Type__c.substring(8);
                companyName = t.Company_Name__c;
                amount = t.Direct_Deposit_Amount__c;
                signature = t.Electronic_Sig_Consent__c;
                
                contact = t.Contact__c;
                
                if ( t.Employee_Id__c != null )
                {
                    eid = t.Employee_Id__c;
                }
                
                for ( Integer i=0; i<acctNum.length(); i++ )
                {
                    if ( i != ( acctNum.length() - 1 ) )
                    {
                    	acctNumSpaced = acctNumSpaced + acctNum.substring(i, i+1) + ' ';
                    }
                    else
                    {
                        acctNumSpaced = acctNumSpaced + acctNum.substring(i, i+1);
                    }
                }
                
                // Format the phone number
                if ( !Test.isRunningTest() )
                {
                    phone = '(' + phone;
                    phone = phone.substring(0, phone.indexOf('-')) + ') ' + phone.substring(phone.indexOf('-')+1);
                }
            }
            
            if ( t.Social_Security_Number__c != null )
            {
                ssnFormatted = t.Social_Security_Number__c.substring(0,3) + '-' + t.Social_Security_Number__c.substring(3,5) + '-' + t.Social_Security_Number__c.substring(5);
            }
        }
        
    }
}