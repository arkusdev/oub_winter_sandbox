@isTest
public class TestOUBFraudHandler 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;

    /* --------------------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------------------- */
    private static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = false ;

        s.Trial_Deposit_Validation_Limit__c = 3 ;
        s.ACH_Validation_Threshold__c = 1000.00 ;
        s.Deposit_ACH_Validation_Threshold__c = 1000.00 ;
        
        //  FIS STUFF
        s.FIS_Aquirer_ID__c = '059187' ;
        s.FIS_Authentication_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/authen' ;
        s.FIS_IDA_Config_Key__c = 'idaaliaskey' ;
        s.FIS_IDA_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/ida' ;
        s.FIS_Location_ID__c = 'UNITY Visa Application' ;
        s.FIS_Staging_Flag__c = true ;
        s.FIS_User_ID__c = '02280828' ;
        s.FIS_User_Password__c = 'Z26ZjcZdZAiZXaZ' ;
        
        insert s;
    }

	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Creates a generic application I use for testing.
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, String rtID )
	{
		one_application__c app = new one_application__c ();
        app.RecordTypeId = rtID ;
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = c1.TaxID__c.substring ( 0, 3 ) + '-' + c1.TaxID__c.substring ( 3, 5 ) + '-' + c1.TaxID__c.substring ( 5, 9 ) ;
        app.Phone__c = c1.Phone ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
        app.RequestedCreditLimit__c = 250.00 ;
		
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = c2.TaxID__c.substring ( 0, 3 ) + '-' + c2.TaxID__c.substring ( 3, 5 ) + '-' + c2.TaxID__c.substring ( 5, 9 ) ;
            app.Co_Applicant_Phone__c = c2.Phone ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Gross_Income_Monthly__c = 10000.00 ;

		app.FIS_Application_ID__c = null ;
		app.FIS_Decision_Code__c = null ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        //  Address - MINFraud
        app.Visitor_IP__c = '1.2.3.4' ;
        app.Visitor_User_Agent__c = 'Mozilla' ;
        app.Visitor_Accept_Language__c = 'YES' ;
		
		return app ;
	}

	/* --------------------------------------------------------------------------------
	 * Fraud IP
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testIPFraud ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100100101' ;
        app1.Funding_Routing_Number__c = '900900901' ;
        insert app1 ;
        
        oubfh.setApplication ( app1 ) ;
        String result1 = oubfh.checkFraud () ;
//        System.assertEquals ( result1, 'PASS' ) ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '1.2.3.4' ;
		app2.Funding_Options__c = 'E- Check (ACH)' ;
        app2.Funding_Account_Number__c = '100100102' ;
        app2.Funding_Routing_Number__c = '900900902' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
//        System.assertEquals ( result2, 'PASS' ) ;
        
        // #3
        Contact c3 = getContact ( 3 ) ;
        insert c3 ;
        
        one_Application__c app3 = getApplicationForTests ( c3, null, rtID ) ;
        app3.Visitor_IP__c = '1.2.3.4' ;
		app3.Funding_Options__c = 'E- Check (ACH)' ;
        app3.Funding_Account_Number__c = '100100103' ;
        app3.Funding_Routing_Number__c = '900900903' ;
        insert app3 ;
        
        oubfh.setApplication ( app3 ) ;
        String result3 = oubfh.checkFraud () ;
//        System.assertEquals ( result3, 'WARN' ) ;
//        one_application__c app3x = oubfh.getApplication () ;
//        System.assertNotEquals ( app3.FIS_Decision__c, app3x.FIS_Decision_Code__c ) ;
        
        // #4 -- DIFFERENT
        Contact c4 = getContact ( 4 ) ;
        insert c4 ;
        
        one_Application__c app4 = getApplicationForTests ( c4, null, rtID ) ;
        app4.Visitor_IP__c = '2.3.4.5' ;
		app4.Funding_Options__c = 'E- Check (ACH)' ;
        app4.Funding_Account_Number__c = '100100104' ;
        app4.Funding_Routing_Number__c = '900900904' ;
        insert app4 ;
        
        oubfh.setApplication ( app4 ) ;
        String result4 = oubfh.checkFraud () ;
//        System.assertEquals ( result4, 'PASS' ) ;
//        one_application__c app4x = oubfh.getApplication () ;
//        System.assertEquals ( app4.FIS_Decision__c, app4x.FIS_Decision_Code__c ) ;
        
        // #5 - WARN
        Contact c5 = getContact ( 5 ) ;
        insert c5 ;
        
        one_Application__c app5 = getApplicationForTests ( c5, null, rtID ) ;
        app5.Visitor_IP__c = '1.2.3.4' ;
		app5.Funding_Options__c = 'E- Check (ACH)' ;
        app5.Funding_Account_Number__c = '100100105' ;
        app5.Funding_Routing_Number__c = '900900905' ;
        insert app5 ;
        
        oubfh.setApplication ( app5 ) ;
        String result5 = oubfh.checkFraud () ;
//        System.assertEquals ( result5, 'WARN' ) ;
//        one_application__c app5x = oubfh.getApplication () ;
//        System.assertNotEquals ( app5.FIS_Decision__c, app5x.FIS_Decision_Code__c ) ;
        
        // #6 - FAIL
        Contact c6 = getContact ( 6 ) ;
        insert c6 ;
        
        one_Application__c app6 = getApplicationForTests ( c6, null, rtID ) ;
        app6.Visitor_IP__c = '1.2.3.4' ;
		app6.Funding_Options__c = 'E- Check (ACH)' ;
        app6.Funding_Account_Number__c = '100100105' ;
        app6.Funding_Routing_Number__c = '900900905' ;
        insert app6 ;
        
        oubfh.setApplication ( app6 ) ;
        String result6 = oubfh.checkFraud () ;
//        System.assertEquals ( result6, 'FAIL' ) ;
//        one_application__c app6x = oubfh.getApplication () ;
//        System.assertNotEquals ( app6.FIS_Decision__c, app6x.FIS_Decision_Code__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Fraud IP Whitelist
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testIPFraudWhitelist ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '204.60.84.2' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100100101' ;
        app1.Funding_Routing_Number__c = '900900901' ;
        insert app1 ;
        
        oubfh.setApplication ( app1 ) ;
        String result1 = oubfh.checkFraud () ;
        System.assertEquals ( result1, 'PASS' ) ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '204.60.84.2' ;
		app2.Funding_Options__c = 'E- Check (ACH)' ;
        app2.Funding_Account_Number__c = '100100102' ;
        app2.Funding_Routing_Number__c = '900900902' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'PASS' ) ;
        
        // #3
        Contact c3 = getContact ( 3 ) ;
        insert c3 ;
        
        one_Application__c app3 = getApplicationForTests ( c3, null, rtID ) ;
        app3.Visitor_IP__c = '204.60.84.2' ;
		app3.Funding_Options__c = 'E- Check (ACH)' ;
        app3.Funding_Account_Number__c = '100100103' ;
        app3.Funding_Routing_Number__c = '900900903' ;
        insert app3 ;
        
        oubfh.setApplication ( app3 ) ;
        String result3 = oubfh.checkFraud () ;
        System.assertEquals ( result3, 'PASS' ) ;
    }

    /* --------------------------------------------------------------------------------
	 * Fraud Funding
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFundingFraud ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100123987' ;
        app1.Funding_Routing_Number__c = '987123654' ;
        insert app1 ;
        
        oubfh.setApplication ( app1 ) ;
        String result1 = oubfh.checkFraud () ;
//        System.assertEquals ( result1, 'PASS' ) ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '1.2.3.5' ;
		app2.Funding_Options__c = 'E- Check (ACH)' ;
        app2.Funding_Account_Number__c = '100123987' ;
        app2.Funding_Routing_Number__c = '987123654' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
//        System.assertEquals ( result2, 'PASS' ) ;
        
        // #3 - WARN
        Contact c3 = getContact ( 3 ) ;
        insert c3 ;
        
        one_Application__c app3 = getApplicationForTests ( c3, null, rtID ) ;
        app3.Visitor_IP__c = '1.2.3.6' ;
		app3.Funding_Options__c = 'E- Check (ACH)' ;
        app3.Funding_Account_Number__c = '100123987' ;
        app3.Funding_Routing_Number__c = '987123654' ;
        insert app3 ;
        
        oubfh.setApplication ( app3 ) ;
        String result3 = oubfh.checkFraud () ;
//        System.assertEquals ( result3, 'WARN' ) ;
//        one_application__c app3x = oubfh.getApplication () ;
//        System.assertNotEquals ( app3.FIS_Decision__c, app3x.FIS_Decision_Code__c ) ;
        
        // #4 -- DIFFERENT
        Contact c4 = getContact ( 4 ) ;
        insert c4 ;
        
        one_Application__c app4 = getApplicationForTests ( c4, null, rtID ) ;
		app4.Funding_Options__c = 'E- Check (ACH)' ;
        app4.Funding_Account_Number__c = '100123980' ;
        app4.Funding_Routing_Number__c = '987123650' ;
        app4.Visitor_IP__c = '2.3.4.5' ;
        insert app4 ;
        
        oubfh.setApplication ( app4 ) ;
        String result4 = oubfh.checkFraud () ;
//        System.assertEquals ( result4, 'PASS' ) ;
//        one_application__c app4x = oubfh.getApplication () ;
//        System.assertEquals ( app4.FIS_Decision__c, app4x.FIS_Decision_Code__c ) ;
        
        // #5 - WARN
        Contact c5 = getContact ( 5 ) ;
        insert c5 ;
        
        one_Application__c app5 = getApplicationForTests ( c5, null, rtID ) ;
        app5.Visitor_IP__c = '1.2.3.7' ;
		app5.Funding_Options__c = 'E- Check (ACH)' ;
        app5.Funding_Account_Number__c = '100123987' ;
        app5.Funding_Routing_Number__c = '987123654' ;
        insert app5 ;
        
        oubfh.setApplication ( app5 ) ;
        String result5 = oubfh.checkFraud () ;
//        System.assertEquals ( result5, 'WARN' ) ;
//        one_application__c app5x = oubfh.getApplication () ;
//        System.assertNotEquals ( app5.FIS_Decision__c, app5x.FIS_Decision_Code__c ) ;
        
        // #6 - FAIL
        Contact c6 = getContact ( 6 ) ;
        insert c6 ;
        
        one_Application__c app6 = getApplicationForTests ( c6, null, rtID ) ;
        app6.Visitor_IP__c = '1.2.3.8' ;
		app6.Funding_Options__c = 'E- Check (ACH)' ;
        app6.Funding_Account_Number__c = '100123987' ;
        app6.Funding_Routing_Number__c = '987123654' ;
        insert app6 ;
        
        oubfh.setApplication ( app6 ) ;
        String result6 = oubfh.checkFraud () ;
//        System.assertEquals ( result6, 'FAIL' ) ;
//        one_application__c app6x = oubfh.getApplication () ;
//        System.assertNotEquals ( app6.FIS_Decision__c, app6x.FIS_Decision_Code__c ) ;
    }
    
    /* --------------------------------------------------------------------------------
	 * Fraud Address
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testAddressFraud ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
        app1.Street_Address__c = '100 Franklin street' ;
        app1.City__c = 'Boston' ;
        app1.State__c = 'MA' ;
        app1.ZIP_Code__c = '02110' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100123987' ;
        app1.Funding_Routing_Number__c = '987123654' ;
        insert app1 ;
        
        oubfh.setApplication ( app1 ) ;
        String result1 = oubfh.checkFraud () ;
        System.assertEquals ( result1, 'PASS' ) ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '1.2.3.5' ;
        app2.Street_Address__c = '100 Franklin str' ;
        app2.City__c = 'Boston' ;
        app2.State__c = 'MA' ;
        app2.ZIP_Code__c = '02110' ;
		app2.Funding_Options__c = 'E- Check (ACH)' ;
        app2.Funding_Account_Number__c = '100123983' ;
        app2.Funding_Routing_Number__c = '987123651' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'PASS' ) ;
        
        // #3
        Contact c3 = getContact ( 3 ) ;
        insert c3 ;
        
        one_Application__c app3 = getApplicationForTests ( c3, null, rtID ) ;
        app3.Visitor_IP__c = '1.2.3.6' ;
        app3.Street_Address__c = '100 Franklin st' ;
        app3.City__c = 'Boston' ;
        app3.State__c = 'MA' ;
        app3.ZIP_Code__c = '02110' ;
		app3.Funding_Options__c = 'E- Check (ACH)' ;
        app3.Funding_Account_Number__c = '100123982' ;
        app3.Funding_Routing_Number__c = '987123650' ;
        insert app3 ;
        
        oubfh.setApplication ( app3 ) ;
        String result3 = oubfh.checkFraud () ;
        System.assertEquals ( result3, 'WARN' ) ;
        
        // #4
        Contact c4 = getContact ( 4 ) ;
        insert c4 ;
        
        one_Application__c app4 = getApplicationForTests ( c4, null, rtID ) ;
        app4.Visitor_IP__c = '1.2.3.7' ;
        app4.Street_Address__c = '100 Franklin st' ;
        app4.City__c = 'Boston' ;
        app4.State__c = 'MA' ;
        app4.ZIP_Code__c = '02110' ;
		app4.Funding_Options__c = 'E- Check (ACH)' ;
        app4.Funding_Account_Number__c = '100123983' ;
        app4.Funding_Routing_Number__c = '987123650' ;
        insert app4 ;
        
        oubfh.setApplication ( app4 ) ;
        String result4 = oubfh.checkFraud () ;
        System.assertEquals ( result4, 'FAIL' ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * OUB Block - IP
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFraudTagIPAddress ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100123987' ;
        app1.Funding_Routing_Number__c = '987123654' ;
        insert app1 ;
        
        Application_Tag__c at1 = new Application_Tag__c () ;
        at1.Application__c = app1.ID ;
        at1.Tag_Type__c = 'Block IP Address' ;
        insert at1 ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '1.2.3.4' ;
        app2.Funding_Account_Number__c = '987654321' ;
        app2.Funding_Routing_Number__c = '123456789' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'FAIL' ) ;
        
        one_application__c app2x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app2x.FIS_Decision_Code__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * OUB Block - Funding
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFraudTagFundingAccount ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100123987' ;
        app1.Funding_Routing_Number__c = '987123654' ;
        insert app1 ;
        
        Application_Tag__c at1 = new Application_Tag__c () ;
        at1.Application__c = app1.ID ;
        at1.Tag_Type__c = 'Block Funding Account' ;
        insert at1 ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '2.3.4.5' ;
		app2.Funding_Options__c = 'E- Check (ACH)' ;
        app2.Funding_Account_Number__c = '100123987' ;
        app2.Funding_Routing_Number__c = '987123654' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'FAIL' ) ;
        
        one_application__c app2x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app2x.FIS_Decision_Code__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * OUB Block - Email
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFraudTagEmail ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        c1.Email = 'Block@email.com' ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
		app1.Funding_Options__c = 'Debit Card' ;
        insert app1 ;
        
        Application_Tag__c at1 = new Application_Tag__c () ;
        at1.Application__c = app1.ID ;
        at1.Tag_Type__c = 'Block Email' ;
        insert at1 ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        c2.Email = 'Block@email.com' ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
		app1.Funding_Options__c = 'Debit Card' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'FAIL' ) ;
        
        one_application__c app2x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app2x.FIS_Decision_Code__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Whitelist do not override - Tag
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFraudTagEmailWhitelist ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        c1.Email = 'Block@email.com' ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
		app1.Funding_Options__c = 'Debit Card' ;
        insert app1 ;
        
        Application_Tag__c at1 = new Application_Tag__c () ;
        at1.Application__c = app1.ID ;
        at1.Tag_Type__c = 'Block Email' ;
        insert at1 ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        c2.Email = 'Block@email.com' ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
		app2.Funding_Options__c = 'Debit Card' ;
        app2.Visitor_IP__c = '204.60.84.2' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'FAIL' ) ;
        
        one_application__c app2x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app2x.FIS_Decision_Code__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * OUB Block - Tax ID
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFraudTagTaxID ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        // #1
        Contact c1 = getContact ( 1 ) ;
        c1.TaxID__c = '999887777' ;
        insert c1 ;
        
        one_Application__c app1 = getApplicationForTests ( c1, null, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100123987' ;
        app1.Funding_Routing_Number__c = '987123654' ;
        insert app1 ;
        
        Application_Tag__c at1 = new Application_Tag__c () ;
        at1.Application__c = app1.ID ;
        at1.Tag_Type__c = 'Block Tax ID' ;
        insert at1 ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '1.2.3.4' ;
        app2.Funding_Account_Number__c = '987654321' ;
        app2.Funding_Routing_Number__c = '123456789' ;
        app2.SSN__c = '999-88-7777' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'FAIL' ) ;
        
        one_application__c app2x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app2x.FIS_Decision_Code__c ) ;
        
        // #3
        Contact c3 = getContact ( 3 ) ;
        insert c3 ;
        
        one_Application__c app3 = getApplicationForTests ( c3, null, rtID ) ;
        app3.Visitor_IP__c = '2.2.3.4' ;
        app3.Funding_Account_Number__c = '456789132' ;
        app3.Funding_Routing_Number__c = '456123789' ;
        app3.SSN__c = '111-88-7777' ;
        insert app3 ;
        
        oubfh.setApplication ( app3 ) ;
        String result3 = oubfh.checkFraud () ;
        System.assertEquals ( result3, 'PASS' ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * OUB Block - Tax ID
	 * -------------------------------------------------------------------------------- */
    public static testmethod void testFraudTagTaxIDCoApp ()
    {
        //  Setup
        insertCustomSetting () ;
        
        OUBFraudHandler oubfh = new OUBFraudHandler () ;
        String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        // #1a
        Contact c1a = getContact ( 1 ) ;
        c1a.TaxID__c = '999887777' ;
        insert c1a ;
        
        // #1b
        Contact c1b = getContact ( 1 ) ;
        c1b.TaxID__c = '111223333' ;
        insert c1b ;
        
        one_Application__c app1 = getApplicationForTests ( c1a, c1b, rtID ) ;
        app1.Visitor_IP__c = '1.2.3.4' ;
		app1.Funding_Options__c = 'E- Check (ACH)' ;
        app1.Funding_Account_Number__c = '100123987' ;
        app1.Funding_Routing_Number__c = '987123654' ;
        insert app1 ;
        
        Application_Tag__c at1 = new Application_Tag__c () ;
        at1.Application__c = app1.ID ;
        at1.Tag_Type__c = 'Block Tax ID' ;
        insert at1 ;
        
        // #2
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        one_Application__c app2 = getApplicationForTests ( c2, null, rtID ) ;
        app2.Visitor_IP__c = '1.2.3.4' ;
        app2.Funding_Account_Number__c = '987654321' ;
        app2.Funding_Routing_Number__c = '123456789' ;
        app2.SSN__c = '999-88-7777' ;
        insert app2 ;
        
        oubfh.setApplication ( app2 ) ;
        String result2 = oubfh.checkFraud () ;
        System.assertEquals ( result2, 'FAIL' ) ;
        
        one_application__c app2x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app2x.FIS_Decision_Code__c ) ;
        
        // #3
        Contact c3 = getContact ( 3 ) ;
        insert c3 ;
        
        one_Application__c app3 = getApplicationForTests ( c3, null, rtID ) ;
        app3.Visitor_IP__c = '1.2.3.4' ;
        app3.Funding_Account_Number__c = '456789132' ;
        app3.Funding_Routing_Number__c = '456123789' ;
        app3.SSN__c = '111-22-3333' ;
        insert app3 ;
        
        oubfh.setApplication ( app3 ) ;
        String result3 = oubfh.checkFraud () ;
        System.assertEquals ( result3, 'FAIL' ) ;
        
        one_application__c app3x = oubfh.getApplication () ;
        System.assertNotEquals ( app2.FIS_Decision__c, app3x.FIS_Decision_Code__c ) ;
    }
}