public class RetailTaskLogic 
{
	/*
	 * Handles email/batch letter functionality for retail tasks
	 */
	public static void handleRetailTaskLogic ( List<Task> taskList )
	{
	//	String APPLICATION_PREFIX = Schema.SObjectType.one_application__c.getKeyPrefix () ;
		String FINANCIAL_ACCOUNT_PREFIX = Schema.SObjectType.Financial_Account__c.getKeyPrefix () ;
		String oneOPPORTUNITY_PREFIX = Schema.SObjectType.oneOpportunity__c.getKeyPrefix () ;
		String WORKSHOPATTENDEE_PREFIX = Schema.SObjectType.Workshop_Attendee__c.getKeyPrefix () ;
		
		String retailRecordTypeId = OneUnitedUtilities.getRecordTypeId ( 'Retail Task' ) ;
		
		//  Select the template id
		EmailTemplate et1 = [
			SELECT Id, Subject, HtmlValue, Body 
			FROM EmailTemplate 
			WHERE IsActive = true AND DeveloperName = 'Log_a_Call_Spoke_To_Customer'
		] ;
		
		//  Stupid Email Hack #1
		Messaging.reserveSingleEmailCapacity(1);
		Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
		String[] toAddresses1 = new String[]{'invalid@emailaddr.es'};
		mail1.setToAddresses(toAddresses1);
		mail1.setUseSignature(false);
		mail1.setSaveAsActivity(false);
		mail1.setSenderDisplayName('MMPT');
		mail1.setTargetObjectId ( UserInfo.getUserId () ) ;
		mail1.setTemplateId ( et1.Id ) ;
	
		Savepoint sp1 = Database.setSavepoint();
		Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail1 } ) ;
		Database.rollback ( sp1 ) ;
		
		EmailTemplate et2 = [
			SELECT Id, Subject, HtmlValue, Body 
			FROM EmailTemplate 
			WHERE IsActive = true AND DeveloperName = 'Log_a_Call_Log_a_Call_Left_a_Message'
		] ;
		
		//  Stupid Email Hack #2
		Messaging.reserveSingleEmailCapacity(1);
		Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
		String[] toAddresses2 = new String[]{'invalid@emailaddr.es'};
		mail2.setToAddresses(toAddresses2);
		mail2.setUseSignature(false);
		mail2.setSaveAsActivity(false);
		mail2.setSenderDisplayName('MMPT');
		mail2.setTargetObjectId ( UserInfo.getUserId () ) ;
		mail2.setTemplateId ( et2.Id ) ;
	
		Savepoint sp2 = Database.setSavepoint();
		Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail2 } ) ;
		Database.rollback ( sp2 ) ;
		
		//  Reply to email
		OrgWideEmailAddress owea = [
			SELECT Id, Address
			FROM OrgWideEmailAddress 
			WHERE Address = 'customersupport@oneunited.com' 
		] ;
		
		//  List o crap
		List<String> applicationIDList = new List<String> () ;
		List<String> contactIDList = new List<String> () ;
		List<String> financialAccountIDList = new List<String> () ;
		List<String> opportunityIDList = new List<String> () ;
		List<String> workshopAttendeeIDList = new List<String> () ;
		
		//  Maps to load stuff for the email message
		Map<String,Integer> templateMap = new Map<String,Integer> () ;
		Map<String,String> whutMap = new Map<String,String> () ;
		Map<String,Task> taskMap = new Map<String, Task> () ;
		List<String> ownList = new List<String> () ;
	
		Map<String,Contact> contactLetterMap = new Map<String, Contact> () ;
		
		//  First loop, sort into buckets
		for ( Task t : taskList )
		{
			String stupidRecordTypeId = t.RecordTypeId ;
			
			if ( ( String.isNotEmpty ( t.RecordTypeId ) ) &&
				 ( t.RecordTypeId == retailRecordTypeId ) && 
			     ( t.Send_Customer_Communication__c == true ) &&
			     ( String.isNotBlank ( t.Type ) ) && 
			     ( t.Type.equalsIgnoreCase ( 'Call' ) ) &&
			     ( String.IsNotBlank ( t.Interested_in_New_Account__c ) )
			   )
			{
				if ( t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Not Enough Liquidity' ) || 
					 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Not Interested' ) ||
					 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Only Invest Through Quickrate' ) ||
					 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Other Investments' ) ||
					 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Left Message' ) 
				   )
				{
					//  Need to look the people up for the email
					ownList.add ( t.CreatedById ) ;
					
					//  Load the What IDs into various lists
					if ( String.isNotBlank ( t.whatId ) )
					{
						//  Copy to string so string functions work
						String stupidWhatId = t.whatId ;
						
		/*				if ( stupidWhatId.startsWith ( APPLICATION_PREFIX ) )
							applicationIDList.add ( t.whatId ) ;	*/
							
						if ( stupidWhatId.startsWith ( FINANCIAL_ACCOUNT_PREFIX ) )
							financialAccountIDList.add ( t.whatId ) ;
							
						if ( stupidWhatId.startsWith ( oneOPPORTUNITY_PREFIX ) )
							opportunityIDList.add ( t.whatId ) ;
							
						if ( stupidWhatId.startsWith ( WORKSHOPATTENDEE_PREFIX ) )
							workshopAttendeeIDList.add ( t.whatId ) ;
							
						taskMap.put ( t.whatId, t ) ;
					}
					
					//  No What, check for Who
					else if ( String.isNotBlank ( t.WhoId ) )
					{
						contactIDList.add ( t.WhoId ) ;
						taskMap.put ( t.WhoId, t ) ;
					}
					
					//  Set up the template
					if ( t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Not Enough Liquidity' ) || 
						 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Not Interested' ) ||
						 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Only Invest Through Quickrate' ) ||
						 t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Other Investments' )
					   )
					{
						System.debug ( '--> 1 ' ) ;
						if ( String.isNotBlank ( t.whatId ) )
						{
							System.debug ( 'WHAT: ' + t.WhatId + ' --> 1' ) ;
							templateMap.put ( t.WhatId, 1 ) ;
						}
						else if ( String.isNotBlank ( t.WhoId ) )
						{
							System.debug ( 'WHO: ' + t.WhoId + ' --> 1' ) ;
							templateMap.put ( t.WhoId, 1 ) ;
						}
						
						templateMap.put ( t.Id, 1 ) ;
					}
					
					if ( t.Interested_in_New_Account__c.equalsIgnoreCase ( 'Left Message' ) )
					{
						System.debug ( '--> 2 ' ) ;
						if ( String.isNotBlank ( t.whatId ) )
						{
							System.debug ( 'WHAT: ' + t.WhatId + ' --> 2' ) ;
							templateMap.put ( t.WhatId, 2 ) ;
						}
						else if ( String.isNotBlank ( t.WhoId ) )
						{
							System.debug ( 'WHO: ' + t.WhoId + ' --> 2' ) ;
							templateMap.put ( t.WhoId, 2 ) ;
						}
						
						templateMap.put ( t.Id, 2 ) ;
					}
				}
			}
		}
			
		/* -----------------------------------------------------------------------------------
		 * CONTACT PROCESSING
		 * ----------------------------------------------------------------------------------- */
	/*	
		if ( ( applicationIDList != null ) && ( applicationIDList.size () > 0 ) )
		{
			//  Contacts
			List<one_application__c> oneApplicationAccountList = [
				SELECT ID, Co_Applicant_Contact__c, Contact__c
				FROM one_application__c
				WHERE ID IN :applicationIDList
				AND Contact__c != NULL
			] ;
			
			for ( one_application__c o : oneApplicationAccountList )
			{
				contactIDList.add ( o.Contact__c ) ;
				
				whutMap.put ( o.Contact__c, o.id ) ;
				taskMap.put ( o.Contact__c, taskMap.get ( o.id ) ) ;
					
				Integer template = templateMap.get ( o.id ) ;
				templateMap.put ( o.Contact__c, template ) ;
				
				if ( String.isNotBlank ( o.Co_Applicant_Contact__c ) )
				{
					contactIDList.add ( o.Co_Applicant_Contact__c ) ;
					
					whutMap.put ( o.Co_Applicant_Contact__c, o.id ) ;
					taskMap.put ( o.Co_Applicant_Contact__c, taskMap.get ( o.id ) ) ;
					
					Integer template = templateMap.get ( o.id ) ;
					templateMap.put ( o.Co_Applicant_Contact__c, template ) ;
				}
			}
		}
	*/	
		if ( ( financialAccountIDList != null ) && ( financialAccountIDList.size () > 0 ) )
		{
			//  Contacts
			List<Financial_Account__c> financialAccountList = [
				SELECT ID, TAXRPTFORPERSNBR__c
				FROM Financial_Account__c
				WHERE ID IN :financialAccountIDList
				AND TAXRPTFORPERSNBR__c != NULL
			] ;
			
			for ( Financial_Account__c f : financialAccountList )
			{
				if ( String.isNotBlank ( f.TAXRPTFORPERSNBR__c ) )
				{
					contactIDList.add ( f.TAXRPTFORPERSNBR__c ) ;
					
					whutMap.put ( f.TAXRPTFORPERSNBR__c, f.id ) ;
					taskMap.put ( f.TAXRPTFORPERSNBR__c, taskMap.get ( f.id ) ) ;
					
					Integer template = templateMap.get ( f.id ) ;
					templateMap.put ( f.TAXRPTFORPERSNBR__c, template ) ;
				}
			}
		}
		
		if ( ( opportunityIDList != null ) && ( opportunityIDList.size () > 0 ) )
		{
			//  Contacts
			List<oneOpportunity__c> opportunityList = [
				SELECT ID, Opportunity_Contact__c
				FROM oneOpportunity__c
				WHERE ID IN :opportunityIDList
				AND Opportunity_Contact__c != NULL
			] ;
			
			for ( oneOpportunity__c o : opportunityList )
			{
				if ( String.isNotBlank ( o.Opportunity_Contact__c ) )
				{
					contactIDList.add ( o.Opportunity_Contact__c ) ;
					
					whutMap.put ( o.Opportunity_Contact__c, o.id ) ;
					taskMap.put ( o.Opportunity_Contact__c, taskMap.get ( o.id ) ) ;
	
					Integer template = templateMap.get ( o.id ) ;
					templateMap.put ( o.Opportunity_Contact__c, template ) ;
				}
			}
		}
		
		if ( ( workshopAttendeeIDList != null ) && ( workshopAttendeeIDList.size () > 0 ) )
		{
			//  Contacts
			List<Workshop_Attendee__c> workshopAttendeeList = [
				SELECT ID, Contact__c
				FROM Workshop_Attendee__c
				WHERE ID IN :workshopAttendeeIDList
				AND Contact__c != NULL
			] ;
			
			for ( Workshop_Attendee__c w : workshopAttendeeList )
			{
				if ( String.isNotBlank ( w.Contact__c ) )
				{
					contactIDList.add ( w.Contact__c ) ;
					
					whutMap.put ( w.Contact__c, w.id ) ;
					taskMap.put ( w.Contact__c, taskMap.get ( w.id ) ) ;
	
					Integer template = templateMap.get ( w.id ) ;
					templateMap.put ( w.Contact__c, template ) ;
				} 
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * Generate EMAILS
		 * ----------------------------------------------------------------------------------- */
		List<Messaging.SingleEmailMessage> lContactList = new List<Messaging.SingleEmailMessage> () ;
		
		if ( contactIDList.size () > 0 )
		{
			//  Load in users so we can get information for email
			List<User> uL = [
				SELECT Name, Title, email
				FROM User
				WHERE id IN :ownList
			] ;
	
			Map<String,User> uMap = new Map<String,User> () ;
			
			for ( User u : uL )
			{
				uMap.put ( u.Id, u ) ;
			}
			
			//  Load in contacts so we can get their information for the email
			List<Contact> cL = [
				SELECT 
				ID, FirstName, LastName, Email,
				MailingStreet, MailingCity, MailingState, MailingPostalCode
				FROM Contact
				WHERE ID IN :contactIdList
			] ;
			
			Map<String,Contact> cMap = new Map<String,Contact> () ;
			
			for ( Contact c : cL )
			{
				cMap.put ( c.Id, c ) ;
			}
			
			//  Loop through contact lists to create email message objects
			for ( String id : contactIDList )
			{
				Contact c = cMap.get ( id ) ;
				
				if ( ( String.isNotBlank ( c.Email ) ) && ( templateMap.containsKey ( id ) ) )
				{
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
					
			        mail.setTargetObjectId ( id ) ; // Must be a contact or lead!
			        
				    Task t = null ;
				    User u = null ;
				    
				    if ( taskMap.containsKey ( id ) )
				    {
				    	t = taskMap.get ( id ) ;
				    	
				    	if ( ( t.CreatedById != null ) && ( uMap.containsKey ( t.createdById ) ) ) 
							u = uMap.get ( t.CreatedById ) ;
				    }
				    
			        if ( u != null )
			        {
			        	mail.setReplyTo ( u.Email ) ;
			        	mail.setSenderDisplayName ( u.Name ) ;
			        }
			        else
			        {
				        mail.setReplyTo ( owea.address ) ;
				        mail.setOrgWideEmailAddressId ( owea.id ) ;
			        }
			        
			        if ( whutMap.containsKey ( id ) )
				        mail.setWhatId ( whutMap.get ( id ) ) ;
				    
			    	Integer templateId = templateMap.get ( id ) ;
			    	
			    	if ( templateId == 1 )
			    	{
				        mail.setSubject ( mail1.getSubject () ) ;
				        
				        String hM = mail1.getHTMLBody () ;
				        hM = hM.replace ( 'Dear', 'Dear ' + c.FirstName + ' ' + c.LastName ) ;
				        
				        if ( u != null )
					        hM = hM.replace ( 'Sincerely,', 'Sincerely,<br><br>'+u.Name+'<br>'+u.Title ) ;
					    else
					        hM = hM.replace ( 'Sincerely,', 'Sincerely,<br><br>'+'OneUnited Bank' ) ;
					        
				        mail.setHtmlBody ( hM ) ;
				        String tM = mail1.getPlainTextBody () ;
				        tM = tM.replace ( 'Dear ', 'Dear ' + c.FirstName + ' ' + c.LastName ) ;
				        
				        if ( u != null )
					        tM = tM.replace ( 'Sincerely,', 'Sincerely,\n\n'+u.Name+'\n'+u.Title ) ;
					    else
					        tM = tM.replace ( 'Sincerely,', 'Sincerely,\n\n'+'OneUnited Bank' ) ;
		
			     		mail.setPlainTextBody ( tM ) ;
			    	}
				    else if ( templateId == 2 )
			    	{
				        mail.setSubject ( mail2.getSubject () ) ;
				        
				        String hM = mail2.getHTMLBody () ;
				        hM = hM.replace ( 'Dear', 'Dear ' + c.FirstName + ' ' + c.LastName ) ;
				        
				        if ( u != null )
					        hM = hM.replace ( 'Sincerely,', 'Sincerely,<br><br>'+u.Name+'<br>'+u.Title ) ;
					    else
					        hM = hM.replace ( 'Sincerely,', 'Sincerely,<br><br>'+'OneUnited Bank' ) ;
					        
				        mail.setHtmlBody ( hM ) ;
				        
				        String tM = mail2.getPlainTextBody () ;
				        tM = tM.replace ( 'Dear', 'Dear ' + c.FirstName + ' ' + c.LastName ) ;
				        
				        if ( u != null )
					        tM = tM.replace ( 'Sincerely,', 'Sincerely,\n\n'+u.Name+'\n'+u.Title ) ;
					    else
					        tM = tM.replace ( 'Sincerely,', 'Sincerely,\n\n'+'OneUnited Bank' ) ;
					        
				        mail.setPlainTextBody ( tM ) ;
			    	}
	
					mail.setSaveAsActivity ( true ) ;
				     	
			     	lContactList.add ( mail ) ;
				}
				else
				{
			        Task t = taskMap.get ( id ) ;
			        
					contactLetterMap.put ( t.id, c ) ;
				}
			}
	
			if ( lContactList.size () > 0 )
			{
				Messaging.reserveSingleEmailCapacity ( lContactList.size () ) ;
				Messaging.SendEmailResult[] sRL = Messaging.sendEmail ( lContactList, false ) ;
				
				for ( Messaging.SendEmailResult sR : sRL )
				{
					if ( ! sR.isSuccess () )
					{
						System.debug ( 'Error found!' ) ;
						Messaging.SendEmailError[] seeL = sR.getErrors () ;
						
						for ( Messaging.SendEmailError see : seeL )
						{
							System.debug ( 'Error -- ' + see.getStatusCode() + ' -- ' + see.getMessage () ) ;
						}
					}
				}
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * SECOND LOOP, BATCH LETTER
		 * ----------------------------------------------------------------------------------- */
		String batchLetterRecordTypeId = OneUnitedUtilities.getRecordTypeId ( 'Batch Letter' ) ; 
		
		List<Task> oT = [
			SELECT ID, Subject, WhatId, WhoId
			FROM Task
			WHERE ID IN :contactLetterMap.keyset ()
		] ;
	
		List<Task> nT = new List<Task> () ;
	
		for ( Task n : oT )
		{
			Contact c = contactLetterMap.get ( n.Id ) ;
			
			Task t = new Task () ;
			
			t.Subject = 'Batch Letter: ' + n.Subject ;
	        t.RecordTypeId = batchLetterRecordTypeId ;
			t.Generate_Batch_Letter__c = true ;
			t.Status = 'Open' ;
			t.Priority = 'Normal' ;
			
			t.First_Name__c = c.FirstName ;
			t.Last_Name__c = c.LastName ;
	        t.Mailing_Address__c = c.MailingStreet ;
	        t.Mailing_City__c = c.MailingCity ;
	        t.Mailing_State__c = c.MailingState ;
	        t.Mailing_Zip__c = c.MailingPostalCode ;
	        
	        t.ActivityDate = Date.today ().addDays ( 1 ) ;
//	        t.ActivityDate = Date.today () ;
	        
	        if ( String.isNotEmpty ( n.WhatId ) )
		        t.WhatId = n.WhatId ;
		        
	        if ( String.isNotEmpty ( n.WhoId ) )
		     	t.WhoId = n.WhoId ;
	        
		    if ( templateMap.containsKey ( n.id ) )
		    	t.Template_ID__c = templateMap.get ( n.id ) ;
		    
		    nT.add ( t ) ;
		}
		
        try
        {
			insert nT ;
        }
        catch ( DMLException e )
        {
            // ?
        }
	}
}