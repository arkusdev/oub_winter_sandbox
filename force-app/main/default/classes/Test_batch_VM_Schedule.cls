@isTest
public with sharing class Test_batch_VM_Schedule 
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';

	static private void setSettings ( Date d )
	{
		Vendor_Management_Settings__c vms = new Vendor_Management_Settings__c () ;
		
		vms.Name = 'VM_Settings' ;
		vms.Email_CC__c = 'test@test.com' ;
		vms.Email_Override__c = 'test@test.com' ;
		vms.Landing_ID__c = '' ;
		vms.Monthly_Notifications__c = true ;
		vms.Monthly_Notification_Day__c = d.day () ;
		vms.Monthly_Upcoming_Warnings__c = true ;
		vms.Monthly_Upcoming_Warning_Day__c = d.day () ;
		vms.Status_Notification__c = true ;
		vms.Status_Notification_Day__c = d.day () ;
		vms.Yearly_Reminder_Date__c = d ;
		vms.Yearly_Reminder_Enabled__c = true ;
		vms.Contract_Expiration__c = true ;
		vms.Contract_Notification__c = true ;
		vms.Contract_Notification_Days__c = String.valueOf ( d.day () ) + ',' + String.valueOf ( d.day () + 15 ) ;
		
		insert vms ;
	}
	
	static testmethod void something ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Date d = Date.Today () ;
		setSettings ( d ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;

		batch_VendorManagement_Schedule myClass = new batch_VendorManagement_Schedule ();   
		String chron = '0 0 23 * * ?';        
		system.schedule ( 'Test Schedule', chron, myClass ) ;
		
		Test.stopTest () ;
	}

}