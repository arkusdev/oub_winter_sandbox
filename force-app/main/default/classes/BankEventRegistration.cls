public with sharing class BankEventRegistration 
{
   	private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    /*  ----------------------------------------------------------------------------------------
     *  Page input variables
     *  ---------------------------------------------------------------------------------------- */
    public Workshop_Attendee__c wa { public get ; public set ; }

    /*  ----------------------------------------------------------------------------------------
     *  Tracking stuff
     *  ---------------------------------------------------------------------------------------- */
    public String utmCampaign { public get ; public set ; }
    public String utmContent { public get ; public set ; }
    public String utmMedium { public get ; public set ; }
    public String utmSource { public get ; public set ; }
    public String utmTerm { public get ; public set ; }
    public String utmVisitorId { public get ; public set ; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Internal variables and controls
     *  ---------------------------------------------------------------------------------------- */
    public String workshopId { public get ; private set ; }
    public Integer fStepNumber { public get ; private set ; }
    
    public Workshop__c w { public get ; private set ; }
    private List<Workshop__c> wL { private get ; private set ; } 
    
    private String prospectRecordTypeId { private get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public Map<String,String> errorMap { public get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Confirmation
     *  ---------------------------------------------------------------------------------------- */
    public String confirmationNumber { public get; private set; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Other stuff
     *  ---------------------------------------------------------------------------------------- */
    public List<SelectOption> workshopOptionList { public get ; private set ; }
    public String workshopPickId { public get ; public set ; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public BankEventRegistration ( ApexPages.Standardcontroller stdC )
    {
        //  Assume not found until proven otherwise
        fStepNumber = 1500 ;
        wL = new List<Workshop__c> () ;
        
        //  Incoming date, time and location
        String wid = ApexPages.currentPage().getParameters().get('wid') ;
        
        System.debug ( 'WID: ' + wid ) ;
        
        //  Confirm variables passed in
        if ( ( String.isNotBlank ( wid ) ) || ( String.isNotBlank ( workshopId ) ) )
        {
            workshopId = wid ;
            
            //  Record type for possible new contact
            prospectRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
            
            //  Check for event
            wL = [
                SELECT ID, Title__c, Event_Start_Date__c, Event_Start_Time__c, Branch_Name__c, Address__c
                FROM Workshop__c
                WHERE ID = :workshopId
            ] ;
            
            //  Event exists!
            if ( ( wL != null ) && ( wL.size () > 0 ) )
            {
                //  Grab the first one, set for output
                w = wL.get ( 0 ) ;
                
                //  Activate the initial panel
                fStepNumber = 1000 ;
                
                //  Initialize variable
                wa = new Workshop_Attendee__c () ; 
            }
        }
        else
        {
            System.debug ( 'Nothing passed in, loading all' ) ;
            
            //  Load and display all FUTURE workshops
            List<Workshop__c> workshopList = [
                SELECT ID, Name, Branch_Name__c, Description__c, Event_Start_Date__c, Event_Start_Time__c, Title__c, Type__c, Workshop_Description__c
                FROM Workshop__c
                WHERE Event_Start_Date__c >= LAST_N_DAYS:7
                ORDER BY Event_Start_Date__c
            ] ;
            
            if ( ( workshopList != null ) && ( workshopList.size () > 0 ) )
            {
                System.debug ( 'Found future workshops' ) ;
                fStepNumber = 3000 ;
                
                workshopOptionList = new List<SelectOption> () ;
                workshopOptionlist.add ( new SelectOption ( '', '- Please choose -' ) ) ;
                
                for ( Workshop__c w : workshopList )
                    workshopOptionlist.add ( new SelectOption ( w.id, w.Name + ' - ' + toStringNotNullDate( w.Event_Start_Date__c ) ) ) ;
            }   
        }
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Submit the form
     *  ---------------------------------------------------------------------------------------- */
    public PageReference submit () 
    {
        //  Reset step number
        fStepNumber = 1000 ;
        
        //  Check for validation, setup
        errorMap = new Map<String, String> () ;
        
        if ( String.isBlank ( wa.First_Name__c ) )
            errorMap.put ( 'First_Name__c' , 'Please enter your first name' ) ;
        else
            errorMap.put ( 'First_Name__c', '' ) ;
        
        if ( String.isBlank ( wa.Last_Name__c ) )
            errorMap.put ( 'Last_Name__c' , 'Please enter your last name' ) ;
        else
            errorMap.put ( 'Last_Name__c', '' ) ;
        
        if ( String.isBlank ( wa.Email_Address__c ) )
            errorMap.put ( 'Email_Address__c' , 'Please enter a valid email address' ) ;
        else if ( ( String.isNotBlank ( wa.Email_Address__c ) ) && ( ! OneUnitedUtilities.isValidEmail ( wa.Email_Address__c ) ) )
            errorMap.put ( 'Email_Address__c' , 'Please enter a valid email address' ) ;
        else
            errorMap.put ( 'Email_Address__c', '' ) ;
            
        if ( String.isBlank ( wa.Phone_Number__c ) )
            errorMap.put ( 'Phone_Number__c' , 'Please enter your phone number' ) ;
        else if ( ! OneUnitedUtilities.isValidPhoneNumber ( wa.Phone_Number__c ) )
            errorMap.put ( 'Phone_Number__c' , 'Please enter a valid phone number' ) ;
        else
        {
            wa.Phone_Number__c = OneUnitedUtilities.cleanPhoneNumber ( wa.Phone_Number__c ) ;
            errorMap.put ( 'Phone_Number__c', '' ) ;
        }
        
        if ( String.isBlank ( wa.Address__c ) )
            errorMap.put ( 'Address__c' , 'Please enter your street' ) ;
        else
            errorMap.put ( 'Address__c', '' ) ;
            
        if ( String.isBlank ( wa.City__c ) )
            errorMap.put ( 'City__c' , 'Please enter your city' ) ;
        else
            errorMap.put ( 'City__c', '' ) ;

        if ( String.isBlank ( wa.State__c ) )
            errorMap.put ( 'State__c' , 'Please enter your state' ) ;
        else
            errorMap.put ( 'State__c', '' ) ;

        if ( String.isBlank ( wa.Zip_Code__c ) )
            errorMap.put ( 'Zip_Code__c' , 'Please enter your zip code' ) ;
        else if ( ! OneUnitedUtilities.isValidZipCode ( wa.Zip_Code__c ) ) 
            errorMap.put ( 'Zip_Code__c' , 'Please enter a valid zip code' ) ;
        else
        {
            wa.Zip_Code__c = OneUnitedUtilities.cleanZipCode ( wa.Zip_Code__c ) ;
            errorMap.put ( 'Zip_Code__c', '' ) ;
        }
        
        //  Validation start
        boolean isValid = true ;
        
        //  Non blank entry means an error occurred
        for ( String key : errorMap.keySet () )
        {
            if ( String.isNotEmpty ( errorMap.get ( key ) ) )
                isValid = false ;
        }
        
        //  Logic based on validation
        if ( isValid )
        {
            System.debug ( 'No Errors, proceding to logic' ) ;
            
            System.debug ( 'UTM1 "' + utmCampaign + '"' ) ;
            System.debug ( 'UTM2 "' + utmContent + '"' ) ;
            System.debug ( 'UTM3 "' + utmMedium + '"' ) ;
            System.debug ( 'UTM4 "' + utmSource + '"' ) ;
            System.debug ( 'UTM5 "' + utmTerm + '"' ) ;
            System.debug ( 'UTM6 "' + utmVisitorId + '"' ) ;
            
            Contact c = OneUnitedUtilities.lookupContact ( wa.Email_Address__c ) ;
            
            if ( c != null )
            {
                System.debug ( 'Found Contact(s) - updating!' ) ;
                
                boolean isUpdate = false ;
                
                if ( String.isNotBlank ( utmCampaign ) )
                {
                    c.UTM_Campaign__c = utmCampaign ;
                    isUpdate = true ;
                }
                
                if ( String.isNotBlank ( utmContent ) )
                {
                    c.UTM_Content__c = utmContent ;
                    isUpdate = true ;
                }
                
                if ( String.isNotBlank ( utmMedium ) )
                {
                    c.UTM_Medium__c = utmMedium ;
                    isUpdate = true ;
                }
                
                if ( String.isNotBlank ( utmSource ) )
                {
                    c.UTM_Source__c = utmSource ;
                    isUpdate = true ;
                }
                
                if ( String.isNotBlank ( utmTerm ) )
                {
                    c.UTM_Term__c = utmTerm ;
                    isUpdate = true ;
                }
                
                if ( String.isNotBlank ( utmVisitorId ) )
                {
                    c.UTM_VisitorID__c = utmVisitorId ;
                    isUpdate = true ;
                }
                
                //  Only update if UTM passed in
                if ( isUpdate )
                {
                    System.debug ( 'Updating contact with UTM' ) ;
                    //update c ;
                    access.updateObject(c);
                }
            }
            else
            {
                System.debug ( 'Create special contact!' ) ;
                
                c = new Contact () ;
                c.FirstName = wa.First_Name__c ;
                c.LastName = wa.Last_Name__c ;
                
                if ( String.isNotBlank ( wa.Email_Address__c ) )
                    c.Email = wa.Email_Address__c ;
                
                c.Phone = wa.Phone_Number__c ;
                
                c.LeadSource = 'OUB Website Workshop' ;
                
                if ( String.isNotBlank ( utmCampaign ) )
                {
                    c.UTM_Campaign__c = utmCampaign ;
                }
                
                if ( String.isNotBlank ( utmContent ) )
                {
                    c.UTM_Content__c = utmContent ;
                }
                
                if ( String.isNotBlank ( utmMedium ) )
                {
                    c.UTM_Medium__c = utmMedium ;
                }
                
                if ( String.isNotBlank ( utmSource ) )
                {
                    c.UTM_Source__c = utmSource ;
                }
                
                if ( String.isNotBlank ( utmTerm ) )
                {
                    c.UTM_Term__c = utmTerm ;
                }
                
                if ( String.isNotBlank ( utmVisitorId ) )
                {
                    c.UTM_VisitorID__c = utmVisitorId ;
                }
                
                if ( String.isNotBlank ( prospectRecordTypeId ) ) 
                    c.RecordTypeID = prospectRecordTypeId ;
                
                //insert c ;
                access.insertObject(c);
            }
            
            System.debug ( 'Contact ID = ' + c.ID ) ;
            System.debug ( 'Creating workshop attendee' ) ;
            
            //  Set up rest of fields
            wa.Contact__c = c.ID ;
            wa.Workshop_ID__c = w.ID ;
            wa.Attended__c = true ;
            
            //insert wa ;
            access.insertObject(wa);
            
            //  Set default blank
            confirmationNumber = null ;
            
            //  Re-select record to get the number
            System.debug ( 'Getting confirmation number' ) ;
            if ( wa != null )
            {
                Workshop_Attendee__c wa2 = [ SELECT ID, Name FROM Workshop_Attendee__c WHERE ID = :wa.ID ] ;
                
                if ( wa2 != null )
                {
                    confirmationNumber = wa2.Name ;
                }
            }
            
            System.debug ( '# - ' + confirmationNumber ) ;
            
            //  Go to final panel!
            fStepNumber = 2000 ;
        }
        else
        {
            fStepNumber = 1001 ;
        }
        
        
        //  Done!
        return null ;
    }

    /*  ----------------------------------------------------------------------------------------
     *  Submit the form
     *  ---------------------------------------------------------------------------------------- */
    public PageReference pickWorkshop () 
    {
        PageReference pageRef = ApexPages.currentPage () ;
        pageRef.setRedirect ( true ) ;
        pageRef.getParameters().put ( 'wid', workshopPickId ) ;
        return pageRef ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Date formatting
     *  ---------------------------------------------------------------------------------------- */
    private string toStringNotNullDate(Date d)
    {
        if(d != null){
            DateTime dt = DateTime.newInstance(d.year(), d.month(), d.day());
            String str = dt.format('MM/dd/YYYY');
            return stringNotNull(str);
        }
        else{
            return '';
        }
    }
    
    private string stringNotNull(string s){
        if(s == null){
            s = '';
        }
        return s;
    }
}