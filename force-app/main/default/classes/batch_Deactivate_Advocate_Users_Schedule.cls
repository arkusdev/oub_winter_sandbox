global class batch_Deactivate_Advocate_Users_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Deactivate_Advocate_Users b = new batch_Deactivate_Advocate_Users () ;
		Database.executeBatch ( b ) ;
   }
}