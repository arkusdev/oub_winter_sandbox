public without sharing class ContentVersionHelper 
{

    public static ID getContentDocumentID ( ID cvID )
    {
        ID xID = null ;
        
        list<ContentVersion> cvL = [
            SELECT ID, ContentDocumentID
            FROM ContentVersion
            WHERE ID = :cvID
            LIMIT 1
        ] ;
        
        if ( ( cvL != null ) && ( cvL.size () > 0 ) )
        {
            xID = cvL [ 0 ].ContentDocumentID ;
        }
        
        return xID ;
    }
}