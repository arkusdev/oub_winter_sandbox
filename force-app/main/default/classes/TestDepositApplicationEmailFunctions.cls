@isTest
public with sharing class TestDepositApplicationEmailFunctions 
{
    /* -----------------------------------------------------------------------------------
	 * Settings
	 * ----------------------------------------------------------------------------------- */
	private static Deposit_Application_Settings__c getSettings ()
	{
		Deposit_Application_Settings__c das = new Deposit_Application_Settings__c () ;
		das.Name = 'Andera' ;
		das.Email_Callout_Functionality__c = true ;
		
		insert das ;
		
		return das ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a deposit application
	 * ----------------------------------------------------------------------------------- */
	private static Deposit_Application__c getDepositApplication ( Integer i )
	{
		Deposit_Application__c da = new Deposit_Application__c () ;
		
		da.First_Name__c = 'dtest' + i ;
		da.Middle_Name__c = 'Er' ;
		da.Last_Name__c = 'duser' + i ;
		da.Email__c = 'dtest' + i + '@duser' + i + '.com' ;
		da.Applicant_SSN__c = '00101100' + i ;
		
		da.Address__c = '1313 Testingbird Lane' ;
		da.City__c = 'Boston' ;
		da.State__c = 'MA' ;
		da.Zip__c = '02110' ;
		da.Country__c = 'USA' ;
		
		da.Daytime_Phone__c = '617-555-1111' ;
		da.Day_Phone_Extension__c = '4357' ;
		da.Evening_Phone__c = '857-555-2222' ;
		
		da.Confirmation_Number__c = 'XYZ123' ;

		da.Co_Applicant_First_Name__c = 'cotest' + i ;
		da.Co_Applicant_Last_Name__c = 'couser' + i ;
		da.Co_Applicant_Email__c = 'cotest' + i + '@couser' + i + '.com' ;
		da.Co_Applicant_SSN__c = '00202200' + i ;
		
		da.Co_Applicant_Address__c = '1313 Testingbird Lane' ;
		da.Co_Applicant_City__c = 'Boston' ;
		da.Co_Applicant_State__c = 'MA' ;
		da.Co_Applicant_Zip__c = '02110' ;
		
		da.Co_Applicant_Daytime_Phone__c = '617-555-1111' ;
		da.Co_Applicant_Day_Phone_Extension__c = '4357' ;
		da.Co_Applicant_Evening_Phone__c = '857-555-2222' ;

		da.Beneficiary_First_Name__c = 'betest' + i ;
		da.Beneficiary_Last_Name__c = 'beuser' + i ;
		da.Beneficiary_Email__c = 'betest' + i + '@beuser' + i + '.com' ;
		da.Beneficiary_SSN__c = '00303300' + i ;

		da.Beneficiary_Address__c = '1313 Testingbird Lane' ;
		da.Beneficiary_City__c = 'Boston' ;
		da.Beneficiary_State__c = 'MA' ;
		da.Beneficiary_Zip__c = '02110' ;
		
		da.Beneficiary_Daytime_Phone__c = '617-555-1111' ;
		da.Beneficiary_Day_Phone_Extension__c = '4357' ;
		da.Beneficiary_Evening_Phone__c = '857-555-2222' ;

		return da ;
	}
    
    /* -----------------------------------------------------------------------------------
	 * Additional info email
	 * ----------------------------------------------------------------------------------- */
	static testmethod void test_sendAdditionalInfoEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Load settings
		Deposit_Application_Settings__c das = getSettings () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
		Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		da1.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		da1.Additional_Information_Required__c = Deposit_Application_Utils.getMultiPicklistDocs ( 'IDV' ) ;
		insert da1 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( da1.ID ) ;
		
		DepositApplicationEmailFunctions.sendDepositApplicationAdditionalInfoEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Denial Email
	 * ----------------------------------------------------------------------------------- */
	static testmethod void test_sendDenialEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Load settings
		Deposit_Application_Settings__c das = getSettings () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
		Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		insert da1 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( da1.ID ) ;
		
		DepositApplicationEmailFunctions.sendDepositApplicationDenialEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * FCRA Denial Email
	 * ----------------------------------------------------------------------------------- */
	static testmethod void test_sendFCRADenialEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Load settings
		Deposit_Application_Settings__c das = getSettings () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
		Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		insert da1 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( da1.ID ) ;
		
		DepositApplicationEmailFunctions.sendDepositApplicationFCRADenialEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
}