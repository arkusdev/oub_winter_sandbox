@isTest
public class TestEverfiActionFunction 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Individuals' ;
        
		return a ;
	}
	
	public static testmethod void ONE ()
    {
        Account a = getAccount () ;
        insert a ;
        
        list<Everi_Module__c> emL = new list<Everi_Module__c> () ;
        
        Everi_Module__c em1 = new Everi_Module__c () ;
        em1.Name = 'Module1' ;
        em1.Module_ID__c = '1001' ;
        em1.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em1 ) ;
        
        Everi_Module__c em2 = new Everi_Module__c () ;
        em2.Name = 'Module2' ;
        em2.Module_ID__c = '1002' ;
        em2.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em2 ) ;
        
        Everi_Module__c em3 = new Everi_Module__c () ;
        em3.Name = 'Module3' ;
        em3.Module_ID__c = '1003' ;
        em3.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em3 ) ;
        
        Everi_Module__c em4 = new Everi_Module__c () ;
        em4.Name = 'Module4' ;
        em4.Module_ID__c = '1004' ;
        em4.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em4 ) ;
        
        insert emL ;
        
        list<Everfi_Playlist__c> epL = new list<Everfi_Playlist__c> () ;
        
        Everfi_Playlist__c ep1 = new Everfi_Playlist__c () ;
        ep1.Name = 'Play1' ;
        ep1.Playlist_ID__c = '2001' ;
        ep1.Playlist_URL__c = 'https://www.cnn.com' ;
        epL.add ( ep1 ) ;
        
        Everfi_Playlist__c ep2 = new Everfi_Playlist__c () ;
        ep2.Name = 'Play2' ;
        ep2.Playlist_ID__c = '2002' ;
        ep2.Playlist_URL__c = 'https://www.cnn.com' ;
        epL.add ( ep2 ) ;
        
        insert epL ;
        
        list<Everfi_Playlist_Module__c> epmL = new list<Everfi_Playlist_Module__c> () ;
        
        Everfi_Playlist_Module__c epm1 = new Everfi_Playlist_Module__c () ;
        epm1.Everfi_Playlist__c = epL [ 0 ].ID ;
        epm1.Everfi_Module__c = emL [ 0 ].ID ;
        epmL.add ( epm1 ) ;
        
        Everfi_Playlist_Module__c epm2 = new Everfi_Playlist_Module__c () ;
        epm2.Everfi_Playlist__c = epL [ 0 ].ID ;
        epm2.Everfi_Module__c = emL [ 1 ].ID ;
        epmL.add ( epm2 ) ;
        
        Everfi_Playlist_Module__c epm3 = new Everfi_Playlist_Module__c () ;
        epm3.Everfi_Playlist__c = epL [ 1 ].ID ;
        epm3.Everfi_Module__c = emL [ 2 ].ID ;
        epmL.add ( epm3 ) ;
        
        Everfi_Playlist_Module__c epm4 = new Everfi_Playlist_Module__c () ;
        epm4.Everfi_Playlist__c = epL [ 1 ].ID ;
        epm4.Everfi_Module__c = emL [ 3 ].ID ;
        epmL.add ( epm4 ) ;
        
        insert epmL ;
        
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test1' ;
        c1.LastName = 'User1' ;
        c1.Email = 'Test1@user1.com' ;
        insert c1 ;
        
        Contact c2 = new Contact () ;
        c2.FirstName = 'Test2' ;
        c2.LastName = 'User2' ;
        c2.Email = 'Test2@user1.com' ;
        insert c2 ;
        
        list<Everfi_Profile__c> eprL = new list<Everfi_Profile__c> () ;

        Everfi_Profile__c epr1 = new Everfi_Profile__c () ;
        epr1.First_Name__c = c1.FirstName ;
        epr1.Last_Name__c = c1.LastName ;
        epr1.Email_Address__c = c1.Email ;
        epr1.Contact__c = c1.ID ;
        eprL.add ( epr1 ) ;
        
        Everfi_Profile__c epr2 = new Everfi_Profile__c () ;
        epr2.First_Name__c = c2.FirstName ;
        epr2.Last_Name__c = c2.LastName ;
        epr2.Email_Address__c = c2.Email ;
        epr2.Contact__c = c2.ID ;
        eprL.add ( epr2 ) ;
        
        insert epRL ;
        
        list<Module__c> mL = new list<Module__c> () ;
        
        Module__c m1 = new Module__c () ;
        m1.Everfi_Profile__c = eprL [ 0 ].ID ;
        m1.Everfi_Module__c = emL [ 0 ].ID ;
        m1.Course_Completed_Date__c = System.now () ;
        mL.add ( m1 ) ;
        
        Module__c m2 = new Module__c () ;
        m2.Everfi_Profile__c = eprL [ 0 ].ID ;
        m2.Everfi_Module__c = emL [ 1 ].ID ;
        m2.Course_Completed_Date__c = System.now () ;
        mL.add ( m2 ) ;
        
        Module__c m3 = new Module__c () ;
        m3.Everfi_Profile__c = eprL [ 1 ].ID ;
        m3.Everfi_Module__c = emL [ 2 ].ID ;
        m3.Course_Completed_Date__c = System.now () ;
        mL.add ( m3 ) ;
        
        Module__c m4 = new Module__c () ;
        m4.Everfi_Profile__c = eprL [ 1 ].ID ;
        m4.Everfi_Module__c = emL [ 3 ].ID ;
        m4.Course_Completed_Date__c = System.now () ;
        mL.add ( m4 ) ;
        
        insert mL ;
        
        list<EverfiActionFunction.EverfiCompletePlaylistRequest> eafEpcrL = new list<EverfiActionFunction.EverfiCompletePlaylistRequest> () ;
        
        EverfiActionFunction.EverfiCompletePlaylistRequest eafepcr1 = new EverfiActionFunction.EverfiCompletePlaylistRequest () ;
        eafepcr1.everFiProfileID = eprL [ 0 ].ID ;
        eafEpcrL.add ( eafEpcr1 ) ;
        
        EverfiActionFunction.EverfiCompletePlaylistRequest eafepcr2 = new EverfiActionFunction.EverfiCompletePlaylistRequest () ;
        eafepcr2.everFiProfileID = eprL [ 1 ].ID ;
        eafEpcrL.add ( eafEpcr2 ) ;
        
		list<EverfiActionFunction.EverfiCompletePlaylistResult> rL = EverfiActionFunction.generateCompletedPlaylists ( eafEpcrL ) ;
	}
    
	public static testmethod void TWO ()
    {
        Account a = getAccount () ;
        insert a ;
        
        list<Everi_Module__c> emL = new list<Everi_Module__c> () ;
        
        Everi_Module__c em1 = new Everi_Module__c () ;
        em1.Name = 'Module1' ;
        em1.Module_ID__c = '1001' ;
        em1.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em1 ) ;
        
        Everi_Module__c em2 = new Everi_Module__c () ;
        em2.Name = 'Module2' ;
        em2.Module_ID__c = '1002' ;
        em2.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em2 ) ;
        
        Everi_Module__c em3 = new Everi_Module__c () ;
        em3.Name = 'Module3' ;
        em3.Module_ID__c = '1003' ;
        em3.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em3 ) ;
        
        Everi_Module__c em4 = new Everi_Module__c () ;
        em4.Name = 'Module4' ;
        em4.Module_ID__c = '1004' ;
        em4.Module_URL__c = 'https://www.cnn.com' ;
        emL.add ( em4 ) ;
        
        insert emL ;
        
        list<Everfi_Playlist__c> epL = new list<Everfi_Playlist__c> () ;
        
        Everfi_Playlist__c ep1 = new Everfi_Playlist__c () ;
        ep1.Name = 'Play1' ;
        ep1.Playlist_ID__c = '2001' ;
        ep1.Playlist_URL__c = 'https://www.cnn.com' ;
        epL.add ( ep1 ) ;
        
        Everfi_Playlist__c ep2 = new Everfi_Playlist__c () ;
        ep2.Name = 'Play2' ;
        ep2.Playlist_ID__c = '2002' ;
        ep2.Playlist_URL__c = 'https://www.cnn.com' ;
        epL.add ( ep2 ) ;
        
        insert epL ;
        
        list<Everfi_Playlist_Module__c> epmL = new list<Everfi_Playlist_Module__c> () ;
        
        Everfi_Playlist_Module__c epm1 = new Everfi_Playlist_Module__c () ;
        epm1.Everfi_Playlist__c = epL [ 0 ].ID ;
        epm1.Everfi_Module__c = emL [ 0 ].ID ;
        epmL.add ( epm1 ) ;
        
        Everfi_Playlist_Module__c epm2 = new Everfi_Playlist_Module__c () ;
        epm2.Everfi_Playlist__c = epL [ 0 ].ID ;
        epm2.Everfi_Module__c = emL [ 1 ].ID ;
        epmL.add ( epm2 ) ;
        
        Everfi_Playlist_Module__c epm3 = new Everfi_Playlist_Module__c () ;
        epm3.Everfi_Playlist__c = epL [ 1 ].ID ;
        epm3.Everfi_Module__c = emL [ 2 ].ID ;
        epmL.add ( epm3 ) ;
        
        Everfi_Playlist_Module__c epm4 = new Everfi_Playlist_Module__c () ;
        epm4.Everfi_Playlist__c = epL [ 1 ].ID ;
        epm4.Everfi_Module__c = emL [ 3 ].ID ;
        epmL.add ( epm4 ) ;
        
        insert epmL ;
        
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test1' ;
        c1.LastName = 'User1' ;
        c1.Email = 'Test1@user1.com' ;
        insert c1 ;
        
        Contact c2 = new Contact () ;
        c2.FirstName = 'Test2' ;
        c2.LastName = 'User2' ;
        c2.Email = 'Test2@user1.com' ;
        insert c2 ;
        
        list<Everfi_Profile__c> eprL = new list<Everfi_Profile__c> () ;

        Everfi_Profile__c epr1 = new Everfi_Profile__c () ;
        epr1.First_Name__c = c1.FirstName ;
        epr1.Last_Name__c = c1.LastName ;
        epr1.Email_Address__c = c1.Email ;
        epr1.Contact__c = c1.ID ;
        eprL.add ( epr1 ) ;
        
        Everfi_Profile__c epr2 = new Everfi_Profile__c () ;
        epr2.First_Name__c = c2.FirstName ;
        epr2.Last_Name__c = c2.LastName ;
        epr2.Email_Address__c = c2.Email ;
        epr2.Contact__c = c2.ID ;
        eprL.add ( epr2 ) ;
        
        insert epRL ;
        
        list<Module__c> mL = new list<Module__c> () ;
        
        Module__c m1 = new Module__c () ;
        m1.Everfi_Profile__c = eprL [ 0 ].ID ;
        m1.Everfi_Module__c = emL [ 0 ].ID ;
        m1.Course_Completed_Date__c = System.now () ;
        mL.add ( m1 ) ;
        
        Module__c m2 = new Module__c () ;
        m2.Everfi_Profile__c = eprL [ 0 ].ID ;
        m2.Everfi_Module__c = emL [ 1 ].ID ;
        m2.Course_Completed_Date__c = System.now () ;
        mL.add ( m2 ) ;
        
        Module__c m3 = new Module__c () ;
        m3.Everfi_Profile__c = eprL [ 1 ].ID ;
        m3.Everfi_Module__c = emL [ 2 ].ID ;
        m3.Course_Completed_Date__c = System.now () ;
        mL.add ( m3 ) ;
        
        Module__c m4 = new Module__c () ;
        m4.Everfi_Profile__c = eprL [ 1 ].ID ;
        m4.Everfi_Module__c = emL [ 3 ].ID ;
        m4.Course_Completed_Date__c = System.now () ;
        mL.add ( m4 ) ;
        
        insert mL ;
        
        list<Completed_Playlist__c> cpL = new list<Completed_Playlist__c> () ;
        
        Completed_Playlist__c cp1 = new Completed_Playlist__c () ;
        cp1.Everfi_Playlist__c = ep1.ID ;
        cp1.Everfi_Profile__c = epr1.ID ;
        cp1.Completed_Date__c = System.now () ;
        cpL.add ( cp1 ) ;
        
        insert cpL ;
        
        list<EverfiActionFunction.EverfiCompletePlaylistRequest> eafEpcrL = new list<EverfiActionFunction.EverfiCompletePlaylistRequest> () ;
        
        EverfiActionFunction.EverfiCompletePlaylistRequest eafepcr1 = new EverfiActionFunction.EverfiCompletePlaylistRequest () ;
        eafepcr1.everFiProfileID = eprL [ 0 ].ID ;
        eafEpcrL.add ( eafEpcr1 ) ;
        
        EverfiActionFunction.EverfiCompletePlaylistRequest eafepcr2 = new EverfiActionFunction.EverfiCompletePlaylistRequest () ;
        eafepcr2.everFiProfileID = eprL [ 1 ].ID ;
        eafEpcrL.add ( eafEpcr2 ) ;
        
		list<EverfiActionFunction.EverfiCompletePlaylistResult> rL = EverfiActionFunction.generateCompletedPlaylists ( eafEpcrL ) ;
	}
}