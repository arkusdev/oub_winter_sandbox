@isTest
public with sharing class TestProjectTrigger 
{
	public static testmethod void testOne ()
	{
		Profile pr = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u1 = new User () ;
		u1.firstName = 'Test' ;
		u1.lastName = 'User' ;
		u1.email = 'test1@user.com' ;
		u1.username = 'test1user1@user.com' ;
		u1.alias = 'tuser1' ;
		u1.CommunityNickname = 'tuser1' ;
		u1.TimeZoneSidKey = 'GMT' ;
		u1.LocaleSidKey = 'en_US' ;
		u1.EmailEncodingKey = 'UTF-8' ;
		u1.ProfileId = pr.id ;
		u1.LanguageLocaleKey = 'en_US' ;
		u1.Branch__c = 'Crenshaw' ;
		insert u1 ;
		
		User u2 = new User () ;
		u2.firstName = 'Test' ;
		u2.lastName = 'User' ;
		u2.email = 'test2@user.com' ;
		u2.username = 'test2user2@user.com' ;
		u2.alias = 'tuser2' ;
		u2.CommunityNickname = 'tuser2' ;
		u2.TimeZoneSidKey = 'GMT' ;
		u2.LocaleSidKey = 'en_US' ;
		u2.EmailEncodingKey = 'UTF-8' ;
		u2.ProfileId = pr.id ;
		u2.LanguageLocaleKey = 'en_US' ;
		u1.Branch__c = 'Stocker' ;
		insert u2 ;
		
		Project__c p = new Project__c () ;
		
		p.Name = 'Test Project' ;
		p.Project_Subject__c = 'Test Project' ;
		p.Project_Description__c = 'Testing' ;
		p.Project_Status__c = 'Open' ;
		p.Project_Priority__c = 'Normal' ;
		p.Project_Due_Date__c = Date.today () ;
		p.OwnerId = u1.id ;
		
		insert p ;
		
		p.OwnerId = u2.id ;
		
		update p ;
	}
}