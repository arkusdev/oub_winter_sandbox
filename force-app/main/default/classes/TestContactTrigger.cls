@isTest
public class TestContactTrigger 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Account getAccount ( Integer x )
    {
        Account a = new Account () ;
        a.Name = 'Test' + x ;
        
        return a ;
    }
    
	private static Contact getContact ( Integer x, Account a )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
        c.AccountId = a.ID ;
		
		return c ;
	}
	
    private static Contact getContactEmail ( Integer i, String rtID, Account a )
    {
    	Contact c = new Contact () ;
    	
    	c.FirstName = 'test' + i ;
    	c.LastName = 'user' + i ;
    	c.Email = 'test' + i + '@user' + i + '.com' ;
    	c.RecordTypeID = rtID ;
    	
    	if ( a != null )
	    	c.Account = a ;
    	
    	return c ;
    }

    private static Contact getContactPhone ( Integer i, String rtID, Account a )
    {
    	Contact c = new Contact () ;
    	
    	c.FirstName = 'test' + i ;
    	c.LastName = 'user' + i ;
    	c.Phone = '00' + i + '-00' + i + '-000' + i ;
    	c.RecordTypeID = rtID ;
    	
    	if ( a != null )
	    	c.Account = a ;
    	
    	return c ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Advocate Setup
	//  --------------------------------------------------------------------------------------
    private static void setupAdvocateCrap ()
    {
        list<Advocate_Badge__c> abL = new list<Advocate_Badge__c> () ;

        Advocate_Badge__c ab1 = new Advocate_Badge__c () ;
        ab1.API_Name__c = 'BankBlack_1' ;
        ab1.Description__c = 'XYZ123' ;
        ab1.Badge_Points__c = 100 ;
        
        abL.add ( ab1 ) ;
        
        Advocate_Badge__c ab2 = new Advocate_Badge__c () ;
        ab2.API_Name__c = 'BankBlack_3' ;
        ab2.Description__c = 'XYZ123' ;
        ab2.Badge_Points__c = 100 ;
        
        abL.add ( ab2 ) ;
        
        Advocate_Badge__c ab3 = new Advocate_Badge__c () ;
        ab3.API_Name__c = 'BankBlack_5' ;
        ab3.Description__c = 'XYZ123' ;
        ab3.Badge_Points__c = 100 ;
        
        abL.add ( ab3 ) ;
        
        Advocate_Badge__c ab4 = new Advocate_Badge__c () ;
        ab4.API_Name__c = 'New_Deposit_Account_1' ;
        ab4.Description__c = 'XYZ123' ;
        ab4.Badge_Points__c = 100 ;
        
        abL.add ( ab4 ) ;
        
        Advocate_Badge__c ab5 = new Advocate_Badge__c () ;
        ab5.API_Name__c = 'BankBlack_Challenge' ;
        ab5.Description__c = 'XYZ123' ;
        ab5.Badge_Points__c = 100 ;
        
        abL.add ( ab5 ) ;
        
        Advocate_Badge__c ab6 = new Advocate_Badge__c () ;
        ab6.API_Name__c = 'Link_Up' ;
        ab6.Description__c = 'XYZ123' ;
        ab6.Badge_Points__c = 100 ;
        
        abL.add ( ab6 ) ;
        
        Advocate_Badge__c ab7 = new Advocate_Badge__c () ;
        ab7.API_Name__c = 'Direct_Satisfaction' ;
        ab7.Description__c = 'XYZ123' ;
        ab7.Badge_Points__c = 100 ;
        
        abL.add ( ab7 ) ;
        
        insert abL ;
        
        list<Advocate_Activity__c> aaL = new list<Advocate_Activity__c> () ;
        
        for ( integer i = 1 ; i <= 10 ; i ++ )
        {
            Advocate_Activity__c aa = new Advocate_Activity__c () ;
            aa.API_Name__c = 'Transactions_' + i ;
            aa.Activity_Points__c = 100 ;
            aa.Name = 'Bleah' ;
            aa.Description__c = 'ABC123' ;
            
	        aaL.add ( aa ) ;
        }
        
        for ( integer i = 2 ; i <= 4 ; i ++ )
        {
            Advocate_Activity__c aa = new Advocate_Activity__c () ;
            aa.API_Name__c = 'New_Deposit_Account_' + i ;
            aa.Activity_Points__c = 100 ;
            aa.Name = 'Bleah' ;
            aa.Description__c = 'ABC123' ;
            
	        aaL.add ( aa ) ;
        }
        
        Advocate_Activity__c aa1 = new Advocate_Activity__c () ;
        aa1.API_Name__c = 'PopMoney' ;
        aa1.Activity_Points__c = 100 ;
        aa1.Name = 'Bleah' ;
        aa1.Description__c = 'ABC123' ;
        
        aaL.add ( aa1 ) ;
        
        Advocate_Activity__c aa2 = new Advocate_Activity__c () ;
        aa2.API_Name__c = 'Mobile_Remote_Deposit_Capture' ;
        aa2.Activity_Points__c = 100 ;
        aa2.Name = 'Bleah' ;
        aa2.Description__c = 'ABC123' ;
        
        aaL.add ( aa2 ) ;
        
        Advocate_Activity__c aa3 = new Advocate_Activity__c () ;
        aa3.API_Name__c = 'Transfer_Funds' ;
        aa3.Activity_Points__c = 100 ;
        aa3.Name = 'Bleah' ;
        aa3.Description__c = 'ABC123' ;
        
        aaL.add ( aa3 ) ;
        
        insert aaL ;
        
        list<Advocate_Level__c> aLL = new list<Advocate_Level__c> () ;
        
        Advocate_Level__c aL1 = new Advocate_Level__c () ;
        aL1.Description__c = 'ABC123' ;
        aL1.Achievement_Points__c = 100.00 ;
        aL1.Name = 'ABC234' ;
        
        aLL.add ( aL1 ) ;
        
        Advocate_Level__c aL2 = new Advocate_Level__c () ;
        aL2.Description__c = 'NP' ;
        aL2.Achievement_Points__c = 0 ;
        aL2.Name = 'Non Participant' ;
        
        aLL.add ( aL2 ) ;
        
        insert aLL ;
     }
    
	//  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	static testmethod void ADVOCATE_ONE ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        c1.Personal_Checking_Accounts__c = 1 ;
        c1.Add_Advocate_Referral_Code__c = 'Y' ;
        cL.add ( c1 ) ;

        Contact c2 = getContact ( 2, a ) ;
        c2.Advocate_Timestamp__c = System.now () ;
        c2.Add_Advocate_Referral_Code__c = 'Y' ;
        cL.add ( c2 ) ;
        
        Advocate_Level__c al1 = new Advocate_Level__c () ;
        al1.name = 'Test' ;
        al1.Internal_Description__c = 'TEST' ;
        insert al1 ;
        
        Contact c3 = getContact ( 3, a ) ;
        c3.Advocate_Level__c = al1.ID ;
        c3.Add_Advocate_Referral_Code__c = 'Y' ;
        cL.add ( c3 ) ;
        
        insert ( cL ) ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :cL [ 0 ].ID 
        ] ;
        
        System.assertNotEquals ( abaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_TWO ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        c1.Personal_Checking_Accounts__c = 1.0 ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
        
        cx1.ADDDATE__c = Date.today ().addYears ( -1 ).addDays ( -1 ) ;
        update cx1 ;
        
        cx1.ADDDATE__c = Date.today ().addYears ( -3 ) ;
        update cx1 ;
        
        cx1.ADDDATE__c = Date.today ().addYears ( -5 ) ;
        update cx1 ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( abaL.size (), 4 ) ; // 3 YEARS + Active account
    }
    
	static testmethod void ADVOCATE_TWO_BEE ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        Contact c1 = getContact ( 1, a ) ;
        c1.Personal_Checking_Accounts__c = 1.0 ;
        c1.ADDDATE__c = Date.today ().addYears ( -10 ).addDays ( -1 ) ;
        
        insert c1 ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( abaL.size (), 4 ) ; // 3 YEARS + Active account
    } 
       
	static testmethod void ADVOCATE_THREE ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
		cx1.MTD_Pers_Trans_POS__c = 125 ;
		update cx1 ;
        
        list<Advocate_Activity_Achieved__c> aaaL = [
            SELECT ID
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( aaaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_THREE_BEE ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
		cx1.MTD_Pers_Trans_POS__c = 65 ;
		update cx1 ;
        
        list<Advocate_Activity_Achieved__c> aaaL = [
            SELECT ID
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( aaaL.size (), 1 ) ;
    }
    
	static testmethod void ADVOCATE_FOUR ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Advocate_Level__c aL = [
            SELECT ID
            FROM Advocate_Level__c
            WHERE Name = 'ABC234'
        ] ;
        
        Contact cx1 = cL.get ( 0 ) ;
        cx1.Advocate_Level__c = aL.ID ;
		update cx1 ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( abaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_FIVE ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
        cx1.Facebook_Handle__c = 'DOH!' ;
		update cx1 ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( abaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_SIX ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
        cx1.Personal_Trans_XDEP__c = 10 ;
        cx1.Personal_Savings_Accounts__c = 10 ;
		update cx1 ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( abaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_SEVEN ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
        cx1.Personal_Savings_Accounts__c = 10 ;
        cx1.MTD_Pers_Pop_Trans__c = 10.0 ;
		update cx1 ;
        
        list<Advocate_Activity_Achieved__c> aaaL = [
            SELECT ID
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( aaaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_EIGHT ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
        cx1.Personal_Savings_Accounts__c = 10 ;
        cx1.MTD_Pers_Remote_Dep_Count__c = 10.0 ;
		update cx1 ;
        
        list<Advocate_Activity_Achieved__c> aaaL = [
            SELECT ID
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( aaaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_NINE ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        cL.add ( c1 ) ;
        
        insert ( cL ) ;
        
        Contact cx1 = cL.get ( 0 ) ;
        cx1.Personal_Savings_Accounts__c = 10 ;
        cx1.MTD_Pers_Funds_Trans__c = 10.0 ;
		update cx1 ;
        
        list<Advocate_Activity_Achieved__c> aaaL = [
            SELECT ID
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( aaaL.size (), 0 ) ;
    }
    
	static testmethod void ADVOCATE_TEN ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        Contact c1 = getContact ( 1, a ) ;
        c1.Personal_Checking_Accounts__c = 1.0 ;
        insert c1 ;
        System.debug ( ':::: ' + c1.ID ) ;
        
        c1.Advocate__c = true ;
        update c1 ;
        
        Contact c1x = [ SELECT Advocate_Level__c FROM Contact WHERE ID = :c1.ID ] ;
        
        System.assertNotEquals ( c1x.Advocate_Level__c, null ) ;
    }
    
	static testmethod void ADVOCATE_QUIT ()
    {
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        Contact c1 = getContact ( 1, a ) ;
        c1.Personal_Checking_Accounts__c = 1.0 ;
        insert c1 ;
        
        Test.startTest () ;
        
		Advocate_Level__c al = [
            SELECT ID 
            FROM Advocate_Level__c 
            WHERE Name='Non Participant' 
            LIMIT 1 
        ] ;
            
	    c1.Advocate_Level__c = al.ID ;
        update c1 ;
        
        Test.stopTest () ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  De-Duplication tests
	//  --------------------------------------------------------------------------------------
	static testmethod void DEDUPE_ONE ()
    {
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
		//  First contact
		Contact cc1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
        insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactEmail ( 1, prospectRecordTypeID, a1 ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
        insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactEmail ( 1, prospectRecordTypeID, a1 ) ;
		cp12.title = 'Tester' ;
        insert cp12 ;
        
        // ?
        cc1.phone = '00' + 1 + '-00' + 1 + '-000' + 1 ;
		update cc1 ;
        
        cc1.phone = '00' + 1 + '-00' + 1 + '-001' + 1 ;
		update cc1 ;
        
        cc1.email = 'test2@user2.com' ;
        update cc1 ;
        
        cc1.DeDuplication_Date__c = Date.today () ;
        update cc1 ;
        
        cc1.DeDuplication_Date__c = Date.today ().addDays ( 1 ) ;
        update cc1 ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  SURVEY
	//  --------------------------------------------------------------------------------------
	static testmethod void CLOSE_ALL_SURVEY ()
    {
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Test.startTest() ;
        
		//  contact
		Contact cc1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
        cc1.Personal_Savings_Accounts__c = 1.0 ;
        insert cc1 ;
        
        // financial account
        Financial_Account__c fa1 = new Financial_Account__c () ;
        fa1.TAXRPTFORPERSNBR__c = cc1.ID ;
        fa1.CURRACCTSTATCD__c = 'ACT' ;
        fa1.NOTEBAL__c = 10.0 ;
        fa1.MJACCTTYPCD__c = 'SAV' ;
        
        insert fa1 ;
        
        fa1.CURRACCTSTATCD__c = 'CLS' ;
        fa1.NOTEBAL__c = 0.0 ;
        
        update fa1 ;
        
        cc1.Personal_Savings_Accounts__c = 0.0 ;
        cc1.Personal_Savings_CLS__c = 1.0 ;
        cc1.Most_Recent_Closed_Financial_Account__c = fa1.ID ;
        update cc1 ;
        
        Test.stopTest() ;
        
        Contact cc1x = [
            SELECT ID, Last_Survey_Sent__c, Sent_Closed_Account_Survey__c, Available_for_Survey__c
            FROM Contact
            WHERE ID = :cc1.ID
        ] ;
        
        System.assertNotEquals ( cc1x.Sent_Closed_Account_Survey__c, null ) ;
        
        list<Automated_Email__c> aeL = [
            SELECT ID
            FROM Automated_Email__c
            WHERE Contact__c = :cc1.ID 
        ] ;
        
        System.assertNotEquals ( aeL.size (), 0 ) ;
    }
    
    static testmethod void smsPhoneHeadache ()
    {
        Contact c = new Contact () ;
        c.FirstName = 'Test' ;
        c.LastName = 'User' ;
        c.MobilePhone = '(123) 456-7890' ;
        
        insert c ;
        
        c.MobilePhone = '890-567-1234' ;
        
        update c ;
    }
    
    static testmethod void OneTransaction ()
    {
        //  Setup
        list<OneCampaign__c> ocL = new list<OneCampaign__c> () ;
        
        OneCampaign__c oc1 = new OneCampaign__c () ;
        oc1.name = 'MC OneTransaction A Will' ;
        oc1.Active__c = true ;
        ocL.add ( oc1 ) ;
        
        OneCampaign__c oc2 = new OneCampaign__c () ;
        oc2.name = 'MC OneTransaction Owning a Business' ;
        oc2.Active__c = true ;
        ocL.add ( oc2 ) ;
        
        insert ocL ;
        
        Test.startTest () ;
        
        //  go #1
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test1' ;
        c1.LastName = 'User1' ;
        c1.MobilePhone = '(123) 456-7890' ;
        c1.Website_Form__c = 'OneTransaction' ;
        c1.Email = 'test1user1@testing.com' ;
        c1.OneTransaction_Focus__c = 'A Will' ;
        
        insert c1 ;
        
        c1.OneTransaction_Focus__c = 'Owning a Business' ;
        
        update c1 ;
        
        list<OneCampaign_Member__c> ocmL1 = [
            SELECT ID
            FROM OneCampaign_Member__c
            WHERE Contact__c = :c1.ID
        ] ;
        
        System.assertNotEquals ( ocmL1, null ) ;
        
        //  go #2
        Contact c2 = new Contact () ;
        c2.FirstName = 'Test2' ;
        c2.LastName = 'User2' ;
        c2.Email = 'test2user2@testing.com' ;
        c2.MobilePhone = '(321) 456-7890' ;
        
        insert c2 ;
        
        c2.Website_Form__c = 'OneTransaction' ;
        c2.OneTransaction_Focus__c = 'A Will' ;
        
        update c2 ;
        
        list<OneCampaign_Member__c> ocmL2 = [
            SELECT ID
            FROM OneCampaign_Member__c
            WHERE Contact__c = :c2.ID
        ] ;
        
        System.assertNotEquals ( ocmL2, null ) ;
        
        Test.stopTest () ;
    }
}