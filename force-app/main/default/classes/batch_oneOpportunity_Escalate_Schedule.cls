global class batch_oneOpportunity_Escalate_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_oneOpportunity_Escalate b = new batch_oneOpportunity_Escalate () ;
		Database.executeBatch ( b ) ;
   }
}