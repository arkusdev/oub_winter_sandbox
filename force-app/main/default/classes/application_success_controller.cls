public with sharing class application_success_controller {

	public String typeParameter{get;set;}
	public String appId{get;set;}
	public String appField{set;get;}
	public one_application__c app{get;set;}
	
	public boolean rerenderOne{set;get;}
	public boolean rerenderTwo{set;get;}
	public boolean rerenderThree{set;get;}
	public boolean rerenderFour{set;get;}
	public boolean rerenderFive{set;get;}
	public boolean rerenderSix{set;get;}
    
	public boolean rerenderAppId{set;get;}

	public boolean allowSendEmail{get;set;}
	public boolean allowSendEmailCoAppl{get;set;}
    
	public String entCnt_email{get;set;}
	public String refCnt_email{get;set;}
	
	private Double trialLimit ;
	
    //  DisclosureURL
    public String ratesURLDisclosure { public get ; private set ; }
    
 	//  QR Code to allow site transfer to mobile device
 	public String qrCode 
 	{ 
 		get; 
 		private set; 
 	}
 	
	//  Additional language based on decision code
    public String codeAdditionalLanguage
    {
        get ;
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
    //  Link to additional pages
    public PageReference nextPageLink
    {
    	get ;
    	private set ;
    }
    
    // Rakuten tracking pixel
    public String rakutenTrackingPixel
    {
    	get ; 
    	private set ;
    }
    
    //  Live Agent
    public String liveAgentButton
    {
    	get ; 
    	private set ;
    }
    
    //  Cart checkout
    public boolean displayCartCheckout { public get ; private set ; }
    public String cartCheckoutJSON { public get ; private set ; }
    public string recordUserJSON { public get; private set; }
    
	public application_success_controller()
	{
		//  --------------------------------------------------------------------------------------------------
		//  JIM cart checkout for google tag tracking
		//  --------------------------------------------------------------------------------------------------
		displayCartCheckout = false ;
        Cookie jCookie = ApexPages.currentPage().getCookies().get ( 'j_Cookie' ) ;
        
		//  --------------------------------------------------------------------------------------------------
		//  Live Agent Button
		//  --------------------------------------------------------------------------------------------------
		if ( ( one_settings__c.getInstance('settings').Live_Agent_Enable__c != null ) &&
             ( one_settings__c.getInstance('settings').Live_Agent_Enable__c == true ) )
        {
			liveAgentButton = one_settings__c.getInstance('settings').Live_Agent_Button_ID__c ;
        }
        else
        {
			liveAgentButton = null ;
        }

        //  Default rate disclosure
    	ratesURLDisclosure = one_utils.getDisclosureURL () ;
		
		trialLimit = one_settings__c.getInstance('settings').ACH_Validation_Threshold__c ;
		
		entCnt_email = null;
		refCnt_email = null;
		
		allowSendEmail = false;
		allowSendEmailCoAppl = false;
		
		app = null;

		system.debug('---- ' + Apexpages.currentPage().getHeaders().get('Referer'));
		system.debug('---- ' + (Apexpages.currentPage().getHeaders().get('Referer') + '').toLowerCase().endsWith('one_application_end'));
		
		rerenderOne = false;
		rerenderTwo = false;
		rerenderThree = false ;
		rerenderFour = false ;
        rerenderFive = false ;
        rerenderSix = false ;
        
		rerenderAppId = false;
		if(system.currentPageReference().getParameters().get('type') != null){	
			typeParameter = System.currentPageReference().getParameters().get('type');
		}
		if(system.currentPageReference().getParameters().get('aid') != null){	
			appId = System.currentPageReference().getParameters().get('aid');
		}
		if(appId != null && appId != ''){
			List<one_application__c> appAuxs = new List<one_application__c>();
			try{
				appAuxs = [
                    SELECT ID, Name, FIS_Application_ID__c, FIS_Decision_Code__c, FIS_Credit_Limit__c, Application_ID__c,RequestedCreditLimit__c,
                    First_Name__c, Last_Name__c, Email_Address__c, DOB__c, Phone__c, City__c, State__c, Zip_Code__c,
                    Co_Applicant_First_Name__c, Co_Applicant_Last_Name__c, Co_Applicant_Email_Address__c, 
                    Entered_By_Contact__c, Entered_By_Contact__r.Email, 
                    Referred_By_Contact__c, Referred_By_Contact__r.Email,
                    Reservation_Vendor_ID__c, Reservation_Vendor_ID__r.Name,  Reservation_Vendor_ID__r.Tracking_Pixel__c,
                    Reservation_Affiliate_Vendor__c, Reservation_Affiliate_Vendor__r.Name,  Reservation_Affiliate_Vendor__r.Tracking_Pixel__c,
                    Product__c, SubProduct__c, Plan__c, Upload_Attachment_key__c, Application_Status__c, Funding_Options__c,
                    RecordTypeId
                    FROM one_application__c 
                    WHERE id = : appId 
                    LIMIT 1
				];
			}
			catch(Exception e)
			{appAuxs = new List<one_application__c>();}
			
			if(appAuxs.size() > 0){
				app = appAuxs[0];
				if(app.Entered_By_Contact__c != null){
					entCnt_email = app.Entered_By_Contact__r.Email;
				}			
				if(app.Referred_By_Contact__c != null){
					refCnt_email = app.Referred_By_Contact__r.Email;
				}			
			}else{
				app = null;
			}
			if(app != null){
                //  --------------------------------------------------------------------------------------------------
                //  JIM user for google tag tracking
                //  --------------------------------------------------------------------------------------------------
                recordUserJSON = one_utils.generateUserJSON ( app ) ;
                
                //  --------------------------------------------------------------------------------------------------
                //  JIM cart checkout for google tag tracking
                //  --------------------------------------------------------------------------------------------------
                if ( jCookie != null )
                {
                    String jFoo = jCookie.getValue() ;
                    
                    if ( jFoo.equalsIgnoreCase ( app.ID ) )
                        displayCartCheckout = true ;
                    
                    //  Remove the cookie
                    Cookie jNYETCookie = new Cookie ( 'j_Cookie', '', null, 0, false ) ;
                    ApexPages.currentPage().setCookies ( new Cookie[] { jNYETCookie } ) ;
                }
                
                if ( displayCartCheckout )
                    cartCheckoutJSON = one_utils.generateCartCheckoutJSON ( app ) ;
                
                System.debug ( '========== Override Rate Disclosure ==========' ) ;
                if ( String.isNotBlank ( app.Product__c ) && String.isNotBlank ( app.SubProduct__c ) && String.isNotBlank ( app.Plan__c ) )
                    ratesURLDisclosure = one_utils.getDisclosureURL ( app.Product__c, app.SubProduct__c, app.Plan__c ) ;
                
				appField = app.Application_ID__c ;
				rerenderAppId = true;

				if((Apexpages.currentPage().getHeaders().get('Referer') + '').toLowerCase().endsWith('application_end')
					|| (Apexpages.currentPage().getHeaders().get('Referer') + '').toLowerCase().endsWith('application_start')
				){	
					//if(app.FIS_Application_ID__c != null && app.FIS_Application_ID__c != ''){
						if(app.Email_Address__c != null && app.Email_Address__c != ''){
							allowSendEmail = true;
						}
						if(app.Co_Applicant_Email_Address__c != null && app.Co_Applicant_Email_Address__c != ''){
							allowSendEmailCoAppl = true;
						}
					//}
				}
				
				//  Pull out tracking pixel
				Account a1 = app.Reservation_Vendor_ID__r ;
				Account a2 = app.Reservation_Affiliate_Vendor__r ;
				
				if ( ( a1 != null ) && ( a1.Tracking_pixel__c != null ) )
				{
					if ( a1.Tracking_Pixel__c )
					{
						one_referral oneR = new one_referral () ;
						oneR.loadThirdPartyData ( app.id ) ;
						rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( app.Name ) ;
					}
				}
				else if ( ( a2 != null ) && ( a2.Tracking_pixel__c != null ) )
				{
					if ( a2.Tracking_Pixel__c )
					{
						one_referral oneR = new one_referral () ;
						oneR.loadThirdPartyData ( app.id ) ;
						rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( app.Name ) ;
					}
				}
                
                //  --------------------------------------------------------------------------------------------------
                //  QUIZ override?!?
                //  --------------------------------------------------------------------------------------------------
                if ( one_utils.codesQuizRequired.containsKey ( app.FIS_Decision_Code__c ) )
                    typeParameter = 'QUIZ' ;
                
                //  --------------------------------------------------------------------------------------------------
                //  IDV override?!?
                //  --------------------------------------------------------------------------------------------------
                if ( one_utils.codesAdditionalInfoMap.containsKey ( app.FIS_Decision_Code__c ) )
                    typeParameter = 'IDV' ;
                
                //  --------------------------------------------------------------------------------------------------
                //  approved override
                //  --------------------------------------------------------------------------------------------------
                if ( ( String.isNotBlank ( app.FIS_Decision_Code__c ) && ( ( app.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) || ( app.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) ) ) )
                    typeParameter = app.FIS_Application_ID__c ;
			}
		}
		
		if(typeParameter != null)
		{
			if(appId != null && appId != '')
			{
				System.debug ( app.FIS_Application_ID__c + ' ==> ' + typeParameter ) ;
				System.debug ( app.FIS_Credit_Limit__c + ' ==> ' + trialLimit ) ;
				
				//  Check for Additional Information
				if ( ( String.isNotBlank ( app.FIS_Decision_Code__c ) ) && ( app.FIS_Decision_Code__c.equalsIgnoreCase ( 'P950' ) ) )
                {
					system.debug ( '----> Pending, Credit report' ) ;
					rerenderOne = false ;
					rerenderTwo = false ;
					rerenderThree = false ;
					rerenderFour = false ;
			        rerenderFive = false ;
                    rerenderSix = true ;
                }
				else if ( typeParameter.equalsIgnoreCase ( 'IDV' ) )
				{
					system.debug ( '----> Additional Info' ) ;
					rerenderOne = false ;
					rerenderTwo = false ;
					rerenderThree = true ;
					rerenderFour = false ;
			        rerenderFive = false ;
                    rerenderSix = false ;

                    // QR code for display
					qrCode = one_utils.getDocUploadQRCode ( appId, app.Upload_Attachment_key__c, 125, 125 ) ;

					//  Get the additional language needed
					codeAdditionalLanguage = one_utils.codesAdditionalLanguage.get ( app.FIS_Decision_Code__c ) ;
					
					//  Get the list of documents to display - assumes at this point they haven't uploaded any!
					docTypeList = one_utils.getDocTypeList ( app.FIS_Decision_Code__c ) ;
					
					//  Set up the page reference used for the link
		    		PageReference pr = Page.application_file_upload ;
			        pr.getParameters().put ('aid', appId ) ;
			        pr.getParameters().put ('kid', app.Upload_Attachment_key__c ) ;
		    		
		    		nextPageLink = pr ;
				}
				//  Check for over the trial limit
				else if ( ( app.FIS_Application_ID__c == typeParameter ) && ( app.FIS_Credit_Limit__c >= trialLimit ) && 
                          ( String.isNotBlank ( app.Funding_Options__c ) && ( app.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) ) )
				{
					system.debug ( '----> Trial deposit' ) ;
					rerenderOne = false;
					rerenderTwo = false;
					rerenderThree = false ;
					rerenderFour = true ;
			        rerenderFive = false ;
                    rerenderSix = false ;
				}
                else if ( app.application_status__c.equalsIgnoreCase ( 'declined' ) )
                {
					system.debug ( '----> DEcline' ) ;
					rerenderOne = false;
					rerenderTwo = false;
					rerenderThree = false ;
					rerenderFour = false ;
					rerenderFive = true ;
                    rerenderSix = false ;
                }
				//  Original logic here
				else
				{
					system.debug ( '----> Original Logic' ) ;
					rerenderOne = (app.FIS_Application_ID__c == typeParameter ? false : true);
					rerenderTwo = (app.FIS_Application_ID__c == typeParameter ? true : false);
					rerenderThree = false ;
					rerenderFour = false ;
			        rerenderFive = false ;
                    rerenderSix = false ;
				}
			}
		}

		entCnt_email = null;
		refCnt_email = null;		
		allowSendEmail = false;
		allowSendEmailCoAppl = false;

		system.debug('@@ appField '+ appField);
		
		//  A hacking we will go! - to show a specific panel, uncomment the below
		//  rerenderOne = true ;
		//  rerenderTwo = true ;
		//  rerenderThree = true ;
		//  rerenderFour = true ;
		//  rerenderFive = true ;
		//  rerenderSix = false ;
	}
	
	public void removeEmailScript()
	{
		allowSendEmail = false;
		allowSendEmailCoAppl = false;
		entCnt_email = '';
		refCnt_email = '';
	}
	
}