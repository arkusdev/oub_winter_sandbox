@isTest
public class TestLoanApplicationTrigger 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Individuals' ;
        
		return a ;
	}
	
	private static Account getBusinessAccount ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Busineses' ;
        
		return a ;
	}
    
    /* -----------------------------------------------------------------------------------
	 * Load and return a account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccountX (Integer i)
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - ' + i + 'A' ;
        
		return a ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		return b ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Branch__c b = getBranch () ;
        insert b ;
		
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x + 'T' ;
		c.LastName = 'Applicant' + x + 'A' ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		c.Branch__c = b.ID ;
		
		return c ;
	}

    /* -----------------------------------------------------------------------------------
	 * Load and return a financial account
	 * ----------------------------------------------------------------------------------- */
	private static Financial_Account__c getFinancialAccount ( Contact c, String acctnbr )
	{
		Branch__c b = getBranch () ;
        insert b ;
		
		String lRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan', 'Financial_Account__c' ) ;

        Financial_Account__c f = new Financial_Account__c () ;
		f.TAXRPTFORPERSNBR__c = c.Id ;
		f.ACCTNBR__c = acctnbr ;
		f.MJACCTTYPCD__c = 'MTG' ;
		f.CURRMIACCTTYPCD__c = 'QB01' ;
		f.OPENDATE__c = Datetime.now ().date () ;
		f.BRANCHORGNBR__c = b.ID ;
        f.RecordTypeId = lRT ;
		
		return f ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return an (OUB) Opportunity
	 * ----------------------------------------------------------------------------------- */
	private static oneOpportunity__c getOpportunity ( Contact c )
	{
		Branch__c b = getBranch () ;
        insert b ;
		
		String xRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;

        oneOpportunity__c oo = new oneOpportunity__c () ;
		oo.RecordTypeId = xRT ;
		oo.First_Name__c = c.FirstName + ' ' + c.LastName ;
        oo.Email__c = c.Email ;
		oo.Status__c = 'Open' ;
		oo.Interested_in_Product__c = 'Single Family Home' ;
		oo.Home_Phone__c = '123-456-7890' ;
		oo.Branch__c = b.ID ;
		
		return oo ;
	}
	
	/* --------------------------------------------------------------------------------
	 * ONE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findContactBothEmail ()
    {
        Account a1 = getAccount () ;
        insert a1 ;
        Account a2 = getBusinessAccount () ;
        insert a2 ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Borrower_Contact__c, Co_Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Borrower_Contact__c, c1.ID ) ;
        System.assertEquals ( lax.Co_Borrower_Contact__c, c2.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * ONE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findContactBothTaxID ()
    {
        Account a1 = getAccount () ;
        a1.FederalTaxID__c = '999000111';
        insert a1 ;
        
        Account a2 = getBusinessAccount () ;
        insert a2 ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.TaxID__c = '111223333' ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        c2.TaxID__c = '333221111' ;
        insert c2 ;
        
        Contact c3 = getContact ( 3 ) ;
        c3.TaxID__c = '333221113' ;
        insert c3 ;
        
        Contact c4 = getContact ( 4 ) ;
        c4.TaxID__c = '333221114' ;
        insert c4 ;
        
        Contact c5 = getContact ( 5 ) ;
        c5.TaxID__c = '333221115' ;
        insert c5 ;
        
        Contact c6 = getContact ( 6 ) ;
        c6.TaxID__c = '333221116' ;
        insert c6 ;
        
        Contact c7 = getContact ( 7 ) ;
        c7.TaxID__c = '333221117' ;
        insert c7 ;
        
        Contact c8 = getContact ( 8 ) ;
        c8.TaxID__c = '333221118' ;
        insert c8 ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        la.Organization_EIN__c  = a1.FederalTaxID__c ;
        la.Borrower_Tax_ID__c = c1.TaxID__c ;
        
        la.Co_Borrower_Tax_ID__c = c2.TaxID__c ;
        la.PriTaxID__c = c3.TaxID__c ;
        la.Own1TaxID__c = c4.TaxID__c ;
        la.Own2TaxID__c = c5.TaxID__c ;
        la.Own3TaxID__c = c6.TaxID__c ;
        la.Own4TaxID__c = c7.TaxID__c ;
        la.Own5TaxID__c = c8.TaxID__c ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Borrower_Contact__c, Co_Borrower_Contact__c,Organization__c,Primary_Person__c,Owner_1_Person__c,
                   Owner_2_Person__c,Owner_3_Person__c,Owner_4_Person__c,Owner_5_Person__c,
                   Primary_Organization__c,Owner_1_Organization__c,
                   Owner_2_Organization__c,Owner_3_Organization__c,Owner_4_Organization__c,Owner_5_Organization__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Organization__c , a1.ID ) ;
        System.assertEquals ( lax.Borrower_Contact__c, c1.ID ) ;
        System.assertEquals ( lax.Co_Borrower_Contact__c, c2.ID ) ;
        System.assertEquals ( lax.Primary_Person__c, c3.ID ) ;
        System.assertEquals ( lax.Owner_1_Person__c, c4.ID ) ;
        System.assertEquals ( lax.Owner_2_Person__c, c5.ID ) ;
        System.assertEquals ( lax.Owner_3_Person__c, c6.ID ) ;
        System.assertEquals ( lax.Owner_4_Person__c, c7.ID ) ;
        System.assertEquals ( lax.Owner_5_Person__c, c8.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * ONE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findAccountsTaxID ()
    {
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Account a = getAccount () ;
        a.FederalTaxID__c = '999000111';
        insert a ;
		
        Account a2 = getAccountX ( 2 ) ;
        a2.FederalTaxID__c = '999000112';
        insert a2 ;
        
        Account a3 = getAccountX ( 3 ) ;
        a3.FederalTaxID__c = '999000113';
        insert a3 ;
		
        Account a4 = getAccountX ( 4 ) ;
        a4.FederalTaxID__c = '999000114';
        insert a4 ;
		
        Account a5 = getAccountX ( 5 ) ;
        a5.FederalTaxID__c = '999000115';
        insert a5 ;
		
        Account a6 = getAccountX ( 6 ) ;
        a6.FederalTaxID__c = '999000116';
        insert a6 ;
		
        Account a7 = getAccountX ( 7 ) ;
        a7.FederalTaxID__c = '999000117';
        insert a7 ;
		

        
        Contact c1 = getContact ( 1 ) ;
        c1.TaxID__c = '111223333' ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        c2.TaxID__c = '333221111' ;
        insert c2 ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        la.Organization_EIN__c  = a.FederalTaxID__c ;
        la.Borrower_Tax_ID__c = c1.TaxID__c ;
        
        la.Co_Borrower_Tax_ID__c = c2.TaxID__c ;
        
        //Set Accounts
        la.PriTaxID__c = a2.FederalTaxID__c ;
        la.Own1TaxID__c = a3.FederalTaxID__c ;
        la.Own2TaxID__c = a4.FederalTaxID__c ;
        la.Own3TaxID__c = a5.FederalTaxID__c ;
        la.Own4TaxID__c = a6.FederalTaxID__c ;
        la.Own5TaxID__c = a7.FederalTaxID__c ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Borrower_Contact__c, Co_Borrower_Contact__c,Organization__c,Primary_Person__c,Owner_1_Person__c,
                   Owner_2_Person__c,Owner_3_Person__c,Owner_4_Person__c,Owner_5_Person__c,
                   Primary_Organization__c,Owner_1_Organization__c,
                   Owner_2_Organization__c,Owner_3_Organization__c,Owner_4_Organization__c,Owner_5_Organization__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Organization__c , a.ID ) ;
        System.assertEquals ( lax.Borrower_Contact__c, c1.ID ) ;
        System.assertEquals ( lax.Co_Borrower_Contact__c, c2.ID ) ;
        System.assertEquals ( lax.Primary_Organization__c, a2.ID ) ;
        System.assertEquals ( lax.Owner_1_Organization__c, a3.ID ) ;
        System.assertEquals ( lax.Owner_2_Organization__c, a4.ID ) ;
        System.assertEquals ( lax.Owner_3_Organization__c, a5.ID ) ;
        System.assertEquals ( lax.Owner_4_Organization__c, a6.ID ) ;
        System.assertEquals ( lax.Owner_5_Organization__c, a7.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * TWO
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findFinancialAccount ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        Financial_Account__c fa1 = getFinancialAccount ( c1, '1112223333' ) ;
        insert fa1 ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        la.Application_Number__c = fa1.ACCTNBR__c ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        la.Borrower_Contact__c = c1.ID ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        la.Co_Borrower_Contact__c = c2.ID ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Financial_Account__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Financial_Account__c, fa1.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * TWO
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findFinancialAccount2 ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        Financial_Account__c fa1 = getFinancialAccount ( c1, '1112223333' ) ;
        insert fa1 ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        la.InsightAccount__c = fa1.ACCTNBR__c ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        la.Borrower_Contact__c = c1.ID ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        la.Co_Borrower_Contact__c = c2.ID ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Financial_Account__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Financial_Account__c, fa1.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * THREE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findOpportunityByEmail ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        oneOpportunity__c oo = getOpportunity ( c1 ) ;
        insert oo ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        la.Borrower_Contact__c = c1.ID ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        la.Co_Borrower_Contact__c = c2.ID ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Opportunity__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Opportunity__c, oo.ID ) ;
        
        oneOpportunity__c ooX = [
            SELECT ID, Loan_Application__c
            FROM oneOpportunity__c
            WHERE ID = :lax.Opportunity__c
            LIMIT 1
        ] ;
        
        System.assertEquals ( ooX.Loan_Application__c, lax.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * THREE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void findOpportunityByContact ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        oneOpportunity__c oo = getOpportunity ( c1 ) ;
        oo.Email__c = 'otheremail@test.com' ;
        oo.Opportunity_Contact__c = c1.ID ;
        insert oo ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        la.Borrower_Contact__c = c1.ID ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        la.Co_Borrower_Contact__c = c2.ID ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Opportunity__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertEquals ( lax.Opportunity__c, oo.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * FOUR
	 * -------------------------------------------------------------------------------- */
	public static testmethod void bunchOCrapOpportunity ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        
        Contact c2 = getContact ( 2 ) ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        
        la.Borrower_Phone__c = '123-456-7890' ;
        la.Loan_Purpose__c = 'Refinance' ;
        la.Loan_Status__c = 'Open' ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Borrower_Contact__c, Co_Borrower_Contact__c, Opportunity__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertNotEquals ( lax.Borrower_Contact__c, null ) ;
        System.assertNotEquals ( lax.Co_Borrower_Contact__c, null ) ;
        System.assertNotEquals ( lax.Opportunity__c, null ) ;
        System.assertNotEquals ( lax.Borrower_Contact__c, lax.Co_Borrower_Contact__c ) ;
        
        oneOpportunity__c oo1 = [
            SELECT ID, Status__c
            FROM oneOpportunity__c
            WHERE ID = :lax.Opportunity__c
            LIMIT 1 
        ] ;
        
        System.assertEquals ( oo1.Status__c, 'Open' ) ;
        
        la.Application_Received__c = true ;
        update la ;
        
        oneOpportunity__c oo2 = [
            SELECT ID, Status__c
            FROM oneOpportunity__c
            WHERE ID = :lax.Opportunity__c
            LIMIT 1 
        ] ;
        
        System.assertEquals ( oo2.Status__c, 'Application Received' ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * GARBAGE ONE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void garbageOpportunityByGarbageContact ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.Email = 'none@none.com' ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        c2.Email = 'none@none.com' ;
        insert c2 ;
        
        oneOpportunity__c oo = getOpportunity ( c1 ) ;
        oo.Email__c = 'none@none.com' ;
        insert oo ;
        
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        la.Borrower_Contact__c = c1.ID ;
        
        la.Co_Borrower_First_Name__c = c2.FirstName ;
        la.Co_Borrower_Last_Name__c = c2.LastName ;
        la.Co_Borrower_Email__c = c2.Email ;
        la.Co_Borrower_Contact__c = c2.ID ;
        
        insert la ;
        
        Loan_Application__c lax = [
            SELECT ID, Opportunity__c
            FROM Loan_Application__c
            WHERE ID = :la.ID
            LIMIT 1
        ] ;
        
        System.assertNotEquals ( lax.Opportunity__c, oo.ID ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * JIM DOUBLING - ONE
	 * -------------------------------------------------------------------------------- */
	public static testmethod void doubleLoanApplicationTestONE ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        
        list<Loan_Application__c> laL = new list<Loan_Application__c> () ;
        
        Loan_Application__c la1 = new Loan_Application__c () ;
        la1.Application_Number__c = '123456' ;
        la1.Loan_Amount__c = 10000.00 ;
        
        la1.Borrower_First_Name__c = c1.FirstName ;
        la1.Borrower_Last_Name__c = c1.LastName ;
        la1.Borrower_Email__c = c1.Email ;
        
        laL.add ( la1 ) ;
        
        Loan_Application__c la2 = new Loan_Application__c () ;
        la2.Application_Number__c = '123458' ;
        la2.Loan_Amount__c = 10000.00 ;
        
        la2.Borrower_First_Name__c = c1.FirstName ;
        la2.Borrower_Last_Name__c = c1.LastName ;
        la2.Borrower_Email__c = c1.Email ;
        
        laL.add ( la2 ) ;
        
        insert laL ;
        
        Loan_Application__c xla1 = [
            SELECT ID, Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :laL [ 0 ].ID
            LIMIT 1
        ] ;
        
        Loan_Application__c xla2 = [
            SELECT ID, Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :laL [ 1 ].ID
            LIMIT 1
        ] ;
        
        System.assertNotEquals ( xla1.Borrower_Contact__c, null ) ;
        System.assertNotEquals ( xla2.Borrower_Contact__c, null ) ;
        System.assertEquals ( xla1.Borrower_Contact__c, xla2.Borrower_Contact__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * JIM DOUBLING - TWO
	 * -------------------------------------------------------------------------------- */
	public static testmethod void doubleLoanApplicationTestTWO ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        
        list<Loan_Application__c> laL = new list<Loan_Application__c> () ;
        
        Loan_Application__c la1 = new Loan_Application__c () ;
        la1.Application_Number__c = '123456' ;
        la1.Loan_Amount__c = 10000.00 ;
        
        la1.Borrower_First_Name__c = c1.FirstName ;
        la1.Borrower_Last_Name__c = c1.LastName ;
        la1.Borrower_Email__c = c1.Email ;
        la1.Borrower_Contact__c = c1.ID ;
        
        laL.add ( la1 ) ;
        
        Loan_Application__c la2 = new Loan_Application__c () ;
        la2.Application_Number__c = '123458' ;
        la2.Loan_Amount__c = 10000.00 ;
        
        la2.Borrower_First_Name__c = c1.FirstName ;
        la2.Borrower_Last_Name__c = c1.LastName ;
        la2.Borrower_Email__c = c1.Email ;
        
        laL.add ( la2 ) ;
        
        insert laL ;
        
        Loan_Application__c xla1 = [
            SELECT ID, Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :laL [ 0 ].ID
            LIMIT 1
        ] ;
        
        Loan_Application__c xla2 = [
            SELECT ID, Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :laL [ 1 ].ID
            LIMIT 1
        ] ;
        
        System.assertNotEquals ( xla1.Borrower_Contact__c, null ) ;
        System.assertNotEquals ( xla2.Borrower_Contact__c, null ) ;
        System.assertEquals ( xla1.Borrower_Contact__c, xla2.Borrower_Contact__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Single New Contact ?
	 * -------------------------------------------------------------------------------- */
	public static testmethod void singleLoanApplicationTestONE ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        Contact c2 = getContact ( 2 ) ;
        
        // # 1
        
        Loan_Application__c la1 = new Loan_Application__c () ;
        la1.Mortgagebot_Web_ID__c = '123456' ; //  Upsert ID
        la1.Application_Number__c = '123456' ;
        la1.Loan_Amount__c = 10000.00 ;
        
        la1.Borrower_First_Name__c = c1.FirstName ;
        la1.Borrower_Last_Name__c = c1.LastName ;
        la1.Borrower_Email__c = c1.Email ;
        
        la1.Co_Borrower_First_Name__c = c2.FirstName ;
        la1.Co_Borrower_Last_Name__c = c2.LastName ;
        la1.Co_Borrower_Email__c = c2.Email ;
        
        upsert la1 ;
        
        Loan_Application__c xla1 = [
            SELECT ID, Borrower_Contact__c, Co_Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :la1.ID
            LIMIT 1
        ] ;
        
        System.assertNotEquals ( xla1.Borrower_Contact__c, null ) ;
        System.assertNotEquals ( xla1.Co_Borrower_Contact__c, null ) ;
        
        // # 2

        Loan_Application__c la2 = new Loan_Application__c () ;
        la2.Mortgagebot_Web_ID__c = '123456' ; //  Upsert ID
        la2.Application_Number__c = '123456' ;
        la2.Loan_Amount__c = 10000.00 ;
        
        la2.Borrower_First_Name__c = c1.FirstName ;
        la2.Borrower_Last_Name__c = c1.LastName ;
        la2.Borrower_Email__c = c1.Email ;
        
        la2.Co_Borrower_First_Name__c = c2.FirstName ;
        la2.Co_Borrower_Last_Name__c = c2.LastName ;
        la2.Co_Borrower_Email__c = c2.Email ;
        
        upsert la2 ;
        
        Loan_Application__c xla2 = [
            SELECT ID, Borrower_Contact__c, Co_Borrower_Contact__c
            FROM Loan_Application__c
            WHERE ID = :la2.ID
            LIMIT 1
        ] ;
        
        System.assertNotEquals ( xla2.Borrower_Contact__c, null ) ;
        System.assertNotEquals ( xla2.Co_Borrower_Contact__c, null ) ;
        
//        System.assertEquals ( xla2.ID, xla1.ID ) ;
        System.assertEquals ( xla2.Borrower_Contact__c, xla1.Borrower_Contact__c ) ;
        System.assertEquals ( xla2.Co_Borrower_Contact__c, xla1.Co_Borrower_Contact__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * EI-EI-O 1
	 * -------------------------------------------------------------------------------- */
	public static testmethod void loanApplicationTestEIN1 ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.Email = 'none@none.com' ;
        c1.TaxID__c = '111223333' ;
        insert c1 ;
        
        Loan_Application__c la1 = new Loan_Application__c () ;
        la1.Mortgagebot_Web_ID__c = '123456' ; //  Upsert ID
        la1.Application_Number__c = '123456' ;
        la1.Loan_Amount__c = 10000.00 ;
        
        la1.Organization_EIN__c = c1.TaxID__c ;
        la1.Organization_Email__c = c1.Email ;
        la1.PriFirstName__c = c1.FirstName ;
        la1.PriLastName__c = c1.LastName ;
        la1.PriTaxID__c = c1.TaxID__c ;
        
        insert la1 ;
        
        Loan_Application__c xLa1x = [
            SELECT ID, Primary_Person__c, Organization__c, Primary_Organization__c
            FROM Loan_Application__c
            WHERE ID = :la1.ID
        ] ;

        System.assertEquals ( xLa1x.Primary_Person__c, c1.ID ) ;
        System.assertEquals ( xLa1x.Primary_Organization__c, xLa1x.Organization__c ) ;
    }
        
	/* --------------------------------------------------------------------------------
	 * EI-EI-O 2
	 * -------------------------------------------------------------------------------- */
	public static testmethod void loanApplicationTestEIN2 ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.Email = 'none@none.com' ;
        c1.TaxID__c = '111223333' ;
        insert c1 ;
        
        Loan_Application__c la2 = new Loan_Application__c () ;
        la2.RecordTypeId = LoanApplicationHelper.getPPPRecordTypeID () ;
        la2.Status__c = 'Processing' ;
        la2.Mortgagebot_Web_ID__c = '123456' ; //  Upsert ID
        la2.Application_Number__c = '123456' ;
        la2.Loan_Amount__c = 10000.00 ;
        
        la2.Organization_EIN__c = '222113333' ;
        la2.Organization_Name__c = 'EI EI OH ORG' ;
        la2.Organization_Email__c = 'eiei@oh.com' ;
        
        la2.PriFullName__c = 'Foo Bar' ;
        la2.PriAddress1__c = 'Yo Noid' ;
        la2.Primary_City__c = 'Zombieland' ;
        la2.Primary_State__c = 'KY' ;
        la2.Primary_Zip_Code__c = '69069' ;
        la2.PriTaxID__c = '333114444' ;
        
        insert la2 ;
        
        Loan_Application__c xLa2x = [
            SELECT ID, Primary_Person__c, Organization__c, Primary_Organization__c
            FROM Loan_Application__c
            WHERE ID = :la2.ID
        ] ;

        System.assertNotEquals ( xLa2x.Primary_Person__c, null ) ;
        System.assertNotEquals ( xLa2x.Organization__c, null ) ;
        System.assertNotEquals ( xLa2x.Primary_Organization__c, null ) ;
        System.assertNotEquals ( xLa2x.Organization__c, xLa2x.Primary_Organization__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * EI-EI-O 3
	 * -------------------------------------------------------------------------------- */
	public static testmethod void loanApplicationTestEIN3 ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Loan_Application__c la2 = new Loan_Application__c () ;
        la2.RecordTypeId = LoanApplicationHelper.getPPPRecordTypeID () ;
        la2.Status__c = 'Processing' ;
        la2.Mortgagebot_Web_ID__c = '123456' ; //  Upsert ID
        la2.Application_Number__c = '123456' ;
        la2.Loan_Amount__c = 10000.00 ;
        
        la2.Organization_EIN__c = '222113333' ;
        la2.Organization_Name__c = 'EI EI OH ORG' ;
        la2.Organization_Email__c = 'eiei@oh.com' ;
        
        la2.PriFullName__c = 'Foo Bar' ;
        la2.PriAddress1__c = 'Yo Noid' ;
        la2.Primary_City__c = 'Zombieland' ;
        la2.Primary_State__c = 'KY' ;
        la2.Primary_Zip_Code__c = '69069' ;
        la2.PriTaxID__c = la2.Organization_EIN__c ;
        
        la2.Own1FullName__c = 'Bar Foo' ;
        la2.Own1Address1__c = 'Nyet' ;
        la2.Own1City__c = 'Non' ;
        la2.Own1State__c = 'ME' ;
        la2.Own1Zip__c = '96096' ;
        la2.Own1TaxID__c = '321654798' ;
        
        insert la2 ;
        
        Loan_Application__c xLa2x = [
            SELECT ID, Primary_Person__c, Organization__c, Primary_Organization__c, Own1PersonID__c
            FROM Loan_Application__c
            WHERE ID = :la2.ID
        ] ;

        System.assertNotEquals ( xLa2x.Primary_Person__c, null ) ;
        System.assertNotEquals ( xLa2x.Own1PersonID__c, null ) ;
        System.assertNotEquals ( xLa2x.Primary_Person__c, xLa2x.Own1PersonID__c ) ;
        
        System.assertNotEquals ( xLa2x.Organization__c, null ) ;
        System.assertNotEquals ( xLa2x.Primary_Organization__c, null ) ;
        System.assertEquals ( xLa2x.Organization__c, xLa2x.Primary_Organization__c ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * EI-EI-O 4
	 * -------------------------------------------------------------------------------- */
	public static testmethod void loanApplicationTestEIN4 ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Loan_Application__c la2 = new Loan_Application__c () ;
        la2.RecordTypeId = LoanApplicationHelper.getPPPRecordTypeID () ;
        la2.Status__c = 'Processing' ;
        la2.Mortgagebot_Web_ID__c = '123456' ; //  Upsert ID
        la2.Application_Number__c = '123456' ;
        la2.Loan_Amount__c = 10000.00 ;

		la2.Organization_Name__c = 'Test org' ;
        la2.Organization_Email__c = 'foo@bar.com' ;
        la2.Organization_EIN__c = '465325795' ;
        
        la2.Organization_Street__c = '100 franklin street' ;
        la2.Organization_City__c = 'Boston' ;
        la2.Organization_State__c = 'MA' ;
        la2.Organization_Zip_Code__c = '02201' ;
        
        la2.PriFullName__c = 'Test person' ;
        la2.PriTaxID__c = '465325795' ;
        
        insert la2 ;
        
        Loan_Application__c xLa2x = [
            SELECT ID, Primary_Person__c, Organization__c, Primary_Organization__c
            FROM Loan_Application__c
            WHERE ID = :la2.ID
        ] ;

        System.assertNotEquals ( xLa2x.Organization__c, null ) ;
        System.assertNotEquals ( xLa2x.Primary_Organization__c, null ) ;
        System.assertNotEquals ( xLa2x.Primary_Person__c, null ) ;
    }
    
	/* --------------------------------------------------------------------------------
	 * EI-EI-O 4
	 * -------------------------------------------------------------------------------- */
	public static testmethod void loanApplicationTestEIN5 ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Account ab = getBusinessAccount () ;
        insert ab ;
        
        Loan_Application__c la1 = new Loan_Application__c () ;
        la1.RecordTypeId = LoanApplicationHelper.getPPPRecordTypeID () ;
        la1.Status__c = 'Processing' ;
        la1.Application_Number__c = '123456' ;
        la1.Loan_Amount__c = 890.00 ;

		la1.Organization_Name__c = 'Gina Ingram' ;
        la1.Organization_Email__c = 'ginai65@aol.com' ;
        la1.Organization_EIN__c = '490783867' ;
        
        la1.Organization_Street__c = '9800 West Green Tree' ;
        la1.Organization_City__c = 'Milwaukee' ;
        la1.Organization_State__c = 'WI' ;
        la1.Organization_Zip_Code__c = '53224' ;
        
        la1.PriFullName__c = 'Gina Ingram' ;
        la1.PriTaxID__c = '490783867' ;
        
        la1.Own1FullName__c = 'Gina Denise Ingram' ;
        la1.own1FirstName__c = 'Gina' ;
        la1.Own1MiddleName__c = 'Denise' ;
        la1.own1LastName__c = 'Ingram' ;
        la1.Own1Address1__c = '9800 West Green Tree' ;
        la1.Own1City__c = 'Milwaukee' ;
        la1.Own1State__c = 'WI' ;
        la1.Own1Zip__c = '53224' ;
        la1.Own1TaxID__c = '490783867' ;
        la1.Own1OwnershipPercent__c = 20.0 ;
        
        la1.Own2FullName__c = 'Kim Maladano Ingram' ;
        la1.own2FirstName__c = 'Kim' ;
        la1.Own2MiddleName__c = 'Maladano' ;
        la1.own2LastName__c = 'Ingram' ;
        la1.Own2Address1__c = '9800 West Green Tree' ;
        la1.Own2City__c = 'Milwaukee' ;
        la1.Own2State__c = 'WI' ;
        la1.Own2Zip__c = '53224' ;
        la1.Own2OwnershipPercent__c = 20.0 ;
        
        la1.Own3FullName__c = 'Lim Naladano Ingram' ;
        la1.own3FirstName__c = 'Lim' ;
        la1.Own3MiddleName__c = 'Naladano' ;
        la1.own3LastName__c = 'Ingram' ;
        la1.Own3Address1__c = '9800 West Green Tree' ;
        la1.Own3City__c = 'Milwaukee' ;
        la1.Own3State__c = 'WI' ;
        la1.Own3Zip__c = '53224' ;
        la1.Own3OwnershipPercent__c = 20.0 ;
        
        la1.Own4FullName__c = 'Mim Paladano Ingram' ;
        la1.own4FirstName__c = 'Mim' ;
        la1.Own4MiddleName__c = 'Paladano' ;
        la1.own4LastName__c = 'Ingram' ;
        la1.Own4Address1__c = '9800 West Green Tree' ;
        la1.Own4City__c = 'Milwaukee' ;
        la1.Own4State__c = 'WI' ;
        la1.Own4Zip__c = '53224' ;
        la1.Own4OwnershipPercent__c = 20.0 ;
        
        la1.Own5FullName__c = 'Nim Qaladano Ingram' ;
        la1.own5FirstName__c = 'Nim' ;
        la1.Own5MiddleName__c = 'Paladano' ;
        la1.own5LastName__c = 'Ingram' ;
        la1.Own5Address1__c = '9800 West Green Tree' ;
        la1.Own5City__c = 'Milwaukee' ;
        la1.Own5State__c = 'WI' ;
        la1.Own5Zip__c = '53224' ;
        la1.Own5OwnershipPercent__c = 20.0 ;
        
        insert la1 ;
        
        Loan_Application__c xLa1x = [
            SELECT ID, Primary_Person__c, Organization__c, Primary_Organization__c, Own1PersonID__c, Own2PersonID__c, Own3PersonID__c, Own4PersonID__c, Own5PersonID__c
            FROM Loan_Application__c
            WHERE ID = :la1.ID
        ] ;

        System.assertNotEquals ( xLa1x.Organization__c, null ) ;
        System.assertNotEquals ( xLa1x.Primary_Organization__c, null ) ;
        System.assertNotEquals ( xLa1x.Primary_Person__c, null ) ;
        
        System.assertNotEquals ( xLa1x.Own1PersonID__c, null ) ;
        System.assertEquals ( xLa1x.Primary_Person__c, xLa1x.Own1PersonID__c ) ;
        
        System.assertNotEquals ( xLa1x.Own2PersonID__c, null ) ;
        System.assertNotEquals ( xLa1x.Own3PersonID__c, null ) ;
        System.assertNotEquals ( xLa1x.Own4PersonID__c, null ) ;
        System.assertNotEquals ( xLa1x.Own5PersonID__c, null ) ;
   }
}