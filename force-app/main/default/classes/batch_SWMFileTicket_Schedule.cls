global class batch_SWMFileTicket_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_SWMFileTicket b = new batch_SWMFileTicket () ;
		Database.executeBatch ( b ) ;
   }
}