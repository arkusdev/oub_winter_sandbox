@isTest
public class TestEmailActionFunction2 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Contact getContact ( Integer x )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
		
		insert c ;
		
		return c ;
	}
	
	private static Account getAccount ( Integer x )
	{
		Account a = new Account () ;
		a.Name = 'Test' + x + ' Account' + x ;
		a.Business_Email__c = 'test' + x + '@user' + x + '.com' ;
		
		insert a ;
		
		return a ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Send Contact Emails Direct
	//  --------------------------------------------------------------------------------------
	static testmethod void test_sendContactEmails ()
	{
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		Contact c11 = getContact ( 11 ) ;
		Contact c12 = getContact ( 12 ) ;
		
		List<String> cL = new List<String> () ;
		cL.add ( c11.Id ) ;
		cL.add ( c12.Id ) ;
		
		EmailActionFunction2.sendContactEmails ( cL, 'test-campaign-template' ) ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Send Account Emails Direct
	//  --------------------------------------------------------------------------------------
	static testmethod void test_sendAccountEmails ()
	{
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		Account a11 = getAccount ( 11 ) ;
		Account a12 = getAccount ( 12 ) ;
		
		List<String> aL = new List<String> () ;
		aL.add ( a11.Id ) ;
		aL.add ( a12.Id ) ;
		
		EmailActionFunction2.sendAccountEmails ( aL, 'test-campaign-template' ) ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact
	//  --------------------------------------------------------------------------------------
	static testmethod void test_sendOnlineBankingBenefitsContactOne ()
	{
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		Contact c11 = getContact ( 11 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr1 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr1.contactId = c11.ID ;
		sr1.TemplateName = 'online-banking-signup-template' ;
		 
		Contact c12 = getContact ( 12 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr2 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr2.contactId = c12.ID ;
		sr2.TemplateName = 'online-banking-benefits-template' ;
		
		Contact c13 = getContact ( 13 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr3 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr3.contactId = c13.ID ;
		sr3.TemplateName = 'online-banking-signup-template' ;
		 
		List<EmailActionFunction2.SendOnlineBankingRequest> srL = new List<EmailActionFunction2.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		srL.add ( sr2 ) ;
		srL.add ( sr3 ) ;
		
		List<EmailActionFunction2.SendOnlineBankingResult> srR = EmailActionFunction2.sendOnlineBankingEmail ( srL ) ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact
	//  --------------------------------------------------------------------------------------
	static testmethod void test_sendOnlineBankingBenefitsContactTwo ()
	{
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		Contact c11 = getContact ( 11 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr1 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr1.contactId = c11.ID ;
		sr1.TemplateName = 'online-banking-signup-template' ;
        sr1.SubAccount = 'SOMETHING' ;
        sr1.Subject = 'Yo!' ;
		sr1.mergeVarName1 = 'CN1' ;
		sr1.mergeVarValue1 = 'CV1' ;
		sr1.mergeVarName2 = 'CN2' ;
		sr1.mergeVarValue2 = 'CV2' ;
		sr1.mergeVarName3 = 'CN3' ;
		sr1.mergeVarValue3 = 'CV3' ;
		sr1.mergeVarName4 = 'CN4' ;
		sr1.mergeVarValue4 = 'CV4' ;
		sr1.mergeVarName5 = 'CN5' ;
		sr1.mergeVarValue5 = 'CV5' ;
		 
		List<EmailActionFunction2.SendOnlineBankingRequest> srL = new List<EmailActionFunction2.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		
		List<EmailActionFunction2.SendOnlineBankingResult> srR = EmailActionFunction2.sendOnlineBankingEmail ( srL ) ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Account
	//  --------------------------------------------------------------------------------------
	static testmethod void test_sendOnlineBankingBenefitsAccountOne ()
	{
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		Account a11 = getAccount ( 11 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr1 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr1.contactId = a11.ID ;
		sr1.TemplateName = 'online-banking-signup-template' ;
		sr1.idType = 'Account' ;
		 
		Account a12 = getAccount ( 12 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr2 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr2.contactId = a12.ID ;
		sr2.TemplateName = 'online-banking-signup-template' ;
		sr2.idType = 'Account' ;
		
		List<EmailActionFunction2.SendOnlineBankingRequest> srL = new List<EmailActionFunction2.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		srL.add ( sr2 ) ;
		
		List<EmailActionFunction2.SendOnlineBankingResult> srR = EmailActionFunction2.sendOnlineBankingEmail ( srL ) ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Account
	//  --------------------------------------------------------------------------------------
	static testmethod void test_sendOnlineBankingBenefitsAccountTwo ()
	{
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		Account a11 = getAccount ( 11 ) ;
		EmailActionFunction2.SendOnlineBankingRequest sr1 = new EmailActionFunction2.SendOnlineBankingRequest () ;
		sr1.contactId = a11.ID ;
		sr1.TemplateName = 'online-banking-signup-template' ;
        sr1.SubAccount = 'SOMETHING' ;
        sr1.Subject = 'Yo!' ;
		sr1.idType = 'Account' ;
		sr1.mergeVarName1 = 'AN1' ;
		sr1.mergeVarValue1 = 'AV1' ;
		sr1.mergeVarName2 = 'AN2' ;
		sr1.mergeVarValue2 = 'AV2' ;
		sr1.mergeVarName3 = 'AN3' ;
		sr1.mergeVarValue3 = 'AV3' ;
		sr1.mergeVarName4 = 'AN4' ;
		sr1.mergeVarValue4 = 'AV4' ;
		sr1.mergeVarName5 = 'AN5' ;
		sr1.mergeVarValue5 = 'AV5' ;
        sr1.surveyYN = 'Y' ;

		List<EmailActionFunction2.SendOnlineBankingRequest> srL = new List<EmailActionFunction2.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		
		List<EmailActionFunction2.SendOnlineBankingResult> srR = EmailActionFunction2.sendOnlineBankingEmail ( srL ) ;
	}
}