public with sharing class Financial_Document_Upload 
{
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    /*  ----------------------------------------------------------------------------------------
     *  LOCKOUT
     *  ---------------------------------------------------------------------------------------- */
    private static integer LOCKOUT_COUNT = 3 ;
     
    /*  ----------------------------------------------------------------------------------------
     *  Display
     *  ---------------------------------------------------------------------------------------- */
    public string accountNumberMasked
    {
    	get ;
    	private set ;
    }
    
    public Integer thisYear
    {
    	get
    	{
    		return System.Today().year() ;
    	}
    	private set ;
    }
    
    public Integer priorYear
    {
    	get
    	{
    		return System.Today().addYears(-1).year() ;
    	}
    	private set ;
    }
    
    public Integer priorPriorYear
    {
    	get
    	{
    		return System.Today().addYears(-2).year() ;
    	}
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Preliminary Inputs
     *  ---------------------------------------------------------------------------------------- */
    //  Account #
    public String accountNumber 
    { 
        get ; 
        set ; 
   	}
   	
   	//  Last 4 Tax ID
    public String taxID 
    { 
        get ; 
        set ; 
   	}
    
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	//  Ticket object to upload
	public Ticket__c ticket 
	{
		get 
		{
			if ( ticket == null )
				ticket = new Ticket__c () ;
			return ticket ;
		}
		
		set ;
	}
     
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
 
	//  Step control for panels on page
	public Integer dStepNumber
	{
		get ;
		private set ;
	}

    /*  ----------------------------------------------------------------------------------------
     *  Internal objects
     *  ---------------------------------------------------------------------------------------- */
    private Financial_Account__c fa ;
    private integer lockoutCount ;
     
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorAccountNumber
    {
    	get ;
    	private set ;
    }
     
    public boolean errorTaxID
    {
    	get ;
    	private set ;
    }
    
    public boolean errorNotes
    {
    	get ;
    	private set ;
    }
     
    public boolean errorDocType
    {
    	get ;
    	private set ;
    }
     
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public Financial_Document_Upload ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		dStepNumber = 1000 ;

		//  Counter!		
		lockoutCount = 0 ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Validate Inputs
     *  ---------------------------------------------------------------------------------------- */
    public PageReference findAccount ()
    {
		boolean isValid = true ;
		errorAccountNumber = false ;
		errorTaxID = false ;
		
		//  Validation
		if ( String.isBlank ( accountNumber ) )
		{
			errorAccountNumber = true ;
			isValid = false ;
		}

		if ( String.isBlank ( taxID ) )
		{
			errorTaxID = true ;
			isValid = false ;
		}
    	
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        dStepNumber = 1001 ;
			return null ;
		}
		else
		{
	    	try
	    	{
	    		list<Financial_Account__c> faL = [
	    			SELECT ID, ACCTNBR__c,
	    			TAXRPTFORPERSNBR__c,
	    			TAXRPTFORORGNBR__c,
	    			Account_Number_Masked__c,
	    			Ownername__c,
	    			Document_Upload_Lockout_Date__c,
	    			Last_4_Owner_Tax_ID__c
	    			FROM Financial_Account__c
	    			WHERE ACCTNBR__c = :accountNumber
	    			AND MJACCTTYPCD__c IN ( 'CML', 'MTG' )
	    			ORDER BY ID
	    		] ;
	    		
	    		if ( ( faL != null ) && ( faL.size () > 0 ) )
	    		{
	    			//  Grab the first one
	    			Financial_Account__c faP = faL [ 0 ] ;
	    			
	    			System.debug ( '?? 0 ' + faP.Last_4_Owner_Tax_ID__c ) ;
	    			System.debug ( '?? 1 ' + faP.TAXRPTFORPERSNBR__c ) ;
	    			System.debug ( '?? 2 ' + faP.TAXRPTFORORGNBR__c ) ;
	    			System.debug ( '?? 3 ' + faP.Document_Upload_Lockout_Date__c ) ;
	    			
	    			//  Last hour
	    			DateTime thingy = system.now ().addHours ( -1 ) ;
	    			
	    			//  LOCKOUT CHECK
	    			if ( ( faP.Document_Upload_Lockout_Date__c == null ) || 
	    				 ( ( faP.Document_Upload_Lockout_Date__c != null ) &&
	    				   ( faP.Document_Upload_Lockout_Date__c <= thingy ) ) )
	    			{
		    			//  Check tax ID
		    			if ( ( String.isNotBlank ( faP.Last_4_Owner_Tax_ID__c ) ) &&
		    			     ( faP.Last_4_Owner_Tax_ID__c.right ( 4 ).equalsIgnoreCase ( TaxID ) ) )
		    			{
		    				System.debug ( 'Found LAST 4 Tax ID!' ) ;
		    				
		    				fa = faP ;
		    				accountNumberMasked = fa.Account_Number_Masked__c ;
		    				
		    				if ( faP.Document_Upload_Lockout_Date__c != null )
		    				{
								try
								{
									System.debug ( 'Resetting lockout date!' ) ;
									
				    				faP.Document_Upload_Lockout_Date__c = null ;
				    				update faP ;
								}
						    	catch ( DMLException e )
								{
								    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
								        System.debug ( e.getDmlMessage ( i ) ) ; 
								}
		    				}
		    				
		    				dStepNumber = 1100 ;
		    			}
		    			else
		    			{
					        System.debug ( '========== TaxID does not Match! ==========' ) ;
					        
							System.debug ( 'Incrementing lockout to :: ' + ( lockoutCount++ ) ) ;
							
							if ( lockoutCount >= LOCKOUT_COUNT )
							{
								System.debug ( 'Writing lockout timestamp' ) ;
								try
								{
									faP.Document_Upload_Lockout_Date__c = System.now () ;
									update faP ;
									
									dStepNumber = 1003 ;
								}
						    	catch ( DMLException e )
								{
								    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
								        System.debug ( e.getDmlMessage ( i ) ) ; 
								}
							}
							else
							{
								System.debug ( '--> ' + lockoutCount + ' -- ' + LOCKOUT_COUNT ) ;					        
					        	dStepNumber = 1002 ;
							}

							return null ;
		    			}
	    			}
	    			else
	    			{
				        System.debug ( '========== LOCKOUT! ==========' ) ;
				        dStepNumber = 1003 ;
						return null ;
	    			}
	    		}
	    		else
	    		{
			        System.debug ( '========== Account not Found! ==========' ) ;
			        dStepNumber = 1002 ;
					return null ;
	    		}
	    		
	    	}
	    	catch ( DMLException e )
	    	{
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
			        System.debug ( e.getDmlMessage ( i ) ) ; 
	    		
	    		dStepNumber = 1002 ;
	    		return null ;
	    	}
		}
	    	
    	return null ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Validate & Upload the file
     *  ---------------------------------------------------------------------------------------- */
	public PageReference upload () 
	{
		boolean isValid = true ;
		errorNotes = false ;
		errorDocType = false ;
		errorName = false ;
		errorFile = false ;
		
		if ( ticket.Financial_Doc_Type__c == null )
		{
			errorDocType = true ;
			isValid = false ;
		}
		
		if ( String.isBlank ( attachment.name ) )
		{
			errorName = true ;
			isValid = false ;
		}
		
		if ( String.isBlank ( attachment.Description ) )
		{
			errorNotes = true ;
			isValid = false ;
		}
		
		if ( attachment.body == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Attachment inputs not Valid! ==========' ) ;
	        dStepNumber = 1101 ;
			return null ;
		}
		
		attachment.OwnerId = UserInfo.getUserId () ;
		attachment.ParentId = fa.Id ;    // the record the file is attached to
		attachment.IsPrivate = false ;  // Or the guest user can't query it!
		
		try 
		{
	        System.debug ( '========== Checking for created ticket ==========' ) ;
	        
			if ( ticket.ID == null )
			{
		        System.debug ( '========== Inserting Ticket ==========' ) ;
		        
		        ticket.RecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Financials', 'Ticket__c' ) ;
		        ticket.Account_Number__c = fa.ACCTNBR__c ;
		        ticket.Financial_Account__c = fa.ID ;
		        ticket.Status__c = 'Received' ;
	
		        System.debug ( 'fa1 :: ' + fa.TAXRPTFORPERSNBR__C ) ;
		        System.debug ( 'fa2 :: ' + fa.TAXRPTFORORGNBR__C ) ;
		        
		        if ( fa.TAXRPTFORPERSNBR__C != NULL )
		        	ticket.Contact__c = fa.TAXRPTFORPERSNBR__c ;
		        
		        if ( fa.TAXRPTFORORGNBR__C != NULL )
		        	ticket.Account__c = fa.TAXRPTFORORGNBR__c ;
	
		        //insert ticket ;
                access.insertObject(ticket);
               
			}
			else
			{
		        System.debug ( '========== Found Ticket ID ' + ticket.ID + ' ==========' ) ;
			}
		}
		catch ( DMLException e )
		{
			System.debug ( '========== Error Inserting Ticket! ==========' ) ;
			dStepNumber = 1102 ;
			
		    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
		        System.debug ( e.getDmlMessage ( i ) ) ; 

			return null;
		}
		
		try
	    { 
	        System.debug ( '========== Inserting Attachment ==========' ) ;
	        attachment.ParentID = ticket.ID ;
	        attachment.Description = '~' + ticket.Financial_Doc_Type__c + '~\n\n' + attachment.Description  ;
			//insert attachment ;
            access.insertObject(attachment);
		}
		catch ( DMLException e )
		{
			System.debug ( '========== Error Inserting Attachment! ==========' ) ;
			dStepNumber = 1102 ;
			
		    for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
		        System.debug ( e.getDmlMessage ( i ) ) ; 

			return null;
		}
		finally 
		{
	        System.debug ( '========== Final stuff ==========' ) ;
	        
	        //  Set panel to success upload
			dStepNumber = 1200 ;
			
			//  Clear errors if any
			errorName = false ;
    		errorFile = false ;
			
			//  Reset attachment
			attachment = new Attachment () ; 
		}
		
		return null ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Continue along
     *  ---------------------------------------------------------------------------------------- */
	public PageReference confirm () 
	{
		dStepNumber = 1100 ;
		
		return null ;
	}
}