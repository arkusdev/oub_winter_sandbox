@isTest
public class Q2BillerHelper_Test 
{
	/* --------------------------------------------------------------------------------
	 * Setup, Settings
	 * -------------------------------------------------------------------------------- */
    private static Q2_Billing_Settings__c getSettings ()
    {
        Q2_Billing_Settings__c q2bs = new Q2_Billing_Settings__c () ;
        
        q2bs.name = 'settings' ;
        q2bs.API_Key__c = 'xyz123' ;
        q2bs.API_Host_URL__c = 'https://foo.bar/v1' ;
        q2bs.Q2B_Card_Swap_URL__c = 'https://product.q2open.io' ;
        q2bs.Q2B_Client_Token_URL__c = 'https://foo.bar/v1/client/create-token' ;
        q2bs.Q2B_Create_Payment_URL__c = 'https://foo.bar/v1/payment' ;
        q2bs.Q2B_List_Payment_URL__c = 'https://foo.bar/v1/payment/list' ;
        q2bs.Q2B_Upsert_Client_URL__c = 'https://foo.bar/v1/client' ;
        
        insert q2bs ;
        
        return q2bs ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Account
	 * -------------------------------------------------------------------------------- */
    private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.Name = 'Test' + i ;
        return a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Account a, Integer i )
	{
		Contact c = new Contact () ;
        
        c.AccountId = A.ID ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Setup, Service
	 * -------------------------------------------------------------------------------- */
    private static Service__c getService ( Contact c, Integer i )
    {
 		String aID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Agreement', 'Service__c' ) ;
        
        Service__c s = new Service__c () ;
        s.Contact__c = c.ID ;
        s.RecordTypeId = aID ;
        s.Status__c = 'Active' ;
        s.Type__c = 'Debit Card' ;
        s.Plastic_Type__c = 'KING' ;
        s.Card_Number__c = '000' + i + '000' + i + '000' + i + '000' + i ;
        s.Expiration_Date__c = Date.newInstance ( 2000 + i, i, i ) ;
        
        return s ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Individual tests
	 * -------------------------------------------------------------------------------- */
    public static testmethod void CLIENT_LOGIN ()
    {
        getSettings () ; 
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        insert c1 ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new Q2BillerHelper_Mock () ) ;
        
        //  GO !?!?
        Test.startTest () ;
        
        Q2BillerHelper q2bh = new Q2BillerHelper () ;
        
        q2bh.setContact ( c1.ID ) ;
        
        String token = q2bh.getClientToken () ;
        
        System.debug ( 'Token :: ' + token ) ;
        
        test.stopTest () ;
	}

    public static testmethod void CLIENT_UPSERT ()
    {
        getSettings () ; 
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        insert c1 ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new Q2BillerHelper_Mock () ) ;
        
        //  GO !?!?
        Test.startTest () ;
        
        Q2BillerHelper q2bh = new Q2BillerHelper () ;
        
        q2bh.setContact ( c1 ) ;
        
        System.debug ( 'Result :: ' + q2bh.upsertCustomer () ) ;
        
        test.stopTest () ;
	}
    
    public static testmethod void PAYMENTS ()
    {
        getSettings () ;
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        insert c1 ;
        
        Service__c s1 = getService ( c1, 1 ) ;
        insert s1 ;
        
        Service__c s2 = getService ( c1, 2 ) ;
        s2.Card_Number__c = '00010010014242' ;
        s2.Expiration_Date__c = Date.newInstance ( 2016, 12, 1 ) ;
        insert s2 ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new Q2BillerHelper_Mock () ) ;
        
        //  GO !?!?
        Test.startTest () ;
        
        Q2BillerHelper_Mock.returnOptionalIdentifier = s1.ID ;
        
        Q2BillerHelper q2bh = new Q2BillerHelper () ;
        
        q2bh.setContact ( c1 ) ;
        
        System.debug ( 'Result :: ' + q2bh.upsertCustomer () ) ;
        System.debug ( 'Result :: ' + q2bh.getPaymentMethods () ) ;
        System.debug ( 'Result :: ' + q2bh.updatePaymentMethods () ) ;
        
        test.stopTest () ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Everything at once
	 * -------------------------------------------------------------------------------- */
    public static testmethod void EVERYTHINGandFORWARD ()
    {
        getSettings () ; 
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        insert c1 ;
        
        Service__c s1 = getService ( c1, 1 ) ;
        insert s1 ;
        
        Service__c s2 = getService ( c1, 2 ) ;
        insert s2 ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new Q2BillerHelper_Mock () ) ;
        
        //  GO !?!?
        Test.startTest () ;
        
        Q2BillerHelper_Mock.returnOptionalIdentifier = s1.Card_Number__c ;
        
        Q2BillerHelper q2bh = new Q2BillerHelper () ;
        
        q2bh.setContact ( c1 ) ;
        
        String url = q2bh.doEverythingAndForward () ;
       
        test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Everything at once
	 * -------------------------------------------------------------------------------- */
    public static testmethod void justFORWARD ()
    {
        getSettings () ; 
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        insert c1 ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new Q2BillerHelper_Mock () ) ;
        
        //  GO !?!?
        Test.startTest () ;
        
        Q2BillerHelper q2bh = new Q2BillerHelper () ;
        
        q2bh.setContact ( c1 ) ;
        
        String url = q2bh.justForward () ;
       
        test.stopTest () ;
    }
}