@isTest
public class Test_application_error_controller
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
	
    public static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = false ;

        s.Trial_Deposit_Validation_Limit__c = 3 ;
        s.ACH_Validation_Threshold__c = 1000.00 ;
        s.Deposit_ACH_Validation_Threshold__c = 1000.00 ;
        
        s.Rates_URL__c = 'https://www.cnn.com' ;
        s.Product__c = 'VSC' ;
        s.SubProduct__c = '000' ;
        s.Plan__c = '000' ;
        
        //  FIS STUFF
        s.FIS_Aquirer_ID__c = '059187' ;
        s.FIS_Authentication_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/authen' ;
        s.FIS_IDA_Config_Key__c = 'idaaliaskey' ;
        s.FIS_IDA_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/ida' ;
        s.FIS_Location_ID__c = 'UNITY Visa Application' ;
        s.FIS_Staging_Flag__c = true ;
        s.FIS_User_ID__c = '02280828' ;
        s.FIS_User_Password__c = 'Z26ZjcZdZAiZXaZ' ;
        
        insert s;
    }
    
	/*
	 * Creates a generic application I use for testing.
	 */
	private static one_application__c getApplicationForTests ()
	{
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		one_application__c app = new one_application__c ();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
        app.Funding_Options__c = 'E- Check (ACH)' ;
        app.RequestedCreditLimit__c = 1000.00 ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'P501' ;
		
		app.Trial_Deposit_1__c = 7 ;
		app.Trial_Deposit_2__c = 11 ;
		app.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
		
		return app ;
	}

	/*
	 * Extra stuff for deposit application.
	 */
    private static Online_Deposit_Application_Account__c getAA ( one_application__c o, Integer i )
    {
        Online_Deposit_Application_Account__c odaa = new Online_Deposit_Application_Account__c () ;
        odaa.Product_Name__c = 'Test ' + i ;
        odaa.Application__c = o.ID ;
        odaa.APY__c = 10.0 ;
        odaa.Debit_card__c = true ;
        odaa.Major_Account_Type_Code__c = 'SAV' ;
        odaa.Minor_Account_Type_Code__c = 'UGES' ;
        odaa.Initial_Funding_amount__c = 100.00 ;
        odaa.Card_Code__c = 'XYZ' ;
        
        return odaa ;
    }
    
	/*
	 * Tests with an existing application passed in
	 */	
	static testmethod void TestDepositAppCurrentSession ()
	{
        insertCustomSetting () ;
        
		//  Point to page to test
		PageReference pr1 = Page.deposit_application_error5 ;
		Test.setCurrentPage ( pr1 ) ;

        //  SETUP
		one_application__c app = getApplicationForTests () ;
        app.RecordTypeID = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ;
        insert app ;
        
        Online_Deposit_Application_Account__c odaa1 = getAA ( app, 1 ) ;
        insert odaa1 ;
        
        Online_Deposit_Application_Account__c odaa2 = getAA ( app, 2 ) ;
        insert odaa2 ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;

        //  Stupid name field
		one_application__c appX = one_utils.getApplicationFromId ( app.ID ) ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( appX ) ;
		
		application_error_controller aec = new application_error_controller ( sc ) ;
		
		//  End of testing
		Test.stopTest () ;
    }
    
	/*
	 * Tests with an existing application passed in
	 */	
	static testmethod void TestDepositAppNewSession ()
	{
        insertCustomSetting () ;
        
		//  Point to page to test
		PageReference pr1 = Page.deposit_application_error5 ;
		Test.setCurrentPage ( pr1 ) ;

        //  SETUP
		one_application__c app = getApplicationForTests () ;
        app.RecordTypeID = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ;
        insert app ;
        
        Online_Deposit_Application_Account__c odaa1 = getAA ( app, 1 ) ;
        insert odaa1 ;
        
        Online_Deposit_Application_Account__c odaa2 = getAA ( app, 2 ) ;
        insert odaa2 ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;

		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', app.id );
        ApexPages.currentPage().getParameters().put ( 'kid', app.Upload_Attachment_Key__c );

        //  Cookie!
        Cookie jCookie = new Cookie ( 'j_Cookie', app.ID, null, -1, false ) ;
        pr1.setCookies ( new Cookie [] { jCookie } ) ;
        
		//  Empty application to pass in
		one_application__c eApp = new one_application__c () ;

		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eApp ) ;
		
		application_error_controller aec = new application_error_controller ( sc ) ;
		
		//  End of testing
		Test.stopTest () ;
    }
}