@isTest
public with sharing class TestoneOpportunityTrigger 
{
    /* -----------------------------------------------------------------------------------
	 * Default account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ()
	{
		Account a = new Account () ;
		a.Name = 'Insight Customers - Individuals' ;
        
		return a ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Branch stuff
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		return getBranch ( null ) ;
	}
	
	private static Branch__c getBranch ( User u )
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		
		if ( u != null )
			b.Branch_Manager__c = u.Id ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * User
	 * ----------------------------------------------------------------------------------- */
	private static User getUser ( String thingy, Branch__c b )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		u.Is_NMLO__c = true ;
		u.Branch__c = b.ID ;
				
		insert u ;
		
		return u ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( String thingy, String state )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + thingy ;
		c.LastName = 'Person' + thingy ;
		c.Email = 'test' + thingy + '.person' + thingy + '@testing' + thingy + '.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = state ;
		c.MailingPostalCode = '02110' ;
		
		insert c ;
		return c ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Actual account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ( String thingy, String state )
	{
		Account a = new Account () ;
		a.name = 'Test' + thingy + ' Account' + thingy ;
		a.BillingState = state ;
		
		insert a ;
		return a ;
	}

	static testmethod void testOneSingleDepositContact ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x = new oneOpportunity__c () ;
		x.Email__c = 'test@user.com' ;
		x.Interested_in_Product__c = 'Savings' ;
		x.First_Name__c = 'Test' ;
		x.Home_Phone__c = '123-456-7890' ;
		x.Opportunity_Contact__c = c1.id ;
		x.OwnerId = u1.id ;
		x.RecordTypeId = rt ;
		
		insert x ;
		
		Test.stopTest () ;
	}

	static testmethod void testTwoUpdateSingleDepositAccount ()
	{
		branch__c b = getBranch () ;
		
		User u1 = getUser ( '1', b ) ;
		User u2 = getUser ( '2', b ) ;
				
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit Opportunity', 'oneOpportunity__c' ) ;

		Account a1 = getAccount ( '1', 'CA' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x = new oneOpportunity__c () ;
		x.Email__c = 'test@user.com' ;
		x.Interested_in_Product__c = 'Checking' ;
		x.First_Name__c = 'Test' ;
		x.Home_Phone__c = '789-456-0123' ;
		x.Relationship__c = a1.id ;
		x.Assigned_To__c = u1.id ;
		x.OwnerId = u1.id ;
		x.RecordTypeId = rt ;		
		
		insert x ;
		
		oneOpportunity__c y = [
			SELECT
			Id, Email__c, Interested_in_Product__c, First_Name__c, Home_Phone__c,
			Relationship__c, Assigned_To__c, RecordTypeId
			FROM
			oneOpportunity__c
			WHERE
			Id = :x.id
		] ;
		
		y.OwnerId = u2.id ;
		
		update y ;
		
		Test.stopTest () ;
	}

	static testmethod void testThreeSingleSingleLoanContact ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x1 = new oneOpportunity__c () ;
		x1.Email__c = 'test@user.com' ;
		x1.Interested_in_Product__c = 'Single Family Home' ;
		x1.First_Name__c = 'Test' ;
		x1.Home_Phone__c = '654-456-1324' ;
		x1.Opportunity_Contact__c = c1.id ;
		x1.OwnerId = u1.id ;
		x1.RecordTypeId = rt ;
		x1.State__c = 'CA' ;
		
		insert x1 ;
				
		Test.stopTest () ;
	}

	static testmethod void testFourMultiLoanContact ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x1 = new oneOpportunity__c () ;
		x1.Email__c = 'test@user.com' ;
		x1.Interested_in_Product__c = 'Multifamily' ;
		x1.First_Name__c = 'Test' ;
		x1.Home_Phone__c = '654-456-1324' ;
		x1.Opportunity_Contact__c = c1.id ;
		x1.OwnerId = u1.id ;
		x1.RecordTypeId = rt ;
		x1.State__c = 'CA' ;
		
		insert x1 ;
		
		Test.stopTest () ;
	}

	static testmethod void testFiveSingleDepositAccount ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		User u2 = getUser ( '2', b ) ;
		
		Account a1 = getAccount ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x = new oneOpportunity__c () ;
		x.Email__c = 'test@user.com' ;
		x.Interested_in_Product__c = 'Checking' ;
		x.First_Name__c = 'Test' ;
		x.Home_Phone__c = '789-456-0123' ;
		x.Relationship__c = a1.id ;
		x.OwnerId = u1.id ;
		x.RecordTypeId = rt ;		
		
		insert x ;
		
		oneOpportunity__c y = [
			SELECT
			Id, Email__c, Interested_in_Product__c, First_Name__c, Home_Phone__c,
			Relationship__c, RecordTypeId
			FROM
			oneOpportunity__c
			WHERE
			Id = :x.id
		] ;
		
		y.OwnerId = u2.id ;
		
		update y ;
		
		Test.stopTest () ;
	}

	static testmethod void testSixSingleSingleLoanContactState ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		Contact c2 = getContact ( '2', 'MA' ) ;
		Contact c3 = getContact ( '3', 'FL' ) ;
		Contact c4 = getContact ( '3', 'TX' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x1 = new oneOpportunity__c () ;
		x1.Email__c = c1.Email ;
		x1.Interested_in_Product__c = 'Single Family Home' ;
		x1.First_Name__c = c1.FirstName ;
		x1.Home_Phone__c = '654-456-1324' ;
		x1.Opportunity_Contact__c = c1.id ;
		x1.OwnerId = u1.id ;
		x1.RecordTypeId = rt ;
		x1.State__c = c1.MailingState ;
		
		insert x1 ;
		
		oneOpportunity__c x2 = new oneOpportunity__c () ;
		x2.Email__c = c2.Email ;
		x2.Interested_in_Product__c = 'Single Family Home' ;
		x2.First_Name__c = c2.FirstName ;
		x2.Home_Phone__c = '654-456-1324' ;
		x2.Opportunity_Contact__c = c2.id ;
		x2.OwnerId = u1.id ;
		x2.RecordTypeId = rt ;
		x2.State__c = c2.MailingState ;
		
		insert x2 ;
		
		oneOpportunity__c x3 = new oneOpportunity__c () ;
		x3.Email__c = c3.Email ;
		x3.Interested_in_Product__c = 'Single Family Home' ;
		x3.First_Name__c = c3.FirstName ;
		x3.Home_Phone__c = '654-456-1324' ;
		x3.Opportunity_Contact__c = c3.id ;
		x3.OwnerId = u1.id ;
		x3.RecordTypeId = rt ;
		x3.State__c = c3.MailingState ;
		
		insert x3 ;
		
		oneOpportunity__c x4 = new oneOpportunity__c () ;
		x4.Email__c = c4.Email ;
		x4.Interested_in_Product__c = 'Single Family Home' ;
		x4.First_Name__c = c4.FirstName ;
		x4.Home_Phone__c = '654-456-1324' ;
		x4.Opportunity_Contact__c = c4.id ;
		x4.OwnerId = u1.id ;
		x4.RecordTypeId = rt ;
		x4.State__c = c4.MailingState ;
		
		insert x4 ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testSevenSPAM ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		User u2 = getUser ( '2', b ) ;
		
		User u3 = getUser ( '3', b ) ;
		
		String lrt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		String drt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit Opportunity', 'oneOpportunity__c' ) ;
		
		Integer LOAN_SPAM_LIMIT = 10 ;
		Integer DEPOSIT_SPAM_LIMIT = 10 ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		List<oneOpportunity__c> ooL = new List<oneOpportunity__c> () ;
		
		for ( Integer i = 0 ; i < LOAN_SPAM_LIMIT ; i ++ )
		{
			Contact c = getContact ( String.valueof ( i ), 'CA' ) ;
		
			oneOpportunity__c oo = new oneOpportunity__c () ;
			oo.Email__c = c.Email ;
			oo.Interested_in_Product__c = 'Single Family Home' ;
			oo.First_Name__c = c.FirstName ;
			oo.Home_Phone__c = '654-456-1324' ;
			oo.Opportunity_Contact__c = c.id ;
			oo.OwnerId = u1.id ;
			oo.RecordTypeId = lrt ;
			oo.State__c = c.MailingState ;
			
			ooL.add ( oo ) ;
		}
		
		for ( Integer i = 0 ; i < DEPOSIT_SPAM_LIMIT ; i ++ )
		{
			Contact c = getContact ( String.valueof ( i ), 'MA' ) ;
		
			oneOpportunity__c oo = new oneOpportunity__c () ;
			oo.Email__c = c.Email ;
			oo.Interested_in_Product__c = 'Savings' ;
			oo.First_Name__c = c.FirstName ;
			oo.Home_Phone__c = '654-456-1324' ;
			oo.Opportunity_Contact__c = c.id ;
			oo.OwnerId = u2.id ;
			oo.RecordTypeId = drt ;
			oo.State__c = c.MailingState ;
			
			ooL.add ( oo ) ;
		}
		
		insert ooL ;
		
		List<oneOpportunity__c> newOOL = [
			SELECT ID, RecordTypeId
			FROM oneOpportunity__c
			WHERE RecordTypeId = :dRT
			OR RecordTypeId = :LRT
		] ;
		
		for ( oneOpportunity__c oo : newOOL )
		{
			oo.OwnerID = u3.ID ;
		}
		
		update newOOL ;
		
		Test.stopTest () ;
	}
    
	static testmethod void testEightSingleFamilyContactNo ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x1 = new oneOpportunity__c () ;
		x1.Email__c = 'test@user.com' ;
		x1.Interested_in_Product__c = 'Not Specified' ;
        x1.Property_has_more_than_4_units__c = 'No' ;
		x1.First_Name__c = 'Test' ;
		x1.Home_Phone__c = '654-456-1324' ;
		x1.Opportunity_Contact__c = c1.id ;
		x1.OwnerId = u1.id ;
		x1.RecordTypeId = rt ;
		x1.State__c = 'CA' ;
		
		insert x1 ;
		
		Test.stopTest () ;
	}

	static testmethod void testNineSingleFamilyContactYes ()
	{
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		oneOpportunity__c x1 = new oneOpportunity__c () ;
		x1.Email__c = 'test@user.com' ;
		x1.Interested_in_Product__c = 'Not Specified' ;
        x1.Property_has_more_than_4_units__c = 'Yes' ;
		x1.First_Name__c = 'Test' ;
		x1.Home_Phone__c = '654-456-1324' ;
		x1.Opportunity_Contact__c = c1.id ;
		x1.OwnerId = u1.id ;
		x1.RecordTypeId = rt ;
		x1.State__c = 'CA' ;
		
		insert x1 ;
		
		Test.stopTest () ;
	}
    
	static testmethod void testLoanApplicationUpdate ()
	{
        Account a = getAccount () ;
        insert a ;
        
		branch__c b = getBranch () ;
		User u1 = getUser ( '1', b ) ;
		
		Contact c1 = getContact ( '1', 'CA' ) ;
		
		String rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;

        Test.startTest () ;
        
		//  --------------------------------------------------
		//  Create opportunity
		//  --------------------------------------------------
		oneOpportunity__c x1 = new oneOpportunity__c () ;
		x1.Email__c = c1.Email ;
		x1.Interested_in_Product__c = 'Not Specified' ;
        x1.Property_has_more_than_4_units__c = 'Yes' ;
		x1.First_Name__c = c1.FirstName ;
		x1.Home_Phone__c = '654-456-1324' ;
		x1.Opportunity_Contact__c = c1.id ;
		x1.OwnerId = u1.id ;
		x1.RecordTypeId = rt ;
		x1.State__c = 'CA' ;
		
		insert x1 ;
		
		//  --------------------------------------------------
		//  Create Application
		//  --------------------------------------------------
        Loan_Application__c la = new Loan_Application__c () ;
        la.Application_Number__c = '100123456' ;
        la.Mortgagebot_Web_ID__c = '123456' ;
        la.Loan_Amount__c = 10000.00 ;
        
        la.Borrower_First_Name__c = c1.FirstName ;
        la.Borrower_Last_Name__c = c1.LastName ;
        la.Borrower_Email__c = c1.Email ;
        
        insert la ;
        
		//  --------------------------------------------------
		//  Update opportunity
		//  --------------------------------------------------
        x1.Loan_Application_ID__c = la.Mortgagebot_Web_ID__c ;
        update x1 ;
        
		//  --------------------------------------------------
		//  Confirm 1
		//  --------------------------------------------------
		Loan_Application__c xla = [
            SELECT ID, Application_Number__c, Opportunity__c
            FROM Loan_Application__c
            WHERE ID = :la.Id
            LIMIT 1
        ] ;
        
        System.assertEquals ( xla.Opportunity__c , x1.ID ) ;
        
		//  --------------------------------------------------
		//  Confirm 2
		//  --------------------------------------------------
		oneOpportunity__c x1x = [
            SELECT ID, Loan_Application_ID__c, Loan_Application__c
            FROM oneOpportunity__c
            WHERE ID = :x1.ID
            LIMIT 1
		] ;
        
        System.assertEquals ( x1x.Loan_Application__c , la.ID ) ;
        
        test.stopTest () ;
    }
}