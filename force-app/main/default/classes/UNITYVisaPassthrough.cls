public with sharing class UNITYVisaPassthrough 
{
	private one_referral oneR ;
	
	public UNITYVisaPassthrough ()
	{
		oneR = new one_referral () ;
	}
	
	public PageReference go ()
	{
		oneR.removeALLCookies () ;
		
		PageReference pr = Page.application_start ;
		return pr ;
	}
}