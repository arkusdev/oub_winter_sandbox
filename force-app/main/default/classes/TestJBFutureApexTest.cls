@isTest
public class TestJBFutureApexTest 
{
	public static testmethod void ONE ()
    {
        Contact c = new Contact () ;
        c.FirstName = 'Luigi' ;
        c.LastName = 'Mattera' ;
        c.Email = 'lmattera@oneunited.com' ;
        insert c ;
        
        Test.startTest () ;
        
        TestJBFutureApex.goEmail ( c.ID ) ;
        
        Test.stopTest () ;
    }
}