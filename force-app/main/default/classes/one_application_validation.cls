public with sharing class one_application_validation 
{
	private one_application__c o ;
	
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public one_application_validation ( ApexPages.Standardcontroller stdC )
    {
    	ApexPages.Standardcontroller theController = stdC ;
    	
		//  Get ID that is passed in, use it to get email address
        String appId = ((one_application__c) theController.getRecord ()).id ;
        
        if ( String.isNotBlank ( appId ) )
        {
	       	o = [ 
	       		SELECT id,Funding_Options__c, ACH_Funding_Status__c, Contact__c 
	       		FROM one_application__c 
	       		WHERE id = :appId 
	       		] ;
        }
    }
    
	public PageReference achRejection ()
	{
		//  Update the option
		if ( ( String.isNotBlank ( o.Funding_Options__c ) ) && 
		     ( o.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) 
		   )
		{
			if ( ( String.isNotBlank ( o.ACH_Funding_Status__c ) ) &&
				 ( ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Originated' ) ) || 
				   ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
				 ) 
			   )
			{
				o.ACH_Funding_Status__c = 'Rejected' ;
				update o ;
			}
		}
		
        //  Redirect the user back to the original page
        PageReference pageRef = new PageReference ( '/' + o.Id ) ;
        pageRef.setRedirect ( true ) ;
        return pageRef ;
	}
}