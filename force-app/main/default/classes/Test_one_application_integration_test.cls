@isTest (SeeAllData=true)
public with sharing class Test_one_application_integration_test 
{
	/*
	 * Creates a generic application I use for testing.
	 */
	private static one_application__c getApplicationForTests ()
	{
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey1.com';
		insert c1;
		
		one_application__c app = new one_application__c ();
		app.Number_of_Applicants__c = 'I will be applying invidually' ;
		
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.SSN__c = '123-45-6789' ;
		app.DOB__c = Date.newInstance ( 1960, 2, 17 ) ;
		app.Gross_Income_Monthly__c = 10000.00 ;

		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'F003' ;
		
		app.Trial_Deposit_1__c = 7 ;
		app.Trial_Deposit_2__c = 11 ;
		app.Application_Processing_Status__c = 'Quiz Present' ;
		
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app ;
		
		return app ;
	}
	
	static testmethod void testOne ()
	{
		Test.setMock ( WebServiceMock.class, new v003QueryobjectsSoapFisdsComMockImpl () ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_integration_test ;
		Test.setCurrentPage ( pr1 ) ;
		
		one_application__c app = getApplicationForTests () ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		one_application_integration_test qp = new one_application_integration_test ( sc ) ;
		
		//  Autorun the action
		qp.testFISLogin () ;
		
		//  End of testing
		Test.stopTest () ;
	}

}