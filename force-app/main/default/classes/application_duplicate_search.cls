public class application_duplicate_search 
{
    public one_application__c o { get; private set; }
    public list<one_application__c> oL { get; private set; }
    public list<one_application__c> oldL { get; private set; }
    public boolean hazException { get ; private set ; }
    public String exceptionError { get ; private set ; }
    public Integer oLsize { get ; private set ; }
    
    public boolean excludeIP { get ; private set ; }
    public list<String> ipExclusionL { get ; private set ; }
    private one_settings__c settings ;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public application_duplicate_search ( ApexPages.Standardcontroller stdC )
    {
        //  Settings
        settings = one_settings__c.getInstance ( 'settings' ) ;
        
        //  Setup
        o = null ;
        oL = null ;
        oldL = new list<one_application__c> () ;
        hazException = false ;
        exceptionError = null ;
		excludeIP = false ;        
        ipExclusionL = null ;
        
        //  Get ID that is passed in, use it to get email address
        String appID = ((one_application__c) stdC.getRecord ()).id ;
        
        if ( appID != null )
        {
            o = one_utils.getApplicationFromId ( appID ) ;
            oldL.add ( o ) ;
        }
        
        if ( ( settings != null ) && ( String.isNotEmpty ( settings.Duplicate_Search_IP_Exclusion__c ) ) )
        {
            if ( settings.Duplicate_Search_IP_Exclusion__c.contains ( ',' ) )
            {
                ipExclusionL = settings.Duplicate_Search_IP_Exclusion__c.split ( ',' ) ; 
            }
            else
            {
    	        ipExclusionL = new list<String> () ;
	    		ipExclusionL.add ( settings.Duplicate_Search_IP_Exclusion__c ) ;
            }
        }
        
        if ( o != null )
        {
            if ( ( ipExclusionL != null ) && ( ipExclusionL.size () > 0 ) )
            {
                for ( String ipExclusion : ipExclusionL )
                {
                    if ( o.Visitor_IP__c.equalsIgnoreCase ( ipExclusion ) )
                    {
                        excludeIP = true ;
                        break ;
                    }
                }
            }
            
            try
            {
                if ( excludeIP )
                {
                    oL = [
                        SELECT ID, Name, Application_Status__c,
                        RecordType.Name, Visitor_IP__c, Requested_Total_Funding__c, 
                        Funding_Options__c, Funding_Routing_Number__c, Funding_Account_Number__c, 
                        Contact__c, Contact__r.Name, Email_Address__c, 
                        Co_Applicant_Contact__c, Co_Applicant_Contact__r.Name, Co_Applicant_Email_Address__c,
                        Street_Address__c, ZIP_Code__c
                        FROM one_application__c
                        WHERE ID <> :o.Id
                        AND 
                        (
							   ( Funding_Options__c = 'E- Check (ACH)' 
                                 AND Funding_Account_Number__c <> NULL AND Funding_Account_Number__c = :o.Funding_Account_Number__c 
                                 AND Funding_Routing_Number__c <> NULL AND Funding_Routing_Number__c = :o.Funding_Routing_Number__c )
                            OR ( Email_Address__c <> NULL AND Email_Address__c = :o.Email_Address__c )
                            OR ( Contact__c <> NULL AND Contact__c = :o.Contact__c )
                            OR ( Co_Applicant_Email_Address__c <> NULL AND Co_Applicant_Email_Address__c = :o.Co_Applicant_Email_Address__c )
                            OR ( Co_Applicant_Contact__c <> NULL AND Co_Applicant_Contact__c = :o.Co_Applicant_Contact__c )
                            OR ( Address_Match__c = :o.Address_Match__c )
                        )
                        ORDER BY CreatedDate
                        LIMIT 1000
                    ] ;
                }
                else
                {
                    oL = [
                        SELECT ID, Name, Application_Status__c,
                        RecordType.Name, Visitor_IP__c, Requested_Total_Funding__c, 
                        Funding_Options__c, Funding_Routing_Number__c, Funding_Account_Number__c, 
                        Contact__c, Contact__r.Name, Email_Address__c, 
                        Co_Applicant_Contact__c, Co_Applicant_Contact__r.Name, Co_Applicant_Email_Address__c,
                        Street_Address__c, ZIP_Code__c
                        FROM one_application__c
                        WHERE ID <> :o.Id
                        AND 
                        (
                            Visitor_IP__c = :o.Visitor_IP__c
                            OR ( Funding_Options__c = 'E- Check (ACH)' 
                                 AND Funding_Account_Number__c <> NULL AND Funding_Account_Number__c = :o.Funding_Account_Number__c 
                                 AND Funding_Routing_Number__c <> NULL AND Funding_Routing_Number__c = :o.Funding_Routing_Number__c )
                            OR ( Email_Address__c <> NULL AND Email_Address__c = :o.Email_Address__c )
                            OR ( Contact__c <> NULL AND Contact__c = :o.Contact__c )
                            OR ( Co_Applicant_Email_Address__c <> NULL AND Co_Applicant_Email_Address__c = :o.Co_Applicant_Email_Address__c )
                            OR ( Co_Applicant_Contact__c <> NULL AND Co_Applicant_Contact__c = :o.Co_Applicant_Contact__c )
                            OR ( Address_Match__c = :o.Address_Match__c )
                        )
                        ORDER BY CreatedDate
                        LIMIT 1000
                    ] ;
                }
                
                if ( ( oL != null ) && ( oL.size () > 0 ) )
                {
                    oLsize = oL.size () ;
                }
            }
            catch ( Exception e )
            {
                hazException = true ;
                exceptionError = e.getStackTraceString () ;
            }
        }
    }
}