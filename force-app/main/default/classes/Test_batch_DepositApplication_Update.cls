@isTest
public with sharing class Test_batch_DepositApplication_Update 
{
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
	private static Deposit_Application__c getDepositApplication ( Integer i )
	{
		Deposit_Application__c da = new Deposit_Application__c () ;
		
		da.First_Name__c = 'dtest' + i ;
		da.Middle_Name__c = 'Er' ;
		da.Last_Name__c = 'duser' + i ;
		da.Email__c = 'dtest' + i + '@duser' + i + '.com' ;
		da.Applicant_SSN__c = '00101100' + i ;
		
		da.Address__c = '1313 Testingbird Lane' ;
		da.City__c = 'Boston' ;
		da.State__c = 'MA' ;
		da.Zip__c = '02110' ;
		da.Country__c = 'USA' ;
		
		da.Daytime_Phone__c = '617-555-000' + i  ;
		da.Day_Phone_Extension__c = '4357' ;
		da.Evening_Phone__c = '857-555-000' + i ;
		
		da.Confirmation_Number__c = 'XYZ123' ;

		return da ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Batch Test to update date
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testONE ()
    {
		setBatchSettings () ;
		
		System.debug ( '------------------------------ -------- ------------------------------' ) ;
		System.debug ( '------------------------------ test ONE ------------------------------' ) ;
		System.debug ( '------------------------------ -------- ------------------------------' ) ;
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	da1.Last_Updated__c = Date.today ().addDays ( -7 ) ;
    	da1.IP_Intelligence_Response__c = 'REVIEW' ;
    	da1.Watch_List_Response__c = 'Accept' ;
    	da1.Debit_History_Response__c = 'Accept' ;
    	da1.Account_History_Response__c = 'Review' ;
    	da1.Identity_Verification_Response__c = 'Review (These codes should trigger a review)' ;
    	da1.Current_Status__c = 'Review' ;
    	
    	insert da1 ;

    	Test.startTest () ;
    	
		batch_DepositApplication_Update b = new batch_DepositApplication_Update () ;
		Database.executeBatch ( b ) ;
    	
    	Test.stopTest () ;
    	
    	Deposit_Application__c da1x = [
    		SELECT ID, Last_Updated__c
    		FROM Deposit_Application__c
    		WHERE ID = :da1.ID
    	] ;
    	
    	System.assertEquals ( da1x.Last_Updated__c, Date.today () ) ;
    }
}