public with sharing class chatter_post 
{
    /*  ----------------------------------------------------------------------------------------
     *  External Variables
     *  ---------------------------------------------------------------------------------------- */
    public FeedItem fi { public get ; public set ; }
    public List<SelectOption> cgS { public get ; private set ; }
    public String cgId { public get ; public set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public chatter_post ( ApexPages.StandardController sc )
	{
		List<CollaborationGroup> cgL = [
			SELECT Id, Name
			FROM CollaborationGroup
		] ;
		
		cgS = new List<SelectOption>();
		cgS.add ( new SelectOption ( '', '- SET TO Created By -' ) ) ;
		
		if ( ( cgL != null ) && ( cgL.size () > 0 ) ) 
		{
			for ( CollaborationGroup cg : cgL )
			{
				 cgS.add ( new SelectOption ( cg.Id, cg.Name ) ) ;
			}
		}
		
		fi = new FeedItem () ;
	}
	
	public void postFeed ()
	{
		if ( String.isNotBlank ( cgId ) )
			fi.parentId = cgId ;
		else
			fi.parentId = fi.CreatedById ;
			
		insert fi ;
		
		fi = new FeedItem () ;
	}

}