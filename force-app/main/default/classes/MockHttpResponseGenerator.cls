@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock 
{
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Optionally, only send a mock response for a specific endpoint and method.
        System.debug ( '--> ' + req.getEndpoint () ) ;

		// Endpoint can change ...
		if ( req.getEndpoint().contains ( 'mandrillapp' ) )
		{
//	        System.assertEquals ( 'https://mandrillapp.com/api/1.0/messages/send-template.json', req.getEndpoint () ) ;
	        System.assertEquals ( 'POST', req.getMethod () ) ;
		}
		
		if ( req.getEndpoint().contains ( 'googleapis' ) )
		{
//	        System.assertEquals ( 'https://maps.googleapis.com/maps/api/geocode/json?address=USA&sensor=false', req.getEndpoint () ) ;
	        System.assertEquals ( 'GET', req.getMethod () ) ;
		}
        
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        res.setHeader ( 'Content-Type', 'application/json' ) ;
        res.setBody ( '{"foo":"bar"}' ) ;
        res.setStatusCode ( 200 ) ;
        return res ;
    }
}