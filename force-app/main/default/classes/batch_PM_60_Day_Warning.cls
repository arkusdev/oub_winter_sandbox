global class batch_PM_60_Day_Warning implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;

	global batch_PM_60_Day_Warning ()
	{
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
	}
	
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- START --------------------' ) ;
		String query ;
		
		Date d = Date.Today ().addDays ( 60 ) ;
		Datetime dt = DateTime.newInstance ( d.year(), d.month(), d.day() ) ;
		
		query  = '' ;
		query += 'SELECT ID, ' ;
		query += 'Name, ' ;
		query += 'Review_Frequency__c, ' ;
		query += 'Owner.Name, ' ;
		query += 'Owner.Email, ' ;
		query += 'Last_Board_Approval_Date__c, ' ;
		query += 'New_Approval_Date__c ' ;
		query += 'FROM ' ;
		query += 'Policy_Management__c ' ;
		query += 'WHERE ' ;
		query += 'New_Approval_Date__c = ' + dt.format('yyyy-MM-dd') + ' ' ;
		query += 'ORDER BY ' ;
		query += 'Owner.ID, ' ;
		query += 'Name ' ;
		query += '' ;
		
		System.debug ( '-------------------------------------------------------------------------------------------' ) ;
		System.debug ( query ) ;
		System.debug ( '-------------------------------------------------------------------------------------------' ) ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Policy_Management__c> iL )
	{
		System.debug ( '-------------------- EXECUTE --------------------' ) ;

		list<Messaging.SingleEmailMessage> mL = new list<Messaging.SingleEmailMessage> () ;
		
		String priorEmail = '' ;
		String message ;
		Messaging.SingleEmailMessage m ;
		
		if ( ( iL != null ) && ( iL.size () > 0 ) )
		{
			for ( Policy_Management__c pm : iL )
			{
				String dt = DateTime.newInstance ( pm.Last_Board_Approval_Date__c.year(), pm.Last_Board_Approval_Date__c.month(), pm.Last_Board_Approval_Date__c.day() ).format ('MM-d-YYYY' ) ;
				String link = URL.getSalesforceBaseUrl ().toExternalForm () + '/' + pm.ID ;
				
				if ( ! priorEmail.equalsIgnoreCase ( pm.Owner.Email ) )
				{
					if ( String.isNotBlank ( priorEmail ) )
					{
						//  Close message html						
						message += '</table>' ;
						message += '</body>' ;
						message += '</html>' ;
						
						m.setHTMLBody ( message ) ;
						
						System.debug ( 'Adding record of prior email to outgoing' ) ;
						mL.add ( m ) ;
					}
					
					priorEmail = pm.Owner.Email ;
					
					System.debug ( 'Setting up new email - ' + pm.Owner.Email ) ;
					
					//  Create message for each user
					m = new Messaging.SingleEmailMessage () ;
		
					List<String> toAddresses = new List<String> () ;
					toAddresses.add ( pm.Owner.Email ) ;
					
					m.setToAddresses ( toAddresses ) ;
					m.setSubject ( 'OUB Upcoming Policy Review' ) ;
					
					//  Start HTML
					message  = '' ;			
					message += '<html>' ;
					message += '<head>' ;
					message += '<title>Policy Management 60 Day Warning</title>' ;
					message += '</head>' ;
					message += '<body>' ;
					message += '<p>This is an automated email from the Policy Management system to notify you that the following policies are due for updating and board approval in 60 days.</p>' ;
					message += '<p>If there are no changes to the Policy, please provide written documentation stating that there are no changes to the Policy.</p>' ;
					
					//  Start Table
					message += '<table border="1">' ;
					message += '<tr>' ;
					message += '<th>Name</th><th>Review Frequency</th><th>Last Board Approval Date</th>' ;
					message += '</tr>' ;
					
					message += '<tr>' ;
					message += '<td><a href="' + link + '">' + pm.Name + '</a></td><td>' + pm.Review_Frequency__c + '</td><td>' + dt + '</td>' ;
					message += '</tr>' ;
	
				}
				else
				{
					message += '<tr>' ;
					message += '<td><a href="' + link + '">' + pm.Name + '</a></td><td>' + pm.Review_Frequency__c + '</td><td>' + dt + '</td>' ;
					message += '</tr>' ;
				}
				
			}
			
			// FINAL Message
			message += '</table>' ;
			message += '</body>' ;
			message += '</html>' ;
			m.setHTMLBody ( message ) ;
			mL.add ( m ) ;
			
			//  Don't email if running test
			if ( ! Test.isRunningTest () )
				Messaging.sendEmail ( mL ) ;
		}
		else
		{
			System.debug ( 'No policy emails found to send!' ) ;
		}
	}

	global void finish ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- FINISH --------------------' ) ;
		
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex PM 60 Day Warning Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}