@RestResource(urlMapping='/customers/*')
global with sharing class customers 
{
    // -----------------------------------------------------------------------------------
    //  POAST
    // -----------------------------------------------------------------------------------
    @HttpPost
    global static void doPost ()
    {
        RestRequest req = RestContext.request ;
        
        String PAN = req.requestURI.substring ( req.requestURI.lastIndexOf ( '/' ) + 1 ) ;
        String hPAN = EncodingUtil.convertToHex ( Crypto.generateDigest ( 'SHA-512', Blob.valueOf ( PAN ) ) ) ;
        
        System.debug ( ' PAN ::' + PAN ) ;
        System.debug ( 'hPAN ::' + hPAN ) ;
        
        String status = 'Not Found' ;
        
        // ?
        list<Service__c> sL = null ;
        
        try
        {
            sL = [
                SELECT ID, Card_Number__c, Contact__c, Primary_Checking__c, Expiration_Date__c,
                First_Name__c, Last_Name__c, Postal_Code__c, Email__c, Computed_Phone__c,
                Status__c
                FROM Service__c
                WHERE Card_Number_Other__c = :hPAN
                AND Expiration_Date__c > :Date.today ()
                AND Status__c = 'Active'
                ORDER BY Expiration_Date__c DESC
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        JSONGenerator g = JSON.createGenerator ( true ) ;
        
        Service__c s = null ;
        
        if ( ( sL != null ) && ( sL.size () > 0 ) )
        {
            //  Found data
            s = sL [ 0 ] ;
            
            //  Create the response object
            g.writeStartObject () ;
            
            if ( s.Card_Number__c.equalsIgnoreCase ( PAN ) )
            {
	            status = 'Success' ;
            
                System.debug ( 'Card Match!' ) ;
                
                g.writeFieldName ( 'Customer' ) ;
                g.writeStartObject () ;
                g.writeStringField ( 'customerid', s.Contact__c ) ;
                g.writeStringField ( 'accountid', s.Primary_Checking__c ) ;
                g.writeStringField ( 'firstname', s.First_Name__c ) ;
                g.writeStringField ( 'lastname', s.Last_Name__c ) ;
                g.writeStringField ( 'zipcode', s.Postal_Code__c ) ;
                g.writeStringField ( 'expirymonth', String.valueOf ( s.Expiration_Date__c.Month () ) ) ;
                g.writeStringField ( 'expiryyear', String.valueOf ( s.Expiration_Date__c.Year () ) ) ;
//                g.writeStringField ( 'email', s.Email__c ) ;
//                g.writeStringField ( 'phonenumber', s.Computed_Phone__c ) ;
                g.writeEndObject () ;
                
                g.writeStringField ( 'responsecode', '0' ) ;
                g.writeStringField ( 'responsemessage', 'success' ) ;
            }
            else
            {
                //  Create the response object
                g.writeStringField ( 'responsecode', '-1' ) ;
                g.writeStringField ( 'responsemessage', 'Not Found' ) ;
            }
                
            //  Done?
            g.writeEndObject () ;
        }
        else
        {
            //  Create the response object
            g.writeStartObject () ;
            g.writeStringField ( 'responsecode', '-1' ) ;
            g.writeStringField ( 'responsemessage', 'Not Found' ) ;
            g.writeEndObject () ;
        }
        
        //  Write the log
        parseLoadBody ( req.requestBody, PAN, status, s ) ;
        
        //  Set up the response
        RestResponse res = RestContext.response ;
        res.addHeader ( 'Content-Type', 'application/json' ) ;
        res.responseBody = Blob.valueOf ( g.getAsString () ) ;
    }
    
    // -----------------------------------------------------------------------------------
    //  Parse the input and create a record
    // -----------------------------------------------------------------------------------
    private static void parseLoadBody ( Blob body, String PAN, String status, Service__c s )
    {
        System.debug ( 'blob? :: ' + body.toString () ) ;
        System.debug ( 'PAN   :: ' + PAN ) ;
        System.debug ( 'Status:: ' + status ) ;
        
        GreenDotSimple gds = GreenDotSimple.parse ( body.toString () ) ;
        
        if ( gds != null )
        {
//            list<GreenDotSimple.options> gdsoL = gds.options ;
            
            Green_Dot_Transaction__c gdt = new Green_Dot_Transaction__c () ;
            gdt.PAN__c = PAN ;
            gdt.Status__c = status ;
            
            if ( s != null )
            {
                gdt.Contact__c = s.Contact__c ;
                gdt.Financial_Account__c = s.Primary_Checking__c ;
                gdt.Service__c = s.ID ;
            }
            
            gdt.Store_Name__c = gds.store_name ;
            gdt.Store_Address_1__c = gds.store_address1 ;
            gdt.Store_Address_2__c = gds.store_address2 ;
            gdt.Store_City__c = gds.store_city ;
            gdt.Store_State__c = gds.store_state ;
            gdt.Store_Zip__c = gds.store_zip ;
            gdt.JSON__c = body.toString () ;
            
            try
            {
            	insert gdt ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
}