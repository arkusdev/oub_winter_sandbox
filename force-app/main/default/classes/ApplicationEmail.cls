public class ApplicationEmail 
{
    /*  --------------------------------------------------------------------------------------------------
	 *  UGH
	 *  -------------------------------------------------------------------------------------------------- */	
    class AppData
    {
        public one_application__c app ;
        public map<String,boolean> ov ;
    }

	/*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */	
    public ApplicationEmail ()
    {
        // NYET!
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Setup
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static one_application__c getApplication ( ID appID )
    {
        one_application__c app = one_utils.getApplicationFromId ( appID ) ;
        
        return app ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Emails Calculations - ALL OF THEM!
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static map<String,boolean> getEmailStates ( one_application__c app )
    {
        one_settings__c settings = one_settings__c.getInstance ( 'settings' ) ;
        
		//  Settings!
		Integer trialValidationLimit = 3 ; // Default
        
		if ( settings.Trial_Deposit_Validation_Limit__c != null )
		{
			trialValidationLimit = settings.Trial_Deposit_Validation_Limit__c.intValue () ;
		}
        
        map<String,boolean> eMap = new map<String,boolean> () ;
        eMap.put ( 'sendTrialEmail', false ) ;
        
        integer trialDepositFailureCount = 0 ;
        if ( app.Trial_Deposit_Failure_Count__c != null )
            trialDepositFailureCount = app.Trial_Deposit_Failure_Count__c.intValue () ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  Trial Deposits
         *  -------------------------------------------------------------------------------------------------- */
        if ( ( app.Trial_Deposit_1__c != null ) && ( app.Trial_Deposit_2__c != null ) && ( String.isNotBlank ( app.ACH_Funding_Status__c ) ) )
        {
            if ( ( trialDepositFailureCount < trialValidationLimit ) && ( ! app.ACH_Funding_Status__c.equalsIgnoreCase ( 'Failed Verification' ) ) )
            {
                if ( app.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) || app.ACH_Funding_Status__c.equalsIgnoreCase ( 'Send Trial Deposit' ) )
                {
                    eMap.put ( 'sendTrialEmail', true ) ;
                }
            }
        }
        
        /*  --------------------------------------------------------------------------------------------------
         *  Return
         *  -------------------------------------------------------------------------------------------------- */	
        return eMap ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Trial Deposit Email
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void sendTrialDepositEmail ( one_application__c app )
    {
        List<String> appL = new List<String> () ;
        appL.add ( app.Id ) ;
        
        if ( ( app.ACH_Funding_Status__c.equalsIgnoreCase ( 'Send Trial Deposit' ) ) &&
            ( String.isNotBlank ( app.Trial_Deposit_Trace_ID__c ) ) &&
            ( app.Trial_Deposit_Batch_Date__c != null ) &&
            ( app.Trial_Deposit_Batch_Ticket__c != null ) )
        {
	        one_application__c upApp = new one_application__c () ;
            upApp.ID = app.ID ;
            upAPP.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
            
            update upApp ;
        }
        
        ApplicationEmailFunctions.sendTrialDepositEmails ( appL ) ;
    }
}