public class IBMWatsonWAVtoSpeechQueueable implements Queueable, Database.AllowsCallouts
{
	private list<ID> objectIDs ;
    
    private IBM_Watson_Settings__c iws ;
    
    public IBMWatsonWAVtoSpeechQueueable ( list<ID> objectIDs ) 
	{
        this.objectIDs = objectIDs ;
        
        iws = IBM_Watson_Settings__c.getOrgDefaults () ;
    }
    
    public void execute ( QueueableContext context )
	{
        map<ID,Case> caseMap = new map<ID, Case> () ;
        
        map<ID, list<IBMWatsonFile.FileBuilder>> iwfM = getAllAttachments ( objectIDs ) ;
        map<ID,Case> OLDcaseMap = getAllCases ( objectIDs ) ;
        
        if ( ( iwfM != null ) && ( iwfM.keyset ().size () > 0 ) ) 
        {
            IBMWatsonAuthenticator authenticator = new IBMWatsonIAMAuthenticator ( iws.API_Key__c ) ;
            
            for ( ID cID : iwfM.keySet () )
            {
                for ( IBMWatsonFile.FileBuilder iwf : iwfM.get ( cID ) )
                {
                    String fileName = iwf.build ().name () ;
                    
                    IBMSpeechToTextV1 istt = new IBMSpeechToTextV1 ( authenticator ) ;
                    istt.setServiceURL ( iws.API_URL__c ) ;
                    
                    IBMSpeechToTextV1Models.RecognizeOptions recognizeOptions = new IBMSpeechToTextV1Models.RecognizeOptionsBuilder()
                      .audio ( iwf.build () )
                      .contentType ( 'audio/wav' )
                      .addHeader ( 'Content-Type', 'application/octet-stream' )
                      .model ( 'en-US_NarrowbandModel' )
                      .build () ;
                    
                    try
                    {
                        IBMSpeechToTextV1Models.SpeechRecognitionResults srrs = istt.recognize ( recognizeOptions ) ;
    
                        if ( srrs != null )
                        {
                            if ( ( srrs.getWarnings () != null ) && ( srrs.getWarnings().size () > 0 ) )
                            {
                                System.debug ( '---------------------- WARNINGS ----------------------' ) ;
                                
                                for ( String s : srrs.getWarnings () )
                                {
                                    if ( caseMap.containsKey ( cID ) )
                                    {
                                        caseMap.get ( cID ).Voicemail_Translation__c += fileName + 'Warning ' + s ;
                                    }
                                    else
                                    {
                                        Case c = new Case () ;
                                        c.ID = cID ;
                                        c.Voicemail_Translation__c = fileName + ' :: Warning ' + s ;
                                        
                                        caseMap.put ( cID, c ) ;
                                    }
                                }
                                
                                System.debug ( '---------------------- END ----------------------' ) ;
                            }
                            
                            list<IBMSpeechToTextV1Models.SpeechRecognitionResult> ssrL = srrs.getResults () ;
                            
                            if ( ( ssrL != null ) && ( ssrL.size () > 0 ) )
                            {
                                System.debug ( '---------------------- RESULTS ----------------------' ) ;
                            
                                for ( IBMSpeechToTextV1Models.SpeechRecognitionResult ssr : ssrL )
                                {
                                    if ( ssr.getXFinal () == true )
                                    {
                                        List<IBMSpeechToTextV1Models.SpeechRecognitionAlternative> sraL = ssr.getAlternatives () ;
                                        
                                        if ( ( sraL != null ) && ( sraL.size () > 0 ) )
                                        {
                                            for ( IBMSpeechToTextV1Models.SpeechRecognitionAlternative sra : sraL )
                                            {
                                                if ( caseMap.containsKey ( cID ) )
                                                {
                                                    caseMap.get ( cID ).Voicemail_Translation__c += 'START :: ' + sra.getTranscript () + ':: END (' + fileName + ')' ;
                                                    caseMap.get ( cID ).Voicemail_Translation_Status__c = 'Translated' ;
                                                    
                                                    if ( OLDCaseMap.containsKey ( cID ) )
                                                    {
                                                        if ( String.isNotBlank ( OLDCaseMap.get ( cID ).Description ) )
                                                        	caseMap.get ( cID ).Description = OLDCaseMap.get ( cID ).Description + caseMap.get ( cID ).Voicemail_Translation__c ;
                                                    	else
                                                        	caseMap.get ( cID ).Description = caseMap.get ( cID ).Voicemail_Translation__c ;
                                                    }
                                                }
                                                else
                                                {
                                                    Case c = new Case () ;
                                                    c.ID = cID ;
                                                    c.Voicemail_Translation__c = 'START :: ' + sra.getTranscript () + ':: END (' + fileName + ') ' ;
                                                    c.Voicemail_Translation_Status__c = 'Translated' ;
                                                    
                                                    if ( OLDCaseMap.containsKey ( cID ) )
                                                    {
                                                        if ( String.isNotBlank ( OLDCaseMap.get ( cID ).Description ) )
	                                                        c.Description = OLDCaseMap.get ( cID ).Description + ' ' + c.Voicemail_Translation__c ;
                                                        else
	                                                        c.Description = c.Voicemail_Translation__c ;
                                                    }
                                                    
                                                    caseMap.put ( cID, c ) ;
                                                }
                                            }
                                        }
                                        
                                        break ;
                                    }
                                }
                            }
                            else
                            {
                                Case c = new Case () ;
                                c.ID = cID ;
                                c.Voicemail_Translation__c = 'NO RESULTS FOUND + (' + filename + ') ' ;
                                c.Voicemail_Translation_Status__c = 'Translated' ;
                                
                                caseMap.put ( cID, c ) ;
                            }
                        }
                    }
                    catch ( IBMWatsonServiceExceptions.BadRequestException bre )
                    {
                        IBMWatsonResponse iwr = bre.getResponse () ;
                        
                        Case c = new Case () ;
                        c.ID = cID ;
                        c.Voicemail_Translation__c = 'ERROR :: ' + iwr.getErrorMessage () + '(' + fileName + ') ' ;
                        c.Voicemail_Translation_Status__c = 'Error Translating' ;
                        
                        caseMap.put ( cID, c ) ;
                    }
                }
            }
        }
        
        if ( ( caseMap != null ) && ( caseMap.keySet ().size () > 0 ) )
        {
            try
            {
                update caseMap.values () ;
            }
            catch ( DMLException e )
            {
                // ?
            }
                
        }
    }
    
    //  --------------------------------------------------------------------------------
    //  Load attachments for parsing
    //  --------------------------------------------------------------------------------
    private static map<ID,list<IBMWatsonFile.FileBuilder>> getAllAttachments ( list<ID> xIDL )
    {
        map<ID,list<IBMWatsonFile.FileBuilder>> iwfM = new map<ID,list<IBMWatsonFile.FileBuilder>> () ;
        
        //  --------------------------------------------------------------------------------
        //  NEW
        //  --------------------------------------------------------------------------------
        list<ContentDocumentLink> cdlL = null ;
        
        try
        {
            cdlL = [
                SELECT ID, ContentDocumentId, LinkedEntityId
                FROM ContentDocumentLink 
                WHERE LinkedEntityId IN :xIDL
                AND isDeleted = false
            ] ;
        }
        catch ( DMLException e )
        {
            cdlL = null ;
        }
        
        if ( ( cdlL != null ) && ( cdlL.size () > 0 ) )
        {
            map<ID, ID> fooM = new map<ID, ID> () ;
            
            for ( ContentDocumentLink cdl : cdlL )
            {
                fooM.put ( cdl.ContentDocumentId, cdl.LinkedEntityId ) ;
            }
            
            list<ContentVersion> cvL = null ;
            
            try
            {
                cvL = [
                    SELECT ID, ContentDocumentId, VersionData, FileType, Title, Description 
                    FROM ContentVersion
                    WHERE ContentDocumentId IN :fooM.keySet()
                    AND FileType = 'WAV'
                    AND IsLatest = true
                ] ;
                
                if ( ( cvL != null ) && ( cvL.size () > 0 ) )
                {
                    for ( ContentVersion cv : cvL )
                    {
                        IBMWatsonFile.FileBuilder iwf = new IBMWatsonFile.FileBuilder () ;
                        iwf.body ( cv.VersionData ) ;
                        iwf.contentType ( 'audio/wav' ) ;
                        iwf.name ( cv.Title ) ;
                        iwf.description ( cv.Description ) ;
                        
                        ID pID = fooM.get ( cv.ContentDocumentId ) ;
                        
                        if ( iwfM.containsKey ( pID ) )
                        {
                            iwfM.get ( pID ).add ( iwf ) ;
                        }
                        else
                        {
                            list<IBMWatsonFile.FileBuilder> iwfL = new list<IBMWatsonFile.FileBuilder> () ;
	                    	iwfL.add ( iwf ) ;
                            
                            iwfM.put ( pID, iwfL ) ;
                        }
                    }
                }
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
        
        //  --------------------------------------------------------------------------------
        //  OLD
        //  --------------------------------------------------------------------------------

        try
        {
            list<Attachment> aL = [
                SELECT ID, Body, Name, Description, ContentType, ParentID
                FROM Attachment
                WHERE ParentId IN :xIDL
            ] ;
            
            if ( ( aL != null ) && ( aL.size () > 0 ) )
            {
                for ( Attachment a : aL )
                {
                    IBMWatsonFile.FileBuilder iwf = new IBMWatsonFile.FileBuilder () ;
                    iwf.attachment ( a ) ;
                    
                    if ( iwfM.containsKey ( a.ParentID ) )
                    {
                        iwfM.get ( a.ParentID ).add ( iwf ) ;
                    }
                    else
                    {
                        list<IBMWatsonFile.FileBuilder> iwfL = new list<IBMWatsonFile.FileBuilder> () ;
                        iwfL.add ( iwf ) ;
                        
                        iwfM.put ( a.parentID, iwfL ) ;
                    }
                }
            }
        }
        catch ( DMLException e )
        {
            // ?
        }

        //  --------------------------------------------------------------------------------
        //  DONE
        //  --------------------------------------------------------------------------------
        return iwfM ;
    }
    
    //  --------------------------------------------------------------------------------
    //  Load cases for parsing
    //  --------------------------------------------------------------------------------
    private static map<ID,Case> getAllCases ( list<ID> xIDL )
    {
        map<ID,Case> caseMap = new map<ID,Case> () ;
        
        try
        {
            caseMap = new map<ID,Case> ([
                SELECT ID, Description, Description_HTML__c
                FROM CASE
                WHERE ID IN :xIDL
            ]) ;
        }
        catch ( DMLException e )
        {
            caseMap = null ;
        }
        
        return caseMap ;
    }
}