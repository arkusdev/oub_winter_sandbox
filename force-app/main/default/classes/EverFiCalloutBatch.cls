global class EverFiCalloutBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts
{
    private static final String JOB_TYPE = 'Salesforce Batch Queuable' ;
    private static final String JOB_NAME = EverFiCalloutQueueable.class.getName () ;

    global Datetime startDT ;
    global String status ;
    global String errorMessage ;
    
    global EverFiHelper.EverFiReporting efr ;
    
    global EverFiCalloutBatch () 
    {
        status = 'WARN' ;
    }

    global Database.QueryLocator start ( Database.Batchablecontext bc )
    {
        System.debug ( '-------------------- START --------------------' ) ;
        
        String query ;
        query  = 'SELECT ID FROM User LIMIT 1' ;
        query += '' ;
        
        return Database.getQueryLocator ( query ) ;
    }
    
    global void execute ( Database.Batchablecontext bc, List<SObject> nyet )
    {
        /*  --------------------------------------------------------------------------------------------------
         *  Setup
         *  -------------------------------------------------------------------------------------------------- */
        startDT = Datetime.now () ;
        status = 'WARN' ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  YEARGH
         *  -------------------------------------------------------------------------------------------------- */
        boolean success = false ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  Login
         *  -------------------------------------------------------------------------------------------------- */
        EverFiHelper efh = new EverFiHelper () ;
        success = efh.everFiLogin () ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  Load
         *  -------------------------------------------------------------------------------------------------- */
        if ( success )
        {
            //  CALLOUT
            success = efh.loadProgramUsersSimple () ;
            
            //  DML
            if ( success )
            {
                success = efh.saveEverFiData () ;
                
                //  Update date in settings
                if ( success )
                {
                    success = efh.updateSince () ;
                }
                else
                {
                    errorMessage = efh.getErrorMessage () ;
                    status = 'ERROR' ;
                }
            }
            else
            {
                errorMessage = efh.getErrorMessage () ;
                status = 'ERROR' ;
            }
        }
        else
        {
            errorMessage = efh.getErrorMessage () ;
            status = 'ERROR' ;
        }
        
        /*  --------------------------------------------------------------------------------------------------
         *  Email results
         *  -------------------------------------------------------------------------------------------------- */
        efr = efh.getReporting () ;
        
        if ( ( efr.profileCount > 0 ) && ( efr.moduleCount > 0 ) && ( efr.programUserCount > 0 ) )
            status = 'GOOD' ;
    }
    
    global void finish ( Database.Batchablecontext bc )
    {
        System.debug ( '-------------------- FINISH --------------------' ) ;
        
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [
            SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()
        ] ;
      
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
        String[] toAddresses ;
        if ( ( efr != null ) && ( String.isNotBlank ( efr.email ) ) )
        {
            toAddresses = new String [] 
            {
                efr.email,
                'helpdesk@OneUnited.com' 
            } ;
        }
        else
        {
            toAddresses = new String [] 
            {
                a.CreatedBy.Email,
                'helpdesk@OneUnited.com' 
            } ;
        }
        
        if ( ( a.Status.equalsIgnoreCase ( 'Failed' ) ) || ( a.NumberOfErrors > 0 ) )
            status = 'ERROR' ;
        else if ( a.Status.equalsIgnoreCase ( 'Aborted' ) )
            status = 'WARN' ;
        else if ( ( a.TotalJobItems == 0 ) && ( a.NumberOfErrors == 0 ) )
            status = 'GOOD' ;
   
        mail.setToAddresses ( toAddresses ) ;
        mail.setSubject ( '[' + status + '][' + JOB_NAME + '][' + JOB_TYPE + '] Advocate Batch Calculator' ) ;
        
        String message = '' ;
		message += '<h1>Results from running the latest EverFI data load:</h1>' ;
		message += '<p>Data loaded since ' + efr.since + '.</p>' ;
		message += '<p>Date updated to   ' + efr.sinceThing + '.</p>' ;
		message += '<p>There were ' + efr.totalUserCount + ' user(s) found.</p>' ;
		message += '<p>There were ' + efr.profileCount + ' profile(s) loaded.</p>' ;
		message += '<p>There were ' + efr.moduleCount + ' program result(s) loaded.</p>' ;
		message += '<p>There were ' + efr.totalProgramUserCount + ' record(s) found.</p>' ;
		message += '<p>There were ' + efr.programUserCount + ' record(s) linked.</p>' ;
        message += '<br/>' ;
		message += '<p>There were ' + efr.calloutCount + ' callout(s).</p>' ;
		message += '<br/>' ;
        
        if ( status.equalsIgnoreCase ( 'error' ) )
            message += '<hr/>' + errorMessage + '<hr/>' ;
            
        message += '' ;
        
        mail.setPlainTextBody ( '' ) ;  // WTB no "null"
        mail.setHtmlBody ( message ) ;

        //  Don't email if running test
        if ( ! Test.isRunningTest () )
        {
            Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
        }
    }
}