public with sharing class AdvocateProgramIncludeController 
{
	public String advocateProgramCode { public get ; private set ; }
	public boolean validAdvocateCode { public get ; private set ; }
	
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public AdvocateProgramIncludeController ()
	{
		one_referral oneR = new one_referral () ;
		
		oneR.loadThirdPartyData () ;
        
        //  Something?
		one_referral.AdvocateProgramData apd = oneR.loadAdvocateProgramCode () ;
		advocateProgramCode = apd.advocateProgramCode ;
		validAdvocateCode = apd.validAdvocateCode ;
	}
}