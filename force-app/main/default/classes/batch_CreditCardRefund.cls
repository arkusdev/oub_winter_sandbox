global class batch_CreditCardRefund implements Database.Batchable<SObject>, Database.Stateful
{
	private final String INITIAL_STATUS = 'Outstanding Card Balance' ;
	private final String ENDING_STATUS  = 'Payment Clearing' ;
	private Batch_Process_Settings__c bps ;
	
	global batch_CreditCardRefund ()
	{
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
	}
	
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String recordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		String query ;
		
		// 1 day after?
		query = '' ;
		query += 'SELECT ' ;
		query += 't.ID, t.Current_Card_Balance__c, Financial_Account__r.NoteBal__c ' ;
		query += 'FROM Ticket__c t ' ; 
		query += 'WHERE t.Status__c = \'' + INITIAL_STATUS + '\' ' ;
		if ( String.isNotBlank ( recordTypeId ) )
			query += 'AND t.RecordTypeId = \'' + recordTypeId + '\' ' ;
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Ticket__c> tL )
	{
		List<Ticket__c> outTL = new List<Ticket__c> () ;
		
		for ( Ticket__c t : tL )
		{
			System.debug ( '1: ' + t.Financial_Account__r.NoteBal__c ) ;
			System.debug ( '2: ' + t.Current_Card_Balance__c ) ;
			
			if ( t.Current_Card_Balance__c == 0 )
			{
				t.Status__c = ENDING_STATUS ;
			
				outTL.add ( t ) ;
			}
		}
		
		if ( outTL.size () > 0 )
		{
			System.debug ( 'Processing ' + outTL.size () + ' records' ) ;
			update outTL ;
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
		   
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
		
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
		
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Batch Credit Card Refund Balance Check Job :: ' + a.Status ) ;
		
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
		
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}