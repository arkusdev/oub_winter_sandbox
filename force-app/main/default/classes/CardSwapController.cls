public with sharing class CardSwapController 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */	
    public CardSwapController ()
    {
        // Empty
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Go swap, user
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static String getSwapURL ()
    {
        Contact c ;
        
        try
        {
            User user = [
                SELECT ContactID 
                FROM User 
                WHERE Id = :UserInfo.getUserId () 
                LIMIT 1
            ] ;
            
            c = [
                SELECT ID, FirstName, LastName, Email, Phone,
                MailingAddress, MailingStreet, MailingCity, MailingState, MailingPostalCode
                FROM Contact
                Where Id = :user.ContactID
            ] ;
        }
        catch ( DMLException ex )
        {
            System.debug ( OneUnitedUtilities.parseStackTrace ( ex ) ) ;
        }
        
        Q2BillerHelper q2bh = new Q2BillerHelper () ;
        
        q2bh.setContact ( c ) ;
        
        String url = q2bh.doEverythingAndForward () ;
//        String url = q2bh.justForward () ;
       
        return url ;
    }
}