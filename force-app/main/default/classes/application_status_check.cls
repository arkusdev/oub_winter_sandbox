public with sharing class application_status_check 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security ?!@?
     *  ---------------------------------------------------------------------------------------- */
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;
    
	/* --------------------------------------------------------------------
	 * Page control variables
	 * -------------------------------------------------------------------- */
    public String ratesURLDisclosure { public get ; private set ; }
    public String liveAgentButton { public get ; private set ; }
    public Integer scStep { public get ; private set ; }
    
	/* --------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------- */
    private one_settings__c settings = null ;
    private String taskRecordTypeId = null ;
    
	/* --------------------------------------------------------------------
	 * Input variables
	 * -------------------------------------------------------------------- */
    public String appID { public get ; public set ; }
    public String mothersMaidenName { public get ; public set ; }
    
	/* --------------------------------------------------------------------
	 * Error variables
	 * -------------------------------------------------------------------- */
    public boolean errorAppID { public get ; private set ; }
    public boolean errorMothersMaidenName { public get ; public set ; }
    
	/* --------------------------------------------------------------------
	 * Application
	 * -------------------------------------------------------------------- */
    public one_application__c o { public get ; private set ; }
    
	/* --------------------------------------------------------------------
	 * Results
	 * -------------------------------------------------------------------- */
    public String title { public get ; private set ; }
    public String body { public get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Template Name
     *  ---------------------------------------------------------------------------------------- */
    public String templateName { public get ; private set ; }
    
	/* --------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------- */
    public application_status_check ( ApexPages.Standardcontroller stdC )
    {
		//  --------------------------------------------------------------------------------------------------
		//  Default template
		//  --------------------------------------------------------------------------------------------------
		templateName = 'Generic_Application_Template' ;
        
	    String pageName = ApexPages.CurrentPage().getUrl();
        
        if ( String.isNotBlank ( pageName ) )
        {
            if ( pageName.containsIgnoreCase ( 'application_status_check' ) )
                templateName = 'application_template' ;
            else if ( pageName.containsIgnoreCase ( 'deposit_status_check' ) )
                templateName = 'Deposit_Application_template_new' ;
        }
        
		//  --------------------------------------------------------------------------------------------------
        //  Default step
		//  --------------------------------------------------------------------------------------------------
        scStep = 9999 ;
        
		//  --------------------------------------------------------------------------------------------------
        //  Settings
		//  --------------------------------------------------------------------------------------------------
        settings = one_settings__c.getInstance ( 'settings' ) ;
        
        if ( settings != null )
        {
            //  Rate disclosure URL - DEFAULT
            ratesURLDisclosure = one_utils.getDisclosureURL ( settings.Product__c, settings.SubProduct__c, settings.Plan__c ) ;
            
            //  Live agent
            liveAgentButton = settings.Live_Agent_Button_ID__c ;
            
            //  Setup
            scStep = 1000 ;
            errorAppID = false ;
            errorMothersMaidenName = false ;
        }
        
		//  --------------------------------------------------------------------------------------------------
        //  TASK Record Type
		//  --------------------------------------------------------------------------------------------------
        taskRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Task', 'Task' ) ;
    }
    
	/* --------------------------------------------------------------------
	 * Go!
	 * -------------------------------------------------------------------- */
    public void submitAppInfo ()
    {
        errorAppID = true ;
        if ( String.isNotBlank ( appID ) )
            errorAppID = false ;
        
        errorMothersMaidenName = true ;
        if ( String.isNotBlank ( mothersMaidenName ) )
            errorMothersMaidenName = false ;
        
        // VALIDATION!
        if ( errorAppID || errorMothersMaidenName )
        {
            scStep = 1000 ;
        }
        else
        {
            scStep = 9000 ;
            
            List<one_application__c> oL = null ;
            try
            {
                oL = [
                   SELECT ID, Contact__c, Application_ID__c,
                    Product__c, SubProduct__c, Plan__c,
                    Number_of_Applicants__c,
                    First_Name__c, Last_Name__c, Email_Address__c,
                    Co_Applicant_First_Name__c, Co_Applicant_Last_Name__c, Co_Applicant_Email_Address__c,
                    Invalid_Email_Address__c, Invalid_Co_Applicant_Email_Address__c,
                    Application_Status__c, ACH_Funding_Status__c, Application_Processing_Status__c, FIS_Decision__c,
                    Phone__c, Street_Address__c, City__c, State__c, Zip_Code__c,
                    Co_Applicant_Phone__c, Co_Applicant_Street_Address__c, Co_Applicant_City__c, Co_Applicant_State__c, Co_Applicant_Zip_Code__c,
                    Promotion_Code__c,
                    OB_Registration_Text__c, Card_Delivery_Date__c,
                    Upload_Attachment_key__c, RecordType.DeveloperName
                    FROM one_application__c
                    WHERE Application_ID__c = :appID
                    AND MothersMaidenName__c = :mothersMaidenName
                    ORDER BY ID DESC
                ] ;
            }
            catch ( DMLException e )
            {
                // ?
            }
            
            if ( ( oL != null ) && ( oL.size () > 0 ) )
            {
                o = oL [ 0 ] ;
                
                if ( String.isNotBlank ( o.RecordType.DeveloperName ) )
                {
                    if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'Deposits' ) )
                        templateName = 'Deposit_Application_template_new' ;
                    else if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'UNITY_Visa' ) )
                        templateName = 'application_template' ; 
                }
                else
                {
                    templateName = 'application_template' ; 
                }
                
                //  Rate disclosure URL - UPDATE if not blank
                if ( String.isNotBlank ( o.Product__c ) && String.isNotBlank ( o.SubProduct__c ) && String.isNotBlank ( o.Plan__c ) )
                    ratesURLDisclosure = one_utils.getDisclosureURL ( o.Product__c, o.SubProduct__c, o.Plan__c ) ;
                
				//  Look up results via helper class
                applicationCheckStatusHelper acsh = new applicationCheckStatusHelper () ;
                
                application_check_status__c acs = acsh.getStatus ( o.RecordType.DeveloperName, o.Application_Status__c, o.Application_Processing_Status__c, o.ACH_Funding_Status__c ) ;
                
                if ( acs != null )
                {
                    System.debug ( 'Found!' ) ;
                    title = variableReplacer ( o, acs.Title__c ) ;
                    body = variableReplacer ( o, acs.Body__c ) ;
                }
                else
                {
                    System.debug ( 'MAJOR ERROR nothing found at all!' ) ;
                    title = 'ERROR' ;
                    body = 'ERROR' ;
                }
                
                //  Save the task
                saveAssociatedTask ( o ) ;
                
                //  Set the step
                scStep = 5000 ;
            }
        }
    }
    
    private String variableReplacer ( one_application__c o, String t )
    {
        String x = t ;
        
        //  Application stuff
        if ( String.isNotBlank ( o.Application_ID__c ) )
	        x = x.replaceAll ( 'Application_ID__c', o.Application_ID__c ) ;
        
        if ( String.isNotBlank ( o.Promotion_Code__c ) )
	        x = x.replaceAll ( 'Promotion_Code__c', o.Promotion_Code__c ) ;
        
        if ( String.isNotBlank ( o.ID ) )
        	x = x.replaceAll ( 'A_ID__c', o.ID ) ;
        	
        if ( String.isNotBlank ( o.OB_Registration_Text__c ) )
        	x = x.replaceAll ( 'OB_Registration_Text__c', o.OB_Registration_Text__c ) ;
        	
        if ( o.Card_Delivery_Date__c != null )
        	x = x.replaceAll ( 'Card_Delivery_Date__c', o.Card_Delivery_Date__c.month()+'/'+o.Card_Delivery_Date__c.day()+'/'+o.Card_Delivery_Date__c.year() ) ;
                 	
        if ( String.isNotBlank ( o.Upload_Attachment_key__c ) )
        	x = x.replaceAll ( 'Upload_Attachment_key__c', o.Upload_Attachment_key__c ) ;
        
        //  Applicant
        if ( String.isNotBlank ( o.First_Name__c ) )
        	x = x.replaceAll ( 'First_Name__c', o.First_Name__c ) ;
        
        if ( String.isNotBlank ( o.Last_Name__c ) )
        	x = x.replaceAll ( 'Last_Name__c', o.Last_Name__c ) ;
        
        if ( String.isNotBlank ( o.Email_Address__c ) )
        	x = x.replaceAll ( 'Email_Address__c', o.Email_Address__c ) ;
        
        if ( String.isNotBlank ( o.Phone__c ) )
        	x = x.replaceAll ( 'Phone__c', o.Phone__c ) ;
        
        if ( String.isNotBlank ( o.Street_Address__c ) )
        	x = x.replaceAll ( 'Street_Address__c', o.Street_Address__c ) ;
        
        if ( String.isNotBlank ( o.City__c ) )
        	x = x.replaceAll ( 'City__c', o.City__c ) ;
        
        if ( String.isNotBlank ( o.State__c ) )
    	    x = x.replaceAll ( 'State__c', o.State__c ) ;
        
        if ( String.isNotBlank ( o.Zip_Code__c ) )
        	x = x.replaceAll ( 'Zip_Code__c', o.Zip_Code__c ) ;

        //  Co-Applicant
        if ( String.isNotBlank ( o.Co_Applicant_First_Name__c ) )
	        x = x.replaceAll ( 'Co_Applicant_First_Name__c', o.Co_Applicant_First_Name__c ) ;
    
        if ( String.isNotBlank ( o.Co_Applicant_Last_Name__c ) )
	        x = x.replaceAll ( 'Co_Applicant_Last_Name__c', o.Co_Applicant_Last_Name__c ) ;
    
        if ( String.isNotBlank ( o.Co_Applicant_Email_Address__c ) )
	        x = x.replaceAll ( 'Co_Applicant_Email_Address__c', o.Co_Applicant_Email_Address__c ) ;
    
        if ( String.isNotBlank ( o.Co_Applicant_Phone__c ) )
	        x = x.replaceAll ( 'Co_Applicant_Phone__c', o.Co_Applicant_Phone__c ) ;
    
        if ( String.isNotBlank ( o.Co_Applicant_Street_Address__c ) )
	        x = x.replaceAll ( 'Co_Applicant_Street_Address__c', o.Co_Applicant_Street_Address__c ) ;
    
        if ( String.isNotBlank ( o.Co_Applicant_City__c ) )
        	x = x.replaceAll ( 'Co_Applicant_City__c', o.Co_Applicant_City__c ) ;

        if ( String.isNotBlank ( o.Co_Applicant_State__c ) )
	        x = x.replaceAll ( 'Co_Applicant_State__c', o.Co_Applicant_State__c ) ;
    
        if ( String.isNotBlank ( o.Co_Applicant_Zip_Code__c ) )
        	x = x.replaceAll ( 'Co_Applicant_Zip_Code__c', o.Co_Applicant_Zip_Code__c ) ;
        
		return x ;
    }
    
    private void saveAssociatedTask ( one_application__c o )
    {
        Task t = new Task () ;
        
        t.priority = 'Normal' ;
        t.status = 'Completed' ;
        t.subject = 'Application Status Checked' ;
        t.description = 'Website account inquiry' ;
        t.Type = 'Other' ;
        t.whatId = o.Id ;
		t.whoId = o.Contact__c ;
        t.recordTypeId = taskRecordTypeId ;
        
        try
        {
            if ( String.isNotBlank ( taskRecordTypeId ) )
                access.insertObject ( t ) ;
            else
                system.debug ( 'Cannot create task with default recordtype.' ) ;
        }
        catch ( Exception e )
        {
            System.debug ( 'Error inserting task' ) ;
        }
    }
}