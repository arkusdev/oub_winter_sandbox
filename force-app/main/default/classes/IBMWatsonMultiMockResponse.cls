@isTest
global class IBMWatsonMultiMockResponse implements HttpCalloutMock 
{
    public static Integer tokenResponseCode = 200 ;
    public static Integer endPointResponseCode = 200 ;
    
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        
		// Endpoint can change ...
		if ( req.getEndpoint().contains ( 'token' ) )
		{
            res.setBody ( IBMWatsonMockResponses.validTokenData () ) ;
            res.setStatusCode ( tokenResponseCode ) ;
        }
        else if ( req.getEndpoint().contains ( 'recognize' ) )
        {
            if ( endPointResponseCode == 200 )
            {
                res.setBody ( IBMWatsonMockResponses.speechToTextRecognize () ) ;
                res.setStatusCode ( endPointResponseCode ) ;
            }
            else if ( endPointResponseCode == 400 )
            {
                res.setBody ( '{ "code_description": "Bad Request", "code": 400, "error": "No speech detected for 30s." }' ) ;
                res.setStatusCode ( endPointResponseCode ) ;
            }
        }
        
        // return
        return res ;
    }
}