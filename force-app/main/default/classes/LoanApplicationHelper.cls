public class LoanApplicationHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  RecordTypes!
	 *  -------------------------------------------------------------------------------------------------- */	
    private static List<RecordType> rtL ;
    
    private static void loadRecordTypeList ()
    {
        List<String> foo = new List<String> () ;
        foo.add ( 'PPP' ) ;
        foo.add ( 'Mortgagebot' ) ;
        foo.add ( 'Customer Org' ) ;
        
        rtL = OneUnitedUtilities.getRecordTypeList ( foo ) ;
    }
    
    public static String getPPPRecordTypeID ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'PPP', 'Loan_Application__c' ) ;
    }
    
    public static String getMortgagebotRecordTypeID ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Mortgagebot', 'Loan_Application__c' ) ;
    }
    
    public static String getCustomerOrgRecordTypeID ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Customer Org', 'Account' ) ;
    }
        
	/*  --------------------------------------------------------------------------------------------------
	 *  Internals
	 *  -------------------------------------------------------------------------------------------------- */	
    private static String loanOpportunityRT ;
    private static String prospectRT ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Maps for tracking
	 *  -------------------------------------------------------------------------------------------------- */	
    public static map <String, map<ID,Loan_Application__c>> laCMap ;
    public static map <String, map<ID,Loan_Application__c>> laAMap ;
    public static map <String, map<ID,Loan_Application__c>> laOOMap ;
    
    /* -----------------------------------------------------------------------------------
     * Record Type #1
     * ----------------------------------------------------------------------------------- */
    public static String getLoanOpportunityRT ()
    {
        if ( loanOpportunityRT == null )
        {
            loanOpportunityRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
        }
        
        return loanOpportunityRT ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Record Type #2
     * ----------------------------------------------------------------------------------- */
    public static String getProspectRT ()
    {
        if ( prospectRT == null )
        {
            prospectRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        }
        
        return prospectRT ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Contacts via name/email and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,Contact> getPossibleContactsByNameEmail ( list<String> iL )
    {
        map<String,Contact> xMap = null ;
		list<Contact> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, Name_Email_Lookup__c
                    FROM Contact
                    WHERE Name_Email_Lookup__c IN :iL
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,Contact> () ;
            
            for ( Contact c : xL )
            {
                if ( ! xMap.containsKey ( c.Name_Email_Lookup__c ) )
	                xMap.put ( c.Name_Email_Lookup__c, c ) ;
            }
        }
        
        return xMap ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Contacts via name/email and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,Contact> getPossibleContactsByTaxID ( list<String> iL )
    {
        map<String,Contact> xMap = null ;
		list<Contact> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, TaxID__c
                    FROM Contact
                    WHERE TaxID__c IN :iL
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,Contact> () ;
            
            for ( Contact c : xL )
            {
                if ( ! xMap.containsKey ( c.TaxID__c ) )
	                xMap.put ( c.TaxID__c, c ) ;
            }
        }
        
        return xMap ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Accounts via name/email and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,Account> getPossibleAccountsByFederalTaxID  ( list<String> iL )
    {
        map<String,Account> xMap = null ;
		list<Account> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, FederalTaxID__c 
                    FROM Account
                    WHERE FederalTaxID__c IN :iL
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,Account> () ;
            
            for ( Account a : xL )
            {
                if ( ! xMap.containsKey ( a.FederalTaxID__c  ) )
	                xMap.put ( a.FederalTaxID__c, a ) ;
            }
        }
        
        return xMap ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Financial accounts via # and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,Financial_Account__c> getPossibleFinancialAccounts ( list<String> iL )
    {
        map<String,Financial_Account__c> xMap = null ;
		list<Financial_Account__c> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, ACCTNBR__c
                    FROM Financial_Account__c
                    WHERE ACCTNBR__c IN :iL
                    AND RecordType.Name = 'Loan'
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,Financial_Account__c> () ;
            
            for ( Financial_Account__c fa : xL )
            {
                if ( ! xMap.containsKey ( fa.ACCTNBR__c ) )
	                xMap.put ( fa.ACCTNBR__c, fa ) ;
            }
        }
        
        return xMap ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Financial accounts via # and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,oneOpportunity__c> getPossibleOpportunitiesByEmail ( list<String> iL )
    {
        map<String,oneOpportunity__c> xMap = null ;
		list<oneOpportunity__c> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, Email__c, Opportunity_Contact__c
                    FROM oneOpportunity__c
                    WHERE Email__c IN :iL
                    AND RecordType.Name = 'Loan Opportunity'
                    AND Closed_Date__c = NULL
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,oneOpportunity__c> () ;
            
            for ( oneOpportunity__c oo : xL )
            {
                if ( ! xMap.containsKey ( oo.Email__c ) )
	                xMap.put ( oo.Email__c, oo ) ;
            }
        }
        
        return xMap ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Financial accounts via # and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,oneOpportunity__c> getPossibleOpportunitiesByContact ( list<String> iL )
    {
        map<String,oneOpportunity__c> xMap = null ;
		list<oneOpportunity__c> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, Opportunity_Contact__c
                    FROM oneOpportunity__c
                    WHERE Opportunity_Contact__c IN :iL
                    AND RecordType.Name = 'Loan Opportunity'
                    AND Closed_Date__c = NULL
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,oneOpportunity__c> () ;
            
            for ( oneOpportunity__c oo : xL )
            {
                if ( ! xMap.containsKey ( oo.Opportunity_Contact__c ) )
	                xMap.put ( oo.Opportunity_Contact__c, oo ) ;
            }
        }
        
        return xMap ;
    }
}