@isTest
public class test_customers_controller 
{
    // -----------------------------------------------------------------------------------
	// Load and return a account
	// -----------------------------------------------------------------------------------
    private static Account getAccount ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        
        return a ;
    }
    
    // -----------------------------------------------------------------------------------
	// Load and return a branch
    // -----------------------------------------------------------------------------------
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		return b ;
	}
	
    // -----------------------------------------------------------------------------------
	// Load and return a contact
    // -----------------------------------------------------------------------------------
	private static Contact getContact ( Account a, Branch__c b, Integer i )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'x' + i + 'test'+ i + 'person@testing.com' ;
		c.phone = '00' + i + '-00' + i + '-000' + i ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
        c.AccountId = a.ID ;
		
		return c ;
	}
    
    /* -----------------------------------------------------------------------------------
	 * Load and return a financial account
	 * ----------------------------------------------------------------------------------- */
    private static Financial_Account__c getFA ( Contact c )
    {
        Financial_Account__c fa = new Financial_Account__c  () ;
        fa.TAXRPTFORPERSNBR__c = c.ID ;
        
        return fa ;
    }
    
    // -----------------------------------------------------------------------------------
	// Load and return a service
    // -----------------------------------------------------------------------------------
    private Static Service__c getService ( Contact c, Financial_Account__c fa, String cn )
    {
        Service__c s = new Service__c () ;
        s.Primary_Checking__c = fa.ID ;
        s.Owner_Contact__c = c.ID ;
        s.Contact__c = c.ID ;

        s.Card_Number__c = cn ;
        s.Card_Number_Other__c = EncodingUtil.convertToHex ( Crypto.generateDigest ( 'SHA-512', Blob.valueOf ( cn ) ) ) ;
        s.Expiration_Date__c = Date.today().addDays ( 180 ) ;
        s.Agreement_Type__c = 'DBTC' ;
        s.Status__c = 'Active' ;
        
        return s ;
    }
    
    // -----------------------------------------------------------------------------------
	// Load and return a .. json?
    // -----------------------------------------------------------------------------------
    private static String getJSONBody ()
    {
        JSONGenerator g = JSON.createGenerator ( false ) ;
        
        g.writeStartObject () ;
        g.writeStringField ( 'store_name', 'xyz bank' ) ;
        g.writeStringField ( 'store_address1', '100 Franklin Street' ) ;
        g.writeStringField ( 'store_address2', 'Suite 600' ) ;
        g.writeStringField ( 'store_city', 'Boston' ) ;
        g.writeStringField ( 'store_state', 'MA' ) ;
        g.writeStringField ( 'store_zip', '02110' ) ;
        g.writeFieldName ( 'options' ) ;
        g.writeStartArray () ;
        g.writeStartObject () ;
        g.writeStringField ( 'option1', 'value1' ) ;
        g.writeEndObject () ;
        g.writeStartObject () ;
        g.writeStringField ( 'option2', 'value2' ) ;
        g.writeEndObject () ;
        g.writeEndArray () ;
        g.writeEndObject () ;
        
        return g.getAsString () ;
    }

    // -----------------------------------------------------------------------------------
	// Go - POAST
    // -----------------------------------------------------------------------------------
	static testmethod void FOUND ()
    {
        Account a = getAccount () ;
        insert a ;
        
        Branch__c b = getBranch () ;
        insert b ;
        
        Contact c1 = getContact ( a, b, 1 ) ;
        insert c1 ;
        
        Financial_Account__c fa1 = getFA ( c1 ) ;
        fa1.MJACCTTYPCD__c = 'CK' ;
        fa1.ACCTNBR__c ='1230001234' ;
        fa1.PRODUCT__c = 'U2 Checking' ;
        fa1.CURRACCTSTATCD__c = 'ACT' ;
        insert fa1 ;
        
        Service__c s1 = getService ( c1, fa1, '1111222233334444' ) ;
        insert s1 ;
        
        Test.startTest () ;
        
        RestRequest request = new RestRequest () ;
        request.requestUri = '/services/apexrest/customers/' + s1.Card_Number__c ;
        request.httpMethod = 'POST' ;
        request.requestBody = Blob.valueOf ( getJSONBody () ) ;
        
        RestContext.request = request ;
        RestContext.response = new RestResponse () ;
        customers.doPost () ;
        
        Blob foo = RestContext.response.responseBody ;
        System.debug ( foo.toString () ) ;
        
        Test.stopTest () ;
    }
    
    // -----------------------------------------------------------------------------------
	// Go - POAST
    // -----------------------------------------------------------------------------------
	static testmethod void NOT_FOUND ()
    {
        Test.startTest () ;
        
        RestRequest request = new RestRequest () ;
        request.requestUri = '/services/apexrest/customers/1001100210031004' ;
        request.httpMethod = 'POST' ;
        request.requestBody = Blob.valueOf ( getJSONBody () ) ;
        
        RestContext.request = request ;
        RestContext.response = new RestResponse () ;
        customers.doPost () ;
        
        Blob foo = RestContext.response.responseBody ;
        System.debug ( foo.toString () ) ;
        
        Test.stopTest () ;
    }
}