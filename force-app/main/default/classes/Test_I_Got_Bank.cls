@isTest
public class Test_I_Got_Bank 
{
	private static Web_Settings__c getSettings ( Date contestEnd, Date contestWinner )
    {
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.I_Got_Bank_Banner__c = 'https://www.oneunited.com' ;
        ws.I_Got_Bank_Contest_End_Date__c = contestEnd ;
        ws.I_Got_Bank_Contest_Winner_Date__c = contestWinner ;
        ws.I_Got_Bank_Contest_Winner_Award__c = '$99,999' ;
        ws.I_Got_Bank_Contest_Winner_Number__c = 'Ninety-Nine (99)' ;

        return ws ;
    }
    
    public static testmethod void testNoContest ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( -100 ), Date.today().addDays ( -90 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank igb = new I_Got_Bank ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 2000 ) ;
        
        Test.stopTest () ;
    }

    public static testmethod void testEmpty ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank igb = new I_Got_Bank ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1000 ) ;
        
        Test.stopTest () ;
    }

    public static testmethod void testError ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank igb = new I_Got_Bank ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1000 ) ;
        
        igb.iGotBank () ;
        
        System.assertEquals ( igb.dStepNumber, 1001 ) ;
        
        Test.stopTest () ;
    }

    public static testmethod void testSuccess ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank igb = new I_Got_Bank ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1000 ) ;
        
        igb.ticket.First_Name__c = 'Test' ;
        igb.ticket.Last_Name__c = 'Parent' ;
        igb.ticket.Email_Address__c = 'Test@Parent.com' ;
        igb.ticket.Phone__c = '6179018745' ;
        
        igb.ticket.Youth_Name__c = 'Test Brat' ;
        igb.ticket.School__c = 'Test School' ;
        igb.pickYouthDate = '11/11/2011' ;
        igb.ticket.Youth_Home_Phone__c = '6174574423' ;
        igb.ticket.Street_Address__c = '100 Franklin Street' ;
        igb.ticket.City__c = 'Boston' ;
        igb.ticket.State__c = 'MA' ;
        igb.ticket.Zip_Code__c = '02110' ;
        
        igb.dropdownPick = 'Essay' ;
        igb.ticket.Essay__c = 'Blah blah blah' ;
        
        igb.ticket.I_Got_Bank_Disclaimer__c = 'Yes' ;
        
        //  Submit
        igb.iGotBank () ;
        
        System.assertEquals ( igb.dStepNumber, 1100 ) ;
        
        Test.stopTest () ;
    }
}