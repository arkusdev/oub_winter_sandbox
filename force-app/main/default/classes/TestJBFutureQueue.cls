public class TestJBFutureQueue implements Queueable, Database.AllowsCallouts
{
    private String xID ;
    
    public TestJBFutureQueue ( String xID )
    {
        this.xID = xID ;
    }
    
    public void execute(QueueableContext context) 
    {
        Contact c = [
            SELECT ID, FirstName, LastName
            FROM Contact
            WHERE ID = :xID
        ] ;
        
        System.debug ( 'Found : ' + c.FirstName + ' ' + c.LastName ) ;
    }
}