global class ContactDeDuplication implements Database.Batchable<SObject>, Database.Stateful
{
	global integer deDuplicationCount = 0 ;
	
	global ContactDeDuplication ()
	{
		// ?
	}
	
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		List<aggregateResult> aR ;
		
		//  Grab aggregate results
		if ( Test.isRunningTest() )
		{
			aR = [
				SELECT c.uploadid__c 
				FROM Contact c 
				WHERE c.uploadId__c LIKE 'INSIGHT-P-%'
				GROUP BY c.UploadID__c 
				HAVING COUNT ( c.id ) >= 1
				LIMIT 140
			] ;
		}
		else
		{
			aR = [
				SELECT c.uploadid__c 
				FROM Contact c 
				WHERE c.uploadId__c LIKE 'INSIGHT-P-%'
				GROUP BY c.UploadID__c 
				HAVING COUNT ( c.id ) > 1
				LIMIT 140
			] ;
		}
		
		deDuplicationCount += aR.size () ;

		List<String> uIDs = new List<String> () ;
		
		//  Load results into List
		for ( aggregateResult a : aR )
		{
			uIDs.add ( (String) a.get ( 'uploadid__c' ) ) ;
		}
		
		//  Load contacts from list
		String cQ = DynamicObjectHandler.getUpdateableFieldQuery ( new Contact(), 'uploadID__c IN :uIDs', 'uploadID__c, CreatedDate' ) ;
		
		return Database.getQueryLocator ( cQ ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Contact> cL )
	{
		//  Merge?
		Contact m = null ;
		List<Contact> mL = new List<Contact> () ;
		List<Contact> mXL = new List<Contact> () ;
		
		List<String> fieldThingy = DynamicObjectHandler.getUpdateableFields ( new Contact () ) ;
				
		for ( Contact c : cL )
		{
			if ( m == null )
				m = c ;

			if ( ( m <> null ) && ( m.uploadId__c.equalsIgnoreCase ( c.UploadID__c ) ) && ( m.Id <> c.Id ) )
			{
				mL.add ( c ) ;
				
				System.debug ( '---- Mapping in missing values ----' ) ;
				for ( String y : fieldThingy )
				{
					if ( ( c.get ( y ) != null ) && ( m.get ( y ) == null ) )
						m.put ( y, c.get ( y ) ) ;
				}
			}
				
			if ( ( m <> null ) && ( ! m.uploadId__c.equalsIgnoreCase ( c.UploadID__c ) ) )
			{
				// Break for merge!
				System.debug ( '-------------------------------------------------' ) ; 
				System.debug ( 'Master! = ' + m.Id + ' --> ' + m.uploadId__c ) ;
				System.debug ( '---- BEGIN duplicates ----' ) ;
				
				//  Convert to IDs
				List<String> sL = new List<String> () ;
				for ( Contact x : mL )
				{
					sL.add ( x.Id ) ;
					System.debug ( '-- ' + x.ID ) ;
				}
				
				System.debug ( '---- Add master to update list ----' ) ;
				mXL.add ( m ) ;
				
				if ( sL.size () > 0 )
				{
					System.debug ( '---- BEGIN merge ----' ) ;
					List<Database.MergeResult> rL = Database.merge ( m, sL ) ;
					
					for ( Database.MergeResult r : rL )
					{
						System.debug ( r.isSuccess () ) ;
						System.debug ( r.getErrors () ) ;
					}
				}
				else
				{
					System.debug ( '---- Nothing found to merge ----' ) ;
				}
				
 				m = c ;
				mL = new List<Contact> () ;
			}
		}
		
		// FINAL MERGE !
		System.debug ( '-------------------------------------------------' ) ; 
		System.debug ( 'Master! = ' + m.Id + ' --> ' + m.uploadId__c ) ;
		System.debug ( '---- BEGIN duplicates ----' ) ;
		
		//  Convert to IDs
		List<String> sL = new List<String> () ;
		for ( Contact x : mL )
		{
			sL.add ( x.Id ) ;
			System.debug ( '-- ' + x.ID ) ;
		}
		
		if ( sL.size () > 0 )
		{
			System.debug ( '---- BEGIN merge ----' ) ;
			List<Database.MergeResult> rL = Database.merge ( m, sL ) ;
		}
		else
		{
			System.debug ( '---- Nothing found to merge ----' ) ;
		}
		
		System.debug ( '===== = FINAL UPDATE = =====' ) ;
		update mXL ; 
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
	   // Get the ID of the AsyncApexJob representing this batch job
	   // from Database.BatchableContext.
	   // Query the AsyncApexJob object to retrieve the current job's information.
	   AsyncApexJob a = [
	   	SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	      TotalJobItems, CreatedBy.Email
	      FROM AsyncApexJob WHERE Id =
	      :BC.getJobId()
	      ] ;
	      
	   // Send an email to the Apex job's submitter notifying of job completion.
	   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
	   
	   String[] toAddresses = new String [] 
	   {
	   		a.CreatedBy.Email 
	   } ;
	   
	   mail.setToAddresses ( toAddresses ) ;
	   mail.setSubject ( 'Apex Contact De-Duplication Job :: ' + a.Status ) ;
	   
	   String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.  There were ' + deDuplicationCount + ' records found.' ;
	   
	   mail.setPlainTextBody ( message ) ;
	   
	   // Only send the email on deduplication found
	   if ( ( a.NumberOfErrors > 0 ) || ( deDuplicationCount > 0 ) )
		   Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
	}
}