@isTest
public class Test_I_Got_Bank_Response 
{
	private static Web_Settings__c getSettings ( Date contestEnd, Date contestWinner )
    {
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.I_Got_Bank_Banner__c = 'https://www.oneunited.com' ;
        ws.I_Got_Bank_Contest_End_Date__c = contestEnd ;
        ws.I_Got_Bank_Contest_Winner_Date__c = contestWinner ;
        ws.I_Got_Bank_Contest_Winner_Award__c = '$99,999' ;
        ws.I_Got_Bank_Contest_Winner_Number__c = 'Ninety-Nine (99)' ;

        return ws ;
    }
    
    public static testmethod void testNoContest ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( -100 ), Date.today().addDays ( -90 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 2000 ) ;
        
        Test.stopTest () ;
    }

    public static testmethod void testEmpty ()
    {
        //  Setup
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 3000 ) ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void testWrongTicket ()
    {
        //  Setup
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
        //  Junk Ticket
        Ticket__c t1 = new Ticket__c () ;
        t1.Security_Key__c = one_utils.getRandomString ( 30 ) ;
        insert t1 ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', t1.id );
        ApexPages.currentPage().getParameters().put ( 'kid', t1.Security_Key__c );
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 3000 ) ;
        Test.stopTest () ;
    }
    
    public static testmethod void testEssayTicket ()
    {
        //  Setup
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
        //  Essay Ticket
        Ticket__c t1 = new Ticket__c () ;
        t1.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'I Got Bank', 'Ticket__c' ) ;
        t1.Security_Key__c = one_utils.getRandomString ( 30 ) ;
        t1.Youth_Artwork_Essay_Choice__c = 'Essay' ;
        t1.Status__c = 'Recieved' ;
        insert t1 ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', t1.id );
        ApexPages.currentPage().getParameters().put ( 'kid', t1.Security_Key__c );
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1600 ) ;
        Test.stopTest () ;
    }
    
    public static testmethod void testBadAttachment ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        t.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'I Got Bank', 'Ticket__c' ) ;
        t.Status__c = 'Received' ;
        t.Youth_Artwork_Essay_Choice__c = 'Artwork' ;
        t.Security_Key__c = one_utils.getRandomString ( 30 ) ;
        insert t ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1000 ) ; //  Initial State
        
        igb.iGotBankUpload () ;
        
        System.assertEquals ( igb.dStepNumber, 1001 ) ; //  Whoops, error
        
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Thingy.exe' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.IsPrivate = false ;
        
        igb.attachment = a1 ;  //  set attachment manually
        
        igb.iGotBankUpload () ;
        
        System.assertEquals ( igb.dStepNumber, 1001 ) ; //  nope
        
        Test.stopTest () ;
    }
    
    public static testmethod void testSubmitFlow ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        t.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'I Got Bank', 'Ticket__c' ) ;
        t.Status__c = 'Received' ;
        t.Youth_Artwork_Essay_Choice__c = 'Artwork' ;
        t.Security_Key__c = one_utils.getRandomString ( 30 ) ;
        insert t ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1000 ) ; //  Initial State
        
        igb.iGotBankUpload () ;
        
        System.assertEquals ( igb.dStepNumber, 1001 ) ; //  Whoops, error
        
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Thingy.pdf' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.IsPrivate = false ;
        
        igb.attachment = a1 ;  //  set attachment manually
        
        igb.iGotBankUpload () ;
        
        System.assertEquals ( igb.dStepNumber, 1100 ) ; //  Success
        
        Test.stopTest () ;
    }
    
    public static testmethod void testNoDuplicates ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Response ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Ticket__c t = new Ticket__c () ;
        t.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'I Got Bank', 'Ticket__c' ) ;
        t.Status__c = 'Responded' ;
        t.Youth_Artwork_Essay_Choice__c = 'Artwork' ;
        t.Security_Key__c = one_utils.getRandomString ( 30 ) ;
        insert t ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Response igb = new I_Got_Bank_Response ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1500 ) ; //  Initial State
        
        Test.stopTest () ;
    }
}