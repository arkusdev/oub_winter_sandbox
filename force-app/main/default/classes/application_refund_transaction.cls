public class application_refund_transaction {

    public String errorMsg {get;set;}
    public String amount {get;set;}
    public String transactionId {get;private set;}
    public Application_Transaction__c appTransaction {get;set;}
    
    public application_refund_transaction(ApexPages.StandardController stdController){
        errorMsg = '';
        appTransaction = (Application_Transaction__c)stdController.getRecord();
        transactionId = ApexPages.currentPage().getParameters().get('transactionId');
    }
    
    public void refund(){
        if(amount!=null){
            
            USAePay_SOAP_Actions usaepayActions = new USAePay_SOAP_Actions('Settings_Unity_Visa');
            usaepayActions.transactionParameters.refNum = transactionId;
            usaepayActions.transactionParameters.amount = amount;
            String clientIP = Apexpages.currentPage().getHeaders().get('X-Salesforce-SIP');
            usaepayActions.TransactionParameters.clientIP = clientIP;
            
            USAePay_SOAP_Actions.ResponseObject response = usaepayActions.RefundTransaction();
            
            if(response.statusCode == 200){
                //SUCCESS
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'New transaction number: ' + response.response));
                appTransaction.Status__c = 'Refunded';
                appTransaction.Transaction_Refunded_Date_Time__c = System.now();
                update appTransaction;
                
                //Creating the Application Transaction record
/*                Application_Transaction__c newAppTransaction = new Application_Transaction__c();
                newAppTransaction.Customer_Number__c  = appTransaction.Customer_Number__c;
                newAppTransaction.Payment_Method_ID__c  = appTransaction.Payment_Method_ID__c;
                newAppTransaction.Settled_Amount__c  = appTransaction.Settled_Amount__c;
                newAppTransaction.Amount__c  = appTransaction.Amount__c;
                newAppTransaction.Application__c = appTransaction.Application__c;
                newAppTransaction.Transaction_Ref_Num__c  = response.response;
                newAppTransaction.Status__c  = 'Settled';
                newAppTransaction.Transaction_Authorized_Date_Time__c = System.now();
                newAppTransaction.Transaction_Settled_Date_Time__c = System.now();
                
                insert newAppTransaction;*/
                
            }else if(response.status == 'APPLICATION_ERROR'){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,response.msg));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,response.response));
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You must to enter the amount'));
        }
        
    }
}