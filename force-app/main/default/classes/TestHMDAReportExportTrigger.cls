@isTest
public class TestHMDAReportExportTrigger 
{
    private static HMDA_Report__c getHMDARecord ( Integer i )
    {
        String si = String.valueOf ( i ) ;
        
		HMDA_Report__c ha = new HMDA_Report__c () ;
        
        ha.Applicants_Name__c = 'Test ' + si + ' User ' + si ;
        ha.Status__c = 'Certified Correct' ;

		return ha ;        
    }
    
    public static testmethod void testOne ()
    {
		list<HMDA_Report__c> hrL = new list<HMDA_Report__c> () ;
        
        for ( Integer i = 1 ; i < 10 ; i ++ )
        	hrL.add ( getHMDARecord ( i ) ) ;
        
        insert hrL ;
        
        test.startTest () ;
        
        HMDA_Report_Export__c hrE = new HMDA_Report_Export__c () ;
        hrE.Start_Date__c = Date.today ().addDays ( -1 ) ;
        
        insert hrE ;
        
        test.stopTest () ;
    }
    
    public static testmethod void testTwo ()
    {
		list<HMDA_Report__c> hrL = new list<HMDA_Report__c> () ;
        
        for ( Integer i = 1 ; i < 10 ; i ++ )
        	hrL.add ( getHMDARecord ( i ) ) ;
        
        insert hrL ;
        
        test.startTest () ;
        
        HMDA_Report_Export__c hrE = new HMDA_Report_Export__c () ;
        hrE.End_Date__c = Date.today ().addDays ( 1 ) ;
        
        insert hrE ;
        
        test.stopTest () ;
    }
    
    public static testmethod void testThree ()
    {
		list<HMDA_Report__c> hrL = new list<HMDA_Report__c> () ;
        
        for ( Integer i = 1 ; i < 10 ; i ++ )
        	hrL.add ( getHMDARecord ( i ) ) ;
        
        insert hrL ;
        
        test.startTest () ;
        
        HMDA_Report_Export__c hrE = new HMDA_Report_Export__c () ;
        hrE.Status__c = 'Certified Correct' ;
        
        insert hrE ;
        
        test.stopTest () ;
    }
    
    public static testmethod void testFour ()
    {
		list<HMDA_Report__c> hrL = new list<HMDA_Report__c> () ;
        
        for ( Integer i = 1 ; i < 10 ; i ++ )
        	hrL.add ( getHMDARecord ( i ) ) ;
        
        insert hrL ;
        
        test.startTest () ;
        
        HMDA_Report_Export__c hrE = new HMDA_Report_Export__c () ;
        hrE.Start_Date__c = Date.today ().addDays ( -1 ) ;
        hrE.End_Date__c = Date.today ().addDays ( 1 ) ;
        hrE.Status__c = 'Certified Correct' ;
        
        insert hrE ;
        
        test.stopTest () ;
    }
}