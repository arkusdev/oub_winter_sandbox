@isTest
public class TestCheckinCheckoutExtension 
{
	/* --------------------------------------------------------------------------------
	 * User
	 * -------------------------------------------------------------------------------- */
	private static User getUser ()
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test1' ;
		u.lastName = 'User1' ;
		u.email = 'test1@user.com' ;
		u.username = 'test1user1@user.com' ;
		u.alias = 'tuser1' ;
		u.CommunityNickname = 'tuser1' ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		return u ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Timesheet?
	 * -------------------------------------------------------------------------------- */
    private static Timesheet_Entry__c getTimesheet ( User u )
    {
        Timesheet_Entry__c te = new Timesheet_Entry__c () ;
        te.user_id__c = u.ID ;
        te.timesheet_entry_day__c = Date.today () ;
        
        return te ;
    }
    
	/* --------------------------------------------------------------------------------
	 * TEST!
	 * -------------------------------------------------------------------------------- */
	static testmethod void INONE ()
	{
        //  Setup
        User u1 = getUser () ;
        insert u1 ;
        
        Timesheet_Entry__c te = getTimesheet ( u1 ) ;
        insert te ;
        
		//  Point to page to test
		PageReference pr1 = Page.Check_In_and_Out ;
		Test.setCurrentPage ( pr1 ) ;
		
        //  Go!
        Test.startTest () ;
        
		System.runAs ( u1 )
		{
            //  Create standard controller object from the application
            ApexPages.StandardController sc = new ApexPages.StandardController ( te ) ;
            
            //  Create class
            CheckinCheckoutExtension cce = new CheckinCheckoutExtension ( sc ) ;
            
            //  Test?
            String foo = cce.getLocationId () ;
            
            //  IN 1
            cce.initCheckin () ;
        }
        
		//  End of testing
		Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 * TEST!
	 * -------------------------------------------------------------------------------- */
	static testmethod void OUTONE ()
	{
        //  Setup
        User u1 = getUser () ;
        insert u1 ;
        
        Timesheet_Entry__c te = getTimesheet ( u1 ) ;
        insert te ;
        
		//  Point to page to test
		PageReference pr1 = Page.Check_In_and_Out ;
		Test.setCurrentPage ( pr1 ) ;
		
        //  Go!
        Test.startTest () ;
        
		System.runAs ( u1 )
		{
            //  Create standard controller object from the application
            ApexPages.StandardController sc = new ApexPages.StandardController ( te ) ;
            
            //  Create class
            CheckinCheckoutExtension cce = new CheckinCheckoutExtension ( sc ) ;
            
            //  Test?
            String foo = cce.getLocationId () ;
            
            //  IN 1
            cce.initCheckin () ;
        }
        
		System.runAs ( u1 )
		{
            //  Create standard controller object from the application
            ApexPages.StandardController sc = new ApexPages.StandardController ( te ) ;
            
            //  Create class
            CheckinCheckoutExtension cce = new CheckinCheckoutExtension ( sc ) ;
            
            //  Test?
            String foo = cce.getLocationId () ;
            
            //  Out 1
            cce.initCheckout () ;
        }
        
		//  End of testing
		Test.stopTest () ;
    }

	/* --------------------------------------------------------------------------------
	 * TEST! Two
	 * -------------------------------------------------------------------------------- */
	static testmethod void OUTTWO ()
	{
        //  Setup
        User u1 = getUser () ;
        insert u1 ;
        
        //  Double up to test whoops condition
        Timesheet_Entry__c te1 = getTimesheet ( u1 ) ;
        insert te1 ;
        Timesheet_Entry__c te2 = getTimesheet ( u1 ) ;
        insert te2 ;
        
		//  Point to page to test
		PageReference pr1 = Page.Check_In_and_Out ;
		Test.setCurrentPage ( pr1 ) ;
		
        //  Go!
        Test.startTest () ;
        
		System.runAs ( u1 )
		{
            //  Create standard controller object from the application
            ApexPages.StandardController sc = new ApexPages.StandardController ( te1 ) ;
            
            //  Create class
            CheckinCheckoutExtension cce = new CheckinCheckoutExtension ( sc ) ;
            
            //  Test?
            String foo = cce.getLocationId () ;
            
            //  IN 1
            cce.initCheckin () ;
        }
        
		//  End of testing
		Test.stopTest () ;
    }
}