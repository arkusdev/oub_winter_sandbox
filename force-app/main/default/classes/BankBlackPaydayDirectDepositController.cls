public with sharing class BankBlackPaydayDirectDepositController {

    @AuraEnabled
    public static String generateTicket(Map<String, Object> info) {
       // Financial_Account__c fa = getFinancialAccountById(faId);
       // System.debug('generateTicket --- fa: ' + fa );
        try
        {
            Contact c = new Contact();
            
            String contactId = (String) info.get('contactId');
            if ( contactId != null )
            {
                c = getContactById(contactId);
            }
            else
            {
                c = getContactById ( getCurrentUserContactId() );
            }
                
            System.debug('generateTicket --- additionalInfo map passed ---');
            for ( String k : info.keySet() )
            {
                System.debug(k + ': ' + info.get(k));
            }
            
            if ( c != null )
            {
                Ticket__c t = new Ticket__c();
                t.RecordTypeId = Schema.SObjectType.Ticket__c.RecordTypeInfosByDeveloperName.get('Direct_Deposit_Form').RecordTypeId; // Direct Deposit Form
                t.Contact__c = c.Id;
                t.Account_Number__c = (String) info.get('acctNum') ;
                t.Closed_Date__c = Datetime.now();
                t.Electronic_Sig_Consent__c = (Boolean) info.get('signature');
                
                if ( c.TaxID__c != null )
                {
                    t.Social_Security_Number__c = c.TaxID__c;
                }
                
                List<String> names = c.Name.split(' ', 2);
                
                t.First_Name__c = names[0];
                t.Last_Name__c = names[1];
                
                String faId = (String) info.get('faId');
                
                if ( faId != null )
                {
                    t.Financial_Account__c = faId;
                }
                
                // Additional Info entered by user
                t.Company_Name__c = (String) info.get('companyName');
                
                if ( info.get('employeeId') != null )
                {
                    t.Employee_Id__c = (String) info.get('employeeId');
                }
                
                String amountChoice = (String) info.get('amountChoice');
                if ( amountChoice.equalsIgnoreCase( 'Other % of Check' ) )
                {
                    t.Direct_Deposit_Amount__c = (String) info.get('amountVal') + '%';
                }
                else if ( amountChoice.equalsIgnoreCase( 'Set $ Amount' ) )
                {
                    t.Direct_Deposit_Amount__c = '$' + (String) info.get('amountVal');
                }
                else
                {
                    t.Direct_Deposit_Amount__c = '100%';
                }
                
                System.debug('generateTicket --- t.Direct_Deposit_Amount__c --- ' + t.Direct_Deposit_Amount__c);
                
                String acctType = (String) info.get('acctType');
                
                if ( ( String.isNotBlank(acctType) ) && ( acctType.equalsIgnoreCase('checking') ) )
                {
                    t.Account_Type__c = 'Primary Checking';
                }
                else if ( ( String.isNotBlank(acctType) ) && ( acctType.equalsIgnoreCase('savings') ) )
                {
                    t.Account_Type__c = 'Primary Savings';
                }
                
                System.debug('generateTicket --- created t: ' + t);
                insert t;
                
                System.debug('generateTicket --- Final t: ' + t);
                System.debug('generateTicket --- Final t.Id: ' + t.Id);
                return t.Id;
            }
            else
            {
                return '';
            }
        }
        catch ( Exception e )
        {
            System.debug('generateTicket --- error: ' + e);
            return '';
        }
    }
    
    public static String getCurrentUserContactId() {
        try
        {
            User user = [SELECT ContactID FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
            return user.ContactId;
        }
        catch ( Exception e )
        {
            System.debug('directDeposit --- getCurrentUserContactId --- error: ' + e);
            return '';
        }
    }
    
    public static Contact getContactById(String contactId) {
        try
        {
            Contact c = new Contact();
            c = [SELECT Id, TaxID__c, Name
                FROM Contact
                WHERE Id = :contactId];
            
            return c;
        }
        catch ( Exception e )
        {
            System.debug('directDeposit --- getContactById --- error: ' + e);
            System.debug('====== Could not find associated contact w/ given id. ======');
            return null;
        }
    }
    
    @AuraEnabled(cacheable=true)
    public static Financial_Account__c getFinancialAccountById(String faId) {
        try
        {
            Financial_Account__c fa = new Financial_Account__c();
            fa = [SELECT ACCTNBR__c, MJACCTTYPCD__c, TAXRPTFORPERSNBR__c, TAXRPTFORPERSNBR__r.Name, TAXRPTFORPERSNBR__r.TaxID__c
                 FROM Financial_Account__c
                 WHERE Id = :faId];
            return fa;
        }
        catch ( Exception e )
        {
            System.debug('directDeposit --- getFinancialAccountById --- error: ' + e);
            System.debug('====== Could not find associated financial account w/ given id. ======');
            return null;
        }
    }
    
    @AuraEnabled(cacheable=true)
    public static Financial_Account__c getFinancialAccountByAcctNum(String acctNum) {
        try
        {
            Financial_Account__c fa = new Financial_Account__c();
            fa = [SELECT ACCTNBR__c, MJACCTTYPCD__c, TAXRPTFORPERSNBR__c, TAXRPTFORPERSNBR__r.Name, TAXRPTFORPERSNBR__r.TaxID__c
                 FROM Financial_Account__c
                 WHERE ACCTNBR__c = :acctNum];
            // fa.TAXRPTFORPERSNBR__c = getCurrentUserContactId(); // <-- for testing against Mary Test -- her savings financial account record is not associated with her contactId
            return fa;
        }
        catch ( Exception e )
        {
            System.debug('directDeposit --- getFinancialAccountByAcctNum --- error: ' + e);
            System.debug('====== Could not find associated financial account w/ given id. ======');
            return null;
        }
    }
    
    // Returns Direct Deposit Form Ticket for this Financial Acocunt & Contact if it exists,
    // otherwise returns null
    @AuraEnabled
    public static Ticket__c getExistingTicket(String contactId, String faAcctNum) {
        try
        {
            String cid;
            String rti = Schema.SObjectType.Ticket__c.RecordTypeInfosByDeveloperName.get('Direct_Deposit_Form').RecordTypeId;
            
            if ( ( contactId != null ) && ( !contactId.equalsIgnoreCase('null') ) )
            {
                cid = contactId;
            }
            else
            {
                cid = getCurrentUserContactId();
            }
            
            try
            {
                Ticket__c t = [SELECT RecordTypeId, Contact__c, Account_Number__c, First_Name__c, Last_Name__c, Account_Type__c, Social_Security_Number__c,
                               Company_Name__c, Employee_Id__c, Direct_Deposit_Amount__c
                    FROM Ticket__c
                    WHERE Contact__c = :cid
                        AND Account_Number__c = :faAcctNum
                        AND RecordTypeId = :rti
                    ORDER BY Id DESC // return only the most recent ticket
                    LIMIT 1];
                return t;
            }
            catch ( Exception e )
            {
                System.debug('directDeposit --- getExistingTicket --- error: ' + e);
                return null;
            }
        }
        catch ( Exception outerE )
        {
            System.debug('directDeposit --- getExistingTicket --- error: ' + outerE);
            return null;
        }
    }
    
    /*
    // Returns the appropriate URL path to redirect to existing ticket page
    @AuraEnabled
    public static String getTicketURL(String tid) {
        System.debug('getTicketURL ---' + ApexPages.currentPage());
        System.debug('getTicketURL --- ' + ApexPages.currentPage().getHeaders().get('Host')); // returns null
        return 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/advocate/direct_deposit_form?tid=' + tid;
    }
	*/
}