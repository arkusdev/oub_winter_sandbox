global class FileCopyActionFunction 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='Copy Files From one Object to Another' )
	global static List<FileCopyResult> copyfiles ( List<FileCopyRequest> requests ) 
	{
		List<FileCopyResult> fcrL = new List<FileCopyResult> () ;
        
		for ( FileCopyRequest fcr : requests )
		{
            Integer fileCount = 0 ;
            
            fileCount += OneUnitedFileUtilities.copyAttachmentsToFiles ( fcr.fromID, fcr.toID ) ;
            
            fileCount += OneUnitedFileUtilities.copyFilesToFiles ( fcr.fromID, fcr.toID ) ;
            
            FileCopyResult xfcr = new FileCopyResult () ;
            xfcr.numberOfFilesCopied = fileCount ;
            
            fcrL.add ( xfcr ) ;
        }
        
        return fcrL ;
    }

	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class FileCopyRequest 
	{
	    @InvocableVariable ( label='From ID' required=true )
	    public ID fromID ;
        
	    @InvocableVariable ( label='To ID' required=true )
	    public ID toID ;
    }
    
	global class FileCopyResult 
	{
	    @InvocableVariable
	    public Integer numberOfFilesCopied ;
	}
}