public class OneUnitedFileUtilities 
{
	public static Integer copyAttachmentsToFiles ( ID fromID, ID toID )
    {
        list<Attachment> aL = null ;
        Integer fileCount = 0 ;
        
        try
        {
            aL = [
                SELECT ID, Name, body, ContentType, Description, OwnerId, IsPrivate
                FROM Attachment
                WHERE ParentID = :fromID 
                AND IsDeleted = false
            ] ;
        }
        catch ( DMLException e )
        {
            aL = null ;
        }
        
        if ( ( aL != null ) && ( aL.size () > 0 ) ) 
        {
            list<ContentVersion> cvL = new list<ContentVersion> () ;
            
            for ( Attachment a : aL )
            {
                String fileName = a.Name ;
                String extension = '' ;
                
                String[] thingy = a.name.split ( '\\.' ) ;
                
                // Split on DOT, 3 at end ASSuMEs extension
                if ( ( thingy.size () > 1 ) && ( ( thingy [ 1 ].length () == 3 ) || ( thingy [ 1 ].length () == 4 ) ) )
                {
                    fileName = thingy [ 0 ] ;
                    extension = thingy [ 1 ] ;
                }
                //  Otherwise try and figure it out from the content type
                else
                {
                    fileName = a.Name ;
                    
                    if ( String.isNotBlank ( a.ContentType ) )
                    {
                        String[] thingy2 = a.ContentType.split ( '\\/' ) ;
                        extension = thingy2 [ 1 ] ;
                        
                        if ( extension.equalsIgnoreCase ( 'jpeg' ) )
                            extension = 'jpg' ;
                    }
                }
                
                ContentVersion cv = new ContentVersion () ;
                cv.ContentLocation = 'S' ;
//                cv.OwnerId = a.OwnerId ;
                cv.Title = fileName ;
                cv.PathOnClient = fileName + '.' + extension ;
                cv.Description = a.Description ;
                cv.VersionData = a.body ;
                cv.firstPublishLocationId = Userinfo.getUserId () ;
                
                cvL.add ( cv ) ;
            }
            
            if ( ( cvL != null ) && ( cvL.size () > 0 ) )
            {
                try
                {
                    insert cvL ;
                }
                catch ( DMLException e )
                {
                    cvL = null ;
                }
                
                if ( ( cvL != null ) && ( cvL.size () > 0 ) )
                {
                    Set<ID> xIDs = new Set<ID> () ;
                    list<ContentVersion> xcvL = null ;
                    
                    for ( ContentVersion cv : cvL )
                        xIDs.add ( cv.ID ) ;
                    
                    try
                    {
                        xcvL = [
                            SELECT ID, ContentDocumentId
                            FROM ContentVersion
                            WHERE ID IN :xIDs
                        ] ;
                    }
                    catch ( DMLException e )
                    {
                        xcvL = null ;
                    }
                    
                    if ( ( xcvL != null ) && ( xcvL.size () > 0 ) ) 
                    {
                        list<ContentDocumentLink> cdlL = new list<ContentDocumentLink> () ;
                        
                        for ( ContentVersion xcv : xcvL )
                        {
                            ContentDocumentLink cdl = new ContentDocumentLink () ;
                            cdl.ContentDocumentId = xcv.ContentDocumentId ;
                            cdl.ShareType = 'V' ;
                            cdl.LinkedEntityId = toID ;
                            cdl.Visibility = 'AllUsers' ;
                                
                            cdlL.add ( cdl ) ;
                        }
                        
                        try
                        {
                            insert cdlL ;
                            
                            fileCount = cdlL.size () ;
                        }
                        catch ( DMLException e )
                        {
                            // ?
                        }
                    }
                }
            }
        }
        
        return fileCount ;
    }
    
    public static Integer copyFilesToFiles ( ID fromID, ID toID )
    {
        Integer fileCount = 0 ;
        list<ContentDocumentLink> icdlL = null ;
        
        try
        {
            icdlL = [
                SELECT ID, ContentDocumentId, ShareType, Visibility
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :fromID
            ] ;
        }
        catch ( DMLException e )
        {
            icdlL = null ;
        }
        
        if ( ( icdlL != null ) && ( icdlL.size () > 0 ) )
        {
            set<ID> icdlS = new set<ID> () ;
            
            for ( ContentDocumentLink icdl : icdlL )
                icdls.add ( icdl.ContentDocumentId ) ;
            
            list<ContentVersion> icvL = null ;
            
            try
            {
                icvL = [
                    SELECT ID, ContentLocation, OwnerID, Title, PathOnClient, Description, VersionData, ContentDocumentId
					FROM ContentVersion
                    WHERE ContentDocumentId IN :icdlS
                ] ;
            }
            catch ( DMLException e )
            {
                icvL = null ;
            }
            
            if ( ( icvL != null ) && ( icvL.size () > 0 ) )
            {
	            list<ContentVersion> cvL = new list<ContentVersion> () ;
                
                for ( ContentVersion icv : icvL )
                {
                    ContentVersion cv = new ContentVersion () ;
                    cv.ContentLocation = 'S' ;
                    cv.OwnerId = icv.OwnerId ;
                    cv.Title = icv.Title ;
                    cv.PathOnClient = icv.PathOnClient ;
                    cv.Description = icv.Description ;
                    cv.VersionData = icv.VersionData ;
                    
                    cvL.add ( cv ) ;
                }
                
                try
                {
                    insert cvL ;
                }
                catch ( DMLException e )
                {
                    cvL = null ;
                }
                
                if ( ( cvL != null ) && ( cvL.size () > 0 ) )
                {
                    Set<ID> xIDs = new Set<ID> () ;
                    list<ContentVersion> xcvL = null ;
                    
                    for ( ContentVersion cv : cvL )
                        xIDs.add ( cv.ID ) ;
                    
                    try
                    {
                        xcvL = [
                            SELECT ID, ContentDocumentId
                            FROM ContentVersion
                            WHERE ID IN :xIDs
                        ] ;
                    }
                    catch ( DMLException e )
                    {
                        xcvL = null ;
                    }
                    
                    if ( ( xcvL != null ) && ( xcvL.size () > 0 ) ) 
                    {
                        list<ContentDocumentLink> cdlL = new list<ContentDocumentLink> () ;
                        
                        for ( ContentVersion xcv : xcvL )
                        {
                            ContentDocumentLink cdl = new ContentDocumentLink () ;
                            cdl.ContentDocumentId = xcv.ContentDocumentId ;
                            cdl.ShareType = 'V' ;
                            cdl.LinkedEntityId = toID ;
                            cdl.Visibility = 'AllUsers' ;
                                
                            cdlL.add ( cdl ) ;
                        }
                        
                        try
                        {
                            insert cdlL ;
                            
                            fileCount = cdlL.size () ;
                        }
                        catch ( DMLException e )
                        {
                            // ?
                        }
                    }
                }
            }
        }
        
        return fileCount ;
    }
}