@isTest
global class MockHttpResponseGeneratorGeoLocation implements HttpCalloutMock 
{
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        res.setHeader ( 'Content-Type', 'application/json' ) ;
        
        String body = '' ;
        body += '{' ;
        body += ' "results" : [ ' ;
        body += '{' ;
        body += '"address_components" : [' ;
        body += '{ ' ;
        body += ' "long_name" : "Malden", ' ;
        body += ' "short_name" : "Malden", ' ;
        body += ' "types" : [ "neighborhood", "political" ] ' ;
        body += ' }, ' ;
        body += '{ ' ;
        body += ' "long_name" : "Massachusetts", ' ;
        body += ' "short_name" : "MA", ' ;
        body += ' "types" : [ "administrative_area_level_1", "political" ] ' ;
        body += ' }, ' ;
        body += '{' ;
        body += ' "long_name" : "Middlesex County", ' ;
        body += ' "short_name" : "Middlesex County", ' ;
        body += ' "types" : [ "administrative_area_level_2", "political" ] ' ;
        body += '},' ;
        body += '{' ;
        body += ' "long_name" : "02148", ' ;
        body += ' "short_name" : "02148", ' ;
        body += ' "types" : [ "postal_code" ] ' ;
        body += '}' ;
        body += '],' ;
        body += '"geometry" : {' ;
        body += '"location": {' ;
        body += '"lat" : 42.4263976, ' ;
        body += '"lng" : -71.058972' ;
        body += '}' ;
        body += '}' ;
        body += '},' ;
        body += '{' ;
        body += '"address_components" : [' ;
        body += '{ ' ;
        body += ' "long_name" : "Downtown Crossing", ' ;
        body += ' "short_name" : "Downtown Crossing", ' ;
        body += ' "types" : [ "neighborhood", "political" ] ' ;
        body += ' }, ' ;
        body += '{ ' ;
        body += ' "long_name" : "Massachusetts", ' ;
        body += ' "short_name" : "MA", ' ;
        body += ' "types" : [ "administrative_area_level_1", "political" ] ' ;
        body += ' }, ' ;
        body += '{' ;
        body += ' "long_name" : "Suffolk County", ' ;
        body += ' "short_name" : "Suffolk County", ' ;
        body += ' "types" : [ "administrative_area_level_2", "political" ] ' ;
        body += '},' ;
        body += '{' ;
        body += ' "long_name" : "02110", ' ;
        body += ' "short_name" : "02110", ' ;
        body += ' "types" : [ "postal_code" ] ' ;
        body += '}' ;
        body += '],' ;
        body += '"geometry" : {' ;
        body += '"location": {' ;
        body += '"lat" : 35.998777, ' ;
        body += '"lng" : -54.321626' ;
        body += '}' ;
        body += '}' ;
        body += '}' ;
        body += ' ], ' ;
        body += '"status" : "OK" ' ;
        body += '}' ;
        body += '' ;
        
        res.setBody ( body ) ;
        res.setStatusCode ( 200 ) ;
        return res ;
    }
}