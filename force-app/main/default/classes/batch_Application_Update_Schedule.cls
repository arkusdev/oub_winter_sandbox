global class batch_Application_Update_Schedule implements Schedulable
{
	private one_settings__c os ;
    
	global void execute ( SchedulableContext sc ) 
   	{
		os = one_settings__c.getInstance ( 'settings' ) ;
        integer recordCount = 0 ;
        
        if ( os != null )
        {
            if ( ( os.Batch_Job_Max_Count__c != null ) && ( os.Batch_Job_Max_Count__c > 0 ) )
                recordCount = os.Batch_Job_Max_Count__c.intValue () ;
        }
        
		batch_Application_Update b = new batch_Application_Update () ;
        
        System.debug ( 'Record batch limit :: ' + recordCount ) ;
        
        if ( recordCount > 0 )
			Database.executeBatch ( b, recordCount ) ;
        else
			Database.executeBatch ( b ) ;
   }    
}