global class batch_oneOpportunity_Escalate implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	
	private Map<String, String> gMap ;
	private List<String> gIds ;
	private String escalateGroupID ;
    private Integer loanOpportunityEscalateDays ;
    
	private String dRT ;
	private String lRT ;
    
    private Map<String,User> uMap ;
	
	global batch_oneOpportunity_Escalate ()
	{
		System.debug ( '--- Constructor Begin ---' ) ;
		
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
		
		List<String> gNames = new list<String> () ;
		gNames.add ( 'Single Family Loan CA' ) ;
		gNames.add ( 'Single Family Loan MA' ) ;
		gNames.add ( 'Single Family Loan FL' ) ;
		
		Map<String, String> gMap = OneUnitedUtilities.getGroupMap ( gNames ) ;
		
		gIds = new List<String> () ;
		for ( String key : gMap.keyset () )
		{
			gIds.add ( gMap.get ( key ) ) ;
		}
		
		escalateGroupID = OneUnitedUtilities.getGroupId ( 'Single Family Loan Escalation' ) ;
		
		dRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit Opportunity', 'oneOpportunity__c' ) ;
		lRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Opportunity', 'oneOpportunity__c' ) ;
		
        uMap = OneUnitedUtilities.getActiveUsers () ;
        
		System.debug ( '--- Constructor End ---' ) ;
	}
	
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		System.debug ( '--- Start Start ---' ) ;
		
		String query = '' ;
		
		query += 'SELECT ID, OwnerId, Days_Since_Change__c, ' ;
		query += 'Branch__c, Branch__r.Branch_Manager__c, ' ;
		query += 'RecordTypeID, Interested_in_Product__c ' ;
		query += 'FROM oneOpportunity__c ' ;
		query += 'WHERE Scheduled_Follow_Up__c = false ' ;
		query += 'AND Closed_Date__c = NULL ' ;
		query += 'AND Status__c = \'New\' ' ;
		query += 'AND ownerId != :escalateGroupID ' ;
		query += '' ;
		
		System.debug ( '--- Start End ---' ) ;
		
		return Database.getQueryLocator ( query ) ;
	}
		
	global void execute ( Database.Batchablecontext bc, List<oneOpportunity__c> ooIN )
	{
		System.debug ( '--- Execute Start ---' ) ;
		
        loanOpportunityEscalateDays = 2 ;
        
        if ( ( bps != null ) && ( bps.Loan_Opportunity_Escalate_Days__c != null ) )
            loanOpportunityEscalateDays = Integer.valueOf ( bps.Loan_Opportunity_Escalate_Days__c ) ;
            
		List<oneOpportunity__c> ooL = new List<oneOpportunity__c> () ;
		
		for ( oneOpportunity__c oo : ooIN )
		{
			// ---------------------------------------------------------------------------
			// Loan Opportunities
			// ---------------------------------------------------------------------------
			if ( oo.RecordTypeID == lRT )
			{
				System.debug ( 'Found loan opportunity' ) ;
				
				if ( oo.Interested_in_Product__c.equalsIgnoreCase ( 'Single Family Home' ) )
				{
					//  If it's been in one of the regional groups for at least one day
					boolean isGroupRegion = false ;
					
					for ( String id : gIds )
					{
						if ( oo.OwnerId == id )
							isGroupRegion = true ;
					}
					
					if ( ( isGroupRegion ) && ( ( oo.Days_Since_Change__c >= 1 ) || ( Test.isRunningTest() ) ) )
					{
						oo.ownerId = escalateGroupID ;
						oo.Assigned_To__c = null ;
					}
						
					//  If it's been assigned to a person and not touched in two days
					else if ( ( OneUnitedUtilities.isUser ( oo.OwnerId ) ) && ( ( oo.Days_Since_Change__c >= loanOpportunityEscalateDays ) || ( Test.isRunningTest() ) ) )
					{
						oo.ownerId = escalateGroupID ;
						oo.Assigned_To__c = null ;
					}
					
					ooL.add ( oo ) ;
				}
			}
			
			// ---------------------------------------------------------------------------
			// Deposit Opportunities
			// ---------------------------------------------------------------------------
			else if ( oo.RecordTypeId == dRT )
			{
				System.debug ( 'Found deposit opportunity' ) ;
				
				if ( ( OneUnitedUtilities.isUser ( oo.OwnerId ) ) && ( ( oo.Days_Since_Change__c >= 2 ) || ( Test.isRunningTest() ) ) )
				{
                    User u = uMap.get ( oo.OwnerID ) ;
                    UserRole uR = u.UserRole ;
                    
                    if ( ( Test.isRunningTest() ) )
                    {
                        uR = new UserRole () ;
                        uR.Name = 'Test Role' ;
                    }
                    
                    if ( u.Department.equalsIgnoreCase ( 'Retail/Branch Admin' ) )
                    {
                        System.debug ( 'Retail/Branch Admin department, SPECIAL CASE' ) ;
                        
                        if ( uR != null )
                        {
                            if ( ( ( ! uR.Name.contains ( 'VP' ) ) && ( ! uR.Name.contains ( 'Chief' ) ) ) || ( uR.Name.contains ( 'AVP' ) ) ) 
                            {
                                System.debug ( 'NOT Chief/VP/SVP - Escalate' ) ;
                                
                                if ( u.ManagerID != null )
                                {
                                    oo.OwnerId = u.ManagerId ;
                                    ooL.add ( oo ) ;
                                }
                            }
                            else
                            {
                                System.debug ( 'Chief/VP/SVP - no escalate' ) ;
                            }
                        }
                    }
                    else
                    {
                        System.debug ( 'Normal override to branch manager' ) ;
                        Branch__c b = oo.Branch__r ;
                        
                        if ( ( b != null ) && ( oo.OwnerId != b.Branch_Manager__c ) )
                        {
                            oo.OwnerId = b.Branch_Manager__c ;
                            
                            ooL.add ( oo ) ;
                        } 
                    }
				}
			}
		}
		
		update ooL ;
		
		System.debug ( '--- Execute End ---' ) ;
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
	   // Get the ID of the AsyncApexJob representing this batch job
	   // from Database.BatchableContext.
	   // Query the AsyncApexJob object to retrieve the current job's information.
	   AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
	      
	   // Send an email to the Apex job's submitter notifying of job completion.
	   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
	   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
	   
	   mail.setToAddresses ( toAddresses ) ;
	   mail.setSubject ( 'Apex Opportunity Escalate Job :: ' + a.Status ) ;
	   
	   String message = 'Opportunities are currently escalated after ' + loanOpportunityEscalateDays + 'day(s).\n' ;
       message += 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
	   
	   mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}