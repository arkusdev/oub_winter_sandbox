@isTest
public class Test_Case_Classification_Controller 
{
    @testSetup
    private static void setup ()
    {
        list<Case_Topic__c> ctL = new list<Case_Topic__c> () ;
        
        Case_Topic__c ct1 = new Case_Topic__c () ;
        ct1.Name = 'Advocate Link' ;
        ct1.Classification__c = 'Advocate_Link' ;
        ct1.Active__c = true ;
        ct1.Message__c = 'Yo Noid!' ;
        ct1.Status__c = 'Proposed Response' ;
        ct1.Order_By__c = 1 ;
        
        ctL.add ( ct1 ) ;
        
        Case_Topic__c ct2 = new Case_Topic__c () ;
        ct2.Name = 'ATM Deposit' ;
        ct2.Classification__c = 'ATM_Deposit' ;
        ct2.Active__c = true ;
        ct2.Message__c = 'Yo Noid!' ;
        ct2.Status__c = 'Proposed Response' ;
        ct2.Order_By__c = 2 ;
        
        ctL.add ( ct2 ) ;
        
        Case_Topic__c ct3 = new Case_Topic__c () ;
        ct3.Name = 'Other' ;
        ct3.Classification__c = 'Other' ;
        ct3.Active__c = true ;
        ct3.Status__c = 'Needs Agent' ;
        ct3.Order_By__c = 3 ;
        
        ctL.add ( ct3 ) ;
        
        insert ctL ;
        
        Account a = new Account () ;
        a.name = 'Insight Customers - Individuals' ;
        
        insert a ;
    }
    
    private static Case getCase ()
    {
        Case c1 = new Case () ;
        c1.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Email Correspondence', 'Case' ) ;
        c1.Status = 'Waiting on Customer' ;
        c1.Description = 'Test' ;
        c1.Description_HTML__c = '<b>Test</b>' ;
        c1.Subject = 'Test Case' ;
        
        return c1 ;
    }
    
	public static testmethod void ONE ()
    {
        Case c1 = getCase () ;
        insert c1 ;
        
        Contact cx = new Contact () ;
        cx.FirstName = 'Test' ;
        cx.LastName = 'User' ;
        cx.Email = 'test@user.com' ;
        cx.Phone = '123-456-7890' ;
        cx.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
        cx.AccountId = OneUnitedUtilities.getDefaultAccount ().ID ;
        
        insert cx ;
            
		//  Point to page to test
		PageReference pr1 = Page.Case_Classification ;
		Test.setCurrentPage ( pr1 ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object, passed in
		ApexPages.StandardController sc = new ApexPages.StandardController ( c1 ) ;
		
		//  Feed the standard controller to the page controller
		Case_Classification_Controller ccc = new Case_Classification_Controller ( sc ) ;
        
        ccc.firstName = 'Test' ;
        ccc.lastName = 'User' ;
        ccc.phoneNumber = '132-456-7890' ;
        ccc.emailAddress = 'test@user.com' ;
        
        List<SelectOption> ccdd = ccc.caseClassificationDropDown ;
        
        ccc.selectedOption = ccdd.get ( 1 ).getValue () ; // 0 is not picked
        
        ccc.validateClassificationChoice ()  ;
        
        ccc.goNo () ;
        
        ccc.selectedOption = ccdd.get ( 2 ).getValue () ; // 0 is not picked
        
        ccc.validateClassificationChoice ()  ;
        
        ccc.goYes () ;
        
        ccc.selectedOption = ccdd.get ( 3 ).getValue () ; // 0 is not picked
        
        ccc.validateClassificationChoice ()  ;
        
		//  End of testing
		Test.stopTest () ;
    }
    
	public static testmethod void TWO ()
    {
        Case c1 = getCase () ;
        insert c1 ;
        
		//  Point to page to test
		PageReference pr1 = Page.Case_Classification ;
		Test.setCurrentPage ( pr1 ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', c1.id );
        
		//  Create standard controller object, empty
		ApexPages.StandardController sc = new ApexPages.StandardController ( new Case () ) ;
		
		//  Feed the standard controller to the page controller
		Case_Classification_Controller ccc = new Case_Classification_Controller ( sc ) ;
        
        List<SelectOption> ccdd = ccc.caseClassificationDropDown ;
        
		//  End of testing
		Test.stopTest () ;
    }
    
	public static testmethod void THREE ()
    {
        Case_Topic__c ct = [
            SELECT ID FROM Case_Topic__c
            WHERE Classification__c = 'Advocate_Link'
        ] ;
        
        Case c1 = getCase () ;
        c1.Case_Topic__c = ct.ID ;
        insert c1 ;
        
		//  Point to page to test
		PageReference pr1 = Page.Case_Classification ;
		Test.setCurrentPage ( pr1 ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', c1.id );
        
		//  Create standard controller object, empty
		ApexPages.StandardController sc = new ApexPages.StandardController ( new Case () ) ;
		
		//  Feed the standard controller to the page controller
		Case_Classification_Controller ccc = new Case_Classification_Controller ( sc ) ;
        
        //  End of testing
		Test.stopTest () ;
    }
    
}