global class EmailActionFunction2 
{
	private static final String API_KEY = 'o-zuUl0_dDiq6OT87S1Agg' ;
	
	private static final String ONEUNITED_FROM_EMAIL = 'noreplies@oneunited.com' ;
	private static final String ONEUNITED_FROM_NAME = 'OneUnited Bank' ;
	private static final String DELIMITER = '~' ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Merge Variable Parser
	 *  -------------------------------------------------------------------------------------------------- */
	private static void parseData ( List<String> sL, Map<String,String> mVar )
    {
        parseData ( sL, mVar, null ) ; 
    }
    
	private static void parseData ( List<String> sL, Map<String,String> mVar, Map<String,String> mVal )
	{
		if ( ( sL != null ) && ( sL.size () > 0 ) )
		{
			for ( String s : sL )
			{
				List<String> l = s.split ( DELIMITER ) ;
				
				mVar.put ( l [ 0 ], l [ 1 ] ) ;
                
                if ( mVal != null )
					mVal.put ( l [ 0 ], l [ 2 ] ) ;
			}
		}
	}	
	 
	/*  --------------------------------------------------------------------------------------------------
	 *  Contact message(s)
	 *  -------------------------------------------------------------------------------------------------- */
	private static void sendMessages ( List<Contact> cL, String messageType, String subject, String subAccount, List<String> m1, List<String> m2, List<String> m3, List<String> m4, List<String> m5 )
	{
		System.debug ( '========== PRE Work MERGE VARS ==========' ) ;
		
		Map<String,String> mVar1 = new Map<String,String> () ;		
		Map<String,String> mVal1 = new Map<String,String> () ;
		
		parseData ( m1, mVar1, mVal1 ) ;
		
		Map<String,String> mVar2 = new Map<String,String> () ;		
		Map<String,String> mVal2 = new Map<String,String> () ;
		
		parseData ( m2, mVar2, mVal2 ) ;
		
		Map<String,String> mVar3 = new Map<String,String> () ;		
		Map<String,String> mVal3 = new Map<String,String> () ;
		
		parseData ( m3, mVar3, mVal3 ) ;
		
		Map<String,String> mVar4 = new Map<String,String> () ;		
		Map<String,String> mVal4 = new Map<String,String> () ;
		
		parseData ( m4, mVar4, mVal4 ) ;
		
		Map<String,String> mVar5 = new Map<String,String> () ;		
		Map<String,String> mVal5 = new Map<String,String> () ;
		
		parseData ( m5, mVar5, mVal5 ) ;
        
		System.debug ( '========== Creating JSON Object ==========' ) ;
		
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
		
		//  Create the object
		g.writeStartObject () ;
		
		//  API KEY
		g.writeStringField ( 'key', API_KEY ) ;
		
		//  Template
		g.writeStringField ( 'template_name', messageType ) ;
		
		g.writeFieldName ( 'template_content' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'OneUnited Bank Email' ) ;
		g.writeStringField ( 'content', messageType ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;
		
		//  Start of message		
		g.writeFieldName ( 'message' ) ;
		g.writeStartObject () ;

		//  Message contents
		g.writeStringField ( 'from_email', ONEUNITED_FROM_EMAIL ) ;
		g.writeStringField ( 'from_name', ONEUNITED_FROM_NAME ) ;
		
		//  Subject
		if ( String.isNotBlank ( subject ) )
			g.writeStringField ( 'subject', subject ) ;
        
		//  Subaccount
		if ( String.isNotBlank ( subAccount ) )
			g.writeStringField ( 'subaccount', subAccount ) ;
        else
			g.writeStringField ( 'subaccount', 'JOURNEY' ) ;
        
		//  To stuff, always an array!
		g.writeFieldName ( 'to' ) ;
		g.writeStartArray () ;

		//  Thingy
		for ( Contact c : cL )
		{
			g.writeStartObject () ;
			g.writeStringField ( 'email', c.Email ) ;
			g.writeEndObject () ;
		}
		
		g.writeEndArray () ;
		
		// GLOBAL Merge vars
		g.writeFieldName ( 'global_merge_vars' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:COMPANY' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:Description' ) ;
		g.writeStringField ( 'content', '' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;
		
		//  LOCAL Merge vars
		g.writeFieldName ( 'merge_vars' ) ;
		g.writeStartArray () ;
		
		//  Thingy
		for ( Contact c : cL )
		{
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', c.Email ) ;
			g.writeFieldName ( 'vars' ) ;
			g.writeStartArray () ;
			
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'name' ) ;
			g.writeStringField ( 'content', c.Name ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'MERGE6' ) ;
			g.writeStringField ( 'content', c.ID ) ;
			g.writeEndObject () ;
			
			if ( ( mVar1.containsKey ( c.ID ) ) && ( mVal1.containsKey ( c.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar1.get ( c.ID ) ) ;
				g.writeStringField ( 'content', mVal1.get ( c.ID ) ) ;
				g.writeEndObject () ;
			}
						
			if ( ( mVar2.containsKey ( c.ID ) ) && ( mVal2.containsKey ( c.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar2.get ( c.ID ) ) ;
				g.writeStringField ( 'content', mVal2.get ( c.ID ) ) ;
				g.writeEndObject () ;
			}
			
			if ( ( mVar3.containsKey ( c.ID ) ) && ( mVal3.containsKey ( c.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar3.get ( c.ID ) ) ;
				g.writeStringField ( 'content', mVal3.get ( c.ID ) ) ;
				g.writeEndObject () ;
			}

			if ( ( mVar4.containsKey ( c.ID ) ) && ( mVal4.containsKey ( c.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar4.get ( c.ID ) ) ;
				g.writeStringField ( 'content', mVal4.get ( c.ID ) ) ;
				g.writeEndObject () ;
			}

			if ( ( mVar5.containsKey ( c.ID ) ) && ( mVal5.containsKey ( c.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar5.get ( c.ID ) ) ;
				g.writeStringField ( 'content', mVal5.get ( c.ID ) ) ;
				g.writeEndObject () ;
			}

			g.writeEndArray () ;
			g.writeEndObject () ;
		}
		g.writeEndArray () ;
		
		//  Campaign
		g.writeStringField ( 'google_analytics_campaign', messageType ) ;
		
		//  Tags
		g.writeFieldName ( 'tags' ) ;
		g.writeStartArray () ;
        if ( String.isNotBlank ( subAccount ) )
			g.writeString ( subAccount ) ;
        else
			g.writeString ( 'journey' ) ;
		g.writeEndArray () ;

		//  Googly
		g.writeFieldName ( 'google_analytics_domains' ) ;
		g.writeStartArray () ;
		g.writeString ( 'oneunited.com' ) ;
		g.writeString ( 'www.oneunited.com' ) ;
		g.writeEndArray () ;
		
		//  End of Message
		g.writeEndObject () ;
		
		//  End of object
		g.writeEndObject () ;
		
		System.debug ( '========== JSON OUTPUT START ==========' ) ;
		System.debug ( g.getAsString () ) ;
		System.debug ( '========== JSON OUTPUT END ==========' ) ;
		
        sendHTTPRequest ( g.getAsString () ) ;
		
        System.debug ( '========== Done! ==========' ) ;		
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  account message(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void sendMessages ( List<Account> aL, String messageType, String subject, String subAccount, List<String> m1, List<String> m2, List<String> m3, List<String> m4, List<String> m5, List<String> aSurvey )
	{
		System.debug ( '========== PRE Work MERGE VARS ==========' ) ;
		
		Map<String,String> mVar1 = new Map<String,String> () ;		
		Map<String,String> mVal1 = new Map<String,String> () ;
		
		parseData ( m1, mVar1, mVal1 ) ;
		
		Map<String,String> mVar2 = new Map<String,String> () ;		
		Map<String,String> mVal2 = new Map<String,String> () ;
		
		parseData ( m2, mVar2, mVal2 ) ;
		
		Map<String,String> mVar3 = new Map<String,String> () ;		
		Map<String,String> mVal3 = new Map<String,String> () ;
		
		parseData ( m3, mVar3, mVal3 ) ;
		
		Map<String,String> mVar4 = new Map<String,String> () ;		
		Map<String,String> mVal4 = new Map<String,String> () ;
		
		parseData ( m4, mVar4, mVal4 ) ;
		
		Map<String,String> mVar5 = new Map<String,String> () ;		
		Map<String,String> mVal5 = new Map<String,String> () ;
		
		parseData ( m5, mVar5, mVal5 ) ;
		
		Map<String,String> asMap = new Map<String,String> () ;		
		
		parseData ( aSurvey, asMap ) ;
        
		System.debug ( '========== Creating JSON Object ==========' ) ;
		
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
		
		//  Create the object
		g.writeStartObject () ;
		
		//  API KEY
		g.writeStringField ( 'key', API_KEY ) ;
		
		//  Template
		g.writeStringField ( 'template_name', messageType ) ;
		
		g.writeFieldName ( 'template_content' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'OneUnited Bank Journey' ) ;
		g.writeStringField ( 'content', messageType ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;
		
		//  Start of message		
		g.writeFieldName ( 'message' ) ;
		g.writeStartObject () ;

		//  Message contents
		g.writeStringField ( 'from_email', ONEUNITED_FROM_EMAIL ) ;
		g.writeStringField ( 'from_name', ONEUNITED_FROM_NAME ) ;
		
		//  Subject
		if ( String.isNotBlank ( subject ) )
			g.writeStringField ( 'subject', subject ) ;
        
        //  Subaccount
		if ( String.isNotBlank ( subAccount ) )
			g.writeStringField ( 'subaccount', subAccount ) ;
        else
			g.writeStringField ( 'subaccount', 'JOURNEY' ) ;
		
		//  To stuff, always an array!
		g.writeFieldName ( 'to' ) ;
		g.writeStartArray () ;

		//  Thingy
		for ( Account a : aL )
		{
			g.writeStartObject () ;
			g.writeStringField ( 'email', a.Business_Email__c ) ;
			g.writeEndObject () ;
		}
		
		g.writeEndArray () ;
		
		// GLOBAL Merge vars
		g.writeFieldName ( 'global_merge_vars' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:COMPANY' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:Description' ) ;
		g.writeStringField ( 'content', '' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;
		
		//  LOCAL Merge vars
		g.writeFieldName ( 'merge_vars' ) ;
		g.writeStartArray () ;
		
		//  Thingy
		for ( Account a : aL )
		{
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', a.Business_Email__c ) ;
			g.writeFieldName ( 'vars' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'name' ) ;
			g.writeStringField ( 'content', a.Name ) ;
			g.writeEndObject () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'MERGE6' ) ;
			g.writeStringField ( 'content', a.ID ) ;
			g.writeEndObject () ;

			if ( ( mVar1.containsKey ( a.ID ) ) && ( mVal1.containsKey ( a.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar1.get ( a.ID ) ) ;
				g.writeStringField ( 'content', mVal1.get ( a.ID ) ) ;
				g.writeEndObject () ;
			}
			
			if ( ( mVar2.containsKey ( a.ID ) ) && ( mVal2.containsKey ( a.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar2.get ( a.ID ) ) ;
				g.writeStringField ( 'content', mVal2.get ( a.ID ) ) ;
				g.writeEndObject () ;
			}
			
			if ( ( mVar3.containsKey ( a.ID ) ) && ( mVal3.containsKey ( a.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar3.get ( a.ID ) ) ;
				g.writeStringField ( 'content', mVal3.get ( a.ID ) ) ;
				g.writeEndObject () ;
			}

			if ( ( mVar4.containsKey ( a.ID ) ) && ( mVal4.containsKey ( a.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar4.get ( a.ID ) ) ;
				g.writeStringField ( 'content', mVal4.get ( a.ID ) ) ;
				g.writeEndObject () ;
			}

			if ( ( mVar5.containsKey ( a.ID ) ) && ( mVal5.containsKey ( a.ID ) ) )
			{
				g.writeStartObject () ;
				g.writeStringField ( 'name', mVar5.get ( a.ID ) ) ;
				g.writeStringField ( 'content', mVal5.get ( a.ID ) ) ;
				g.writeEndObject () ;
			}
			
			g.writeEndArray () ;
			g.writeEndObject () ;
		}
		g.writeEndArray () ;
		
		//  Campaign
		g.writeStringField ( 'google_analytics_campaign', messageType ) ;
		
		//  Tags
		g.writeFieldName ( 'tags' ) ;
		g.writeStartArray () ;
		g.writeString ( 'journey' ) ;
		g.writeEndArray () ;

		//  Googly
		g.writeFieldName ( 'google_analytics_domains' ) ;
		g.writeStartArray () ;
		g.writeString ( 'oneunited.com' ) ;
		g.writeString ( 'www.oneunited.com' ) ;
		g.writeEndArray () ;
		
		//  End of Message
		g.writeEndObject () ;
		
		//  End of object
		g.writeEndObject () ;
		
		System.debug ( '========== JSON OUTPUT START ==========' ) ;
		System.debug ( g.getAsString () ) ;
		System.debug ( '========== JSON OUTPUT END ==========' ) ;
		
        sendHTTPRequest ( g.getAsString () ) ;
		
        System.debug ( '========== Done! ==========' ) ;		
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Sends the email
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void sendHTTPRequest ( String js )
	{
		System.debug ( '========== Setting up HTTP Request Object ==========' ) ;
		HttpRequest request = new HttpRequest() ;

		request.setEndPoint ( 'https://mandrillapp.com/api/1.0/messages/send-template.json' ) ;
		request.setMethod ( 'POST' ) ;
		request.setHeader ( 'Content-Type', 'application/json' ) ;
		request.setBody ( js ) ;

        System.debug ( '========== Calling method ==========' ) ;
        Http http = new Http () ;
        HTTPResponse res = null ;
        try
        {
			res = http.send ( request ) ;
        }
        catch ( System.CalloutException e )
        {
        	System.debug ( 'HTTP Request fail. :(' ) ;
        }

        System.debug ( '========== RETURN START ==========' ) ;
        if ( res != null )
			System.debug ( res.getBody () ) ;
        System.debug ( '========== RETURN END ==========' ) ;
	}

	/*  --------------------------------------------------------------------------------------------------
	 *  Send email method(s)
	 *  -------------------------------------------------------------------------------------------------- */
	public static void sendContactEmails ( List<String> xL, String campaign )
	{
		sendContactEmails ( xL, campaign, null, 'JOURNEY', null, null, null, null, null, null ) ;
	}
	 
	@future ( callout=true ) // <- Headaches!
	public static void sendContactEmails ( List<String> xL, String campaign, String subject, String subAccount, List<String> m1, List<String> m2, List<String> m3, List<String> m4, List<String> m5, List<String> cSurvey )
	{
		List<Contact> cL_ALL = [
			SELECT ID, Name, Email, Available_for_Survey__c
			FROM Contact 
			WHERE ID IN :xL
		] ;
        
		Map<String,String> csMap = new Map<String,String> () ;		
		parseData ( cSurvey, csMap ) ;
		
        List<Contact> cL = new List<Contact> () ;
        List<Contact> cL2 = new List<Contact> () ;
        
        for ( Contact c : cL_ALL )
        {
            if ( String.isNotBlank ( c.Email ) )
            {
                String surveyYN = csMap.get ( c.ID ) ;
                
                if ( String.isBlank ( surveyYN ) )
                    surveyYN = 'N' ;
                
                Boolean aYN = c.Available_for_Survey__c ;
                if ( aYN == NULL )
                    aYN = true ;
                
                if ( surveyYN.equalsIgnoreCase ( 'N' ) )
                    cL.add ( c ) ;
                else if ( ( surveyYN.equalsIgnoreCase ( 'Y' ) ) && ( aYN == true ) )
                {
                    cL.add ( c ) ;
                    cL2.add ( c ) ;
                }
            }
        }
        
		if ( ( cL != null ) && ( cL.size () > 0 ) )
		{
			sendMessages ( cL, campaign, subject, subAccount, m1, m2, m3, m4, m5 ) ;
		}
        
        // AFTER?!?
		if ( ( cL2 != null ) && ( cL2.size () > 0 ) )
        {
            DateTime dt = DateTime.now () ;
            for ( Contact c : cL2 )
                c.Last_Survey_Sent__c = dt ;

            update cL2 ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Send email method(s)
	 *  -------------------------------------------------------------------------------------------------- */
	public static void sendAccountEmails ( List<String> xL, String campaign )
	{
		sendAccountEmails ( xL, campaign, 'JOURNEY', null,  null, null, null, null, null, null ) ;
	}
	 
	@future ( callout=true ) // <- Headaches!
	public static void sendAccountEmails ( List<String> xL, String campaign, String subject, String subAccount, List<String> m1, List<String> m2, List<String> m3, List<String> m4, List<String> m5, List<String> aSurvey )
	{
		List<Account> aL_ALL = [
			SELECT ID, Name, Business_Email__c, Available_for_Survey__c
			FROM Account 
			WHERE ID IN :xL
		] ;
        
		Map<String,String> asMap = new Map<String,String> () ;		
		parseData ( aSurvey, asMap ) ;
		
        List<Account> aL = new List<Account> () ;
        List<Account> aL2 = new List<Account> () ;
        
        for ( Account a : aL_ALL )
        {
            if ( String.isNotBlank ( a.Business_Email__c ) )
            {
                String surveyYN = asMap.get ( a.ID ) ;
                
                if ( String.isBlank ( surveyYN ) )
                    surveyYN = 'N' ;
                
                Boolean aYN = a.Available_for_Survey__c ;
                if ( aYN == NULL )
                    aYN = true ;
                
                if ( surveyYN.equalsIgnoreCase ( 'N' ) )
                    aL.add ( a ) ;
                else if ( ( surveyYN.equalsIgnoreCase ( 'Y' ) ) && ( aYN == true ) )
                {
                    aL.add ( a ) ;
                    aL2.add ( a ) ;
                }
            }
        }
		
		if ( ( aL != null ) && ( aL.size () > 0 ) )
		{
			sendMessages ( aL, campaign, subject, subAccount, m1, m2, m3, m4, m5, aSurvey ) ;
		}
        
        // AFTER?!?
		if ( ( aL2 != null ) && ( aL2.size () > 0 ) )
        {
            DateTime dt = DateTime.now () ;
            for ( Account a : aL2 )
                a.Last_Survey_Sent__c = dt ;

            update aL2 ;
        }
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='Send Mandrill Email' )
	global static List<SendOnlineBankingResult> sendOnlineBankingEmail ( List<SendOnlineBankingRequest> requests ) 
	{
		//  Not used?
		List<SendOnlineBankingResult> rL = new List<SendOnlineBankingResult> () ;
        
        //  Map of subaccounts based on template
        Map<String,String> saMap = new Map<String,String> () ;
        
        //  Map of subject to template
        Map<String,String> sMap = new Map<String,String> () ;
        
		//  Map of lists based off of campaigns and email addresses, Contact
		Map<String,List<String>> cMap = new Map<String,List<String>> () ;
		List<String> cMerge1 = new List<String> () ;
		List<String> cMerge2 = new List<String> () ;
		List<String> cMerge3 = new List<String> () ;
		List<String> cMerge4 = new List<String> () ;
		List<String> cMerge5 = new List<String> () ;
        List<String> cSurvey = new List<String> () ;
		
		//  Map of lists based off of campaigns and email addresses, Account
		Map<String,List<String>> aMap = new Map<String,List<String>> () ;
		List<String> aMerge1 = new List<String> () ;
		List<String> aMerge2 = new List<String> () ;
		List<String> aMerge3 = new List<String> () ;
		List<String> aMerge4 = new List<String> () ;
		List<String> aMerge5 = new List<String> () ;
        List<String> aSurvey = new List<String> () ;
		
		//  Create the map
		for ( SendOnlineBankingRequest r : requests )
		{
            if ( String.isNotBlank ( r.Subaccount ) )
            {
             	if ( ! saMap.containsKey ( r.Subaccount ) )   
                    saMap.put ( r.TemplateName, r.Subaccount ) ;
            }
            
            if ( String.isNotBlank ( r.Subject ) )
            {
             	if ( ! sMap.containsKey ( r.Subject ) )   
                    sMap.put ( r.TemplateName, r.Subject ) ;
            }
            
			if ( ( String.isBlank ( r.idType ) ) || ( r.idType.equalsIgnoreCase ( 'Contact' ) ) )
			{
				if ( cMap.containsKey ( r.TemplateName ) )
				{
					cMap.get ( r.TemplateName ).add ( r.contactID ) ;
				}
				else
				{
					List<String> nL = new List<String> () ;
					nL.add ( r.ContactID ) ;
					
					cMap.put ( r.TemplateName, nL ) ;
				}
				
				if ( String.isNotBlank ( r.mergeVarName1 ) && String.isNotBlank ( r.mergeVarValue1 ) )
					cMerge1.add ( r.contactID + DELIMITER + r.mergeVarName1 + DELIMITER + r.mergeVarValue1 ) ;
					
				if ( String.isNotBlank ( r.mergeVarName2 ) && String.isNotBlank ( r.mergeVarValue2 ) )
					cMerge2.add ( r.contactID + DELIMITER + r.mergeVarName2 + DELIMITER + r.mergeVarValue2 ) ;
					
				if ( String.isNotBlank ( r.mergeVarName3 ) && String.isNotBlank ( r.mergeVarValue3 ) )
					cMerge3.add ( r.contactID + DELIMITER + r.mergeVarName3 + DELIMITER + r.mergeVarValue3 ) ;

				if ( String.isNotBlank ( r.mergeVarName4 ) && String.isNotBlank ( r.mergeVarValue4 ) )
					cMerge4.add ( r.contactID + DELIMITER + r.mergeVarName4 + DELIMITER + r.mergeVarValue4 ) ;

				if ( String.isNotBlank ( r.mergeVarName5 ) && String.isNotBlank ( r.mergeVarValue5 ) )
					cMerge5.add ( r.contactID + DELIMITER + r.mergeVarName5 + DELIMITER + r.mergeVarValue5 ) ;
                
				if ( String.isNotBlank ( r.surveyYN ) )
					cSurvey.add ( r.contactID + DELIMITER + r.surveyYN ) ;
			}
			
			if ( ( String.isNotBlank ( r.idType ) ) && ( r.idType.equalsIgnoreCase ( 'Account' ) ) )
			{
				if ( aMap.containsKey ( r.TemplateName ) )
				{
					aMap.get ( r.TemplateName ).add ( r.contactID ) ;
				}
				else
				{
					List<String> nL = new List<String> () ;
					nL.add ( r.ContactID ) ;
					
					aMap.put ( r.TemplateName, nL ) ;
				}
				
				if ( String.isNotBlank ( r.mergeVarName1 ) && String.isNotBlank ( r.mergeVarValue1 ) )
					aMerge1.add ( r.contactID + DELIMITER + r.mergeVarName1 + DELIMITER + r.mergeVarValue1 ) ;
					
				if ( String.isNotBlank ( r.mergeVarName2 ) && String.isNotBlank ( r.mergeVarValue2 ) )
					aMerge2.add ( r.contactID + DELIMITER + r.mergeVarName2 + DELIMITER + r.mergeVarValue2 ) ;
					
				if ( String.isNotBlank ( r.mergeVarName3 ) && String.isNotBlank ( r.mergeVarValue3 ) )
					aMerge3.add ( r.contactID + DELIMITER + r.mergeVarName3 + DELIMITER + r.mergeVarValue3 ) ;

				if ( String.isNotBlank ( r.mergeVarName4 ) && String.isNotBlank ( r.mergeVarValue4 ) )
					aMerge4.add ( r.contactID + DELIMITER + r.mergeVarName4 + DELIMITER + r.mergeVarValue4 ) ;

				if ( String.isNotBlank ( r.mergeVarName5 ) && String.isNotBlank ( r.mergeVarValue5 ) )
					aMerge5.add ( r.contactID + DELIMITER + r.mergeVarName5 + DELIMITER + r.mergeVarValue5 ) ;
                
				if ( String.isNotBlank ( r.surveyYN ) )
					aSurvey.add ( r.contactID + DELIMITER + r.surveyYN ) ;
			}
		}
		
		//  Loop map & email
		for ( String t : cMap.keySet () )
		{
            String subAccount = 'JOURNEY' ;
            if ( saMap.containsKey ( t ) ) 
                subAccount = saMap.get ( t ) ;
            
            String subject = null ;
            if ( sMap.containsKey ( t ) )
                subject = sMap.get ( t ) ;
            
			sendContactEmails ( cMap.get ( t ), t, subject, subAccount, cMerge1, cMerge2, cMerge3, cMerge4, cMerge5, cSurvey ) ;
		}
		
		//  Loop map & email
		for ( String t : aMap.keySet () )
		{
            String subAccount = 'JOURNEY' ;
            if ( saMap.containsKey ( t ) ) 
                subAccount = saMap.get ( t ) ;
            
            String subject = null ;
            if ( sMap.containsKey ( t ) )
                subject = sMap.get ( t ) ;
            
			sendAccountEmails ( aMap.get ( t ), t, subject, subAccount, aMerge1, aMerge2, aMerge3, aMerge4, aMerge5, aSurvey ) ;
		}
		
		return rL ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class SendOnlineBankingRequest 
	{
	    @InvocableVariable ( label='Contact ID' required=true )
	    public ID contactID ;
	    
	    @InvocableVariable ( label='Template Name' required=true )
	    public String TemplateName ;
	    
	    @InvocableVariable ( label='Subaccount' required=false )
	    public String Subaccount ;
	    
	    @InvocableVariable ( label='Subject' required=false )
	    public String Subject ;
	    
	    @InvocableVariable ( label='Contact OR Account' required=false )
	    public String idType ;
	    
	    @InvocableVariable ( label='Is A Survey' required=false )
	    public String surveyYN ;
	    
	    @InvocableVariable ( label='Merge Var Name 1' required=false )
	    public String mergeVarName1 ;
	    
	    @InvocableVariable ( label='Merge Var Value 1' required=false )
	    public String mergeVarValue1 ;
	    
	    @InvocableVariable ( label='Merge Var Name 2' required=false )
	    public String mergeVarName2 ;
	    
	    @InvocableVariable ( label='Merge Var Value 2' required=false )
	    public String mergeVarValue2 ;
	    
	    @InvocableVariable ( label='Merge Var Name 3' required=false )
	    public String mergeVarName3 ;
	    
	    @InvocableVariable ( label='Merge Var Value 3' required=false )
	    public String mergeVarValue3 ;

	    @InvocableVariable ( label='Merge Var Name 4' required=false )
	    public String mergeVarName4 ;
	    
	    @InvocableVariable ( label='Merge Var Value 4' required=false )
	    public String mergeVarValue4 ;

	    @InvocableVariable ( label='Merge Var Name 5' required=false )
	    public String mergeVarName5 ;
	    
	    @InvocableVariable ( label='Merge Var Value 5' required=false )
	    public String mergeVarValue5 ;
	}	
	
	global class SendOnlineBankingResult 
	{
	    @InvocableVariable
	    public ID contactID ;
	}
}