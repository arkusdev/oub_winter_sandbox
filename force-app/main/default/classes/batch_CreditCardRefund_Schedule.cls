global class batch_CreditCardRefund_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_CreditCardRefund b = new batch_CreditCardRefund () ;
		Database.executeBatch ( b ) ;
   }
}