public class EverfiProfileHelper 
{
    /*  --------------------------------------------------------------------------------------------------
	 *  Internals
	 *  -------------------------------------------------------------------------------------------------- */	
    private static String prospectRT ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Maps for tracking
	 *  -------------------------------------------------------------------------------------------------- */	
    public static map <String, map<ID,Everfi_Profile__c>> efMap ;
    
    /* -----------------------------------------------------------------------------------
     * Record Type #2
     * ----------------------------------------------------------------------------------- */
    public static String getProspectRT ()
    {
        if ( prospectRT == null )
        {
            prospectRT = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        }
        
        return prospectRT ;
    }
    
    /* -----------------------------------------------------------------------------------
     * Lookup Contacts via name/email and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,Contact> getPossibleContactsByNameEmail ( list<String> iL )
    {
        map<String,Contact> xMap = null ;
		list<Contact> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, Name_Email_Lookup__c
                    FROM Contact
                    WHERE Name_Email_Lookup__c IN :iL
                    ORDER BY CreatedDate
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,Contact> () ;
            
            for ( Contact c : xL )
            {
                if ( ! xMap.containsKey ( c.Name_Email_Lookup__c ) )
	                xMap.put ( c.Name_Email_Lookup__c, c ) ;
            }
        }
        
        return xMap ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  String conversion thingy
	 *  -------------------------------------------------------------------------------------------------- */	
    public static String convertDateTimeToString ( Datetime dt )
    {
        String x = '' ;
        
        if ( dt == null )
            x = null ;
        else
            x = dt.formatGMT ( 'yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'' ) ;
        
        return x ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Other way conversion thingy
	 *  -------------------------------------------------------------------------------------------------- */
	public static DateTime convertISO8601 ( String iso8601_ts )
	{
        DateTime dt = (DateTime) JSON.deserialize('"' + iso8601_ts + '"', DateTime.class);
        
        // Bug in JSONParser or DateTime object results in a malformed DateTime,
        // so convert to Long and back to DateTime.  Without the conversion,
        // methods that access timeGmt() and its components will actually get
        // local time instead of GMT.
        return DateTime.newInstance ( dt.getTime () ) ;
    
        // Once bug is fixed, this return is preferred
        // return dt;
	}
}