public with sharing class one_application_send_trial_email 
{
	private final one_application__c o ;
	
	public boolean emailInStatus
	{
		get ;
		private set ;
	}
	
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public one_application_send_trial_email ( ApexPages.Standardcontroller stdC )
    {
    	ApexPages.Standardcontroller theController = stdC ;
    	
		//  Get ID that is passed in, use it to get email address
        String appId = ((one_application__c) theController.getRecord ()).id ;
        
        if ( String.isNotBlank ( appId ) )
        {
	       	o = [ Select id, ACH_Funding_Status__c FROM one_application__c WHERE id = :appId ] ;
        }
    }
    
    public PageReference sendEmail ()
    {
    	emailInStatus = false ;
    	
		if ( ( o != null ) && ( String.isNotBlank ( o.ACH_Funding_Status__c ) ) )
		{
        	if ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
        	{
	        	List<String> appL = new List<String> () ;
	        	appL.add ( o.Id ) ;
	        		
	        	ApplicationEmailFunctions.sendTrialDepositEmails ( appL ) ;
	        	emailInStatus = true ;
        	}
		}
        
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference ( '/' + o.Id ) ;
        pageRef.setRedirect ( true ) ;
        return pageRef ;
    }

}