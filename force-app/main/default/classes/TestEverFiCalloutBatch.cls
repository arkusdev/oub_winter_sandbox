@isTest
public class TestEverFiCalloutBatch 
{
	/* --------------------------------------------------------------------------------
	 * Setup, Settings
	 * -------------------------------------------------------------------------------- */
    private static void setCustomSettings ()
    {
        EverFi_Settings__c efs = new EverFi_Settings__c () ;

        efs.Grant_Type__c = 'foo' ;
        efs.Client_ID__c = 'abc123' ;
        efs.Client_Secret__c = 'youandme' ;
        efs.Login_URL__c = 'https://www.oneunited.com/token' ;

        efs.User_URL__c = 'https://www.oneunited.com/users' ;
        efs.Location_URL__c = 'https://www.oneunited.com/locations' ;
        efs.Program_User_URL__c = 'https://www.oneunited.com/program_users' ;
        
        efs.Email__c = 'test@user.com' ;

        insert efs ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Defaults
	 * -------------------------------------------------------------------------------- */
    private static void setDefaults ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        insert a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Account
	 * -------------------------------------------------------------------------------- */
    private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.Name = 'Test' + i ;
        return a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Account a, Integer i )
	{
		Contact c = new Contact () ;
        
        c.AccountId = A.ID ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * ??
	 * -------------------------------------------------------------------------------- */
	public static testmethod void ONE ()
    {
        setCustomSettings () ;
        setDefaults () ; // Required because TRIGGER
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'User' ;
        c1.Email = 'test@test.com' ;
        insert c1 ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        MockHTTPEverFiHelper.scrollNUM = 2 ;
        
        Test.startTest () ;
        
        EverFiCalloutBatch b = new EverFiCalloutBatch () ;
        Database.executeBatch ( b ) ;

        Test.stopTest () ;
        
		list<EverFi_Profile__c> epL = [
            SELECT ID, Name
            FROM EverFi_Profile__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( epL.size (), 1 ) ;
        
        list<Module__c> mL = [
            SELECT ID, Name, Everfi_Profile__c, Everfi_Module__c
            FROM Module__c
            WHERE Everfi_Profile__c = :epL[0].ID
            AND Everfi_Module__c != NULL
        ] ;
        
        System.assertNotEquals ( mL.size (),  0 ) ;
	}
}