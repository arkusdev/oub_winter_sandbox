public class oneOpportunityHelper 
{
	private static String depositTypeId = null ;
	private static String loanTypeId = null ;
	private static Map<String, String> gMap = null ;
	private static Map<ID, User> uMap = null ;
	private static Map<String,Branch__c> bNameMap = null ;
	private static Map<String,Branch__c> bIDMap = null ;

	private static void loadData ()
	{
		System.debug ( '----- Loading Data' ) ;
		
	    /* -----------------------------------------------------------------------------------
	     * Record Types
	     * ----------------------------------------------------------------------------------- */
	    List<String> rtNames = new List<String> () ;
	    rtNames.add ( 'Deposit Opportunity' ) ;
	    rtNames.add ( 'Loan Opportunity' ) ;
	     
	    //  Load to list for parsing - DML Limits! 
	    List<RecordType> rtL = OneUnitedUtilities.getRecordTypeList ( rtNames ) ;
	    
	    depositTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Deposit Opportunity', 'oneOpportunity__c' ) ; 
	    loanTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Loan Opportunity', 'oneOpportunity__c' ) ;
	    
	    /* -----------------------------------------------------------------------------------
	     * Groups
	     * ----------------------------------------------------------------------------------- */
	    List<String> gNames = new list<String> () ;
//	    gNames.add ( 'Single Family Loan CA' ) ;
//	    gNames.add ( 'Single Family Loan MA' ) ;
//	    gNames.add ( 'Single Family Loan FL' ) ;
	    gNames.add ( 'Single Family Loan' ) ;
	    gNames.add ( 'MultiFamily Loan' ) ;
	    gNames.add ( 'Single Family Loan Escalation' ) ;
	    
	    gMap = OneUnitedUtilities.getGroupMap ( gNames ) ;
	    
	    /* -----------------------------------------------------------------------------------
	     * Users
	     * ----------------------------------------------------------------------------------- */
	    uMap = new Map<Id, User> ([
	        SELECT id, Branch__c, Loan_Opportunity_Default_Assignment__c, Department, Is_NMLO__c
	        FROM user
            WHERE Profile.Name <> 'OneUnited Advocate Apps User'
	    ]) ;
	    
	    /* -----------------------------------------------------------------------------------
	     * Branches
	     * ----------------------------------------------------------------------------------- */
	    List<Branch__c> bL = [
	        SELECT ID, Name, Branch_Manager__c
	        FROM Branch__c
	    ] ;
	     
	    bNameMap = new Map<String,Branch__c> () ;
	    
	    for ( Branch__c b : bL )
	    {
	        bNameMap.put ( b.Name, b ) ;
	    }
	    
	    bIDMap = new Map<String,Branch__c> () ;
	    
	    for ( Branch__c b : bL )
	    {
	        bIDMap.put ( b.ID, b ) ;
	    }
	}
	
	public static String getDepositTypeId ()
	{
		if ( depositTypeID == null )
			loadData () ;
		
		return depositTypeID ;
	}
	
	public static String getLoanTypeId ()
	{
		if ( loanTypeId == null )
			loadData () ;
		
		return loanTypeId ;
	}
	
	public static Map<String, String> getGroupMap ()
	{
		if ( gMap == null )
			loadData () ;
		
		return gMap ;
	}
	
	public static Map<ID, User> getUserMap ()
	{
		if ( uMap == null )
			loadData () ;
		
		return uMap ;
	}
	
	public static Map<String, Branch__c> getBranchNameMap ()
	{
		if ( bNameMap == null )
			loadData () ;
		
		return bNameMap ;
	}
	
	public static Map<String, Branch__c> getBranchIDMap ()
	{
		if ( bIDMap == null )
			loadData () ;
		
		return bIDMap ;
	}
    
    /* -----------------------------------------------------------------------------------
     * Lookup Contacts via name/email and return map
     * ----------------------------------------------------------------------------------- */
    public static map<String,Loan_Application__c> getPossibleLoanAppByID ( list<String> iL )
    {
        map<String,Loan_Application__c> xMap = null ;
		list<Loan_Application__c> xL = null ;
        
        if ( ( iL != null ) && ( iL.size () > 0 ) )
        {
            try
            {
                xL = [
                    SELECT ID, Application_Number__c, Mortgagebot_Web_ID__c
                    FROM Loan_Application__c
                    WHERE Mortgagebot_Web_ID__c IN :iL
                ] ;
            }
            catch ( Exception e )
            {
                // Nothing
            }
        }
        
        //  Create contact map
        if ( ( xL != null ) && ( xL.size () > 0 ) )
        {
            xMap = new map<String,Loan_Application__c> () ;
            
            for ( Loan_Application__c x : xL )
            {
                if ( ! xMap.containsKey ( x.Mortgagebot_Web_ID__c ) )
	                xMap.put ( x.Mortgagebot_Web_ID__c, x ) ;
            }
        }
        
        return xMap ;
    }
}