@isTest
public class test_loan_load_lead 
{
    /* --------------------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------------------- */
    private static Web_Settings__c insertCustomSetting ( String url )
    {
        Web_Settings__c ws = new Web_Settings__c () ;

        ws.Name = 'Web Forms' ;
        
        if ( String.isNotBlank ( url ) )
            ws.Loan_Landing_Page__c = url ;
        
        insert ws ;
        
        return ws ;
    }

	public static testmethod void testZero ()
    {
        Web_Settings__c ws = insertCustomSetting ( null ) ;
        
        Test.startTest() ;
        
        loan_load_lead all = new loan_load_lead () ;
        all.go () ;
        
        Test.stopTest() ;
    }

    public static testmethod void testloanleadNEW ()
    {
        //  Test settings missing entirely!
//        Web_Settings__c settings = insertCustomSetting ( null ) ;
        
        Test.startTest() ;
        
        ApexPages.currentPage().getParameters().put ( 'fN', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'lN', 'Special' ) ;
        ApexPages.currentPage().getParameters().put ( 'eN', 'Kevin@Special.com' ) ;
        ApexPages.currentPage().getParameters().put ( 'pN', 'Purchase' ) ;
        ApexPages.currentPage().getParameters().put ( 'skip__c', 'JIMSKIP' ) ;

        loan_load_lead all = new loan_load_lead () ;
        all.go () ;
        
        Test.stopTest() ;
    }
    
    public static testmethod void testloanleadExist ()
    {
        //  Test missing setting
        Web_Settings__c settings = insertCustomSetting ( null ) ;
        
        Lead l = new Lead () ;
        l.Company = 'Test Company' ;
        l.FirstName = 'Kevin' ;
        l.LastName = 'Special' ;
        l.Email = 'Kevin@Special.com' ;

        insert l ;
        
        Test.startTest() ;
        
        ApexPages.currentPage().getParameters().put ( 'fN', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'lN', 'Special' ) ;
        ApexPages.currentPage().getParameters().put ( 'eN', 'Kevin@Special.com' ) ;
        ApexPages.currentPage().getParameters().put ( 'pN', 'Purchase' ) ;

        ApexPages.currentPage().getParameters().put ( 'xN', 'JIMPASS' ) ;
        ApexPages.currentPage().getParameters().put ( 'skip__c', 'JIMSKIP' ) ;

        ApexPages.currentPage().getParameters().put ( 'UTM_Content__c', 'a' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Campaign__c', 'b' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Medium__c', 'c' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Source__c', 'd' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Term__c', 'e' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_VisitorID__c', 'f' ) ;

        loan_load_lead all = new loan_load_lead () ;
        all.go () ;
        
        Test.stopTest() ;
    }
    
    public static testmethod void testloanContact ()
    {
        //  setting is set
        Web_Settings__c settings = insertCustomSetting ( 'https://www.oneunited.com' ) ;
        
        Account a = new Account () ;
        a.Name = 'Test Account' ;
        insert a ;
        
        Contact c = new Contact () ;
        c.FirstName = 'Kevin' ;
        c.LastName = 'Special' ;
        c.Email = 'Kevin@Special.com' ;
        c.AccountId = a.ID ;

        insert c ;
        
        Test.startTest() ;
        
        ApexPages.currentPage().getParameters().put ( 'fN', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'lN', 'Special' ) ;
        ApexPages.currentPage().getParameters().put ( 'eN', 'Kevin@Special.com' ) ;
        ApexPages.currentPage().getParameters().put ( 'pN', 'Purchase' ) ;
        ApexPages.currentPage().getParameters().put ( 'skip__c', 'JIMSKIP' ) ;

        loan_load_lead all = new loan_load_lead () ;
        all.go () ;
        
        Test.stopTest() ;
    }
}