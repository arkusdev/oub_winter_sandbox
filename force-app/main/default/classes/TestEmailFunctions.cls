@isTest
global class TestEmailFunctions
{
	//  -------------------------------------
	//  ------------ Send Email -------------
	//  -------------------------------------
	
	static testmethod void test_sendUNITYVisaWelcomeEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
			
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		one_application__c app = new one_application__c();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.FIS_Credit_Limit__c = 250.00 ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app;
		
		EmailFunctions.sendUNITYVisaWelcomeEmails ( app.Id ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaApprovedEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Application_ID__c = '111222333' ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Application_ID__c = '333222111' ;
		app2.FIS_Credit_Limit__c = 250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaApprovedEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaFundingEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Application_ID__c = '333222111' ;
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Application_ID__c = '111222333' ;
		app2.FIS_Credit_Limit__c = 250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaFundingEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaAdditionalInfoEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Application_ID__c = '333222111' ;
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		app1.FIS_Decision_Code__c = 'P090' ;
		app1.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Application_ID__c = '111222333' ;
		app2.FIS_Credit_Limit__c = 250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		app2.FIS_Decision_Code__c = 'P090' ;
		app2.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaAdditionalInfoEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaTrialDepositEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Application_ID__c = '111222333' ;
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Application_ID__c = '333222111' ;
		app2.FIS_Credit_Limit__c = 3250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYTrialDepositEmails ( idL ) ;
		
		EmailFunctions.sendUNITYVisaTrialDepositEmails ( app2.ID ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaTrialDepositFAILEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Application_ID__c = '333222111' ;
		app2.FIS_Credit_Limit__c = 3250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
				
		EmailFunctions.sendUNITYVisaTrialDepositFailEmails ( app2.ID ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaDenialEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Application_ID__c = '111222333' ;
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Application_ID__c = '333222111' ;
		app2.FIS_Credit_Limit__c = 3250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaDenialEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendUNITYVisaWithdrawalEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Application_ID__c = '111222333' ;
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Decision__c = 'V004' ;
		app2.FIS_Credit_Limit__c = 3250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaWithdrawalEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendUNITYVisaIDVDeclineEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Decision__c = 'D006' ;
		app2.FIS_Credit_Limit__c = 3250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaIDVDeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendUNITYVisaOFACDeclineEmails ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		one_application__c app1 = new one_application__c();
		app1.Number_of_Applicants__c = 'I will be applying individually' ;
		
		app1.Contact__c = c1.Id;
		app1.First_Name__c = c1.FirstName ;
		app1.Last_Name__c = c1.LastName ;
		app1.Email_Address__c = c1.Email ;
		app1.Gross_Income_Monthly__c = 10000.00 ;
		
		app1.FIS_Credit_Limit__c = 250.00 ;
		app1.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app1.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app1 ;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Contact c3 = new Contact();
		c3.FirstName = 'c3';
		c3.LastName = 'c3';
		c3.Email = 'hey3@emailhey.com';
		insert c3;
		
		one_application__c app2 = new one_application__c();
		app2.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app2.Contact__c = c2.Id;
		app2.First_Name__c = c2.FirstName ;
		app2.Last_Name__c = c2.LastName ;
		app2.Email_Address__c = c2.Email ;
		app2.Gross_Income_Monthly__c = 10000.00 ;
		
		app2.Co_Applicant_Contact__c = c3.Id;
		app2.Co_Applicant_First_Name__c = c3.FirstName ;
		app2.Co_Applicant_Last_Name__c = c3.LastName ;
		app2.Co_Applicant_Email_Address__c = c3.Email ;
		
		app2.Referred_By_Contact__c = c2.Id;
		app2.Entered_By_Contact__c = c2.Id;
		
		app2.FIS_Decision__c = 'D004' ;
		app2.FIS_Credit_Limit__c = 3250.00 ;
		app2.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app2.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app2 ;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		EmailFunctions.sendUNITYVisaOFACDeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
  	static testmethod void test_OneApplicationAfterUpdateTrigger ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		one_application__c app = new one_application__c();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app.Contact__c = c1.Id;
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;
		
		app.Funding_Options__c = 'Mail' ;
		app.FIS_Application_ID__c = '111222333' ;
		app.FIS_Credit_Limit__c = 250.00 ;
		app.FIS_Decision_Code__c = null ;
		
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
		insert app;

	 	one_application__c o = Database.query ( 'SELECT Id FROM one_application__c WHERE Id = \'' + app.Id + '\''  ) ;
	 	
	 	o.FIS_Decision_Code__c = 'P064' ;
	 	update o ;
	 	
	 	o.FIS_Decision_Code__c = 'A002' ;
	 	update o ;
	 	
	 	o.Last_Application_Status_Change__c = System.now ().addDays ( -3 ) ;
	 	update o ;
	 	
	 	o.FIS_Decision_Code__c = null ;
	 	update o ;
		
		//  End of testing
		Test.stopTest () ;
	}
}