public with sharing class Customer_Support_Controller 
{
    //  ----------------------------------------------------------------------------------------
    //  Security
    //  ----------------------------------------------------------------------------------------
	private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;

    //  ----------------------------------------------------------------------------------------
    //  Bleah
    //  ----------------------------------------------------------------------------------------
    private Case ca ;
    public Contact co 
    {
        get ;
        set ;
    }
    
    private ID caseTopicID ;
    
    private ID accountID ;
    
    public String caseNumber
    {
        get ; 
        private set ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Page objects
    //  ----------------------------------------------------------------------------------------
	public Integer cStepNumber
	{
		get ;
		private set ;
	}
    
    public boolean isValid
    {
        get ;
        private set ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Inputs
    //  ----------------------------------------------------------------------------------------
    public String firstName 
    { 
    	get ; 
        set ;
    }
    
    public String lastName 
    { 
    	get ;
        set ;
    }

    public String phoneNumber 
    { 
    	get ; 
        set ;
    }

    public String emailAddress
    { 
    	get ; 
        set ;
    }

    public String alternatePhone
    { 
    	get ; 
        set ;
    }

    public String address
    { 
    	get ; 
        set ;
    }

    public String city
    { 
    	get ; 
        set ;
    }
    
    public list<SelectOption> stateSelect
    {
        get ;
        private set ;
    }
    
    public String stateOption
    {
        get ; 
        set ; 
    }
    
    public String zipCode
    {
        get ;
        set ;
    }

    //  ----------------------------------------------------------------------------------------
    //  Problem
    //  ----------------------------------------------------------------------------------------
    public String subject
    {
        get ;
        set ;
    }
    
    public String description
    {
        get ;
        set ;
    }

    //  ----------------------------------------------------------------------------------------
    //  Errors
    //  ----------------------------------------------------------------------------------------
	public Map<String,String> errorMap
	{
		get ;
		private set ;
	}
    
    //  ----------------------------------------------------------------------------------------
    //  Drop down and handling
    //  ----------------------------------------------------------------------------------------
    public List<SelectOption> caseClassificationDropDown
    {
        get
        {
            return Case_Classification_Helper.getCaseClassificationList () ;
        }
        
		private set ;
    }
    
    public String selectedOption
    {
        get ;
        set ;
    }
    
    public String message
    {
        get ;
        private set ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Main Controller
    //  ----------------------------------------------------------------------------------------
    public Customer_Support_Controller ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 1000
		cStepNumber = 1000 ;
        isValid = false ;
        
        Account a = OneUnitedUtilities.getDefaultAccount () ;
        
        if ( a != null )
        	accountID = a.ID ;
        
        loadStateDropDown () ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Validate the dropdown
    //  ----------------------------------------------------------------------------------------
	public PageReference validateClassificationChoice () 
	{
        Case_Topic__c ccMDT = Case_Classification_Helper.getClassificationInfo ( selectedOption ) ;
        
        if ( ccMDT != null )
        {
            if ( String.isNotBlank ( ccMDT.Message__c ) )
            {
		        cStepNumber = 1100 ;
        
                message = ccMDT.Message__c ;
                
                caseTopicID = ccMDT.ID ;
            }
            else
            {
		        cStepNumber = 1101 ;
                
                message = 'Thank you for your feedback.' ;
                
                caseTopicID = ccMDT.ID ;
            }
        }
        else
        {
            caseTopicID = null ;
            cStepNumber = 1000 ;
        }
        
        System.debug ( 'STEP :: ' + cStepNumber ) ;
        return null ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  YAY
    //  ----------------------------------------------------------------------------------------
    public PageReference goYES ()
    {
        cStepNumber = 1200 ;
        
        return null ;
    }

    //  ----------------------------------------------------------------------------------------
    //  NEIGH
    //  ----------------------------------------------------------------------------------------
    public PageReference goNO ()
    {
        cStepNumber = 1101 ;
        
        return null ;
    }


    //  ----------------------------------------------------------------------------------------
    //  Submit
    //  ----------------------------------------------------------------------------------------
    public PageReference goSubmit ()
    {
        errorMap = new Map<String, String> () ;
        
        if ( String.isBlank ( firstName ) )
            errorMap.put ( 'firstName' , 'Please enter your first name' ) ;
        else
            errorMap.put ( 'firstName' , '' ) ;
        
        if ( String.isBlank ( lastName ) )
            errorMap.put ( 'lastName' , 'Please enter your last name' ) ;
        else
            errorMap.put ( 'lastName' , '' ) ;
        
        if ( String.isBlank ( emailAddress ) || ( ! OneUnitedUtilities.isValidEmail ( emailAddress ) ) )
            errorMap.put ( 'emailAddress' , 'Please enter a valid email address' ) ;
        else
			errorMap.put ( 'emailAddress' , '' ) ;
        
        if ( String.isBlank ( phoneNumber ) || ( ! OneUnitedUtilities.isValidPhoneNumber ( phoneNumber ) ) )
            errorMap.put ( 'phoneNumber' , 'Please enter your phone number' ) ;
        else
            errorMap.put ( 'phoneNumber' , '' ) ;
        
        if ( String.isBlank ( address ) )
            errorMap.put ( 'address' , 'Please enter your address' ) ;
        else
            errorMap.put ( 'address' , '' ) ;
        
        if ( String.isBlank ( city ) )
            errorMap.put ( 'city' , 'Please enter your city' ) ;
        else
            errorMap.put ( 'city' , '' ) ;
        
        if ( String.isBlank ( stateOption ) )
            errorMap.put ( 'state' , 'Please select your state' ) ;
        else
            errorMap.put ( 'state' , '' ) ;
        
        if ( String.isBlank ( zipCode ) )
            errorMap.put ( 'zipcode' , 'Please enter your zip code' ) ;
        else
            errorMap.put ( 'zipcode' , '' ) ;
        
        if ( String.isBlank ( subject ) && ( selectedOption.equalsIgnoreCase ( 'Other' ) ) )
            errorMap.put ( 'subject' , 'Please enter the topic' ) ;
        else
            errorMap.put ( 'subject' , '' ) ;
        
        if ( String.isBlank ( description ) )
            errorMap.put ( 'description' , 'Please enter a description of the problem' ) ;
        else
            errorMap.put ( 'description' , '' ) ;
        
        //  Validation start
        isValid = true ;
        
        //  Non blank entry means an error occurred
        for ( String key : errorMap.keySet () )
        {
            if ( String.isNotEmpty ( errorMap.get ( key ) ) )
                isValid = false ;
        }

        //  Logic based on validation
        if ( isValid )
        {
            ID xID = Case_Classification_Helper.getContact ( firstName, lastName, emailAddress ) ;
            
            //  New Case!
            ca = new Case () ;
            ca.RecordTypeID = Schema.SObjectType.Case.RecordTypeInfosByName.get('Web Correspondence').RecordTypeId ;
            
            if ( xID != null )
            {
                //  Clear out new contact and assign found one
                co = new Contact () ;
                co.ID = xID ;
                
                ca.ContactId = xID ;
            }
            else
            {
                // create new contact for assigning
                co = new Contact () ;
                co.AccountId = accountID ;
                co.RecordTypeID = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Prospect').RecordTypeId ;
                
                co.FirstName = firstName ;
                co.LastName = lastName ;
                co.Email = emailAddress ;
                co.Phone = phoneNumber ;
                co.MobilePhone = alternatePhone ;
                
                co.MailingStreet = address ;
            }
            
        	try
            {
                if ( ( co != null ) && ( co.ID == null ) )
                    access.insertObject ( co ) ;
                
                ca.ContactId = co.ID ;
                ca.SuppliedName = firstName + lastName ;
	            ca.SuppliedEmail = emailAddress ;
	            ca.SuppliedPhone = phoneNumber ;
                ca.Case_Topic__c = caseTopicID ;
                ca.Status = 'Needs Agent' ;
                ca.Description = description ;
                
                if ( String.isNotBlank ( subject ) )
                    ca.subject = subject ;
                else
                    ca.subject = selectedOption ;
                
                access.insertObject ( ca ) ;
                
                list<Case> xcaL = [ 
                    SELECT ID, CaseNumber
                    FROM CASE
                    WHERE ID = :ca.ID ] ;
                
                if ( ( xcaL != null ) && ( xcaL.size () > 0 ) )
                    caseNumber = xcaL [ 0 ].CaseNumber ;
            }
            catch ( DMLException e )
            {
                caseNumber = '' ;
            }
            
    	    cStepNumber = 1300 ;
        }
        
        return null ;
    }
    
    private void loadStateDropDown ()
    {
        stateSelect = new list<SelectOption> () ;
        stateSelect.add ( new SelectOption ( '', '-- State --' ) ) ;
        
        Schema.DescribeFieldResult fieldResult = one_application__c.Billing_State__c.getDescribe () ;
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues () ;
        
        for( Schema.PicklistEntry pickListVal : ple)
        {
            stateSelect.add ( new SelectOption ( picklistVal.getValue (), picklistVal.getLabel () ) ) ;
        }
    }
}