public class EverFiCalloutQueueable implements Queueable, Database.AllowsCallouts
{
    private static final String JOB_TYPE = 'Salesforce Batch Queuable' ;
    private static final String JOB_NAME = EverFiCalloutQueueable.class.getName () ;
    private Datetime startDT ;
    private String status ;
    private String errorMessage ;
    
    public EverFiCalloutQueueable () 
    {
        // ?
    }
    
    public void execute ( QueueableContext context ) 
    {
        /*  --------------------------------------------------------------------------------------------------
         *  Setup
         *  -------------------------------------------------------------------------------------------------- */
        startDT = Datetime.now () ;
        status = 'WARN' ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  YEARGH
         *  -------------------------------------------------------------------------------------------------- */
        boolean success = false ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  Login
         *  -------------------------------------------------------------------------------------------------- */
        EverFiHelper efh = new EverFiHelper () ;
        success = efh.everFiLogin () ;
        
        /*  --------------------------------------------------------------------------------------------------
         *  Load
         *  -------------------------------------------------------------------------------------------------- */
        if ( success )
        {
            //  CALLOUT
            success = efh.loadProgramUsersSimple () ;
            
            //  DML
            if ( success )
            {
	            success = efh.saveEverFiData () ;
                
                //  Update date in settings
                if ( success )
                {
                    success = efh.updateSince () ;
                }
                else
                {
                    errorMessage = efh.getErrorMessage () ;
		            status = 'ERROR' ;
                }
            }
            else
            {
                errorMessage = efh.getErrorMessage () ;
	            status = 'ERROR' ;
            }
        }
        else
        {
            errorMessage = efh.getErrorMessage () ;
            status = 'ERROR' ;
        }
        
        /*  --------------------------------------------------------------------------------------------------
         *  Email results
         *  -------------------------------------------------------------------------------------------------- */
        EverFiHelper.EverFiReporting efr = efh.getReporting () ;
        if ( ( efr.profileCount > 0 ) && ( efr.moduleCount > 0 ) && ( efr.programUserCount > 0 ) )
            status = 'GOOD' ;
        
        String subject = '[' + status + '][' + JOB_NAME + '][' + JOB_TYPE + '] EverFi Data Load' ;
        
		String message = '' ;
		message += '<h1>Results from running the latest EverFI data load:</h1>' ;
		message += '<p>Data loaded since ' + efr.since + '.</p>' ;
		message += '<p>Date updated to   ' + efr.sinceThing + '.</p>' ;
		message += '<p>There were ' + efr.totalUserCount + ' user(s) found.</p>' ;
		message += '<p>There were ' + efr.profileCount + ' profile(s) loaded.</p>' ;
		message += '<p>There were ' + efr.moduleCount + ' program result(s) loaded.</p>' ;
		message += '<p>There were ' + efr.totalProgramUserCount + ' record(s) found.</p>' ;
		message += '<p>There were ' + efr.programUserCount + ' record(s) linked.</p>' ;
        message += '<br/>' ;
		message += '<p>There were ' + efr.calloutCount + ' callout(s).</p>' ;
		message += '<br/>' ;
        
        if ( status.equalsIgnoreCase ( 'error' ) )
            message += '<hr/>' + efh.getErrorMessage () + '<hr/>' ;
            
		message += '' ;
        
        OneUnitedUtilities.sendBatchHTMLEmail ( efr.email, subject, message ) ;
    }
}