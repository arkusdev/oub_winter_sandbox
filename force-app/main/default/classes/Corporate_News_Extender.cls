public class Corporate_News_Extender {

    public Corporate_News__c news;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public Corporate_News_Extender() {
        list<Corporate_News__c> cnL = null ;
        
        try
        {
         	cnL = [  SELECT Id,
                     Name,
                     Publish_Date__c,
                     CreatedById,
                     OwnerId
                FROM Corporate_News__c 
               WHERE Publish_Date__c<=TODAY
               ORDER BY Publish_Date__c DESC
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( cnL != null ) && ( cnL.size () > 0 ) )
        {
            this.news = cnL [ 0 ] ;
        }
    }
    
    public Id getCorporateNewsId() {
        return this.news.ID;
    }
    
}