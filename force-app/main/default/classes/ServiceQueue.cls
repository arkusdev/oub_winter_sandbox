public class ServiceQueue implements Queueable
{
    private list<Contact> cL = null ;
    private list<Financial_Account__c> faL = null ;
    
    public ServiceQueue ( list<Contact> xL )
    {
        cL = xL ;
    }
    
    public ServiceQueue ( list<Financial_Account__c> xL )
    {
        faL = xL ;
    }
    
    public void execute(QueueableContext context) 
    {
        try
        {
            if ( ( cL != null ) && ( cL.size () > 0 ) )
                update cL ;
            
            if ( ( faL != null ) && ( faL.size () > 0 ) )
                update faL ;
        }
        catch ( Exception e )
        {
            // ?
        }
    }
}