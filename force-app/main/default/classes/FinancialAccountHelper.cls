public class FinancialAccountHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Record types
	 *  -------------------------------------------------------------------------------------------------- */
    private static List<RecordType> rtL ;
    
    private static void loadRecordTypeList ()
    {
        List<String> foo = new List<String> () ;
        foo.add ( 'Deposit' ) ;
        foo.add ( 'Loan' ) ;
        foo.add ( 'Credit Card' ) ;
        foo.add ( 'Deposit Opportunity' ) ;
        foo.add ( 'Loan Opportunity' ) ;
        
        rtL = OneUnitedUtilities.getRecordTypeList ( foo ) ;
    }
    
    public static String getDepositRecordTypeID ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Deposit', 'Financial_Account__c' ) ;
    }
    
    public static String getLoanRecordTypeID ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Loan', 'Financial_Account__c' ) ;
    }
    
    public static String getCreditCardRecordTypeID ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Credit Card', 'Financial_Account__c' ) ;
    }
    
    public static String getDepositOpportunityRecordTypeId ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Deposit Opportunity', 'oneOpportunity__c' ) ;
    }
    
    public static String getLoanOpportunityRecordTypeId ()
    {
        if ( rtL == null )
            loadRecordTypeList () ;
        
        return OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Loan Opportunity', 'oneOpportunity__c' ) ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Batch process
	 *  -------------------------------------------------------------------------------------------------- */
    private static Batch_Process_Settings__c bps ;
    
    private static void loadBatchProcess ()
    {
        if ( bps == null )
        {
            bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
        }
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Opportunities, Deposit
	 *  -------------------------------------------------------------------------------------------------- */
    private static List<oneOpportunity__c> ooDL ;
    
    public static List<oneOpportunity__c> getOpenDepositOpportunities ()
    {
        if ( ooDL == null )
        {
            String depositOpportunityRecordTypeId = getDepositOpportunityRecordTypeId () ;
            
            try
            {
                ooDL = [
                    SELECT ID, Email__c, Opportunity_Contact__c, RecordTypeId, Interested_in_Product__c, MJACCTTYPCD__c
                    FROM oneOpportunity__c
                    WHERE RecordTypeId = :depositOpportunityRecordTypeId
                    AND Closed_Date__c = NULL
                    AND Financial_Account__c = NULL
                ] ;
            }
            catch ( Exception e )
            {
                OneUnitedUtilities.parseStackTrace ( e ) ;
            }
        }
        
        return ooDL ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Opportunities, Loan
	 *  -------------------------------------------------------------------------------------------------- */
    private static List<oneOpportunity__c> ooLL ;
    
    public static List<oneOpportunity__c> getOpenLoanOpportunities ()
    {
        if ( ooLL == null )
        {
            String loanOpportunityRecordTypeId = getLoanOpportunityRecordTypeId () ;
            
            try
            {
                ooLL = [
                    SELECT ID, Email__c, Opportunity_Contact__c, RecordTypeId, Interested_in_Product__c, MJACCTTYPCD__c
                    FROM oneOpportunity__c
                    WHERE RecordTypeId = :loanOpportunityRecordTypeId
                    AND Closed_Date__c = NULL
                    AND Financial_Account__c = NULL
                ] ;
            }
            catch ( Exception e )
            {
                OneUnitedUtilities.parseStackTrace ( e ) ;
            }
        }
        
        return ooLL ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Rewards
	 *  -------------------------------------------------------------------------------------------------- */
    private static ID advocateRewardID ;
    private static ID getAdvocateRewardID ()
    {
        if ( advocateRewardID == null )
        {
            try
            {
                list<Advocate_Reward__c> arL = [
                    SELECT ID
                    FROM Advocate_Reward__c
                    WHERE API_Name__c = 'The_Hook_Up'
                    LIMIT 1
                ] ;
                
                if ( ( arL != null) && ( arL.size () > 0 ) )
                    advocateRewardID = arL [ 0 ].ID ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
        
        return advocateRewardID ;
    }
    
    public static Advocate_Reward_Achieved__c getAdvocateReward ( Financial_Account__c fa )
    {
        if ( bps == null )
            loadBatchProcess () ;
            
        Advocate_Reward_Achieved__c ara = new Advocate_Reward_Achieved__c () ;
        ara.Advocate_Reward__c = getAdvocateRewardID () ;
        ara.Financial_Account__c = fa.ID ;
        ara.Contact_Referred__c = fa.TAXRPTFORPERSNBR__c ;
        ara.Contact__c = fa.Advocate_Referral_Code_Contact__c ;
        
        if ( bps != null )
            ara.Reward_Value__c = bps.Advocate_Reward_Amount__c ;
        
        return ara ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Double map tracking
	 *  -------------------------------------------------------------------------------------------------- */	
    public static map <String, boolean> faMap ;
    public static map <String, boolean> cMap ;
    public static map <String, boolean> awMap ;

	/*  --------------------------------------------------------------------------------------------------
	 *  Generic checker
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean checkDuplicate ( Map<String,boolean> xMap, String id )
	{
		boolean returnValue = false ;
		
		if ( xMap.containsKey ( id ) == true )
			returnValue = true ;
		else
			xMap.put ( id, true ) ;
		
        System.debug ( 'Returning : ' + returnValue ) ;
        
		return returnValue ;
	}
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Emails
	 *  -------------------------------------------------------------------------------------------------- */	
    public static Automated_Email__c getOptInEmail ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Is_A_Survey__c = 'N' ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = 'ODPL opt in confirmed' ;

		return ae ;        
    }

    public static Automated_Email__c getOptOutEmail ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Is_A_Survey__c = 'N' ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = 'ODPL opt out confirmed' ;

		return ae ;        
    }
    
    public static Automated_Email__c getAccountOverdrawn10Days ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Financial_Account__c = fa.ID ;
        ae.First_Name__c = c.FirstName ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Last_Name__c = c.LastName ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = '10 Day OD Program Email' ;

		return ae ;        
    }
    
    public static Automated_Email__c getAccountOverdrawn15Days ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Financial_Account__c = fa.ID ;
        ae.First_Name__c = c.FirstName ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Last_Name__c = c.LastName ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = '15 Day OD Program Email' ;

		return ae ;        
    }
    
    public static Automated_Email__c getAcctOverdrawn30Days ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Financial_Account__c = fa.ID ;
        ae.First_Name__c = c.FirstName ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Last_Name__c = c.LastName ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = '30 Day OD Program Email' ;
        
		return ae ;        
    }
    
    public static Automated_Email__c getAcctOverdrawn30DaysPlus250 ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Financial_Account__c = fa.ID ;
        ae.First_Name__c = c.FirstName ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Last_Name__c = c.LastName ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = '30 Day OD Repayment Plan Program Email' ;
        
		return ae ;        
    }

    public static Automated_Email__c getAcctOverdrawn45Days ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Financial_Account__c = fa.ID ;
        ae.First_Name__c = c.FirstName ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Last_Name__c = c.LastName ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = '45 Day OD Program Email' ;
        
		return ae ;        
    }
    
    public static Automated_Email__c getAcctOverdrawn45DaysPlus250 ( Financial_Account__c fa, Contact c )
    {
		Automated_Email__c ae = new Automated_Email__c () ;
        
        ae.Contact_OR_Account__c = 'Contact' ;
        ae.Contact__c = fa.TAXRPTFORPERSNBR__c ;
        ae.Email_Address__c = c.Email ;
        ae.Financial_Account__c = fa.ID ;
        ae.First_Name__c = c.FirstName ;
        ae.Is_A_Survey__c = 'N' ;
        ae.Last_Name__c = c.LastName ;
        ae.RecordTypeId = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Survey_Email').RecordTypeId ;
        ae.Template_Name__c = '45 Day OD Program Repayment Plan Email' ;
        
		return ae ;        
    }
}