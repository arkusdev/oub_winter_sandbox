@isTest
public with sharing class TestNewAccountEmailFunctions 
{
    /* -----------------------------------------------------------------------------------
	 * Settings
	 * ----------------------------------------------------------------------------------- */
	private static Deposit_Application_Settings__c getSettings ()
	{
		Deposit_Application_Settings__c das = new Deposit_Application_Settings__c () ;
		das.Name = 'Andera' ;
		das.Email_Callout_Functionality__c = true ;
		
		insert das ;
		
		return das ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Branch__c b = getBranch () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'x' + i + 'test'+ i + 'person@testing.com' ;
		c.phone = '00' + i + '-00' + i + '-000' + i ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		
		insert c ;
		
		return c ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a Service
	 * ----------------------------------------------------------------------------------- */
	private static Service__c getService ( Contact c )
	{
		Service__c s = new Service__c () ;
		s.Contact__c = c.ID ;
		s.Issue_Date__c = Date.today ().addDays ( -1 ) ;
		s.Status__c = 'Issued' ;
		s.Type__c = 'Debit Card' ;
		
		insert s ;
		
		return s ;
	}
	
	static testmethod void test_NewAccountEmailFunctions ()
	{
		//  Setup
		Contact c1 = getContact ( 1 ) ;
		Service__c s1 = getService ( c1 ) ;
		
		Contact c2 = getContact ( 2 ) ;
		Service__c s2 = getService ( c2 ) ;
		
		//  Re-select, because... it doesn't work otherwise
		list<Service__c> sL = [
			SELECT ID,
			Contact__r.FirstName, Contact__r.LastName, Contact__r.Email,
			Owner_Contact__r.FirstName, Owner_Contact__r.LastName, Owner_Contact__r.Email,
			Activation_Date__c, Issue_Date__c, Status__c,
			Card_Number_Masked__c,
			Primary_Checking__r.Acctnbr__c,
			Primary_Savings__r.Acctnbr__c
			FROM Service__c
			WHERE ID IN ( :s1.ID, :s2.ID )
		] ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
		
		NewAccountEmailFunctions.sendNewDebitCardEmails ( sL ) ;

		Test.stopTest () ;
	}
}