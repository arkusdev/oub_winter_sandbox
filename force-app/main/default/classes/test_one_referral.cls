@isTest
public with sharing class test_one_referral 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
	
    public static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group () ;
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c ();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Referral_Tracking_Hold_Days__c = 7 ;
        s.Rakuten_Tracking_ID__c = 'TESTID' ;
        s.Rates_URL__c = 'https://www.test.com' ;
        
        insert s;
    }
    
    private static Account getAccount ()
    {
    	return getAccount ( null ) ;
    }
    
    private static Account getAccount ( Integer holdDays )
    {
    	Account a = new Account () ;
    	a.name = 'Test Account' ;
    	
    	if ( holdDays != null )
    		a.Referral_Tracking_Hold_Days__c = holdDays ;
    	
    	insert a ;
    	
    	return a ;
    }
    
    private static one_application__c getApplication ( Account a1, Account a2 )
    {
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
    	one_application__c app = new one_application__c () ;
    	
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Funding_Options__c = 'E- Check (ACH)' ;
		app.Gross_Income_Monthly__c = 10000.00 ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'F003' ;
		
		// Tracking stuff
		app.Application_Landing_Date__c = Datetime.Now () ;
		app.Creative_ID__c = 'xyz' ;
		app.Creative_Type__c = 'abc' ;
		app.Offer_ID__c = '123abc' ;
		
		app.ReservationID__c = 'abc132xyz974ytx' ;
		app.Reservation_Vendor_ID__c = a1.ID ;
		
		app.Reservation_Affiliate_Vendor_ID__c = 'Test Affiliate' ; 
		app.Reservation_Affiliate_Vendor__c = a2.ID ;
		
    	insert app ;
    	
    	return app ;
    }
    
    private static oneOffers__c getOffer ( String thingy, String promoCode )
    {
    	oneOffers__c oo = new oneOffers__c () ;
    	oo.Name = 'Test Offer' ;
    	oo.External_Offer_ID__c = 'abc,xyz,' + thingy + ',pbq' ;
    	oo.Dollar_Amount__c = 25.00 ;
    	oo.Promotion_Code__c = promoCode ;
    	oo.Start_Date__c = Date.Today () - 1 ;
    	oo.End_Date__c = Date.Today () + 1 ;
    	oo.Active__c = true ;
    	oo.Plan__c = '999' ;
    	oo.Product__c = 'XTZ' ;
    	oo.SubProduct__c = '000' ;
        oo.Terms_ID__c = 'ANOTHER' ;
        oo.Bill_Code__c = 'LAST MINUTE' ;
        oo.Offer_Confirm_Text__c = 'BLAH THANKS JIM' ;
    	
    	insert oo ;
    	return oo ;
    }

    static testmethod void testZero ()
    {
		insertCustomSetting () ;
		
		Test.startTest () ;
    	
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		Test.stopTest () ;
    }
    
	static testmethod void testOne ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		//  Load accounts/etc
		String cid = 'XYZ123' ;
		Account acvid = getAccount () ;
		Account aaaid = getAccount () ;
		
		//  Set up the afflilate
		aaaid.Affiliate_ID__c = 'A1A' ;
		aaaid.ParentId = acvid.ID ;
		update aaaid ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', cid ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', acvid.Id ) ;
        ApexPages.currentPage().getParameters().put ( 'aaid', aaaid.Affiliate_ID__c ) ;
        
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		//  Random asserts
		System.assertEquals ( oneR.getReservationId (), 'XYZ123' ) ;
		System.assertEquals ( oneR.getReservationVendorId (), acvid.ID ) ;
		System.assertEquals ( oneR.getAffiliateCode (), 'A1A' ) ;
		System.assertEquals ( oneR.getAffiliateVendorId (), aaaid.id ) ;
		
		Test.stopTest () ;
	}

	static testmethod void testTwo ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		//  Random string indicating the person
		String cid = 'XYZ123' ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', cid ) ;
        
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		Test.stopTest () ;
	}

	static testmethod void testThree ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		//  Load accounts
		String cid = 'XYZ123' ;
		Account acvid = getAccount ( 13 ) ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', cid ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', acvid.Id ) ;
        
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testFour ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		//  Load accounts
		String cid = 'XYZ123' ;
		Account acvid = getAccount ( 13 ) ;
		Account aaaid = getAccount ( 15 ) ;
		
		//  Set up the afflilate
		aaaid.Affiliate_ID__c = 'A1A' ;
		aaaid.ParentId = acvid.ID ;
		update aaaid ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', cid ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', acvid.Id ) ;
        ApexPages.currentPage().getParameters().put ( 'aaid', aaaid.Affiliate_ID__c ) ;
        
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testFive ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		//  Load accounts
		String cid = 'XYZ123' ;
		Account acvid = getAccount ( 13 ) ;
		Account aaaid = getAccount ( 15 ) ;
		
		//  Set up the afflilate
		aaaid.Affiliate_ID__c = 'A1A' ;
		aaaid.ParentId = acvid.ID ;
		update aaaid ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'cid', cid ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', acvid.Id ) ;
        ApexPages.currentPage().getParameters().put ( 'aaid', aaaid.Affiliate_ID__c ) ;
        
        //  Other parameters
        ApexPages.currentPage().getParameters().put ( 'ofid', 'something1' ) ;
        ApexPages.currentPage().getParameters().put ( 'crid', 'something2' ) ;
        ApexPages.currentPage().getParameters().put ( 'ctid', 'something3' ) ;
        
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		//  Validates
		System.assertEquals ( oneR.getOfferID (), 'something1' ) ;
		System.assertEquals ( oneR.getCreativeID (), 'something2' ) ;
		System.assertEquals ( oneR.getCreativeType (), 'something3' ) ;
		
		
		Test.stopTest () ;
	}
	
	static testmethod void testCOOKIE ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		String cid = 'XYZ123' ;
		Account acvid = getAccount () ;
		Account aaaid = getAccount () ;
		
		//  Hold for later
		Datetime thingy = Datetime.newInstanceGMT ( 1999, 9, 22, 3, 1, 2 ) ;
		
		//  Create cookies!
		Cookie dtCookie = new Cookie ( 'dt_Cookie', String.valueOfGmt ( thingy ), null, -1, false ) ;
		Cookie cidCookie = new Cookie ( 'cid_Cookie', cid , null, -1, false ) ;
		Cookie cvidCookie = new Cookie ( 'cvid_Cookie', acvid.id , null, -1, false ) ;
		Cookie aaidCookie = new Cookie ( 'aaid_Cookie', aaaid.id , null, -1, false ) ;
		
		//  Create other cookies!
		Cookie ofidCookie = new Cookie ( 'ofid_Cookie', 'something1' , null, -1, false ) ;
		Cookie cridCookie = new Cookie ( 'crid_Cookie', 'something2' , null, -1, false ) ;
		Cookie ctidCookie = new Cookie ( 'ctid_Cookie', 'something3' , null, -1, false ) ;

		ApexPages.currentPage().setCookies ( new Cookie[] { dtCookie, cidCookie, cvidCookie, aaidCookie, ofidCookie, cridCookie, ctidCookie } ) ;
		
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		//  Random gets
		System.assertEquals ( oneR.getApplicationAge (), thingy ) ;
		
		System.assertEquals ( oneR.getOfferID (), 'something1' ) ;
		System.assertEquals ( oneR.getCreativeID (), 'something2' ) ;
		System.assertEquals ( oneR.getCreativeType (), 'something3' ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testCombinedCOOKIE ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		String cid1 = 'XYZ123' ;
		Account acvid = getAccount () ;
		Account aaaid = getAccount () ;
		
		//  Hold for later
		Datetime thingy = Datetime.newInstanceGMT ( 1999, 9, 22, 3, 1, 2 ) ;
		
		//  Create cookies!
		Cookie dtCookie = new Cookie ( 'dt_Cookie', String.valueOfGmt ( thingy ), null, -1, false ) ;
		Cookie cidCookie = new Cookie ( 'cid_Cookie', cid1 , null, -1, false ) ;
		Cookie cvidCookie = new Cookie ( 'cvid_Cookie', acvid.id , null, -1, false ) ;
		Cookie aaidCookie = new Cookie ( 'aaid_Cookie', aaaid.id , null, -1, false ) ;
		
		//  Create other cookies!
		Cookie ofidCookie = new Cookie ( 'ofid_Cookie', 'something1' , null, -1, false ) ;
		Cookie cridCookie = new Cookie ( 'crid_Cookie', 'something2' , null, -1, false ) ;
		Cookie ctidCookie = new Cookie ( 'ctid_Cookie', 'something3' , null, -1, false ) ;

		ApexPages.currentPage().setCookies ( new Cookie[] { dtCookie, cidCookie, cvidCookie, aaidCookie, ofidCookie, cridCookie, ctidCookie } ) ;
		
		String cid2 = '123XYZ' ;
		
		//  Pass in completely different set of parameters after cookies to test overrides
        ApexPages.currentPage().getParameters().put ( 'cid', cid2 ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', acvid.Id ) ;
        ApexPages.currentPage().getParameters().put ( 'aaid', aaaid.Affiliate_ID__c ) ;
        
        //  Other parameters
        ApexPages.currentPage().getParameters().put ( 'ofid', 'something4' ) ;
        ApexPages.currentPage().getParameters().put ( 'crid', 'something5' ) ;
        ApexPages.currentPage().getParameters().put ( 'ctid', 'something6' ) ;
        
        //  Create object
		one_referral oneR = new one_referral () ;
		
		//  Load cookies
		oneR.loadThirdPartyData () ;
		
		//  Check to see they're new and not from the cookie
		System.assertEquals ( oneR.getReservationId (), cid2 ) ;
		
		System.assertEquals ( oneR.getOfferID (), 'something4' ) ;
		System.assertEquals ( oneR.getCreativeID (), 'something5' ) ;
		System.assertEquals ( oneR.getCreativeType (), 'something6' ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testRakutenTrackingPixel ()
	{
		insertCustomSetting () ;
		
		Account a1 = getAccount ( 1 ) ;
		Account a2 = getAccount ( 2 ) ;
		one_application__c o = getApplication ( a1, a2 ) ; 
		
		Test.startTest () ;
		
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData ( o.ID ) ;
		
		String rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( o.Name ) ;
		System.assertNotEquals ( rakutenTrackingPixel, null ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testPromotions_EMPTY ()
	{
		insertCustomSetting () ;
		
		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		oneOffers__c oo2 = getOffer ( 'bar', 'TESTJIM' ) ;
		
		Test.startTest () ;
		
        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  EMPTY
		one_referral.PromotionData oPD = oneR.loadPromoCode () ;
		
		//  Invalid code?
		System.assertEquals ( oPD.validPromotionCode, false ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testPromotions_DEFAULT ()
	{
		insertCustomSetting () ;
		
		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		oneOffers__c oo2 = getOffer ( 'bar', 'TESTJIM' ) ;
		
		Test.startTest () ;
		
		//  Promo code pass in #1
		ApexPages.currentPage().getParameters().put ( 'prcd', 'JIMTEST' ) ;
		
        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #1 passes
		one_referral.PromotionData oPD = oneR.loadPromoCode () ;
		System.assertEquals ( oPD.promoCode, 'JIMTEST' ) ;
		
		//  Valid code?
		System.assertEquals ( oPD.validPromotionCode, true ) ;
		
		//  Promo code pass in #2
		ApexPages.currentPage().getParameters().put ( 'prcd', 'TESTJIM' ) ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #2 passes
		oPD = oneR.loadPromoCode () ;
		
		//  Valid code?
		System.assertEquals ( oPD.validPromotionCode, true ) ;
		System.assertEquals ( oPD.promoCode, 'TESTJIM' ) ;
		
		Test.stopTest () ;
	}

	static testmethod void testPromotions_THIRDPARTY_YAY ()
	{
		insertCustomSetting () ;
		
		Test.startTest () ;
		
		Account a1 = new Account () ;
		a1.Name = 'campaign' ;
		insert a1 ;
		
		Account a2 = new Account () ;
		a2.Name = 'affiliate' ;
		a2.Affiliate_ID__c = 'aa1' ;
		a2.parentID = a1.ID ;
		insert a2 ;

		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		oneOffers__c oo2 = getOffer ( 'bar', 'TESTJIM' ) ;
		
		//  Parameters
        ApexPages.currentPage().getParameters().put ( 'cid', 'OOGABOOGA' ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', a1.ID ) ;
        ApexPages.currentPage().getParameters().put ( 'aaid', 'aa1' ) ;
        ApexPages.currentPage().getParameters().put ( 'ofid', 'foo' ) ;
		ApexPages.currentPage().getParameters().put ( 'prcd', 'JIMTEST' ) ;

        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #1 passes
		one_referral.PromotionData oPD = oneR.loadPromoCode () ;
		
		//  Valid code?
		System.assertEquals ( oPD.validPromotionCode, true ) ;
		System.assertEquals ( oPD.promoCode, 'JIMTEST' ) ;
		
		//  Promo code pass in #2
		ApexPages.currentPage().getParameters().put ( 'prcd', 'TESTJIM' ) ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #2 fails
		oPD = oneR.loadPromoCode () ;
		
		//  Not valid promo, code saved regardless
		System.assertEquals ( oPD.validPromotionCode, false ) ;
		System.assertEquals ( oPD.promoCode, 'TESTJIM' ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testPromotions_THIRDPARTY_NAY ()
	{
		insertCustomSetting () ;
		
		Account a1 = new Account () ;
		a1.Name = 'campaign' ;
		insert a1 ;
		
		Account a2 = new Account () ;
		a2.Name = 'affiliate' ;
		a2.Affiliate_ID__c = 'aa1' ;
		a2.parentID = a1.ID ;
		insert a2 ;

		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		oneOffers__c oo2 = getOffer ( 'bar', 'TESTJIM' ) ;
		
		Test.startTest () ;
		
		//  Parameters
        ApexPages.currentPage().getParameters().put ( 'cid', 'OOGABOOGA' ) ;
        ApexPages.currentPage().getParameters().put ( 'cvid', a1.ID ) ;
        ApexPages.currentPage().getParameters().put ( 'aaid', 'aa1' ) ;
        ApexPages.currentPage().getParameters().put ( 'ofid', 'nyet' ) ;
		ApexPages.currentPage().getParameters().put ( 'prcd', 'JIMTEST' ) ;

        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #1 fails
		one_referral.PromotionData oPD = oneR.loadPromoCode () ;
		
		//  Valid code?
		System.assertEquals ( oPD.validPromotionCode, false ) ;
		System.assertEquals ( oPD.promoCode, 'JIMTEST' ) ;

		//  Invalid Promo Code		
		ApexPages.currentPage().getParameters().put ( 'prcd', 'TESTJIM' ) ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #2 fails
		oPD = oneR.loadPromoCode () ;
		
		//  Not valid promo, code saved regardless
		System.assertEquals ( oPD.validPromotionCode, false ) ;
		System.assertEquals ( oPD.promoCode, 'TESTJIM' ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testPromotions_COOKIE ()
	{
		insertCustomSetting () ;
		
		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		
		Test.startTest () ;
		
		//  Promo code cookie #1
		Cookie prcdCookie = new Cookie ( 'prcd_Cookie', 'JIMTEST', null, -1, false ) ;
		
		ApexPages.currentPage().setCookies ( new Cookie[] { prcdCookie } ) ;
		
        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #1 passes
		one_referral.PromotionData oPD = oneR.loadPromoCode () ;
		
		//  Valid code?
		System.assertEquals ( oPD.validPromotionCode, true ) ;
		System.assertEquals ( oPD.promoCode, 'JIMTEST' ) ;
				
		Test.stopTest () ;
	}
	
	static testmethod void testPromotions_COOKIE_CHANGE ()
	{
		insertCustomSetting () ;
		
		oneOffers__c oo1 = getOffer ( 'foo', 'JIMTEST' ) ;
		
		Test.startTest () ;
		
		//  Promo code cookie #1
		Cookie cidCookie = new Cookie ( 'cid_Cookie', 'OOGABOOGA', null, -1, false ) ;
		Cookie prcdCookie = new Cookie ( 'prcd_Cookie', 'JIMTEST', null, -1, false ) ;
		
		ApexPages.currentPage().setCookies ( new Cookie[] { cidCookie, prcdCookie } ) ;
		
        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #1 passes
		one_referral.PromotionData oPD1 = oneR.loadPromoCode () ;
		
		//  Valid code?
		System.assertEquals ( oPD1.validPromotionCode, true ) ;
		System.assertEquals ( oPD1.promoCode, 'JIMTEST' ) ;
        System.assertEquals ( oneR.getReservationId (), 'OOGABOOGA' ) ;

        //  Promo code override cookie
		ApexPages.currentPage().getParameters().put ( 'prcd', 'TESTJIM' ) ;
		oneR.loadThirdPartyData () ;
        
        //  reload promodata
        one_referral.PromotionData oPD2 = oneR.loadPromoCode () ;
        
        //  Validate
        System.assertEquals ( opd2.validPromotionCode, false ) ;
        System.assertNotEquals ( oneR.getReservationId (), 'OOGABOOGA' ) ;
		
        Test.stopTest () ;
	}
    
	static testmethod void testAdvocate_DEFAULT ()
	{
		insertCustomSetting () ;
		
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'User' ;
        c1.Email = 'test@user.com' ;
        
        insert c1 ;
        
        Referral_Code__c rf = new Referral_Code__c () ;
        rf.Name = 'JIMTEST' ;
        rf.Contact__c = c1.ID ;
        
        insert rf ;
        
		Test.startTest () ;
		
		//  Promo code pass in #1
		ApexPages.currentPage().getParameters().put ( 'apcd', 'JIMTEST' ) ;
		
        //  Create object
		one_referral oneR = new one_referral () ;
		oneR.loadThirdPartyData () ;
		
		//  Check to see if code #1 passes
		one_referral.AdvocateProgramData aPD = oneR.loadAdvocateProgramCode () ;
		System.assertEquals ( aPD.advocateProgramCode, 'JIMTEST' ) ;
		
		//  Valid code?
		System.assertEquals ( aPD.validAdvocateCode, true ) ;
        System.assertNotEquals ( aPD.referralObj, NULL ) ;
		
		Test.stopTest () ;
	}
}