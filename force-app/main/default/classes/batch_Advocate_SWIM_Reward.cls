global class batch_Advocate_SWIM_Reward implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;

    private static final String JOB_TYPE = 'Salesforce Batch Apex' ;
    private static final String JOB_NAME = batch_Advocate_SWIM_Reward.class.getName () ;
    global String status ;

    //  Groups & RecordType
	private Map<String,String> groupMap ;
	private List<RecordType> rtL ;
    
	//  Counters & Ticket ID
	global Integer accountAwardCount ;
    global Integer possibleAwardCount ;
	global Decimal accountAwardSum ;
	global String ticketId ;
	
    global batch_Advocate_SWIM_Reward ()
    {
        status = 'WARN' ;
        
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
        
        //  Groups
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		//  Set up the record types we need for later
		List<String> rtNameList = new List<String> () ;
		rtNameList.add ( 'CS Batch File' ) ;
		
		rtL = OneUnitedUtilities.getRecordTypeList ( rtNameList ) ; 
        
        // Sums and crap
        accountAwardCount = 0 ;
        accountAwardSum = 0.0 ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- START --------------------' ) ;
        
		String query ;
		query  = '' ;
        query += 'SELECT ID, Name, ' ;
        query += 'Contact__c ' ;
        query += 'FROM Advocate_Reward_Achieved__c ' ;
        query += 'WHERE Status__c = \'New\' ' ;
        query += 'AND Advocate_Reward__r.API_Name__c = \'The_Hook_Up\' ' ;
        query += 'AND Financial_Account__c <> NULL ' ;
        query += 'AND Contact_Referred__c <> NULL ' ;
        query += '' ;
        
		return Database.getQueryLocator ( query ) ;
    }
    
	global void execute ( Database.Batchablecontext bc, list<Advocate_Reward_Achieved__c> araL )
	{
		System.debug ( '-------------------- EXECUTE --------------------' ) ;
        boolean hazReward = false ;
        
        if ( ( araL != null ) && ( araL.size () > 0 ) )
        {
            possibleAwardCount = araL.size () ; // Number of possible awards
            
            COCCSWIMHelper csh = new COCCSWIMHelper () ;
            csh.setCashbox ( bps.Advocate_Reward_Cashbox__c ) ;
            
            System.debug ( '-------------------- Get the list of contacts --------------------' ) ;
            set<ID> cS = new set<ID> () ;
            
            for ( Advocate_Reward_Achieved__c ara : araL )
            {
                if ( ara.Contact__c <> null )
                    cS.add ( ara.Contact__c ) ;
            }
            
            System.debug ( '-------------------- Get the roles for contacts --------------------' ) ;
            list<Financial_Account_Contact_Role__c> facrL = new list<Financial_Account_Contact_Role__c> () ;
            
            if ( ( cs != null ) && ( cs.size () > 0 ) )
            {
                try
                {
                    facrL = [
                        SELECT ID, Contact__c, Contact__r.FirstName, Contact__r.LastName,
                        Financial_Account__c, Financial_Account__r.MJACCTTYPCD__c, Financial_Account__r.ACCTNBR__c
                        FROM Financial_Account_Contact_Role__c
                        WHERE Contact__c IN :cS
                        AND Financial_Account_Role__c IN ( 'Tax Owner', 'NonTax Signator' )
                        AND Financial_Account__r.CURRACCTSTATCD__c IN ( 'ACT', 'IACT', 'DORM' )
                        AND Financial_Account__r.MJACCTTYPCD__c IN ( 'CK', 'SAV' )
                        AND Financial_Account__r.Business_Account__c = FALSE
                        ORDER BY Contact__c, Financial_Account__r.MJACCTTYPCD__c, Financial_Account_Role__c DESC
                    ] ;
                }
                catch ( Exception e )
                {
                    facrL = null ;
                }
            }
            
            System.debug ( '-------------------- Map the roles returned --------------------' ) ;
            map<ID,list<Financial_Account_Contact_Role__c>> facrMap = new map<ID,list<Financial_Account_Contact_Role__c>> () ;
            if ( ( facrL != null ) && ( facrL.size () > 0 ) )
            {
                for ( Financial_Account_Contact_Role__c facr : facrL )
                {
                    if ( ! facrMap.containsKey ( facr.Contact__c ) )
                    {
                        list<Financial_Account_Contact_Role__c> foofL = new list<Financial_Account_Contact_Role__c> () ;
                        foofL.add ( facr ) ;
                        
                        facrMap.put ( facr.Contact__c, foofL ) ;
                    }
                    else
                    {
                        facrMap.get ( facr.Contact__c ).add ( facr ) ;
                    }
                }
            }
            
            System.debug ( '-------------------- Get the financial accounts for contacts --------------------' ) ;
            list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
            
            if ( ( cs != null ) && ( cs.size () > 0 ) )
            {
                try
                {
                    faL = [
                        SELECT ID, TAXRPTFORPERSNBR__c, TAXRPTFORPERSNBR__r.FirstName, TAXRPTFORPERSNBR__r.LastName,
                        MJACCTTYPCD__c, ACCTNBR__c
                        FROM Financial_Account__c
                        WHERE TAXRPTFORPERSNBR__c IN :cS
                        AND CURRACCTSTATCD__c IN ( 'ACT', 'IACT', 'DORM' )
                        AND MJACCTTYPCD__c IN ( 'CK', 'SAV' )
                        AND Business_Account__c = FALSE
                        ORDER BY TAXRPTFORPERSNBR__c, MJACCTTYPCD__c
                    ] ;
                }
                catch ( Exception e )
                {
                    faL = null ;
                }
            }
            
            System.debug ( '-------------------- Map the accounts returned --------------------' ) ;
            map<ID,list<Financial_Account__c>> faMap = new map<ID,list<Financial_Account__c>> () ;
            if ( ( faL != null ) && ( faL.size () > 0 ) )
            {
                for ( Financial_Account__c fa : faL )
                {
                    if ( ! faMap.containsKey ( fa.TAXRPTFORPERSNBR__c ) )
                    {
                        list<Financial_Account__c> foofL = new list<Financial_Account__c> () ;
                        foofL.add ( fa ) ;
                        
                        faMap.put ( fa.TAXRPTFORPERSNBR__c, foofL ) ;
                    }
                    else
                    {
                        faMap.get ( fa.TAXRPTFORPERSNBR__c ).add ( fa ) ;
                    }
                }
            }
            
            System.debug ( '-------------------- Find the accounts to send the money to --------------------' ) ;
            list<Advocate_Reward_Achieved__c> araLP = new list<Advocate_Reward_Achieved__c> () ;
            
            for ( Advocate_Reward_Achieved__c ara : araL )
            {
                if ( facrMap.containsKey ( ara.Contact__c ) )
                {
                    Financial_Account_Contact_Role__c facr = facrMap.get ( ara.Contact__c ) [ 0 ] ;
                    
                    String fixName = facr.Contact__r.LastName.replace ( '\'', '-' ) ;
                    String description = 'Advocate Reward :: ' + facr.Financial_Account__r.ACCTNBR__c + ' -- ' + fixName ;
                    csh.createSWIMRecord ( COCCSWIMHelper.isWithdrawal (), bps.Advocate_Reward_GL_Account__c, bps.Advocate_Reward_Cashbox__c, bps.Advocate_Reward_Amount__c, facr.Financial_Account__r.ACCTNBR__c, description ) ;
                    csh.createSWIMRecord ( COCCSWIMHelper.isDescriptiveDeposit (), facr.Financial_Account__r.ACCTNBR__c, bps.Advocate_Reward_Cashbox__c, bps.Advocate_Reward_Amount__c, facr.Financial_Account__r.ACCTNBR__c, description ) ;

                    ara.Status__c = 'Processed' ;
                    araLP.add ( ara ) ;
                    
                    accountAwardCount ++ ;
                    accountAwardSum += bps.Advocate_Reward_Amount__c ;
                    
                    hazReward = true ;
                }
                else if ( faMap.containsKey ( ara.Contact__c ) )
                {
                    Financial_Account__c fa = faMap.get ( ara.Contact__c ) [ 0 ] ;
                    
                    String fixName = fa.TAXRPTFORPERSNBR__r.LastName.replace ( '\'', '-' ) ;
                    String description = 'Advocate Reward :: ' + fa.ACCTNBR__c + ' -- ' + fixName ;
                    csh.createSWIMRecord ( COCCSWIMHelper.isWithdrawal (), bps.Advocate_Reward_GL_Account__c, bps.Advocate_Reward_Cashbox__c, bps.Advocate_Reward_Amount__c, fa.ACCTNBR__c, description ) ;
                    csh.createSWIMRecord ( COCCSWIMHelper.isDescriptiveDeposit (), fa.ACCTNBR__c, bps.Advocate_Reward_Cashbox__c, bps.Advocate_Reward_Amount__c, fa.ACCTNBR__c, description ) ;
                    
                    ara.Status__c = 'Processed' ;
                    araLP.add ( ara ) ;
                    
                    accountAwardCount ++ ;
                    accountAwardSum += bps.Advocate_Reward_Amount__c ;
                    
                    hazReward = true ;
                }
            }
            
            if ( hazReward == true )
            {
                //  File name
                Datetime dt = System.now () ;
                String fileName = '1218_ODADVOCATE_' + dt.format ( 'yyyyMMddHHmmss' ) + '.SWM' ;
                
                //  Create the overall ticket
                Ticket__c batchTicket = new Ticket__c () ;
                
                batchTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
                batchTicket.Status__c = 'New' ;
                batchTicket.Batch_Account_Total__c = accountAwardCount ;
                batchTicket.Batch_Amount_Total__c = accountAwardSum ;
                batchTicket.ownerId = groupMap.get ( 'Credit Card Batch Processing' ) ;
                batchTicket.Is_Batch_Refund__c = false ;
                batchTicket.Batch_Type__c = 'SWM DEBIT' ;
                batchTicket.Batch_File_Name__c = fileName ;
                batchTicket.Description__c = 'BATCH Deposit SWIM Settlement' ;
                
                insert batchTicket ;
                
                //  Attachment ?!
                Attachment a = new Attachment () ;
                a.ParentId = batchTicket.ID ;
                a.Name = fileName ;
                a.ContentType = 'Text/txt' ;
                a.Body = Blob.valueOf ( csh.getSWIMFile ().escapeHTML4 () ) ;
                
                insert a ;
                
                if ( ( araLP != null ) && ( araLP.size () > 0 ) )
                {
                    for ( Advocate_Reward_Achieved__c ara : araLP )
                        ara.Advocate_Reward_SWIM_Ticket__c = batchTicket.ID ;
                        
                    try
                    {
                        update araLP ;
                    }
                    catch ( DMLException e )
                    {
                        status = 'ERROR' ;
                    }
                }
                
                ticketID = batchTicket.ID ;
                
                status = 'GOOD' ;
            }
        }
        else
        {
            status = 'GOOD' ;
        }
    }
    
	global void finish ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- FINISH --------------------' ) ;
		
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
      	OrgWideEmailAddress o = [
      		SELECT ID
      		FROM OrgWideEmailAddress
      		WHERE Address = 'customersupport@oneunited.com'
            ORDER BY ID
            LIMIT 1
      	] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c,
				'helpdesk@OneUnited.com' 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email,
				'helpdesk@OneUnited.com' 
			} ;
		}
   
        if ( ( a.Status.equalsIgnoreCase ( 'Failed' ) ) || ( a.NumberOfErrors > 0 ) )
            status = 'ERROR' ;
        else if ( a.Status.equalsIgnoreCase ( 'Aborted' ) )
            status = 'WARN' ;
        else if ( ( a.TotalJobItems == 0 ) && ( a.NumberOfErrors == 0 ) )
            status = 'GOOD' ;
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( '[' + status + '][' + JOB_NAME + '][' + JOB_TYPE + '] Advocate Batch SWIM Reward' ) ;
   
		if ( o != null )
			mail.setOrgWideEmailAddressId ( o.Id ) ;
		
		String message = '' ;
		message += '<h1>Results from creating the newest Advocate Reward SWIM batch file:</h1>' ;
		message += '<p>The batch processed ' + a.TotalJobItems +' batches ' ;
		if ( a.NumberOfErrors > 0 )
			message += 'with '+ a.NumberOfErrors + ' failures.' ;
		message += '</p>' ;
        message += '<p>There were ' + possibleAwardCount + ' possible reward records found.</p>' ;
		message += '<p>There are ' + accountAwardCount + ' reward(s) totalling ' + formatMoney ( accountAwardSum ) + ' in the batch.</p>' ;
		message += '<br/>' ;
		
		if ( ( accountAwardCount > 0 ) && ( accountAwardSum > 0.0 ) && ( String.isNotBlank ( ticketId ) ) )
		{
			System.URL sURL = URL.getSalesforceBaseURL () ;
			String urly = sURL.toExternalForm () + '/' + ticketId ;
			message += '<p>You can set the batch at:</p>' ;
			message += '<a href="'+ urly + '">' + urly + '</a>' ;
		}
   
   		mail.setPlainTextBody ( '' ) ;  // WTB no "null"
		mail.setHtmlBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
	
	private String formatMoney ( Decimal x )
	{
		Decimal dollars;
		Decimal cents;
		dollars = x.intValue();
		cents = x - x.intValue();
		cents = cents.setScale(2);  // it is possible to store repeating decimals in sfdc currency fields…I found out the hard way.
		String amtText = '$' + dollars.format() + cents.toPlainString().substring(1) ;
		return amtText ;		
	}
}