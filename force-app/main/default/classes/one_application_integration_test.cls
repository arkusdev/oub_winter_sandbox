public with sharing class one_application_integration_test 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Page inputs
     *  -------------------------------------------------------------------------------------------------- */
    public String userName { public get ; public set ; }
    public String password { public get ; public set ; }
    public String endpoint { public get ; public set ; }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page outputs
     *  -------------------------------------------------------------------------------------------------- */
	public boolean authPanel { public get ; private set ; }
    public String authResponse { public get ; private set ; }
    public String authError { public get ; private set ; }
     
    /*  --------------------------------------------------------------------------------------------------
     *  Private variables
     *  -------------------------------------------------------------------------------------------------- */
	private v002ReplyobjectsSoapFisdsCom.authentication idAuth ;
	
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public one_application_integration_test ( ApexPages.Standardcontroller stdC )
    {
        idAuth = new v002ReplyobjectsSoapFisdsCom.authentication () ;
        
        //  Panel off
        authPanel = false ; 
    	
    	//  Defaults - SANDBOX
    	userName = '42284828' ;
    	password = 't26{jcdAiyXax%' ;
    	endpoint = 'https://penleyincqa.penleyinc.com/fissoap1/services/authentication' ;
    }

    /*  ----------------------------------------------------------------------------------------
     *  Page actions
     *  ---------------------------------------------------------------------------------------- */
	public PageReference testFISLogin () 
	{
		PageReference pr = null ;
	
        //  Panel on
        authPanel = true ; 
    
    	//  reset values
    	authResponse = null ;
    	authError = null ;
        
        idAuth.endpoint_x = endpoint ;
        
        try
        {
        	authResponse = idAuth.fisauthenticate ( userName, password ) ;
        }
        catch ( System.CalloutException e )
		{
			authError = e.getStackTraceString () ;
		}
		catch ( NullPointerException e )
		{
			authError = e.getStackTraceString () ;
		}
		
		return pr ;
	}
}