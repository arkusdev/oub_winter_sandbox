@isTest
public with sharing class Test_batch_oneOpportunity_Escalate 
{
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
        bps.Loan_Opportunity_Escalate_Days__c = 2 ;
		
		insert bps ;
	}
	
	private static User getUser ( String thingy )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
        u.Department = 'test department' ;
		
		insert u ;
		
		return u ;
	}
	
	private static Branch__c getBranch ()
	{
		return getBranch ( null ) ;
	}
	
	private static Branch__c getBranch ( User u )
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		
		if ( u != null )
			b.Branch_Manager__c = u.Id ;
		
		insert b ;
		
		return b ;
	}
	
	private static Contact getContact ()
	{
		Contact c = new Contact () ;
		c.FirstName = 'TestOO' ;
		c.LastName = 'PersonOO' ;
		c.Email = 'testOO.personOO@testingOO.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		
		insert c ;
		
		return c ;
	}
	
	private static oneOpportunity__c getOpportunity ( Contact c, String rType, String product )
	{
		return getOpportunity ( c, rType, product, null, null ) ;
	}
	
	private static oneOpportunity__c getOpportunity ( Contact c, String rType, String product, Branch__c b, User u )
	{
		oneOpportunity__c oo = new oneOpportunity__c () ;
		
		oo.Opportunity_Contact__c = c.id ;
		oo.RecordTypeId = rType ;
		oo.First_Name__c = c.FirstName + ' ' + c.LastName ;
		oo.Status__c = 'New' ;
		oo.Interested_in_Product__c = product ;
		oo.Type__c = 'Purchase' ;
		oo.Home_Phone__c = '123-456-7890' ;
		oo.Priority__c = 'Normal' ;
		oo.Opportunity_Source__c = 'Branch' ;
		
		if ( b != null )
			oo.Branch__c = b.ID ;
        
        if ( u != null )
	        oo.OwnerId = u.ID ;
		
		insert oo ;
		
		return oo ;
	}
	
	static testmethod void escalateLoanTestOne ()
	{
		setBatchSettings () ;
		
		//  --------------------------------------------------
		//  Opportunity Setup
		//  --------------------------------------------------
		Contact c = getContact () ;
				
		String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;

		oneOpportunity__c oo1 = getOpportunity ( c, rtId, 'Single Family Home' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		batch_oneOpportunity_Escalate b = new batch_oneOpportunity_Escalate () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
	}
	
	static testmethod void escalateLoanTestTwo ()
	{
		setBatchSettings () ;
		
		//  --------------------------------------------------
		//  Background setup
		//  --------------------------------------------------
		User u1 = getUser ( '1' ) ;
        
		//  --------------------------------------------------
		//  Opportunity Setup
		//  --------------------------------------------------
		Contact c = getContact () ;
				
		String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;

		oneOpportunity__c oo1 = getOpportunity ( c, rtId, 'Single Family Home', null, u1 ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		batch_oneOpportunity_Escalate b = new batch_oneOpportunity_Escalate () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
	}
    
	static testmethod void escalateDepositTest ()
	{
		setBatchSettings () ;
		
		//  --------------------------------------------------
		//  Background setup
		//  --------------------------------------------------
		User u1 = getUser ( '1' ) ;
		User u2 = getUser ( '2' ) ;
		Branch__c br = getBranch ( u2 ) ;
		
		//  --------------------------------------------------
		//  Opportunity Setup
		//  --------------------------------------------------
		Contact c = getContact () ;
				
		String rtId = OneUnitedUtilities.getRecordTypeId ( 'Deposit Opportunity' ) ;

		oneOpportunity__c oo1 = getOpportunity ( c, rtId, 'Checking', br, u1 ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
		
		batch_oneOpportunity_Escalate b = new batch_oneOpportunity_Escalate () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
	}
    
	static testmethod void escalateKarenSpecial ()
	{
		setBatchSettings () ;
		
		//  --------------------------------------------------
		//  Background setup
		//  --------------------------------------------------
        User u1 = getUser ( '1' ) ;
        u1.Department = 'Retail/Branch Admin' ;
        update u1 ;
        
		User u2 = getUser ( '2' ) ;
        u2.Department = 'Retail/Branch Admin' ;
        u2.ManagerID = u1.ID ;
        update u2 ;
        
        System.debug ( 'U1: ' + u1.ID ) ;
        System.debug ( 'U2: ' + u2.ID ) ;
         
		Branch__c br = getBranch () ;
        
		//  --------------------------------------------------
		//  Opportunity Setup
		//  --------------------------------------------------
		Contact c = getContact () ;

        String rtId = OneUnitedUtilities.getRecordTypeId ( 'Deposit Opportunity' ) ;

		oneOpportunity__c oo1 = getOpportunity ( c, rtId, 'Checking', br, u2 ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest () ;
        
		batch_oneOpportunity_Escalate b = new batch_oneOpportunity_Escalate () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
        
        oneOpportunity__c oo2 = [
            SELECT ID, OwnerID
            FROM oneOpportunity__c
            WHERE ID = :oo1.ID
        ] ;

       	System.assertEquals ( oo2.OwnerId, u1.ID ) ;
    }
}