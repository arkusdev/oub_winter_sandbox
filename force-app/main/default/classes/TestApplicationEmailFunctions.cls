@isTest
public class TestApplicationEmailFunctions 
{
	//  -------------------------------------
	//  ------------- Settings --------------
	//  -------------------------------------
	private static one_settings__c getSettings ( String classThing )
    {
        one_settings__c settings = new one_settings__c () ;
        settings.Name = 'settings' ;
        settings.Email_Integration_Class__c = classThing ;
        settings.Google_API_Key__c = 'abc123' ;
        
        return settings ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Creates a generic application I use for testing.
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, String rtID )
	{
		one_application__c app = new one_application__c ();
        app.RecordTypeId = rtID ;
        app.Upload_Attachment_Key__c = 'xyz123abc546' ;
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = '666-01-1111' ;
        app.Phone__c = c1.Phone ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
        app.RequestedCreditLimit__c = 250.00 ;
		
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = '666-02-1111' ;
            app.Co_Applicant_Phone__c = c2.Phone ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Funding_Options__c = 'E- Check (ACH)' ;
		app.Gross_Income_Monthly__c = 10000.00 ;

		app.FIS_Application_ID__c = null ;
		app.FIS_Decision_Code__c = null ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        app.IDV_Applicant_Deceased__c = false ;
        app.IDV_Applicant_SSN_Before_Birth__c = false ;
        app.IDV_Applicant_Underage__c  = false ;
        
        //  Address - MINFraud
        app.Visitor_IP__c = '1.2.3.4' ;
        app.Visitor_User_Agent__c = 'Mozilla' ;
        app.Visitor_Accept_Language__c = 'YES' ;
        
        app.Unknown_Location__c = true ;
		
		return app ;
	}
    
	//  -------------------------------------
	//  ------------ Send Email -------------
	//  -------------------------------------
	
	static testmethod void test_sendWelcomeEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		ApplicationEmailFunctions.sendWelcomeEmails ( app1.Id ) ;
        
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		ApplicationEmailFunctions.sendWelcomeEmails ( app2.Id ) ;
        
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendWelcomeEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;
			
		Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtd ) ;
		insert app1;
        
		one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
            
    	ApplicationEmailFunctions.sendWelcomeEmails ( app1.Id ) ;
        
		//  End of testing
		Test.stopTest () ;
	}

    static testmethod void test_sendApprovedEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendApprovedEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
    static testmethod void test_sendApprovedEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
            
		ApplicationEmailFunctions.sendApprovedEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendFundingEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendFundingEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendFundingEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendFundingEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendAdditionalInfoEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		app1.FIS_Decision_Code__c = 'P090' ;
		app1.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		app2.FIS_Decision_Code__c = 'P090' ;
		app2.Application_Processing_Status__c = 'Additional Documentation Needed' ;
        app2.Additional_Info_Special_Instructions__c = 'Blah Blah Blah' ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendAdditionalInfoEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendAdditionalInfoEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		app1.FIS_Decision_Code__c = 'P090' ;
		app1.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		app2.FIS_Decision_Code__c = 'P090' ;
		app2.Application_Processing_Status__c = 'Additional Documentation Needed' ;
        app2.Additional_Info_Special_Instructions__c = 'Blah Blah Blah' ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendAdditionalInfoEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendTrialDepositEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendTrialDepositEmails ( idL ) ;
		
		ApplicationEmailFunctions.sendTrialDepositEmail ( app2.ID ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendTrialDepositEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendTrialDepositEmails ( idL ) ;
		
		ApplicationEmailFunctions.sendTrialDepositEmail ( app2.ID ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendTrialDepositFAILEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		ApplicationEmailFunctions.sendTrialDepositFailEmails ( app2.ID ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendTrialDepositFAILEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendTrialDepositFailEmails ( app2.ID ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendDenialEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;

        List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendDenialEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void test_sendDenialEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendDenialEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendWithdrawalEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendWithdrawalEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendWithdrawalEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendWithdrawalEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendIDVDeclineEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendIDVDeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendIDVDeclineEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
                
		ApplicationEmailFunctions.sendIDVDeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendOFACDeclineEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendOFACDeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendOFACDeclineEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendOFACDeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendFCRADeclineEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendFCRADeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendFCRADeclineEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
        
		ApplicationEmailFunctions.sendFCRADeclineEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
    }
    
	static testmethod void test_sendACHRejectionEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendACHRejectionEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendACHRejectionEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
				
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;
                
		ApplicationEmailFunctions.sendACHRejectionEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendCounterOfferEmails1 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;

        List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendCounterOfferEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendCounterOfferEmails2 ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;

        List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
        one_settings__c settings = getSettings ( 'ApplicationEmailFunctionsMC' ) ;
        insert settings ;

        ApplicationEmailFunctions.sendCounterOfferEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
    static testmethod void test_OneApplicationAfterUpdateTrigger ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;

		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		one_application__c app = new one_application__c();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		
		app.Contact__c = c1.Id;
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;
		
		app.Funding_Options__c = 'Mail' ;
		app.FIS_Application_ID__c = '111222333' ;
		app.FIS_Credit_Limit__c = 250.00 ;
		app.FIS_Decision_Code__c = null ;
		
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
        
        app.Unknown_Location__c = true ;

        insert app ;
				
	 	one_application__c o = Database.query ( 'SELECT Id FROM one_application__c WHERE Id = \'' + app.Id + '\''  ) ;
	 	
	 	o.FIS_Decision_Code__c = 'P064' ;
	 	update o ;
	 	
	 	o.FIS_Decision_Code__c = 'A002' ;
	 	update o ;
	 	
	 	o.Last_Application_Status_Change__c = System.now ().addDays ( -3 ) ;
	 	update o ;
	 	
	 	o.FIS_Decision_Code__c = null ;
	 	update o ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendNotACitizenEmails ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendNotACitizenEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendOutOfAreaEmails ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendOutOfAreaEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendMinorEmails ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendMinorEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_sendYoungEmails ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	

        Contact c1 = getContact ( 1 ) ;
		insert c1;
		
		Contact c2 = getContact ( 2 ) ;
		insert c2;
		
		one_application__c app1 = getApplicationForTests ( c1, c2, rtu ) ;
		insert app1;
		
		Contact c3 = getContact ( 3 ) ;
		insert c3;
		
		Contact c4 = getContact ( 4 ) ;
		insert c4;
		
		one_application__c app2 = getApplicationForTests ( c3, c4, rtd ) ;
		insert app2;
		
		List<String> idL = new List<String> () ;
		idL.add ( app1.Id ) ;
		idL.add ( app2.Id ) ;
		
		ApplicationEmailFunctions.sendYoungEmails ( idL ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
	static testmethod void test_EMPTY ()
	{
        String rtu = OneUnitedUtilities.getRecordTypeIdForObject ( 'UNITY Visa', 'one_application__c' ) ;
        String rtd = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
        
		ApplicationEmailFunctions.sendEmails ( null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ) ;

        Test.stopTest () ;
    }
}