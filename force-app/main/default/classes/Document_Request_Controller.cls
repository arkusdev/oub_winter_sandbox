public with sharing class Document_Request_Controller 
{
    //  ----------------------------------------------------------------------------------------
    //  Security ?!@?
    //  ----------------------------------------------------------------------------------------
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;
    
    //  ----------------------------------------------------------------------------------------
    //  Page objects
    //  ----------------------------------------------------------------------------------------
 	public Ticket__c t 
 	{ 
 		get; 
 		private set; 
 	}
 	
	//  Step control for panels on page
	public Integer drStepNumber
	{
		get ;
		private set ;
	}

    public list<SelectOption> docSelect
    {
        get ;
        private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
    public List<String> docTypeDisplayList
    {
        get ;
		private set ;
    }
    
    // Count of needed documents
    public Integer docTypeListSize
    {
    	get
    	{
    		Integer dtl = 0 ;
    		
    		if ( docTypeList != null )
    			dtl = docTypeList.size () ;
    			
    		return dtl ;
    	}
    	private set ;
    }
    
 	//  Thing
	public ContentVersion cv 
	{
		get 
		{
			if ( cv == null )
				cv = new ContentVersion () ;
            
			return cv ;
		}
		
		set ;
	}
 
    //  ----------------------------------------------------------------------------------------
    //  Errors
    //  ----------------------------------------------------------------------------------------
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
     
    //  ----------------------------------------------------------------------------------------
    //  Translate
    //  ----------------------------------------------------------------------------------------
    private map<String,String> docTypeMap ;

    //  ----------------------------------------------------------------------------------------
    //  Main Controller
    //  ----------------------------------------------------------------------------------------
    public Document_Request_Controller ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		drStepNumber = 0 ;
		
        //  First check for application in session
        t = (Ticket__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
 
 		//  If no session present, use querystring instead
        if ( ( t == null ) || ( t.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
			
			t = null ;

			//  Both need to be passed in
			if ( ( aid != null ) && ( kid != null ) )
			{
                list<Ticket__c> tL = null ;
                
				try
                {
                    tL = [
                        SELECT ID, Upload_Attachment_Key__c, 
                        Documents_Required__c, Documents_Request_Special_Instructions__c,
                        Account__c, Case__c, Contact__c, Financial_Account__c, Master_Ticket__c
                        FROM Ticket__c
                        WHERE ID = :aid
                        AND Upload_Attachment_Key__c = :kid
                        AND RecordType.DeveloperName = 'Document_Request'
                        ORDER BY ID
                        LIMIT 1
                    ] ;
				}
				catch ( DmlException e )
				{
					//  Defaults are already set
					for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
					{
				        System.debug ( e.getDmlMessage ( i ) ) ;
					}				
			    }
                
                if ( ( tL != null ) && ( tL.size () > 0 ) )
                {
					//  Grab the first one
					t = tL [ 0 ] ;
                }
				else
				{
		            System.debug ( '========== No Data found for ID & Key ==========' ) ;
				}
            }
        }
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = t.Id ;
			kid = t.Upload_Attachment_Key__c ;
		}
        
        //  Check again for things existing
		if ( ( t != null ) && ( aid != null ) && ( kid != null ) )
		{
            System.debug ( '========== Valid ticket found ==========' ) ;
            
            loadSelectDropDown () ;
            
            docTypeList = t.Documents_Required__c.split ( ';' ) ;
            
            docTypeDisplayList = new list<String> () ;
            for ( String dt : docTypeList )
                docTypeDisplayList.add ( docTypeMap.get ( dt ) ) ;
            
            docTypeDisplayList.sort () ;
            
            //  Set the step number
            if ( ( docTypeList != null ) && ( docTypeList.size () > 0 ) )
            {
                drStepNumber = 1000 ;
            }
            else if ( ( docTypeList != null ) && ( docTypeList.size () == 0 ) )
            {
                drStepNumber = 2000 ;
            }
        }
    }
    
    //  ----------------------------------------------------------------------------------------
	//  Validate & Upload the file
    //  ----------------------------------------------------------------------------------------
	public PageReference upload () 
	{
        // Defaults
		boolean isValid = true ;
		errorName = false ;
		errorFile = false ;
		
		//  Start with validation
		if ( String.isBlank ( cv.PathOnClient ) )
		{
			errorName = true ;
			isValid = false ;
		}
		
		if ( cv.VersionData == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        drStepNumber = 1002 ;
			return null ;
		}
        
        try 
		{
	        System.debug ( '========== Inserting Objects ==========' ) ;
            
            access.insertObject ( cv ) ;
            
            if ( ( cv != null ) && ( cv.ID != null ) )
            {
                System.debug ( 'CV ID :: ' + cv.ID ) ;
                
                ID cdID = ContentVersionHelper.getContentDocumentID ( cv.ID ) ;
                
                System.debug ( 'CD ID :: ' + cdID ) ;
                
                if ( cdID != null )
                {
                    list<ContentDocumentLink> cdlL = new list<ContentDocumentLink> () ;
                    
                    ContentDocumentLink cdl0 = new ContentDocumentLink () ;
                    cdl0.ContentDocumentId = cdID ;
                    cdl0.ShareType = 'V' ;
                    cdl0.LinkedEntityId = t.ID ;
                    cdl0.Visibility = 'AllUsers' ;
                    
                    cdlL.add ( cdl0 ) ;
                    
                    if ( t.Account__c != null )
                    {
                        ContentDocumentLink cdl1 = new ContentDocumentLink () ;
                        cdl1.ContentDocumentId = cdID ;
                        cdl1.ShareType = 'V' ;
                        cdl1.LinkedEntityId = t.Account__c ;
                        cdl1.Visibility = 'AllUsers' ;
                        
                    	cdlL.add ( cdl1 ) ;
                    }
                    
                    if ( t.Case__c != null )
                    {
                        ContentDocumentLink cdl2 = new ContentDocumentLink () ;
                        cdl2.ContentDocumentId = cdID ;
                        cdl2.ShareType = 'V' ;
                        cdl2.LinkedEntityId = t.Case__c ;
                        cdl2.Visibility = 'AllUsers' ;
                        
                    	cdlL.add ( cdl2 ) ;
                    }
                    
                    if ( t.Contact__c != null )
                    {
                        ContentDocumentLink cdl3 = new ContentDocumentLink () ;
                        cdl3.ContentDocumentId = cdID ;
                        cdl3.ShareType = 'V' ;
                        cdl3.LinkedEntityId = t.Contact__c ;
                        cdl3.Visibility = 'AllUsers' ;
                        
                    	cdlL.add ( cdl3 ) ;
                    }
                    
                    if ( t.Financial_Account__c != null )
                    {
                        ContentDocumentLink cdl4 = new ContentDocumentLink () ;
                        cdl4.ContentDocumentId = cdID ;
                        cdl4.ShareType = 'V' ;
                        cdl4.LinkedEntityId = t.Financial_Account__c ;
                        cdl4.Visibility = 'AllUsers' ;
                        
                    	cdlL.add ( cdl4 ) ;
                    }
                    
                    if ( t.Master_Ticket__c != null )
                    {
                        ContentDocumentLink cdl5 = new ContentDocumentLink () ;
                        cdl5.ContentDocumentId = cdID ;
                        cdl5.ShareType = 'V' ;
                        cdl5.LinkedEntityId = t.Master_Ticket__c ;
                        cdl5.Visibility = 'AllUsers' ;
                       
                    	cdlL.add ( cdl5 ) ;
                    }
                    
                    access.insertObjects ( cdlL ) ;
                }
                
                System.debug ( '========== Updating Ticket ==========' ) ;
                
                t.Documents_Received_Date_Time__c = System.now () ;
                t.Status__c = 'Received' ;
                t.Documents_Required__c = removeFoundDoc ( t.Documents_Required__c, cv.Title ) ;
                
                access.updateObject ( t ) ;
            }
        }
        catch ( DMLException e )
        {
            // ?
        }
        finally
        {
	        System.debug ( '========== Final stuff ==========' ) ;
            
	        //  Set panel to success upload
			drStepNumber = 1001 ;
			
			//  Remove document from list
			Integer j = 0 ;
			while ( j < docTypeList.size () )
			{
				if ( docTypeList [j].equalsIgnoreCase ( cv.Title ) )
					docTypeList.remove ( j ) ;
				j++ ;
            }
            
            // remove from displaylist
            Integer k = 0 ;
            while ( k < docTypeDisplayList.size () )
            {
                if ( docTypeDisplayList [ k ].equalsIgnoreCase ( docTypeMap.get ( cv.Title ) ) )
                    docTypeDisplayList.remove ( k ) ;
                k++ ;
            }
	            
			//  If all documents are gone, go to overall success
			if ( docTypeList.size () == 0 )
				drStepNumber = 2000 ;
			
			//  Clear errors if any
			errorName = false ;
    		errorFile = false ;
            
            //  clear out
            cv = new ContentVersion () ;
        }
        
        return null ;
    }

    //  ----------------------------------------------------------------------------------------
    //  Clear docs as they're added
    //  ----------------------------------------------------------------------------------------
	private String removeFoundDoc ( String docList, String doc )
	{
		String returnList = '' ;
		
		list<String> dL = docList.split ( ';' ) ;
		list<String> xL = new list<String> () ;
		
		for ( String s : dL )
		{
			if ( ! s.equalsIgnoreCase ( doc ) )
				xL.add ( s ) ;
		}
		
		boolean start = true ;
		for ( String s : xL )
		{
			if ( start )
				start = false ;
			else
				returnList = returnList + ';' ;
				
			returnList = returnList + s ;
		}
		
		return returnList ;
	}
    
    //  ----------------------------------------------------------------------------------------
	//  Load the dropdown list from the picklist
    //  ----------------------------------------------------------------------------------------
    private void loadSelectDropDown ()
    {
        docTypeMap = new map<String,String> () ;
        
        docSelect = new list<SelectOption> () ;
        docSelect.add ( new SelectOption ( '', '-- Documents --' ) ) ;
        
        Schema.DescribeFieldResult fieldResult = Ticket__c.Documents_Required__c.getDescribe () ;
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues () ;
        
        for( Schema.PicklistEntry pickListVal : ple)
        {
            docTypeMap.put ( picklistVal.getValue (), picklistVal.getLabel () ) ;
            
            docSelect.add ( new SelectOption ( picklistVal.getValue (), picklistVal.getLabel () ) ) ;
        }
    }
}