public class LeadQueue implements Queueable
{
    private List<Task> tL ;
    private list<Workshop_Attendee__c> waL ;
    private List<oneOpportunity__c> ooL ;
    
    public LeadQueue ()
    {
        
    }
    
    public LeadQueue ( list<Task> xL )
    {
        tL = xL ;
    }
    
    public LeadQueue ( list<Workshop_Attendee__c> xL )
    {
        waL = xL ;
    }
    
    public LeadQueue ( list<oneOpportunity__c> xL )
    {
        ooL = xL ;
    }
    
    public void execute(QueueableContext context) 
    {
        try
        {
            if ( ( tL != null ) && ( tL.size () > 0 ) )
                insert tL ;
            
            if ( ( waL != null ) && ( waL.size () > 0 ) )
                insert waL ;
            
            if ( ( ooL != null ) && ( ooL.size () > 0 ) )
                insert ooL ;
        }
        catch (Exception e)
        {
            // ?
        }
    }

}