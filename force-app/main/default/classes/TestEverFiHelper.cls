@isTest
public class TestEverFiHelper 
{
	/* --------------------------------------------------------------------------------
	 * Setup, Settings
	 * -------------------------------------------------------------------------------- */
    private static void setCustomSettings ()
    {
        EverFi_Settings__c efs = new EverFi_Settings__c () ;

        efs.Grant_Type__c = 'foo' ;
        efs.Client_ID__c = 'abc123' ;
        efs.Client_Secret__c = 'youandme' ;
        efs.Login_URL__c = 'https://www.oneunited.com/token' ;

        efs.User_URL__c = 'https://www.oneunited.com/users' ;
        efs.Location_URL__c = 'https://www.oneunited.com/locations' ;
        efs.Program_User_URL__c = 'https://www.oneunited.com/program_users' ;
        efs.Scroll_Size__c = 1000.0 ;
        
        efs.Email__c = 'test@user.com' ;

        insert efs ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Defaults
	 * -------------------------------------------------------------------------------- */
    private static void setDefaults ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        insert a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Account
	 * -------------------------------------------------------------------------------- */
    private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.Name = 'Test' + i ;
        return a ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Account a, Integer i )
	{
		Contact c = new Contact () ;
        
        c.AccountId = A.ID ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 *  Just the login
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TestLogin ()
    {
        setCustomSettings () ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        
        //  Go!
        Test.startTest () ;
        
        EverFiHelper efh = new EverFiHelper () ;
        efh.everFiLogin () ;
        
		//  Done        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Locations, not used
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TestLocations ()
    {
        setCustomSettings () ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        
        //  Go!
        Test.startTest () ;
        
        EverFiHelper efh = new EverFiHelper () ;
        efh.everFiLogin () ;
        
        efh.loadLocations () ;

		//  Done        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Users, not used
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TestUsers ()
    {
        setCustomSettings () ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        
        //  Go!
        Test.startTest () ;
        
        EverFiHelper efh = new EverFiHelper () ;
        efh.everFiLogin () ;
        
        efh.loadUsers () ;

		//  Done        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Program users, half way
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TestProgramUsersSimple ()
    {
        setCustomSettings () ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        
        //  Go!
        Test.startTest () ;
        
        EverFiHelper efh = new EverFiHelper () ;
        efh.everFiLogin () ;
        
        Datetime dt = System.now () ;
        efh.loadProgramUsersSimple ( dt ) ;

		//  Done        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Program users, half way
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TestProgramUsersNYET ()
    {
        setCustomSettings () ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        MockHTTPEverFiHelper.since = 'null' ;
        
        //  Go!
        Test.startTest () ;
        
        EverFiHelper efh = new EverFiHelper () ;
        efh.everFiLogin () ;
        
        Datetime dt = System.now () ;
        efh.loadProgramUsersSimple ( dt ) ;

        efh.updateSince () ;
        
        EverFiHelper.EverFiReporting efr = efh.getReporting () ;
        
        System.assertNotEquals ( efr, null ) ;
        
		//  Done        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 * ??
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TESTEverFiDataSave ()
    {
        setCustomSettings () ;
        setDefaults () ; // Required because TRIGGER
        
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( a1, 1 ) ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'User' ;
        c1.Email = 'test@test.com' ;
        insert c1 ;
        
        boolean success = false ;
        
        //  Load MOCK
		Test.setMock ( HttpCalloutMock.class, new MockHTTPEverFiHelper () ) ;
        MockHTTPEverFiHelper.scrollNUM = 2 ;
        
        Test.startTest () ;
        
        EverFiHelper efh = new EverFiHelper () ;

        efh.everFiLogin () ;

        efh.loadProgramUsersSimple () ;
        
		success = efh.saveEverFiData () ;
        
        EverFiHelper.EverFiReporting efr = efh.getReporting () ;
        
        efh.updateSince () ;
        
        Test.stopTest () ;
        
        System.assertEquals ( success, true ) ;
        
		list<EverFi_Profile__c> epL = [
            SELECT ID, Name
            FROM EverFi_Profile__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( epL.size (), 1 ) ;
        
        list<Module__c> mL = [
            SELECT ID, Name, Everfi_Profile__c, Everfi_Module__c
            FROM Module__c
            WHERE Everfi_Profile__c = :epL[0].ID
            AND Everfi_Module__c != NULL
        ] ;
        
        System.assertNotEquals ( mL.size (),  0 ) ;
	}
    
	/* --------------------------------------------------------------------------------
	 * ??
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TESTEverFiSimple ()
    {
        EverFiProgramUserSimple.Incentive_activities efpusia = new EverFiProgramUserSimple.Incentive_activities () ;
        efpusia.completion = 'Yo' ;
        efpusia.id = 123 ;
        efpusia.incentive_id = 234 ;
        efpusia.incentive_playlist_id = 345 ;
        efpusia.incentive_playlist_name = 'Test' ;
        efpusia.incentive_title = 'Testing' ;
        efpusia.start_datetime = '11/11/2011' ;
        efpusia.status = 'Test' ;
        
        EverFiProgramUserSimple.User efpusu = new EverFiProgramUserSimple.User () ;
        efpusu.email = 'test@user.com' ;
        efpusu.first_name = 'Test' ;
        efpusu.last_name = 'User' ;
        efpusu.id = 123 ;
        efpusu.type = 'tester' ;
        efpusu.foundry_user_id = 'tuser' ;
        
        EverFiProgramUserSimple.User_content efpusuc = new EverFiProgramUserSimple.User_content () ;
        efpusuc.completed_at = '11/11/2011' ;
        efpusuc.content_name = 'Test' ;
        efpusuc.id = 123 ;
        efpusuc.opened_at = '11/11/2011' ;
        efpusuc.started_at = '11/11/2011' ;
        efpusuc.state = 'MA' ;
        
        EverFiProgramUserSimple.Program efpusp = new EverFiProgramUserSimple.Program () ;
        efpusp.id = 123 ;
        efpusp.name = 'Test' ;
        
        EverFiProgramUserSimple.Responses efpusr = new EverFiProgramUserSimple.Responses () ;
        efpusr.id = 123 ;
        efpusr.question_answer = 'Yo' ;
        efpusr.question_id = 324 ;
        efpusr.question_text = 'Test?' ;
        
        list<EverFiProgramUserSimple.Incentive_activities> efpusiaL = new list<EverFiProgramUserSimple.Incentive_activities> () ;
        efpusiaL.add ( efpusia ) ;
        
        list<EverFiProgramUserSimple.Responses> efpusrL = new list<EverFiProgramUserSimple.Responses> () ;
        efpusrL.add ( efpusr ) ;
        
        list<EverFiProgramUserSimple.User_content> efpusucL = new list<EverFiProgramUserSimple.User_content> () ;
        efpusucL.add ( efpusuc ) ;
        
        EverFiProgramUserSimple.Data efpusd = new EverFiProgramUserSimple.Data () ;
        efpusd.id = 123 ;
        efpusd.updated_at = '1/12/2018' ;
        efpusd.user = efpusu ;
        efpusd.incentive_activities = efpusiaL ;
        efpusd.program = efpusp ;
        efpusd.responses = efpusrL ;
        efpusd.user_content = efpusucL ;
        efpusd.deleted = false ;
        
        list<EverFiProgramUserSimple.Data> efpusdL = new list<EverFiProgramUserSimple.Data> () ;
        efpusdL.add ( efpusd ) ;
        
        EverFiProgramUserSimple.Next efpusn = new EverFiProgramUserSimple.Next () ;
        efpusn.href = 'https://www.test.com' ;
        efpusn.scroll_id = '1234' ;
        efpusn.since = '11/11/2011' ;
        
        EverFiProgramUserSimple efpus = new EverFiProgramUserSimple () ;
        efpus.data = efpusdL ;
        efpus.next = efpusn ;
    }
}