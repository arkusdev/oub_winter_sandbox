public class Q2BillerHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Settings
	 *  -------------------------------------------------------------------------------------------------- */	
    Q2_Billing_Settings__c q2bs = null ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Internals
	 *  -------------------------------------------------------------------------------------------------- */	
    private Contact c ;
    private list<Service__C> sL ;
    
    private ClientToken ct = null ;
    private UpsertClient uc = null ;
    private Integer paymentCardCount ;

    private String errorMessage ;
    private Integer statusCode ;
    
    map<String,Q2BillerPaymentMethods.cls_data> q2bpmDM = null ;
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */	
    public Q2BillerHelper ()
    {
        c = null ;
        sL = null ;
        
        ct = null ;
        uc = null ;
        
        q2bs = Q2_Billing_Settings__c.getInstance ( 'settings' ) ;
        
        q2bpmDM = new map<String,Q2BillerPaymentMethods.cls_data> () ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Contact
	 *  -------------------------------------------------------------------------------------------------- */	
    public void setContact ( ID xID )
    {
        list<Contact> cL = null ;
        
        try
        {
            cL = [
                SELECT ID, FirstName, LastName, Email, Phone,
                MailingAddress, MailingStreet, MailingCity, MailingState, MailingPostalCode
                FROM Contact
                WHERE ID = :xID
                LIMIT 1
            ] ;
        }
        catch ( DMLException e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
        }
        
        if ( ( cL != null ) && ( cL.size () > 0 ) )
            setContact ( cL [ 0 ] ) ;
    }
    
    public void setContact ( Contact xC )
    {
        c = xC ;
        
        try
        {
            sL = [
                SELECT ID, Card_Number__c, Expiration_Date__c
                FROM Service__C
                WHERE Type__c = 'Debit Card'
                AND Status__c = 'Active'
                AND Contact__c = :c.ID
            ] ;
        }
        Catch ( DMLException e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            sL = null ;
        }
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Create client (customer)
	 *  -------------------------------------------------------------------------------------------------- */	
    @testVisible
    private boolean upsertCustomer () // TEMP
    {
        boolean isSuccess = false ;
        
        HttpRequest request = new HttpRequest () ;
		request.setEndPoint ( q2bs.Q2B_Upsert_Client_URL__c ) ;
		request.setMethod ( 'POST' ) ;
        request.setHeader ( 'Authorization', 'Bearer ' + q2bs.API_Key__c ) ; 
		request.setHeader ( 'Content-Type', 'application/json' ) ;
		request.setBody ( generateUpsertClientJSON () ) ;
        
        System.debug ( '========== Calling method ==========' ) ;
        Http http = new Http () ;
        HTTPResponse res = null ;
        try
        {
            res = http.send ( request ) ;
        }
        catch ( System.CalloutException e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            isSuccess = false ;
        }
        
        //  Success ?!?
        if ( res != null )
        {
            if  ( res.getStatusCode () == 200 )
            {
                JSONParser parser = JSON.createParser ( res.getBody () ) ;
                
                while ( parser.nextToken() != null )
                {
                    if ( parser.getCurrentToken() == JSONToken.START_OBJECT ) 
                    {
                        uc = (UpsertClient) parser.readValueAs ( UpsertClient.class ) ;
                        
                        System.debug ( 'Client ID returned :: ' + uc.ClientID ) ;
                        
                        isSuccess = true ;
                    }
                }
            }
            else
            {
                printResponseError ( res ) ;
            }
        }
        else
        {
            isSuccess = false ;
        }
        
        return isSuccess ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  upsert client JSON
	 *  -------------------------------------------------------------------------------------------------- */	
    private String generateUpsertClientJSON ()
    {
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
        
		//  Create the object
		g.writeStartObject () ;

		g.writeStringField ( 'identifier', c.ID ) ;
        
        g.writeFieldName ( 'meta' ) ;

        g.writeStartObject () ;
        
		g.writeStringField ( 'firstName', c.FirstName ) ;
		g.writeStringField ( 'lastName', c.LastName ) ;
		g.writeStringField ( 'email', c.Email ) ;
		g.writeStringField ( 'phone', c.Phone ) ;
		g.writeStringField ( 'address1', c.MailingStreet ) ;
		g.writeStringField ( 'address2', '' ) ;
		g.writeStringField ( 'city', c.MailingCity ) ;
		g.writeStringField ( 'state', c.MailingState ) ;
		g.writeStringField ( 'zipcode', c.MailingPostalCode ) ;
        
        g.writeEndObject () ;
        
		//  End of object
		g.writeEndObject () ;
        
        //  Done!
        return g.getAsString () ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Get payment methods
	 *  -------------------------------------------------------------------------------------------------- */	
    @testVisible
    private boolean getPaymentMethods ()
    {
        boolean isSuccess = false ;
        
        HttpRequest request = new HttpRequest () ;
		request.setEndPoint ( q2bs.Q2B_List_Payment_URL__c + '/' + uc.clientID ) ;
		request.setMethod ( 'GET' ) ;
        request.setHeader ( 'Authorization', 'Bearer ' + q2bs.API_Key__c ) ;
        
        System.debug ( '========== Calling method ==========' ) ;
        Http http = new Http () ;
        HTTPResponse res = null ;
        
        try
        {
            res = http.send ( request ) ;
        }
        catch ( System.CalloutException e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            isSuccess = false ;
        }
        
        //  Success ?!?
        if ( res != null )
        {
            if  ( res.getStatusCode () == 200 )
            {
                String responseBody = res.getBody () ;
                responseBody = responseBody.replace ( '_id', 'x_id' ) ;
                responseBody = responseBody.replace ( 'number', 'xnumber' ) ;
                responseBody = responseBody.replace ( 'default', 'xdefault' ) ;
                
                Q2BillerPaymentMethods q2bpm = Q2BillerPaymentMethods.parse ( responseBody ) ;
                
                if ( q2bpm != null )
                {
                    if ( ( q2bpm.data != null ) && ( q2bpm.data.size () > 0 ) )
                    {
                        for ( Q2BillerPaymentMethods.cls_data q2bpmd : q2bpm.data )
                        {
                            if ( q2bpmd.type.equalsIgnoreCase ( 'card' ) )
                            {
                                //  #1
                                q2bpmDM.put ( q2bpmd.identifier, q2bpmd ) ;
                                
	                            //  #2
                                Q2BillerPaymentMethods.cls_card q2bpmc = q2bpmd.card ;
                                String xfoo = q2bpmc.xnumber.right ( 4 ) + '!' + q2bpmc.expDate ;
                                q2bpmDM.put ( xfoo, q2bpmd ) ;
                            }
                        }
                    }
                }
                
                isSuccess = true ;
            }
            else
            {
                printResponseError ( res ) ;
            }
        }
        
        return isSuccess ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Update payment methods
	 *  -------------------------------------------------------------------------------------------------- */	
    @testVisible
    private boolean updatePaymentMethods ()
    {
        boolean isSuccess = false ;
        
        boolean hazPayment = false ;
        
        //  Compare results
        if ( ( sL != null ) && ( sL.size () > 0 ) )
        {
            for ( Service__c s : sL )
            {
                //  #2, calculate
        		if (s.Expiration_Date__c != null) {
                	Datetime dt = Datetime.newInstance ( s.Expiration_Date__c.year (), s.Expiration_Date__c.month (), s.Expiration_Date__c.day () ) ;
                    String foo = s.Card_Number__c.right ( 4 ) + '!' + dt.format ( 'YYYY-MM-dd' ) + 'T12:00:00.000Z' ;
    
                    //  New card doesn't have an entry from them
                    if ( ( ! q2bpmDM.containsKey ( s.ID ) ) && ( ! q2bpmDM.containsKey ( foo ) ) )
                    {
                        HttpRequest request = new HttpRequest () ;
                        request.setEndPoint ( q2bs.Q2B_Create_Payment_URL__c ) ;
                        request.setMethod ( 'POST' ) ;
                        request.setHeader ( 'Authorization', 'Bearer ' + q2bs.API_Key__c ) ;
                        request.setHeader ( 'Content-Type', 'application/json' ) ;
                        request.setBody ( generateCreatePaymentMethodJSON ( s ) ) ;
                        
                        System.debug ( '========== Calling method ==========' ) ;
                        Http http = new Http () ;
                        HTTPResponse res = null ;
                        
                        try
                        {
                            res = http.send ( request ) ;
                        }
                        catch ( System.CalloutException e )
                        {
                            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
                            isSuccess = false ;
                        }
                        
                        //  Success ?!?
                        if ( res != null )
                        {
                            if  ( res.getStatusCode () == 200 )
                            {
                                JSONParser parser = JSON.createParser ( res.getBody () ) ;
                                
                                while ( parser.nextToken () != null )
                                {
                                    if ( parser.getCurrentToken() == JSONToken.START_OBJECT ) 
                                    {
                                        AddPayment ap = (AddPayment) parser.readValueAs ( AddPayment.class ) ;
                                        
                                        isSuccess = true ;
                                    }
                                }
                            }
                            else
                            {
                                printResponseError ( res ) ;
                            }
                        }
                    }
                    else
                    {
                        System.debug ( 'Found prior!' ) ;
                    }
                }
            }
        }
        
        return isSuccess ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  client token JSON
	 *  -------------------------------------------------------------------------------------------------- */	
    private String generateCreatePaymentMethodJSON ( Service__c s )
    {
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
        
		//  Create the object
		g.writeStartObject () ;
        
        g.writeStringField ( 'clientId', uc.clientID ) ;
        g.writeStringField ( 'paymentType', 'card' ) ;
        g.writeStringField ( 'identifier', s.ID ) ;
        g.writeStringField ( 'cardNumber', s.Card_Number__c ) ;
//        g.writeStringField ( 'cardCode', '' ) ;
        if (s.Expiration_Date__c != null) {
            g.writeStringField ( 'expMonth', formatMonth ( s.Expiration_Date__c.month () ) ) ;
            g.writeStringField ( 'expYear', String.valueOf ( s.Expiration_Date__c.year () ) ) ;
        }
        g.writeStringField ( 'zipcode', c.MailingPostalCode ) ;
        g.writeStringField ( 'name', c.FirstName + ' ' + c.LastName ) ;
        g.writeBooleanField ( 'setAsDefault', false ) ;

        g.writeEndObject () ;
        
        System.debug ( 'PM : ' + g.getAsString () ) ;
        
        return g.getAsString () ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Create client token
	 *  -------------------------------------------------------------------------------------------------- */
    private boolean clientCreateToken ()
    {
        boolean isSuccess = false ;
        
        HttpRequest request = new HttpRequest () ;
		request.setEndPoint ( q2bs.Q2B_Client_Token_URL__c ) ;
		request.setMethod ( 'POST' ) ;
        request.setHeader ( 'Authorization', 'Bearer ' + q2bs.API_Key__c ) ;
		request.setHeader ( 'Content-Type', 'application/json' ) ;
		request.setBody ( generateClientCreateTokenJSON () ) ;
        
        System.debug ( '========== Calling method ==========' ) ;
        Http http = new Http () ;
        HTTPResponse res = null ;
        
        try
        {
            res = http.send ( request ) ;
        }
        catch ( System.CalloutException e )
        {
            errorMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            isSuccess = false ;
        }
        
        //  Success ?!?
        if ( res != null )
        {
            if  ( res.getStatusCode () == 200 )
            {
                JSONParser parser = JSON.createParser ( res.getBody () ) ;
                
                while ( parser.nextToken() != null )
                {
                    if ( parser.getCurrentToken() == JSONToken.START_OBJECT ) 
                    {
                        ct = (ClientToken) parser.readValueAs ( ClientToken.class ) ;
                        
                        System.debug ( 'CLIENT TOKEN ::' + ct.token ) ;
                        
                        isSuccess = true ;
                    }
                }
            }
            else
            {
                printResponseError ( res ) ;
            }
        }
        else
        {
            isSuccess = false ;
        }
        
        return isSuccess ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  client token JSON
	 *  -------------------------------------------------------------------------------------------------- */	
    private String generateClientCreateTokenJSON ()
    {
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
        
		//  Create the object
		g.writeStartObject () ;

		g.writeStringField ( 'identifier', c.ID ) ; // ? 
        g.writeBooleanField ( 'nonce', false ) ;
        
        g.writeFieldName ( 'meta' ) ;

        g.writeStartObject () ;
        
		g.writeStringField ( 'firstName', c.FirstName ) ;
		g.writeStringField ( 'lastName', c.LastName ) ;
		g.writeStringField ( 'email', c.Email ) ;
		g.writeStringField ( 'phone', c.Phone ) ;
		g.writeStringField ( 'address1', c.MailingStreet ) ;
		g.writeStringField ( 'address2', '' ) ;
		g.writeStringField ( 'city', c.MailingCity ) ;
		g.writeStringField ( 'state', c.MailingState ) ;
		g.writeStringField ( 'zipcode', c.MailingPostalCode ) ;
        
        g.writeEndObject () ;
        
		//  End of object
		g.writeEndObject () ;
        
        //  Done!
        return g.getAsString () ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Token
	 *  -------------------------------------------------------------------------------------------------- */	
    @testVisible
    private String getClientToken ()
    {
        String x = null ;
        boolean isSuccess = clientCreateToken () ;
        
        if ( isSuccess )
            x = ct.token ;
        
        return x ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Attempt to setup customer and forward to card swap.
	 *  -------------------------------------------------------------------------------------------------- */	
    public String doEverythingAndForward ()
    {
        String x = null ;
        
        //  Load teh client
        boolean isSuccess1 = upsertCustomer () ;
        
        if ( isSuccess1 )
        {
            //  Get payment methods
            boolean isSuccess2 = getPaymentMethods () ;
            
            if ( isSuccess2 )
            {
                //  Update ones that are missing
                boolean isSuccess3 = updatePaymentMethods () ;
            }
        }
        
        String token = getClientToken () ;
        
        if ( String.isNotBlank ( token ) )
            x = q2bs.Q2B_Card_Swap_URL__c + '?token=' + token ;
        
        return x ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Just forward to cardswap without customer setup
	 *  -------------------------------------------------------------------------------------------------- */	
    public String justForward ()
    {
        String x = null ;
        
        String token = getClientToken () ;
        
        if ( String.isNotBlank ( token ) )
            x = q2bs.Q2B_Card_Swap_URL__c + '?token=' + token ;
        
        return x ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Sigh
	 *  -------------------------------------------------------------------------------------------------- */	
    private String formatMonth ( Integer m )
    {
        String x = String.valueOf ( m ) ;
        
        if ( x.length () == 1 ) 
            x = '0' + x ;
        
        return x ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ERROR
	 *  -------------------------------------------------------------------------------------------------- */	
    public string getErrorMessage ()
    {
        return errorMessage ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  ERROR
	 *  -------------------------------------------------------------------------------------------------- */	
	private void printResponseError ( HTTPResponse res )
    {
        System.debug ( '-------------------------- ERROR --------------------------' ) ;
        System.debug ( 'Status :: ' + res.getStatusCode () ) ;
		System.debug ( 'Code   :: ' + res.getStatusCode () ) ;
        System.debug ( 'Body   :: ' + res.getBody () ) ;
        System.debug ( '-------------------------- ----- --------------------------' ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Classes for Interfaces
	 *  -------------------------------------------------------------------------------------------------- */	
    public class UpsertClient
    {
        public String clientID ;
    }
    
    public class ClientToken
    {
        public String token ;
    }
    
    public class AddPayment
    {
        public String paymentMethodId ;
    }
}