@isTest
public with sharing class Test_batch_ScheduledTaskReassign 
{
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
	private static User getStandardUser ( String thingy )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'Standard User'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
        u.Department = 'test department' ;
		
		insert u ;
		
		return u ;
	}

	public static testmethod void testOne ()
	{
		setBatchSettings () ;
		
		User uA = getStandardUser ( 'A' ) ;
		
		String xID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Scheduled Email Template', 'Task' ) ;
		
		Task t1 = new Task () ;
		t1.RecordTypeID = xID ;
		t1.OwnerID = uA.ID ;
		t1.Subject = 'Test Subject' ;
		t1.Status = 'Open' ;
		t1.Email_Date_and_Time__c = Date.today().addDays ( 1 ) ;
		
		System.runAs ( uA )
		{
			insert t1 ;
		}
		
		list<Task> xTLo = [
			SELECT ID, CreatedByID
			FROM Task
			WHERE CreatedByID = :uA.ID
		] ;
		
		System.assertEquals ( xTLo.size () , 1 ) ;
		
		Test.startTest () ;
		
		batch_ScheduledTaskReassign b = new batch_ScheduledTaskReassign () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
		
		list<Task> xTLp = [
			SELECT ID, CreatedByID
			FROM Task
			WHERE CreatedByID = :uA.ID
		] ;
		
		System.assertEquals ( xTLp.size () , 0 ) ;
	}
}