@isTest
public with sharing class Test_batch_CreditCardPromotionTicket 
{
	// -----------------------------------------------------------------------------
	// SETUP @_@    
	// -----------------------------------------------------------------------------
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
	private static oneOffers__c getOffer ()
	{
    	oneOffers__c oo = new oneOffers__c () ;
    	
    	oo.Name = 'Test Offer' ;
    	oo.External_Offer_ID__c = 'abc,xyz,pbq' ;
    	oo.Dollar_Amount__c = 25.00 ;
    	oo.Promotion_Code__c = 'UGET25' ;
    	oo.Start_Date__c = Date.Today () - 1 ;
    	oo.End_Date__c = Date.Today () + 1 ;
    	oo.Active__c = true ;
    	oo.Plan__c = '999' ;
    	oo.Product__c = 'XTZ' ;
    	oo.SubProduct__c = '000' ;
        oo.Terms_ID__c = 'ANOTHER' ;
        oo.Bill_Code__c = 'LAST MINUTE' ;
        oo.Offer_Confirm_Text__c = 'BLAH THANKS JIM' ;
    	
    	insert oo ;
		
		return oo ;	
	}
	
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' + i ;
		c.LastName = 'User' + i ;
		c.Email = 'test'+ i + '@user' + i + '.com' ;
		
		insert c ;
		
		return c ;
	}
	
	private static one_Application__c getApplication ( Contact c, oneOffers__c oo )
	{
		one_application__c o = new one_application__c () ;
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		o.Contact__c = c.Id ;
		o.First_Name__c = c.FirstName ;
		o.Last_Name__c = c.LastName ;
		o.Email_Address__c = c.Email ;
		o.Street_Address__c = c.MailingStreet ;
		o.State__c = c.MailingState ;
		o.City__c = c.MailingCity ;
		o.ZIP_Code__c = c.MailingPostalCode ;
		o.Mailing_Address_Same__c = 'Yes' ;
		
		o.Own_Rent_Other__c = 'Rent' ;
		o.Monthly_Rent_Payment__c = 1000.00 ;
		o.How_Long_mths__c = 1 ;
		o.How_Long_yrs__c = 11 ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		o.Additional_Income_Y_N__c = 'No' ;

		o.Employment_Status__c = 'Retired' ;
		o.Employer__c = 'N/A' ;
		o.Position_Occupation__c = 'N/A' ;
		
		o.Credit_Report_Authorization__c = true ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		o.Funding_Account_Number__c = '1343432432' ;
		o.Funding_Routing_Number__c = '132456789' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.RequestedCreditLimit__c = 2500.00 ;
		
		o.Application_Approved_Date__c = DateTime.now () ;
		
		o.FIS_Application_ID__c = '3211321' ;
		o.FIS_Decision_Code__c = 'A001' ;
		o.FIS_Credit_Score__c = '980' ;
		o.FIS_Decision_Letter__c = 'A' ;
		o.FIS_Credit_Limit__c = 2500.00 ;
		
		o.Unknown_Location__c = true ;
		o.Invalid_Email_Address__c = true ;
		o.Invalid_Co_Applicant_Email_Address__c = true ;
		
		o.Offer__c = oo.ID ;
		o.Promotion_Code__c = oo.Promotion_Code__c ;
		
		insert o ;
				
		return o ;
	}
	
	private static Financial_Account__c getFinancialAccount ( Contact c, one_Application__c o )
	{
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;

		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		
		fa.Credit_Card_Application__c = o.ID ;
		fa.OPENDATE__c = Date.Today () - 100 ;
		fa.CONTRACTDATE__c = Date.Today () - 100 ;
		fa.TAXRPTFORPERSNBR__c = c.ID ;
		fa.Past_Due_History__c = 'ZZZZZZZZZ000' ;
		
		insert fa ;
		
		return fa ;
	}

	// -----------------------------------------------------------------------------
	// TEST METHODS
	// -----------------------------------------------------------------------------
	static testmethod void something ()
	{
		//  Setup
		setBatchSettings () ;
		oneOffers__c oo = getOffer () ;
		Contact c1 = getContact ( 1 ) ;
		one_Application__c o1 = getApplication ( c1, oo ) ;
		Financial_Account__c fa1 = getFinancialAccount ( c1, o1 ) ;
		
		//  ACTUAL TEST
		Test.startTest () ;
		
		batch_CreditCardPromotionTicket b = new batch_CreditCardPromotionTicket () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest () ;
		
		// TEST?
		Financial_Account__c x = [ SELECT ID, Promotion_Batch_Ticket__c FROM Financial_Account__c WHERE ID = :fa1.ID ] ;
		
		System.assertNotEquals ( x.Promotion_Batch_Ticket__c, NULL ) ;
	}
}