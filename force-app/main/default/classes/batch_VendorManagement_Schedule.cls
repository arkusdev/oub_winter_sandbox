global class batch_VendorManagement_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		Vendor_Management_Settings__c vms = Vendor_Management_Settings__c.getInstance ( 'VM_Settings' ) ;
		
		Date d = Date.today () ;	
		
		if ( vms != null )
		{
		    /*  ----------------------------------------------------------------------------------------
		     *  Upcoming Expiration
		     *  ---------------------------------------------------------------------------------------- */
		   if ( vms.Contract_Notification__c == true )
		   {
		   		if ( vms.Contract_Notification_Days__c != null )
		   		{
			   		List<String> dL = vms.Contract_Notification_Days__c.split ( ',' ) ;
			   		
			   		for ( String s : dL )
			   		{
			   			if ( d.day () == Integer.valueOf ( s ) )
			   			{
							batch_VM_Contract_Notification b = new batch_VM_Contract_Notification () ;
							Database.executeBatch ( b ) ;
			   			}
			   		}
		   		}
		   }
		   
		    /*  ----------------------------------------------------------------------------------------
		     *  Contract HAS Expired
		     *  ---------------------------------------------------------------------------------------- */
		   if ( vms.Contract_Expiration__c == true )
		   {
				batch_VM_Contract_Expiration b = new batch_VM_Contract_Expiration () ;
				Database.executeBatch ( b ) ;
		   }
		     
		    /*  ----------------------------------------------------------------------------------------
		     *  Status Notification
		     *  ---------------------------------------------------------------------------------------- */
			if ( vms.Status_Notification__c == true )
			{
				if ( ( vms.Status_Notification_Day__c != null ) && ( vms.Status_Notification_Day__c == d.Day () ) )
				{
					batch_VM_Status_Notification b = new batch_VM_Status_Notification () ;
					Database.executeBatch ( b ) ;
				}
			}
			
		    /*  ----------------------------------------------------------------------------------------
		     *  Monthly Notification
		     *  ---------------------------------------------------------------------------------------- */
			if ( vms.Monthly_Notifications__c == true )
			{
				if ( ( vms.Monthly_Notification_Day__c != null ) && ( vms.Monthly_Notification_Day__c == d.Day () ) )
				{
					batch_VM_Monthly_Notification b = new batch_VM_Monthly_Notification () ;
					Database.executeBatch ( b ) ;
				}
			}
		     
		    /*  ----------------------------------------------------------------------------------------
		     *  Monthly Expiration Warning
		     *  ---------------------------------------------------------------------------------------- */
			if ( vms.Monthly_Upcoming_Warnings__c == true )
			{
				if ( ( vms.Monthly_Upcoming_Warning_Day__c != null ) && ( vms.Monthly_Upcoming_Warning_Day__c == d.Day () ) )
				{
					batch_VM_Monthly_Expiration b = new batch_VM_Monthly_Expiration () ;
					Database.executeBatch ( b ) ;
				}
			}
			
		    /*  ----------------------------------------------------------------------------------------
		     *  Yearly Reminder
		     *  ---------------------------------------------------------------------------------------- */
			if ( vms.Yearly_Reminder_Enabled__c == true )
			{
				if ( ( vms.Yearly_Reminder_Enabled__c != null ) && ( vms.Yearly_Reminder_Date__c == d ) )
				{
					batch_VM_Yearly_Reminder b = new batch_VM_Yearly_Reminder () ;
					Database.executeBatch ( b ) ;
				}
			}
		}
   	}
}