@isTest
public with sharing class TestDepositApplicationEmailHistory 
{
	static testmethod void applicationEmailClassTest ()
	{
		//  Point to page to test
		PageReference pr = Page.Deposit_Application_Email_History_Page ;
		Test.setCurrentPage ( pr ) ;
		
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		Deposit_Application__c app = new Deposit_Application__c ();
		
		app.Tax_Reported_Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email__c = c1.Email ;
		
		app.Co_Applicant_Tax_Reported_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email__c = c2.Email ;
		
		insert app;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController c = new ApexPages.StandardController ( app ) ;
		
		//  Mock HTTP Response, Search version
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGeneratorSearch () ) ;	
		
		//  Feed the standard controller to the page controller
		Deposit_Application_Email_History controller = new Deposit_Application_Email_History ( c ) ;

		//  Check the result object
		List<Deposit_Application_Email_History.MandrillJSONSearchResponse> rL = controller.mjrList ;
		
		for ( Deposit_Application_Email_History.MandrillJSONSearchResponse r : rL )
		{
			System.assertEquals ( r.ts, 1391537268 ) ;
			System.assertEquals ( r.state, 'sent' ) ;
			System.assertEquals ( r.subject, 'Thanks for Applying for Your UNITY Visa Card' ) ;
			System.assertEquals ( r.email, 'lmattera@oneunited.com' ) ;
			
			for ( String t : r.tags )
				System.assertEquals ( t, 'welcome-email' ) ;
			
			System.assertEquals ( r.opens, 0 ) ;
			System.assertEquals ( r.clicks, 0 ) ;
			
			List<Deposit_Application_Email_History.MandrillJSONSearchResponseSMTPEvents> rsL = r.smtp_events ;
			
			for ( Deposit_Application_Email_History.MandrillJSONSearchResponseSMTPEvents rs : rsL )
			{
				System.assertEquals ( rs.ts, 1391537269 ) ;
				System.assertEquals ( rs.type, 'sent' ) ;
				System.assertEquals ( rs.diag, '250 Ok: queued as 0B9CA1622052' ) ;
				System.assertEquals ( rs.source_ip, '198.2.128.8' ) ;
				System.assertEquals ( rs.destination_ip, '204.60.84.2' ) ;
				System.assertEquals ( rs.size, 35959 ) ;
			}
			
			System.assertEquals ( r.subaccount, 'UNITYVISAWELCOME' ) ;
			
			for ( String s : r.resends )
				System.assertEquals ( s, '0' ) ;
			
			System.assertEquals ( r.sender, 'noreplies@unityvisa.com' ) ;
			System.assertEquals ( r.template, 'unity-visa-thanks-for-applying' ) ;
			
			List<Deposit_Application_Email_History.MandrillJSONSearchResponseOpens> roL = r.opens_detail ;
			
			for ( Deposit_Application_Email_History.MandrillJSONSearchResponseOpens ro : roL )
			{
				System.assertEquals ( ro.ts, 1234 ) ;
				System.assertEquals ( ro.ip, '111.222.111.222' ) ;
				System.assertEquals ( ro.location, 'xyz' ) ;
				System.assertEquals ( ro.ua, 'abc' ) ;
			}
			
			List<Deposit_Application_Email_History.MandrillJSONSearchResponseClicks> rcL = r.clicks_detail ;
			
			for ( Deposit_Application_Email_History.MandrillJSONSearchResponseClicks rc : rcL )
			{
				System.assertEquals ( rc.ts, 1234 ) ;
				System.assertEquals ( rc.url, 'http://foo.bar.com' ) ;
				System.assertEquals ( rc.ip, '111.222.111.222' ) ;
				System.assertEquals ( rc.location, 'xyz' ) ;
				System.assertEquals ( rc.ua, 'abc' ) ;
			}
			
		}

		//  End of testing
		Test.stopTest () ;
	}
}