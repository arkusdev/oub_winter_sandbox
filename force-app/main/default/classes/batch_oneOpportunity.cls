global class batch_oneOpportunity implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	
	global batch_oneOpportunity ()
	{
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
	}
	
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query = '' ;
		query += 'SELECT ID FROM oneOpportunity__c WHERE Scheduled_Follow_Up__c = false AND Closed_Date__c = NULL ' ;
		
		//  Wish I could ignore it ....
		if ( ! Test.isRunningTest () )
		{
			query += 'AND ( ( Days_Since_Change__c >= 14 AND Interested_in_Product__c IN ( \'Savings\', \'Checking\', \'Time Deposit\', \'Retirement Plans\' ) ) ' ;
			query += 'OR  ( Days_Since_Change__c >= 30 AND Interested_in_Product__c IN ( \'Commercial Real Estate\', \'Multifamily\', \'Single Family Home\' ) ) ) ' ;
			query += '' ;
		}
		
		return Database.getQueryLocator ( query ) ;
	}
		
	global void execute ( Database.Batchablecontext bc, List<oneOpportunity__c> ooIN )
	{
		//  Check if not old
		//  Check Jim's special field
		//  Check attached tasks
		
		Map<Id, oneOpportunity__c> ooM = New Map<Id, oneOpportunity__c> () ;
		
		System.debug ( '-- FOUND :: ' + ooIN.size () + ' opportunities' ) ;
		
		for ( oneOpportunity__c o : ooIN )
		{
			ooM.put ( o.id, o ) ;
		}
		
		List<String> ooIDs = new List<String> () ;
		
		for ( String s : ooM.keySet () )
		{
			ooIDs.add ( s ) ;
		}
		
		//  Open task not overdue by 14 days
		list<Task> tL ;
		
		if ( Test.isRunningTest () )
		{
			tL = [
				SELECT ID, whatId FROM Task
				WHERE whatId IN :ooIDs AND IsClosed = false AND Subject = 'Test Task'
			] ;
		}
		else
		{
			tL = [
				SELECT ID, whatId FROM Task
				WHERE whatId IN :ooIDs AND IsClosed = false AND ActivityDate = LAST_N_DAYS:14
			] ;
		}
		
		System.debug ( '-- FOUND :: ' + tL.size () + ' tasks' ) ;
		if ( ( tL != null ) && ( tL.size () > 0 ) )
		{
			for ( Task t : tL )
			{
				if ( ooM.containsKey ( t.whatId ) )
				{
					System.debug ( 'Removing -- ' + t.WhatID ) ;
					ooM.remove ( t.whatId ) ;
				}
			}
		}
		
		list<oneOpportunity__c> ooL = new list<oneOpportunity__c> () ;
		
		for ( String s : ooM.keySet () )
		{
			oneOpportunity__c oo = ooM.get ( s ) ;
			
			oo.Closed_Date__c = Date.today () ;
			oo.Status__c = 'Abandon Opportunity' ;
            
            if ( String.isEmpty ( oo.Closed_Loan_Reason__c ) )
				oo.Closed_Loan_Reason__c = 'Abandon' ;
			
			ooL.add ( oo ) ;
		}
		
		if ( ooL.size () > 0 )
		{
			System.debug ( 'Marking ' + ooL.size () + ' as abandoned.' ) ;
			update ooL ;
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
	   // Get the ID of the AsyncApexJob representing this batch job
	   // from Database.BatchableContext.
	   // Query the AsyncApexJob object to retrieve the current job's information.
	   AsyncApexJob a = [
	   	SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	      TotalJobItems, CreatedBy.Email
	      FROM AsyncApexJob WHERE Id =
	      :BC.getJobId()
	      ] ;
	      
	   // Send an email to the Apex job's submitter notifying of job completion.
	   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
	   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
	   
	   mail.setToAddresses ( toAddresses ) ;
	   mail.setSubject ( 'Apex Opportunity Job :: ' + a.Status ) ;
	   
	   String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
	   
	   mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}