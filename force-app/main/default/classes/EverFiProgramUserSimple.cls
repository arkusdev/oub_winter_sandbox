public class EverFiProgramUserSimple 
{
	public class Incentive_activities {
		public Integer id {get;set;} 
		public Integer incentive_id {get;set;} 
		public String incentive_title {get;set;} 
		public Integer incentive_playlist_id {get;set;} 
		public String incentive_playlist_name {get;set;} 
		public String status {get;set;} 
		public String start_datetime {get;set;} 
		public String completion {get;set;} 
    }
    
	public class User {
		public Integer id {get;set;} 
		public String type {get;set;}
		public String email {get;set;} 
		public String last_name {get;set;} 
		public String first_name {get;set;} 
		public String foundry_user_id {get;set;} 
    }
    
	public class Program {
		public Integer id {get;set;} 
		public String name {get;set;} 
    }
    
	public class Next {
		public String since {get;set;} 
		public String scroll_id {get;set;} 
		public String href {get;set;} 
    }

	public class User_content {
		public Integer id {get;set;} 
		public String state {get;set;} 
		public String opened_at {get;set;} 
		public String started_at {get;set;} 
		public String completed_at {get;set;} 
		public String content_name {get;set;} 
    }

	public class Responses {
		public Integer id {get;set;} 
		public Integer question_id {get;set;} 
		public String question_text {get;set;} 
		public String question_answer {get;set;} 
    }
    
	public class Data {
		public Integer id {get;set;} 
		public User user {get;set;} 
		public Boolean deleted {get;set;} 
		public Program program {get;set;} 
		public List<Responses> responses {get;set;} 
		public String updated_at {get;set;} 
		public List<User_content> user_content {get;set;} 
		public List<Incentive_activities> incentive_activities {get;set;} 
    }
    
	public Next next {get;set;} 
	public List<Data> data {get;set;} 
    
    public static EverFiProgramUserSimple parse(String json) {
        return (EverFiProgramUserSimple) System.JSON.deserialize(json, EverFiProgramUserSimple.class);
    }
}