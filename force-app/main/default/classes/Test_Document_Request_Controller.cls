@isTest
public class Test_Document_Request_Controller 
{
	// ------------------------------------------------------------
	// Setup
	// ------------------------------------------------------------
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		return b ;
	}
	
	private static Contact getContact ( Integer i, Account a, Branch__c b )
	{
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		
		Contact c = new Contact () ;
        c.AccountId = a.ID ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'test'+ i + '.person' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		return c ;
	}
	
	private static Account getAccount ( Integer i )
	{
		Account a = new Account () ;
		a.Name = 'Test' + i + 'Account' + i ;
		a.FederalTaxID__c = 'FA-000' + i ;
		
		return a ; 
	}
	
	private static Financial_Account__c getFinancialAccount ( Contact c, Account a, Branch__c b )
	{
		String loanRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan', 'Financial_Account__c' ) ;
		
		Financial_Account__c f = new Financial_Account__c () ;
		f.ACCTNBR__c = '1111333355559999' ;
		f.CURRMIACCTTYPCD__c = 'QB01' ;
		f.MJACCTTYPCD__c = 'MTG' ;
		f.OPENDATE__c = Datetime.now ().date () ;
		f.BRANCHORGNBR__c = b.ID ;
		f.RecordTypeID = loanRecordTypeId ;
		
		if ( c != null )
			f.TAXRPTFORPERSNBR__c = c.Id ;
			
		if ( a != null )
			f.TAXRPTFORORGNBR__c = a.Id ;
		
		return f ;
	}
    
    private static Ticket__c getTicketShell ()
    {
        Ticket__c t = new Ticket__c () ;
        t.recordTypeID = Schema.SObjectType.Ticket__c.RecordTypeInfosByName.get('Document Request').RecordTypeId;
        t.Documents_Request_Special_Instructions__c = 'Yo Noid' ;
        t.Documents_Required_Status__c = 'Documentation Needed' ;
        
        return t ;
    }
	
	// ------------------------------------------------------------
	// Go?!
	// ------------------------------------------------------------
	public static testmethod void Parameters ()
    {
        branch__c b = getBranch () ;
        insert b;
        
        account a1 = getAccount ( 1 ) ;
        insert a1 ;
            
        Contact c1 = getContact ( 1, a1, b ) ;
        insert c1 ;
        
        Financial_Account__c fa1 = getFinancialAccount ( c1, a1, b ) ;
        insert fa1 ;
        
        Ticket__c t = getTicketShell () ;
        t.Documents_Required__c = 'Bank Statement;Death Certificate' ;
        t.Account__c = a1.ID ;
        t.Contact__c = c1.ID ;
        t.Financial_Account__c = fa1.ID ;
        
        insert t ;
        
        Ticket__c tx = [
            SELECT ID, Upload_Attachment_Key__c
            FROM Ticket__c
            WHERE ID = :t.ID 
        ] ;
        
		//  Point to page to test
		PageReference pr1 = Page.Document_Request ;
		Test.setCurrentPage ( pr1 ) ;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', tx.id );
        ApexPages.currentPage().getParameters().put ( 'kid', tx.Upload_Attachment_Key__c );
        
		//  Empty application to pass in
		Ticket__c tc = new Ticket__c () ;

		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( tc ) ;
        
		//  Feed the standard controller to the page controller
		Document_Request_Controller drc = new Document_Request_Controller ( sc ) ;
        
        System.assertEquals ( drc.drStepNumber, 1000 ) ;
        System.assertEquals ( drc.docTypeListSize, 2 ) ;
        
        drc.upload () ;
        
        System.assertEquals ( drc.drStepNumber, 1002 ) ;
        
        ContentVersion cv1 = new ContentVersion () ;
        cv1.title = 'Bank Statement' ;
        cv1.PathOnClient = 'bank.docx' ;
        cv1.VersionData = Encodingutil.base64Decode ( 'Tests' ) ;
        
        drc.cv = cv1 ;
        
        drc.upload () ;
        
        System.assertEquals ( drc.drStepNumber, 1001 ) ;
        System.assertEquals ( drc.docTypeListSize, 1 ) ;
    }
}