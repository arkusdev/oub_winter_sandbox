public class COCCSWIMHelper 
{
    /*  --------------------------------------------------------------------------------------------------
	 *  Internal Variables
	 *  -------------------------------------------------------------------------------------------------- */   
    private List<String> swimLines ;
	private Batch_Process_Settings__c bps ;
    private Date batchDate ;
    
    private String applicationID ;
    private String accountNumber ;
    private Decimal amount ;
    private String description ;
    
    private String cashbox ;
    private String fromAccount ;
    private String toAccount ;

    /*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */   
    public COCCSWIMHelper ()
    {
		batchDate = Date.today () ;
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
        
        resetSWIMFile () ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Reset
	 *  -------------------------------------------------------------------------------------------------- */
    public void resetSWIMFile ()
    {
        swimLines = new list<String> () ;
	}
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Helpers, Deposits
	 *  -------------------------------------------------------------------------------------------------- */
    public static String isDeposit ()
    {
        return 'GLR ' ;
    }
    
    public static String isDescriptiveDeposit ()
    {
        return 'DEPD' ;
    }
    
    public static String isExternalDeposit ()
    {
        return 'XDEP' ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Helpers, Withdrawal
	 *  -------------------------------------------------------------------------------------------------- */
    public static String isWithdrawal ()
    {
        return 'GLD ' ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Helpers, Cashbox
	 *  -------------------------------------------------------------------------------------------------- */
    public void setCashboxFromSettings ()
    {
        setCashbox ( bps.Application_Cashbox__c ) ;
    }
    
    public void setCashbox ( String s )
    {
    	cashbox = s ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Generate a record
	 *  -------------------------------------------------------------------------------------------------- */
    public void createSWIMRecord ( String direction, String accountNumber, Decimal amount, String applicationID, String description )
    {
        if ( String.isBlank ( cashbox ) )
            cashbox = bps.Application_Cashbox__c ;
        
        createSWIMRecord ( direction, accountNumber, cashbox, amount, applicationID, description ) ;
    }
    
    public void createSWIMRecord ( String direction, String accountNumber, String cashbox, Decimal amount, String applicationID, String description )
    {
        String swimLine = '' ;
        
        //  Account number
        swimLine += accountNumber.leftPad ( 17 ).replace ( ' ', '0' ) ;
        
        //  in/out?
        swimLine += direction ;
        
        //  Amount
        swimLine += getNumberWithCents ( amount ).leftPad ( 10 ).replace ( ' ', '0' ) ;
        
        //  Number of items
        swimLine += '000001' ;
        
        //  Effective Date of Transaction
        swimLine += getBatchDate () ;
        
        //  Transaction Description -- Max length 45!
        if ( description.length () > 45 )
            swimLine += description.left ( 45 ) ;
        else
        	swimLine += description.rightPad ( 45 ) ;
        
        //  Trace Number
        String shortAppID = applicationID ;
        if ( applicationID.length () > 7 )
        	shortAppID = applicationID.right ( 7 ) ;
        
        swimLine += bps.Routing_Number__c + shortAppID.leftPad ( 15-bps.Routing_Number__c.length () ).replace ( ' ', '0' ) ;
        
        //  Cashbox
        swimLine += cashbox.leftPad ( 10 ).replace ( ' ', '0' ) ;
        
        //  Retirement Code
        swimLine += '    ' ;
        
        //  Retirement Year
        swimLine += '    ' ;
        
        //  Fund Type Code
        swimLine += 'EL  ' ;
        
        //  Fund Type Detail
        swimLine += 'INTR' ;
        
        //  Clearing Category Code
        swimLine += 'IMED' ;
        
        //  Check Number
        swimLine += '          ' ;
        
        //  Balance Category Code
        swimLine += '    ' ;
        
        //  Balance Type Code
        swimLine += '    ' ;
        
        //  Reverse Paid in Effect
        swimLine += ' ' ;
        
        //  END OF LINE
        swimLine += '\r\n' ;
        
        swimLines.add ( swimLine ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Generate a double record
	 *  -------------------------------------------------------------------------------------------------- */
    public void createSWIMDoubleRecord ( String fromAccountNumber, String toAccountNumber, Decimal amount, String applicationID, String description )
    {
        if ( String.isBlank ( cashbox ) )
            cashbox = bps.Application_Cashbox__c ;
        
        createSWIMDoubleRecord ( fromAccountNumber, toAccountNumber, cashbox, amount, applicationID, description ) ;
    }
    
    public void createSWIMDoubleRecord ( String fromAccountNumber, String toAccountNumber, String cashbox, Decimal amount, String applicationID, String description )
    {
        createSWIMRecord ( isWithdrawal (), fromAccountNumber, cashbox, amount, applicationID, description ) ;
        
        createSWIMRecord ( isDeposit (), toAccountNumber, cashbox, amount, applicationID, description ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Generate file
	 *  -------------------------------------------------------------------------------------------------- */   
    public String getSWIMFile ()
    {
        String swimFile = '' ;
        
        for ( String swimLine : swimLines )
        {
            swimFile += swimLine ;
        }
        
        return swimFile ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Formatters
	 *  -------------------------------------------------------------------------------------------------- */   
   private String getBatchDate ()
   {
       String sMonth = String.valueof ( batchDate.month () ) ;
       String sDay = String.valueof ( batchDate.day () ) ;
            
       if ( sMonth.length () == 1 )
           sMonth = '0' + sMonth;
    
       if ( sDay.length () == 1 )
           sDay = '0' + sDay;
       
       return String.valueof ( batchDate.year () ) + sMonth + sDay ;
   }
    
    private String getNumberWithCents ( Decimal x )
    {
        //  Convert to integer to get the whole amount
        Integer y = x.intValue () ;
        
        //  Remainder
        Decimal z = x - y ;
        
        //  Convert to String
        String foo = String.valueof ( y ) ;
        
        //  Default to trailing zeroes
        String bar = '00' ;
        
        //  Convert remainder to String and override zeroes
        if ( z > 0 )
        {
            bar = String.valueOf ( (z * 100).intValue () ) ;
            
			if ( bar.length () == 1 )
			{
				bar = '0' + bar ;
			}
        }
        
        //  Combine!
        return foo + bar ;
    }
}