public class DynamicOfferHandler 
{
    // This class acts as a controller for the DynamicObjectViewer component
    
    private String objType ;
    
    public String branchId
    {
        public get ; private set ;
    }
    
    public String offerName
    {
        public get ; private set ;
    }
    
    public String departmentId
    {
        public get ; private set ;
    }
    
    public Map<String,Integer> yesMap 
    {
        public get ; private set ;
    }
    
    public Map<String,Integer> noMap 
    {
        public get ; private set ;
    }
    
    public Map<String,Boolean> ownedProductMap
    {
        public get ; private set ;
    }
    
	public boolean validDataForOpportunity { 
		public get ; 
		private set ;
	}
	
	public List<String> missingDataForOpportunity {
		public get ;
		private set ;
	}

    public list<oneOffers__c> offerList;
    private Map<String,List<Offer_Filter_Criteria__c>> offerFilterCriteraMap ;
            
    public sObject obj 
    { 
        get; 
        set 
        {
			setObjectType ( value ) ;
            String uid = Userinfo.getUserId();
            setUserBranchDepartment ( uid ) ;
              
            validateInBound ( value ) ;

            //discoverAccessibleFields(value);
            //obj = reloadObjectWithAllFieldData();
            setupOffers();
            setupResponseCounter ( value ) ;
              
			offerList = filterProducts ( value ) ;
            loadPriorProducts ( value ) ;
              
            reSortOffers () ;
        }
    }
        
    public List<oneOffers__c> getOfferList() {
          return(this.offerList);
    }
    
    // The sObject type as a string
    public String getObjectType() {
          return(this.objType);
    }
    
    public String setObjectType ( sObject newObj ) 
    {
        this.objType = newObj.getSObjectType().getDescribe().getName();
        return ( this.objType ) ;
    }
    
	//  --------------------------------------------------------------------------------------
    //  Obtain the list of offers for the Target Object Type
	//  --------------------------------------------------------------------------------------
    private void setupOffers () 
    {
        this.offerList = [
         Select Id,
                Name,
                Image__c, 
                Opportunity_Product__c,
                Offer_Icon__c,
                Offer_Sales_Coaching__c,
                Offer_Order__c,
                Offer_Criteria_Object__c,
                Related_To_Field__c,
                Filter_Criteria__c,
                Offer_Target_Object__c,
                Target_Filter_Critera__c
           From oneOffers__c
          where Active__c=true
            and Start_Date__c <= TODAY
            and End_Date__c>= TODAY 
            and Offer_Target_Object__c = :objType
            ORDER BY Offer_Order__c
         ];
        
        offerFilterCriteraMap = loadOfferFilterCritera ( offerList ) ; 
        
        //  Load zeros into yes and no maps for prior offers
        yesMap = new Map<String,Integer> () ;
        noMap = new Map<String,Integer> () ;
        
        //  VisualForce doesn't handle missing keys gracefully, so...
        //  Set up map of owned products to default to false and or zero counts
        ownedProductMap = new Map<String,Boolean> () ;
        
        for ( oneOffers__c o : offerList )
        {
        	System.debug ( '-- Putting ' + o.Name + ' into yes/no maps.' ) ;
        	
            yesMap.put ( o.Id, 0 ) ;
            noMap.put ( o.Id, 0 ) ;
            
            if ( o.ID != null )
            {
	        	System.debug ( '-- Putting ' + o.Name + ' into owned map as false.' ) ;
                ownedProductMap.put ( o.ID, false ) ;
            }
        }
    }
    
	//  --------------------------------------------------------------------------------------
	//  Sort the offers by modifying the order based on weight
	//  --------------------------------------------------------------------------------------
    private void reSortOffers ()
    {
        for ( oneOffers__c o : offerList )
        {
            System.debug ( 'O : ' + o.Id + ' -- ' + o.Offer_Order__c ) ;
            
            if ( o.Offer_Order__c == null )
            	o.Offer_Order__c = 100 ;
            
            //  Weight the ones we find heavily so they fall to the end
            if ( ( ownedProductMap.containsKey ( o.ID ) ) && ( ownedProductMap.get ( o.ID ) == true ) )
                o.Offer_Order__c += 100000 ;
        }

        //  Create wrapper for sorting
        List<OfferWrapper> offerWrapperList = new List<OfferWrapper> () ;

        //  Load offers into wrapper
        for ( oneOffers__c o : offerList )
        {
            offerWrapperList.add ( new OfferWrapper ( o ) ) ;
        }

        //  Re-sort!
        offerWrapperList.sort () ;

        //  Empty old list
        offerList.clear () ;

        //  Re-load list from the wrapper
        for ( OfferWrapper ow : offerWrapperList )
        {
            offerList.add ( ow.offer ) ;
        }
    }
    
	//  --------------------------------------------------------------------------------------
	//  Determine how many times they've been offered something
	//  --------------------------------------------------------------------------------------
    private void setupResponseCounter ( SObject tObj )
    {
        String theID = (String) tObj.get ( 'id' ) ;
        
        //  Grab all the offers at once
        List<Offer_Response__c> orL = [
            SELECT Id, Name, Offer__c, Response__c
            FROM Offer_Response__c 
            WHERE (
                Contact__c = :theID
                OR Account__c = :theID
            )
        ] ;
        
        //  Double loop to determine if they've been offered this or not
        if ( ( orL != null ) && ( orL.size () > 0 ) )
        {
            for ( Offer_Response__c orT : orL )
            {
                for ( oneOffers__c o : offerList )
                {
                    if ( orT.Offer__c == o.id )
                    {
                    	if ( orT.Response__c != null )
                    	{
	                        if ( orT.Response__c.equalsIgnoreCase ( 'Yes' ) )
	                        {
	                            if ( yesMap.containsKey ( o.Id ) )
	                            {
	                                Integer oCount = yesMap.get ( o.Id ) ;
	                                oCount++ ;
	                                yesMap.put ( o.Id, oCount ) ;
	                            }
	                            else
	                            {
	                            	//  Shouldn't happen! - zeros should have been loaded for all offers at the start
	                            	yesMap.put ( o.Id, 1 ) ;
	                            }
	                        }
	                        else if ( orT.Response__c.equalsIgnoreCase ( 'No' ) )
	                        {
	                            if ( noMap.containsKey ( o.Id ) )
	                            {
	                                Integer oCount = noMap.get ( o.Id ) ;
	                                oCount++ ;
	                                noMap.put ( o.Id, oCount ) ;
	                            }
	                            else
	                            {
	                            	//  Shouldn't happen! - zeros should have been loaded for all offers at the start
	                            	noMap.put ( o.Id, 1 ) ;
	                            }
	                        }
                    	}
                    }  
                }
            }
        }
    }
	//  --------------------------------------------------------------------------------------
	//  Set up offer filter critera
	//  --------------------------------------------------------------------------------------
    private Map<String,List<Offer_Filter_Criteria__c>> loadOfferFilterCritera ( List<oneOffers__c> oL )
    {
        System.debug ( '-------------------------------------------------- LOADING FILTER CRITERA --------------------------------------------------' ) ;
    	
    	List<Offer_Filter_Criteria__c> ofcL = new List<Offer_Filter_Criteria__c> () ;
    	
        //  List of offer IDs to feed to query
        List<String> ooIDL = new List<String> () ;
        for ( oneOffers__c o : oL )
        {
            ooIDL.add ( o.Id ) ;
        }
        
        //  Select all filter critera into a big list
        ofcL = [
            SELECT ID, Name, oneOffer__c,
            Field__c, Operator__c, Value__c
            FROM Offer_Filter_Criteria__c
            WHERE oneOffer__c IN :ooIDL
            ORDER BY oneOffer__c, Name
        ] ;
        
        //  Going to break up the big list into smaller lists and shove them into a map based on the offer
        Map<String,List<Offer_Filter_Criteria__c>> ofcML = new Map<String,List<Offer_Filter_Criteria__c>> () ;
        
        if ( ( ofcL != null ) && ( ofcL.size () > 0 ) )
        {
            //  Set up the defaults
            String pO = 'XXXXXX' ;
            List<Offer_Filter_Criteria__c> ofcN = new List<Offer_Filter_Criteria__c> () ;
            
            //  Needs to hold state through the loop, so outside
            String ugh ;
            
            for ( Offer_Filter_Criteria__c ofc : ofcL )
            {
				// ID is technically not a String ... Apex barfs when you try and use the two together so cast to string first
                ugh = ofc.oneOffer__c ;  
                
                //  If the offer ID changes, we've got a new offer so add current list to the overall map and restart the list
                if ( ( ! pO.equalsIgnoreCase ( ugh ) ) && ( ! pO.equalsIgnoreCase ( 'XXXXXX' ) ) )
                {
                    ofcML.put ( pO, ofcN ) ;
                    ofcN = new List<Offer_Filter_Criteria__c> () ;
                }
                
                //  Add to the current list
                ofcN.add ( ofc ) ;
                
                //  Update the last pointer
                pO = ugh ;
            }
            
            //  Load the last one in
            ofcML.put ( pO, ofcN ) ;
        }
        else
        {
            //  This shouldn't ever happen!
            System.debug ( 'No filter criteria found for any offers ?!' ) ;
        }
    	
    	return ofcML ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Setup to look up prior products
	//  --------------------------------------------------------------------------------------
    private void loadPriorProducts ( SObject tObj )
    {
        String xID = (String) tObj.get ( 'id' ) ;
        
        List<String> xAccounts = returnFinancialAccountList ( xID, offerList ) ;
        
        for ( String s : xAccounts )
        {
        	//  Owned products will have their key overwritten from false to true
            ownedProductMap.put ( s, true ) ;
        }
    }
    
	//  --------------------------------------------------------------------------------------
	//  Look up prior products based on filter criteria
	//  --------------------------------------------------------------------------------------
    private List<String> returnFinancialAccountList ( String xID, List<oneOffers__c> ooL )
    {
        List<String> aL = new List<String> () ;
        
        if ( ( ooL != null ) && ( ooL.size () > 0 ) )
        {
            System.debug ( '-------------------------------------------------- QUERIES --------------------------------------------------' ) ;
            
            for ( oneOffers__c o : ooL )
            {
                System.debug ( '-------------------------------------------------- BASE --------------------------------------------------' ) ;
                System.debug ( 'Offer :: ' + o.Name ) ;
                
                //  Constuct baseline query
                String q = '' ;
                q += 'SELECT ID ' ;
                q += 'FROM ' + o.Offer_Criteria_Object__c + ' ' ;
                q += 'WHERE ' + o.Related_To_Field__c + ' = \'' + xID + '\'' ;
                q += '' ;
                
                System.debug ( '-------------------------------------------------- FILTER --------------------------------------------------' ) ;
                
                if ( o.Filter_Criteria__c != null )
                {
                    //  Filter criteria base string
                    String f = '' ;
                    f+= ' AND ' + o.Filter_Criteria__c + ' ' ;
                    f+= '' ;
                    
                    System.debug ( 'F :: ' + f ) ;
                    
                    //  Pull list for this offer out of the map
                    List<Offer_Filter_Criteria__c> ofcPL = offerFilterCriteraMap.get ( o.ID ) ;
                    
                    if ( ( ofcPL != null ) && ( ofcPL.size () > 0 ) )
                    {
                        for ( Offer_Filter_Criteria__c ofcP : ofcPL )
                        {
                            //  Contruct the comparison
                            String p = ofcP.Field__c + ' ' + ofcP.Operator__c + ' ' + ofcP.Value__c ;
                            System.debug ( 'P :: ' + p ) ;
                            
                            //  Swap it into the base string
                            f = f.replace ( '{' + ofcP.name + '}', p ) ;
                            System.debug ( 'F :: ' + f ) ;
                        }
                    }
                    else
                    {
                        //  Without critera we're pretty much guaranteed a match?!
                        System.debug ( 'No filter critera found?!?' ) ;
                    }
                    
                    //  Add the filter stuff to the base query
                    q = q + f ;
                
	                System.debug ( '-------------------------------------------------- FINAL --------------------------------------------------' ) ;
	                System.debug ( q ) ; 
	                
	                try
	                {
	                    List<SObject> sOL = Database.query ( q ) ;
	                    
	                    //  If anything comes back they have the current product
	                    if ( ( sOL != null ) && ( sOL.size () > 0 ) )
	                    {
	                    	System.debug ( 'ADDING as OWNED - ' + o.ID ) ;
	                        aL.add ( o.ID ) ;
	                    }
	                }
	                catch ( DmlException e )
	                {
	                    // Bad Query....
	                    System.debug ( 'Bad query formed - check the filters!' ) ;
	                }
                }
                else
                {
                	System.debug ( 'No filter found, skipping check!' ) ;
                }
                
                System.debug ( '-------------------------------------------------- LOOP --------------------------------------------------' ) ;
            }
            
            System.debug ( '-------------------------------------------------- END --------------------------------------------------' ) ;
        }
        
        return aL ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Setup to strip out offers
	//  --------------------------------------------------------------------------------------
    private List<oneOffers__c> filterProducts ( SObject tObj )
    {
        String xID = (String) tObj.get ( 'id' ) ;
        
        Map<String,String> xOffers = filterTargetCritera ( xID, offerList ) ;
        
        List<oneOffers__c> oL = new List<oneOffers__c> () ; 
        
        for ( oneOffers__c o : offerList )
        {
        	if ( ! xOffers.containsKey ( o.Id ) )
        	{
        		oL.add ( o ) ;
        	}
        	else
        	{
        		System.debug ( 'Offer ' + o.Name + ' filtered out' ) ;
        	}
        }
        
        return oL ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Filter out offers based on target criteria
	//  --------------------------------------------------------------------------------------
    private Map<String,String> filterTargetCritera ( String xID, List<oneOffers__c> ooL )
    {
        Map<String,String> aM = new Map<String,String> () ;
        
        if ( ( ooL != null ) && ( ooL.size () > 0 ) )
        {
            System.debug ( '-------------------------------------------------- QUERIES --------------------------------------------------' ) ;
            
            for ( oneOffers__c o : ooL )
            {
                System.debug ( '-------------------------------------------------- BASE --------------------------------------------------' ) ;
                
                //  Constuct baseline query
                String q = '' ;
                q += 'SELECT ID ' ;
                q += 'FROM ' + o.Offer_Target_Object__c + ' ' ;
                q += 'WHERE ID = \'' + xID + '\'' ;
                q += '' ;
                
                System.debug ( '-------------------------------------------------- FILTER --------------------------------------------------' ) ;
            	
                if ( o.Target_Filter_Critera__c != null )
                {
                    //  Filter criteria base string
                    String f = '' ;
                    f+= ' AND ' + o.Target_Filter_Critera__c + ' ' ;
                    f+= '' ;
                    
                    System.debug ( 'F :: ' + f ) ;
                    
                    //  Pull list for this offer out of the map
                    List<Offer_Filter_Criteria__c> ofcPL = offerFilterCriteraMap.get ( o.ID ) ;
                    
                    if ( ( ofcPL != null ) && ( ofcPL.size () > 0 ) )
                    {
                        for ( Offer_Filter_Criteria__c ofcP : ofcPL )
                        {
                            //  Contruct the comparison
                            String p = ofcP.Field__c + ' ' + ofcP.Operator__c + ' ' + ofcP.Value__c ;
                            System.debug ( 'P :: ' + p ) ;
                            
                            //  Swap it into the base string
                            f = f.replace ( '{' + ofcP.name + '}', p ) ;
                            System.debug ( 'F :: ' + f ) ;
                        }
                    }
                    else
                    {
                        //  Without critera we're pretty much guaranteed a match?!
                        System.debug ( 'No filter critera found?!?' ) ;
                    }
                    
                    //  Add the filter stuff to the base query
                    q = q + f ;
                
	                System.debug ( '-------------------------------------------------- FINAL --------------------------------------------------' ) ;
	                System.debug ( q ) ; 
	                
	                try
	                {
	                    List<SObject> sOL = Database.query ( q ) ;
	                    
	                    //  If anything comes back they qualify
	                    if ( ( sOL != null ) && ( sOL.size () > 0 ) )
	                    {
	                    	System.debug ( 'Qualifies!' ) ;
	                    }
	                    else
	                    {
	                    	System.debug ( ' NOT QUALIFIED - ADDING as REMOVED - ' + o.ID ) ;
	                        aM.put ( o.ID, o.ID ) ;
	                    }
	                }
	                catch ( DmlException e )
	                {
	                    // Bad Query....
	                    System.debug ( 'Bad query formed - check the filters!' ) ;
	                }
                }
                else
                {
                	System.debug ( 'No filter found, skipping check!' ) ;
                }
                
                System.debug ( '-------------------------------------------------- LOOP --------------------------------------------------' ) ;
            }
            
            System.debug ( '-------------------------------------------------- END --------------------------------------------------' ) ;
        }
    	
    	return aM ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Look up branch & department based on the user to set into the offer response
	//  --------------------------------------------------------------------------------------
    private void setUserBranchDepartment ( String uid )
    {
    	List<User> uL = [
    		SELECT ID, Branch__c, Department
    		FROM User
    		WHERE ID=:uid
    	] ;
    	
    	if ( ( uL != null ) && ( uL.size () > 0 ) )
    	{
    		User u = uL [ 0 ] ;
    	
	    	if ( u.Branch__c != null )
	    	{
		    	List<Branch__c> bL = [
		    		SELECT ID
		    		FROM Branch__c
		    		WHERE Name=:u.Branch__c
		    	] ;
		    	
		    	if ( ( bL != null ) && ( bL.size () > 0 ) )
		    	{
		    		Branch__c b = bL [ 0 ] ;
		    		branchId = b.Id ;
		    	}
	    	}
	    	
	    	if ( u.Department != null )
	    	{
		    	List<SFDC_Project__c> dL = [
		    		SELECT ID 
		    		FROM SFDC_Project__c
		    		WHERE SFDC_Project_Name__c = :u.Department
		    	] ;
		    	
		    	if ( ( dL != null ) && ( dL.size () > 0 ) )
	    		{
					SFDC_Project__c d = dL [ 0 ] ;		
		    		departmentId = d.Id ;
	    		}
	    	}
    	}
    }
    
	//  --------------------------------------------------------------------------------------
	//  Wrapper class to resort offers
	//  --------------------------------------------------------------------------------------
    public class OfferWrapper implements Comparable 
    {
		public oneOffers__c offer = new oneOffers__c () ;
        
        // Constructor
        public offerWrapper ( oneOffers__c offerRecord ) 
        {
            offer = offerRecord ;
        }
        
        // Compare offers based on the oneOffers__c Sort.
        public Integer compareTo ( Object compareTo ) 
        {
            // Cast argument to OfferWrapper
            OfferWrapper compareToOffer = (OfferWrapper) compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if ( offer.Offer_Order__c > compareToOffer.offer.Offer_Order__c ) 
            {
                // Set return value to a positive value.
                returnValue = 1;
            } 
            else if ( offer.Offer_Order__c < compareToOffer.offer.Offer_Order__c ) 
            {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue ;       
        }
    }
    
	//  --------------------------------------------------------------------------------------
	//  Determine if they have all the required stuff for offers to work
	//  --------------------------------------------------------------------------------------
    private void validateInBound ( SObject tObj )
    {
    	validDataForOpportunity = true ;
    	missingDataForOpportunity = new List<String> () ;
    	
    	String xID = (String) tObj.get ( 'id' ) ;
    	
    	// Contact
    	if ( tObj instanceOf Contact )
    	{
    		List<Contact> cL = [
    			SELECT ID, Email, Phone, Name 
    			FROM Contact 
    			WHERE ID = :xID
    		] ;
    		
    		if ( ( cL != null ) && ( cL.size () > 0 ) )
    		{
    			Contact c = cL [ 0 ] ;
    			
	    		if ( String.isBlank ( c.Email ) )
	    		{
	    			validDataForOpportunity = false ;
	    			missingDataForOpportunity.add ( 'Email' ) ;
	    		}
	    		
	    		if ( String.isBlank ( c.Phone ) )
	    		{
	    			validDataForOpportunity = false ;
	    			missingDataForOpportunity.add ( 'Phone Number' ) ;
	    		}
	    		
	    		if ( String.isNotBlank ( c.Name ) )
	    		{
	    			offerName = ' for ' + c.Name ;
	    		}
    		}
    		else
    		{
    			validDataForOpportunity = false ;
    			missingDataForOpportunity.add ( 'No Contact Found?!?' ) ;
    		}
    	}
    	
    	// Account
    	else if ( tObj instanceOf Account )
    	{
    		List<Account> aL = [
    			SELECT ID, Business_Email__c, Phone, Name 
    			FROM Account 
    			WHERE ID = :xID
    		] ;
    		
    		if ( ( aL != null ) && ( aL.size () > 0 ) )
    		{
	    		Account a = aL [ 0 ] ;
	    		
	    		if ( String.isBlank ( a.Phone ) )
	    		{
	    			validDataForOpportunity = false ;
	    			missingDataForOpportunity.add ( 'Phone Number' ) ;
	        	}
	        	
	    		if ( String.isBlank ( a.Business_Email__c ) )
	    		{
	    			validDataForOpportunity = false ;
	    			missingDataForOpportunity.add ( 'Email' ) ;
	        	}
	        	
	    		if ( String.isNotBlank ( a.Name ) )
	    		{
	    			offerName = ' for ' + a.Name ;
	    		}
	    	}
	    	else
	    	{
    			validDataForOpportunity = false ;
    			missingDataForOpportunity.add ( 'No Account Found?!?' ) ;
	    	}
    	}
    }
}