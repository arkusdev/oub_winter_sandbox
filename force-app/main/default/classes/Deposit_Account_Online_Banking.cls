public class Deposit_Account_Online_Banking 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Page stuff
     *  -------------------------------------------------------------------------------------------------- */
    public String existingURL { public get ; private set ; }
    public String newURL { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Redirection stuff
     *  -------------------------------------------------------------------------------------------------- */
    private DepositAccountHelper dah ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public Deposit_Account_Online_Banking ()
    {
        //  -------------------- LOAD DEFAULTS & OVERRIDES --------------------
        dah = new DepositAccountHelper () ;

        existingURL = dah.getOnlineBankingLoginURL () ;

        PageReference pr ;
        if ( dah.sendToDepositApplication () )
        {
            pr = new PageReference ( '/application/deposit_account' ) ;
        }
        else 
        {
            pr = new PageReference ( dah.getAnderaURL () ) ;
            pr.getParameters().put ( 'fiid', dah.getAnderaFIID () ) ;
        }
        
        //  Pass all incoming to outgoing
        for ( String s : ApexPages.currentPage().getParameters().keyset () )
        {
            if ( String.isNotBlank ( ApexPages.currentPage().getParameters().get ( s ) ) )
            {
                pr.getParameters().put ( s, ApexPages.currentPage().getParameters().get ( s ) ) ;
            }
       }
        
        newURL = pr.getUrl () ;
    }
}