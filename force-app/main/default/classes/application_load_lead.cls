public with sharing class application_load_lead 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security ?!@?
     *  ---------------------------------------------------------------------------------------- */
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Incoming stuff for lead
     *  -------------------------------------------------------------------------------------------------- */
    private lead l ;
    private Contact c ;
    private Task t ;
    private String taskRecordTypeId ;
    
    private String fN ;
    private String lN ;
    private String eN ;
    private String pN ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  UTM
     *  -------------------------------------------------------------------------------------------------- */
    private String UTM_Content ;
    private String UTM_Campaign ;
    private String UTM_Medium ;
    private String UTM_Source ;
    private String UTM_Term ;
    private String UTM_VisitorID ;

    /*  --------------------------------------------------------------------------------------------------
     *  Redirection stuff
     *  -------------------------------------------------------------------------------------------------- */
    private DepositAccountHelper dah ;
    private PageReference pr ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Calculation
     *  -------------------------------------------------------------------------------------------------- */
    private boolean isExistingCustomer ;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
	public application_load_lead ()
    {
        //  --------------------- Record Type ---------------------
        if ( String.isBlank ( taskRecordTypeId ) )
            taskRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Task', 'Task' ) ;

        //  -------------------- LOAD DEFAULTS & OVERRIDES --------------------
        dah = new DepositAccountHelper () ;

        //  -------------------- UTM Stuff --------------------
	    UTM_Content = ApexPages.currentPage().getParameters().get('UTM_Content__c') ;
        UTM_Campaign = ApexPages.currentPage().getParameters().get('UTM_Campaign__c') ;
        UTM_Medium = ApexPages.currentPage().getParameters().get('UTM_Medium__c') ;
        UTM_Source = ApexPages.currentPage().getParameters().get('UTM_Source__c') ;
        UTM_Term = ApexPages.currentPage().getParameters().get('UTM_Term__c') ;
        UTM_VisitorID = ApexPages.currentPage().getParameters().get('UTM_VisitorID__c') ;

        //  -------------------- Check existing --------------------
        fN = ApexPages.currentPage().getParameters().get('fN') ;
        lN = ApexPages.currentPage().getParameters().get('lN') ;
        eN = ApexPages.currentPage().getParameters().get('eN') ;
        pN = ApexPages.currentPage().getParameters().get('pN') ;
        
        SObject xo = one_utils.getContactOrLead ( fN, lN, eN ) ;

        if ( xo != null )
        {
            //  Contact
            if ( xo instanceof Contact )
            {
                c = (Contact) xo ;
               	l = null ;
            }
            
            //  Lead
            else if ( xo instanceof Lead )
            {
                l = (Lead) xo ;
                c = null ;
            }
        }
        else
        {
            // OLD!
            l = new Lead () ;
            l.FirstName = fN ;
            l.LastName = lN ;
            l.Email = eN ;
        }
        
        //  -------------------- CONTACT --------------------
        if ( c != null )
        {
            if ( String.isNotBlank ( pN ) )
            {
                c.ProductInterest__c = pN ;
                c.LeadSource = pN ;
                c.LeadSource_Change_Date__c = Date.today () ;
            }
            
            // UTM CRAP
            one_utils.loadUTMValues ( null, c, UTM_Campaign, UTM_Content, UTM_Medium, UTM_Source, UTM_Term, UTM_VisitorID ) ;
            
            //  -------------------- BACKUP Task --------------------
            t = new Task () ;
            t.Product_Interest__c = pN ;
            t.UTM_Content__c = UTM_Content ;
            t.UTM_Campaign__c = UTM_Campaign ;
            t.UTM_Medium__c = UTM_Medium ;
            t.UTM_Source__c = UTM_Source ;
            t.UTM_Term__c = UTM_Term ;
            t.UTM_VisitorID__c = UTM_VisitorID ;
            t.WhoId = c.ID ;
            t.Priority = 'Medium' ;
            t.Status = 'New' ;
            t.RecordTypeId = taskRecordTypeId ;
            
            if ( String.IsBlank ( pN ) )
                t.subject = 'OUB Website UNITY Visa Prospect' ;
            else if ( pN.equalsIgnoreCase ( 'UNITY Visa' ) )
                t.subject = 'OUB Website UNITY Visa Prospect' ;
            else
                t.subject = 'OUB Website Deposit Prospect' ;
                
            t.First_name__c = fN ;
            t.Last_name__c = lN ;
            t.Form_Email__c = eN ;
        }
        
        //  -------------------- LEAD --------------------
        else if ( l != null )
        {
            if ( String.isNotBlank ( pN ) )
            {
                l.ProductSelection__c = pN ;
                l.LeadSource = pN ;
                l.LeadSource_Change_Date__c = Date.today () ;
            }
            
            l.Company = 'Web User Application form' ;
            
            // UTM CRAP
            one_utils.loadUTMValues ( l, null, UTM_Campaign, UTM_Content, UTM_Medium, UTM_Source, UTM_Term, UTM_VisitorID ) ;
        }
        	
		//  Check for exsting customer
        isExistingCustomer = isOnlineBankingCustomer ( eN ) ;

        //  -------------------- WHERE TO SEND THEM --------------------
		pr = null ;

		if ( String.IsBlank ( pN ) )
        {
            //  No incoming field assume UNITY Visa
            pr = new PageReference ( '/application/application_start' ) ;
        }
        else 
		{  
			if ( pN.equalsIgnoreCase ( 'UNITY Visa' ) )
			{
                //  Unity Visa goes to itself
				pr = new PageReference ( '/application/application_start' ) ;
			}
			else
			{
                //  Deposit Application!
                if ( ( isExistingCustomer ) && ( dah.isRedirectExistingCustomer () ) )
                {
					pr = new PageReference ( '/application/online_banking_deposit_account' ) ;
                }
                else
                {
                    if ( dah.sendToDepositApplication () )
                    {
						pr = new PageReference ( '/application/deposit_account' ) ;
/*                        
                        if ( pN.containsIgnoreCase ( 'Checking' ) )
                            pr.getParameters().put ( 'accCodes', 'UEGC' ) ;
                        else if ( pN.containsIgnoreCase ( 'Savings' ) )
                            pr.getParameters().put ( 'accCodes', 'UEGS' ) ;
                        else if ( pN.containsIgnoreCase ( 'Online CD' ) )
                            pr.getParameters().put ( 'accCodes', 'OL1Y' ) ;
*/
                    }
                    else
                    {
                        pr = new PageReference ( dah.getAnderaURL () ) ;
                        pr.getParameters().put ( 'fiid', dah.getAnderaFIID () ) ;
                    }
                }  
			}
		}
        
        if ( pr == null ) // shouldn't happen
            pr = new PageReference ( '/application/deposit_account' ) ;
        
        //  Pass all incoming to outgoing
        for ( String s : ApexPages.currentPage().getParameters().keyset () )
        {
            if ( String.isNotBlank ( ApexPages.currentPage().getParameters().get ( s ) ) )
            {
                if ( ( ! s.equalsIgnoreCase ( 'fN' ) ) &&
                    ( ! s.equalsIgnoreCase ( 'lN' ) ) &&
                    ( ! s.equalsIgnoreCase ( 'eN' ) ) &&
                    ( ! s.equalsIgnoreCase ( 'pN' ) ) && 
                    ( ! s.endsWith ( '__c') ) )
                {
                    System.debug ( 'Adding - "' + s + '" - with - "' + ApexPages.currentPage().getParameters().get ( s ) + '"' ) ;
                    pr.getParameters().put ( s, ApexPages.currentPage().getParameters().get ( s ) ) ;
                }
                else
                {
                    System.debug ( 'Skipping - "' + s + '" - Jim Said so!' ) ;
                }
            }
            else
            {
                System.debug ( 'Ignoring - "' + s + '" - it is blank!' ) ;
            }
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  GO!
     *  -------------------------------------------------------------------------------------------------- */
	public PageReference go ()
	{
        boolean valid = true ;
        
        try
        {
            if ( c != null )
            {
                System.debug ( 'Found Contact - Updating' ) ;
//                update c ;
                access.updateObject ( c ) ;
            }
            else if ( l != null )
            {
                if ( l.ID != null )
                {
                    System.debug ( 'Found Lead - Updating' ) ;
//                    update l ;
	                access.updateObject ( l ) ;
                }
                else if ( l.ID == null )
                {
                    System.debug ( 'New customer Inserting Lead' ) ;
//                    insert l ;
	                access.insertObject ( l ) ;
                }
            }
        }
        catch ( Exception e )
        {
            if ( ( c != null ) && ( c.ID != null ) )
            {
                try
                {
//                	insert t ;
	                access.insertObject ( t ) ;
                }
                catch ( Exception e2 )
                {
                    //  Bad or no data passed in
                    valid = false ;
                }
            }
            else
            {
                //  Bad or no data passed in
                valid = false ;
            }
        }
        
        System.debug ( 'KEVIN SPECIAL!' ) ;
        String ks = fN + '#' + lN + '#' + eN ;
            
        if ( valid )
        {
            if ( c != null )
                ks += '#' + c.ID ;
            else if ( l != null )
                ks += '#' + l.ID ;
        }
        else
        {
            ks += '#XXX' ;
        }
            
        Cookie ksCookie = new Cookie ( 'ks_Cookie', ks, null, -1, false ) ;
        pr.setCookies ( new Cookie [] { ksCookie } ) ;
        
        //  Set UTM
        if ( ( String.isNotBlank ( UTM_Content ) ) && ( UTM_Content != '-' ) )
	        pr.getParameters().put ( 'UTM_Content', UTM_Content ) ;

        if ( ( String.isNotBlank ( UTM_Campaign ) ) && ( UTM_Campaign != '-' ) && ( UTM_Campaign != '(direct)' ) )
	        pr.getParameters().put ( 'UTM_Campaign', UTM_Campaign ) ;
        
        if ( ( String.isNotBlank ( UTM_Medium ) ) && ( UTM_Medium != '-' ) && ( UTM_Medium != '(none)' ) )
	        pr.getParameters().put ( 'UTM_Medium', UTM_Medium ) ;

        if ( ( String.isNotBlank ( UTM_Source ) ) && ( UTM_Source != '-' ) && ( UTM_Source != '(direct)' ) )
	        pr.getParameters().put ( 'UTM_Source', UTM_Source ) ;

        if ( ( String.isNotBlank ( UTM_Term ) ) && ( UTM_Term != '-' ) )
	        pr.getParameters().put ( 'UTM_Term', UTM_Term ) ;

        if ( ( String.isNotBlank ( UTM_VisitorID ) ) && ( UTM_VisitorID != '-' ) )
	        pr.getParameters().put ( 'UTM_VisitorID', UTM_VisitorID ) ;
        
        //  Always return a page reference to forward to the application
		return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Current customer check
     *  -------------------------------------------------------------------------------------------------- */
    private boolean isOnlineBankingCustomer ( String email )
    {
        boolean customer = false ;
        
        list<Service__c> serviceL = null ;
        
        try
        {
            serviceL = [
                SELECT ID, Contact__r.Email, Owner_Contact__r.Email
                FROM Service__c
                WHERE Type__c = 'Online Banking'
                AND Status__c = 'Active'
                AND Contact__r.Email = :email 
            ] ;
        }
        catch ( DMLException e )
        {
            // .... do nothing
        }
        
        if ( ( serviceL != null ) && ( serviceL.size () > 0 ) )
            customer = true ;
        
        return customer ;
    }
}