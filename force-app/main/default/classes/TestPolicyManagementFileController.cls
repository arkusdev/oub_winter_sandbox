@isTest
public with sharing class TestPolicyManagementFileController 
{
	private static User getUser ()
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test1' ;
		u.lastName = 'User1' ;
		u.email = 'test1@user.com' ;
		u.username = 'test1user1@user.com' ;
		u.alias = 'tuser1' ;
		u.CommunityNickname = 'tuser1' ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		insert u ;
		
		return u ;
	}
	
	private static PermissionSet getPermissionSet ( String name )
	{
		PermissionSet ps = [
	        select Id
	        from PermissionSet
	        where Name = :name
	    ];
	    
		return ps ;
	}
	
	private static PermissionSetAssignment setPermissionSetAssignment ( PermissionSet ps, User u )
	{
		PermissionSetAssignment psa = new PermissionSetAssignment () ;
		psa.AssigneeId = u.Id ;
		psa.PermissionSetId = ps.Id ;
		
		insert psa ;
		
		return psa ;
	}
	
	private static User getUserWithPermissionSet ( String name )
	{
		PermissionSet ps = getPermissionSet ( name ) ;

		User u = getUser () ;
		
		PermissionSetAssignment psa = setPermissionSetAssignment ( ps, u ) ;
		
		return u ;
	}
	
	private static User getUserWithPermissionSetNEW ( String name )
	{
		PermissionSet ps = new PermissionSet () ;
		ps.Name = name ;
		ps.Label = 'Test' ;
		insert ps ;
		
		User u = getUser () ;

		PermissionSetAssignment psa = setPermissionSetAssignment ( ps, u ) ;
		
		return u ;
	}
	
	static testmethod void testOne_BLANK ()
	{
		User u1 ;
		Policy_Management__c pm ;

		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()] ;
		System.runAs (thisUser)
		{
			//  --------------------------------------------------
			//  User & permissions
			//  --------------------------------------------------
			u1 = getUserWithPermissionSetNEW ( 'PM_TEST_BLANK' ) ;
			
			//  --------------------------------------------------
			//  Object Setup
			//  --------------------------------------------------
			pm = new Policy_Management__c () ;
			pm.Name = 'Test Policy' ;
			insert pm ;
		}
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.Policy_Management_Files ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		System.runAs ( u1 )
		{
			//  Create standard controller object from the application
			ApexPages.StandardController sc = new ApexPages.StandardController ( pm ) ;
	        
			//  Feed the standard controller to the page controller
			PolicyManagementFileController pfc = new PolicyManagementFileController ( sc ) ;
			
			System.assertEquals ( pfc.mode, 500 ) ;
		}
				
		Test.stopTest() ;
	}
	
	static testmethod void testTwo_ADMIN ()
	{
		User u1 ;
		Policy_Management__c pm ;

		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()] ;
		System.runAs (thisUser)
		{
			//  --------------------------------------------------
			//  User & permissions
			//  --------------------------------------------------
			u1 = getUserWithPermissionSetNEW ( 'PM_TEST_ADMIN' ) ;
			
			//  --------------------------------------------------
			//  Object Setup
			//  --------------------------------------------------
			pm = new Policy_Management__c () ;
			pm.Name = 'Test Policy' ;
			pm.Policy_ID__c = 123 ;
			insert pm ;
		}
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.Policy_Management_Files ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		System.runAs ( u1 )
		{
			
			//  Create standard controller object from the application
			ApexPages.StandardController sc = new ApexPages.StandardController ( pm ) ;
	        
			//  Feed the standard controller to the page controller
			PolicyManagementFileController pfc = new PolicyManagementFileController ( sc ) ;
			
			System.assertEquals ( pfc.mode, 0 ) ;
			System.assertEquals ( pfc.isAdministrator, 'Y' ) ;
		}
		
		Test.stopTest() ;
	}
	
	static testmethod void testThree_NORMAL ()
	{
		User u1 ;
		Policy_Management__c pm ;

		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()] ;
		System.runAs (thisUser)
		{
			//  --------------------------------------------------
			//  User & permissions
			//  --------------------------------------------------
			u1 = getUserWithPermissionSetNEW ( 'PM_TEST_NORMAL' ) ;
			
			//  --------------------------------------------------
			//  Object Setup
			//  --------------------------------------------------
			pm = new Policy_Management__c () ;
			pm.Name = 'Test Policy' ;
			pm.Policy_ID__c = 123 ;
			insert pm ;
		}
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.Policy_Management_Files ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest() ;
		
		System.runAs ( u1 )
		{
			
			//  Create standard controller object from the application
			ApexPages.StandardController sc = new ApexPages.StandardController ( pm ) ;
	        
			//  Feed the standard controller to the page controller
			PolicyManagementFileController pfc = new PolicyManagementFileController ( sc ) ;
			
			System.assertEquals ( pfc.mode, 0 ) ;
			System.assertEquals ( pfc.isAdministrator, 'N' ) ;
		}
		
		Test.stopTest() ;
	}
}