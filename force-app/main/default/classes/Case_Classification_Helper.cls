public class Case_Classification_Helper 
{
    //  ----------------------------------------------------------------------------------------
    //  Constants
    //  ----------------------------------------------------------------------------------------
    private static final String DEFAULT_CASE_CLASSIFICATION = 'Default' ;

    //  ----------------------------------------------------------------------------------------
    //  Internals
    //  ----------------------------------------------------------------------------------------
	private static list<SelectOption> caseClassificationList ;
    private static map<String,Case_Topic__c> ccMDT ;
    
    //  ----------------------------------------------------------------------------------------
    //  Getters
    //  ----------------------------------------------------------------------------------------
    public static list<SelectOption> getCaseClassificationList ()
    {
        if ( caseClassificationList == null )
            loadCaseClassificationMDT () ;
        
        return caseClassificationList ;
    }

    public static Case_Topic__c getClassificationInfo ( String classification )
    {
        System.debug ( 'Searching for :: ' + classification ) ;
        Case_Topic__c cc = null ;
        
        if ( ccMDT == null )
            loadCaseClassificationMDT () ;
        
        if ( ccMDT != null )
        {
            if ( ccMDT.containsKey ( classification ) )
                cc = ccMDT.get ( classification ) ;
            else
                cc = ccMDT.get ( DEFAULT_CASE_CLASSIFICATION ) ;
        }
        
        return cc ;
    }
    
    //  ----------------------------------------------------------------------------------------
    //  Constructor
    //  ----------------------------------------------------------------------------------------
    public Case_Classification_Helper ()
    {
        // EMPTY
    }
    
    //  ----------------------------------------------------------------------------------------
    //  loading
    //  ----------------------------------------------------------------------------------------
	private static void loadCaseClassificationMDT ()
    {
        if ( ( ccMDT == null ) || ( caseClassificationList == null ) )
        {
            list<Case_Topic__c> ccL = null ;
            
            try
            {
                ccL = [
                    SELECT Name, Status__c, Message__c, Classification__c
                    FROM Case_Topic__c
                    WHERE Active__c = true
                    ORDER BY Order_By__c
                ] ;
            }
            catch ( DMLException e )
            {
                ccL = null ;
            }
            
            if ( ( ccL != null ) && ( ccL.size () > 0 ) )
            {
                ccMDT = new map<String,Case_Topic__c> () ;
                caseClassificationList = new list<SelectOption> () ;
                caseClassificationList.add ( new SelectOption ( '', '-- Please choose your main topic HERE --' ) ) ;
                
                for ( Case_Topic__c cc : ccL )
                {
                    ccMDt.put ( cc.Classification__c, cc ) ;
                    caseClassificationList.add ( new SelectOption ( cc.Classification__c, cc.Name ) ) ;
                }
            }
        }
    }
    
    //  ----------------------------------------------------------------------------------------
    //  get 'em contact?!?
    //  ----------------------------------------------------------------------------------------
    public static ID getContact ( String firstName, String lastName, String Email )
    {
        ID xID = null ;
        
        String lookup = firstName + '-' + lastName + '-' + email ;
        
        try
        {
            list<Contact> cL = [
                SELECT ID
				FROM Contact
                WHERE Name_Email_Lookup__c = :lookup
                ORDER BY ID
            ] ;
            
            if ( ( cL != null ) && ( cL.size () > 0 ) )
                xID = cL [ 0 ]. ID ;
        }
        catch ( DMLException e )
        {
            xID = null ;
        }
        
        return xID ;
    }
}