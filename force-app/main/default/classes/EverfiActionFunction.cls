global class EverfiActionFunction 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='Everfi Check for Completed Playlists' )
	global static List<EverfiCompletePlaylistResult> generateCompletedPlaylists ( List<EverfiCompletePlaylistRequest> requests ) 
	{
		//  Not used?
		List<EverfiCompletePlaylistResult> rL = new List<EverfiCompletePlaylistResult> () ;
        
        //  Outbound!
        list<Completed_Playlist__c> cpL = new list<Completed_Playlist__c> () ;
        
        // Convert!
        map<ID,boolean> idMap = new Map<ID,boolean> () ;
        
        for ( EverfiCompletePlaylistRequest r : requests )
        {
            if ( ! idMap.containsKey ( r.everFiProfileID ) )
                idMap.put ( r.everFiProfileID, true ) ;
        }
        
        map<String,boolean> ctMap = getFULLCompletedThingyList () ;
        map<ID,List<Module__c>> mpm = getModulesByProfile ( idMap.keySet () ) ;
        
        if ( ( mpm != null ) && ( mpm.size () > 0 ) )
        {
            Map<ID,Map<ID,boolean>> xpm = getPlaylistMasterMap () ;
            
            for ( ID profileID : mpm.keySet () )
            {
                Map<ID,Map<ID,boolean>> ypm = mapKlone ( xpm ) ;
                
                List<Module__c> mL = mpm.get ( profileID ) ;
                
                for ( Module__c m : mL )
                {
                    for ( ID playlistID : ypm.keySet () )
                    {
                        if ( ypm.get ( playlistID ).containsKey ( m.Everfi_Module__c ) )
                        {
                            ypm.get ( playlistID ).remove ( m.Everfi_Module__c ) ;
                        }
                    }
                }
                
                for ( ID playlistID : ypm.keySet () )
                {
                    if ( ypm.get ( playlistID ).size () == 0 )
                    {
                        if ( ! ctMap.containsKey ( getKey ( profileID, playlistID ) ) )
                        {
                            Completed_Playlist__c cp = new Completed_Playlist__c () ;
                            cp.Completed_Date__c = System.now () ;
                            cp.Everfi_Playlist__c = playlistID ;
                            cp.Everfi_Profile__c = profileID ;
                            
                            cpL.add ( cp ) ;
                        }
                    }
                }
            }
        }
        
        if ( ( cpL != null ) && ( cpL.size () > 0 ) )
        {
            try
            {
                insert cpL ;
            }
            catch ( Exception e )
            {
                // ?
            }
        }
        
		return rL ;
    }

	/*  --------------------------------------------------------------------------------------------------
	 *  Master playlist crap
	 *  -------------------------------------------------------------------------------------------------- */	
    public static Map<ID,Map<ID,boolean>> getPlaylistMasterMap ()
    {
        Map<ID,Map<ID,boolean>> epM = new Map<ID,Map<ID,boolean>> () ;
        
        list<Everfi_Playlist_Module__c> epmL = null ;
        
        try
        {
            epmL = [
                SELECT Everfi_Module__c, Everfi_Playlist__c
                FROM Everfi_Playlist_Module__c
            ] ;
        }
        catch ( Exception e )
        {
            epM = null ;
        }
        
        if ( ( epmL != null ) && ( epmL.size () > 0 ) )
        {
            for ( Everfi_Playlist_Module__c e : epmL )
            {
                if ( epM.containsKey ( e.Everfi_Playlist__c ) )
                {
                    epM.get ( e.Everfi_Playlist__c ).put ( e.Everfi_Module__c, true ) ;
                }
                else
                {
                    map<ID,boolean> xM = new map<ID,boolean> () ;
                    xM.put ( e.Everfi_Module__c, true ) ;
                    epM.put ( e.Everfi_Playlist__c, xM ) ;
                }
            }
       }
        
        return epM ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Persons and the thingys they completed
	 *  -------------------------------------------------------------------------------------------------- */	
    private static map<ID,List<Module__c>> getModulesByProfile ( Set<ID> idS )
    {
        map<ID,List<Module__c>> mpm = new map <ID,List<Module__c>> () ;
        
        list<Module__c> mL = null ;
        
        try
        {
            mL = [
                SELECT Everfi_Module__c, Everfi_Profile__c
                FROM Module__c
                WHERE Course_Completed_Date__c <> NULL
                AND Everfi_Profile__c IN :idS
            ] ;
        }
        catch ( Exception e )
        {
            mL = null ;
            mpm = null ;
        }
        
        if ( ( mL != null ) && ( mL.size () > 0 ) )
        {
            for ( Module__c m : mL )
            {
                if ( mpm.containsKey ( m.Everfi_Profile__c ) )
                {
                    mpm.get ( m.Everfi_Profile__c ).add ( m ) ;
                }
                else
                {
                    list<Module__c> xL = new list<Module__c> () ;
                    xL.add ( m ) ;
                    mpm.put ( m.Everfi_Profile__c, xL ) ;
                }
            }
        }
        
        return mpm ;
    }
        
	/*  --------------------------------------------------------------------------------------------------
	 *  Klone!
	 *  -------------------------------------------------------------------------------------------------- */	
    private static Map<ID,Map<ID,boolean>> mapKlone ( Map<ID,Map<ID,boolean>> iMap )
    {
        Map<ID,Map<ID,boolean>> xMap = new Map<ID,Map<ID,boolean>> () ;
        
        for ( ID l : iMap.keySet () )
        {
			xMap.put ( l, iMap.get ( l ).clone () ) ;
        }
        
        return xMap ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  All prior completed thingys
	 *  -------------------------------------------------------------------------------------------------- */	
    private static map<String,boolean> getFULLCompletedThingyList ()
    {
        list<Completed_Playlist__c> cpL = null ;
        map<String,boolean> ctMap = new map<String,boolean> () ;
        
        try
        {
            cpL = [
                SELECT Everfi_Playlist__c, Everfi_Profile__c
                FROM Completed_Playlist__c
            ] ;
        }
        catch ( Exception e )
        {
            ctMap = null ;
        }
        
        if ( ( cpL != null ) && ( cpl.size () > 0 ) )
        {
            for ( Completed_Playlist__c cp : cPL )
            {
                String key = getkey ( cp.Everfi_Profile__c, cp.Everfi_Playlist__c ) ;
                if ( ! ctMap.containsKey ( key ) )
                {
                    ctMap.put ( key, true ) ;
                }
            }
        }
        
        return ctMap ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Key
	 *  -------------------------------------------------------------------------------------------------- */	
    private static String getkey ( String s1, String s2 )
    {
        return s1 + '--' + s2 ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class EverfiCompletePlaylistRequest 
	{
	    @InvocableVariable ( label='Everfi Profile ID' required=true )
	    public ID everFiProfileID ;
    }

	global class EverfiCompletePlaylistResult
	{
	    @InvocableVariable
	    public ID everFiProfileID ;
        public ID completedPlaylistID ;
	}
}