public with sharing class Deposit_Application_Additional_Info 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public Deposit_Application__c d 
 	{ 
 		get; 
 		private set; 
 	}
 	
 	//  QR Code to allow site transfer to mobile device
 	public String qrCode 
 	{
 		get; 
 		private set; 
 	}
 	
 	public String appField
 	{
 		get;
 		private set;
 	}
 	
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
 
	//  Step control for panels on page
	public Integer flStepNumber
	{
		get ;
		private set ;
	}

	//  Additional language based on decision code
    public String codeAdditionalLanguage
    {
        get ;
		private set ;
    }
    
	//  List for document type drop down
    public List<SelectOption> docTypeItemDropDown
    {
        get
        {
	        return Deposit_Application_Utils.docTypeItems ;
        }
        
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
    // Count of needed documents
    public Integer docTypeListSize
    {
    	get
    	{
    		Integer dtl = 0 ;
    		
    		if ( docTypeList != null )
    			dtl = docTypeList.size () ;
    			
    		return dtl ;
    	}
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Live Agent
     *  ---------------------------------------------------------------------------------------- */
    public String liveAgentButton
    {
    	get ; 
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }

    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public Deposit_Application_Additional_Info ( ApexPages.Standardcontroller stdC )
	{
		//  --------------------------------------------------------------------------------------------------
		//  Live Agent Button
		//  --------------------------------------------------------------------------------------------------
		liveAgentButton = null ;  // OFF
		
		//  Controls which panel we display, default of 0
		flStepNumber = 0 ;
		
        //  First check for application in session
        d = (Deposit_Application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
 
 		//  If no session present, use querystring instead
        if ( ( d == null ) || ( d.ID == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
			
			//  Reset, just in case
			d = null ;

			//  Both need to be passed in
			if ( ( aid != null ) && ( kid != null ) )
			{
				List<Deposit_Application__c> dL = null ;
				
				try
				{
					dL = [
						SELECT ID, External_Key__c, Application_Status__c,
						Application_Processing_Status__c, Additional_Information_Required__c, 
						Current_Status__c, Confirmation_Number__c
						FROM Deposit_Application__c
						WHERE ID = :aid
						AND External_Key__c = :kid
						ORDER BY ID
					] ;
				}
				catch ( DmlException e )
				{
					//  Defaults are already set
					for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
					{
				        System.debug ( e.getDmlMessage ( i ) ) ;
					}				
			   }
				
				if ( ( dL != null ) && ( dL.size () > 0 ) )
				{
					//  Grab the first one
					d = dL [ 0 ] ;
				}
				else
				{
		            System.debug ( '========== No Data found for ID & Key ==========' ) ;
                    
                    list<one_application__c> oL = [
                        SELECT ID, Upload_Attachment_Key__c
                        FROM one_application__c
                        WHERE ID = :aid
                        AND Upload_Attachment_key__c = :kid
                    ] ;
                    
                    if ( ( oL != null ) && ( oL.size () > 0 ) )
                    {
			            System.debug ( '========== Found other application, redirecting? ==========' ) ;
                        flStepNumber = 9999 ;
                    }
				}
			}
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = d.Id ;
			kid = d.External_Key__c ;
		}
		
		//  Check again for things existing
		if ( ( d != null ) && ( aid != null ) && ( kid != null ) )
		{
            System.debug ( '========== Valid application found ==========' ) ;
            
			//  Set up QR Code, need the full URL
			qrCode = Deposit_Application_Utils.getDocUploadQRCode ( aid, kid, 125, 125 ) ;
			
			//  Application ID for page
			appField = null ;
			if ( String.isNotBlank ( d.Confirmation_Number__c ) )
				appField = d.Confirmation_Number__c ;
				
			//  Get the additional language needed
			codeAdditionalLanguage = Deposit_Application_Utils.codesAdditionalLanguage.get ( d.Application_Status__c ) ;
			
			//  Get the list of documents to display (copy since we'll be modifying it)
			docTypeList = Deposit_Application_Utils.getAdditionalInfoNeeded ( d.Additional_Information_Required__c ) ;
			
			if ( docTypeList != null )
			{
				//  Load attachments separately
				Attachment[] aL = null ;
				
				try
				{
					aL = [
						SELECT Name 
						FROM Attachment 
						WHERE ParentID = :aid 
						] ;
				}
				catch ( Exception e )
				{
					System.debug ( e.getMessage () ) ;
				}
				
				//  Check the attachments (if any!)
				if ( aL != null )
				{
					for ( Attachment oA : aL )
					{
						System.debug ( 'Found attachement! - ' + oA.name ) ;
						
						Integer j = 0 ;
						while ( j < docTypeList.size () )
						{
							if ( docTypeList [j].equalsIgnoreCase ( oA.name ) )
								docTypeList.remove ( j ) ;
							j++ ;
						}
					}
				}
				
				//  Set the step number
				flStepNumber = 1000 ;
			}
			else
			{
				System.debug ( 'No documents found for this code! :: "' + d.Additional_Information_Required__c + '" ?' ) ;
			}
		}
	}
    
    /*  ----------------------------------------------------------------------------------------
     *  Validate & Upload the file
     *  ---------------------------------------------------------------------------------------- */
	public PageReference upload () 
	{
		boolean isValid = true ;
		errorName = false ;
		errorFile = false ;
		
		//  Start with validation
		if ( String.isBlank ( attachment.name ) )
		{
			errorName = true ;
			isValid = false ;
		}
		
		if ( attachment.body == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        flStepNumber = 1002 ;
			return null ;
		}

		attachment.OwnerId = UserInfo.getUserId () ;
		attachment.ParentId = d.Id ;    // the record the file is attached to
		attachment.IsPrivate = false ;  // Or the guest user can't query it!

		try 
		{
	        System.debug ( '========== Inserting Attachment ==========' ) ;
			insert attachment ;

	        System.debug ( '========== Flipping application status ==========' ) ;
	        d.Application_Processing_Status__c = 'Additional Documentation Received' ;
	        d.Additional_Information_Required__c = removeFoundDoc ( d.Additional_Information_Required__c, attachment.name ) ;

	        update d ;
		}
		catch (DMLException e)
		{
			System.debug ( '========== Error Inserting! ==========' ) ;

			return null;
		}
		finally 
		{
	        System.debug ( '========== Final stuff ==========' ) ;
	        
	        //  Set panel to success upload
			flStepNumber = 1001 ;
			
			//  Remove document from list
			Integer j = 0 ;
			while ( j < docTypeList.size () )
			{
				System.debug ( '-- ' + docTypeList [j] + ' --> ' + attachment.name + ' ?? ' + Deposit_Application_Utils.translateAdditionalInfoShortToLong ( attachment.name ) ) ;
				
				if ( docTypeList [j].equalsIgnoreCase ( Deposit_Application_Utils.translateAdditionalInfoShortToLong ( attachment.name ) ) )
					docTypeList.remove ( j ) ;
				j++ ;
			}
			
			//  If all documents are gone, go to overall success
			if ( docTypeList.size () == 0 )
				flStepNumber = 2000 ;
			
			//  Clear errors if any
			errorName = false ;
    		errorFile = false ;
			
			//  Reset attachment
			attachment = new Attachment () ; 
		}

		return null;
	}
	
	private String removeFoundDoc ( String docList, String doc )
	{
		String returnList = '' ;
		
		list<String> dL = docList.split ( ';' ) ;
		list<String> xL = new list<String> () ;
		
		for ( String s : dL )
		{
			if ( ! s.equalsIgnoreCase ( doc ) )
				xL.add ( s ) ;
		}
		
		boolean start = true ;
		for ( String s : xL )
		{
			if ( start )
				start = false ;
			else
				returnList = returnList + ';' ;
				
			returnList = returnList + s ;
		}
		
		return returnList ;
	}
    
    public PageReference redirect ()
    {
		String aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
		String kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
        String url = 'https://' + site.getDomain () + '/application/application_file_upload' ; //  Can't reference cross site, have to contruct it manually

        System.debug ( url ) ;
        PageReference pr = new PageReference ( url ) ;
        pr.getParameters().put ( 'aid', aid ) ;
        pr.getParameters().put ( 'kid', kid ) ;
        pr.setRedirect ( true ) ;
        
        return pr ;
    }
}