@isTest
public class testBBPdOptInController {
    
    public static void setCustomSettings() {
        DI_Settings__c dis = new DI_Settings__c () ;
        
        dis.Name = 'settings';
        dis.Client_ID__c = 'abc123' ;
        dis.Client_Secret__c = 'youandme' ;
        dis.Login_URL__c = 'https://diapis.digitalinsight.com/v1/oauth/token' ;
        dis.DI_FiId__c = '12345' ;
        dis.Register_URL__c = 'https://diapis.digitalinsight.com/registration/v4/fis/' + dis.DI_FiId__c + '/fiCustomers';
        dis.CustomerInfo_URL__c = 'https://diapis.digitalinsight.com/bankingservices/v2/fis/' + dis.DI_FiId__c + '/fiCustomers/';
        dis.Subscriptions_URL__c = 'https://diapis.digitalinsight.com/subscriptions/v1/fis/' + dis.DI_FiId__c + '/fiCustomers/';
        dis.Events_URL__c = 'https://diapis.digitalinsight.com/subscriptions/v1/fis/' + dis.DI_FiId__c + '/fiCustomers/';
        dis.Destinations_URL__c = 'https://diapis.digitalinsight.com/destinations/v2/fis/' + dis.DI_FiId__c + '/products/IB/notificationApps/MBL/fiCustomers/'; // assuming IB = product_id and MBL = notification_app_id for all customers
        dis.Last_Timestamp__c = null;
        
        insert dis ;
    }
    
    public static User setupUser() {
        Profile testProfile = [SELECT Id 
                           FROM profile
                           WHERE Name = 'OneUnited Advocate Identity User' 
                           LIMIT 1];
        
        Account a = new Account(Name='Test Account');
        insert a;
        
		Contact c = new Contact();
        c.Email = 'test1@ouuser.com';
        c.LastName = 'UserAdvocateProgramDisplayController';
        c.AccountId = a.Id;
        c.TaxID__c = '111223333';
        insert c;
        
    	User testUser = new User(
            FirstName = 'Test1',
            LastName = 'UserAdvocateProgramDisplayController',
            Email = 'test1@ouuser.com',
            Username = 'test1user1@ouuser.com',
            Alias = 'dsplctrl',
            CommunityNickname = 'tuser1',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = testProfile.id,
            LanguageLocaleKey = 'en_US',
            IsActive=true,
            ContactId=c.Id ); 
        
        return testUser;
    }
    
    @isTest static void testGetAllAcctInfo() {
        setCustomSettings();
        Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
        
        User testUser = setupUser();
        
        /*
        DIHelper.account a = new DIHelper.account( '123', 'abc', '123456', '', 'checking', 'primary', 'open', '*456' );
        diFaList.add(a);
        a = new DIHelper.account( '234', 'abc', '234567', '', 'savings', 'primary', 'open', '*567' );
        diFaList.add(a);
		*/
        List<Financial_Account__c> faList = new List<Financial_Account__c>();
        
        Financial_Account__c f = new Financial_Account__c();
        f.TAXRPTFORPERSNBR__c = testUser.contactId;
       	f.ACCTNBR__c = '123456';
        f.MJACCTTYPCD__c = 'CK';
        faList.add(f);
        f = new Financial_Account__c();
        f.ACCTNBR__c = '234567';
        f.MJACCTTYPCD__c = 'CK';
        faList.add(f);
        insert faList;
        
        Financial_Account_Contact_Role__c facr = new Financial_Account_Contact_Role__c();
        facr.External_ID__c = 'lalala';
        facr.Contact__c = testUser.ContactId;
        facr.Financial_Account__c = f.Id;
        facr.Financial_Account_Role__c = 'nontax signator';
        insert facr;
        
        List<Payday_Response__c> pdrList = new List<Payday_Response__c>();
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        pdr.OptIn_Status__c = 'Active';
        pdr.Contact__c = testUser.ContactId;
        pdr.Financial_Account__c = f.Id;
        pdr.Response_Date__c = Datetime.now();
        pdrList.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        pdr.OptIn_Status__c = 'Active';
        pdr.Comms_Channels__c = 'Email only';
        pdr.Contact__c = testUser.ContactId;
        pdr.Financial_Account__c = f.Id;
        pdr.Response_Date__c = Datetime.now();
        pdrList.add(pdr);
        insert pdrList;

        test.startTest();
        
        System.runAs(testUser)
        {
            
            String results = BankBlackPaydayOptInController.getAllAcctInfo('checking');
            System.debug('testGetAllAcctInfo --- results ---');
            System.debug(results);
            
            System.assertEquals( true, ( results != null ) );
            
            // results = BankBlackPaydayOptInController.getAllAcctInfo('savings');
        }
        
        test.stopTest();
    }
    
    // No associated SF FA records
    @isTest static void testGetAllAcctInfo2() {
        setCustomSettings();
        Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
        
        User testUser = setupUser();

        test.startTest();
        
        System.runAs(testUser)
        {
            
            String results = BankBlackPaydayOptInController.getAllAcctInfo('checking');
            System.debug('testGetAllAcctInfo --- results ---');
            System.debug(results);
            
            System.assertEquals( true, ( results != null ) );
            
            // results = BankBlackPaydayOptInController.getAllAcctInfo('savings');
        }
        
        test.stopTest();
    }
    
    @isTest static void testGetSFFiAccounts() {
        User testUser = setupUser();
        
        Financial_Account__c fa = new Financial_Account__c();
        fa.TAXRPTFORPERSNBR__c = testUser.contactId;
        fa.MJACCTTYPCD__c = 'CK';
        insert fa;
        
        Financial_Account__c fa2 = new Financial_Account__c();
        fa2.MJACCTTYPCD__c = 'CK';
        insert fa2;
        
        Financial_Account_Contact_Role__c facr = new Financial_Account_Contact_Role__c();
        facr.Contact__c = testUser.contactId;
        facr.Financial_Account_Role__c = 'NonTax Signator';
        facr.Financial_Account__c = fa2.Id;
        facr.External_ID__c = '1234567890';
        insert facr;
        
        test.startTest();
        
        System.runAs(testUser)
        {
            List<Financial_Account__c> faList = BankBlackPaydayOptInController.getSFFiAccounts('CK');
            System.assertEquals( 2, faList.size() );
            
            faList = BankBlackPaydayOptInController.getSFFiAccounts('SAV');
            System.assertEquals( 0, faList.size() );
        }
        
        test.stopTest();
    }
    
    @isTest static void testGetDIFiAccounts() {
        setCustomSettings();
        Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
        
        User testUser = setupUser();
        
        test.startTest();
        
        System.runAs(testUser)
        {
            List<DIHelper.account> accounts = BankBlackPaydayOptInController.getDIFiAccounts('checking');
            System.assertEquals( 1, accounts.size() );
            
            accounts = BankBlackPaydayOptInController.getDIFiAccounts('savings');
            System.assertEquals( 1, accounts.size() );
        }
        
        test.stopTest();
    }
    
    @isTest static void testGetCurrentUserContactId() {
        // Setup user
        User testUser = setupUser();
        
        test.startTest();
        
        String contactId = BankBlackPaydayOptInController.getCurrentUserContactId();
        
        System.assertEquals( contactId, null );
        
        System.runAs(testUser)
        {
            contactId = BankBlackPaydayOptInController.getCurrentUserContactId();
            System.assertEquals( contactId, testUser.contactId );
        }
        
        test.stopTest();
    }
    
    @isTest static void testGetCurrentContact() {
        // Setup user
        User testUser = setupUser();
        
        test.startTest();
        
        Contact c2 = BankBlackPaydayOptInController.getCurrentContact();
        System.assertEquals( c2, null );
        
        System.runAs(testUser)
        {
        	Contact c3 = BankBlackPaydayOptInController.getCurrentContact();
            System.assertEquals( c3.Id, testUser.ContactId );
        }
        
        test.stopTest();   
    }
    
    @isTest static void testGetCheckValue() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        fa.BankBlack_Payday_OptIn__c = 'Inactive';
        insert fa;
        
        test.startTest();
        
        boolean testNull = BankBlackPaydayOptInController.getCheckValue(fa.Id);
        System.assertEquals( testNull, null );
        
        List<Payday_Response__c> pdrs  = new List<Payday_Response__c>();
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Pending';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Pending';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'no notifications';
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Pending';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        insert pdrs;
        
        boolean testTrue = BankBlackPaydayOptInController.getCheckValue(fa.Id);
        System.assertEquals( testTrue, true );
        
        test.stopTest();
    }
    
    @isTest static void testGetCommsValue() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        fa.BankBlack_Payday_OptIn__c = 'Inactive';
        fa.BankBlack_Payday_Comms_OptIn__c = 'Email only';
        insert fa;
        
        test.startTest();
        
        String testNull = BankBlackPaydayOptInController.getCommsValue(fa.Id);
        System.assertEquals( testNull, null );
        
        List<Payday_Response__c> pdrs  = new List<Payday_Response__c>();
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'Push notification';
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'no notifications';
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'Push notification';
        pdrs.add(pdr);
        insert pdrs;
        
        String testBoth = BankBlackPaydayOptInController.getCommsValue(fa.Id);
        System.assertEquals( testBoth, 'Push notification' );
        
        test.stopTest();
    }
    
    @isTest static void testGetPaydayResponses() {
        Financial_Account__c fa = new Financial_Account__c();
        fa.ACCTNBR__c = '1234567890123456';
        insert fa;
        
        List<Payday_Response__c> pdrs  = new List<Payday_Response__c>();
        String rti = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId;
        
        Payday_Response__c pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdr.Comms_Channels__c = 'No notifications';
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = rti;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now();
        pdrs.add(pdr);
        pdr = new Payday_Response__c();
        pdr.RecordTypeId = Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId;
        pdr.Financial_Account__c = fa.Id;
        pdr.OptIn_Status__c = 'Active';
        pdr.Response_Date__c = Datetime.now().addDays(-1);
        pdrs.add(pdr);
        insert pdrs;
        
        test.startTest();
        
        System.assertEquals( 3, BankBlackPaydayOptInController.getPaydayResponses( fa.Id, rti ).size() );
        System.assertEquals( 1, BankBlackPaydayOptInController.getPaydayResponses( fa.Id, Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId ).size() );
        
        test.stopTest();
    }
    
}