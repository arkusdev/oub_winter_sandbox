global class TaskSurveyUpdate 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='Update Task with Survey' )
	global static List<TaskResult> updateTask ( List<TaskRequest> requests ) 
	{
		//  Not used?
		List<TaskResult> trL = new List<TaskResult> () ;
        
        //  List o List
        Map<String, String> tMap = new Map<String, String> () ;
        
		//  Load the list
		for ( TaskRequest tr : requests )
            tMap.put ( tr.taskID, tr.surveyID ) ;
        
        //  Grabbo
        try
        {
            List<Task> tL = [
                SELECT ID
                FROM Task
                WHERE ID IN :tMap.keySet ()
            ] ;
            
            if ( ( tL != null ) && ( tL.size() > 0 ) )
            {
            	for ( Task t : tL )
                {
                    t.Survey_Response__c = tMap.get ( t.ID ) ;
                }
            }
            
            update tL ;
        }
        catch ( Exception e )
        {
            // ?
        }
        
        return trL ;
    }
        
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class TaskRequest 
	{
	    @InvocableVariable ( label='Task ID' required=true )
	    public String taskID ;
        
	    @InvocableVariable ( label='Survey Response ID' required=true )
	    public String surveyID ;
    }
    
	global class TaskResult 
	{
	    @InvocableVariable
	    public String taskID ;
	}
}