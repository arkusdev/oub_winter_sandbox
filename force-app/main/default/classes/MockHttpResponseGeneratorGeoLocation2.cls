@isTest
global class MockHttpResponseGeneratorGeoLocation2 implements HttpCalloutMock 
{
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        res.setHeader ( 'Content-Type', 'application/json' ) ;
        
        String body = '' ;
        body += '{' ;
        body += '"geometry" : {' ;
        body += '"location": {' ;
        body += '"NYET" : 1 ' ;
        body += '}' ;
        body += '}' ;
        body += '}' ;
        body += '' ;
        
        res.setBody ( body ) ;
        res.setStatusCode ( 200 ) ;
        return res ;
    }
}