@isTest
public with sharing class test_insight_passthrough 
{
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		
		insert b ;
		
		return b ;
	}
	
	private static Contact getC ( String taxID )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'test98@user98.com' ;
		c.TaxID__c = taxID ;
		
		insert c ;
		
		return c ;
	}
	
	private static Account getA ( String taxID )
	{
		Account a = new Account () ;
		a.Name = 'Test Account' ;
		a.FederalTaxID__c = taxID ;
		
		insert a ;
		return a ;
		
	}
	
	private static Financial_Account__c getFA ( Contact c, Branch__c b )
	{
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c.Id ;
		fa.CREDITLIMITAMT__c = 1000.00 ;
		fa.ACCTNBR__c = '123456789' ;
		fa.BRANCHORGNBR__c = b.ID ;
		
		insert fa ;
		
		return fa ;
	}
	
	private static Financial_Account__c getFA ( Account a, Branch__c b )
	{
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'SAV' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'ITY Holiday Club Savings' ;
		fa.NOTEBAL__c = 10000 ;
		fa.TAXRPTFORORGNBR__c = a.Id ;
		fa.CREDITLIMITAMT__c = 1000.00 ;
		fa.ACCTNBR__c = '123456789' ;
		fa.BRANCHORGNBR__c = b.ID ;
		
		insert fa ;
		
		return fa ;
	}
	
	static testmethod void testCrash ()
	{
		Contact c = getC ( '321659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'NotRealObject__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'NotRealField__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '32169959874' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
		
		//  No Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testNone ()
	{
		Contact c = getC ( '321659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Contact' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'TaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '32169959874' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
		
		//  No Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testSingleContact ()
	{
		Contact c = getC ( '321659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Contact' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'TaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '321659874' ) ;
        ApexPages.currentPage().getParameters().put ( 'p', 'abc' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testDoubleContact ()
	{
		Branch__c b = getBranch () ;
		Contact c = getC ( '321659874' ) ;
		
		Financial_Account__c fa1 = getFA ( c, b ) ;
		Financial_Account__c fa2 = getFA ( c, b ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Financial_Account__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'ACCTNBR__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '123456789' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testDoubleContactStah ()
	{
		Contact c1 = getC ( '321659874' ) ;
		Contact c2 = getC ( '421659874' ) ;
		Contact c3 = getC ( '521659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Contact' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'TaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '***-**-9874' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( c1 ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testSingleAccount ()
	{
		Account a = getA ( '321659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Account' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'FederalTaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '321659874' ) ;
        ApexPages.currentPage().getParameters().put ( 'p', 'abc' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( a ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testDoubleAccount ()
	{
		Branch__c b = getBranch () ;
		Account a = getA ( '321659874' ) ;
		
		Financial_Account__c fa1 = getFA ( a, b ) ;
		Financial_Account__c fa2 = getFA ( a, b ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Financial_Account__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'ACCTNBR__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '123456789' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( a ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testDoubleAccountStah ()
	{
		Account a1 = getA ( '321659874' ) ;
		Account a2 = getA ( '322659874' ) ;
		Account a3 = getA ( '323659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Account' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'FederalTaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '*32-****874' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( a1 ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testMaskedUpperContact ()
	{
		Contact c1 = getC ( '321659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Contact' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'TaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '***-**-9874' ) ;
        ApexPages.currentPage().getParameters().put ( 'p', 'abc' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( c1 ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testMaskedUpperAccount ()
	{
		Account a1 = getA ( '321659874' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.insight_passthrough ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'o', 'Account' ) ;
        ApexPages.currentPage().getParameters().put ( 'f', 'FederalTaxID__c' ) ;
        ApexPages.currentPage().getParameters().put ( 'q', '32-****874' ) ;
        ApexPages.currentPage().getParameters().put ( 'p', 'abc' ) ;
        
		//  Feed the standard controller to the page controller
		insight_passthrough ip = new insight_passthrough () ;
        
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( a1 ) ;
		
		//  Haz Page?!
		PageReference pr = ip.getPR () ;
		System.assertNotEquals ( pr, null ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
}