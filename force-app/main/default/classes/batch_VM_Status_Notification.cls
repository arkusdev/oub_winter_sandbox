global class batch_VM_Status_Notification implements Database.Batchable<SObject>, Database.Stateful
{
	private Vendor_Management_Settings__c vms ;
	private Batch_Process_Settings__c bps ;
	
	global batch_VM_Status_Notification ()
	{
		vms = Vendor_Management_Settings__c.getInstance ( 'VM_Settings' ) ;	
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
	}

	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query ;
		
		query  = '' ;
		query += 'SELECT ID, ' ;
		query += 'Vendor_Management__r.ID, ' ;
		query += 'Vendor_Management__r.OwnerID, ' ;
		query += 'Vendor_Management__r.Name, ' ;
		query += 'Vendor_Management__r.Vendor_Status__c, ' ;
		query += 'Name, ' ; 
		query += 'Notification_Date__c, ' ; 
		query += 'Expiration_Date__c, ' ;
		query += 'Vendor_Status__c,  ' ;
		query += 'GLBA_Compliant__c ' ;
		query += 'FROM ' ;
		query += 'Vendor_Contract__c ' ;
		query += 'WHERE ' ;
        query += 'Vendor_Management__r.RecordType.Name <> \'Service Provider\' ' ;
		if ( ( vms != null ) && ( String.isNotBlank ( vms.Batch_Exclude_Status__c ) ) )
			query += 'AND Vendor_Management__r.Vendor_Status__c <> \'' + vms.Batch_Exclude_Status__c + '\' ' ;
		else
			query += 'AND Vendor_Management__r.Vendor_Status__c <> \'Closed\' ' ;
		query += 'ORDER BY ' ;
		query += 'Vendor_Management__r.OwnerID, ' ;
		query += 'Vendor_Management__r.Name, ' ;
		query += 'Name ' ;
				
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Vendor_Contract__c> iL )
	{
		Map<String,User> uMap = OneUnitedUtilities.getActiveUsers () ;
		List<Messaging.SingleEmailMessage> mL = new List<Messaging.SingleEmailMessage> () ;
		
		String priorEmail = '' ;
		String message ;
		Messaging.SingleEmailMessage m ;
		
		if ( iL.size () > 0 )
		{
			for ( Vendor_Contract__c vc : iL )
			{
				//  Used later on
				Vendor_Management__c vm = vc.Vendor_Management__r ;
				User u = uMap.get ( vm.OwnerID ) ;
				
				System.debug ( priorEmail + ' -- ' + u.Email ) ;
				
				if ( ! priorEmail.equalsIgnoreCase ( u.Email ) )
				{
					if ( String.isNotBlank ( priorEmail ) )
					{
						System.debug ( 'Adding record of prior email to outgoing' ) ;
						
						message += '</table>' ;
						message += '</body>' ;
						message += '</html>' ;
						m.setHTMLBody ( message ) ;
						
						mL.add ( m ) ;
					}
						
					priorEmail = u.Email ;
					
					System.debug ( 'Setting up new email - ' + u.Email ) ;
					
					//  Create message for each user
					m = new Messaging.SingleEmailMessage () ;
	
					List<String> toAddresses = new List<String> () ;
					
					if ( vms != null )
					{
						if ( String.IsNotBlank ( vms.Email_Override__c ) )
							toAddresses.add ( vms.Email_Override__c ) ;
						else
							toAddresses.add ( u.Email ) ;
							
						if ( String.IsNotBlank ( vms.Email_CC__c ) )
							toAddresses.add ( vms.Email_CC__c ) ;
					}
					else
					{
						toAddresses.add ( u.Email ) ;
					}
					
					m.setToAddresses ( toAddresses ) ;
					m.setSubject ( 'Vendor Contract Status as of ' + Date.today ().format () ) ;
				
					message  = '' ;
					message += '<html>' ;
					message += '<body>' ;
					message += '<p>Vendor Status Notification</p>' ;
		
					message += '<p>As the manager assigned to these contracts, it is your responsibility ' ;
					message += 'to ensure that the vendor meets all GLBA requirements and that the ' ;
					message += 'contract remains current. Please ensure that your vendor contract is ' ;
					message += 'scanned in and all information regarding this vendor is accurate.</p>' ;
		
					message += '<p>Any vendor on this list runs the risk of being denied payment.</p>' ;
		
					message += '<table width="100%" border="1">' ;
					message += '<tr>' ;
					message += '<th>Status</th><th>Expiration</th><th>Compliance</th><th>Vendor</th><th>Contract</th>';
					message += '</tr>' ;
					
					//  FIRST RECORD
					String ed = '' ;
					if ( vc.Expiration_Date__c != null )
						ed = vc.Expiration_Date__c.format () ;
					
					message += '<tr>' ;
					message += '<td>' +vc.Vendor_Status__c + '</td><td>' + ed + '</td><td>' + vc.GLBA_Compliant__c + '</td>' ;
					message += '<td><a href="' + URL.getSalesforceBaseUrl ().toExternalForm () + '/' + vm.ID + '">' + vm.Name + '</a></td>'  ;
					message += '<td><a href="' + URL.getSalesforceBaseUrl ().toExternalForm () + '/' + vc.ID + '">' + vc.Name + '</a></td>'  ;
					message += '</tr>' ;
					message += '' ;
				}
				else
				{
					String ed = '' ;
					if ( vc.Expiration_Date__c != null )
						ed = vc.Expiration_Date__c.format () ;
					
					message += '<tr>' ;
					message += '<td>' +vc.Vendor_Status__c + '</td><td>' + ed + '</td><td>' + vc.GLBA_Compliant__c + '</td>' ;
					message += '<td><a href="' + URL.getSalesforceBaseUrl ().toExternalForm () + '/' + vm.ID + '">' + vm.Name + '</a></td>'  ;
					message += '<td><a href="' + URL.getSalesforceBaseUrl ().toExternalForm () + '/' + vc.ID + '">' + vc.Name + '</a></td>'  ;
					message += '</tr>' ;
					message += '' ;
				}
			}
			
			// FINAL MESSAGE
			message += '</table>' ;
			message += '</body>' ;
			message += '</html>' ;
			m.setHTMLBody ( message ) ;
			mL.add ( m ) ;
			
			//  Don't email if running test
			if ( ! Test.isRunningTest () )
				Messaging.sendEmail ( mL ) ;
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Vendor Management Status Notification Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}