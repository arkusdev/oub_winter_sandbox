@isTest
public class TestTaskSurveyUpdate 
{
	private static OneSurvey__c getSurvey ()
	{
		OneSurvey__c os = new OneSurvey__c () ;
		os.Active__c = true ;
		os.Feedback_Type__c = '1 - 5' ;
		os.Start_Date__c = Date.today () -1 ;
		os.End_Date__c = Date.today () +1 ;
		
		insert os ;
		return os ;
	}
    
	public static testmethod void testOne ()
    {
        //  Setup
        Task t = new Task () ;
        t.Name__c = 'Test Task' ;
        
        insert t ;
        
        OneSurvey__c os = getSurvey () ;
        
        Survey_Response__c s = new Survey_Response__c () ;
        s.Survey__c = os.ID ;
        
        insert s ;

        //  Go!
        Test.startTest () ;
        
        TaskSurveyUpdate.TaskRequest tsuTR = new TaskSurveyUpdate.TaskRequest () ;
        tsuTR.taskID = t.ID ;
        tsuTR.surveyID = s.ID ;
        
        List<TaskSurveyUpdate.TaskRequest> tsuTRL = new List<TaskSurveyUpdate.TaskRequest> () ;
        tsuTRL.add ( tsuTR ) ;
        
        List<TaskSurveyUpdate.TaskResult> tsuTRL2 = TaskSurveyUpdate.updateTask ( tsuTRL ) ;
            
        //  End
        Test.stopTest () ;
    }
}