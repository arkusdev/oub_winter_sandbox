public with sharing class application_3rd_Party_Send 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Results
     *  -------------------------------------------------------------------------------------------------- */
    public boolean applicantResult { public get ; private set ; }
    public boolean coApplicantResult { public get ; private set ; }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Application / Record Type IDs
     *  -------------------------------------------------------------------------------------------------- */
    private one_application__c app;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public application_3rd_Party_Send ( ApexPages.StandardController controller )
    {
        //  Get (possibly incomplete) application 
        this.app = (one_application__c) controller.getRecord () ;
        
        //  Reload application
        this.app = one_utils.getApplicationFromId ( app.Id ) ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the report
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runReport ()
    {
        //  Return back to original application
        PageReference pr = new PageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        //  Set up helper object
        FISHelper fish = new FISHelper () ;
        fish.setApplication ( app ) ;
        
        //  Jim's all-in-one special
        fish.runEverything () ;
        
        return pr ;
    }
}