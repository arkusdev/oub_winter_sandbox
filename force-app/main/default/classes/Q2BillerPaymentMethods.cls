public class Q2BillerPaymentMethods 
{
	public cls_data[] data;
    
	public class cls_data 
    {
		public String x_id;
		public String type;
		public String identifier;
		public String status;
		public boolean xdefault;
		public String name;
		public cls_card card;
        public cls_bank bank;
	}
    
	public class cls_card 
    {
		public String xnumber;
		public String brand;
		public String type;
		public String expDate;
	}
    
    public class cls_bank
    {
        public String routing ;
        public String account ;
        public String type ;
    }
    
	public static Q2BillerPaymentMethods parse(String json){
		return (Q2BillerPaymentMethods) System.JSON.deserialize(json, Q2BillerPaymentMethods.class);
	}
}