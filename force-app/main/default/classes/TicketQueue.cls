public class TicketQueue implements Queueable
{
    private list<Automated_Email__c> aeL ;
	
    public TicketQueue ()
    {
        
    }
    
    public TicketQueue ( list<Automated_Email__c> xL )
    {
        aeL = xL ;
    }
    
    public void execute(QueueableContext context) 
    {
        try
        {
            if ( ( aeL != null ) && ( aeL.size () > 0 ) )
                insert aeL ;
        }
        catch (Exception e)
        {
            // ?
        }
    }
}