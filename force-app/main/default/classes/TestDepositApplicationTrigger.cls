@isTest
public with sharing class TestDepositApplicationTrigger 
{
    /* -----------------------------------------------------------------------------------
	 * Settings
	 * ----------------------------------------------------------------------------------- */
	private static Deposit_Application_Settings__c getSettings ()
	{
		Deposit_Application_Settings__c das = new Deposit_Application_Settings__c () ;
		das.Name = 'Andera' ;
		das.Email_Callout_Functionality__c = true ;
		
		insert das ;
		
		return das ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Account
	 * ----------------------------------------------------------------------------------- */
    private static Account getAccount ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        
        insert a ;
        
        return a ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Load and return a deposit application
	 * ----------------------------------------------------------------------------------- */
	private static Deposit_Application__c getDepositApplication ( Integer i )
	{
		Deposit_Application__c da = new Deposit_Application__c () ;
		
		da.First_Name__c = 'dtest' + i ;
		da.Middle_Name__c = 'Er' ;
		da.Last_Name__c = 'duser' + i ;
		da.Email__c = 'dtest' + i + '@duser' + i + '.com' ;
		
		if ( i >= 0 && i < 10)
			da.Applicant_SSN__c = '00101100' + i ;
		else if ( i >= 10 && i < 100 )
			da.Applicant_SSN__c = '0010110' + i ;
		else
			da.Applicant_SSN__c = '001011' + i ;
		
		da.Address__c = '1313 Testingbird Lane' ;
		da.City__c = 'Boston' ;
		da.State__c = 'MA' ;
		da.Zip__c = '02110' ;
		da.Country__c = 'USA' ;
		
		da.Daytime_Phone__c = '617-555-1111' ;
		da.Day_Phone_Extension__c = '4357' ;
		da.Evening_Phone__c = '857-555-2222' ;
		
		da.Confirmation_Number__c = 'XYZ123' ;

		da.Co_Applicant_First_Name__c = 'cotest' + i ;
		da.Co_Applicant_Last_Name__c = 'couser' + i ;
		da.Co_Applicant_Email__c = 'cotest' + i + '@couser' + i + '.com' ;

		if ( i >= 0 && i < 10)
			da.Co_Applicant_SSN__c = '00202200' + i ;
		else if ( i >= 10 && i < 100 )
			da.Co_Applicant_SSN__c = '0020220' + i ;
		else
			da.Co_Applicant_SSN__c = '002022' + i ;
		
		da.Co_Applicant_Address__c = '1313 Testingbird Lane' ;
		da.Co_Applicant_City__c = 'Boston' ;
		da.Co_Applicant_State__c = 'MA' ;
		da.Co_Applicant_Zip__c = '02110' ;
		
		da.Co_Applicant_Daytime_Phone__c = '617-555-1111' ;
		da.Co_Applicant_Day_Phone_Extension__c = '4357' ;
		da.Co_Applicant_Evening_Phone__c = '857-555-2222' ;

		da.Beneficiary_First_Name__c = 'betest' + i ;
		da.Beneficiary_Last_Name__c = 'beuser' + i ;
		da.Beneficiary_Email__c = 'betest' + i + '@beuser' + i + '.com' ;
		
		if ( i >= 0 && i < 10)
			da.Beneficiary_SSN__c = '00303300' + i ;
		else if ( i >= 10 && i < 100 )
			da.Beneficiary_SSN__c = '0030330' + i ;
		else
			da.Beneficiary_SSN__c = '003033' + i ;

		da.Beneficiary_Address__c = '1313 Testingbird Lane' ;
		da.Beneficiary_City__c = 'Boston' ;
		da.Beneficiary_State__c = 'MA' ;
		da.Beneficiary_Zip__c = '02110' ;
		
		da.Beneficiary_Daytime_Phone__c = '617-555-1111' ;
		da.Beneficiary_Day_Phone_Extension__c = '4357' ;
		da.Beneficiary_Evening_Phone__c = '857-555-2222' ;
		
		da.Created_DateTime__c = System.now () ;

		return da ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Select deposit application
	 * ----------------------------------------------------------------------------------- */
    private static Deposit_Application__c reGetDepositApplication ( ID x )
    {
    	Deposit_Application__c da = null ;
    	
    	try
    	{
	    	da = [
	    		SELECT ID, 
	    		External_Key__c, Last_Updated__c,
	    		Email__c, Applicant_SSN__c,
	    		Tax_Reported_Contact__c, 
	    		Tax_Reported_Contact__r.Email, Tax_Reported_Contact__r.TaxID__c, Tax_Reported_Contact__r.recordTypeID, Tax_Reported_Contact__r.Date_Last_Deposit_App__c,
	    		Co_Applicant_Email__c, Co_Applicant_SSN__c,
	    		Co_Applicant_Tax_Reported_Contact__c,
	    		Co_Applicant_Tax_Reported_Contact__r.Email, Co_Applicant_Tax_Reported_Contact__r.TaxID__c, Co_Applicant_Tax_Reported_Contact__r.recordTypeID, Co_Applicant_Tax_Reported_Contact__r.Date_Last_Deposit_App__c,
	    		Beneficiary_Email__c, Beneficiary_SSN__c,
	    		Beneficiary_Tax_Reported_Contact__c,
	    		Beneficiary_Tax_Reported_Contact__r.Email, Beneficiary_Tax_Reported_Contact__r.TaxID__c, Beneficiary_Tax_Reported_Contact__r.recordTypeID, Beneficiary_Tax_Reported_Contact__r.Date_Last_Deposit_App__c,
	    		Application_Processing_Status__c, Additional_Information_Required__c,
	    		Denial_Email_Sent__c, Last_Additional_Info_Email_Date__c,
	    		Created_DateTime__c,
                Additional_Doc_Needed_Datetime__c
	    		FROM Deposit_Application__c
	    		WHERE ID = :x 
	    	] ;
    	}
    	catch ( DMLException e )
    	{
    		// WTF
    	}
    	
    	return da ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Select Contact
	 * ----------------------------------------------------------------------------------- */
    private static Contact reGetContact ( ID x )
    {
    	Contact c = null ;
    	
    	try
    	{
    		c = [
    			SELECT ID, Email,
    			TaxID__c, FirstName, LastName,
    			Date_Last_Deposit_App__c,
    			recordTypeID
    			FROM Contact
    			WHERE ID = :x
    		] ;
    	}
    	catch ( DMLException e )
    	{
    		// WTF
    	}
    	
    	return c ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, no contacts at all
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testNoExistingContacts ()
    {
		System.debug ( '------------------------------ test NoExistingContacts ------------------------------' ) ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
    	
    	Test.startTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	insert da1 ;

		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	//  Simple stuff first
    	System.assertNotEquals ( da1x.External_Key__c, null ) ;
    	
    	//  Applicant
    	System.assertNotEquals ( da1x.Tax_Reported_Contact__c, null ) ;
    	System.assertEquals ( da1x.Email__c, da1x.Tax_Reported_Contact__r.Email ) ;
    	System.assertEquals ( da1x.Applicant_SSN__c, da1x.Tax_Reported_Contact__r.TaxID__c ) ;
    	System.assertEquals ( prospectRecordTypeID, da1x.Tax_Reported_Contact__r.recordTypeID ) ;
    	System.assertEquals ( da1x.Created_DateTime__c, da1x.Tax_Reported_Contact__r.Date_Last_Deposit_App__c ) ;
    	
    	//  Co-Applicant
    	System.assertNotEquals ( da1x.Co_Applicant_Tax_Reported_Contact__c, null ) ;
    	System.assertEquals ( da1x.Co_Applicant_Email__c, da1x.Co_Applicant_Tax_Reported_Contact__r.Email ) ;
    	System.assertEquals ( da1x.Co_Applicant_SSN__c, da1x.Co_Applicant_Tax_Reported_Contact__r.TaxID__c ) ;
    	System.assertEquals ( prospectRecordTypeID, da1x.Co_Applicant_Tax_Reported_Contact__r.recordTypeID ) ;
    	System.assertEquals ( da1x.Created_DateTime__c, da1x.Co_Applicant_Tax_Reported_Contact__r.Date_Last_Deposit_App__c ) ;
    	
    	//  Beneficiary
    	System.assertNotEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, null ) ;
    	System.assertEquals ( da1x.Beneficiary_Email__c, da1x.Beneficiary_Tax_Reported_Contact__r.Email ) ;
    	System.assertEquals ( da1x.Beneficiary_SSN__c, da1x.Beneficiary_Tax_Reported_Contact__r.TaxID__c ) ;
    	System.assertEquals ( prospectRecordTypeID, da1x.Beneficiary_Tax_Reported_Contact__r.recordTypeID ) ;
    	System.assertEquals ( da1x.Created_DateTime__c, da1x.Beneficiary_Tax_Reported_Contact__r.Date_Last_Deposit_App__c ) ;
    	
    	Test.stopTest () ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, all contacts pre-exist by first name, last name AND email
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testPreExistingContactByFirstLastEmail ()
    {
		System.debug ( '------------------------------ test PreExistingContactByFirstLastEmail ------------------------------' ) ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	
    	Contact c1 = new Contact () ;
    	c1.FirstName = da1.First_Name__c ;
    	c1.LastName = da1.Last_Name__c ;
    	c1.Email = da1.Email__c ;
    	c1.recordTypeID = customerRecordTypeID ;
    	
    	insert c1 ;
    	
    	Contact c2 = new Contact () ;
    	c2.FirstName = da1.Co_Applicant_First_Name__c ;
    	c2.LastName = da1.Co_Applicant_Last_Name__c ;
    	c2.Email = da1.Co_Applicant_Email__c ;
    	c2.recordTypeID = customerRecordTypeID ;
    	
    	insert c2 ;
    	
    	Contact c3 = new Contact () ;
    	c3.FirstName = da1.Beneficiary_First_Name__c ;
    	c3.LastName = da1.Beneficiary_Last_Name__c ;
    	c3.Email = da1.Beneficiary_Email__c ;
    	c3.recordTypeID = customerRecordTypeID ;
    	
    	insert c3 ;
    	
    	insert da1 ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	//  Re-select
    	Contact c1x = reGetContact ( c1.ID ) ;
    	
    	//  Applicant
    	System.assertEquals ( da1x.Tax_Reported_Contact__c, c1x.ID ) ;
    	System.assertEquals ( da1x.Email__c, c1x.Email ) ;
    	System.assertEquals ( da1x.Applicant_SSN__c, c1x.TaxID__c ) ;
    	
    	//  Re-select
    	Contact c2x = reGetContact ( c2.ID ) ;
    	
    	//  Co-Applicant
    	System.assertEquals ( da1x.Co_Applicant_Tax_Reported_Contact__c, c2x.ID ) ;
    	System.assertEquals ( da1x.Co_Applicant_Email__c, c2x.Email ) ;
    	System.assertEquals ( da1x.Co_Applicant_SSN__c, c2x.TaxID__c ) ;
    	
    	//  Re-select
    	Contact c3x = reGetContact ( c3.ID ) ;
    	
    	//  Beneficiary
    	System.assertEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, c3x.ID ) ;
    	System.assertEquals ( da1x.Beneficiary_Email__c, c3x.Email ) ;
    	System.assertEquals ( da1x.Beneficiary_SSN__c, c3x.TaxID__c ) ;
    	
    	Test.stopTest () ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, all contacts have first/last/email mismatch
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testPreExistingContactFirstLastEmailMismatch ()
    {
		System.debug ( '------------------------------ test PreExistingContactFirstLastEmailMismatch ------------------------------' ) ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	
    	Contact c1 = new Contact () ;
    	c1.FirstName = da1.First_Name__c + 'NYET' ;
    	c1.LastName = da1.Last_Name__c + 'NYET'  ;
    	c1.Email = da1.Email__c ;
    	c1.recordTypeID = customerRecordTypeID ;
    	
    	insert c1 ;
    	
    	Contact c2 = new Contact () ;
    	c2.FirstName = da1.Co_Applicant_First_Name__c + 'NYET'  ;
    	c2.LastName = da1.Co_Applicant_Last_Name__c + 'NYET'  ;
    	c2.Email = da1.Co_Applicant_Email__c ;
    	c2.recordTypeID = customerRecordTypeID ;
    	
    	insert c2 ;
    	
    	Contact c3 = new Contact () ;
    	c3.FirstName = da1.Beneficiary_First_Name__c + 'NYET'  ;
    	c3.LastName = da1.Beneficiary_Last_Name__c + 'NYET'  ;
    	c3.Email = da1.Beneficiary_Email__c ;
    	c3.recordTypeID = customerRecordTypeID ;
    	
    	insert c3 ;
    	
    	insert da1 ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	//  Applicant
    	System.assertNotEquals ( da1x.Tax_Reported_Contact__c, c1.ID ) ;
    	
    	//  Co-Applicant
    	System.assertNotEquals ( da1x.Co_Applicant_Tax_Reported_Contact__c, c2.ID ) ;
    	
    	//  Beneficiary
    	System.assertNotEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, c3.ID ) ;
    	
    	Test.stopTest () ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, all contacts pre-exist by SSN
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testPreExistingContactBySSN ()
    {
		System.debug ( '------------------------------ test PreExistingContactBySSN ------------------------------' ) ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	
    	Test.StartTest () ;
    	
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	
    	Contact c1 = new Contact () ;
    	c1.FirstName = da1.First_Name__c ;
    	c1.LastName = da1.Last_Name__c ;
    	c1.TaxID__c = da1.Applicant_SSN__c ;
    	c1.recordTypeID = customerRecordTypeID ;
    	
    	insert c1 ;
    	
    	Contact c2 = new Contact () ;
    	c2.FirstName = da1.Co_Applicant_First_Name__c ;
    	c2.LastName = da1.Co_Applicant_Last_Name__c ;
    	c2.TaxID__c = da1.Co_Applicant_SSN__c ;
    	c2.recordTypeID = customerRecordTypeID ;
    	
    	insert c2 ;
    	
    	Contact c3 = new Contact () ;
    	c3.FirstName = da1.Beneficiary_First_Name__c ;
    	c3.LastName = da1.Beneficiary_Last_Name__c ;
    	c3.TaxID__c = da1.Beneficiary_SSN__c ;
    	c3.recordTypeID = customerRecordTypeID ;
    	
    	insert c3 ;
    	
    	insert da1 ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	//  Applicant
    	System.assertEquals ( da1x.Tax_Reported_Contact__c, c1.ID ) ;
    	System.assertEquals ( da1x.Applicant_SSN__c, c1.TaxID__c ) ;
    	
    	//  Co-Applicant
    	System.assertEquals ( da1x.Co_Applicant_Tax_Reported_Contact__c, c2.ID ) ;
    	System.assertEquals ( da1x.Co_Applicant_SSN__c, c2.TaxID__c ) ;
    	
    	//  Beneficiary
    	System.assertEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, c3.ID ) ;
    	System.assertEquals ( da1x.Beneficiary_SSN__c, c3.TaxID__c ) ;
    	
    	Test.stopTest () ;    	
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, all contacts pre-exist on application!
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testPreExistingContacts ()
    {
		System.debug ( '------------------------------ test testPreExistingContacts ------------------------------' ) ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	
    	Test.StartTest () ;
    	
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	
    	Contact c1 = new Contact () ;
    	c1.FirstName = da1.First_Name__c ;
    	c1.LastName = da1.Last_Name__c ;
    	c1.TaxID__c = da1.Applicant_SSN__c ;
    	c1.recordTypeID = customerRecordTypeID ;
    	
    	insert c1 ;
    	da1.Tax_Reported_Contact__c = c1.ID ;
    	
    	Contact c2 = new Contact () ;
    	c2.FirstName = da1.Co_Applicant_First_Name__c ;
    	c2.LastName = da1.Co_Applicant_Last_Name__c ;
    	c2.TaxID__c = da1.Co_Applicant_SSN__c ;
    	c2.recordTypeID = customerRecordTypeID ;
    	
    	insert c2 ;
    	da1.Co_Applicant_Tax_Reported_Contact__c = c2.ID ;
    	
    	Contact c3 = new Contact () ;
    	c3.FirstName = da1.Beneficiary_First_Name__c ;
    	c3.LastName = da1.Beneficiary_Last_Name__c ;
    	c3.TaxID__c = da1.Beneficiary_SSN__c ;
    	c3.recordTypeID = customerRecordTypeID ;
    	
    	insert c3 ;
		da1.Beneficiary_Tax_Reported_Contact__c = c3.ID ;
		    	
    	insert da1 ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	//  Applicant
    	System.assertEquals ( da1x.Tax_Reported_Contact__c, c1.ID ) ;
    	System.assertEquals ( da1x.Applicant_SSN__c, c1.TaxID__c ) ;
    	
    	//  Co-Applicant
    	System.assertEquals ( da1x.Co_Applicant_Tax_Reported_Contact__c, c2.ID ) ;
    	System.assertEquals ( da1x.Co_Applicant_SSN__c, c2.TaxID__c ) ;
    	
    	//  Beneficiary
    	System.assertEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, c3.ID ) ;
    	System.assertEquals ( da1x.Beneficiary_SSN__c, c3.TaxID__c ) ;
    	
    	Test.stopTest () ;    	
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, duplicates by SSN :-/
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDuplicateApplicationContactsSSN ()
    {
		System.debug ( '------------------------------ test DuplicateApplicationContactsSSN ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
    	list<Deposit_Application__c> daL = new list<Deposit_Application__c> () ;
    	
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	daL.add ( da1 ) ;
    	
    	Deposit_Application__c da2 = getDepositApplication ( 1 ) ;
    	daL.add ( da2 ) ;
    	
    	insert daL ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	Deposit_Application__c da2x = reGetDepositApplication ( da2.ID ) ;

    	System.assertEquals ( da1x.Tax_Reported_Contact__c, da2x.Tax_Reported_Contact__c ) ;
    	System.assertEquals ( da1x.Co_Applicant_Tax_Reported_Contact__c, da2x.Co_Applicant_Tax_Reported_Contact__c ) ;
    	System.assertEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, da2x.Beneficiary_Tax_Reported_Contact__c ) ;
    	
    	Test.stopTest () ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, duplicates by SSN :-/
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testOTHERDuplicateApplicationContactsSSN ()
    {
		System.debug ( '------------------------------ test OTHERDuplicateApplicationContactsSSN ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
    	list<Deposit_Application__c> daL = new list<Deposit_Application__c> () ;
    	
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	daL.add ( da1 ) ;
    	
    	Deposit_Application__c da2 = getDepositApplication ( 2 ) ;
    	da2.Co_Applicant_SSN__c = da1.Applicant_SSN__c ;
    	daL.add ( da2 ) ;
    	
    	insert daL ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	Deposit_Application__c da2x = reGetDepositApplication ( da2.ID ) ;

    	System.assertNotEquals ( da1x.Tax_Reported_Contact__c, da2x.Tax_Reported_Contact__c ) ;
    	System.assertEquals ( da1x.Tax_Reported_Contact__c, da2x.Co_Applicant_Tax_Reported_Contact__c ) ;
    	System.assertNotEquals ( da1x.Beneficiary_Tax_Reported_Contact__c, da2x.Beneficiary_Tax_Reported_Contact__c ) ;
    	
    	Test.stopTest () ;
    }

    /* -----------------------------------------------------------------------------------
	 * Deposit applications, duplicates by SSN :-/
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testOTHERDuplicateApplicationSinglePreExistingContactsSSN ()
    {
		System.debug ( '------------------------------ test OTHERDuplicateApplicationSinglePreExistingContactsSSN ------------------------------' ) ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
    	
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	
    	Deposit_Application__c da2 = getDepositApplication ( 1 ) ;
    	da2.Email__c = 'dtest2@duser2.com' ;
    	
    	Contact c1 = new Contact () ;
    	c1.FirstName = da1.First_Name__c ;
    	c1.LastName = da1.Last_Name__c ;
    	c1.Email = da1.Email__c ;
    	c1.recordTypeID = prospectRecordTypeID ;
    	
    	insert c1 ;
    	
    	Test.StartTest () ;
    	
    	list<Deposit_Application__c> daL = new list<Deposit_Application__c> () ;
    	
    	daL.add ( da1 ) ;
    	daL.add ( da2 ) ;
    	
    	insert daL ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	Deposit_Application__c da2x = reGetDepositApplication ( da2.ID ) ;
    	Contact c1x = reGetContact ( c1.ID ) ;
    	
    	System.assertEquals ( da1x.Applicant_SSN__c, c1x.TaxID__c ) ;
    	System.assertEquals ( da1x.Tax_Reported_Contact__c, c1x.ID ) ;
    	System.assertEquals ( da2x.Applicant_SSN__c, c1x.TaxID__c ) ;
    	System.assertEquals ( da2x.Tax_Reported_Contact__c, c1x.ID ) ;
    	
    	Test.stopTest () ;
    }

    /* -----------------------------------------------------------------------------------
	 * Deposit applications, duplicates by SSN :-/
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testOTHERDuplicateApplicationPreExistingContactsSSN ()
    {
		System.debug ( '------------------------------ test OTHERDuplicateApplicationPreExistingContactsSSN ------------------------------' ) ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
    	
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	
    	Deposit_Application__c da2 = getDepositApplication ( 1 ) ;
    	da2.Email__c = 'dtest2@duser2.com' ;
    	
    	Contact c1 = new Contact () ;
    	c1.FirstName = da1.First_Name__c ;
    	c1.LastName = da1.Last_Name__c ;
    	c1.Email = da1.Email__c ;
    	c1.recordTypeID = prospectRecordTypeID ;
    	
    	insert c1 ;
    	
    	Contact c2 = new Contact () ;
    	c2.FirstName = da2.First_Name__c ;
    	c2.LastName = da2.Last_Name__c ;
    	c2.Email = da2.Email__c ;
    	c2.recordTypeID = prospectRecordTypeID ;
    	
    	insert c2 ;
    	
    	Test.StartTest () ;
    	
    	list<Deposit_Application__c> daL = new list<Deposit_Application__c> () ;
    	
    	daL.add ( da1 ) ;
    	daL.add ( da2 ) ;
    	
    	insert daL ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	Contact c1x = reGetContact ( c1.ID ) ;
    	
    	Deposit_Application__c da2x = reGetDepositApplication ( da2.ID ) ;
    	Contact c2x = reGetContact ( c2.ID ) ;

    	System.assertEquals ( da1x.Applicant_SSN__c, c1x.TaxID__c ) ;
    	System.assertNotEquals ( da2x.Applicant_SSN__c, c2x.TaxID__c ) ;
    	System.assertEquals ( da2x.Applicant_SSN__c, c1x.TaxID__c ) ;
    	
    	Test.stopTest () ;
    }

    /* -----------------------------------------------------------------------------------
	 * Deposit applications, additional info email test INSERT 1
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationInfoEmailINSERT1 ()
    {
		System.debug ( '------------------------------ test DepositApplicationInfoEmailINSERT1 ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	da1.Current_Status__c = 'REVIEW' ;
    	da1.Debit_History_Response__c = 'REVIEW' ;
    	da1.Debit_History_Strategy_Code__c = 'ADDRESS ALERT: NONRESIDENTIAL' ;
		
		insert da1 ;
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Last_Additional_Info_Email_Date__c, Date.today () ) ;
    	System.assertEquals ( da1x.Application_Processing_Status__c, 'Additional Documentation Needed' ) ;
    	System.assertEquals ( Deposit_Application_Utils.getAdditionalInfoNeeded ( da1x.Additional_Information_Required__c ).size(), 1 ) ;
        System.assertNotEquals ( da1x.Additional_Doc_Needed_Datetime__c, null ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, additional info email test INSERT 2
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationInfoEmailINSERT2 ()
    {
		System.debug ( '------------------------------ test DepositApplicationInfoEmailINSERT2 ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	da1.Current_Status__c = 'REVIEW' ;
    	da1.Identity_Verification_Response__c = 'Review (Blah Blah Blah)' ;
		
		insert da1 ;
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Last_Additional_Info_Email_Date__c, Date.today () ) ;
    	System.assertEquals ( da1x.Application_Processing_Status__c, 'Additional Documentation Needed' ) ;
    	System.assertEquals ( Deposit_Application_Utils.getAdditionalInfoNeeded ( da1x.Additional_Information_Required__c ).size(), 2 ) ;
        System.assertNotEquals ( da1x.Additional_Doc_Needed_Datetime__c, null ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, additional info email test UPDATE 1
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationInfoEmailUPDATE1 ()
    {
		System.debug ( '------------------------------ test DepositApplicationInfoEmailUPDATE1 ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		
		insert da1 ;
		
    	da1.Current_Status__c = 'REVIEW' ;
    	da1.Debit_History_Response__c = 'REVIEW' ;
    	da1.Debit_History_Strategy_Code__c = 'ADDRESS ALERT: NONRESIDENTIAL' ;
		
		update da1 ;	
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Last_Additional_Info_Email_Date__c, Date.today () ) ;
    	System.assertEquals ( da1x.Application_Processing_Status__c, 'Additional Documentation Needed' ) ;
    	System.assertEquals ( Deposit_Application_Utils.getAdditionalInfoNeeded ( da1x.Additional_Information_Required__c ).size(), 1 ) ;
        System.assertNotEquals ( da1x.Additional_Doc_Needed_Datetime__c, null ) ;
    }

    /* -----------------------------------------------------------------------------------
	 * Deposit applications, additional info email test UPDATE 2
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationInfoEmailUPDATE2 ()
    {
		System.debug ( '------------------------------ test DepositApplicationInfoEmailUPDATE2 ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		
		insert da1 ;
		
    	da1.Current_Status__c = 'REVIEW' ;
    	da1.Identity_Verification_Response__c = 'Review (Blah Blah Blah)' ;
		
		update da1 ;	
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Last_Additional_Info_Email_Date__c, Date.today () ) ;
    	System.assertEquals ( da1x.Application_Processing_Status__c, 'Additional Documentation Needed' ) ;
    	System.assertEquals ( Deposit_Application_Utils.getAdditionalInfoNeeded ( da1x.Additional_Information_Required__c ).size(), 2 ) ;
        System.assertNotEquals ( da1x.Additional_Doc_Needed_Datetime__c, null ) ;
    }

    /* -----------------------------------------------------------------------------------
	 * Deposit applications, additional info email test UPDATE 3
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationInfoEmailUPDATE3 ()
    {
		System.debug ( '------------------------------ test DepositApplicationInfoEmailUPDATE3 ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
    	da1.Current_Status__c = 'REVIEW' ;
    	da1.Debit_History_Response__c = 'REVIEW' ;
    	da1.Debit_History_Strategy_Code__c = 'ADDRESS ALERT: NONRESIDENTIAL' ;
		
		System.debug ( '----- INSERT -----' ) ;
		insert da1 ;
		
		da1.Last_Additional_Info_Email_Date__c = date.today ().addDays ( -7 ) ;
		
		System.debug ( '----- UPDATE -----' ) ;
		update da1 ;	
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Last_Additional_Info_Email_Date__c, Date.today () ) ;
    	System.assertEquals ( da1x.Application_Processing_Status__c, 'Additional Documentation Needed' ) ;
    	System.assertEquals ( Deposit_Application_Utils.getAdditionalInfoNeeded ( da1x.Additional_Information_Required__c ).size(), 1 ) ;
        System.assertNotEquals ( da1x.Additional_Doc_Needed_Datetime__c, null ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, decline email test INSERT
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationDenyEmailINSERT ()
    {
		System.debug ( '------------------------------ test DepositApplicationDenyEmailINSERT ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		da1.Current_Status__c = 'Failed (AFTER REVIEW)' ;
		da1.Debit_History_Response__c = 'Accept' ;
				
		insert da1 ;
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Denial_Email_Sent__c, true ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, decline email test UPDATE
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationDenyEmailUPDATE ()
    {
		System.debug ( '------------------------------ test DepositApplicationDenyEmailUPDATE ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		
		insert da1 ;
		
		da1.Current_Status__c = 'Failed (AFTER REVIEW)' ;
		da1.Debit_History_Response__c = 'Accept' ;
		
		update da1 ;	
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Denial_Email_Sent__c, true ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, FCRA decline email test INSERT
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationFCRADenyEmailINSERT ()
    {
		System.debug ( '------------------------------ test DepositApplicationFCRADenyEmailINSERT ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
		
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		da1.Current_Status__c = 'Failed (AFTER REVIEW)' ;
		da1.Debit_History_Response__c = 'Review' ;
				
		insert da1 ;
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Denial_Email_Sent__c, true ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, decline email test UPDATE
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationFCRADenyEmailUPDATE ()
    {
		System.debug ( '------------------------------ test DepositApplicationFCRADenyEmailUPDATE ------------------------------' ) ;
		
    	Test.StartTest () ;
    	
		Deposit_Application_Settings__c das = getSettings () ;
        Account a = getAccount () ;
				
		Test.setMock ( HttpCalloutMock.class, new MockHttpResponseGenerator () ) ;	
		
    	Deposit_Application__c da1 = getDepositApplication ( 1 ) ;
		
		insert da1 ;
		
		da1.Current_Status__c = 'Failed (AFTER REVIEW)' ;
		da1.Debit_History_Response__c = 'Review' ;
		
		update da1 ;	
		
    	Test.stopTest () ;
    	
		//  RE-Select to get info
    	Deposit_Application__c da1x = reGetDepositApplication ( da1.ID ) ;
    	
    	System.assertEquals ( da1x.Denial_Email_Sent__c, true ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Deposit applications, BULK
	 * ----------------------------------------------------------------------------------- */
    static testmethod void testDepositApplicationBULK ()
    {
		System.debug ( '------------------------------ --------------------------- ------------------------------' ) ;
		System.debug ( '------------------------------ test DepositApplicationBULK ------------------------------' ) ;
		System.debug ( '------------------------------ --------------------------- ------------------------------' ) ;
		
    	Test.StartTest () ;
/*    	
    	list<Deposit_Application__c> daL = new list<Deposit_Application__c> () ;
    	
    	for ( Integer i = 0 ; i < 200 ; i ++ )
    	{
    		Deposit_Application__c da = getDepositApplication ( i ) ;
    		daL.add ( da ) ;
    	}
    	
    	insert daL ;
*/    	
    	Test.stopTest () ;
    }
}