public with sharing class AttachmentViewer 
{
    /*  --------------------------------------------------------------------------------------------------
	 *  GET OLD
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static list<Attachment> getAttachmentList ( ID xID )
    {
        list<Attachment> aL = null ;
        
        try
        {
           aL = [
               SELECT ID, Name, ContentType, Description
               FROM Attachment
               WHERE ParentID = :xID
           ]  ;
        }
        catch ( Exception e )
        {
            aL = new list<Attachment> () ;
        }
        
        return aL ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  GET NEW
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static list<ContentDocument> getContentDocumentList ( ID xID )
    {
        list<ContentDocument> xL = null ;
        
        try
        {
            list<ContentDocumentLink> cdLL = [
                SELECT 
                ID, ShareType, Visibility, ContentDocumentID,
                ContentDocument.ID, ContentDocument.Title, ContentDocument.FileExtension, ContentDocument.FileType
                FROM 
                ContentDocumentLink
                WHERE 
                LinkedEntityId = :xID
            ] ;
            
            List<ID> fooyooL = new list<ID> () ;
            
            if ( ( cdLL != null ) && ( cdLL.size () > 0 ) )
            {
                for ( ContentDocumentLink cdL : cdLL )
                    fooyooL.add ( cdL.ContentDocumentID ) ;
                
                if ( ( fooyooL != null ) && ( fooyooL.size () > 0 ) )
                {
                    xL = [
                        SELECT 
                        ID, Title, FileExtension, FileType
                        FROM 
                        ContentDocument
                        WHERE 
                        ID IN :fooyooL
                    ] ;
                }
            }
        }
        catch ( Exception e )
        {
        	xL = new list<ContentDocument> () ;
        }
        
        return xL ;
    }
}