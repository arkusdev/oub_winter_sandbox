@isTest
public class Test_I_Got_Bank_Book 
{
	private static Web_Settings__c getSettings ( Date contestEnd, Date contestWinner )
    {
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.I_Got_Bank_Banner__c = 'https://www.oneunited.com' ;
        ws.I_Got_Bank_Contest_End_Date__c = contestEnd ;
        ws.I_Got_Bank_Contest_Winner_Date__c = contestWinner ;
        ws.I_Got_Bank_Contest_Winner_Award__c = '$99,999' ;
        ws.I_Got_Bank_Contest_Winner_Number__c = 'Ninety-Nine (99)' ;

        return ws ;
    }
    
    public static testmethod void testNoContest ()
    {
        Web_Settings__c ws = getSettings ( Date.today().addDays ( -100 ), Date.today().addDays ( -90 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Book ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Contact c = new Contact () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Book igb = new I_Got_Bank_Book ( sc ) ;
        
    }

    public static testmethod void testSubmitFlow ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        insert a ;
        
        Web_Settings__c ws = getSettings ( Date.today().addDays ( 360 ), Date.today().addDays ( 360 ) ) ;
        insert ws ;
        
		//  Point to page to test
		PageReference pr1 = Page.I_Got_Bank_Book ;
		Test.setCurrentPage ( pr1 ) ;
        
        //  Controller
        Contact c = new Contact () ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController ( c ) ;
		
        Test.startTest () ;
        
        I_Got_Bank_Book igb = new I_Got_Bank_Book ( sc ) ;
        
        System.assertEquals ( igb.dStepNumber, 1000 ) ;
        
        igb.submitInformation () ;
        
        System.assertEquals ( igb.dStepNumber, 1001 ) ;
        
        igb.FirstName = 'Test' ;
        igb.LastName = 'User' ;
        igb.Email = 'test@user.com' ;
        igb.MobilePhone = '123-456-7890' ;
        
        igb.submitInformation () ;
        
        System.assertEquals ( igb.dStepNumber, 1100 ) ;
        
        Test.stopTest () ;
    }
}