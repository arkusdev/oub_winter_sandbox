global class batch_Advocate_Calculator implements Database.Batchable<SObject>, Database.Stateful 
{
	private Batch_Process_Settings__c bps ;
    
    private static final String JOB_TYPE = 'Salesforce Batch Apex' ;
    private static final String JOB_NAME = batch_Advocate_Calculator.class.getName () ;

	//  Counters & Ticket ID
	global Integer rewardAchieved ;
	global Integer levelAchieved ;
    
    global String status ;
    
    global batch_Advocate_Calculator ()
    {
        status = 'WARN' ;
        rewardAchieved = 0 ;
        levelAchieved = 0 ;
        
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- START --------------------' ) ;
        
		String query ;
		query  = '' ;
		query += 'SELECT ID, ' ;
        query += 'Advocate_Next_Level_Has_Reward__c, Reached_Next_Advocate_Level__c, ' ;
        query += 'Advocate_Level__r.Next_Level__r.ID, ' ;
        query += 'Advocate_Level__r.Next_Level__r.Advocate_Reward__c ' ;
        query += '' ;
        query += '' ;
		query += 'FROM Contact ' ;
        query += 'WHERE Reached_Next_Advocate_Level__c = TRUE ' ;
        query += 'AND Advocate_Level__r.Next_Level__c != NULL ' ;
        query += '' ;
        
		return Database.getQueryLocator ( query ) ;
    }
    
	global void execute ( Database.Batchablecontext bc, list<Contact> cL )
	{
		System.debug ( '-------------------- EXECUTE --------------------' ) ;
        
        list<Contact> ncL = new list<Contact> () ;
        list<Advocate_Reward_Achieved__c> araL = new list<Advocate_Reward_Achieved__c> () ;
        
        if ( ( cL != null ) && ( cL.size () > 0 ) )
        {
            for ( Contact c : cL )
            {
                if ( c.Advocate_Next_Level_Has_Reward__c == true )
                {
                    Advocate_Reward_Achieved__c ara = new Advocate_Reward_Achieved__c () ;
                    ara.Advocate_Reward__c = c.Advocate_Level__r.Next_Level__r.Advocate_Reward__c ;
                    ara.Contact__c = c.ID ;
                    
                    araL.add ( ara ) ;
                    rewardAchieved ++ ;
                }
                
                if ( ( c.Reached_Next_Advocate_Level__c == true ) && ( c.Advocate_Level__r.Next_Level__r.ID != null ) )
                {
                    Contact nc = new Contact () ;
                    nc.ID = c.ID ;
                    nc.Advocate_Level__c = c.Advocate_Level__r.Next_Level__r.ID ;
                    
                    ncL.add ( nc ) ;
                    levelAchieved ++ ;
                }
            }
        }
        else
        {
            status = 'GOOD' ;
        }
        
        try
        {
            if ( ( ncL != null ) && ( ncL.size () > 0 ) )
                update ncL ;
            
            if ( ( araL != null ) && ( araL.size () > 0 ) )
                insert araL ;
            
            status = 'GOOD' ;
        }
        catch ( Exception e )
        {
            status = 'ERROR' ;
        }
    }
    
	global void finish ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- FINISH --------------------' ) ;
		
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c,
				'helpdesk@OneUnited.com' 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email,
				'helpdesk@OneUnited.com' 
			} ;
		}
        
        if ( ( a.Status.equalsIgnoreCase ( 'Failed' ) ) || ( a.NumberOfErrors > 0 ) )
            status = 'ERROR' ;
        else if ( a.Status.equalsIgnoreCase ( 'Aborted' ) )
            status = 'WARN' ;
        else if ( ( a.TotalJobItems == 0 ) && ( a.NumberOfErrors == 0 ) )
            status = 'GOOD' ;
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( '[' + status + '][' + JOB_NAME + '][' + JOB_TYPE + '] Advocate Batch Calculator' ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.\n' ;
        
        if ( rewardAchieved > 0 )
            message += 'There were ' + rewardAchieved + ' contacts with awards.\n' ;
        else
            message += 'There were no contacts with awards.\n' ;
   
        if ( levelAchieved > 0 )
            message += 'There were ' + levelAchieved + ' contacts that have reached the next level.\n' ;
        else
            message += 'There were no contacts that reached the next level.\n' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ( ! Test.isRunningTest () ) && ( ! status.equalsIgnoreCase ( 'good' ) ) )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}