@isTest
public class TestOneTicketTab 
{
	public static testmethod void testOneTicket ()
    {
		String prospectRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
		
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'test@user.com' ;
		c.MailingState = 'CA' ;
		c.RecordTypeId = prospectRecordTypeId ;
		
		insert c ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		
		t.Name_of_Organization__c = 'Test Organization' ;
		t.First_Name__c = 'Test' ;
		t.Last_Name__c = 'User' ;
		t.Email_Address__c = 'test@user.com' ;
		t.Contact__c = c.id ;
		t.Date_Requested__c = Date.newInstance ( 2025, 10, 10 ) ;
		t.Beginning_Time__c = '10:00 AM PST' ;
		t.Ending_Time__c = '11:00 AM PST' ;
		t.Number_of_People_Expected__c = 5 ;
		t.Phone__c = '123-456-7890' ;
		t.Purpose_of_Meeting__c = 'Na na na na' ;
		
		insert t ;
        
		Test.StartTest () ;
        
        oneTicketTab ott = new oneTicketTab () ;
           
        System.assertNotEquals ( ott.recentTickets, null ) ;
        
		Test.StopTest () ;
    }
}