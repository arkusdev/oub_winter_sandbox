@isTest
public with sharing class Test_Batch_CreditCardRefund 
{
	static testmethod void oppTest ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c = new Contact () ;
		c.FirstName = 'Test11' ;
		c.LastName = 'User11' ;
		c.Email = 'test11@user11.com' ;
		
		insert c ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c.Id ;
		
		insert fa ; 
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Outstanding Card Balance' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		
		insert t ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_creditCardRefund b = new batch_creditCardRefund () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}
}