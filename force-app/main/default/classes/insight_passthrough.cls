public with sharing class insight_passthrough 
{
    /*  ----------------------------------------------------------------------------------------
     *  External Variables
     *  ---------------------------------------------------------------------------------------- */
    public String errorMsg { public get ; private set ; }
     
    /*  ----------------------------------------------------------------------------------------
     *  Internal Variables
     *  ---------------------------------------------------------------------------------------- */
    private PageReference gotoPR ;
	
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public insight_passthrough ()
	{
		gotoPR = null ;
		errorMsg = '' ;
		
		String o = ApexPages.currentPage().getParameters().get('o') ;  // Object
		String f = ApexPages.currentPage().getParameters().get('f') ;  // Field ...
		String q = ApexPages.currentPage().getParameters().get('q') ;  // Query Thingy ...
		String p = ApexPages.currentPage().getParameters().get('p') ;  // Passthrough page
		
		String origQ = q ;
		
		// Query?
		String query = '' ;
		query += 'SELECT ' ;
		query += 'ID ' ;
		query += 'FROM ' ;
		query += ' ' + o + ' ' ;
		query += 'WHERE ' ;
		
		// Exception for TaxID - strip out '-'s and 'X's
		if ( ( f.equalsIgnoreCase ( 'TaxID__c' ) ) || ( f.equalsIgnoreCase ( 'FederalTaxID__c' ) ) )
		{
			q = q.replace ( '-', '' ) ;
				
			// Replace %'s
			if ( q.Contains ( '*' ) )
			{
				q = q.replace ( '*', '%' ) ;
				query += ' ' + f + ' LIKE \'' + q + '\'' ;
			}
			else
			{		
				query += ' ' + f + ' = \'' + q + '\'' ;
			}
		}
		else
		{
			query += ' ' + f + ' = \'' + q + '\'' ;
		}
		
		query += '' ;
		
		System.debug ( '-------------------------------------------------------------------------------------------' ) ;
		System.debug ( query ) ;
		System.debug ( '-------------------------------------------------------------------------------------------' ) ;
		
		SObject[] sobj = null ;
		
		try
		{
			sobj = Database.query ( query ) ;
		}
		catch ( Exception e )
		{
			errorMsg = 'Unknown error occured' ;
		}
		
		if ( ( sobj != null ) && ( sobj.size () > 0 ) )
		{
			if ( sobj.size () == 1 )
			{
				Object obj = sobj[0].get ( 'id' ) ;
				
				System.debug ( '-------------------------------------------------------------------------------------------' ) ;
				System.debug ( 'ID ::' + obj ) ;
				System.debug ( '-------------------------------------------------------------------------------------------' ) ;
				
				if ( obj != null )
				{
					String sid = (String) obj ;
					String prURL = '/' ;
					
					if ( p != null )
						prURL += p ;
						
					gotoPR = new PageReference ( prURL + sid ) ;
					
					gotoPR.setRedirect ( true ) ;
				}
			}
			else
			{
				if ( ( f.equalsIgnoreCase ( 'TaxID__c' ) ) || ( f.equalsIgnoreCase ( 'FederalTaxID__c' ) ) )
				{
					origQ = origQ.replace ( '-', '' ) ;
					origQ = origQ.replace ( '*', '?' ) ;
					
					if ( o.equalsIgnoreCase ( 'Contact' ) )
					{
						gotoPR = new PageReference ( '/_ui/search/ui/UnifiedSearchResults?offset=0&setupid=ContactSearchLayouts&initialViewMode=detail&fen=003&noLookUp=true&str='+ origQ +'&isNFltr=1&relatedListId=Contact&00N50000002nbagEAAContact=1362&sFltrFields=00N50000002nbagEAAContact&asPhrase=1&fpg=-lzy3yo6ho' ) ;
					}
					else if ( o.equalsIgnoreCase ( 'Account' ) )
					{
						gotoPR = new PageReference ( '/_ui/search/ui/UnifiedSearchResults?offset=0&setupid=AccountSearchLayouts&initialViewMode=detail&00N30000000yhKnEAIAccount=629&fen=001&noLookUp=true&isNFltr=1&relatedListId=Account&str='+ origQ +'&sFltrFields=00N30000000yhKnEAIAccount&asPhrase=1&fpg=n07lvgaut' ) ;
					}
					else
					{
						gotoPR = new PageReference ( '/_ui/search/ui/UnifiedSearchResults?str=' + origQ ) ;
					}
				}
				else
				{
					gotoPR = new PageReference ( '/_ui/search/ui/UnifiedSearchResults?str=' + origQ ) ;
				}
			
				gotoPR.setRedirect ( true ) ;
			}
		}
		else
		{
			errorMsg = 'No data found on Salesforce for that record' ;
		}
	}
	
	public PageReference getPR ()
	{
		return gotoPR ;
	}

}