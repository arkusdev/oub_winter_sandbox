public class ApplicationTransactionHelper 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Double mapping tracking
	 *  -------------------------------------------------------------------------------------------------- */	
	public static map <String, boolean> voidMap ; 
	public static map <String, boolean> settleMap ; 
	public static map <String, boolean> refundMap ; 

	/*  --------------------------------------------------------------------------------------------------
	 *  Generic checker
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean checkDuplicate ( Map<String,boolean> xMap, String id )
	{
		boolean returnValue = false ;
		
		if ( xMap.containsKey ( id ) == true )
			returnValue = true ;
		else
			xMap.put ( id, true ) ;
		
        System.debug ( 'Returning : ' + returnValue ) ;
        
		return returnValue ;
	}
}