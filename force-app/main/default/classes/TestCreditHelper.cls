@isTest
public class TestCreditHelper 
{
	public static one_application__c createTestApplication() {
		one_application__c app = new one_application__c();
        app.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Unity Visa', 'one_application__c' );
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = 'First' ;
		app.Last_Name__c = 'Last' ;
		app.Email_Address__c = 'test@test.com' ;
        app.DOB__c = System.today().addYears(-20);
        app.SSN__c = '001-01-1001' ;
        app.Co_Applicant_SSN__c = '111-22-3333';
        app.Phone__c = one_utils.formatPhone ( '1112223333' ) ;
        
        app.Street_Address__c = '111 Test St.' ;
        app.City__c = 'Test City' ;
        app.State__c = 'MA' ;
        app.ZIP_Code__c = '12345' ;
        app.Mailing_Address_Same__c = 'Yes' ;

        app.Gross_Income_Monthly__c = 4000.00;

		return app;
	}

	private static creditchecker__Credit_Report__c createTestCreditReport() {
		creditchecker__Credit_Report__c cccr = new creditchecker__Credit_Report__c();
		cccr.creditchecker__Applicants_First_Name__c = 'First';
		cccr.creditchecker__Applicants_Last_Name__c = 'Last';
		cccr.creditchecker__Applicants_SSN__c = '001-01-1001';
		cccr.creditchecker__CoApplicants_SSN__c = '111-22-3333';
		cccr.creditchecker__Current_City__c = 'TestCity';
		cccr.creditchecker__Current_Line__c = '111 Test St.';
		cccr.creditchecker__Current_StateCode__c = 'MA';
		cccr.creditchecker__Current_Zip_Code__c = '12345';
		cccr.creditchecker__Current_Country__c = 'USA';
		return cccr;
	}

	@isTest static void testLoadCreditDataNotCompleted() {
		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Requested';
		insert cccr;

		Test.startTest();
			CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);
		Test.stopTest();

		// When the Credit Report status is not Completed, the reportComplete flag should be false.
		System.assertEquals(false, chcr.reportComplete);
	}

	@isTest static void testLoadCreditDataCompleted() {
		one_application__c app = createTestApplication();
		insert app;

		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Completed';
		cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
		cccr.creditchecker__Applicants_Date_Of_Birth__c = System.today().addYears(-20);
		cccr.creditchecker__CoApplicants_Date_Of_Birth__c = System.today().addYears(-20);
		cccr.creditchecker__Average_Score__c = 700.0;
		cccr.creditchecker__Bankruptcy_Count__c = 0.0;
		cccr.creditchecker__Total_Liability_Payment__c = 100.0;
		cccr.creditchecker__Previous_City__c = 'TestCity2';
		cccr.creditchecker__Previous_Line__c = '112 Test St.';
		cccr.creditchecker__Previous_StateCode__c = 'MA';
		cccr.creditchecker__Previous_Zip_Code__c = '54321';
		cccr.creditchecker__Previous_Country__c = 'USA';
		insert cccr;

		List<creditchecker__Score_Model__c> ccsmList = new List<creditchecker__Score_Model__c>();
		creditchecker__Score_Model__c ccsm = new creditchecker__Score_Model__c();
		ccsm.creditchecker__Credit_Score__c = 80.0;
		ccsm.creditchecker__Factor_Code__c = '0080 :: ESTIMATED ANNUAL INCOME: $80,000';
        ccsm.creditchecker__Model_Name_Description__c = 'IncomeInsight';
        ccsm.creditchecker__Whose__c = 'Borrower';
        ccsm.creditchecker__Credit_Report__c = cccr.Id;
        ccsmList.add(ccsm);
        ccsm = new creditchecker__Score_Model__c();
		ccsm.creditchecker__Credit_Score__c = 40.0;
		ccsm.creditchecker__Factor_Code__c = '0040 :: ESTIMATED ANNUAL INCOME: $40,000';
        ccsm.creditchecker__Model_Name_Description__c = 'IncomeInsight';
        ccsm.creditchecker__Whose__c = 'Co-Borrower';
        ccsm.creditchecker__Credit_Report__c = cccr.Id;
        ccsmList.add(ccsm);
        ccsm = new creditchecker__Score_Model__c();
        ccsm.creditchecker__Credit_Score__c = 700.0;
        ccsm.creditchecker__Whose__c = 'Co-Borrower';
        ccsm.creditchecker__Credit_Report__c = cccr.Id;
        ccsmList.add(ccsm);
        insert ccsmList;

        List<creditchecker__Tradeline__c> cctlList = new List<creditchecker__Tradeline__c>();
        creditchecker__Tradeline__c cctl = new creditchecker__Tradeline__c();
        cctl.creditchecker__Account_Type__c = 'Mortgage';
        cctl.creditchecker__Credit_Report__c = cccr.Id;
        cctl.creditchecker__Date_Reported__c = System.today().addMonths(-5);
        cctl.creditchecker__Monthly_Payment_Amount__c = 1000.0;
        cctl.creditchecker__Whose__c = 'Borrower';
        cctl.creditchecker__Account_Status__c = 'Open' ;
        cctlList.add(cctl);
        cctl = new creditchecker__Tradeline__c();
        cctl.creditchecker__Account_Type__c = 'Mortgage';
        cctl.creditchecker__Credit_Report__c = cccr.Id;
        cctl.creditchecker__Date_Reported__c = System.today().addMonths(-4);
        cctl.creditchecker__Monthly_Payment_Amount__c = 1100.0;
        cctl.creditchecker__Whose__c = 'Borrower';
        cctl.creditchecker__Account_Status__c = 'Open' ;
        cctlList.add(cctl);
        cctl = new creditchecker__Tradeline__c();
        cctl.creditchecker__Account_Type__c = 'Mortgage';
        cctl.creditchecker__Credit_Report__c = cccr.Id;
        cctl.creditchecker__Date_Reported__c = System.today().addMonths(-3);
        cctl.creditchecker__Monthly_Payment_Amount__c = 1200.0;
        cctl.creditchecker__Whose__c = 'Co-Borrower';
        cctl.creditchecker__Account_Status__c = 'Open' ;
        cctlList.add(cctl);
        cctl = new creditchecker__Tradeline__c();
        cctl.creditchecker__Account_Type__c = 'Mortgage';
        cctl.creditchecker__Credit_Report__c = cccr.Id;
        cctl.creditchecker__Date_Reported__c = System.today().addDays(-15);
        cctl.creditchecker__Monthly_Payment_Amount__c = 1300.0;
        cctl.creditchecker__Whose__c = 'Joint';
        cctl.creditchecker__Account_Status__c = 'Open' ;
        cctlList.add(cctl);
        cctl = new creditchecker__Tradeline__c();
        cctl.creditchecker__Account_Type__c = 'Revolving';
        cctl.creditchecker__Credit_Report__c = cccr.Id;
        cctl.creditchecker__Date_Reported__c = System.today().addMonths(-1);
        cctl.creditchecker__Whose__c = 'Borrower';
        cctl.creditchecker__Monthly_Payment_Amount__c = 200.0;
        cctl.creditchecker__Account_Status__c = 'Open' ;
        cctlList.add(cctl);
        insert cctlList;

		Test.startTest();
			CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);
		Test.stopTest();

		// When the Credit Report status is Completed, the reportComplete flag should be true
		// and all relevant data should be filled.
		System.assertEquals(true, chcr.reportComplete);
		System.assertEquals(app.Id, chcr.applicationID);
		System.assertEquals(cccr.Id, chcr.creditReportID);
		System.assertEquals(false, chcr.noHitFound);
		System.assertEquals(cccr.creditchecker__Average_Score__c, chcr.creditScore);
		System.assertEquals(ccsmList[2].creditChecker__Credit_Score__c, chcr.creditScoreCoApp);
		System.assertEquals(true, chcr.incomeInsightReturned);
		System.assertEquals(ccsmList[0].creditChecker__Credit_Score__c * 1000.0, chcr.bureauReportedIncome);
		System.assertEquals(true, chcr.incomeInsightReturnedCoApp);
		System.assertEquals(ccsmList[1].creditChecker__Credit_Score__c * 1000.0, chcr.bureauReportedIncomeCoApp);
		System.assertEquals(2300.0, chcr.monthlyExpenses);
		System.assertEquals(1200.0, chcr.monthlyExpensesCoApp);
		System.assertEquals(1300.0, chcr.monthlyExpensesJoint);
		System.assertEquals(2, chcr.totalOpenMortgages);
		System.assertEquals(1, chcr.totalOpenMortgagesCoapp);
		System.assertEquals(1, chcr.totalOpenMortgagesJoint);
		System.assertEquals(false, chcr.deceased);
		System.assertEquals(false, chcr.deceasedCoApp);
		System.assertEquals(false, chcr.underage);
		System.assertEquals(false, chcr.underageCoApp);
		System.assertEquals(false, chcr.frozenFile);
		System.assertEquals(false, chcr.frozenFileCoApp);
		System.assertEquals(false, chcr.ssnMismatch);
		System.assertEquals(false, chcr.ssnMismatchCoApp);
		System.assertEquals(false, chcr.AddressDiscrepancy);
		System.assertEquals(false, chcr.AddressDiscrepancyCoApp);
	}

	@isTest static void testLoadCreditDataAlerts() {
		one_application__c app = createTestApplication();
		insert app;

		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Completed';
		cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
		cccr.creditchecker__Average_Score__c = 700.0;
		insert cccr;

		Id recordTypeId = 
			Schema.SObjectType.creditchecker__Alert__c.getRecordTypeInfosByName().get('Credit Response Alerts').getRecordTypeId();

        List<creditchecker__Alert__c> ccaList = new List<creditchecker__Alert__c>();
        creditchecker__Alert__c cca = new creditchecker__Alert__c();
        cca.creditchecker__Credit_Report__c = cccr.Id;
        cca.creditchecker__Whose__c = 'Borrower';
        cca.creditchecker__Credit_Alert_Description__c = 'FACTA: Address Discrepancy - Substantial difference between the address submitted in the credit request and the address(es) in the credit file. VERIFY IDENTITY OF CONSUMER BEFORE GRANTING CREDIT.';
        cca.RecordTypeId = recordTypeId;
        ccaList.add(cca);
        cca = new creditchecker__Alert__c();
        cca.creditchecker__Credit_Report__c = cccr.Id;
        cca.creditchecker__Whose__c = 'Co-Borrower';
        cca.creditchecker__Credit_Alert_Description__c = 'FACTA: Address Discrepancy - Substantial difference between the address submitted in the credit request and the address(es) in the credit file. VERIFY IDENTITY OF CONSUMER BEFORE GRANTING CREDIT.';
        cca.RecordTypeId = recordTypeId;
        ccaList.add(cca);
        cca = new creditchecker__Alert__c();
        cca.creditchecker__Credit_Report__c = cccr.Id;
        cca.creditchecker__Whose__c = 'Borrower';
        cca.creditchecker__Credit_Alert_Description__c = 'Test for deceased in the text.';
        cca.RecordTypeId = recordTypeId;
        ccaList.add(cca);
        cca = new creditchecker__Alert__c();
        cca.creditchecker__Credit_Report__c = cccr.Id;
        cca.creditchecker__Whose__c = 'Co-Borrower';
        cca.creditchecker__Credit_Alert_Description__c = 'Test for deceased in the text.';
        cca.RecordTypeId = recordTypeId;
        ccaList.add(cca);
        cca = new creditchecker__Alert__c();
        cca.creditchecker__Credit_Report__c = cccr.Id;
        cca.creditchecker__Whose__c = 'Borrower';
        cca.creditchecker__Credit_Alert_Description__c = 'Test for file frozen in the text.';
      //cca.creditchecker__Credit_Alert_Description__c = 'Test for file frozen in the text. VERIFY IDENTITY OF CONSUMER'; 
        ccaList.add(cca);
        cca.RecordTypeId = recordTypeId;
        cca = new creditchecker__Alert__c();
        cca.creditchecker__Credit_Report__c = cccr.Id;
        cca.creditchecker__Whose__c = 'Co-Borrower';
        cca.creditchecker__Credit_Alert_Description__c = 'Test for file frozen in the text.';
      //cca.creditchecker__Credit_Alert_Description__c = 'Test for file frozen in the text. VERIFY IDENTITY OF CONSUMER';   
        ccaList.add(cca);
        cca.RecordTypeId = recordTypeId;
        insert ccaList;

		CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);

		System.assertEquals(true, chcr.deceased);
		System.assertEquals(true, chcr.deceasedCoApp);
		System.assertEquals(true, chcr.frozenFile);
		System.assertEquals(true, chcr.frozenFileCoApp);
      //System.assertEquals(true, chcr.frozenFilePend);
      //System.assertEquals(true, chcr.frozenFileCoAppPend);
		System.assertEquals(true, chcr.AddressDiscrepancy);
		System.assertEquals(true, chcr.AddressDiscrepancyCoApp);

		ccaList[4].creditchecker__Credit_Alert_Description__c = 'Test for id fraud victim in the text.';
		ccaList[5].creditchecker__Credit_Alert_Description__c = 'Test for id fraud victim in the text.';
      //ccaList[4].creditchecker__Credit_Alert_Description__c = 'Test for id fraud victim in the text. VERIFY IDENTITY OF CONSUMER';
      //ccaList[5].creditchecker__Credit_Alert_Description__c = 'Test for id fraud victim in the text. VERIFY IDENTITY OF CONSUMER';
		update ccaList;

		chcr = CreditHelper.loadCreditData(cccr.Id);

		System.assertEquals(true, chcr.deceased);
		System.assertEquals(true, chcr.deceasedCoApp);
		System.assertEquals(true, chcr.frozenFile);
		System.assertEquals(true, chcr.frozenFileCoApp);
      //System.assertEquals(true, chcr.frozenFilePend);
      //System.assertEquals(true, chcr.frozenFileCoAppPend);
		System.assertEquals(true, chcr.AddressDiscrepancy);
		System.assertEquals(true, chcr.AddressDiscrepancyCoApp);

		ccaList[4].creditchecker__Credit_Alert_Description__c = 'FACTA: Fraud Victim test at the start';
		ccaList[5].creditchecker__Credit_Alert_Description__c = 'FACTA: Fraud Victim test at the start';
      //ccaList[4].creditchecker__Credit_Alert_Description__c = 'FACTA: Fraud Victim test at the start. VERIFY IDENTITY OF CONSUMER';
      //ccaList[5].creditchecker__Credit_Alert_Description__c = 'FACTA: Fraud Victim test at the start. VERIFY IDENTITY OF CONSUMER';
		update ccaList;

		chcr = CreditHelper.loadCreditData(cccr.Id);

		System.assertEquals(true, chcr.deceased);
		System.assertEquals(true, chcr.deceasedCoApp);
		System.assertEquals(true, chcr.frozenFile);
		System.assertEquals(true, chcr.frozenFileCoApp);
      //System.assertEquals(true, chcr.frozenFilePend);
      //System.assertEquals(true, chcr.frozenFileCoAppPend);
		System.assertEquals(true, chcr.AddressDiscrepancy);
		System.assertEquals(true, chcr.AddressDiscrepancyCoApp);
	}

	@isTest static void testLoadCreditDataBankruptcies() {
		one_application__c app = createTestApplication();
		insert app;

		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Completed';
		cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
		cccr.creditchecker__Average_Score__c = 700.0;
		insert cccr;

        List<creditchecker__Public_Record__c> ccprList = new List<creditchecker__Public_Record__c>();
        creditchecker__Public_Record__c ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Discharged';
        ccpr.creditchecker__Whose__c = 'Borrower';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Dismissed';
        ccpr.creditchecker__Whose__c = 'Borrower';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Discharged';
        ccpr.creditchecker__Whose__c = 'Co-Borrower';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Dismissed';
        ccpr.creditchecker__Whose__c = 'Co-Borrower';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Discharged';
        ccpr.creditchecker__Whose__c = 'Joint';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Dismissed';
        ccpr.creditchecker__Whose__c = 'Joint';
        ccprList.add(ccpr);
        insert ccprList;

		CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);

		System.assertEquals(2, chcr.publicBankrupcyRecordTotal);
		System.assertEquals(1, chcr.dischargeBankrupcyRecordTotal);
		System.assertEquals(1, chcr.dismissedBankrupcyRecordTotal);
		System.assertEquals(2, chcr.publicBankrupcyRecordTotalCoApp);
		System.assertEquals(1, chcr.dischargeBankrupcyRecordTotalCoApp);
		System.assertEquals(1, chcr.dismissedBankrupcyRecordTotalCoApp);
		System.assertEquals(2, chcr.publicBankrupcyRecordTotalJoint);
		System.assertEquals(1, chcr.dischargeBankrupcyRecordTotalJoint);
		System.assertEquals(1, chcr.dismissedBankrupcyRecordTotalJoint);
		System.assertEquals(false, chcr.bankrupcy());
		System.assertEquals(false, chcr.bankrupcyCoApp());
		System.assertEquals(false, chcr.bankrupcyJoint());

        ccprList = new List<creditchecker__Public_Record__c>();
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Satisfied';
        ccpr.creditchecker__Whose__c = 'Borrower';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Satisfied';
        ccpr.creditchecker__Whose__c = 'Co-Borrower';
        ccprList.add(ccpr);
        ccpr = new creditchecker__Public_Record__c();
        ccpr.creditchecker__Credit_Report__c = cccr.Id;
        ccpr.creditchecker__Disposition_Type__c = 'Satisfied';
        ccpr.creditchecker__Whose__c = 'Joint';
        ccprList.add(ccpr);
        insert ccprList;

		chcr = CreditHelper.loadCreditData(cccr.Id);

		System.assertEquals(3, chcr.publicBankrupcyRecordTotal);
		System.assertEquals(1, chcr.dischargeBankrupcyRecordTotal);
		System.assertEquals(1, chcr.dismissedBankrupcyRecordTotal);
		System.assertEquals(3, chcr.publicBankrupcyRecordTotalCoApp);
		System.assertEquals(1, chcr.dischargeBankrupcyRecordTotalCoApp);
		System.assertEquals(1, chcr.dismissedBankrupcyRecordTotalCoApp);
		System.assertEquals(3, chcr.publicBankrupcyRecordTotalJoint);
		System.assertEquals(1, chcr.dischargeBankrupcyRecordTotalJoint);
		System.assertEquals(1, chcr.dismissedBankrupcyRecordTotalJoint);
		System.assertEquals(true, chcr.bankrupcy());
		System.assertEquals(true, chcr.bankrupcyCoApp());
		System.assertEquals(true, chcr.bankrupcyJoint());
	}

	@isTest static void testLoadCreditDataInquiries() {
		one_application__c app = createTestApplication();
		insert app;

		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Completed';
		cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
		cccr.creditchecker__Average_Score__c = 700.0;
		insert cccr;

		// Borrower 3 within 6 months, 1 not
        List<creditchecker__Credit_Inquiry__c> ccciList = new List<creditchecker__Credit_Inquiry__c>();
        creditchecker__Credit_Inquiry__c ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-2);
        ccci.creditchecker__Whose__c = 'Borrower';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-3);
        ccci.creditchecker__Whose__c = 'Borrower';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-4);
        ccci.creditchecker__Whose__c = 'Borrower';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-8);
        ccci.creditchecker__Whose__c = 'Borrower';
        ccciList.add(ccci);
        // Co-Borrower 1 within 6 months, 2 not
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-2);
        ccci.creditchecker__Whose__c = 'Co-Borrower';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-9);
        ccci.creditchecker__Whose__c = 'Co-Borrower';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-8);
        ccci.creditchecker__Whose__c = 'Co-Borrower';
        ccciList.add(ccci);
        // Joint 2 within 6 months, 1 not
		ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-2);
        ccci.creditchecker__Whose__c = 'Joint';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-3);
        ccci.creditchecker__Whose__c = 'Joint';
        ccciList.add(ccci);
        ccci = new creditchecker__Credit_Inquiry__c();
        ccci.creditchecker__Credit_Report__c = cccr.Id;
        ccci.creditchecker__Date__c = System.today().addMonths(-8);
        ccci.creditchecker__Whose__c = 'Joint';
        ccciList.add(ccci);
        insert ccciList;

		CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);

		System.assertEquals(3, chcr.sixMonthCreditInquiry);
		System.assertEquals(1, chcr.sixMonthCreditInquiryCoApp);
		System.assertEquals(2, chcr.sixMonthCreditInquiryJoint);
	}

	@isTest static void testLoadCreditDataOtherConditions() {
		one_application__c app = createTestApplication();
		insert app;

		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Completed';
		cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
		cccr.creditchecker__Applicants_Date_Of_Birth__c = System.today().addYears(-10);
		cccr.creditchecker__CoApplicants_Date_Of_Birth__c = System.today().addYears(-10);
		cccr.creditchecker__Applicants_SSN__c = '111-11-1111';
		cccr.creditchecker__CoApplicants_SSN__c = '111-11-1111';
		insert cccr;

		Test.startTest();
			CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);
		Test.stopTest();

		// When the Credit Report status is Completed, the reportComplete flag should be true
		// and all relevant data should be filled.
		System.assertEquals(true, chcr.reportComplete);
		System.assertEquals(true, chcr.underage);
		System.assertEquals(true, chcr.underageCoApp);
		System.assertEquals(0, chcr.publicBankrupcyRecordTotal);
		System.assertEquals(false, chcr.bankrupcy());
		System.assertEquals(true, chcr.ssnMismatch);
		System.assertEquals(true, chcr.ssnMismatchCoApp);
	}

	@isTest static void testGetCredit() {
		one_application__c app = createTestApplication();
		insert app;

		Test.startTest();
			CreditHelper ch = new CreditHelper();
			CreditHelper.CreditResults chcr = CreditHelper.getCredit(app.Id);
		Test.stopTest();
	}

	@isTest static void testGetCreditAlreadyExists() {
		one_application__c app = createTestApplication();
		insert app;

		creditchecker__Credit_Report__c cccr = createTestCreditReport();
		cccr.creditchecker__Status__c = 'Completed';
		cccr.creditchecker__Parent_Record_Id__c = app.Id;
		insert cccr;

		Test.startTest();
			CreditHelper ch = new CreditHelper();
			CreditHelper.CreditResults chcr = CreditHelper.getCredit(app.Id);
		Test.stopTest();
	}
    
    @isTest static void testLoadScoreModel() {
        one_application__c app = createTestApplication();
        insert app;
        
        creditchecker__Credit_Report__c cccr = createTestCreditReport();
        cccr.creditchecker__Status_Description__c = 'abcd file frozen';
        cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
        cccr.creditchecker__Status__c = 'Completed';
        insert cccr;
        
        List<creditchecker__Score_Model__c> ccsmList = new List<creditchecker__Score_Model__c>();
		creditchecker__Score_Model__c ccsm = new creditchecker__Score_Model__c();
		ccsm.creditchecker__Credit_Score__c = 80.0;
		ccsm.creditchecker__Factor_Code__c = '0080 :: ESTIMATED ANNUAL INCOME: $80,000';
        ccsm.creditchecker__Model_Name_Description__c = 'IncomeInsight';
        ccsm.creditchecker__Whose__c = 'Borrower';
        ccsm.creditchecker__Credit_Report__c = cccr.Id;
        ccsmList.add(ccsm);
        ccsm = new creditchecker__Score_Model__c();
		ccsm.creditchecker__Credit_Score__c = 40.0;
		ccsm.creditchecker__Factor_Code__c = '0040 :: ESTIMATED ANNUAL INCOME: $40,000';
        ccsm.creditchecker__Model_Name_Description__c = 'IncomeInsight';
        ccsm.creditchecker__Whose__c = 'Co-Borrower';
        ccsm.creditchecker__Credit_Report__c = cccr.Id;
        ccsmList.add(ccsm);
        ccsm = new creditchecker__Score_Model__c();
        ccsm.creditchecker__Credit_Score__c = 700.0;
        ccsm.creditchecker__Whose__c = 'Co-Borrower';
        ccsm.creditchecker__Credit_Report__c = cccr.Id;
        ccsmList.add(ccsm);
        insert ccsmList;
        
        CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);
        
        System.debug('App IncomeInsight: ' + chcr.incomeInsightReturned);
        System.debug('Co-App IncomeInsight: ' + chcr.incomeInsightReturnedCoApp);
        System.debug('App BureauReportedIncome: ' + chcr.bureauReportedIncome);
        System.debug('Co-App BureauReportedIncome: ' + chcr.bureauReportedIncomeCoApp);
        System.debug('App grossMonthlyIncome: ' + app.Gross_Income_Monthly__c);
        System.debug('App additional income: ' + app.Additional_Income_Amount__c);
        
        System.debug('Co-App creditScore: ' + chcr.creditScoreCoApp);
    
    }
    
    @isTest static void testP022CRStatusDescOnly() {
        one_application__c app = createTestApplication();
        insert app;
        
        creditchecker__Credit_Report__c cccr = createTestCreditReport();
        //cccr.creditchecker__Status_Description__c = 'abcd file frozen';
        cccr.creditchecker__Status_Description__c = 'abcd file locked';
        cccr.creditchecker__Parent_Record_Id__c = app.Id;
		cccr.Application__c = app.Id;
        cccr.creditchecker__Status__c = 'Completed';
        insert cccr;
        
        CreditHelper.CreditResults chcr = CreditHelper.loadCreditData(cccr.Id);
        
        System.debug('chcr: ' + chcr);
        System.debug('Status Description: ' + chcr.statusDescription);
        System.debug('Status descr. contains file frozen: ' + chcr.statusDescription.containsIgnoreCase('file frozen'));
        System.debug('Status descr. contains file locked: ' + chcr.statusDescription.containsIgnoreCase('file locked'));
        System.debug('Status descr. not null: ' + (chcr.statusDescription != NULL));
        System.debug('Status descr. contains verifying consumer information: ' + chcr.statusDescription.containsIgnoreCase('VERIFYING CONSUMER INFORMATION'));
        System.debug('frozenFilePend: ' + chcr.frozenFilePend);
        
        /*
        for (creditchecker__Alert__c alert: cccr.creditchecker__Alerts__r) {
                String alertDesc = alert.creditchecker__Credit_Alert_Description__c;
        System.debug('Condition evaluation: ' + (((alertDesc != NULL &&
                        	   (alertDesc.containsIgnoreCase('file frozen') ||
                               alertDesc.containsIgnoreCase('id fraud victim') ||
                               alertDesc.startsWithIgnoreCase('FACTA: Fraud Victim ') ) ) && 
                               (alertDesc.containsIgnoreCase('VERIFY IDENTITY OF CONSUMER BEFORE GRANTING CREDIT') ||
                                alertDesc.containsIgnoreCase('DO NOT EXTEND CREDIT WITHOUT VERIFYING CONSUMER INFORMATION') ) ) || 
                               (cccr.creditchecker__Status_Description__c != NULL &&
                               (cccr.creditchecker__Status_Description__c.containsIgnoreCase('file frozen') ||
                               cccr.creditchecker__Status_Description__c.containsIgnoreCase('file locked') ) ) ) );
        }
		*/
        
        System.debug('Alerts descriptions:' + chcr.alertDescriptions);
        
        System.assertEquals(true, chcr.frozenFilePend);
    }
}