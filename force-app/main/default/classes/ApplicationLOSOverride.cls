public with sharing class ApplicationLOSOverride 
{
    /*  --------------------------------------------------------------------------------------------------
	 *  Setup
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static List<String> getDeclineReasons() {
        Schema.DescribeFieldResult F = one_application__c.Credit_Decline_Reasons__c.getDescribe () ;
        Schema.sObjectField T = F.getSObjectField () ;
        List<PicklistEntry> declineReasonEntries = T.getDescribe().getPicklistValues () ;
        List<String> declineReasons = new List<String>();
        for (PicklistEntry entry: declineReasonEntries) {
            declineReasons.add(entry.getValue());
        }
		return declineReasons;
    }

    @AuraEnabled
    public static one_application__c getApplication ( ID appID )
    {
        one_application__c app = one_utils.getApplicationFromId ( appID ) ;
        
        return app ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Should Override/Decline be displayed - Pre-Bureau
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static boolean isPreBureauOverride ( one_application__c app )
    {
        boolean result = false;

        if ((app.FIS_Decision_Code__c == null || !app.FIS_Decision_Code__c.startsWith('D')) &&
            app.Pre_Bureau_Disposition__c == 'Warn' &&
            app.Pre_Bureau_Override_Datetime__c == null &&
            app.Pre_Bureau_Override_User__c == null) {

			result = true;
        }
        
        return result ;
    }

    /*  --------------------------------------------------------------------------------------------------
     *  Override - Pre-Bureau
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void overrideApplicationPreBureau ( one_application__c app, String overrideReason )
    {
        LOSHelper lh = new LOSHelper();
        lh.setApplication(app);
        boolean success = lh.overridePreBureau(overrideReason);
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline Pre-Bureau
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationPreBureau ( one_application__c app, String declineReasons )
    {
        LOSHelper lh = new LOSHelper();
        lh.setApplication(app);
        boolean success = lh.declinePreBureau(declineReasons);
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Should Override/Decline be displayed - Post-Bureau
	 *  -------------------------------------------------------------------------------------------------- */	
	@AuraEnabled
    public static Decimal isPostBureauOverride ( one_application__c app )
    {
        Decimal result = -1.0 ;
        
        if ((app.FIS_Decision_Code__c == null || !app.FIS_Decision_Code__c.startsWith('D')) &&
            app.Post_Bureau_Disposition__c == 'Warn' &&
            app.Post_Bureau_Override_Datetime__c == null &&
            app.Post_Bureau_Override_User__c == null) {

            user u = [ SELECT ID, UNITY_Visa_Credit_LImit__c FROM User WHERE ID=:UserInfo.getUserId () ] ;
            
            Decimal creditLimit = 0 ;
            
            if ( ( u != null ) && ( u.UNITY_Visa_Credit_LImit__c != null ) && ( u.UNITY_Visa_Credit_LImit__c > 0.0 ) )
                creditLimit = u.UNITY_Visa_Credit_LImit__c ;
            
            result = creditLimit ;
        }

        return result ;
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Override - Post-Bureau
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void overrideApplicationPostBureau ( one_application__c app, String overrideReason, Decimal creditLimit )
    {
        LOSHelper lh = new LOSHelper();
        lh.setApplication(app);
        boolean success = lh.overridePostBureau(overrideReason, creditlimit);
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Decline Post-Bureau
     *  -------------------------------------------------------------------------------------------------- */
    @AuraEnabled
    public static void declineApplicationPostBureau ( one_application__c app, String declineReasons )
    {
        LOSHelper lh = new LOSHelper();
        lh.setApplication(app);
        boolean success = lh.declinePostBureau(declineReasons);
    }
}