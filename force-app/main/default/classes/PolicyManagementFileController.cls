public with sharing class PolicyManagementFileController 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public String isAdministrator
 	{ 
 		get; 
 		private set; 
 	}
 	
 	public String url
 	{
 		get ;
 		private set ;
 	}
 	
 	public Integer mode
 	{
 		get ;
 		private set ;
 	}
 	
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public PolicyManagementFileController ( ApexPages.Standardcontroller stdC )
	{
		//  Get the object from the page
		Policy_Management__c pm = (Policy_Management__c) stdC.getRecord () ;
		
		//  Set the defaults
		isAdministrator = 'N' ;
		url = 'https://portal.oneunited.local/intranet/Policy_Management/policyFiles.asp' ;
		mode = 999 ;
		
		//  Permission set to check against
		List<PermissionSetAssignment> psaL = null ;
		
		//  Check for ID
		if ( pm.Policy_ID__c == null )
		{
			mode = 500 ;
		}
		else
		{
			//  Check for administrator permission
			if ( Test.isRunningTest () )
			{
				psaL = [
					SELECT PermissionSetId 
					FROM PermissionSetAssignment 
					WHERE AssigneeId= :UserInfo.getUserId () 
					AND PermissionSet.Name = 'PM_TEST_ADMIN'
				] ;
			}
			else
			{
				psaL = [
					SELECT PermissionSetId 
					FROM PermissionSetAssignment 
					WHERE AssigneeId= :UserInfo.getUserId () 
					AND PermissionSet.Name = 'Policy_Management_Full_Access'
				] ;
			}
			
			if ( ( psaL != null ) && ( psaL.size () > 0 ) )
			{
				isAdministrator = 'Y' ;
				mode = 0 ;
			}
			else
			{
				//  Check for read only permission
				if ( Test.isRunningTest () )
				{
					psaL = [
						SELECT PermissionSetId 
						FROM PermissionSetAssignment 
						WHERE AssigneeId= :UserInfo.getUserId () 
						AND PermissionSet.Name = 'PM_TEST_NORMAL'
					] ;
				}
				else
				{
					psaL = [
						SELECT PermissionSetId 
						FROM PermissionSetAssignment 
						WHERE AssigneeId= :UserInfo.getUserId () 
						AND PermissionSet.Name = 'Policy_Management_Basic_Access'
					] ;
				}
					
				if ( ( psaL != null ) && ( psaL.size () > 0 ) )
				{
					isAdministrator = 'N' ;
					mode = 0 ;
				}
			}
		}		
	}
}