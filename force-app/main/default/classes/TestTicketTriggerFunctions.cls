@isTest
public class TestTicketTriggerFunctions 
{
	public static testmethod void testRecordTypeList ()
	{
		//  Random list of pre-exsiting record types
		List<String> rtNames = new List<String> () ;
		
		rtNames.add ( 'Prospect' ) ;
		rtNames.add ( 'Community Room' ) ;
		rtNames.add ( 'Social Media Alert' ) ;
		
		Test.startTest () ;

        //  Look for (lack of) double DML in debug output!
		List<RecordType> rtL1 = TicketTriggerFunctions.getRecordTypeList ( rtNames ) ;
		List<RecordType> rtL2 = TicketTriggerFunctions.getRecordTypeList ( rtNames ) ;
		
		Test.stopTest () ;
	}
	
	public static testmethod void testGroupMap ()
	{
		//  Random list of pre-existing groups
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Community Room Request' ) ;
		groupList.add ( 'Social Media Alert' ) ;
		groupList.add ( 'Credit Card Refund Processing' ) ;
		
		Test.startTest () ;

        //  Look for (lack of) double DML in debug output!
		Map<String,String> groupMap1 = TicketTriggerFunctions.getGroupMap ( groupList ) ; 
		Map<String,String> groupMap2 = TicketTriggerFunctions.getGroupMap ( groupList ) ; 
		
		Test.stopTest () ;
	}
    
	private static User getUser ( String thingy )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
        u.Department = 'test department' ;
		
		return u ;
	}
    
    private static void addUserToGroup ( User u, group g )
    {
        GroupMember gm = new GroupMember () ; 
        gm.GroupId = g.id ;
        gm.UserOrGroupId = u.id ;
        
        insert gm ;
    }
	
    public static testmethod void testUsersGroups ()
    {
        //  Initial objects
        User u1 = getUser ( '1' ) ;
        insert u1 ;
        
        User u2 =  getUser ( '2' ) ;
        insert u2 ;
        
    	// Group
        Group g = new Group () ;
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        //  Members
        addUserToGroup ( u1, g ) ;
        addUserToGroup ( u2, g ) ;
         
		Test.startTest () ;
        
        //  Setup 
        TicketTriggerFunctions.uLMap = new map<String,List<String>> () ;
        TicketTriggerFunctions.esMapMap = new map<String,Map<String, String>> () ;
        
        List<String> uL1 = TicketTriggerFunctions.getUserIdsFromGroupId ( g.ID ) ; 
        
        System.assertEquals ( uL1.size (), 2 ) ;
        
        List<String> uL2 = TicketTriggerFunctions.getUserIdsFromGroupId ( g.ID ) ; 
        
        System.assertEquals ( uL2.size (), 2 ) ;
        
        Test.stopTest () ;
    }
    
    public static testmethod void testSubscribers ()
    {
        //  Initial objects
        User u1 = getUser ( '1' ) ;
        insert u1 ;
        
		Contact c = new Contact () ;
		c.FirstName = 'TestC' ;
		c.LastName = 'UserC' ;
		c.Email = 'testC@userC.com' ;
		
		insert c ;
		
		EntitySubscription es = new EntitySubscription () ;
		es.ParentId = c.Id ;
		es.SubscriberId = u1.Id ;
		
		insert es ;
		
		Test.startTest () ;
        
        //  Setup 
        TicketTriggerFunctions.uLMap = new map<String,List<String>> () ;
        TicketTriggerFunctions.esMapMap = new map<String,Map<String, String>> () ;
        
        Map<String,String> subList1 = TicketTriggerFunctions.getCurrentSubscribers ( c.ID ) ; 
        
		System.assertNotEquals ( subList1, null ) ;
		System.assertEquals ( subList1.containsKey ( u1.Id ), true ) ;
                
        Map<String,String> subList2 = TicketTriggerFunctions.getCurrentSubscribers ( c.ID ) ; 
        
		System.assertNotEquals ( subList2, null ) ;
		System.assertEquals ( subList2.containsKey ( u1.Id ), true ) ;
                
        Test.stopTest () ;
    }
}