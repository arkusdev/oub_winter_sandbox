@isTest
public class TestIPRiskHelper 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
	
    public static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group () ;
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c ();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Referral_Tracking_Hold_Days__c = 7 ;
        s.Rakuten_Tracking_ID__c = 'TESTID' ;
        s.Rates_URL__c = 'https://www.test.com' ;
        
        insert s;
    }
    
	/*
	 * Creates a generic application I use for testing.
	 */
	private static one_application__c getApplicationForTests ()
	{
		//  Create dummy one_application__c for reference
		Contact c1 = new Contact();
		c1.FirstName = 'c1';
		c1.LastName = 'c1';
		c1.Email = 'hey1@emailhey.com';
		insert c1;
		
		Contact c2 = new Contact();
		c2.FirstName = 'c2';
		c2.LastName = 'c2';
		c2.Email = 'hey2@emailhey.com';
		insert c2;
		
		
		one_application__c app = new one_application__c ();
		app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		app.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Unity Visa', 'one_application__c' ) ;

        app.Visitor_IP__c = '1.2.3.4' ;
        app.Visitor_Accept_Language__c = 'Yah' ;
        app.Visitor_User_Agent__c = 'Vol' ;
                
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Gross_Income_Monthly__c = 10000.00 ;
        app.Phone__c = '123-456-7890' ;
        
        app.Street_Address__c = '100 Franklin Street' ;
        app.Street_Address_2__c = 'Suite 600' ;
        app.City__c = 'Boston' ;
        app.State__c = 'MA' ;
        app.ZIP_Code__c = '02210' ;
        
        app.Mailing_Address_Same__c = 'No' ;
        
        app.Mailing_Street_Address__c = '3683 Crenshaw Blvd' ;
        app.Mailing_Street_Address_2__c = '5th floor' ;
        app.Mailing_City__c = 'Los Angles' ;
        app.Mailing_State__c = 'CA' ;
        app.Mailing_ZIP_Code__c = '90016' ;
		
		app.Co_Applicant_Contact__c = c2.Id;
		app.Co_Applicant_First_Name__c = c2.FirstName ;
		app.Co_Applicant_Last_Name__c = c2.LastName ;
		app.Co_Applicant_Email_Address__c = c2.Email ;
        app.Co_Applicant_Phone__c = '123-456-7890' ;
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.FIS_Application_ID__c = '111222333' ;
		app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.FIS_Decision_Code__c = 'F003' ;
		
		app.Product__c = 'VSC' ;
		app.SubProduct__c = '000' ;
		app.Plan__c = '000' ;
        
        app.Funding_Options__c = 'Debit Card' ;
		
		return app ;
	}

    public static testmethod void ONE ()
    {
		insertCustomSetting () ;
        
		one_application__c app = getApplicationForTests () ;
        
        IP_Risk_Helper irh1 = new IP_Risk_Helper ( app ) ;
        irh1.getRiskScore () ;
        
        app.minFraud_ID__c = '123456' ;
        insert app ;
        
        Application_Tag__c at = new Application_Tag__c () ;
        at.Application__c = app.ID ;
        at.Tag_Type__c = 'Chargeback' ;
        insert at ;
        
        IP_Risk_Helper irh2 = new IP_Risk_Helper ( at ) ;
        irh2.reportChargeback () ;
    }
    
    public static testmethod void TWO ()
    {
		insertCustomSetting () ;
        
		one_application__c app = getApplicationForTests () ;
        
        app.Billing_Address__c = 'Other' ;
        app.Billing_Street_Address__c = '1311 Mocking Bird Lane' ;
        app.Billing_Street_Address_2__c = '13th Floor' ;
        app.Billing_City__c = 'New York' ;
        app.Billing_State__c = 'NY' ;
        app.Billing_Zip_Code__c = '10013' ;
        
        IP_Risk_Helper irh1 = new IP_Risk_Helper ( app ) ;
        irh1.getRiskScore () ;
        
        app.minFraud_ID__c = '123456' ;
        insert app ;
    }
}