@isTest
public class test_card_stock_order_v2 {
    
    // SETUP
    private static User setupUser() {
        Profile testProfile = [SELECT Id 
                           FROM profile
                           WHERE Name = 'OneUnited Advocate Identity User' 
                           LIMIT 1];
        
        Account a = new Account(Name='Test Account');
        insert a;
        
		Contact c = new Contact();
        c.Email = 'test1@ouuser.com';
        c.FirstName = 'helloabc123';
        c.LastName = 'UserAdvocateProgramDisplayController';
        c.Birthdate = Date.newInstance(1990, 12, 12);
        c.AccountId = a.Id;
        c.TaxID__c = '111223333';
        c.MailingStreet = '100 franklin st';
        c.MailingCity = 'boston';
        c.MailingState = 'ma';
        c.MailingPostalCode = '02110';
        
        insert c;
        
    	User testUser = new User(
            FirstName = 'Test1',
            LastName = 'UserAdvocateProgramDisplayController',
            Email = 'test1@ouuser.com',
            Username = 'test1user1@ouuser.com',
            Alias = 'dsplctrl',
            CommunityNickname = 'tuser1',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = testProfile.id,
            LanguageLocaleKey = 'en_US',
            IsActive=true,
            ContactId=c.Id ); 
        
        return testUser;
    }
    
    private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
    
    @isTest static void testGetCards() {
        Test.startTest();
        
        String debitCards = card_stock_order_v2.getCards('Debit Card');
        String atmCards = card_stock_order_v2.getCards('ATM Card');
        
        System.assertEquals( debitCards != null , true );
        System.assertEquals( atmCards != null , true );
        
        Test.stopTest();
    }
    
    @isTest static void testSubmitTicket() {
        User testUser = setupUser();
        
        Service__c s = new Service__c();
        s.RecordTypeId = Schema.SObjectType.Service__c.RecordTypeInfosByDeveloperName.get('Agreement').RecordTypeId;
        s.Status__c = 'Active';
        s.Type__c = 'ATM Card';
        s.Card_Number__c = 'XXXXXXXXXXXXXXX4444';
        s.Contact__c = testUser.contactId;
        insert s;
        
        Map<String, Object> info = new Map<String, Object>();
        info.put('last4Digits', '4444');
        info.put('cardType', 'ATM');
        info.put('plastic', 'Lady');
        
        Test.startTest();
        
        String confirmNum = card_stock_order_v2.submitTicket(info);
        
        System.assertEquals( confirmNum, '' );
        
        System.runAs(testUser)
        {
            confirmNum = card_stock_order_v2.submitTicket(info);
            System.assertEquals( ( confirmNum != null ) && ( confirmNum != '' ) && ( confirmNum != 'Invalid last 4 digits' ) , true );
        }
        
        Test.stopTest();
        
    }
    
    @isTest static void testGetCurrentUserContactId() {
        User testUser = setupUser();
        
        Test.startTest();
        
        String userContactId = card_stock_order_v2.getCurrentUserContactId();
        
        System.assertEquals( userContactId, null );
        
        System.runAs(testUser)
        {
            userContactId = card_stock_order_v2.getCurrentUserContactId();
            System.assertEquals( userContactId, testUser.contactId );
        }
        
        Test.stopTest();
    }
    
    @isTest static void testGetContactById() {
        Contact c = getContact(1);
        insert c;
        
        Test.startTest();
        
        Contact c2 = card_stock_order_v2.getContactById('123');
        Contact c3 = card_stock_order_v2.getContactById(c.Id);
        
        System.assertEquals( c2, null );
        System.assertEquals( c3.Id, c.Id );
        
        Test.stopTest();
    }
    
    @isTest static void testGetServices() {
        User testUser = setupUser();
        
        Test.startTest();
        
        String result = card_stock_order_v2.getServices('ATM Card');
        
        System.assertEquals( result, '[]' );
        
        System.runAs(testUser)
        {
            result = card_stock_order_v2.getServices('ATM Card');
            System.assertEquals( result != null, true );
        }
        
        Test.stopTest();
    }
}