public with sharing class Community_Room_Registration 
{
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
	//  Step control for panels on page
	public Integer cmStepNumber
	{
		get ;
		private set ;
	}

	//  Ticket object to populate
	public Ticket__c ticket
	{
		get 
		{
			if ( ticket == null )
				ticket = new Ticket__c () ;

			return ticket ;
		}
		
		set ;
	}
	
	//  Confirm of email
	public String confirmEmail
	{
		get ;
		set ;
	}
	
	//  Date for dropdown since inputfield doesn't work with calendar
	public String pickDate
	{
		get ;
		set ;
	}
	
	//  Error checking
	public Map<String,String> errorMap
	{
		get ;
		private set ;
	}
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public Community_Room_Registration ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		cmStepNumber = 1000 ;
		
		//  ??
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Validate & create the ticket
     *  ---------------------------------------------------------------------------------------- */
	public PageReference submitTicket () 
	{
		//  Check for validation, setup
		errorMap = new Map<String, String> () ;
		
		//  Validation
		//  We dump blanks in if no error found so the page doesn't crash and burn
		if ( String.isBlank ( ticket.Name_of_Organization__c ) )
			errorMap.put ( 'Name_of_Organization__c' , 'Please enter the name of your organization' ) ;
		else
			errorMap.put ('Name_of_Organization__c', '' ) ;
		
		if ( String.isBlank ( ticket.Street_Address__c ) )
			errorMap.put ( 'Street_Address__c' , 'Please enter your street' ) ;
		else
			errorMap.put ( 'Street_Address__c', '' ) ;
			
		if ( String.isBlank ( ticket.City__c ) )
			errorMap.put ( 'City__c' , 'Please enter your city' ) ;
		else
			errorMap.put ( 'City__c', '' ) ;
		
		if ( String.isBlank ( ticket.State__c ) )
			errorMap.put ( 'State__c' , 'Please enter your state' ) ;
		else
			errorMap.put ( 'State__c', '' ) ;
		
		if ( String.isBlank ( ticket.Zip_Code__c ) )
			errorMap.put ( 'Zip_Code__c' , 'Please enter your zip code' ) ;
		else if ( ! OneUnitedUtilities.isValidZipCode ( ticket.Zip_Code__c ) ) 
			errorMap.put ( 'Zip_Code__c' , 'Please enter a valid zip code' ) ;
		else
		{
			ticket.Zip_Code__c = OneUnitedUtilities.cleanZipCode ( ticket.Zip_Code__c ) ;
			errorMap.put ( 'Zip_Code__c', '' ) ;
		}
		
		if ( String.isBlank ( ticket.First_Name__c ) )
			errorMap.put ( 'First_Name__c' , 'Please enter your first name' ) ;
		else
			errorMap.put ( 'First_Name__c', '' ) ;
		
		if ( String.isBlank ( ticket.Last_Name__c ) )
			errorMap.put ( 'Last_Name__c' , 'Please enter your last name' ) ;
		else
			errorMap.put ( 'Last_Name__c', '' ) ;
				
		if ( String.isBlank ( ticket.Phone__c ) )
			errorMap.put ( 'Phone__c' , 'Please enter your phone number' ) ;
		else if ( ! OneUnitedUtilities.isValidPhoneNumber ( ticket.Phone__c ) )
			errorMap.put ( 'Phone__c' , 'Please enter a valid phone number' ) ;
		else
		{
			ticket.Phone__c = OneUnitedUtilities.cleanPhoneNumber ( ticket.Phone__c ) ;
			errorMap.put ( 'Phone__c', '' ) ;
		}
		
		if ( ( String.isNotBlank ( ticket.Email_Address__c ) ) && ( ! OneUnitedUtilities.isValidEmail ( ticket.Email_Address__c ) ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter a valid email address' ) ;
		else
			errorMap.put ( 'Email_Address__c', '' ) ;
		
		if ( String.isNotBlank ( ticket.Email_Address__c ) )
		{
			if ( ( String.isNotBlank ( confirmEmail ) ) && ( ticket.Email_Address__c.equalsIgnoreCase ( confirmEmail ) ) )
				errorMap.put ( 'confirmEmail', '' ) ;
			else
				errorMap.put ( 'confirmEmail' , 'Please confirm your email address' ) ;
		}
		else if ( String.isNotBlank ( confirmEmail ) )
		{
			errorMap.put ( 'confirmEmail' , 'Please enter an email address to confirm' ) ;
		}
		else
		{
			errorMap.put ( 'confirmEmail', '' ) ;
		}

		//  Attempt to convert date from string... catch exception to prevent kablooey on page
		Date pDate = null ;
		try
		{
			if ( pickDate != null )
				pDate = Date.parse ( pickDate ) ;
		}
		catch ( TypeException te )
		{
			pDate = null ;	
		}
		
		if ( pDate == null )
			errorMap.put ( 'Date_Requested__c' , 'Please enter the date requested' ) ;
		else if ( pDate < Date.today () )
			errorMap.put ( 'Date_Requested__c' , 'Only dates after today are bookable' ) ;
		else if ( OneUnitedUtilities.getDayOfWeek ( pDate ) == 0 )
			errorMap.put ( 'Date_Requested__c' , 'The Community room is not available on Sunday' ) ;
		else
		{
			ticket.Date_Requested__c = pDate ;
			errorMap.put ( 'Date_Requested__c', '' ) ;
		}

		if ( ticket.Beginning_Time__c == null )
			errorMap.put ( 'Beginning_Time__c' , 'Please enter the beginning time' ) ;
		else
			errorMap.put ( 'Beginning_Time__c', '' ) ;

		if ( ticket.Ending_Time__c == null )
			errorMap.put ( 'Ending_Time__c' , 'Please enter the ending time' ) ;
		else if ( formTimeConvert ( ticket.Ending_Time__c ) <= formTimeConvert ( ticket.Beginning_Time__c ) )
			errorMap.put ( 'Ending_Time__c' , 'The ending time of the event must be after the starting time' ) ;
		else
			errorMap.put ( 'Ending_Time__c', '' ) ;

		if ( String.isBlank ( ticket.Purpose_of_Meeting__c ) )
			errorMap.put ( 'Purpose_of_Meeting__c' , 'Please enter the purpose of the meeting' ) ;
		else
			errorMap.put ( 'Purpose_of_Meeting__c', '' ) ;

		if ( ticket.Number_of_People_Expected__c == null )
			errorMap.put ( 'Number_of_People_Expected__c' , 'Please enter the number of people expected' ) ;
		else
			errorMap.put ( 'Number_of_People_Expected__c', '' ) ;

		//  Validation start
		boolean isValid = true ;
		
		//  Non blank entry means an error occurred
		for ( String key : errorMap.keySet () )
		{
			if ( String.isNotEmpty ( errorMap.get ( key ) ) )
				isValid = false ;
		}
		
		//  Logic based on validation
		if ( isValid )
		{
			cmStepNumber = 2000 ;
			
			ticket.recordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Ticket__c' ) ;
			ticket.Status__c = 'New' ;
			
			//insert ticket ;
            access.insertObject(ticket);
            
		}
		else
		{
			cmStepNumber = 1001 ;
		}
		
		// Does nothing
		return null ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Convert time picklist string to time object so we can compare
     *  ---------------------------------------------------------------------------------------- */
	private Time formTimeConvert ( String thingy )
	{
		String x = thingy.replace ( ':', ' ' ) ;
		List<String> xL = x.split ( ' ' ) ;
		
		Integer hour = Integer.valueOf ( xL [ 0 ] ) ;
		Integer minute = Integer.valueOf ( xL [ 1 ] ) ;
		String ampm = xL [ 2 ] ;
		String tz = xL [ 3 ] ; // Not used
		
		if ( ampm.equalsIgnoreCase ( 'PM' )  )
			hour += 12 ;
			
		Time t = Time.newInstance ( hour, minute, 0, 0 ) ;
		
		return t ;
	}
}