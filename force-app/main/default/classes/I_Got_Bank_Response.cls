public with sharing class I_Got_Bank_Response 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security
     *  ---------------------------------------------------------------------------------------- */
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Settings
     *  ---------------------------------------------------------------------------------------- */
    private Web_Settings__c ws ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Display objects
     *  ---------------------------------------------------------------------------------------- */
    public String iGotBankBanner { public get ; private set ; }

    public Date contestEndDate { public get ; private set ; }
    public String contestEndDateFormat { public get ; private set ; }

    private Date contestWinnerDate { private get ; private set ; }
    public String contestWinnerDateFormat { public get ; private set ; }
    
    public String contestWinnerNumber { public get ; private set ; }
    public String contestWinnerAmount { public get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Form objects
     *  ---------------------------------------------------------------------------------------- */
 	//  Ticket object to upload
	public Ticket__c ticket 
	{
		public get ;	
		private set ;
	}
     
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
 
	//  Step control for panels on page
	public Integer dStepNumber { get ; private set ; }

    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public I_Got_Bank_Response ( ApexPages.Standardcontroller stdC )
	{
        //  incoming IDs
        String aid = null ;
        String kid = null ;
        
		//  Controls which panel we display, default of 2000 - No Contest!
		dStepNumber = 2000 ;
        
        //  Load settings
        ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;
        
        //  Active contest?
        if ( ws != null )
        {
            contestEndDate = ws.I_Got_Bank_Contest_End_Date__c ;
            
            if ( ( contestEndDate != null ) && ( contestEndDate >= Date.today () ) )
                dStepNumber = 1000 ;
            
            if ( ws.I_Got_Bank_Banner__c != null )
                iGotBankBanner = ws.I_Got_Bank_Banner__c ;
            
            if ( ws.I_Got_Bank_Contest_Winner_Date__c != null )
                contestWinnerDate = ws.I_Got_Bank_Contest_Winner_Date__c ;
            else
                contestWinnerDate = contestEndDate.addDays ( 90 ) ;
            
            if ( contestWinnerDate <= contestEndDate )
                contestWinnerDate = contestEndDate.addDays ( 90 ) ;
            
            Datetime dte = contestEndDate.addDays ( 1 ) ;
            contestEndDateFormat = dte.format ( 'M/d/yyyy' ) ;
            
            Datetime dtw = contestWinnerDate.addDays ( 1 ) ;
            contestWinnerDateFormat = dtw.format ( 'M/d/yyyy' ) ;
            
            if ( String.isNotBlank ( ws.I_Got_Bank_Contest_Winner_Number__c ) )
                contestWinnerNumber = ws.I_Got_Bank_Contest_Winner_Number__c ;
            else
                contestWinnerNumber = 'Three (3)' ;
            
            if ( String.isNotBlank ( ws.I_Got_Bank_Contest_Winner_Award__c ) )
                contestWinnerAmount = ws.I_Got_Bank_Contest_Winner_Award__c ;
            else
                contestWinnerAmount = '$1,000' ;
        }
        
        if ( dStepNumber == 1000 )
        {
            //  First check for ticket in session
            ticket = (Ticket__c) stdC.getRecord () ;
            
            //  If no session present, use querystring instead
            if ( ( ticket == null ) || ( ticket.Id == null ) )
            {
                System.debug ( '========== No session info found ==========' ) ;
                
                aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
                kid = ApexPages.currentPage().getParameters().get('kid') ;  // Randomly generated security key
                
                ticket = null ;
    
                //  Both need to be passed in
                if ( ( aid != null ) && ( kid != null ) )
                {
                    //  Return list
                    list<Ticket__c> tL = null ;
                    
                    try
                    {
                        tL = [
                            SELECT ID, Name, Status__c, Youth_Artwork_Essay_Choice__c
                            FROM Ticket__c
                            WHERE ID = :aid
                            AND Security_Key__c = :kid
                            AND RecordType.DeveloperName = 'I_Got_Bank'
                            LIMIT 1
                        ] ;
                    }
                    catch ( DMLException e )
                    {
                        tL = null ;
                    }
                    
                    if ( ( tL != null ) && ( tL.size () > 0 ) )
                    {
                        //  Grab the first one
                        ticket = tL [ 0 ] ;
                    }
                    else
                    {
                        System.debug ( '========== No Data found for ID ==========' ) ;
                        dStepNumber = 3000 ;
                    }
                }
                else
                {
                    System.debug ( '========== No Data found ==========' ) ;
                    dStepNumber = 3000 ;
                }
            }
            
            if ( ticket != null )
            {
                if ( String.isEmpty ( ticket.Status__c ) )
                {
                    dStepNumber = 3000 ;
                }
                else 
                {
                    if ( ticket.Youth_Artwork_Essay_Choice__c.equalsIgnoreCase ( 'Artwork' ) )
                    {
                        if ( ticket.Status__c.equalsIgnoreCase ( 'Received' ) )
                        {
                            //  Good to go
                            dStepNumber = 1000 ;
                        }
                        else
                        {
                            //  Already done?
                            dStepNumber = 1500 ;
                        }
                    }
                    else
                    {
						// Essay?
                        dStepNumber = 1600 ;
                    }
                }
            }
        }
    }

    /*  ----------------------------------------------------------------------------------------
     *  Validate entry
     *  ---------------------------------------------------------------------------------------- */
	public PageReference iGotBankUpload () 
	{
        //  Defaults
		boolean isValid = true ;
		errorName = false ;
		errorFile = false ;
		
		//  Start with validation
		if ( String.isBlank ( attachment.name ) )
		{
			errorName = true ;
			isValid = false ;
		}
        else if ( ( attachment.name.endswithIgnoreCase ( 'exe' ) ) ||
                  ( attachment.name.endswithIgnoreCase ( 'msi' ) ) ||
                  ( attachment.name.endswithIgnoreCase ( 'dll' ) ) ||
                  ( attachment.name.endswithIgnoreCase ( 'bat' ) ) ||
                  ( attachment.name.endswithIgnoreCase ( 'zip' ) ) ||
                  ( attachment.name.endswithIgnoreCase ( 'gz' ) ) )
        {
            errorName = true ;
            isvalid = false ;
        }
		
		if ( attachment.body == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        dStepNumber = 1001 ;
			return null ;
		}
		
		attachment.OwnerId = UserInfo.getUserId () ;
		attachment.ParentId = ticket.Id ;    // the record the file is attached to
		attachment.IsPrivate = false ;  // Or the guest user can't query it!
        
		try 
		{
	        System.debug ( '========== Inserting Attachment ==========' ) ;
			access.insertObject ( attachment ) ;
			
	        System.debug ( '========== Flipping ticket status ==========' ) ;
            ticket.Status__c = 'Responded' ;
            
	        access.updateObject ( ticket ) ;
		}
		catch (DMLException e)
		{
			System.debug ( '========== Error Inserting! ==========' ) ;

			return null;
		}
		finally 
		{
	        System.debug ( '========== Final stuff ==========' ) ;
	        
	        //  Set panel to success upload
			dStepNumber = 1100 ;
        }
        
        //  Done
        System.debug ( '========== Returning! ==========' ) ;
        return null ;
    }
}