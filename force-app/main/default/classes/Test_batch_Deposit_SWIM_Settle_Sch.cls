@isTest
public class Test_batch_Deposit_SWIM_Settle_Sch 
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	static testmethod void something ()
	{
		Test.startTest();

		batch_Deposit_SWIM_Settlement_Sch myClass = new batch_Deposit_SWIM_Settlement_Sch ();   
		String chron = '0 0 23 * * ?';        
		system.schedule('Test Sched', chron, myClass);
		
		Test.stopTest();
	}
}