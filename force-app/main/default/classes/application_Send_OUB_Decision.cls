public with sharing class application_Send_OUB_Decision 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Input
     *  -------------------------------------------------------------------------------------------------- */
    public String overrideReason { public get ; public set ; }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Error?!?
     *  -------------------------------------------------------------------------------------------------- */
    public String overrideReasonError { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Form & Button display
     *  -------------------------------------------------------------------------------------------------- */
    public boolean canOverride { public get ; private set ; }
    public boolean overrideButton { public get ; private set ; }
    public boolean declineButton { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Application
     *  -------------------------------------------------------------------------------------------------- */
    private one_application__c app;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public application_Send_OUB_Decision ( ApexPages.StandardController controller )
    {
        //  DEFAULTS
        overrideReason = 'Passed OUB' ;
        overrideReasonError = null ;
        
        //  Only one override possible since there's only one IP address.
        canOverride = false ;
        overrideButton = false ;
        declineButton = false ;
        
        //  Get (possibly incomplete) application 
        this.app = (one_application__c) controller.getRecord () ;
        
        //  Reload application
        this.app = one_utils.getApplicationFromId ( app.Id ) ;
        
        //  Have they been overridden?
        if ( ( String.isNotBlank ( app.OUB_Fraud_Disposition__c ) ) &&
            ( ( app.OUB_Fraud_Disposition__c.equalsIgnoreCase ( 'WARN' ) ) || ( app.OUB_Fraud_Disposition__c.equalsIgnoreCase ( 'NONE' ) ) ) &&
            ( app.OUB_Fraud_Override_Datetime__c == null ) &&
            ( app.OUB_Fraud_Override_User__c == null ) )
        {
            canOverride = true ;
	        overrideButton = true ;
            declineButton = true ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for applicant
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runOverride ()
    {
        overrideReasonError = null ;
        
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideOUBVelocity ( overrideReason ) ;
            
            //  Get the current source
            app = fish.getApplication () ;
            
            System.debug ( 'Applicant Post-Override Code :: ' + app.FIS_Decision_Code__c ) ; 

            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Decline
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference declineApplication ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        try
        {
            //  Replace to ... something else?
            String newCode = app.FIS_Decision_Code__c.replace ( 'D', 'F' ) ;
            
            app.FIS_Decision_Code__c = newCode ;
            System.debug('Update app debugging 4:');
            update app ;
        }
        catch ( Exception e )
        {
            System.debug ( 'WTF?!?' ) ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Cancel ?!?
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference cancel ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        return pr ;
    }
}