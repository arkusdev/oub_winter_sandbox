public class BBPaydayPushQueue implements Queueable, Database.AllowsCallouts {
    
    private List<Automated_Email__c> pushNotifications;
    
    public BBPaydayPushQueue(List<Automated_Email__c> pushNotifications) {
        this.pushNotifications = pushNotifications;
    }
    
    public void execute(QueueableContext context) {
        
        // 	S	E	T	U	P -- still needs to be addressed
        // thru API call? but only needs to be done once -- create segment that targets users with custom attribute
        // thru portal -- create campaign & assign it to this specific segment
        
        List<String> aeIds=new List<String>();
        
        for(Automated_Email__c aeT : pushNotifications)
        {
            aeIds.add(aeT.Id);
        }
        
        this.pushNotifications = [SELECT Contact_GUUID__c,Last_Name__c,Email_Sent__c,Callout_Details__c,Status__c,
                                         		Template_Name__c,CreatedDate,
                                         		Merge_Var_Name_1__c,Merge_Var_Value_1__c,
                                         		Merge_Var_Name_2__c,Merge_Var_Value_2__c,
		                                        Merge_Var_Name_3__c,Merge_Var_Value_3__c,
                                         		Merge_Var_Name_4__c,Merge_Var_Value_4__c,
                                         		Merge_Var_Name_5__c,Merge_Var_Value_5__c
                                         FROM Automated_Email__c 
										 WHERE Id IN :aeIds];
        
        
        PSPHelper psph = new PSPHelper();
        Integer successCount = 0;
        Integer failCount = 0;
        
        for ( Automated_Email__c ae : pushNotifications )
        {
            if ( String.isNotBlank(ae.Contact_GUUID__c) )
            {
                // update each user to include merge vars; these are all considered custom attributes to Pulsate PSP
                String customJSON = '"custom_attributes":[';
                    
                
                if (String.isNotBlank( ae.Merge_Var_Name_1__c )) {
                    customJSON=customJSON+'{' +
                        '"name":"' + ae.Merge_Var_Name_1__c + '",' +
                        '"value":"' + ae.Merge_Var_Value_1__c + '",' +
                        '"type":"String",' +
                        '"action":"Update"' +
                        '},';
                }
                
                if (String.isNotBlank( ae.Merge_Var_Name_2__c )) {
                    customJSON=customJSON+'{' +
                        '"name":"' + ae.Merge_Var_Name_2__c + '",' +
                        '"value":"' + ae.Merge_Var_Value_2__c + '",' +
                        '"type":"String",' +
                        '"action":"Update"' +
                        '},';
                }
                
                if (String.isNotBlank( ae.Merge_Var_Name_3__c )) {
                    customJSON=customJSON+'{' +
                        '"name":"' + ae.Merge_Var_Name_3__c + '",' +
                        '"value":"' + ae.Merge_Var_Value_3__c + '",' +
                        '"type":"String",' +
                        '"action":"Update"' +
                        '},';
                }
                
                if (String.isNotBlank( ae.Merge_Var_Name_4__c )) {
                    customJSON=customJSON+'{' +
                        '"name":"' + ae.Merge_Var_Name_4__c + '",' +
                        '"value":"' + ae.Merge_Var_Value_4__c + '",' +
                        '"type":"String",' +
                        '"action":"Update"' +
                        '},';
                }
                
                if (String.isNotBlank( ae.Merge_Var_Name_5__c )) {
                    customJSON=customJSON+'{' +
                        '"name":"' + ae.Merge_Var_Name_5__c + '",' +
                        '"value":"' + ae.Merge_Var_Value_5__c + '",' +
                        '"type":"String",' +
                        '"action":"Update"' +
                        '},';
                }
                
                customJSON=customJSON+'{' +
                    '"name":"lastName",' +
                    '"value":"' + ae.Last_Name__c + '",' +
                    '"type":"String",' +
                    '"action":"Update"' +
                    '}';
                
                //End custom_attributes
                customJSON=customJSON+']';
                
                psph.updateSingleUser( ae.Contact_GUUID__c , customJSON );
                
                // idk test class cannot associate mockhttp because psphelper is being instantiated within this class....
                if ( Test.isRunningTest() )
                {
                    System.debug('running test --- overwriting necessary variable values');
                    psph.statusCode = 200;
                    psph.response = 'fake_response';
                }
                
                if ( psph.statusCode == 200 )
                {
                	psph.resetVariables();
                    
                    psph.createSingleEvent(ae.Contact_GUUID__c,ae.Template_Name__c,ae.CreatedDate);
					
                    if ( Test.isRunningTest() )
                    {
                        System.debug('running test --- overwriting necessary variable values');
                        psph.statusCode = 200;
                        psph.response = 'fake_response';
                    }
                    
                    if ( psph.statusCode == 200 )
                    {
                        ae.Email_Sent__c = true;
                        ae.Status__c = 'Completed';
                        successCount ++;
                    } else
                    {
                        failCount ++;
                        
                        ae.Callout_Details__c = psph.getCalloutErrorDetails();
    
                    }
                    
                }
                else
                {
                    failCount ++;
                    ae.Callout_Details__c = psph.getCalloutErrorDetails();
                }
                
                psph.resetVariables();
            }
        }
        
        System.debug('BBPaydayPushQueue -- update user custom attributes -- successCount: ' + successCount);
        System.debug('BBPaydayPushQueue -- update user custom attributes -- failCount: ' + failCount);
        
        if ( successCount+failCount > 0 )
        {
            update pushNotifications;
        }
        //boolean sent = false;
        
        // check that push notifications were sent....
        // need specific campaign id....will be retrieved from portal or can use PSPHelper to getAllCampaigns then filter results by campaign name to retrieve
        /*
        psph.getSingleCampaign('');

        if ( psph.statusCode == 200 )
        {
            PSPHelper.Campaign c = PSPHelper.parseCampaign( psph.response );
            if ( c.subscriptions_count == successCount )
            {
                System.debug( successCount + 'push notifications were successfully sent');
                sent = true;
            }
        }
        
        if ( Test.isRunningTest() )
        {
            sent = true;
        }

        if ( sent )
        {
            // update each user's custom attribute flag (boolean = false) to remove user from reusable segment
            successCount = 0;
            failCount = 0;
            
            for ( Automated_Email__c ae : pushNotifications )
            {
                if ( String.isNotBlank(ae.Merge_Var_Value_3__c) )
                {
                    String customJSON = '"custom_attributes":[' +
                        '{' +
                        '"name":"sendPush",' +	
                        '"value":"false",' +
                        '"type":"boolean",' +
                        '"action":"Update"' +
                        '}' +
                        ']';
                    
                    psph.updateSingleUser( ae.Merge_Var_Value_3__c , customJSON );
                    
                    if ( Test.isRunningTest() )
                    {
                        System.debug('running test --- overwriting necessary variable values');
                        psph.statusCode = 200;
                        psph.response = 'fake_response';
                    }
                    
                    if ( psph.statusCode == 200 )
                    {
                        successCount ++;
                        
                        // update Automated_Email record
                        ae.Email_Sent__c = true;
                        ae.Status__c = 'Completed';
                    }
                    else
                    {
                        failCount ++;
                    }
                    
                    psph.resetVariables();
                }
            }
            
            if ( successCount > 0 )
            {
            	update pushNotifications;
            }
            
            System.debug('BBPaydayPushQueue -- turn off user custom attribute flag for segment targeting -- successCount: ' + successCount);
            System.debug('BBPaydayPushQueue -- turn off user custom attribute flag for segment targeting -- failCount: ' + failCount);
        }
		*/
    }
}