public with sharing class I_Got_Bank 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security
     *  ---------------------------------------------------------------------------------------- */
    private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Settings
     *  ---------------------------------------------------------------------------------------- */
    private Web_Settings__c ws ;
    
    /*  ----------------------------------------------------------------------------------------
     *  Display objects
     *  ---------------------------------------------------------------------------------------- */
    public String iGotBankBanner { public get ; private set ; }

    public Date contestEndDate { public get ; private set ; }
    public String contestEndDateFormat { public get ; private set ; }

    private Date contestWinnerDate { private get ; private set ; }
    public String contestWinnerDateFormat { public get ; private set ; }
    
    public String contestWinnerNumber { public get ; private set ; }
    public String contestWinnerAmount { public get ; private set ; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Form objects
     *  ---------------------------------------------------------------------------------------- */
 	//  Ticket object to upload
	public Ticket__c ticket 
	{
		get 
		{
			if ( ticket == null )
				ticket = new Ticket__c () ;
			return ticket ;
		}
		
		set ;
	}
     
    //  Date for dropdown since inputfield doesn't work with calendar
	public String pickYouthDate { get ; set ; }
	
	//  Step control for panels on page
	public Integer dStepNumber { get ; private set ; }

	//  Error checking
	public Map<String,String> errorMap { get ; private set ; }
    
    //  ugh
    public SelectOption[] dropdown 
    { 
        get
        {
			return new SelectOption[] { 
                new SelectOption('', '--None--'),
	            new SelectOption('Essay', 'Essay'), 
                new SelectOption('Artwork', 'Artwork') 
                };
        } 
        private set ; 
    }
    public String dropdownPick { get ; set ; }
    
    //  Results
    public String ticketNumber { get ; private set ; }
    
	private Contact c ;

    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public I_Got_Bank ( ApexPages.Standardcontroller stdC )
	{
        //  display results
        ticketNumber = '' ;
        
		//  Controls which panel we display, default of 2000 - No Contest!
		dStepNumber = 2000 ;
        
        //  Load settings
        ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;
        
        //  Active contest?
        if ( ws != null )
        {
            contestEndDate = ws.I_Got_Bank_Contest_End_Date__c ;
            
            if ( ( contestEndDate != null ) && ( contestEndDate >= Date.today () ) )
                dStepNumber = 1000 ;
            
            if ( ws.I_Got_Bank_Banner__c != null )
                iGotBankBanner = ws.I_Got_Bank_Banner__c ;
            
            if ( ws.I_Got_Bank_Contest_Winner_Date__c != null )
                contestWinnerDate = ws.I_Got_Bank_Contest_Winner_Date__c ;
            else
                contestWinnerDate = contestEndDate.addDays ( 90 ) ;
            
            if ( contestWinnerDate <= contestEndDate )
                contestWinnerDate = contestEndDate.addDays ( 90 ) ;
            
            Datetime dte = contestEndDate.addDays ( 1 ) ;
            contestEndDateFormat = dte.format ( 'M/d/yyyy' ) ;
            
            Datetime dtw = contestWinnerDate.addDays ( 1 ) ;
            contestWinnerDateFormat = dtw.format ( 'M/d/yyyy' ) ;
            
            if ( String.isNotBlank ( ws.I_Got_Bank_Contest_Winner_Number__c ) )
                contestWinnerNumber = ws.I_Got_Bank_Contest_Winner_Number__c ;
            else
                contestWinnerNumber = 'Three (3)' ;
            
            if ( String.isNotBlank ( ws.I_Got_Bank_Contest_Winner_Award__c ) )
                contestWinnerAmount = ws.I_Got_Bank_Contest_Winner_Award__c ;
            else
                contestWinnerAmount = '$1,000' ;
        }
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Validate & Upload the file
     *  ---------------------------------------------------------------------------------------- */
	public PageReference iGotBank () 
	{
		//  Check for validation, setup
		errorMap = new Map<String, String> () ;
        
		//  Validation
		//  We dump blanks in if no error found so the page doesn't crash and burn

        //  -------------------- PARENT --------------------
		if ( String.isEmpty ( ticket.First_Name__c ) )
			errorMap.put ( 'First_Name__c' , 'Please enter the first name of your parent' ) ;
		else
			errorMap.put ( 'First_Name__c', '' ) ;
		
		if ( String.isEmpty ( ticket.Last_Name__c ) )
			errorMap.put ( 'Last_Name__c' , 'Please enter the last name of your parent' ) ;
		else
			errorMap.put ( 'Last_Name__c', '' ) ;
		
		if ( String.isEmpty ( ticket.Email_Address__c ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter the email address of your parent' ) ;
        else if ( ! OneUnitedUtilities.isValidEmail ( ticket.Email_Address__c ) )
			errorMap.put ( 'Email_Address__c' , 'Please enter the a valid email address for your parent' ) ;
		else
			errorMap.put ( 'Email_Address__c', '' ) ;
		
		if ( String.isEmpty ( ticket.Phone__c ) )
			errorMap.put ( 'Phone__c' , 'Please enter the mobile phone of your parent' ) ;
		else
			errorMap.put ( 'Phone__c', '' ) ;
        
        //  -------------------- CHILD --------------------
		if ( String.isEmpty ( ticket.Youth_Name__c ) )
			errorMap.put ( 'Youth_Name__c' , 'Please enter your name' ) ;
		else
			errorMap.put ( 'Youth_Name__c', '' ) ;
		
		if ( String.isEmpty ( ticket.School__c ) )
			errorMap.put ( 'School__c' , 'Please enter your school' ) ;
		else
			errorMap.put ( 'School__c', '' ) ;
		
		//  Attempt to convert date from string... catch exception to prevent kablooey on page
		Date pDate = null ;
		try
		{
			if ( pickYouthDate != null )
				pDate = Date.parse ( pickYouthDate ) ;
		}
		catch ( TypeException te )
		{
			pDate = null ;	
		}
		
		if ( pDate == null )
			errorMap.put ( 'pickYouthDate' , 'Please enter your date of birth' ) ;
		else
		{
			ticket.Youth_Date_of_Birth__c = pDate ;
			errorMap.put ( 'pickYouthDate', '' ) ;
		}

		if ( String.isEmpty ( ticket.Youth_Home_Phone__c ) )
			errorMap.put ( 'Youth_Home_Phone__c' , 'Please enter your home phone' ) ;
		else
			errorMap.put ( 'Youth_Home_Phone__c', '' ) ;
		
		if ( String.isEmpty ( ticket.Street_Address__c ) )
			errorMap.put ( 'Street_Address__c' , 'Please enter your home address' ) ;
		else
			errorMap.put ( 'Street_Address__c', '' ) ;
		
		if ( String.isEmpty ( ticket.City__c ) )
			errorMap.put ( 'City__c' , 'Please enter your home city' ) ;
		else
			errorMap.put ( 'City__c', '' ) ;
		
		if ( String.isEmpty ( ticket.State__c ) )
			errorMap.put ( 'State__c' , 'Please enter your home state' ) ;
		else
			errorMap.put ( 'State__c', '' ) ;
		
		if ( String.isEmpty ( ticket.Zip_Code__c ) )
			errorMap.put ( 'Zip_Code__c' , 'Please enter your home zip code' ) ;
		else
			errorMap.put ( 'Zip_Code__c', '' ) ;
		
		boolean isValid = true ;
        
        //  -------------------- Which one --------------------
		if ( String.isEmpty ( dropdownPick ) )
			errorMap.put ( 'dropdownPick' , 'Please choose your type of entry' ) ;
		else
			errorMap.put ( 'dropdownPick', '' ) ;
        
        //  -------------------- ESSAY --------------------
        if ( ( ! String.isEmpty ( dropdownPick ) ) &&
             ( dropdownPick.equalsIgnoreCase ( 'Essay' ) ) )
        {
            if ( String.isEmpty ( ticket.Essay__c ) )
                errorMap.put ( 'Essay__c' , 'Please enter your essay' ) ;
            else
                errorMap.put ( 'Essay__c', '' ) ;
        }
        else
            errorMap.put ( 'Essay__c', '' ) ;

        //  -------------------- Disclaimer --------------------
        if ( String.isEmpty ( ticket.I_Got_Bank_Disclaimer__c ) )
			errorMap.put ( 'I_Got_Bank_Disclaimer__c' , 'Please accept the disclaimer' ) ;
        else if ( ! ticket.I_Got_Bank_Disclaimer__c.equalsIgnoreCase ( 'Yes' ) )
			errorMap.put ( 'I_Got_Bank_Disclaimer__c' , 'Please accept the disclaimer' ) ;
		else
			errorMap.put ( 'I_Got_Bank_Disclaimer__c', '' ) ;
		
		//  Non blank entry means an error occurred
		for ( String key : errorMap.keySet () )
		{
			if ( String.isNotEmpty ( errorMap.get ( key ) ) )
				isValid = false ;
		}

        if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        dStepNumber = 1001 ;
		}
        else
        {
            dStepNumber = 1100 ;
            
            System.debug ( '========== Checking for Contact ==========' ) ;
            c = OneUnitedUtilities.getContactByNameAndEmail ( ticket.First_Name__c, ticket.Last_Name__c,ticket.Email_Address__c ) ;
            
            if ( c == null )
            {
            	System.debug ( '========== Contact not found, creating ==========' ) ;
                
                c = new Contact () ;
                c.FirstName = ticket.First_Name__c ;
                c.LastName = ticket.Last_Name__c ;
                c.Email = ticket.Email_Address__c ;
                c.MobilePhone = ticket.Phone__c ;
                c.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
                
                Account a = OneUnitedUtilities.getDefaultAccount () ;
                if ( a!= null )
                	c.AccountId = a.ID ;
                
                try
                {
                	access.insertObject ( c ) ;
                }
                catch ( Exception e )
                {
                    c = null ;
                }
            }
            
            try 
            {
                System.debug ( '========== Checking for created ticket ==========' ) ;
                
                if ( ticket.ID == null )
                {
                    System.debug ( '========== Inserting Ticket ==========' ) ;
                    
                    ticket.RecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'I Got Bank', 'Ticket__c' ) ;
                    ticket.Status__c = 'Received' ;
                    ticket.Youth_Artwork_Essay_Choice__c = dropdownPick ;
                    ticket.Security_Key__c = one_utils.getRandomString ( 30 ) ; // force it

                    if ( c != null )
	                    ticket.Contact__c = c.ID ;

                    access.insertObject ( ticket ) ;
                }
            }
            catch ( DMLException e )
            {
                System.debug ( '========== Error Inserting Ticket! ==========' ) ;
                dStepNumber = 1200 ;
                
                for ( Integer i = 0 ; i < e.getNumDml () ; i++ ) 
                    System.debug ( e.getDmlMessage ( i ) ) ; 
    
                return null;
            }
            
            //  Get the name if they succeeded & send the email
            if ( dStepNumber == 1100 )
            {
                System.debug ( '========== Success getting # from created ticket ==========' ) ;
                list<Ticket__c> ntL = null ;
                
                try
                {
                    nTL = [
                        SELECT ID, Name
                        FROM Ticket__c
                        WHERE ID = :ticket.ID 
                        LIMIT 1
                    ] ;
                }
                catch ( DMLException e )
                {
                    // ?
                }
                
                if ( ( nTL != null ) && ( nTL.size () > 0 ) )
                {
                    Ticket__c nT = nTL [ 0 ] ;
                    ticketNumber = nT.Name ;
                }
                
                System.debug ( '========== Sending email ==========' ) ;
                if ( c != null )
                {
                    Automated_Email__c ae = new Automated_Email__c () ;
	                ae.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Survey Email', 'automated_email__c' ) ;
                    ae.Ticket__c = ticket.ID ;
                    ae.Contact_OR_Account__c = 'Contact' ;
                    ae.Contact__c = c.ID ;
                    ae.Is_A_Survey__c = 'N' ;
                    ae.Template_Name__c = 'i-got-bank-submission' ;
                    ae.First_Name__c = ticket.First_Name__c ;
                    ae.Last_Name__c = ticket.Last_Name__c ;
                    ae.Email_Address__c = ticket.Email_Address__c ;
                    ae.Upload_ID__c = ticket.Security_Key__c ;
                    
                    ae.Merge_Var_Name_1__c = 'Confirmation' ;
                    ae.Merge_Var_Value_1__c = ticketNumber ;
                    ae.Merge_Var_Name_2__c = 'aid' ;
                    ae.Merge_Var_Value_2__c = 'aid' ;
                    ae.Merge_Var_Name_3__c = 'aid' ;
                    ae.Merge_Var_Value_3__c = ticket.ID ;
                    ae.Merge_Var_Name_4__c = 'kid' ;
                    ae.Merge_Var_Value_4__c = 'kid' ;
                    ae.Merge_Var_Name_5__c = 'kid' ;
                    ae.Merge_Var_Value_5__c = ticket.Security_Key__c ;
                    
                    try
                    {
                        access.insertObject ( ae ) ;
                    }
                    catch ( DMLException e )
                    {
                        // ?
                    }
                }
                else
                {
                    System.debug ( 'No contact found to send email ?!?' ) ;
                }
            }
        }
		
        System.debug ( '========== Returning! ==========' ) ;
        return null ;
    }
    
}