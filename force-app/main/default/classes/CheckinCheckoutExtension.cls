public class CheckinCheckoutExtension {

	public Boolean initialised{ public get; private set;}

    public Timesheet_Entry__c timesheetEntry;
    
    private list<Timesheet_Entry__c> duplicateEntryIDL ;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public CheckinCheckoutExtension(ApexPages.StandardController stdController) {
        String userId=UserInfo.getUserId();
        
        list<Timesheet_Entry__c> teL = null ;
        duplicateEntryIDL = new list<Timesheet_Entry__c> () ;
        
        try
        {
            teL = [
					SELECT ID,
                    Name,
                    Afternoon_In_Time__c,
                    Afternoon_Time_In_Str__c,
                    Afternoon_Out_time__c,
                    Afternoon_Time_Out_Str__c,
                    CreatedById,
                    entered_by_id__c,
                    user_id__c,
                    actual_Location_id__c,
                    Morning_In_Time__c,
                    Morning_Time_In_Str__c,
                    Morning_Out_Time__c,
                    Morning_Time_Out_Str__c,
                    OwnerId,
                    timesheet_entry_day__c,
                    timesheet_entry_id__c,
                    timesheet_entry_type_cd__c,
                    Total_Paid_Hours__c
               FROM Timesheet_Entry__c 
              WHERE user_id__c=:userId 
                AND timesheet_entry_day__c=TODAY
                AND Duplicate__c=FALSE
           ORDER BY Name
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( teL != null ) && ( teL.size () > 0 ) )
        {
            this.timesheetEntry = teL [ 0 ] ;
            
            if ( teL.size () > 1 )
            {
                for ( integer i = 1 ; i < teL.size() ; i ++ )
                {
                    Timesheet_Entry__c te = new Timesheet_Entry__c () ;
                    te.ID = teL [ i ].ID ;
                    te.Duplicate__c = true ;
                    
					duplicateEntryIDL.add ( te ) ;
                }
            }
            else
            {
                duplicateEntryIDL = null ;
            }
        }

        initialised=false;
    }
    
    public String getLocationId() {
        return ''+timesheetEntry.actual_Location_id__c;
    }
    
    
    public void initCheckin(){
        if (!initialised){
        
             if (this.timesheetEntry.Morning_In_Time__c==null) {
                 this.timesheetEntry.Morning_In_Time__c= system.now();
                 this.timesheetEntry.Morning_Out_Time__c=null;
             } else {
                 this.timesheetEntry.Afternoon_In_time__c= system.now();
                 this.timesheetEntry.Afternoon_Out_Time__c=null;
             }
             
             update this.timesheetEntry;
             initialised=true;
            
            if ( ( duplicateEntryIDL != null ) && ( duplicateEntryIDL.size () > 0 ) )
            {
                try
                {
                    update duplicateEntryIDL ;
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }
         }
     }
     
    public void initCheckout(){
        if (!initialised){
        
             if (this.timesheetEntry.Morning_Out_Time__c==null) {
                 this.timesheetEntry.Morning_Out_Time__c= system.now();                 
                 this.timesheetEntry.Afternoon_In_time__c=null;
                 this.timesheetEntry.Afternoon_Out_Time__c=null;
             } else {
                 this.timesheetEntry.Afternoon_Out_time__c= system.now();
             }
             
             update this.timesheetEntry;
             initialised=true;
         }
     }
    
    
}