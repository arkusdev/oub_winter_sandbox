global class batch_ScheduledTaskReassign implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
	
    global batch_ScheduledTaskReassign ()
    {
    	System.debug ( '----------------------------- CONSTRUCTOR -----------------------------' ) ;
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
    	System.debug ( '----------------------------- START -----------------------------' ) ;
    	
		String selectOMatic = 'IsClosed = false AND RecordType.DeveloperName=\'Scheduled_Email_Template\' AND Email_Date_and_Time__c >= TODAY' ; 
		
		if ( ! Test.isRunningTest () )
			selectOMatic += ' AND CreatedBy.IsActive = false' ;

		list<String> otherFieldList = new list<String> () ;
		otherFieldList.add ( 'IsDeleted' ) ;
		otherFieldList.add ( 'CreatedBy.Id' ) ;
		otherFieldList.add ( 'CreatedBy.IsActive' ) ;
		otherFieldList.add ( 'CreatedBy.Name' ) ;
		
		String query = DynamicObjectHandler.getUpdateableFieldQuery ( new Task(), selectOMatic, 'ID', otherFieldList ) ;
		
		System.debug ( 'QQ :: ' + query ) ;
    
    	System.debug ( '----------------------------- START END -----------------------------' ) ;
    	
		return Database.getQueryLocator ( query ) ;
	}
    
	global void execute ( Database.Batchablecontext bc, List<Task> tL )
	{
		System.debug ( '------------------ EXECUTE ------------------' ) ;
		
		System.debug ( '------------------ Cloning tasks ------------------' ) ;
		list<Task> ntL = new list<Task> () ;
		
		list<ID> idl = new list<ID> () ;
		
		if ( ( tL != null ) && ( tL.size () > 0 ) )
		{
			for ( Task t : tL )
			{
				//  Selecting on isDeleted times out
				if ( ! t.isDeleted )
				{
					Task nT = t.clone ( false, true, true, true ) ;
					ntL.add ( nT ) ;
					idl.add ( t.ID ) ;
				}
			}
		}
		else
		{
			system.debug ( 'No tasks to reassign' ) ;
		}
		
		System.debug ( 'Inserting cloned tasks' ) ;
		boolean successInsert = true ;
		
		try
		{
			insert nTL ;
		}
		catch ( DMLException e )
		{
			successInsert = false ;
		    for ( Integer i = 0 ; i < e.getNumDml () ; i++ )
		    {
		        System.debug ( e.getDmlMessage ( i ) ) ;
		    }
		}

		if ( successInsert )
		{
			System.debug ( 'Deleting original tasks' ) ;
			
			try
			{
				list<Task> tdL = [
					SELECT ID
					FROM Task
					WHERE ID IN :iDL
				] ;
				
				if ( ( tdL != null ) && ( tdL.size () > 0 ) )
				{
					System.debug ( 'Found re-selected original tasks to delete' ) ;
					
					delete tdL ;
					Database.emptyRecycleBin ( tdL ) ;
				}
			}
			catch ( DMLException e )
			{
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ )
			    {
			        System.debug ( e.getDmlMessage ( i ) ) ;
			    }
			}
		}

		System.debug ( '------------------ EXECUTE END ------------------' ) ;
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		System.debug ( '------------------ FINISH ------------------' ) ;
		
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Scheduled Task Reassignment Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
			
		System.debug ( '------------------ FINISH END ------------------' ) ;
	}
}