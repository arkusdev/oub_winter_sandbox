global class batch_Deposit_SWIM_Settlement_Sch implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Deposit_SWIM_Settlement b = new batch_Deposit_SWIM_Settlement () ;
		Database.executeBatch ( b ) ;
   }
}