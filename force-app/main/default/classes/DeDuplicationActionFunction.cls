global class DeDuplicationActionFunction 
{
	private static final String DELIMITER = '~' ;
	
	private static String customerRecordTypeID ;
	private static String prospectRecordTypeID ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Internal
	 *  -------------------------------------------------------------------------------------------------- */	
	private static void dedupeContact ( List<ID> xL )
	{
		customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
		
		Contact emptyC = new Contact () ;
		List<String> emptyContactFields = DynamicObjectHandler.getUpdateableFields ( emptyC ) ;
		
		String cLs = DynamicObjectHandler.joinListQuoted ( xL, ',' ) ;
		
		String c1 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyC, 'ID IN ( ' + cLs + ' ) AND recordTypeID = \'' + customerRecordTypeID + '\'' , 'ID' ) ;
		
//		System.debug ( 'C1: ' + c1 ) ;
		
		list<String> sEL = new list<String> () ;
		list<String> sPL = new list<String> () ;
		list<Contact> cL = Database.query ( c1 ) ;
		
		map<String,Contact> cEMap = new Map<String,Contact> () ;
		
		System.debug ( 'Contact list size: ' + cL.size () ) ;
		
		if ( ( cL != null ) && ( cL.size () > 0 ) )
		{
			for ( Contact c : cL )
			{
				if ( String.isNotBlank ( getKeyByEmail ( c ) ) )
				{
					cEMap.put ( getKeyByEmail ( c ), c ) ;
					sEL.add ( c.Email ) ;
				}
				
				if ( String.isNotBlank ( getKeyByPhone ( c ) ) )
				{
					cEMap.put ( getKeyByPhone ( c ), c ) ;
					sPL.add ( c.Phone ) ;
				}
			} 
			
			String oELs = null ;
			String oPLs = null ;
			
			// JIM's crazy Prospect check
			String thingy = ' (  recordTypeID = \'' + prospectRecordTypeID + '\' AND UploadID__c = NULL AND TaxID__c = NULL ) ' ;
			boolean foundData = false ;
			
			if ( ( sEL.size () > 0 ) && ( sPL.size () > 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += 'AND ( Email IN ( ' + oELs + ' ) OR Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () > 0 ) && ( sPL.size () == 0 ) )
			{
				oELs = DynamicObjectHandler.joinListQuoted ( sEL, ',' ) ;
				
				thingy += 'AND ( Email IN ( ' + oELs + ' ) )' ;
				foundData = true ;
			}
			else if ( ( sEL.size () == 0 ) && ( sPL.size () > 0 ) )
			{
				oPLs = DynamicObjectHandler.joinListQuoted ( sPL, ',' ) ;
				
				thingy += 'AND ( Phone IN ( ' + oPLs + ' ) )' ;
				foundData = true ;
			}
			
			map<ID,Contact> contactMapToUpdate = new map<ID,Contact> () ;
			map<ID,Contact> masterContactMap = new map<ID,Contact> () ;
			map<ID,List<Contact>> duplicateContactMap = new map<ID,List<Contact>> () ;
			
			if ( foundData == true )
			{
				
				String c2 = DynamicObjectHandler.getUpdateableFieldQuery ( emptyC, thingy, 'ID' ) ;
		
//				System.debug ( 'C2: ' + c2 ) ;
				
				list<Contact> oL = Database.query ( c2 ) ;
				
				if ( ( oL != null ) && ( oL.size () > 0 ) )
				{
					for ( Contact c : oL )
					{
						boolean found = false ;
						
						System.debug ( 'Checking for :: ' + getKeyByEmail ( c ) ) ;
						
						if ( cEMap.containsKey ( getKeyByEmail ( c ) ) )
						{
							System.debug ( 'Email Key FOUND!' ) ;
							
							Contact cc = cEMap.get ( getKeyByEmail ( c ) ) ;
							
							if ( cc.ID != c.ID )
							{
								for ( String s : emptyContactFields )
								{
									if ( ( cc.get ( s ) == null ) && ( c.get ( s ) != null ) )
									{
									 	System.debug ( 'Setting "' + s + '", found difference' ) ;
									 	
									 	found = true ;
									 	cc.put ( s, c.get ( s ) ) ;
									}
								}
								
								if ( found == true )
								{
									if ( ! contactMapToUpdate.containsKey ( cc.ID ) )
										contactMapToUpdate.put ( cc.ID, cc ) ;
								}
								
								if ( ! masterContactMap.containsKey ( cc.ID ) )
									masterContactMap.put ( cc.ID, cc ) ;
								
								if ( ! duplicateContactMap.containsKey ( cc.ID ) )
									duplicateContactMap.put ( cc.ID, new list<Contact> () ) ;	
			
								duplicateContactMap.get ( cc.ID ).add ( c ) ;
							}
							else
							{
								System.debug ( 'Duplicate Master ID found, skipping' ) ;
							}
						}
						else
						{
							System.debug ( 'Email Key Not Found, checking phone!' ) ;
							
							System.debug ( 'Checking for :: ' + getKeyByPhone ( c ) ) ;
						
							if ( cEMap.containsKey ( getKeyByPhone ( c ) ) )
							{
								System.debug ( 'Phone Key FOUND!' ) ;
								
								Contact cc = cEMap.get ( getKeyByPhone ( c ) ) ;
								
								if ( cc.ID != c.ID )
								{
									for ( String s : emptyContactFields )
									{
										if ( ( cc.get ( s ) == null ) && ( c.get ( s ) != null ) )
										{
										 	System.debug ( 'Setting "' + s + '", found difference' ) ;
										 	
										 	found = true ;
										 	cc.put ( s, c.get ( s ) ) ;
										}
									}
									
									if ( found == true )
									{
										if ( ! contactMapToUpdate.containsKey ( cc.ID ) )
											contactMapToUpdate.put ( cc.ID, cc ) ;
									}
									
									if ( ! masterContactMap.containsKey ( cc.ID ) )
										masterContactMap.put ( cc.ID, cc ) ;
									
									if ( ! duplicateContactMap.containsKey ( cc.ID ) )
										duplicateContactMap.put ( cc.ID, new list<Contact> () ) ;	
				
									duplicateContactMap.get ( cc.ID ).add ( c ) ;
								}
								else
								{
									System.debug ( 'Duplicate Master ID found, skipping' ) ;
								}
							}
							else
							{
								System.debug ( 'Key Not Found!' ) ;
							}
						}
					}
				}
				else
				{
					System.debug ( 'No prospect records found to merge!' ) ;
				}
			}
			else
			{
				System.debug ( 'No valid emails or phone numbers found to merge on!' ) ;
			}
	
			if ( contactMapToUpdate.keyset ().size () > 0 )
			{
				list<Contact> contactListToUpdate = new list<Contact> () ;
				
				for ( String cID : contactMapToUpdate.keyset () )
					contactListToUpdate.add ( contactMapToUpdate.get ( cID ) ) ;
				
				try
				{
					System.debug ( 'Updating ' + contactListToUpdate.size () + ' contact records' ) ;
				
					update contactListToUpdate ;
				}
				catch ( DMLException e )
				{
					System.debug ( 'DML : ' + e.getMessage () ) ;
				}
			}
				
			if ( masterContactMap.keyset ().size () > 0 )
			{
				System.debug ( 'Merging into ' + masterContactMap.keyset ().size () + ' contact records' ) ;
				
				for ( ID x : masterContactMap.keyset () )
				{
					Database.MergeResult[] results = Database.merge ( masterContactMap.get ( x ), duplicateContactMap.get ( x ), false ) ;
					
					for ( Database.MergeResult res : results ) 
					{
					    if ( res.isSuccess () ) 
						{
							System.debug ( 'MERGE SUCCESS!' ) ;
						}
						else
						{
							for ( Database.Error err : res.getErrors () )
								System.debug ( err.getMessage () );
						}
					}
				}
			}
		}
		else
		{
			System.debug ( 'No customer contacts found to merge' ) ;
		}
	}
	
	//  Key mapping from contact
	private static String getKeyByEmail ( Contact c )
	{
		String x = '' ;
		
		if ( ( String.isNotBlank ( c.Email ) ) &&
		     ( String.isNotBlank ( c.FirstName ) ) &&
		     ( String.isNotBlank ( c.LastName ) ) )
		{
			x = c.Email.toUpperCase () + DELIMITER + c.FirstName.toUpperCase () + DELIMITER + c.LastName.toUpperCase () ;
		}
		
		return x ;
	}
	
	//  Key mapping from contact
	private static String getKeyByPhone ( Contact c )
	{
		String x = '' ;
		
		if ( ( String.isNotBlank ( c.Phone ) ) &&
		     ( String.isNotBlank ( c.FirstName ) ) &&
		     ( String.isNotBlank ( c.LastName ) ) )
		{
			x = c.Phone.toUpperCase () + DELIMITER + c.FirstName.toUpperCase () + DELIMITER + c.LastName.toUpperCase () ;
		}
		
		return x ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='DeDuplicate Object' )
	global static List<DeDuplicationActionFunctionResult> deduplicateContact ( List<DeDuplicationActionFunctionRequest> requests ) 
	{
		System.debug ( '------------------------- DEDUPE STARTING ------------------------------' ) ;
		
		//  Not used?
		list<DeDuplicationActionFunctionResult> rL = new list<DeDuplicationActionFunctionResult> () ;
		
		if ( ( requests != null ) && ( requests.size () > 0 ) )
		{
			//  Get IDs out of Object
			list<ID> incL = new List<ID> () ;
			
			for ( DeDuplicationActionFunctionRequest d : requests )
			{
				incL.add ( d.cID ) ;
			}
			
			dedupeContact ( incL ) ;
		}
		
		System.debug ( '------------------------- DEDUPE ENDING ------------------------------' ) ;
		return rL ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input/output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class DeDuplicationActionFunctionRequest
	{
	    @InvocableVariable ( label='Contact ID' required=true )
	    public ID cID ;
	}
	
	global class DeDuplicationActionFunctionResult
	{
	    @InvocableVariable
	    public ID cID ;
	}
}