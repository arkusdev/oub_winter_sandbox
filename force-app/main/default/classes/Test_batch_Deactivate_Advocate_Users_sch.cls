@isTest
public class Test_batch_Deactivate_Advocate_Users_sch 
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	static testmethod void something ()
	{
		Test.startTest();

		batch_Deactivate_Advocate_Users_Schedule myClass = new batch_Deactivate_Advocate_Users_Schedule ();   
		String chron = '0 0 23 * * ?';        
		system.schedule('Test Sched', chron, myClass);
		
		Test.stopTest();
	}
}