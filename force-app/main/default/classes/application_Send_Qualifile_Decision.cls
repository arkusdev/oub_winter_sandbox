public with sharing class application_Send_Qualifile_Decision 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Input
     *  -------------------------------------------------------------------------------------------------- */
    public String overrideReason { public get ; public set ; }
    public String overrideReasonError { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Form & Button display
     *  -------------------------------------------------------------------------------------------------- */
    public boolean canOverride { public get ; private set ; }
    public boolean applicantOverrideButton { public get ; private set ; }
    public boolean coApplicantOverrideButton { public get ; private set ; }
    public boolean bothOverrideButton { public get ; private set ; }
    public boolean declineButton { public get ; private set ; }
    public boolean LOSOverride { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Application
     *  -------------------------------------------------------------------------------------------------- */
    private one_application__c app;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public application_Send_Qualifile_Decision ( ApexPages.StandardController controller )
    {
        //  DEFAULTS
        overrideReason = 'Passed Qualifile' ;
        overrideReasonError = null ;
        
        canOverride = false ;
        applicantOverrideButton = false ;
        coApplicantOverrideButton = false ;
        bothOverrideButton = false ;
        declineButton = false ;
        LOSOverride = false ;
        
        //  Get (possibly incomplete) application 
        this.app = (one_application__c) controller.getRecord () ;
        
        //  Reload application
        this.app = one_utils.getApplicationFromId ( app.Id ) ;
        
        //  Check for LOS
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
        {
            LOSOverride = true ;
        }
        else
        {
            //  Check the co-applicant
            if ( app.Number_of_Applicants__c.equalsIgnoreCase ( 'I will be applying jointly with another person' ) )
            {
                //  Do they have a valid Score to override?
                if ( ( String.isNotBlank ( app.QualiFile_CoApp_Decision__c ) ) &&
                    ( app.QualiFile_CoApp_Decision__c.equalsIgnoreCase ( 'Review' ) ) &&
                    ( app.Qualifile_CoApplicant_Override_User__c == null ) &&
                    ( app.Qualifile_CoApplicant_Override_Datetime__c == null ) )
                {
                    canOverride = true ;
                    coApplicantOverrideButton = true ;
                }
            }
            
            //  Do they have a valid Score to override?
            if ( ( String.isNotBlank ( app.QualiFile_Decision__c ) ) &&
                ( app.QualiFile_Decision__c.equalsIgnoreCase ( 'Review' ) ) &&
                ( app.Qualifile_Override_User__c == null ) &&
                ( app.Qualifile_Override_Datetime__c == null ) )
            {
                canOverride = true ;
                applicantOverrideButton = true ;
            }
            
            //  Override one, override both
            if ( applicantOverrideButton && coApplicantOverrideButton )
                bothOverrideButton = true ;
            
            //  Override one, allow for decline
            if ( applicantOverrideButton || coApplicantOverrideButton )
                declineButton = true ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for applicant
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runApplicantOverride ()
    {
        overrideReasonError = null ;
        
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for co-applicant
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runCoApplicantOverride ()
    {
        overrideReasonError = null ;
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the co-applicant
            fish.setCoApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override for both
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runBothApplicantOverride ()
    {
        overrideReasonError = null ;
        
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  run the applicant
            fish.setApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  run the applicant
            fish.setCoApplicant () ;
            fish.overrideQualifile ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Decline
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference declineApplication ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        try
        {
            app.FIS_Decision_Code__c = 'D911' ; // ?
            System.debug('Update app debugging 6:');
            update app ;
        }
        catch ( Exception e )
        {
            System.debug ( 'WTF?!?' ) ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Cancel ?!?
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference cancel ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        return pr ;
    }
}