global class batch_PolicyManagement_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		Date d = Date.today () ;
		
		//  Run on the first of the month - OR TESTING
		if ( ( d.day () == 1 ) || ( Test.isRunningTest () ) )
		{
	   		//  #1
			batch_PM_Monthly_Status bms = new batch_PM_Monthly_Status () ;
			Database.executeBatch ( bms ) ;
		}
		
		//  Run every day
		batch_PM_60_Day_Warning b60 = new batch_PM_60_Day_Warning () ;
		Database.executeBatch ( b60 ) ;
   	}
}