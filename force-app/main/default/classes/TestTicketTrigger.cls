@isTest
public class TestTicketTrigger 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		
		Branch__c b = getBranch () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'test'+ i + '.person' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		insert c ;
		
		return c ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return an account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ( Integer i )
	{
		Account a = new Account () ;
		a.Name = 'Test' + i + 'Account' + i ;
		a.FederalTaxID__c = 'FA-000' + i ;
		
		insert a ;
		
		return a ; 
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a financial account
	 * ----------------------------------------------------------------------------------- */
	private static Financial_Account__c getFinancialAccount ( Contact c, Account a )
	{
		String loanRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan', 'Financial_Account__c' ) ;
		
		Branch__c b = getBranch () ;
		
		Financial_Account__c f = new Financial_Account__c () ;
		f.ACCTNBR__c = '1111333355559999' ;
		f.CURRMIACCTTYPCD__c = 'QB01' ;
		f.MJACCTTYPCD__c = 'MTG' ;
		f.OPENDATE__c = Datetime.now ().date () ;
		f.BRANCHORGNBR__c = b.ID ;
		f.RecordTypeID = loanRecordTypeId ;
		
		if ( c != null )
			f.TAXRPTFORPERSNBR__c = c.Id ;
			
		if ( a != null )
			f.TAXRPTFORORGNBR__c = a.Id ;
		
		insert f ;
		
		return f ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Users and groups
	 * ----------------------------------------------------------------------------------- */
	private static User getUser ( String thingy )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
        u.Department = 'test department' ;
		
		return u ;
	}
    
    private static Group getGroup ( String name )
    {
    	// Group
        Group g = new Group () ;
        g.Name = name ;
        g.Type = 'Queue';
        
        return g ;
    }
    
    private static void addUserToGroup ( group g, user u )
    {
        GroupMember gm = new GroupMember () ; 
        gm.GroupId = g.id ;
        gm.UserOrGroupId = u.id ;
        
        insert gm ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * TEST METHODS
	 * ----------------------------------------------------------------------------------- */
	public static testmethod void testCommunityRoomTicketInsertNoContact ()
	{
		//  Need the record type
		String communityRoomRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Ticket__c' ) ;
		
		//  Create ticket
		Ticket__c t = new Ticket__c () ;
		
		t.First_Name__c = 'Test9' ;
		t.Last_Name__c = 'User9' ;
		t.Email_Address__c = 'test9@user9.com' ;
		t.Name_of_Organization__c = 'Test Organization' ;
		t.Date_Requested__c = Date.newInstance ( 2025, 10, 10 ) ;
		t.Beginning_Time__c = '10:00 AM PST' ;
		t.Ending_Time__c = '11:00 AM PST' ;
		t.Number_of_People_Expected__c = 5 ;
		t.Phone__c = '123-456-7890' ;
		t.Purpose_of_Meeting__c = 'Na na na na' ;
		t.RecordTypeId = communityRoomRecordTypeId ;
		
		insert t ;
		
		//  Check for contact
		Contact nC = [
			SELECT Id, Email, RecordTypeId
			FROM Contact
			WHERE Email = 'test9@user9.com'
		] ;
		
		String prospectRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
		
		//  Check to make sure contact is correct type
		System.assertEquals ( nC.RecordTypeId, prospectRecordTypeId ) ;
		
		//  Reload ticket
		Ticket__c nT = [
			SELECT Id, Contact__c
			FROM Ticket__c
			WHERE Id = :t.id
		] ;
		
		//  Ticket has newly created contact assigned
		System.assertEquals ( nC.Id, nT.Contact__c ) ;
	}
	
	public static testmethod void testCommunityRoomTicketWithContact ()
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test9' ;
		c.LastName = 'User9' ;
		c.Email = 'test9@user9.com' ;
		c.MailingState = 'CA' ;
		
		insert c ;
		
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		
		t.First_Name__c = 'Test9' ;
		t.Last_Name__c = 'User9' ;
		t.Email_Address__c = 'test9@user9.com' ;
		t.Name_of_Organization__c = 'Test Organization' ;
		t.Date_Requested__c = Date.newInstance ( 2025, 10, 10 ) ;
		t.Beginning_Time__c = '10:00 AM PST' ;
		t.Ending_Time__c = '11:00 AM PST' ;
		t.Number_of_People_Expected__c = 5 ;
		t.Phone__c = '123-456-7890' ;
		t.Purpose_of_Meeting__c = 'Na na na na' ;
		t.RecordTypeId = xRecordTypeId ;
		
		insert t ;
		
		Ticket__c nT = [
			SELECT Id, Contact__c, State__c
			FROM Ticket__c
			WHERE Id = :t.id
		] ;
				
		System.assertEquals ( nT.Contact__c, c.id ) ;
		//System.assertEquals ( nT.State__c, c.MailingState ) ;
	}
	
	public static testmethod void testTicketStateContact ()
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'test@user.com' ;
		c.MailingState = 'CA' ;
		
		insert c ;
		
		Ticket__c t = new Ticket__c () ;
		
		t.First_Name__c = 'Test' ;
		t.Last_Name__c = 'User' ;
		t.Email_Address__c = 'test@user.com' ;
		t.Contact__c = c.id ;
		t.Name_of_Organization__c = 'Test Organization' ;
		t.Date_Requested__c = Date.newInstance ( 2025, 10, 10 ) ;
		t.Beginning_Time__c = '10:00 AM PST' ;
		t.Ending_Time__c = '11:00 AM PST' ;
		t.Number_of_People_Expected__c = 5 ;
		t.Phone__c = '123-456-7890' ;
		t.Purpose_of_Meeting__c = 'Na na na na' ;
		
		insert t ;
		
		Ticket__c nT = [
			SELECT Id, Contact__c, State__c
			FROM Ticket__c
			WHERE Id = :t.id
		] ;
		
		System.assertEquals ( nT.State__c, c.MailingState ) ;
	}
	
	public static testmethod void testTicketStateAccount ()
	{
		Account a = new Account () ;
		a.BillingState = 'CA' ;
		a.Name = 'Test Account' ;
		
		insert a ;
		
		Ticket__c t = new Ticket__c () ;
		
		t.First_Name__c = 'Test9' ;
		t.Last_Name__c = 'User9' ;
		t.Email_Address__c = 'test9@user9.com' ;
		t.Account__c = a.id ;
		t.Name_of_Organization__c = 'Test Organization' ;
		t.Date_Requested__c = Date.newInstance ( 2025, 10, 10 ) ;
		t.Beginning_Time__c = '10:00 AM PST' ;
		t.Ending_Time__c = '11:00 AM PST' ;
		t.Number_of_People_Expected__c = 5 ;
		t.Phone__c = '123-456-7890' ;
		t.Purpose_of_Meeting__c = 'Na na na na' ;
		
		insert t ;
		
		Ticket__c nT = [
			SELECT Id, Contact__c, State__c
			FROM Ticket__c
			WHERE Id = :t.id
		] ;
		
		System.assertEquals ( nT.State__c, a.BillingState ) ;
	}
	
	public static testmethod void testTicketUsers ()
	{
		User u1 = getUser ( '1' ) ;
		insert u1 ;
		
		User u2 = getUser ( '2' ) ;
		insert u2 ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test9' ;
		c.LastName = 'User9' ;
		c.Email = 'test9@user9.com' ;
		c.MailingState = 'CA' ;
		
		insert c ;
		
		Ticket__c t = new Ticket__c () ;
		
		t.First_Name__c = 'Test9' ;
		t.Last_Name__c = 'User9' ;
		t.Email_Address__c = 'test9@user9.com' ;
		t.Contact__c = c.id ;
		t.OwnerId = u1.id ;
		t.Name_of_Organization__c = 'Test Organization' ;
		t.Date_Requested__c = Date.newInstance ( 2025, 10, 10 ) ;
		t.Beginning_Time__c = '10:00 AM PST' ;
		t.Ending_Time__c = '11:00 AM PST' ;
		t.Number_of_People_Expected__c = 5 ;
		t.Phone__c = '123-456-7890' ;
		t.Purpose_of_Meeting__c = 'Na na na na' ;
		
		insert t ;
		
		t.OwnerId = u2.id ;
		
		update t ;
	}
	
	public static testmethod void testSocialMediaAlertTicketNoContact ()
	{
		User u1 = getUser ( '1' ) ;
		insert u1 ;
		
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Social Media Alert', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
		
		insert t ;
		
		String xGroupId = OneUnitedUtilities.getGroupId ( 'Community Room Request' ) ;
		
		t.ownerId = xGroupId ;
		
		update t ;
	}
	
	public static testmethod void testContentRequestTicketNoContact ()
	{
		User u1 = getUser ( '1' ) ;
		insert u1 ;
		
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Content Request', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
		
		insert t ;
    }
    public static testmethod void testWebsiteTicketNoContact ()
	{
		User u1 = getUser ( '1' ) ;
		insert u1 ;
		
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Public Website Comment', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
		
		insert t ;
		
		String xGroupId = OneUnitedUtilities.getGroupId ( 'Public Website Comment' ) ;
		
		t.ownerId = xGroupId ;
		
		update t ;
	}

	public static testmethod void testWebsiteTicketNoContactNoName ()
	{
		User u1 = getUser ( '1' ) ;
		insert u1 ;
		
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Public Website Comment', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		t.Description__c = 'Test Empty Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
		
		insert t ;
		
		String xGroupId = OneUnitedUtilities.getGroupId ( 'Public Website Comment' ) ;
		
		t.ownerId = xGroupId ;
		
		update t ;
	}
	
	public static testmethod void testCSBatchQueue ()
	{
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
		t.recordTypeId = xRecordTypeId ;
		t.Batch_Type__c = 'SWM' ;
		
		insert t ;
	}
	
	public static testmethod void testLoanFinancialTicketContact ()
	{
		//  Load a contact & financial account
		Contact c1 = getContact ( 1 ) ;
		Financial_Account__c fac1 = getFinancialAccount ( c1, null ) ;
		
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan Financials', 'Ticket__c' ) ;
		
		Test.startTest () ;
		
		//  Create ticket
		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Received' ;
		t.Financial_Account__c = fac1.ID ;
		t.recordTypeID = xRecordTypeId ;
		
		insert t ;
		
		//  Create attachment #1
		Attachment a1 = new Attachment () ;
		a1.Name = 'Test Thingy 1' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.IsPrivate = false ;
		a1.parentID = t.ID ;
		
		insert a1 ;
		
		//  Create attachment #2
		Attachment a2 = new Attachment () ;
		a2.Name = 'Test Thingy 1' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.IsPrivate = false ;
		a2.parentID = t.ID ;
		
		insert a2 ;
		
		//  Update ticket
		t.Status__c = 'Accepted' ;

		update t ;
		
		// Select attachments #1
		list<Attachment> aL1 = [
			SELECT ID, ParentID, IsDeleted
			FROM Attachment
			WHERE ParentID = :t.ID
		] ;
		
		System.assertEquals ( aL1.size (), 0 ) ;
		
		// Select attachments #2
		list<Attachment> aL2 = [
			SELECT ID, ParentID
			FROM Attachment
			WHERE ParentID = :fac1.ID
		] ;
		
		System.assertEquals ( aL2.size (), 2 ) ;
		
		Test.stopTest () ;
	}
    
	public static testmethod void testDebitCardRefundTicket ()
	{
		one_application__c app = new one_application__c ();
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = 'fName' ;
		app.Last_Name__c = 'lName' ;
		app.Email_Address__c = 'test@test.com' ;
        app.DOB__c = Date.newInstance ( 1911, 11, 11 ) ;
        app.SSN__c = '666-01-1111' ;
        app.Phone__c = '111-22-3333' ;
        
        app.Street_Address__c = '1 Test Street' ;
        app.City__c = 'Test City' ;
        app.State__c = 'CA' ;
        app.ZIP_Code__c = '90021' ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
        
		app.Gross_Income_Monthly__c = 10000.00 ;

        Decimal amount = Math.random () * 1000 + 250 ;
		app.FIS_Application_ID__c = '111222333' ;
		app.FIS_Decision_Code__c = 'A001' ;
        app.RequestedCreditLimit__c = amount ;
        app.FIS_Credit_Limit__c = amount ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        app.Funding_Options__c = 'E- Check (ACH)' ;
		
        insert app ;
        
        Application_Transaction__c at = new Application_Transaction__c () ;
        
        at.Transaction_Ref_Num__c = '11223344' ;
        at.Customer_Number__c = '44332211' ;
        at.Payment_Method_ID__c = '99887766' ;
        at.Amount__c = app.FIS_Credit_Limit__c ;
        at.Application__c = app.id ;
        at.Status__c = 'Settled' ;
        
        insert at ;
        
        Financial_Account__c fa = new Financial_Account__c () ;
        
		fa.recordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.CREDITLIMITAMT__c = app.FIS_Credit_Limit__c ;
		
        insert fa ;
        
        Ticket__c t1 = new Ticket__c () ;
		t1.Status__c = 'Approved for Refund' ;
		t1.recordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		t1.Financial_Account__c = fa.Id ;
		t1.Application__c = app.Id ;
		t1.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		t1.Application_Refund_Source__c = 'Collateral' ;
		t1.Card_Blocked__c = true ;
        
        insert t1 ;
        
        Ticket__c t2 = new Ticket__c () ;
		t2.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
        t2.Status__c = 'New' ;
        t2.Batch_Account_Total__c = 1 ;
        t2.Batch_Amount_Total__c = app.FIS_Credit_Limit__c ;
        t2.Batch_Type__c = 'SWM DEBIT' ;
        t2.Is_Batch_Refund__c = true ;
        
        insert t2 ;
        
        t1.SWM_Batch_Ticket__c = t2.ID ;
        
        update t1 ;
        
        t2.Status__c = 'Processed' ;
        
        update t2 ;
    }
    
    private static list<ticket__c> setupMessDebit ( Integer counter )
    {
        list<one_application__c> appL = new list<one_application__c> () ;

        for ( Integer i = 0 ; i < counter ; i ++ )
        {
            one_application__c app = new one_application__c ();
            
            app.Number_of_Applicants__c = 'I will be applying individually' ;
            
            app.First_Name__c = 'fName' ;
            app.Last_Name__c = 'lName' ;
            app.Email_Address__c = 'test@test.com' ;
            app.DOB__c = Date.newInstance ( 1911, 11, 11 ) ;
            app.SSN__c = '666-01-1111' ;
            app.Phone__c = '111-22-3333' ;
            
            app.Street_Address__c = '1 Test Street' ;
            app.City__c = 'Test City' ;
            app.State__c = 'CA' ;
            app.ZIP_Code__c = '90021' ;
            app.Mailing_Address_Same__c = 'Yes' ;
            
            app.PrimID_Number__c = 'X00001' ;
            app.PrimID_State__c = 'CA' ;
            
            app.Funding_Account_Type__c = 'Debit Card' ;
            
            app.Gross_Income_Monthly__c = 10000.00 ;
    
            Decimal amount = Math.random () * 1000 + 250 ;
            app.FIS_Application_ID__c = '111222333' ;
            app.FIS_Decision_Code__c = 'A001' ;
            app.RequestedCreditLimit__c = amount ;
            app.FIS_Credit_Limit__c = amount ;
            
            app.UTM_Campaign__c = 'A%20B' ;
            app.UTM_Content__c = 'A%20B' ;
            app.UTM_Medium__c = 'A%20B' ;
            app.UTM_Source__c = 'A%20B' ;
            app.UTM_Term__c = 'A%20B' ;
            app.UTM_VisitorID__c = 'A%20B' ;
            
            app.Unknown_Location__c = true ;
            app.Invalid_Email_Address__c = true ;
            app.Invalid_Co_Applicant_Email_Address__c = true ;
            app.Duplicate_Funding_Account__c = false ;
            app.Duplicate_IP_Address__c = false ;
            
            app.Funding_Options__c = 'Debit Card' ;
            
            appL.add ( app ) ;
        }

        insert appL ;
            
        list<Application_Transaction__c> atL = new list<Application_Transaction__c> () ;
        for ( Integer i = 0 ; i < counter ; i ++ )
        {            
            Application_Transaction__c at = new Application_Transaction__c () ;
            
            at.Transaction_Ref_Num__c = '11223344' ;
            at.Customer_Number__c = '44332211' ;
            at.Payment_Method_ID__c = '99887766' ;
            at.Amount__c = appL[i].FIS_Credit_Limit__c ;
            at.Application__c = appL[i].id ;
            at.Status__c = 'Settled' ;
         
            atL.add ( at ) ;
        }
        
        insert atL ;
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        String rID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
        for ( Integer i = 0 ; i < counter ; i ++ )
        {            
            Financial_Account__c fa = new Financial_Account__c () ;
            
            fa.recordTypeId = rID ;
            fa.MJACCTTYPCD__c = 'VISA' ;
            fa.CURRACCTSTATCD__c = 'ACT' ;
            fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
            fa.NOTEBAL__c = 0 ;
            fa.CREDITLIMITAMT__c = appL[i].FIS_Credit_Limit__c ;
            
            faL.add ( fa ) ;
        }
        
        insert faL ;
        
        list<ticket__c> tL = new list<ticket__c> () ;
        
        String gID = OneUnitedUtilities.getGroupId ( 'Credit Card Refund Approver' ) ;
        String rrID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
        for ( Integer i = 0 ; i < counter ; i ++ )
        {            
            Ticket__c t1 = new Ticket__c () ;
            t1.Status__c = 'Approved for Refund' ;
            t1.recordTypeId = rrID ;
            t1.Financial_Account__c = faL[i].Id ;
            t1.Application__c = appL[i].Id ;
            t1.Approved_Refund_Amount__c = faL[i].CREDITLIMITAMT__c ;
            t1.Application_Refund_Source__c = 'Collateral' ;
            t1.Card_Blocked__c = true ;
            t1.OwnerId = gID ;
            
            tL.add ( t1 ) ;
        }
        
        insert tL ;
        
        return tL ;
    }
    
    private static list<ticket__c> setupMessACH ( Integer counter )
    {
        list<one_application__c> appL = new list<one_application__c> () ;

        for ( Integer i = 0 ; i < counter ; i ++ )
        {
            one_application__c app = new one_application__c ();
            
            app.Number_of_Applicants__c = 'I will be applying individually' ;
            
            app.First_Name__c = 'fName' ;
            app.Last_Name__c = 'lName' ;
            app.Email_Address__c = 'test@test.com' ;
            app.DOB__c = Date.newInstance ( 1911, 11, 11 ) ;
            app.SSN__c = '666-01-1111' ;
            app.Phone__c = '111-22-3333' ;
            
            app.Street_Address__c = '1 Test Street' ;
            app.City__c = 'Test City' ;
            app.State__c = 'CA' ;
            app.ZIP_Code__c = '90021' ;
            app.Mailing_Address_Same__c = 'Yes' ;
            
            app.PrimID_Number__c = 'X00001' ;
            app.PrimID_State__c = 'CA' ;
            
            app.Funding_Account_Type__c = 'Savings' ;
            app.Funding_Account_Number__c = '111222333' ;
            app.Funding_Routing_Number__c = '011001576' ;
            
            app.Gross_Income_Monthly__c = 10000.00 ;
    
            Decimal amount = Math.random () * 1000 + 250 ;
            app.FIS_Application_ID__c = '111222333' ;
            app.FIS_Decision_Code__c = 'A001' ;
            app.RequestedCreditLimit__c = amount ;
            app.FIS_Credit_Limit__c = amount ;
            
            app.UTM_Campaign__c = 'A%20B' ;
            app.UTM_Content__c = 'A%20B' ;
            app.UTM_Medium__c = 'A%20B' ;
            app.UTM_Source__c = 'A%20B' ;
            app.UTM_Term__c = 'A%20B' ;
            app.UTM_VisitorID__c = 'A%20B' ;
            
            app.Unknown_Location__c = true ;
            app.Invalid_Email_Address__c = true ;
            app.Invalid_Co_Applicant_Email_Address__c = true ;
            app.Duplicate_Funding_Account__c = false ;
            app.Duplicate_IP_Address__c = false ;
            
            app.Funding_Options__c = 'E- Check (ACH)' ;
            
            appL.add ( app ) ;
        }

        insert appL ;
            
        String stupidID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        for ( Integer i = 0 ; i < counter ; i ++ )
        {            
            Financial_Account__c fa = new Financial_Account__c () ;
            
            fa.recordTypeId = stupidID ;
            fa.MJACCTTYPCD__c = 'VISA' ;
            fa.CURRACCTSTATCD__c = 'ACT' ;
            fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
            fa.NOTEBAL__c = 0 ;
            fa.CREDITLIMITAMT__c = appL[i].FIS_Credit_Limit__c ;
            
            faL.add ( fa ) ;
        }
        
        insert faL ;
        
        list<ticket__c> tL = new list<ticket__c> () ;
        
        String gID = OneUnitedUtilities.getGroupId ( 'Credit Card Refund Approver' ) ;
        String rID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
        for ( Integer i = 0 ; i < counter ; i ++ )
        {            
            Ticket__c t1 = new Ticket__c () ;
            t1.Status__c = 'Approved for Refund' ;
            t1.recordTypeId = rID ;
            t1.Financial_Account__c = faL[i].Id ;
            t1.Application__c = appL[i].Id ;
            t1.Approved_Refund_Amount__c = faL[i].CREDITLIMITAMT__c ;
            t1.Application_Refund_Source__c = 'Collateral' ;
            t1.Card_Blocked__c = true ;
            t1.OwnerId = gID ;
            
            tL.add ( t1 ) ;
        }
        
        insert tL ;
        
        return tL ;
    }
    
	public static testmethod void testDebitCardRefundTicketMASS ()
	{
        String gID = OneUnitedUtilities.getGroupId ( 'Credit Card Batch Processing' ) ;
        
        //  Ugh
        list<Ticket__c> tL1 = setupMessDebit ( 20 ) ;
        
        double amount = 0.0 ;
        for ( Ticket__c t : tL1 )
            amount += t.Approved_Refund_Amount__c ;

        Test.startTest() ;
        
        Ticket__c t2 = new Ticket__c () ;
		t2.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
        t2.Status__c = 'New' ;
        t2.Batch_Account_Total__c = 1 ;
        t2.Batch_Amount_Total__c = amount ;
        t2.Batch_Type__c = 'SWM DEBIT' ;
        t2.Is_Batch_Refund__c = true ;
        t2.OwnerId = gID ;
        
        insert t2 ;
        
        for ( Ticket__c t : tL1 )
            t.SWM_Batch_Ticket__c = t2.ID ;
        
        update tL1 ;
        
		t2.Status__c = 'Processed' ;
        
        update t2 ;
        
        Test.stopTest () ;
    }
    
	public static testmethod void testACHRefundTicketMASS ()
	{
        String gID = OneUnitedUtilities.getGroupId ( 'Credit Card Batch Processing' ) ;

        // Double ugh!
        list<Ticket__c> tL1 = setupMessACH ( 25 ) ;
        
        double amount = 0.0 ;
        for ( Ticket__c t : tL1 )
            amount += t.Approved_Refund_Amount__c ;

        Test.startTest() ;
        
        Ticket__c t2 = new Ticket__c () ;
		t2.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
        t2.Status__c = 'New' ;
        t2.Batch_Account_Total__c = 1 ;
        t2.Batch_Amount_Total__c = amount ;
        t2.Batch_Type__c = 'SWM DEBIT' ;
        t2.Is_Batch_Refund__c = true ;
        t2.OwnerId = gID ;
        
        insert t2 ;
        
        for ( Ticket__c t : tL1 )
            t.SWM_Batch_Ticket__c = t2.ID ;
        
        update tL1 ;
        
		t2.Status__c = 'Processed' ;
        
        update t2 ;
        
        Test.stopTest () ;
    }
    
	public static testmethod void testSubscriptionStupidity ()
	{
        User u1 = getUser ( '1' ) ;
        insert u1 ;
        
        User u2 = getUser ( '2' ) ;
        insert u2 ;
        
        Test.startTest() ;
        
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Social Media Alert', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
        
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
        t.OwnerId = u1.ID ;
        
		insert t ;
        
        t.OwnerId = u2.ID ;
        
        update t ;
		
        Test.stopTest () ;
    }
    
    public static testmethod void testAccountsPayable ()
    {
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Accounts Payable', 'Ticket__c' ) ;
		
        //  And the group
        String xGroupId = OneUnitedUtilities.getGroupId ( 'IT Invoices' ) ;
        
        Test.startTest() ;
        
		Ticket__c t = new Ticket__c () ;
        
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
        t.OwnerId = xGroupId ;
        t.Status__c = 'Open' ;
        
        insert t ;
        
        t.Amount__c = 100.0 ;
        t.Status__c = 'Approved' ;
        
        update t ;

		Test.stopTest () ;
        
        Ticket__C tx = [
            SELECT ID, OwnerID
            FROM Ticket__c
            WHERE ID = :t.ID
        ] ;
        
        System.assertNotEquals ( t.OwnerID, tx.OwnerID ) ;
    }
    
    public static testmethod void testPPP ()
    {
        Account a1 = new Account () ;
		a1.Name = 'Insight Customers - Individuals' ;
        insert a1 ;
        
		Account a2 = new Account () ;
		a2.Name = 'Insight Customers - Busineses' ;
        insert a2 ;
        
        Contact c1 = getContact ( 1 ) ;

        Loan_Application__c la1 = new Loan_Application__c () ;
        la1.RecordTypeId = LoanApplicationHelper.getPPPRecordTypeID () ;
        la1.Status__c = 'Processing' ;
        la1.Application_Number__c = '123456' ;
        la1.Loan_Amount__c = 890.00 ;

		la1.Organization_Name__c = 'Gina Ingram' ;
        la1.Organization_Email__c = 'ginai65@aol.com' ;
        la1.Organization_EIN__c = '490783867' ;
        
        la1.Organization_Street__c = '9800 West Green Tree' ;
        la1.Organization_City__c = 'Milwaukee' ;
        la1.Organization_State__c = 'WI' ;
        la1.Organization_Zip_Code__c = '53224' ;
        
        insert la1 ;
        
        Test.startTest() ;
        
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Support', 'Ticket__c' ) ;
        
		Ticket__c t = new Ticket__c () ;

        t.RecordTypeId = xRecordTypeId ;
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.Email_Address__c = la1.Organization_Email__c ;
        t.Email_To_Address__c = 'pppservice@oneunited.com' ;
        t.Status__c = 'Open' ;
        
        insert t ;
        
        Test.stopTest () ;
        
        Ticket__c xt = [
            SELECT ID, Loan_Application__c, RecordType.Name
            FROM Ticket__C
            WHERE ID = :t.ID
        ] ;
        
        System.assertEquals ( xt.Loan_Application__c, la1.ID ) ;
        System.assertEquals ( xt.RecordType.Name, 'PPP' ) ;
    }
    
    public static testmethod void testNewDocumentRequestTicket ()
    {
        Contact c1 = getContact ( 1 ) ;
        Account a1 = getAccount ( 1 ) ;
        Financial_Account__c fa1 = getFinancialAccount ( c1, a1 ) ;
        
        Test.startTest() ;
        
		//  Need the record type
		String xRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Document Request', 'Ticket__c' ) ;
		
		Ticket__c t = new Ticket__c () ;
        
		t.First_Name__c = 'TestSA' ;
		t.Last_Name__c = 'UserSA' ;
		t.Description__c = 'Test Alert' ;
		t.recordTypeId = xRecordTypeId ;
		t.Email_Address__c = 'testSA@userSA.com' ;
        t.Documents_Request_Special_Instructions__c = 'Yo' ;
        t.Documents_Required__c = 'Bank Statement' ;
        
		insert t ;
        
        test.stopTest () ;
        
        list<Automated_Email__c> aeL = [
            SELECT ID
            FROM Automated_Email__c
            WHERE Ticket__c = :t.ID
        ] ;
        
        System.assertNotEquals ( aeL.size (), 0 ) ;
    }
}