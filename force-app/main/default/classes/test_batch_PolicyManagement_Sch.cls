@isTest
public with sharing class test_batch_PolicyManagement_Sch 
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
	static testmethod void something ()
	{
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;

		batch_PolicyManagement_Schedule myClass = new batch_PolicyManagement_Schedule ();   
		String chron = '0 0 23 * * ?';        
		system.schedule ( 'Test Schedule', chron, myClass ) ;
		
		Test.stopTest () ;
	}
}