public with sharing class Overdraft_Program {

	private static OneUnitedWebDMLHelper access = OneUnitedWebDMLHelper.getInstance () ;
    public Contact con {get;set;}
	public String firstName {get;set;}
	public String lastName {get;set;}
	public String socialSecurityNumber {get;set;}
	public String dateOfBirth {get;set;}

	public Boolean disabledName {get;set;}

	public Boolean accountsReady {get;set;}
	public Boolean error {get;set;}
	public Boolean success {get;set;}

	public String errorMessage {get;set;}
	public Map<String,Boolean> errorMap {get;set;}
	public Map<String,String> errorMapMessages {get;set;}

	public List<OISWrapper> oisWrapperList {get;set;}

	public transient Map<Integer, string> map_errors_account{get;private set;} 
    public transient String map_keys_account {get; set;}

    public Integer actionTimes {get;set;}
    public Boolean errorRetry {get;set;}
    public String errorRetryMessage {get;set;}

	public Overdraft_Program() {
		accountsReady = false;
		error = false;
		errorRetry = false;
		success = false;
		actionTimes = 0;
		errorRetryMessage = '';

		oisWrapperList = new List<OISWrapper>();

		errorMap = new Map<String,Boolean>();
		errorMapMessages = new Map<String,String>();
		resetMaps();

		try{
			String conId = ApexPages.currentPage().getParameters().get('id');
			if(conId != null){
				con = [SELECT FirstName, LastName, TaxID__c, BirthDate
						FROM Contact WHERE Id =:conId];

				firstName = con.FirstName;
				lastName = con.LastName;
				disabledName = true;
			}else{
				disabledName = false;
			}
		}catch(Exception e){
			error = true;
			errorMessage = 'Contact could not be retrieved'; 
		}
	}

	public PageReference listAccounts (){
		Date bDate;
		resetMaps();
		Boolean formError = false;
		errorRetry = false;

		try{
			if (String.isNotBlank(dateOfBirth)){
				bDate = Date.parse ( dateOfBirth );
			}else{
				errorMap.put('BirthDate', true);
				errorMapMessages.put('BirthDate', 'Please enter your date of birth');
				formError = true;
			}
		}
		catch (Exception e){
			bDate = null ;
			errorMap.put('BirthDate', true);
			errorMapMessages.put('BirthDate' , 'Wrong date of birth format try: mm/dd/yyyy' );
			formError = true;
		}

		if(socialSecurityNumber.length() != 4){
			errorMap.put('socialSecurityNumber', true);
			formError = true;
			if(String.isBlank(socialSecurityNumber)){
				errorMapMessages.put('socialSecurityNumber' , 'Please enter your social security number' );
			}else{
				errorMapMessages.put('socialSecurityNumber' , 'Wrong social security number format, try: ****' );
			}
		}
		if(String.isBlank(firstName)){
			errorMap.put('firstName', true);
			errorMapMessages.put('firstName' , 'Please enter your first name' );
			formError = true;
		}
		if(String.isBlank(lastName)){
			errorMap.put('lastName', true);
			errorMapMessages.put('lastName' , 'Please enter your last name' );
			formError = true;
		}

		if(!formError){
			actionTimes++;

			String social = '%'+socialSecurityNumber;

			List<Contact> listCon = [SELECT FirstName, LastName, SocialSecurityNumber__c, BirthDate 
									FROM Contact 
									WHERE FirstName = :firstName AND LastName = :lastName 
									AND TaxID__c LIKE :social AND BirthDate =:bDate];

			if(!listCon.isEmpty()){
				if(listCon.size() == 1){
					con = listCon[0];
				}else{
					errorMessage = 'Your information was not found. Please try again. If you are still having trouble, please call (877) 663-8648.';
					error = true;
					accountsReady = false;
					return null;
				}
			}else{
				if(actionTimes < 3){
					errorRetryMessage = 'Your information was not found. Please try again. If you are still having trouble, please call (877) 663-8648.';
	                errorRetry = true;
				}else{
					errorMessage = 'Thank you for contacting OneUnited Bank.<br/> <br/>We are not able to process your request at this time.<br/> <br/>Please call (877) 663-8648 to discuss your overdraft options today.';
					error = true;
				}
				accountsReady = false;
				return null;
			}
				
			List<OIS_Status__c> oisList = [
                SELECT Id, Name, Contact__c, Account_Number__c, Opt_In_Status__c, 
                Overdraft_Privledge_Status__c, Financial_Account_Current_Status__c
                FROM OIS_Status__c 
                WHERE Contact__c = :con.Id 
                ORDER BY CreatedDate DESC];

			if(oisList.isEmpty()){
				errorMessage = 'Your information was not found. Please try again. If you are still having trouble, please call (877) 663-8648.';
				error = true;
				accountsReady = false;
				return null;
			}else{
				for(OIS_Status__c ois: oisList){
					if(String.isBlank(ois.Opt_In_Status__c)){
						ois.Opt_In_Status__c = 'D';
					}
                    
                    String odps = 'X' ;
                    if ( String.isNotBlank ( ois.Overdraft_Privledge_Status__c ) )
                        odps = ois.Overdraft_Privledge_Status__c ;
                    
                    String fas = 'X' ;
                    if ( String.isNotBlank ( ois.Financial_Account_Current_Status__c ) )
                        fas = ois.Financial_Account_Current_Status__c ;

                    if ( ( fas.equalsIgnoreCase ( 'ACT' ) || fas.equalsIgnoreCase ( 'IACT' ) || fas.equalsIgnoreCase ( 'DORM' ) ) &&
                         ( odps.equalsIgnoreCase ( 'A' ) || odps.equalsIgnoreCase ( 'I' ) ) )
	                    oisWrapperList.add(new OISWrapper(ois.Account_Number__c, ois.Opt_In_Status__c, ois.Id));
				}
                
                if ( oisWrapperlist.isEmpty () ){
					errorRetryMessage = 'Your information was not found. Please try again. If you are still having trouble, please call (877) 663-8648.';
	                errorRetry = true;
                    accountsReady = false;
                    return null;
                }
			}

			accountsReady = true;
		}

		return null;
	}

	public PageReference acceptSelection (){


			map_keys_account = '';
	        map_errors_account = new Map<Integer,String>();
	        for(Integer pos = 0; pos < oisWrapperList.size(); pos++){
	            if(oisWrapperList[pos].OptIn == 'D'){
	                map_errors_account.put(pos,'You must choose one of the options');
	                map_keys_account += ','+String.valueOf(pos);
	            }
	        }

	        if(!map_errors_account.isEmpty()){
	        	return null;
	        }

		try{

			List<OIS_Status__c> oisListToUpdate = new List<OIS_Status__c>();

			for(OISWrapper oisw: oisWrapperList){
				OIS_Status__c ois = new OIS_Status__c(Id=oisw.oisId, Opt_In_Status__c=oisw.OptIn,Response_Date__c=Datetime.now());
				oisListToUpdate.add(ois);
			}

			//update oisListToUpdate;
            access.updateObjects(oisListToUpdate);

			Set<Id> oisIds = new Set<Id>((new Map<Id,OIS_Status__c>(oisListToUpdate)).keySet());

			oisListToUpdate = [SELECT Id, Contact__c, Account_Number__c, City__c, Date_of_Birth__c, 
								Financial_Account__c, First_Name__c, Last_Name__c, Opt_In_Status__c,
								Phone__c, Response_Date__c, State__c, Zip_Code__c
								FROM OIS_Status__c WHERE Id IN: oisIds];

			List<OIS_Response__c> oisResponseToInsert = new List<OIS_Response__c>();

			for(OIS_Status__c ois: oisListToUpdate){
				oisResponseToInsert.add(new OIS_Response__c(Contact__c=ois.Contact__c,
															Acct_Number__c=ois.Account_Number__c,
															City__c=ois.City__c,
															Birthdate__c=ois.Date_of_Birth__c,
															Financial_Account__c=ois.Financial_Account__c,
															First_Name__c=ois.First_Name__c,
															Last_Name__c=ois.Last_Name__c,
															Opt_In_Status__c=ois.Opt_In_Status__c,
															Phone__c=ois.Phone__c,
                                                            Processing_Status__c='New',
															Response_Date__c=ois.Response_Date__c,
															State__c=ois.State__c,
															Zip_Code__c=ois.Zip_Code__c));

			}
			//insert oisResponseToInsert;
            access.insertObjects(oisResponseToInsert);
			success = true;
			accountsReady = false;
            error = false ;

		}catch(Exception e){
			errorMessage = 'There was a problem saving your selection';
			error = true;
			accountsReady = false;
			return null;
		}


		return null;
	}

	public List<SelectOption> getOptions(){
  		List<SelectOption> options = new List<SelectOption>();
  		options.add(new SelectOption('D', '-Select one-'));
        options.add(new SelectOption('Y', 'I want'));
        options.add(new SelectOption('N', 'I do not want'));
        return options;
   }       

	public class OISWrapper{
		public String AccountNumber {get;set;}
		public String OptIn {get;set;}
		private Id oisId;

		public OISWrapper(String an, String opt, Id oId){
			this.AccountNumber = '******'+an.substring(an.length()-4,an.length());
			this.OptIn = opt;
			this.oisId = oId;
		}

	}

	private void resetMaps(){
		errorMap.put('BirthDate', false);
		errorMap.put('socialSecurityNumber', false);
		errorMap.put('firstName', false);
		errorMap.put('lastName', false);
		errorMapMessages.put('BirthDate', '');
		errorMapMessages.put('socialSecurityNumber', '');
		errorMapMessages.put('firstName', '');
		errorMapMessages.put('lastName', '');
	}
}