global class batch_ScheduledTaskReassign_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_ScheduledTaskReassign b = new batch_ScheduledTaskReassign () ;
		Database.executeBatch ( b ) ;
   }
}