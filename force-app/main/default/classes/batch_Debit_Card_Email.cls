global class batch_Debit_Card_Email implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts
{
	private Batch_Process_Settings__c bps ;
	
	global batch_Debit_Card_Email ()
	{
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
	}
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query ;
		
		query  = '' ;
		query += 'SELECT ID, ' ;
		query += 'Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, ' ;
		query += 'Owner_Contact__r.FirstName, Owner_Contact__r.LastName, Owner_Contact__r.Email, ' ;
		query += 'Activation_Date__c, Issue_Date__c, Status__c, ' ;
		query += 'Card_Number_Masked__c, ' ;
		query += 'Primary_Checking__r.Acctnbr__c, ' ;
		query += 'Primary_Savings__r.Acctnbr__c ' ;
		query += 'From Service__c ' ;
		query += 'WHERE Type__c = \'Debit Card\' ' ;
		query += 'AND Issue_Date__c = LAST_N_DAYS:7 ' ;
		query += 'AND Contact__r.Email <> NULL ' ;
		query += 'AND Last_Notice_Date__c = NULL ' ;
		query += 'AND Status__c = \'Issued\' ' ;
		query += 'AND Activation_Date__c = NULL ' ;
		
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Service__c> iL )
	{
		list<Service__c> nSL = new list<Service__c> () ;
		
		Date d = Date.today () ;
		
		if ( ( iL != null ) && ( iL.size () > 0 ) )
		{
			for ( Service__c s : iL )
			{
				Service__c xs = new Service__c () ;
				xs.ID = s.ID ;
				xs.Last_Notice_Date__c = d ;
				
				nSL.add ( xs ) ;
			}
			
			boolean isSuccess = NewAccountEmailFunctions.sendNewDebitCardEmails ( iL ) ;
			
			if ( isSuccess )
			{
				try
				{
					update nsL ;
				}
				catch ( Exception e )
				{
					System.debug ( 'WTF' ) ;
				}
			}
		}
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex VM Debit Card Email Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}