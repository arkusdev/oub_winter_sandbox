public class TriggerFunctions
{
    //  Recordtype
    private static String webCaseRecordTypeId ;
    private static String taskRecordTypeId ;
    
    /*
     *  Generates the case based on information from the lead.
     *  Will return NULL if it's not a website lead.
     *
     *  Note that the case is not associated to the lead here!
     *  -  It's possible there's an existing contact to attach it to instead
     */
    public static Case createWebsiteCaseFromLead ( Lead inLead )
    {
        System.debug ( '========== Record type?! ==========' ) ;
        if ( String.isBlank ( webCaseRecordTypeId ) )
            webCaseRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Web Correspondence', 'Case' ) ;
        
        System.debug ( '========== Checking for case! ==========' ) ;

        boolean isValidWebsiteCase = false ;

        //  Create the case with the default stuff
        Case nC = new Case
        (
            priority = 'Medium' ,
            status = 'New' ,
            origin = 'Web',
            suppliedname = inLead.name ,
            suppliedemail = inLead.email
        ) ;
        
        if ( String.isNotBlank ( webCaseRecordTypeId ) )
            nC.recordTypeId = webCaseRecordTypeId ;
        
        if ( inLead.Website_Form__c == null )
        {
        	// do nothing
        }
        else if ( inLead.Website_Form__c.equals ( 'Customer Support' ) )
        {
            System.debug ( '----- Customer Support -----' ) ;
            nC.subject = inLead.What_is_the_purpose_of_this_inquiry__c ;
            nC.reason = 'Customer Support/Contact Us Web Request' ;

            nC.suppliedPhone = inLead.phone ;
            nC.Evening_Phone__c = inLead.Evening_Phone__c ;
            nC.description = inLead.Message__c ;

            isValidWebsiteCase = true ;
        }
        else if ( inLead.Website_Form__c.equals ( 'Open Account' ) )
        {
            System.debug ( '----- Open Account -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Contributions' ) )
        {
            System.debug ( '----- Contributions -----' ) ;
            nC.subject = 'New Contribution' ;
            nC.reason = 'OUB Website Contribution Request' ;

            nC.suppliedPhone = inLead.phone ;
            nC.description = inLead.Message__c ;

            isValidWebsiteCase = true ;
        }
        else if ( inLead.Website_Form__c.equals ( 'Mortgages' ) )
        {
            System.debug ( '----- Business Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Consumer Mortgage' ) )
        {
            System.debug ( '----- Consumer Mortgage -----' ) ;
            
            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'MultiFamily Mortgage' ) )
        {
            System.debug ( '----- MultiFamily Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Stay Connected' ) )
        {
            System.debug ( '----- Stay Connected -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'UNITY Visa' ) )
        {
            System.debug ( '----- UNITY Visa -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Connections Matter' ) )
        {
            System.debug ( '----- Connections Matter -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Grand ReOpening' ) )
        {
            System.debug ( '----- Grand Re-Opening -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Waive' ) )
        {
            System.debug ( '----- Waive Program -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Backyard' ) )
        {
            System.debug ( '----- Yes In Our Backyard -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Home Loan Trust' ) )
        {
            System.debug ( '----- Home Loan Trust -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'First Time Home Buyer' ) )
        {
            System.debug ( '----- First Time Home Buyer -----' ) ;
            
            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'Advocate' ) )
        {
            System.debug ( '----- Advocate -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equals ( 'OneTransaction' ) )
        {
            System.debug ( '----- OneTransaction -----' ) ;

            // do nothing!
        }

        if ( isValidWebsiteCase )
        {
            System.debug ( '========== Returning created case ==========' ) ;
            return nC ;
        }

        System.debug ( '========== No case to return ==========' ) ;
        return null ;
    }

    /*
     *  Creates a task as a record of the person applying to the website.
     */
    public static Task createWebsiteTaskFromLead ( Lead inLead )
    {
        System.debug ( '========== Checking for task! ==========' ) ;

        System.debug ( '========== Record type?! ==========' ) ;
        if ( String.isBlank ( taskRecordTypeId ) )
            taskRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Task', 'Task' ) ;
            
        boolean isValidWebsiteTask = false ;

        //  Create the task with the default stuff
        Task nT = new Task
        (
            priority = 'Medium' ,
            status = 'New'
        ) ;
        
        if ( String.isNotBlank ( taskRecordTypeId ) )
            nT.recordTypeId = taskRecordTypeId ;

        if ( inLead.Website_Form__c == null )
        {
        	// do nothing
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Customer Support' ) )
        {
            System.debug ( '----- Customer Support -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Open Account' ) )
        {
            System.debug ( '----- Open Account -----' ) ;

            nT.callType = 'Outbound' ;
            nT.status = 'Completed' ;
            nT.description = inLead.ProductSelection__c ;
            nT.subject = 'OUB Website New Account Prospect' ;

            nT.First_name__c = inLead.FirstName ;
            nT.Last_name__c = inLead.LastName ;
            nT.Form_Email__c = inLead.Email ;
//            nT.Form_Phone__c = inLead.Phone ;

            isValidWebsiteTask = true ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Contributions' ) )
        {
            System.debug ( '----- Contributions -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Mortgages' ) )
        {
            System.debug ( '----- Business Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'MultiFamily Mortgage' ) )
        {
            System.debug ( '----- MultiFamily Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Consumer Mortgage' ) )
        {
            System.debug ( '----- Consumer Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Stay Connected' ) )
        {
            System.debug ( '----- Stay Connected -----' ) ;

            nT.callType = 'Outbound' ;
            nT.status = 'Completed' ;
            nT.description = inLead.ProductSelection__c ;
            nT.subject = 'OUB Website Stay Connected Submission' ;

            nT.First_name__c = inLead.FirstName ;
            nT.Last_name__c = inLead.LastName ;
            nT.Form_Email__c = inLead.Email ;
            nT.Form_Phone__c = inLead.Phone ;

            isValidWebsiteTask = true ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'UNITY Visa' ) )
        {
            System.debug ( '----- UNITY Visa -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Connections Matter' ) )
        {
            System.debug ( '----- Connections Matter -----' ) ;
			String cmRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Connections Matter', 'Task' ) ;
            
            nT.recordTypeId = cmRecordTypeId ;
            nT.First_name__c = inLead.FirstName ;
            nT.Last_name__c = inLead.LastName ;
            nT.Form_Email__c = inLead.Email ;
            nT.Form_Phone__c = inLead.Phone ;

            nT.callType = 'Outbound' ;
            nT.status = 'Completed' ;
            nT.description = inLead.ProductSelection__c ;
            nT.subject = 'Connections Matter' ;
            
            nT.Financial_Priority_Business__c = inLead.Financial_Priority_Business__c ;
            nT.Financial_Priority_Credit__c = inLead.Financial_Priority_Credit__c ;
            nT.Financial_Priority_Home_Finance__c = inLead.Financial_Priority_Home_Finance__c ;
            nT.Financial_Priority_Literacy__c = inLead.Financial_Priority_Literacy__c ;
            nT.Financial_Priority_Online_Banking__c = inLead.Financial_Priority_Online_Banking__c ;
            nT.Financial_Priority_Retirement__c = inLead.Financial_Priority_Retirement__c ;
            nT.Financial_Priority_Savings__c = inLead.Financial_Priority_Savings__c ;

            isValidWebsiteTask = true ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Grand ReOpening' ) )
    	{
            System.debug ( '----- Grand Re-Opening -----' ) ;
            
            // do nothing!
    	}
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Advocate' ) )
        {
            System.debug ( '----- Advocate -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'OneTransaction' ) )
        {
            System.debug ( '----- OneTransaction -----' ) ;

            // do nothing!
        }

        if ( isValidWebsiteTask )
        {
            System.debug ( '========== Returning created task ==========' ) ;
            return nT ;
        }

        System.debug ( '========== No task to return ==========' ) ;
        return null ;
    }
    
    /*
     *  Generates an attendee from a contact.
     *  Will return NULL if it's not a website lead.
     */
    public static Workshop_Attendee__c createAttendeeFromLead ( Lead inLead )
    {
        System.debug ( '========== Checking for attendee! ==========' ) ;

        boolean isValidAttendee = false ;

		//  Default attendee
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		
        if ( inLead.Website_Form__c == null )
        {
        	// do nothing
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Customer Support' ) )
        {
            System.debug ( '----- Customer Support -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Open Account' ) )
        {
            System.debug ( '----- Open Account -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Contributions' ) )
        {
            System.debug ( '----- Contributions -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Mortgages' ) )
        {
            System.debug ( '----- Business Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'MultiFamily Mortgage' ) )
        {
            System.debug ( '----- MultiFamily Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Consumer Mortgage' ) )
        {
            System.debug ( '----- Consumer Mortgage -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Stay Connected' ) )
        {
            System.debug ( '----- Stay Connected -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'UNITY Visa' ) )
        {
            System.debug ( '----- UNITY Visa -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Connections Matter' ) )
        {
            System.debug ( '----- Connections Matter -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Grand ReOpening' ) )
    	{
            System.debug ( '----- Grand Re-Opening -----' ) ;
            
			String workshopId = Web_Settings__c.getInstance ( 'Web Forms' ).Crenshaw_RSVP_Workshop_ID__c ;
			
			wa.First_Name__c = inLead.FirstName ;
			wa.Last_Name__c = inLead.LastName ;
			wa.Email_Address__c = inLead.Email ;
			wa.Phone_Number__c = inLead.Phone ;
			
            wa.Workshop_ID__c = workshopId ;
            wa.Lead__c = inLead.id ;
            
            isValidAttendee = true ;
    	}
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Waive' ) )
        {
            System.debug ( '----- Waive Program -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Backyard' ) )
        {
            System.debug ( '----- Yes In Our Backyard -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Home Loan Trust' ) )
        {
            System.debug ( '----- Home Loan Trust -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'First Time Home Buyer' ) )
        {
            System.debug ( '----- First Time Home Buyer -----' ) ;
            
            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Advocate' ) )
        {
            System.debug ( '----- Advocate -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'OneTransaction' ) )
        {
            System.debug ( '----- OneTransaction -----' ) ;

			String workshopId = Web_Settings__c.getInstance ( 'Web Forms' ).OneTransaction_Workshop_ID__c ;
			
			wa.First_Name__c = inLead.FirstName ;
			wa.Last_Name__c = inLead.LastName ;
			wa.Email_Address__c = inLead.Email ;
			wa.Phone_Number__c = inLead.Phone ;
			
            wa.Workshop_ID__c = workshopId ;
            wa.Lead__c = inLead.id ;
            
            isValidAttendee = true ;
        }
    	
		if ( isValidAttendee )
		{
            System.debug ( '========== Returning created attendee ==========' ) ;
            
			return wa ;
		}
    	
        System.debug ( '========== No attendee to return ==========' ) ;
        return null ;
    }
    
    /*
     *  Generates an opportunity from a contact.
     *  Will return NULL if it's not a website lead.
     */
    public static oneOpportunity__c createOpportunityFromLead ( Lead inLead )
    {
        System.debug ( '========== Checking for opportunity! ==========' ) ;

		//  Blank Opportunity
		oneOpportunity__c oo = null ;
    	
        if ( inLead.Website_Form__c == null )
        {
        	// do nothing
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Customer Support' ) )
        {
            System.debug ( '----- Customer Support -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Open Account' ) )
        {
            System.debug ( '----- Open Account -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Contributions' ) )
        {
            System.debug ( '----- Contributions -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Mortgages' ) )
        {
            System.debug ( '----- Business Mortgage -----' ) ;
            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.Address_1__c = inLead.Street ;
			oo.City__c = inLead.City ;
			oo.State__c = inLead.State ;
			oo.Zip_Code__c = inLead.PostalCode ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'Web Site' ;
			oo.Interested_in_Product__c = 'Commercial Real Estate' ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'MultiFamily Mortgage' ) )
        {
            System.debug ( '----- MultiFamily Mortgage -----' ) ;

            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.Address_1__c = inLead.Street ;
			oo.City__c = inLead.City ;
			oo.State__c = inLead.State ;
			oo.Zip_Code__c = inLead.PostalCode ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'Web Site' ;
			oo.Interested_in_Product__c = 'Multifamily' ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Consumer Mortgage' ) )
        {
            System.debug ( '----- Consumer Mortgage -----' ) ;

            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.Address_1__c = inLead.Street ;
			oo.City__c = inLead.City ;
			oo.State__c = inLead.State ;
			oo.Zip_Code__c = inLead.PostalCode ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'Web Site' ;
			oo.Interested_in_Product__c = 'Single Family Home' ;
			oo.Type__c = 'Purchase' ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Stay Connected' ) )
        {
            System.debug ( '----- Stay Connected -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'UNITY Visa' ) )
        {
            System.debug ( '----- UNITY Visa -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Connections Matter' ) )
        {
            System.debug ( '----- Connections Matter -----' ) ;

            // do nothing!
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Grand ReOpening' ) )
    	{
            System.debug ( '----- Grand ReOpening -----' ) ;

            // do nothing!
    	}
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Waive' ) )
        {
            System.debug ( '----- Waive Program -----' ) ;

            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.State__c = 'XX' ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'Waive' ;
			oo.Interested_in_Product__c = 'Single Family Home' ;
			oo.Type__c = 'Purchase' ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Backyard' ) )
        {
            System.debug ( '----- Yes In Our Backyard -----' ) ;

            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.State__c = 'FL' ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'Yes In Our Backyard' ;
			oo.Interested_in_Product__c = 'Single Family Home' ;
			oo.Type__c = 'Purchase' ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'Home Loan Trust' ) )
        {
            System.debug ( '----- Home Loan Trust -----' ) ;

            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.State__c = inLead.State ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'Home Loan Trust' ;
			oo.Interested_in_Product__c = 'Single Family Home' ;
			oo.Type__c = 'Purchase' ;
        }
        else if ( inLead.Website_Form__c.equalsIgnoreCase ( 'First Time Home Buyer' ) )
        {
            System.debug ( '----- First Time Home Buyer -----' ) ;

            String rtId = OneUnitedUtilities.getRecordTypeId ( 'Loan Opportunity' ) ;
            
            oo = new oneOpportunity__c () ;
            
            oo.recordTypeId = rtId ;
			oo.First_Name__c = inLead.FirstName + ' ' + inLead.LastName ;
			oo.Email__c = inLead.Email ;
			oo.Home_Phone__c = inLead.Phone ;
			oo.State__c = inLead.State ;
			oo.Priority__c = 'Medium' ;
			oo.Status__c = 'New' ;
			oo.Opportunity_Source__c = 'First Time Home Buyer' ;
			oo.Interested_in_Product__c = 'Single Family Home' ;
			oo.Type__c = 'Purchase' ;
        }
        else if ( inLead.Website_Form__c.equals ( 'Advocate' ) )
        {
            System.debug ( '----- Advocate -----' ) ;

            // do nothing!
        }
    	
        return oo ;
    }
    
    /**
     *  Copies the stuff we want from the incoming lead to the contact.
     */
    public static Contact updateContactFromLead ( Contact eC, Lead iL )
	{
		eC.ProductInterest__c = iL.ProductInterest__c ;
		eC.Referred_By__c = iL.Referred_By__c ;
		eC.What_is_your_intended_opening_deposit__c = iL.What_is_your_intended_opening_deposit__c ;
		
		eC.Do_you_have_a_ChexSystems_record__c = iL.Do_you_have_a_ChexSystems_record__c ;
		
        if ( String.isNotBlank ( iL.LeadSource ) )
			eC.LeadSource = iL.LeadSource ;
        
        eC.Website_Form__c = iL.Website_Form__c ;
        
		eC.OneUnitedNewsletter__c = iL.OneUnited_Newsletter__c ;
		eC.NewsList__c = iL.News_List__c ;
		eC.Newsletter__c = iL.Newsletter__c ;
		eC.EventsList__c = iL.Events_List__c ;

		eC.UTM_Campaign__c = iL.UTM_Campaign__c ;
		eC.UTM_Content__c = iL.UTM_Content__c ;
		eC.UTM_Medium__c = iL.UTM_Medium__c ;
		eC.UTM_Source__c = iL.UTM_Source__c ;
		eC.UTM_Term__c = iL.UTM_Term__c ;
		eC.UTM_VisitorID__c = iL.UTM_VisitorID__c ;

        eC.Advocate_Text__c = iL.Advocate_Text__c ;
        eC.Advocate__c = iL.Advocate__c ;
        if ( iL.Advocate__c )
            eC.Advocate_Timestamp__c = System.now () ;
        
        eC.Ambassador__c = iL.Ambassador__c ;
        if ( iL.Ambassador__c )
            eC.Ambassador_Timestamp__c = System.now () ;
        
        eC.Facebook_URL__c = iL.Facebook_URL__c ;
        eC.Instagram_Handle__c = iL.Instagram_Handle__c ;
        eC.Twitter_Handle__c = iL.Twitter_Handle__c ;
        
        eC.OneTransaction_Focus__c = iL.OneTransaction_Focus__c ;
		
		return eC ;
	}

    /**
     *  Copies the stuff we want from the incoming lead to the contact.
     */
    public static Lead updateLeadFromLead ( Lead eL, Lead iL )
	{
        eL.Title = iL.Title ;
		eL.FirstName = iL.FirstName ;
		eL.LastName = iL.LastName ;
		eL.Phone = iL.Phone ;
        eL.MobilePhone = iL.MobilePhone ;
        
        eL.Website_Form__c = iL.Website_Form__c ;
        
        eL.Work_Email__c = iL.Work_Email__c ;
		
        eL.Organization__c = iL.Organization__c ;
        
		eL.Street = iL.Street ;
		eL.City = iL.City ;
		eL.State = iL.State ;
		eL.PostalCode = iL.PostalCode ;
        eL.Country = iL.Country ;
        
        eL.Description = iL.Description ;
        eL.LeadSource = iL.LeadSource ;
        
		eL.ProductInterest__c = iL.ProductInterest__c ;
		eL.Referred_By__c = iL.Referred_By__c ;
		eL.What_is_your_intended_opening_deposit__c = iL.What_is_your_intended_opening_deposit__c ;
		
		eL.Do_you_have_a_ChexSystems_record__c = iL.Do_you_have_a_ChexSystems_record__c ;
		
		eL.OneUnited_Newsletter__c = iL.OneUnited_Newsletter__c ;
		eL.News_List__c = iL.News_List__c ;
		eL.Newsletter__c = iL.Newsletter__c ;
		eL.Events_List__c = iL.Events_List__c ;
		
		eL.UTM_Campaign__c = iL.UTM_Campaign__c ;
		eL.UTM_Medium__c = iL.UTM_Medium__c ;
		eL.UTM_Source__c = iL.UTM_Source__c ;
		eL.UTM_Content__c = iL.UTM_Content__c ;
		eL.UTM_Term__c = iL.UTM_Term__c ;
		eL.UTM_VisitorID__c = iL.UTM_VisitorID__c ;
        
        eL.Advocate_Text__c = iL.Advocate_Text__c ;
        eL.Advocate__c = iL.Advocate__c ;
		if ( iL.Advocate__c )
            eL.Advocate_Timestamp__c = System.now () ;
        
        eL.Ambassador__c = iL.Ambassador__c ;
        if ( iL.Ambassador__c )
            eL.Ambassador_Timestamp__c = System.now () ;
        
        eL.OneTransaction_Focus__c = iL.OneTransaction_Focus__c ;
        
        eL.Facebook_URL__c = iL.Facebook_URL__c ;
        eL.Instagram_Handle__c = iL.Instagram_Handle__c ;
        eL.Twitter_Handle__c = iL.Twitter_Handle__c ;
        
        eL.HasOptedOutOfEmail = iL.HasOptedOutOfEmail ;
        eL.DoNotCall = iL.DoNotCall ;
        
        eL.OneTransaction_Focus__c = iL.OneTransaction_Focus__c ;
		
		return eL ;
	}
	
	public static Contact createContactFromLead ( Lead iL )
	{
		String recordTypeId = OneUnitedUtilities.getRecordTypeId ( 'Prospect' ) ;
		
		Contact c = new Contact () ;
		c.recordTypeId = recordTypeId ;

		//  Default Fields
        c.Title = iL.title ;
		c.FirstName = iL.FirstName ;
		c.LastName = iL.LastName ;
		c.Email = iL.email ;

		c.MailingStreet = iL.Street ;
		c.MailingCity = iL.City ;
		c.MailingState = iL.State ;
		c.MailingPostalCode = iL.PostalCode ;
		c.MailingCountry = iL.Country ;
		
		c.Phone = iL.Phone ;
		c.MobilePhone = iL.MobilePhone ;
		
		c.Description = iL.Description ;
		c.LeadSource = iL.LeadSource ;
		c.HasOptedOutOfEmail = iL.HasOptedOutOfEmail ;
		c.DoNotCall = iL.DoNotCall ;
		
		//  Custom fields
		c.Website_Form__c = iL.Website_Form__c ;

        c.WorkEmail__c = iL.Work_Email__c ;
        c.Organization__c = iL.Organization__c ;
        
		c.ProductInterest__c = iL.ProductInterest__c ;
		c.Referred_By__c = iL.Referred_By__c ;
		c.What_is_your_intended_opening_deposit__c = iL.What_is_your_intended_opening_deposit__c ;
		
		c.Do_you_have_a_ChexSystems_record__c = iL.Do_you_have_a_ChexSystems_record__c ;
		
		c.LeadSource = iL.LeadSource ;
		c.OneUnitedNewsletter__c = iL.OneUnited_Newsletter__c ;
		c.NewsList__c = iL.News_List__c ;
		c.Newsletter__c = iL.Newsletter__c ;
		c.EventsList__c = iL.Events_List__c ;

		c.UTM_Campaign__c = iL.UTM_Campaign__c ;
		c.UTM_Content__c = iL.UTM_Content__c ;
		c.UTM_Medium__c = iL.UTM_Medium__c ;
		c.UTM_Source__c = iL.UTM_Source__c ;
		c.UTM_Term__c = iL.UTM_Term__c ;
		c.UTM_VisitorID__c = iL.UTM_VisitorID__c ;
		
        c.Advocate_Text__c = iL.Advocate_Text__c ;
        c.Advocate__c = iL.Advocate__c ;
        if ( iL.Advocate__c )
            c.Advocate_Timestamp__c = System.now () ;
        
        c.Ambassador__c = iL.Ambassador__c ;
        if ( iL.Ambassador__c )
            c.Ambassador_Timestamp__c = System.now () ;
        
        c.Facebook_URL__c = iL.Facebook_URL__c ;
        c.Instagram_Handle__c = iL.Instagram_Handle__c ;
        c.Twitter_Handle__c = iL.Twitter_Handle__c ;
		
        c.OneTransaction_Focus__c = iL.OneTransaction_Focus__c ;
        
		insert c ;
		
		return c ;
	}
}