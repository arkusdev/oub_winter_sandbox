@isTest
public class TestAdvocateBadgeAwardedTrigger 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Contact getContact ( Integer x )
	{
        Account a = new Account();
        a.Name= 'Account'+x;
        insert a;
        
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
        c.AccountID=a.ID;
		
		return c ;
	}
	
    private static Referral_Code__c getReferralCode ( String x, Contact c )
    {
        Referral_Code__c rc = new Referral_Code__c () ;
        rc.Contact__c = c.ID ;
        rc.Name = x ;
        
        return rc ;
    }
	
	//  --------------------------------------------------------------------------------------
	//  Badgers
	//  --------------------------------------------------------------------------------------
    public static testmethod void TEST_Badges ()
    {
        list<Advocate_Badge__c> abL = new list<Advocate_Badge__c> () ;
        
        Advocate_Badge__c aa1 = new Advocate_Badge__c () ;
        aa1.API_Name__c = 'BADGE1' ;
        aa1.Description__c = 'XYZ123' ;
        aa1.Badge_Points__c = 100 ;
        
        abL.add ( aa1 ) ;
        
        Advocate_Badge__c aa2 = new Advocate_Badge__c () ;
        aa2.API_Name__c = 'BADGE2' ;
        aa2.Description__c = 'XYZ123' ;
        aa2.Badge_Points__c = 1000 ;
        
        abL.add ( aa2 ) ;
        
        insert abL ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Referral_Code__c rc1 = getReferralCode ( 'AXYA', c1 ) ;
        insert rc1 ;
        Test.setCreatedDate ( rc1.ID, Datetime.now().addDays ( -10 ) ) ;

        Referral_Code__c rc2 = getReferralCode ( 'AXYB', c1 ) ;
        insert rc2 ;
        Test.setCreatedDate ( rc2.ID, Datetime.now().addDays ( -5 ) ) ;

        Contact c2 = getContact ( 2 ) ;
        insert c2 ;

        Referral_Code__c rc3 = getReferralCode ( 'BXYA', c2 ) ;
        insert rc3 ;
        Test.setCreatedDate ( rc3.ID, Datetime.now().addDays ( -15 ) ) ;
        
        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar11 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar11.contactID = c1.ID ;
        apaafapar11.requestName = 'BADGE1' ;
        apaafapar11.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar11 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar12 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar12.contactID = c1.ID ;
        apaafapar12.requestName = 'BADGE1' ;
        apaafapar12.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar12 ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar2 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar2.contactID = c2.ID ;
        apaafapar2.requestName = 'BADGE2' ;
        apaafapar2.requestType = 'Badge' ;
        
        apaafaparL.add ( apaafapar2 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL ) ;
        
        List<Advocate_Badge_Awarded__c> abaL1 = [
            SELECT ID, Referral_Code__C
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( abaL1.size (), 1 ) ;
        System.assertEquals ( abaL1 [ 0 ].Referral_Code__C, rc2.ID ) ;
        
        List<Advocate_Badge_Awarded__c> abaL2 = [
            SELECT ID, Referral_Code__C
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c2.ID 
        ] ;
        
        System.assertEquals ( abaL2.size (), 1 ) ;
        System.assertEquals ( abaL2 [ 0 ].Referral_Code__C, rc3.ID ) ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest apaafapar3 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        apaafapar3.contactID = c1.ID ;
        apaafapar3.requestName = 'BADGE1' ;
        apaafapar3.requestType = 'Badge' ;
        
        list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> apaafaparL2 = new list<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        apaafaparL2.add ( apaafapar3 ) ;
        
        AdvocateProgramAwardInvocationFunction.advocateProgramAward ( apaafaparL2 ) ;
        
        List<Advocate_Badge_Awarded__c> abaL1D = [
            SELECT ID, Referral_Code__C
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertEquals ( abaL1D.size (), 1 ) ;
    }
}