@isTest
public class IBMWatsonWAVtoSpeechQueueTest 
{
    private static void insertCustomSettings ()
    {
        IBM_Watson_Settings__c iws = new IBM_Watson_Settings__c () ;
        iws.API_IAM_Key__c = 'abc123' ;
        iws.API_Key__c = '123abc' ;
        iws.API_URL__c = 'http://baby.you.and.me/recognize' ;
        iws.API_Callout_Limit__c = 2.0 ;
        
        insert iws ;
    }
    
    private static Case getCase ( Integer i )
    {
        Case c = new Case () ;
        c.Subject = 'Test ' + i + ' case.' ;
        
        return c ;
    }
    
    private static Attachment getAttachment ( ID xID )
    {
        Attachment a = new Attachment () ;
        a.ParentId = xID ;
		a.Name = 'Attach' + xID + '.wav' ;
        a.ContentType = 'audio/wav' ;
		a.IsPrivate = false ;

		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a.body = bodyBlob1 ;
        
        return a ;
    }
    
    private static ContentDocumentLink getContentCrap ( ID xID )
    {
        ContentVersion cv = new ContentVersion () ;
        cv.title = 'AUDIO' ;
        cv.PathOnClient = 'AUDIO_' + xID + '_FILE.wav' ;
        cv.Description = 'AUDIO FILE?' ;
        
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
        cv.VersionData = bodyBlob1 ;
        
        insert cv ;
        
        ContentVersion xcv = [
            SELECT ID, ContentDocumentId 
            FROM ContentVersion
            WHERE ID = :cv.ID 
        ] ;
        
        ContentDocumentLink cdl = new ContentDocumentLink () ;
        cdl.LinkedEntityId = xID ;
        cdl.ContentDocumentId = xcv.ContentDocumentId ;
        cdl.ShareType = 'V' ;
        cdl.Visibility = 'AllUsers' ;
        
        insert cdl ;
        
        return cdl ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Test
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TEST_Attachment ()
    {
        insertCustomSettings () ;
        
        //  Setup
        Case c1 = getCase ( 1 ) ;
        insert c1 ;
        
        Attachment a1 = getAttachment ( c1.ID ) ;
        insert a1 ;
        
        Case c2 = getCase ( 2 ) ;
        c2.Description = 'Foo Bar' ;
        insert c2 ;
        
        Attachment a2 = getAttachment ( c2.ID ) ;
        insert a2 ;
        
        list<ID> xL = new list<ID> () ;
        xL.add ( c1.ID ) ;
        xL.add ( c2.ID ) ;
        
        IBMWatsonMultiMockResponse mockResponse = new IBMWatsonMultiMockResponse () ;
        Test.setMock ( HttpCalloutMock.class, mockResponse ) ;
            
        //  Go!
        Test.startTest () ;
        
        IBMWatsonWAVtoSpeechQueueable q = new IBMWatsonWAVtoSpeechQueueable ( xL ) ;
        
        System.enqueueJob ( q ) ;
        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Test
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TEST_Attachment_FAIL ()
    {
        insertCustomSettings () ;
        
        //  Setup
        Case c1 = getCase ( 1 ) ;
        insert c1 ;
        
        Attachment a1 = getAttachment ( c1.ID ) ;
        insert a1 ;
        
        list<ID> xL = new list<ID> () ;
        xL.add ( c1.ID ) ;
        
        IBMWatsonMultiMockResponse mockResponse = new IBMWatsonMultiMockResponse () ;
        IBMWatsonMultiMockResponse.endPointResponseCode = 400 ;
        Test.setMock ( HttpCalloutMock.class, mockResponse ) ;
            
        //  Go!
        Test.startTest () ;
        
        IBMWatsonWAVtoSpeechQueueable q = new IBMWatsonWAVtoSpeechQueueable ( xL ) ;
        
        System.enqueueJob ( q ) ;
        
        Test.stopTest () ;
    }
    
	/* --------------------------------------------------------------------------------
	 *  Test
	 * -------------------------------------------------------------------------------- */
	public static testmethod void TEST_ContentCrap ()
    {
        insertCustomSettings () ;
        
        //  Setup
        Case c1 = getCase ( 1 ) ;
        insert c1 ;
        
        ContentDocumentLink cdl = getContentCrap ( c1.ID ) ;
        
        list<ID> xL = new list<ID> () ;
        xL.add ( c1.ID ) ;
        
        IBMWatsonMultiMockResponse mockResponse = new IBMWatsonMultiMockResponse () ;
        Test.setMock ( HttpCalloutMock.class, mockResponse ) ;
            
        //  Go!
        Test.startTest () ;
        
        IBMWatsonWAVtoSpeechQueueable q = new IBMWatsonWAVtoSpeechQueueable ( xL ) ;
        System.enqueueJob ( q ) ;
        
        Test.stopTest () ;
    }
    
    //
    //  Test IBMWatsonResponse HACKED.
    //
    //
    static testMethod void testIBMWatsonResponseHACKED () 
    {
        Test.startTest();
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(400);
        resp.setStatus('Success');
        resp.setBodyAsBlob(Blob.valueOf('{ "code_description": "Bad Request", "code": 400, "error": "No speech detected for 30s." }'));
        resp.setHeader('key', 'value');
        resp.setHeader('Content-Type', 'text/plain');
        IBMWatsonResponse response= new IBMWatsonResponse('url', resp);
        System.assertEquals(response.getStatus(), 'Success');
        System.assertEquals(response.getHeaderKeys()[1], 'Content-Type');
        System.assertEquals(response.getErrorMessage(), 'No speech detected for 30s.');
        Test.stopTest();
    }
}