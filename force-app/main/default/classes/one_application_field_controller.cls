public with sharing class one_application_field_controller {

	public String requiredSpan{get;set;}
	public String iconPopUp{get;set;}

	public one_application_field_controller(){
		
		requiredSpan = '<span class="starRequiredSpan" >&nbsp;*</span>';
		
		/*iconPopUp = '';		
		iconPopUp += '<a href="' + Label.one_Link_to_unity_visa_application_security + '" class="colorbox-600x450-popups cboxElement popUpIconClass">';
		iconPopUp += '<img src="https://www.oneunited.com/images/Padlock.gif" alt="security" />';
		iconPopUp += '</a>';*/
		
		iconPopUp = '';		
		iconPopUp += '<a href="javascript:openDialog(\'' + Label.one_Link_to_unity_visa_application_security + '\', \'Information\')" class="popUpIconClass">';
		iconPopUp += '<img src="https://www.oneunited.com/images/Padlock.gif" alt="security" />';
		iconPopUp += '</a>';
		
	}

}