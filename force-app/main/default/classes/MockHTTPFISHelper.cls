@isTest
public class MockHTTPFISHelper implements WebServiceMock
{
    public static boolean idvMatchingRecords = true ;
    public static Integer idvOID = 132456789 ;
    public static Integer ofacOID = 132456789 ;
	public static boolean idCheckOK = true ;
	public static boolean ofacHit = false ;
	public static String qfDecision = 'Accept' ;
    public static String productStrategyCd = 'A0200001' ;
    
	public static String quizResponse = 'Pass' ;
	public static String quizErrorCd = null ;
	public static String quizErrortext = null ;
	
    public static boolean isDead = false ;
    public static boolean isKid = false ;
    public static boolean isImpossibleSSN = false ;
    
	public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType) 
    {
		System.debug(LoggingLevel.INFO, 'MockHTTPFISHelper.doInvoke() - ' +
			'\n request: ' + request +
			'\n response: ' + response +
			'\n endpoint: ' + endpoint +
			'\n soapAction: ' + soapAction +
			'\n requestName: ' + requestName +
			'\n responseNS: ' + responseNS +
			'\n responseName: ' + responseName +
			'\n responseType: ' + responseType) ;

		if(request instanceOf v002ReplyobjectsSoapFisdsCom.changepassV001_element) 
        {
            v002ReplyobjectsSoapFisdsCom.changepassResponseV001_element e = new v002ReplyobjectsSoapFisdsCom.changepassResponseV001_element () ;
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v002ReplyobjectsSoapFisdsCom.fisauthenticate_element) 
        {
            //  Set and return random string for FIS login response
            v002ReplyobjectsSoapFisdsCom.fisauthenticateResponse_element e = new v002ReplyobjectsSoapFisdsCom.fisauthenticateResponse_element () ;
            e.fisauthenticateReturn = 'abc123xyz456' ;
            
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v002ReplyobjectsSoapFisdsCom.retrievePwdExpdate_element) 
        {
            v002ReplyobjectsSoapFisdsCom.retrievePwdExpdateResponse_element e = new v002ReplyobjectsSoapFisdsCom.retrievePwdExpdateResponse_element () ;
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v004QueryobjectsSoapFisdsCom.addIDVActions_element) 
        {
            v004QueryobjectsSoapFisdsCom.addIDVActionsResponse_element e = new v004QueryobjectsSoapFisdsCom.addIDVActionsResponse_element () ;
			response.put( 'response_x', e ) ;
		}
		else if(request instanceOf v004QueryobjectsSoapFisdsCom.addIDVOverride_element) 
        {
            v004QueryobjectsSoapFisdsCom.addIDVOverrideResponse_element e = new v004QueryobjectsSoapFisdsCom.addIDVOverrideResponse_element () ;
            e.addIDVOverrideReturn = true ;
            
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v004QueryobjectsSoapFisdsCom.addOFACOverride_element) 
        {
            v004QueryobjectsSoapFisdsCom.addOFACOverrideResponse_element e = new v004QueryobjectsSoapFisdsCom.addOFACOverrideResponse_element () ;
            e.addOFACOverrideReturn = true ;
            
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v004QueryobjectsSoapFisdsCom.applyIDVAccountStatus_element)
        {
            v004QueryobjectsSoapFisdsCom.applyIDVAccountStatusResponse_element e = new v004QueryobjectsSoapFisdsCom.applyIDVAccountStatusResponse_element () ;
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v004QueryobjectsSoapFisdsCom.callChexSystemsServiceV001_element) 
        {
            //  Top level object
            v004ConsumerReplyobjectsSoapFisdsCo.PersonReply pR = new v004ConsumerReplyobjectsSoapFisdsCo.PersonReply () ;
            pR.txtreport = '<html><head><title>Mock Report</title></head><body>Mocking a report!</body></html>' ;

            //  Second level, has most of the crap
			v004ConsumerReplyobjectsSoapFisdsCo.Consumer c = new v004ConsumerReplyobjectsSoapFisdsCo.Consumer () ;
            
            //  Ofac response
            v004ConsumerReplyobjectsSoapFisdsCo.OfacResponse ofR = new v004ConsumerReplyobjectsSoapFisdsCo.OfacResponse () ;
            
            //  Ofac log IDs
            v004ConsumerReplyobjectsSoapFisdsCo.OfacLogIDs ofli = new v004ConsumerReplyobjectsSoapFisdsCo.OfacLogIDs () ;
            ofli.oid = ofacOID ;
            
            //  Actual Ofac Hit
            v004ConsumerReplyobjectsSoapFisdsCo.OfacValidationsResult ofvr = new v004ConsumerReplyobjectsSoapFisdsCo.OfacValidationsResult () ;
            ofVr.ofacHit = ofacHit ;
            
            //  Load up ... OFAC
            ofR.ofacLogIds = ofli ;
            ofR.ofacValidationsResult = ofvr ;
            c.ofacResponse = ofR ;
            
            //  IDV Response
            v004ConsumerReplyobjectsSoapFisdsCo.IDMResponse idR = new v004ConsumerReplyobjectsSoapFisdsCo.IDMResponse () ;
            
            //  IDV Log IDs
            v004ConsumerReplyobjectsSoapFisdsCo.IDMLogIDs idL = new v004ConsumerReplyobjectsSoapFisdsCo.IDMLogIDs () ;
            idL.oid = idvOID ;
            
            //  Result
            v004ConsumerReplyobjectsSoapFisdsCo.IDMValidationsResult idV = new v004ConsumerReplyobjectsSoapFisdsCo.IDMValidationsResult () ;
            idV.idCheckHadMatchingRecords = idvMatchingRecords ;
            idV.idCheckOk = idCheckOK ;
            idV.isDeceased = isDead ;
            idV.minorYes = isKid ;
            idV.afterSSNDate = isImpossibleSSN ;
            
            //  Load up ... IDV
            idR.logids = idL ;
            idR.validationsResult = idV ;
            c.identityManagerResponse = idR ;
            
            //  Qualifile
            v004ConsumerReplyobjectsSoapFisdsCo.QualiFileResponse qR = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileResponse () ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.AccountActionInfo aai = new v004ConsumerReplyobjectsSoapFisdsCo.AccountActionInfo () ;
            aai.accountAcceptanceTxt = qfDecision ;
            aai.accountActionTxt1 = 'aat1' ;
            aai.accountActionTxt2 = 'aat1' ;
            aai.accountActionTxt3 = 'aat1' ;
            aai.accountActionTxt4 = 'aat1' ;
            aai.accountActionTxt5 = 'aat1' ;
            aai.accountActionTxt6 = 'aat1' ;
            aai.accountActionTxt7 = 'aat1' ;
            aai.accountActionTxt8 = 'aat1' ;
            aai.accountActionTxt9 = 'aat1' ;
            aai.accountActionTxt10 = 'aat1' ;

			v004ConsumerReplyobjectsSoapFisdsCo.QualiFileInformation qfi = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileInformation () ;
            qfi.scoreNbr = '456' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason1 qfr1 = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason1 () ;
            qfr1.reasonCd = 'abc' ;
            qfr1.reasonTxt = 'test' ;
            qfi.QualiFileReason1 = qfr1 ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason2 qfr2 = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason2 () ;
            qfr1.reasonCd = 'bcd' ;
            qfr1.reasonTxt = 'test' ;
            qfi.QualiFileReason2 = qfr2 ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason3 qfr3 = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason3 () ;
            qfr1.reasonCd = 'cde' ;
            qfr1.reasonTxt = 'test' ;
            qfi.QualiFileReason3 = qfr3 ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason4 qfr4 = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason4 () ;
            qfr1.reasonCd = 'def' ;
            qfr1.reasonTxt = 'test' ;
            qfi.QualiFileReason4 = qfr4 ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason5 qfr5 = new v004ConsumerReplyobjectsSoapFisdsCo.QualiFileReason5 () ;
            qfr1.reasonCd = 'efg' ;
            qfr1.reasonTxt = 'test' ;
            qfi.QualiFileReason5 = qfr5 ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductStrategyCd pscd = new v004ConsumerReplyobjectsSoapFisdsCo.ProductStrategyCd () ;
            pscd.productStrategyCd1 = productStrategyCd ;
            pscd.productStrategyCd2 = 'Blah' ;
            pscd.productStrategyCd3 = 'Blah' ;
            pscd.productStrategyCd4 = 'Blah' ;
            pscd.productStrategyCd5 = 'Blah' ;
            pscd.productStrategyCd6 = 'Blah' ;
            pscd.productStrategyCd7 = 'Blah' ;
            pscd.productStrategyCd8 = 'Blah' ;
            pscd.productStrategyCd9 = 'Blah' ;
            pscd.productStrategyCd10 = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.CrossSellInformation csi = new v004ConsumerReplyobjectsSoapFisdsCo.CrossSellInformation () ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer1 po1 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer1 () ;
            po1.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer2 po2 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer2 () ;
            po2.productOfferTxt = 'Blah' ;

            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer3 po3 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer3 () ;
            po3.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer4 po4 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer4 () ;
            po4.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer5 po5 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer5 () ;
            po5.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer6 po6 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer6 () ;
            po6.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer7 po7 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer7 () ;
            po7.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer8 po8 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer8 () ;
            po8.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer9 po9 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer9 () ;
            po9.productOfferTxt = 'Blah' ;
            
            v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer10 po10 = new v004ConsumerReplyobjectsSoapFisdsCo.ProductOffer10 () ;
            po10.productOfferTxt = 'Blah' ;
            
            //  Load up ... CrossSellInformation
            csi.productOffer1 = po1 ;
            csi.productOffer2 = po2 ;
            csi.productOffer3 = po3 ;
            csi.productOffer4 = po4 ;
            csi.productOffer5 = po5 ;
            csi.productOffer6 = po6 ;
            csi.productOffer7 = po7 ;
            csi.productOffer8 = po8 ;
            csi.productOffer9 = po9 ;
            csi.productOffer10 = po10 ;
            
            //  Load up ... Qualifile
            qR.accountActionInfo = aai ;
            qR.qualifileInformation = qfi ;
            qR.productStrategyCd = pscd ;
            qR.crossSellInformation = csi ;
            c.qualifileResponse = qR ;
            
            //  Final assignment ?!?
            pR.consumer = c ;
            
            //  Set and return
            v004QueryobjectsSoapFisdsCom.callChexSystemsServiceV001Response_element e = new v004QueryobjectsSoapFisdsCom.callChexSystemsServiceV001Response_element () ;
            e.callChexSystemsServiceV001Return = pR ;
            response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v004QueryobjectsSoapFisdsCom.chexAuthenticate_element) 
        {
            v004QueryobjectsSoapFisdsCom.chexAuthenticateResponse_element e = new v004QueryobjectsSoapFisdsCom.chexAuthenticateResponse_element () ;
			response.put ( 'response_x', e ) ;
		}
		else if(request instanceOf v003QueryobjectsSoapFisdsCom.idaAnswerV003_element) 
		{
			v003QueryobjectsSoapFisdsCom.idaAnswerV003Response_element x = new v003QueryobjectsSoapFisdsCom.idaAnswerV003Response_element () ;
			
			v003ConsumerReplyobjectsSoapFisdsCo.IdaDecision id = new v003ConsumerReplyobjectsSoapFisdsCo.IdaDecision () ;
			
			if ( String.isNotBlank ( quizResponse ) )
				id.decision = quizResponse ;
			else
				id.decision = 'Pass' ;
				
			if ( String.isNotBlank ( quizErrorCd ) )
				id.errorCd = quizErrorCd ;
			
			if ( String.isNotBlank ( quizErrorText ) )
				id.errorTxt = quizErrortext ;
			
			//  Challenge questions.
			id.idaQuiz = getQuestions () ;
			
			x.idaAnswerV003Return = id ;
			
			response.put( 'response_x', x );
		}
		else if ( request instanceOf v003QueryobjectsSoapFisdsCom.idaAuthenticate_element ) 
		{
			v003QueryobjectsSoapFisdsCom.idaAuthenticateResponse_element x = new v003QueryobjectsSoapFisdsCom.idaAuthenticateResponse_element () ;
			
			x.idaAuthenticateReturn = 'VALID-TEST-AUTH' ;
			
			response.put( 'response_x', x ) ;
		}
		else if ( request instanceOf v003QueryobjectsSoapFisdsCom.idaQuizV003_element ) 
		{
			v003QueryobjectsSoapFisdsCom.idaQuizV003Response_element x = new v003QueryobjectsSoapFisdsCom.idaQuizV003Response_element () ;
			
			x.idaQuizV003Return = getQuestions () ;

			response.put ( 'response_x', x ) ;
		}
	}
	
	private v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestions getQuestions ()
	{
		// LUIGI CHANGE - We require an actual response object so hack one together!
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestions y = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestions () ;
		y.quizId = '1234' ;
		y.transId = '123456' ;
		
		if ( String.isNotBlank ( quizErrorCd ) )
			y.errorCd = quizErrorCd ;
		
		if ( String.isNotBlank ( quizErrorText ) )
			y.errorTxt = quizErrortext ;
			
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q1 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q1.id = '0001' ;
		q1.questionText = 'Mock Question 1' ;
		q1.answer1Id = 'A101' ;
		q1.answer1Text = 'Mock Question 1 Answer 1' ;
		q1.answer2Id = 'A102' ;
		q1.answer2Text = 'Mock Question 1 Answer 2' ;
		q1.answer3Id = 'A103' ;
		q1.answer3Text = 'Mock Question 1 Answer 3' ;
		q1.answer4Id = 'A104' ;
		q1.answer4Text = 'Mock Question 1 Answer 4' ;
		q1.answer5Id = 'A105' ;
		q1.answer5Text = 'Mock Question 1 Answer 5' ;
		q1.answer6Id = 'A106' ;
		q1.answer6Text = 'Mock Question 1 Answer 6' ;
		
		y.idaQuestion1 = q1 ;
		
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q2 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q2.id = '0002' ;
		q2.questionText = 'Mock Question 2' ;
		q2.answer1Id = 'A201' ;
		q2.answer1Text = 'Mock Question 2 Answer 1' ;
		q2.answer2Id = 'A202' ;
		q2.answer2Text = 'Mock Question 2 Answer 2' ;
		q2.answer3Id = 'A203' ;
		q2.answer3Text = 'Mock Question 2 Answer 3' ;
		q2.answer4Id = 'A204' ;
		q2.answer4Text = 'Mock Question 2 Answer 4' ;
		q2.answer5Id = 'A205' ;
		q2.answer5Text = 'Mock Question 2 Answer 5' ;
		q2.answer6Id = 'A206' ;
		q2.answer6Text = 'Mock Question 2 Answer 6' ;
		
		y.idaQuestion2 = q2 ;
		
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q3 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q3.id = '0003' ;
		q3.questionText = 'Mock Question 3' ;
		q3.answer1Id = 'A301' ;
		q3.answer1Text = 'Mock Question 3 Answer 1' ;
		q3.answer2Id = 'A302' ;
		q3.answer2Text = 'Mock Question 3 Answer 2' ;
		q3.answer3Id = 'A303' ;
		q3.answer3Text = 'Mock Question 3 Answer 3' ;
		q3.answer4Id = 'A304' ;
		q3.answer4Text = 'Mock Question 3 Answer 4' ;
		q3.answer5Id = 'A305' ;
		q3.answer5Text = 'Mock Question 3 Answer 5' ;
		q3.answer6Id = 'A306' ;
		q3.answer6Text = 'Mock Question 3 Answer 6' ;
		
		y.idaQuestion3 = q3 ;
		
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q4 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q4.id = '0004' ;
		q4.questionText = 'Mock Question 4' ;
		q4.answer1Id = 'A401' ;
		q4.answer1Text = 'Mock Question 4 Answer 1' ;
		q4.answer2Id = 'A402' ;
		q4.answer2Text = 'Mock Question 4 Answer 2' ;
		q4.answer3Id = 'A403' ;
		q4.answer3Text = 'Mock Question 4 Answer 3' ;
		q4.answer4Id = 'A404' ;
		q4.answer4Text = 'Mock Question 4 Answer 4' ;
		q4.answer5Id = 'A405' ;
		q4.answer5Text = 'Mock Question 4 Answer 5' ;
		q4.answer6Id = 'A406' ;
		q4.answer6Text = 'Mock Question 4 Answer 6' ;
	
		y.idaQuestion4 = q4 ;
		
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q5 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q5.id = '0005' ;
		q5.questionText = 'Mock Question 5' ;
		q5.answer1Id = 'A501' ;
		q5.answer1Text = 'Mock Question 5 Answer 1' ;
		q5.answer2Id = 'A502' ;
		q5.answer2Text = 'Mock Question 5 Answer 2' ;
		q5.answer3Id = 'A503' ;
		q5.answer3Text = 'Mock Question 5 Answer 3' ;
		q5.answer4Id = 'A504' ;
		q5.answer4Text = 'Mock Question 5 Answer 4' ;
		q5.answer5Id = 'A505' ;
		q5.answer5Text = 'Mock Question 5 Answer 5' ;
		q5.answer6Id = 'A506' ;
		q5.answer6Text = 'Mock Question 5 Answer 6' ;
		
		y.idaQuestion5 = q5 ;
			
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q6 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q6.id = '0006' ;
		q6.questionText = 'Mock Question 6' ;
		q6.answer1Id = 'A601' ;
		q6.answer1Text = 'Mock Question 6 Answer 1' ;
		q6.answer2Id = 'A602' ;
		q6.answer2Text = 'Mock Question 6 Answer 2' ;
		q6.answer3Id = 'A603' ;
		q6.answer3Text = 'Mock Question 6 Answer 3' ;
		q6.answer4Id = 'A604' ;
		q6.answer4Text = 'Mock Question 6 Answer 4' ;
		q6.answer5Id = 'A605' ;
		q6.answer5Text = 'Mock Question 6 Answer 5' ;
		q6.answer6Id = 'A606' ;
		q6.answer6Text = 'Mock Question 6 Answer 6' ;
		
		y.idaQuestion6 = q6 ;
		
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q7 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q7.id = '0007' ;
		q7.questionText = 'Mock Question 7' ;
		q7.answer1Id = 'A701' ;
		q7.answer1Text = 'Mock Question 7 Answer 1' ;
		q7.answer2Id = 'A702' ;
		q7.answer2Text = 'Mock Question 7 Answer 2' ;
		q7.answer3Id = 'A703' ;
		q7.answer3Text = 'Mock Question 7 Answer 3' ;
		q7.answer4Id = 'A704' ;
		q7.answer4Text = 'Mock Question 7 Answer 4' ;
		q7.answer5Id = 'A705' ;
		q7.answer5Text = 'Mock Question 7 Answer 5' ;
		q7.answer6Id = 'A706' ;
		q7.answer6Text = 'Mock Question 7 Answer 6' ;
		
		y.idaQuestion7 = q7 ;

		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q8 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q8.id = '0008' ;
		q8.questionText = 'Mock Question 8' ;
		q8.answer1Id = 'A801' ;
		q8.answer1Text = 'Mock Question 8 Answer 1' ;
		q8.answer2Id = 'A802' ;
		q8.answer2Text = 'Mock Question 8 Answer 2' ;
		q8.answer3Id = 'A803' ;
		q8.answer3Text = 'Mock Question 8 Answer 3' ;
		q8.answer4Id = 'A804' ;
		q8.answer4Text = 'Mock Question 8 Answer 4' ;
		q8.answer5Id = 'A805' ;
		q8.answer5Text = 'Mock Question 8 Answer 5' ;
		q8.answer6Id = 'A806' ;
		q8.answer6Text = 'Mock Question 8 Answer 6' ;
		
		y.idaQuestion8 = q8 ;
		
		v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion q9 = new v003ConsumerReplyobjectsSoapFisdsCo.IdaQuestion () ;
		q9.id = '0009' ;
		q9.questionText = 'Mock Question 9' ;
		q9.answer1Id = 'A901' ;
		q9.answer1Text = 'Mock Question 9 Answer 1' ;
		q9.answer2Id = 'A902' ;
		q9.answer2Text = 'Mock Question 9 Answer 2' ;
		q9.answer3Id = 'A903' ;
		q9.answer3Text = 'Mock Question 9 Answer 3' ;
		q9.answer4Id = 'A904' ;
		q9.answer4Text = 'Mock Question 9 Answer 4' ;
		q9.answer5Id = 'A905' ;
		q9.answer5Text = 'Mock Question 9 Answer 5' ;
		q9.answer6Id = 'A906' ;
		q9.answer6Text = 'Mock Question 9 Answer 6' ;
		
		y.idaQuestion9 = q9 ;
	
		return y ;
	}
}