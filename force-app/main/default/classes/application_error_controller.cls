public class application_error_controller 
{
    /*  ----------------------------------------------------------------------------------------
     *  page objects
     *  ---------------------------------------------------------------------------------------- */
    public boolean displayCartCheckout { public get ; private set ; }
    public String cartCheckoutJSON { public get ; private set ; }
    public string recordUserJSON { public get; private set; }
    
    /*  ----------------------------------------------------------------------------------------
     *  Constructor
     *  ---------------------------------------------------------------------------------------- */
    public application_error_controller  ( ApexPages.Standardcontroller stdC )
    {
		//  --------------------------------------------------------------------------------------------------
		//  JIM cart checkout for google tag tracking
		//  --------------------------------------------------------------------------------------------------
		displayCartCheckout = false ;
        Cookie jCookie = ApexPages.currentPage().getCookies().get ( 'j_Cookie' ) ;
        
        //  First check for application in session
        one_application__c o = (one_application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
 
 		//  If no session present, use querystring instead
        if ( ( o == null ) || ( o.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...

			o = one_utils.getApplicationFromId ( aid ) ;
            
            //  Confirm kid matches
            if ( ! o.Upload_Attachment_Key__c.equalsIgnoreCase ( kid ) )
            {
                System.debug ( 'Key does not match' ) ;
                o = null ;
            }
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = o.Id ;
			kid = o.Upload_Attachment_Key__c ;
		}
		
		//  Check again for things existing
		if ( ( o != null ) && ( aid != null ) && ( kid != null ) )
		{
            //  --------------------------------------------------------------------------------------------------
            //  JIM user for google tag tracking
			//  --------------------------------------------------------------------------------------------------
            recordUserJSON = one_utils.generateUserJSON ( o ) ;
            
            //  --------------------------------------------------------------------------------------------------
            //  JIM cart checkout for google tag tracking
			//  --------------------------------------------------------------------------------------------------
            if ( jCookie != null )
            {
	            System.debug ( '========== Checking cookie ==========' ) ;
                String jFoo = jCookie.getValue () ;
                
                if ( jFoo.equalsIgnoreCase ( o.ID ) )
                    displayCartCheckout = true ;
                
                //  Remove the cookie
                Cookie jNYETCookie = new Cookie ( 'j_Cookie', '', null, 0, false ) ;
                ApexPages.currentPage().setCookies ( new Cookie[] { jNYETCookie } ) ;
            }
            
			if ( displayCartCheckout )
                cartCheckoutJSON = one_utils.generateCartCheckoutJSON ( o ) ;
        }
    }
}