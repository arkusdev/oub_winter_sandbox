@isTest
public with sharing class test_one_application_ob_registration
{
    private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
    
	/* --------------------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------------------- */
    private static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = false ;
        s.Direct_From_App__c = 'hello' ;
        s.Verify_ID_Attempts__c = 3 ;
        
        insert s;
        
        DI_Settings__c dis = new DI_Settings__c () ;

        dis.Client_ID__c = 'abc123' ;
        dis.Client_Secret__c = 'youandme' ;
        dis.Login_URL__c = 'https://diapis.digitalinsight.com/v1/oauth/token' ;
        dis.DI_FiId__c = '12345' ;
        dis.Register_URL__c = 'https://diapis.digitalinsight.com/registration/v4/fis/' + dis.DI_FiId__c + '/fiCustomers';
        dis.CustomerInfo_URL__c = 'https://diapis.digitalinsight.com/bankingservices/v2/fis/' + dis.DI_FiId__c + '/fiCustomers/';
        dis.Subscriptions_URL__c = 'https://diapis.digitalinsight.com/subscriptions/v1/fis/' + dis.DI_FiId__c + '/fiCustomers/';
        dis.Events_URL__c = 'https://diapis.digitalinsight.com/subscriptions/v1/fis/' + dis.DI_FiId__c + '/fiCustomers/';
        dis.Destinations_URL__c = 'https://diapis.digitalinsight.com/destinations/v2/fis/' + dis.DI_FiId__c + '/products/IB/notificationApps/MBL/fiCustomers/'; // assuming IB = product_id and MBL = notification_app_id for all customers
        dis.Last_Timestamp__c = null;
        dis.name = 'settings';
        
        insert dis ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Creates a generic application I use for testing.
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, ID recordTypeID )
	{
		one_application__c app = new one_application__c ();
        app.RecordTypeId = recordTypeID ;
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = '666-01-1111' ;
        app.Phone__c = c1.Phone ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
		
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = '666-02-1111' ;
            app.Co_Applicant_Phone__c = c2.Phone ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Funding_Options__c = 'E- Check (ACH)' ;
		app.Gross_Income_Monthly__c = 10000.00 ;

		app.FIS_Application_ID__c = 'null' ;
		app.FIS_Decision_Code__c = 'A001' ;
        
        app.OFAC_Transaction_ID__c = '1853285650';
        app.minFraud_ID__c = '33c23683-76e3-48dd-b0d6-5e9e11d5568f';
        
		app.Application_Processing_Status__c = 'Quiz Present' ;
        app.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        // Create accounts to attach to app
        List<Account> aList = new List<Account>();
        Account a1 = new Account();
        a1.Name = 'helloMyNameIs';
        a1.Tracking_Pixel__c = true;
        aList.add(a1);
        Account a2 = new Account();
        a2.Name = 'abcdef';
        a2.Tracking_Pixel__c = true;
        aList.add(a2);
        insert aList;
        
        app.Reservation_Vendor_ID__c = a1.Id;
        app.Reservation_Affiliate_Vendor__c = a2.Id;
        
        app.MothersMaidenName__c = 'smith';
        
		return app ;
	}

    /*
	 *  Tests blank session
	 */
	@isTest static void testOBRegisterBlankSession ()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HTTPCalloutMock.class, new MockHTTPDIHelper () ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create empty object
		one_application__c app = new one_application__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( app ) ;
		
		//  Feed the standard controller to the page controller
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 6000 ) ; // default
		
		//  End of testing
		Test.stopTest () ;
	}
	
	/*
	 * Tests with an existing application passed in:
	 * test for applicant already registered for online banking -- has one_application__c.Applicant_OB_Registered__c != null
	 */	
	@isTest static void testOBRegAlreadyRegistered()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HTTPCalloutMock.class, new MockHTTPDIHelper () ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        app.Applicant_OB_Registered__c = DateTime.now();
        insert app ;
        System.debug('app id: ' + app.Id);
        
        one_application__c appAfterInsert = one_utils.getApplicationFromId(app.Id);
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;

		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( appAfterInsert ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
		
        System.debug('app FIS Decision code: ' + app.FIS_Decision_Code__c);
        System.debug('application status: ' + app.Application_Status__c);
        System.debug('app minFraud id: ' + app.minFraud_ID__c);
        System.debug('app ofac trans id: ' + app.OFAC_Transaction_ID__c);
        System.debug('app fis app id: ' + app.FIS_Application_ID__c);
        
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 8000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
    
    /*
	 * Tests with an existing application passed in:
	 * test for applicant not already registered for online banking -- successful registration
	 */	
	@isTest static void testOBRegPASS()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        insert app ;
        
        one_Application__c appAfterInsert = one_utils.getApplicationFromId(app.Id);
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( appAfterInsert ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
		
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 5000 ) ;
        
        // Pass the verify identity check
        obRegPg.last4SSN = '1111';
        
        // Inputs for registration
        obRegPg.loginId = 'hellomylogin';
        obRegPg.userPw = 'mypassword1234!';
        
        // Click checkLoginIdBtn
        obRegPg.registerUsers();
        
        // Check resulting controller object
        System.assertEquals ( obRegPg.vStepNumber, 2000 );
        System.assertEquals ( ( obRegPg.o.Applicant_OB_Registered__c != null ), true );
        System.assertEquals ( obRegPg.alreadyRegistered(), true );
        
		//  End of testing
		Test.stopTest () ;
	}
    
    /*
	 * Tests with an existing application manually passed in:
	 * test for pagetimeout
	 */	
	@isTest static void testOBRegPageTimeout()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        insert app ;
        
        one_Application__c appAfterInsert = one_utils.getApplicationFromId(app.Id);
        one_Application__c eApp = new one_application__c();
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
        
        // manually pass in mismatched parameters
        ApexPages.currentPage().getParameters().put ( 'aid', app.id ) ;
        ApexPages.currentPage().getParameters().put ( 'kid', 'abc123') ;

		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eApp ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
        
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 6000 ) ;
        
        // Force pageTimeout
        obRegPg.pageTimeout();
        
        // Check resulting controller object
        System.assertEquals ( obRegPg.vStepNumber, 6000 );
        
        sc = new ApexPages.StandardController ( appAfterInsert ) ;
		obRegPg = new one_application_ob_registration ( sc ) ;
        
        System.debug('o: ' + obRegPg.o);
        System.debug('o.Application_ID__c: ' + obRegPg.o.Application_ID__c);
        
        obRegPg.goPageAction();
        
        // Force pageTimeout
        obRegPg.pageTimeout();
        
        // Check resulting controller object
        System.assertEquals ( obRegPg.vStepNumber,  5000 );
        
		//  End of testing
		Test.stopTest () ;
	}
    
    /*
	 * Tests with an existing application passed in:
	 * test for applicant not already registered for online banking -- 
	 * fail registration: username & pw entries don't meet reqs
	 */	
	@isTest static void testOBRegFAIL()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        
        Account a = new Account();
        a.Name = 'helloName';
        a.Tracking_Pixel__c = true;
        insert a;
        
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        app.Reservation_Vendor_ID__c = null;
        app.Reservation_Affiliate_Vendor_ID__c = a.Id;
        insert app ;
        
        one_Application__c appAfterInsert = one_utils.getApplicationFromId(app.Id);
        
        Service__c s = new Service__c();
        s.Alternate_Login_ID__c = 'alreadyExists';
        s.Type__c = 'Online Banking';
        s.RecordTypeId = '01238000000Lx7qAAC'; //Online Services
        s.Status__c = 'ACTIVE';
        insert s;
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( appAfterInsert ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
		
        System.debug('app.Reservation_Vendor_ID__c: ' + app.Reservation_Vendor_ID__c);
        System.debug('app.Reservation_Affiliate_Vendor_ID__c: ' + app.Reservation_Affiliate_Vendor_ID__c);
        
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 5000 ) ;
        
        // Pass the verify identity check
        obRegPg.last4SSN = '1111';
        
        // Inputs for registration
        obRegPg.loginId = '';
        obRegPg.userPw = 'mypassword!';
        
        // Click checkLoginIdBtn
        obRegPg.registerUsers();
        
        // Check resulting controller object
        System.assertEquals ( obRegPg.vStep1001, true );
        
        // Checking for username that already exists
        obRegPg.loginId = 'alreadyExists';
        
        obRegPg.registerUsers();
        
        System.assertEquals( obRegPg.vStep1002, true );
        
        // Checking for username that doesn't meet reqs
        obRegPg.loginId = 'abc';
        
        obRegPg.registerUsers();
        
        System.assertEquals( obRegPg.vStep1003, true );
        
        // Checking for username that is same as ssn
        obRegPg.loginId = '666011111';
        
        obRegPg.registerUsers();
        
        System.assertEquals( obRegPg.vStep1004, true );
        
        // Set valid username
        obRegPg.loginId = 'fakeuser123456';
        
        // Checking for blank pw entry
        obRegPg.userPw = '';
        
        obRegPg.registerUsers();
        
        System.assertEquals( obRegPg.vStep1005, true );
        
        // Checking for pw not meeting reqs
        obRegPg.userPw = 'mypass ';
        
        obRegPg.registerUsers();
        
        System.assertEquals( obRegPg.vStep1006, true );
        
		//  End of testing
		Test.stopTest () ;
	}
    
    @isTest static void testAlreadyRegisteredMethod() 
    {
        insertCustomSetting();
        
        // Setup
        List<Contact> cList = new List<Contact>();
        Contact c1 = getContact ( 1 ) ;
        cList.add(c1);
        Contact c2 = getContact ( 2 ) ;
        cList.add(c2);
        insert cList ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        List<one_application__c> appList = new List<one_application__c>();
        one_Application__c app1 = getApplicationForTests ( c1, null, rt ) ;
        appList.add(app1);
        one_Application__c app2 = getApplicationForTests ( c2, null, rt );
        appList.add(app2);
        insert appList ;
        
        Service__c s = new Service__c();
        s.Alternate_Login_ID__c = 'alreadyExists';
        s.Type__c = 'Online Banking';
        s.RecordTypeId = '01238000000Lx7qAAC'; //Online Services
        s.Status__c = 'ACTIVE';
        s.Contact__c = c1.Id;
        insert s;
        
        Test.startTest();
        
       	ApexPages.StandardController sc = new ApexPages.StandardController ( app1 ) ;
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
        
        boolean testForTrue = obRegPg.alreadyRegistered();
        
        System.assertEquals(testForTrue, true);
        
        sc = new ApexPages.StandardController ( app2 ) ;
		obRegPg = new one_application_ob_registration ( sc ) ;
        
        //  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', app2.id ) ;
        ApexPages.currentPage().getParameters().put ( 'kid', app2.Upload_Attachment_Key__c ) ;
        
        boolean testForFalse = obRegPg.alreadyRegistered();
        
        System.assertEquals(testForFalse, false);
        
        Test.stopTest();
        
    }
    
    /*
	 * Tests with an existing application passed in:
	 * test for applicant not already registered for online banking -- successful registration
	 */	
	@isTest static void testOBRegVerifyFAIL()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        insert app ;
        
        one_Application__c appAfterInsert = one_utils.getApplicationFromId(app.Id);
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( appAfterInsert ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
		
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 5000 ) ;
        
        // Inputs for registration
        obRegPg.last4SSN = '1111';
        //obRegPg.mmn = 'los angeles';
        
        // Click on button
        obRegPg.registerUsers();
        obRegPg.registerUsers();
        obRegPg.registerUsers();
        
        // Check resulting controller object
        System.assertEquals ( obRegPg.verifyAttempts, 1 );
        
        obRegPg.last4SSN = '1234';
        
        obRegPg.registerUsers();
        obRegPg.registerUsers();
        
        System.assertEquals ( obRegPg.verifyAttempts, 3 );
        System.assertEquals ( ( obRegPg.o.OB_Reg_Lockout__c != null ), true );
        
		//  End of testing
		Test.stopTest () ;
	}
    
    /*
	 * Tests with an existing application passed in:
	 * test for applicant who is locked out of OB registration form
	 */	
	@isTest static void testOBRegVerifyLockout()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        app.OB_Reg_Lockout__c = DateTime.now();
        insert app ;
        
        one_Application__c appAfterInsert = one_utils.getApplicationFromId(app.Id);
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( appAfterInsert ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
		
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 6000 ) ;
        
		//  End of testing
		Test.stopTest () ;
	}
    
    /*
	 * Tests with an existing application passed in:
	 * test for applicant coming directly from application page
	 */	
	@isTest static void testOBRegDirectApplication()
	{
        //  Setup
        insertCustomSetting () ;
        
		Test.setMock ( HttpCalloutMock.class, new MockHTTPDIHelper() ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.one_application_ob_registration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//	Set up application
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
        one_Application__c app = getApplicationForTests ( c1, null, rt ) ;
        insert app ;
        
        one_Application__c eApp = new one_application__c();
        
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
        
        // manually pass in mismatched parameters
        ApexPages.currentPage().getParameters().put ( 'aid', app.id ) ;
        ApexPages.currentPage().getParameters().put ( 'kid', app.Upload_Attachment_Key__c ) ;
        ApexPages.currentPage().getParameters().put ( 'tid', 'hello') ;

		//  Create standard controller object from empty application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eApp ) ;
		
		one_application_ob_registration obRegPg = new one_application_ob_registration ( sc ) ;
        
		//  Autorun the action that's supposed to happen on load
		obRegPg.goPageAction () ;
		
		//  Check the resulting controller object
		System.assertEquals ( obRegPg.vStepNumber, 9000 ) ;
        
		//  End of testing
		Test.stopTest () ;
	}
    
}