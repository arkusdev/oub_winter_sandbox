public without sharing class CreditHelper 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public CreditHelper ()
    {
        // Empty
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Get Credit report data
     *  - Currently singleton, can be rebuilt to allow for multiple pulls.
     *  -------------------------------------------------------------------------------------------------- */
    public static CreditHelper.CreditResults getCredit ( ID inID )
    {
        CreditHelper.CreditResults chcr = new CreditHelper.CreditResults () ;
        
        //  Check for prior credit report
        //  
        list<creditchecker__Credit_Report__c> cccrL = null ;
        
        try
        {
            cccrL = [
                SELECT ID
                FROM creditchecker__Credit_Report__c
                WHERE application__c = :inID
            ] ;
        }
        catch ( Exception e )
        {
            System.debug ( OneUnitedUtilities.parseStackTrace ( e ) ) ;
        }
        
        if ( ( cccrL != null ) && ( cccrL.size () > 0 ) ) 
        {
            //  found it, skip call.
        }
        else
        {
            //  Load ID into list.
            list<ID> idL = new list<ID> () ;
            idL.add ( inID ) ;
            
			//  Callout and save existing records
            Creditchecker.CreateReportRecords.processItems ( idL ) ;  // ?!? time frame
            
            //  Reload second time
            try
            {
                cccrL = [
                    SELECT ID
                    FROM creditchecker__Credit_Report__c
                    WHERE application__c = :inID
                ] ;
            }
            catch ( Exception e )
            {
                // ?
            }
        }
        
        //  Convert from cccr to credit result object
        //  
        if ( ( cccrL != null ) && ( cccrL.size () > 0 ) ) 
        {
            chcr = loadCreditData ( cccrL [ 0 ].ID ) ;
        }
        
        return chcr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Load all data
     *  -------------------------------------------------------------------------------------------------- */
    public static CreditHelper.CreditResults loadCreditData ( ID cccrID )
    {
        CreditHelper.CreditResults chcr = new CreditHelper.CreditResults () ;
        
        //  need to select from various custom objects and load into internal class for return.
        //  They all start with "creditchecker__"
        creditchecker__Credit_Report__c cr =
            [Select Id, creditchecker__Applicants_Date_Of_Birth__c, creditchecker__Applicants_SSN__c,
                    creditchecker__Average_Score__c, creditchecker__CoApplicants_Date_Of_Birth__c,
                    creditchecker__CoApplicants_SSN__c, creditchecker__Parent_Record_Id__c,
                    creditchecker__Score_Tier__c, creditchecker__Status__c,
             		creditchecker__Status_Description__c,
                    creditchecker__Total_Inquiry_Count__c, creditchecker__Total_Liability_Payment__c,
                    Application__r.Co_Applicant_SSN__c, Application__r.SSN__c, 
                    Total_Monthly_Payments__c , Monthly_Payments__c ,
                    Joint_Monthly_Payments__c, Co_Applicant_Monthly_Payments__c,
                    (Select Id, creditchecker__Credit_Score__c, creditchecker__Factor_Code__c,
                            creditchecker__Model_Name_Description__c, creditchecker__Whose__c
                     From creditchecker__Score_Models__r),
                    (Select Id, creditchecker__Credit_Alert_Description__c,
                            creditchecker__Whose__c
                     From creditchecker__Alerts__r
                     Where RecordType.Name = 'Credit Response Alerts'),
                    (Select creditchecker__Disposition_Type__c, creditchecker__Whose__c
                     From creditchecker__Public_Records__r),
                    (Select Id, creditchecker__Account_Type__c, creditchecker__Date_Reported__c,
                            creditchecker__Monthly_Payment_Amount__c, creditchecker__Whose__c,
                            creditchecker__Account_Status__c 
                     From creditchecker__Tradelines__r
                     Where creditchecker__Date_Reported__c >= LAST_N_DAYS:182
                       AND creditchecker__Account_Status__c = 'Open'),
                    (Select creditchecker__Date__c,creditchecker__Whose__c
                     From creditchecker__Credit_Inquiries__r
                     Where creditchecker__Date__c >= LAST_N_DAYS:182)
             From creditchecker__Credit_Report__c
             Where Id = :cccrId];

        // Check to see if the Credit Report is complete. Until the status is complete,
        // we don't have complete data so set the flag and don't process the data further.
        if (cr.creditchecker__Status__c != 'Completed') {
            chcr.reportComplete = false;
        } else {
            //***************************************************
            // Set fields from the top-level Credit Report object
            //***************************************************
            chcr.reportComplete = true;
            chcr.applicationID = cr.creditchecker__Parent_Record_Id__c;
            chcr.creditReportID = cr.Id;
            chcr.noHitFound = (cr.creditchecker__Score_Tier__c == 'No Score Available');
            chcr.creditScore = Integer.valueOf(cr.creditchecker__Average_Score__c);
            chcr.monthlyExpenses = cr.creditchecker__Total_Liability_Payment__c;

            // Calculate age from date of birth returned to check for underage - applicant
            if (cr.creditchecker__Applicants_Date_Of_Birth__c != null) {
                // If the date of birth plus 18 years is after today, the applicant is under 18 years old.
                if (cr.creditchecker__Applicants_Date_Of_Birth__c.addYears(18).daysBetween(System.today()) < 0) {
                    chcr.underage = true;
                } else {
                    chcr.underage = false;
                }
            }
            // Calculate age from date of birth returned to check for underage - co-applicant
            if (cr.creditchecker__CoApplicants_Date_Of_Birth__c != null) {
                // If the date of birth plus 18 years is after today, the applicant is under 18 years old.
                if (cr.creditchecker__CoApplicants_Date_Of_Birth__c.addYears(18).daysBetween(System.today()) < 0) {
                    chcr.underageCoApp = true;
                } else {
                    chcr.underageCoApp = false;
                }
            }
            // Check SSN for applicant and co-applicant from Application record against the SSN
            // returned in the Credit Report to look for mismatches
            chcr.ssnMismatch = false;
            chcr.ssnMismatchCoApp = false;
            if (cr.Application__r.SSN__c != null && cr.creditchecker__Applicants_SSN__c != null &&
                cr.Application__r.SSN__c != cr.creditchecker__Applicants_SSN__c) {

                chcr.ssnMismatch = true;
            }
            if (cr.Application__r.Co_Applicant_SSN__c != null && cr.creditchecker__CoApplicants_SSN__c != null &&
                cr.Application__r.Co_Applicant_SSN__c != cr.creditchecker__CoApplicants_SSN__c) {

                chcr.ssnMismatchCoApp = true;
            }

            chcr.sixMonthCreditInquiry = Integer.valueOf(cr.creditchecker__Total_Inquiry_Count__c);

            //*******************************************************************************
            // Get Income Insight fields from Score Model object. Credit Score on this object
            // has the income in thousands, so multiply by 1,000 to get true income.
            // When the record is not IncomeInsight, it contains actual credit score so get
            // this for Co-Borrower when present.
            //*******************************************************************************
            chcr.incomeInsightReturned = false;
            chcr.incomeInsightReturnedCoApp = false;
            for (creditchecker__Score_Model__c sm: cr.creditchecker__Score_Models__r) {
                if (sm.creditchecker__Model_Name_Description__c == 'IncomeInsight' && sm.creditchecker__Credit_Score__c != null) {
                    if (sm.creditchecker__Whose__c == 'Borrower') {
                        chcr.incomeInsightReturned = true;
                        chcr.bureauReportedIncome = sm.creditchecker__Credit_Score__c * 1000.0;
                    } else if (sm.creditchecker__Whose__c == 'Co-Borrower') {
                        chcr.incomeInsightReturnedCoApp = true;
                        chcr.bureauReportedIncomeCoApp = sm.creditchecker__Credit_Score__c * 1000.0;
                    } 
                } else if (sm.creditchecker__Whose__c == 'Co-Borrower' && sm.creditchecker__Credit_Score__c != null) {
                    chcr.creditScoreCoApp = Integer.valueOf(sm.creditchecker__Credit_Score__c);
                }
            }
            chcr.noHitFoundCoApp = false;
            if (chcr.creditScoreCoApp == null) {
                chcr.noHitFoundCoApp = true;
            }

            //*********************************************************************************
            // Process the Alert object and try to match description text to specific alerts we
            // are interested in.
            //*********************************************************************************
            chcr.statusDescription = cr.creditchecker__Status_Description__c;
            
            chcr.AddressDiscrepancy = false;
            chcr.AddressDiscrepancyCoApp = false;
            chcr.deceased = false;
            chcr.deceasedCoApp = false;
            chcr.frozenFile = false;
            chcr.frozenFileCoApp = false;
            chcr.frozenFilePend = false;
            chcr.frozenFileCoAppPend = false;
            for (creditchecker__Alert__c alert: cr.creditchecker__Alerts__r) {
                String alertDesc = alert.creditchecker__Credit_Alert_Description__c;
                chcr.alertDescriptions.add(alertDesc);
                if (String.isNotBlank(alertDesc)) {
                    // Address Discrepancy
                    if (alertDesc.startsWithIgnoreCase('FACTA: Address Discrepancy')) {
                        if (alert.creditchecker__Whose__c == 'Borrower') {
                            chcr.AddressDiscrepancy = true;
                        } else if (alert.creditchecker__Whose__c == 'Co-Borrower') {
                            chcr.AddressDiscrepancyCoApp = true;
                        }
                    // Deceased
                    } else if (alertDesc.containsIgnoreCase('deceased')) {
                        if (alert.creditchecker__Whose__c == 'Borrower') {
                            chcr.deceased = true;
                        } else if (alert.creditchecker__Whose__c == 'Co-Borrower') {
                            chcr.deceasedCoApp = true;
                        }
                    // File Frozen (Pending)
                    } else if ((alertDesc.containsIgnoreCase('file frozen') ||
                               alertDesc.containsIgnoreCase('id fraud victim') ||
                               alertDesc.startsWithIgnoreCase('FACTA: Fraud Victim ') ) && 
                               (alertDesc.containsIgnoreCase('VERIFY IDENTITY OF CONSUMER BEFORE GRANTING CREDIT') ||
                                alertDesc.containsIgnoreCase('DO NOT EXTEND CREDIT WITHOUT VERIFYING CONSUMER INFORMATION') ) ) { 
                        if (alert.creditchecker__Whose__c == 'Borrower') {
                            chcr.frozenFilePend = true;
                        }
                        else if (alert.creditchecker__Whose__c == 'Co-Borrower') {
                            chcr.frozenFileCoAppPend = true;
                        }

                    // File Frozen (Decline)
                    } else if ((alertDesc.containsIgnoreCase('file frozen') ||
                               alertDesc.containsIgnoreCase('id fraud victim') ||
                               alertDesc.startsWithIgnoreCase('FACTA: Fraud Victim ') ) && 
                               !alertDesc.containsIgnoreCase('VERIFY IDENTITY OF CONSUMER BEFORE GRANTING CREDIT') &&
                               !alertDesc.containsIgnoreCase('DO NOT EXTEND CREDIT WITHOUT VERIFYING CONSUMER INFORMATION') )  {     
                        if (alert.creditchecker__Whose__c == 'Borrower') {
                        	chcr.frozenFile = true;          
                        }
                        else if (alert.creditchecker__Whose__c == 'Co-Borrower') {
                            chcr.frozenFileCoApp = true;
                        }
                    }
                }
            }
            
            if(cr.creditchecker__Status_Description__c != NULL &&
                               (cr.creditchecker__Status_Description__c.containsIgnoreCase('file frozen') ||
                               cr.creditchecker__Status_Description__c.containsIgnoreCase('file locked') ) ) {
            	chcr.frozenFilePend = true;
            }

            //*******************************************************************************
            // Process the Public Record object to find Total, Dismissed or Discharged
            // Bankruptcies.
            // Have seen record with Discharged, but not Dismissed. Need to determine if
            // this is how they would be returned.
            //*******************************************************************************
            chcr.publicBankrupcyRecordTotal = 0;
            chcr.dischargeBankrupcyRecordTotal = 0;
            chcr.dismissedBankrupcyRecordTotal = 0;
            chcr.publicBankrupcyRecordTotalCoApp = 0;
            chcr.dischargeBankrupcyRecordTotalCoApp = 0;
            chcr.dismissedBankrupcyRecordTotalCoApp = 0;
            chcr.publicBankrupcyRecordTotalJoint = 0;
            chcr.dischargeBankrupcyRecordTotalJoint = 0;
            chcr.dismissedBankrupcyRecordTotalJoint = 0;
            for (creditchecker__Public_Record__c pr: cr.creditchecker__Public_Records__r) {
                if (pr.creditchecker__Whose__c == 'Borrower') {
                    chcr.publicBankrupcyRecordTotal++;
                    if (pr.creditchecker__Disposition_Type__c == 'Discharged') {
                        chcr.dischargeBankrupcyRecordTotal++;
                    } else if (pr.creditchecker__Disposition_Type__c == 'Dismissed') {
                        chcr.dismissedBankrupcyRecordTotal++;
                    }
                } else if (pr.creditchecker__Whose__c == 'Co-Borrower') {
                    chcr.publicBankrupcyRecordTotalCoApp++;
                    if (pr.creditchecker__Disposition_Type__c == 'Discharged') {
                        chcr.dischargeBankrupcyRecordTotalCoApp++;
                    } else if (pr.creditchecker__Disposition_Type__c == 'Dismissed') {
                        chcr.dismissedBankrupcyRecordTotalCoApp++;
                    }
                } else if (pr.creditchecker__Whose__c == 'Joint') {
                    chcr.publicBankrupcyRecordTotalJoint++;
                    if (pr.creditchecker__Disposition_Type__c == 'Discharged') {
                        chcr.dischargeBankrupcyRecordTotalJoint++;
                    } else if (pr.creditchecker__Disposition_Type__c == 'Dismissed') {
                        chcr.dismissedBankrupcyRecordTotalJoint++;
                    }
                }
            }

            //********************************************************************************
            // Process the Tradeline object to find total monthly expenses and open mortgages.
            // Find these totals for Borrower, Co-Borrower, and Joint.
            //********************************************************************************
            chcr.monthlyExpenses = 0.0;
            chcr.monthlyExpensesCoapp = 0.0;
            chcr.monthlyExpensesJoint = 0.0;
            chcr.totalOpenMortgages = 0;
            chcr.totalOpenMortgagesCoapp = 0;
            chcr.totalOpenMortgagesJoint = 0;
            Date sixMonthsAgo = System.today().addMonths(-6);
            for (creditchecker__Tradeline__c tl: cr.creditchecker__Tradelines__r) {
                if (tl.creditchecker__Account_Type__c != null &&
                    tl.creditchecker__Account_Type__c == 'Mortgage') {
                        
                    if (tl.creditchecker__Whose__c == 'Borrower') {
                        chcr.totalOpenMortgages += 1;
                    } else if (tl.creditchecker__Whose__c == 'Co-Borrower') {
                        chcr.totalOpenMortgagesCoapp += 1;
                    } else if (tl.creditchecker__Whose__c == 'Joint') {
                        chcr.totalOpenMortgagesJoint += 1;
                    }
                }
                
                if (tl.creditchecker__Monthly_Payment_Amount__c != null &&
                    tl.creditchecker__Monthly_Payment_Amount__c != 0) {

                    if (tl.creditchecker__Whose__c == 'Borrower') {
                        chcr.monthlyExpenses += tl.creditchecker__Monthly_Payment_Amount__c;
                    } else if (tl.creditchecker__Whose__c == 'Co-Borrower') {
                        chcr.monthlyExpensesCoApp += tl.creditchecker__Monthly_Payment_Amount__c;
                    } else if (tl.creditchecker__Whose__c == 'Joint') {
                        chcr.monthlyExpensesJoint += tl.creditchecker__Monthly_Payment_Amount__c;
                    }
                }

            }

            //*******************************************************************************
            // Process the Credit Inquiry object to get the number of inquiries in the past
            // 6 months for Borrower, Co-Borrower, and
            //*******************************************************************************
            chcr.sixMonthCreditInquiry = 0;
            chcr.sixMonthCreditInquiryCoApp = 0;
            chcr.sixMonthCreditInquiryJoint = 0;
            for (creditchecker__Credit_Inquiry__c ci: cr.creditchecker__Credit_Inquiries__r) {
                if (ci.creditchecker__Whose__c == 'Borrower') {
                    chcr.sixMonthCreditInquiry++;
                } else if (ci.creditchecker__Whose__c == 'Co-Borrower') {
                    chcr.sixMonthCreditInquiryCoApp++;
                } else if (ci.creditchecker__Whose__c == 'Joint') {
                    chcr.sixMonthCreditInquiryJoint++;
                }
            }
        }
        System.debug('CreditResults after populating = ' + chcr);
        
        return chcr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Return object for populating
     *  -------------------------------------------------------------------------------------------------- */
    public class CreditResults
    {
        public ID applicationID ;
        public ID creditReportID ;
        public boolean reportComplete;

        public boolean noHitFound ;
        public boolean noHitFoundCoApp;
        public integer creditScore ;
        public integer creditScoreCoApp;
        public boolean incomeInsightReturned ;
        public boolean incomeInsightReturnedCoApp;
        public double bureauReportedIncome ;
        public double bureauReportedIncomeCoApp;
        public double monthlyExpenses ;
        public double monthlyExpensesCoapp;
        public double monthlyExpensesJoint; // Total sum monthly payment on all open trades reported within 6 months of the profile date
        public double totalOpenMortgages ;
        public double totalOpenMortgagesCoapp;
        public double totalOpenMortgagesJoint; // Total number of open mortgage type trades reported in the last 6 months

        public boolean deceased ;
        public boolean deceasedCoApp;
        public boolean underage ;
        public boolean underageCoApp;
        public boolean frozenFile ;
        public boolean frozenFileCoApp;
        public boolean frozenFilePend;
        public boolean frozenFileCoAppPend;
        public boolean ssnMismatch ;
        public boolean ssnMismatchCoApp;
        
        public Integer publicBankrupcyRecordTotal ;
        public Integer publicBankrupcyRecordTotalCoapp ;
        public Integer publicBankrupcyRecordTotalJoint;
        public Integer dischargeBankrupcyRecordTotal ;
        public Integer dischargeBankrupcyRecordTotalCoapp ;
        public Integer dischargeBankrupcyRecordTotalJoint ;
        public Integer dismissedBankrupcyRecordTotal ;
        public Integer dismissedBankrupcyRecordTotalCoapp ;
        public Integer dismissedBankrupcyRecordTotalJoint ;
        
        public Integer sixMonthCreditInquiry;
        public Integer sixMonthCreditInquiryCoApp;
        public Integer sixMonthCreditInquiryJoint;

        // replaces check currentAddress with priorAddress above
        public boolean AddressDiscrepancy;
        public boolean AddressDiscrepancyCoApp;
        
        public boolean bankrupcy ()
        {
            if ( ( publicBankrupcyRecordTotal - dischargeBankrupcyRecordTotal - dismissedBankrupcyRecordTotal ) > 0 )
                return true ;
            else
                return false ;
        }
        public boolean bankrupcyCoapp ()
        {
            if ( ( publicBankrupcyRecordTotalCoApp - dischargeBankrupcyRecordTotalCoApp - dismissedBankrupcyRecordTotalCoApp ) > 0 )
                return true ;
            else
                return false ;
        }
        public boolean bankrupcyJoint ()
        {
            if ( ( publicBankrupcyRecordTotalJoint - dischargeBankrupcyRecordTotalJoint - dismissedBankrupcyRecordTotalJoint ) > 0 )
                return true ;
            else
                return false ;
        }
        
        public String statusDescription;
        public List<String> alertDescriptions = new List<String>();
    }
}