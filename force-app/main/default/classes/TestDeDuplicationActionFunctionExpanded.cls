@isTest
public class TestDeDuplicationActionFunctionExpanded 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
    private static Account getAccount ( Integer i )
    {
    	Account a = new Account () ;
    	a.name = 'Test' + i + ' Account' + i ;
    	
    	return a ;
    }
    
	private static Contact getContactEmail ( Integer i, String rtID )
	{
		return getContactEmail ( i, rtID, null ) ;
	}
	
    private static Contact getContactEmail ( Integer i, String rtID, Account a )
    {
    	Contact c = new Contact () ;
    	
    	c.FirstName = 'test' + i ;
    	c.LastName = 'user' + i ;
    	c.Email = 'test' + i + '@user' + i + '.com' ;
    	c.RecordTypeID = rtID ;
    	
    	if ( a != null )
	    	c.Account = a ;
    	
    	return c ;
    }
    
	private static Contact getContactPhone ( Integer i, String rtID )
	{
		return getContactPhone ( i, rtID, null ) ;
	}
	
    private static Contact getContactPhone ( Integer i, String rtID, Account a )
    {
    	Contact c = new Contact () ;
    	
    	c.FirstName = 'test' + i ;
    	c.LastName = 'user' + i ;
    	c.Phone = '00' + i + '-00' + i + '-000' + i ;
    	c.RecordTypeID = rtID ;
    	
    	if ( a != null )
	    	c.Account = a ;
    	
    	return c ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, NADA
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Zero ()
	{
		System.debug ( '----------------------------------- TEST ZERO -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  Run (empty) function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
    
    static testmethod void test_new_One ()
    {
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
        
		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		Contact cc1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
    	cc1.phone = '00' + 1 + '-00' + 1 + '-000' + 1 ;
		insert cc1 ;
        
        Test.startTest () ;
        
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
    }
/*	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Email
	//  --------------------------------------------------------------------------------------
	static testmethod void test_One ()
	{
		System.debug ( '----------------------------------- TEST ONE -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		Contact cc1 = getContactEmail ( 1, customerRecordTypeID, a1 ) ;
    	cc1.phone = '00' + 1 + '-00' + 1 + '-000' + 1 ;
		insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactEmail ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactEmail ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Second contact
		Contact cc2 = getContactEmail ( 2, customerRecordTypeID, a1 ) ;
		insert cc2 ;
		
		//  Not so duplicate prospects, second contact
		Contact cp21 = getContactEmail ( 1, prospectRecordTypeID ) ;
		cp21.FirstName = 'Other1' ;
    	cp21.Description = 'Nyet Mergy' ;
		insert cp21 ;

		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr2 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr2.cID = cc2.ID ;
        dafr2.cType = 'Customer' ;
		
		dafrL.add ( dafr2 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
		
		//  Check to see that stuff merged
		Contact c1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__C
			FROM Contact
			WHERE ID = :cc1.ID
		] ;
		
		if ( c1 != null )
		{
			System.assertEquals ( c1.Description, cp11.Description ) ;
			System.assertEquals ( c1.Title, cp12.Title ) ;
			System.assertEquals ( c1.WorkEmail__c, cp11.WorkEmail__c ) ;
		}
		
		//  Check to see that stuff did not merge
		Contact c2 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__C
			FROM Contact
			WHERE ID = :cc2.ID
		] ;
		
		if ( c2 != null )
		{
			System.assertNotEquals ( c2.Description, cp21.Description ) ;
		}
	}
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Phone
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Two ()
	{
		System.debug ( '----------------------------------- TEST TWO -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		Contact cc1 = getContactPhone ( 1, customerRecordTypeID, a1 ) ;
		insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
		
		//  Check to see that stuff merged
		Contact c1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__c, Computed_Phone__c
			FROM Contact
			WHERE ID = :cc1.ID
		] ;
		
		if ( c1 != null )
		{
			System.assertEquals ( c1.Description, cp11.Description ) ;
			System.assertEquals ( c1.Title, cp12.Title ) ;
			System.assertEquals ( c1.WorkEmail__c, cp11.WorkEmail__c ) ;
		}
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Phone & Contact with Email
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Three ()
	{
		System.debug ( '----------------------------------- TEST THREE -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		Contact cc1 = getContactPhone ( 1, customerRecordTypeID, a1 ) ;
		insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;

		Contact cc2 = getContactEmail ( 2, customerRecordTypeID, a1 ) ;
    	cc2.phone = '00' + 2 + '-00' + 2 + '-000' + 1 ;
		insert cc2 ;
		
		//  Duplicate prospects, first contact
		Contact cp21 = getContactEmail ( 2, prospectRecordTypeID ) ;
    	cp21.WorkEmail__c = 'test' + 2 + '@work' + 2 + '.com' ;
    	cp21.Description = 'Mergy Merge' ;
		insert cp21 ;
		
		//  Duplicate prospects, second contact
		Contact cp22 = getContactEmail ( 2, prospectRecordTypeID ) ;
		cp22.title = 'Tester' ;
		insert cp22 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr2 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr2.cID = cc2.ID ;
        dafr2.cType = 'Customer' ;
		
		dafrL.add ( dafr2 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, ONLY Prospects
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Four ()
	{
		System.debug ( '----------------------------------- TEST FOUR -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
    	
		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
    	//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
    	
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cp11.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;
    	
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, ONLY Customers
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Five ()
	{
		System.debug ( '----------------------------------- TEST FIVE -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;
    	
		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
    	//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, customerRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, customerRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
    	
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cp11.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Phone & Contact with Email - LOTS
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Six ()
	{
		System.debug ( '----------------------------------- TEST SIX -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Account a1 = getAccount ( 1 ) ;
		insert a1 ;
		
		Contact cc1 = getContactPhone ( 1, customerRecordTypeID, a1 ) ;
		insert cc1 ;
		
        list<Contact> dL = new list<Contact> () ;
        
        
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		dL.add ( cp11 ) ;
        
        //  LOTS!
        for ( Integer i = 0 ; i < 10 ; i ++ )
        {
            Contact cpX = getContactPhone ( 1, prospectRecordTypeID ) ;
            cpX.title = 'Tester' ;
            dL.add ( cpX ) ;
        }
		
        //  Insert
		insert dL ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Customer' ;
		
		dafrL.add ( dafr1 ) ;

		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Email
	//  --------------------------------------------------------------------------------------
	static testmethod void test_One_Prospect ()
	{
		System.debug ( '----------------------------------- TEST ONE PROSPECT -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  Original contact
		Contact cc1 = getContactEmail ( 1, prospectRecordTypeID ) ;
		insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactEmail ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactEmail ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Prospect' ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Second contact
		Contact cc2 = getContactEmail ( 2, prospectRecordTypeID ) ;
		insert cc2 ;
		
		//  Not so duplicate prospects, second contact
		Contact cp21 = getContactEmail ( 1, prospectRecordTypeID ) ;
		cp21.FirstName = 'Other1' ;
    	cp21.Description = 'Nyet Mergy' ;
		insert cp21 ;

		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr2 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr2.cID = cc2.ID ;
        dafr2.cType = 'Prospect' ;
		
		dafrL.add ( dafr2 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
		
		//  Check to see that stuff merged
		Contact c1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__C
			FROM Contact
			WHERE ID = :cc1.ID
		] ;
		
		if ( c1 != null )
		{
			System.assertEquals ( c1.Description, cp11.Description ) ;
			System.assertEquals ( c1.Title, cp12.Title ) ;
			System.assertEquals ( c1.WorkEmail__c, cp11.WorkEmail__c ) ;
		}
		
		//  Check to see that stuff did not merge
		Contact c2 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__C
			FROM Contact
			WHERE ID = :cc2.ID
		] ;
		
		if ( c2 != null )
		{
			System.assertNotEquals ( c2.Description, cp21.Description ) ;
		}
	}

	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Phone
	//  --------------------------------------------------------------------------------------
    static testmethod void test_Two_Prospect ()
	{
		System.debug ( '----------------------------------- TEST TWO PROSPECT -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Contact cc1 = getContactPhone ( 1, prospectRecordTypeID ) ;
		insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Prospect' ;
		
		dafrL.add ( dafr1 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
		
		//  Check to see that stuff merged
		Contact c1 = [
			SELECT ID, FirstName, LastName, Email, Description, Title, WorkEmail__c, Computed_Phone__c
			FROM Contact
			WHERE ID = :cc1.ID
		] ;
		
		if ( c1 != null )
		{
			System.assertEquals ( c1.Description, cp11.Description ) ;
			System.assertEquals ( c1.Title, cp12.Title ) ;
			System.assertEquals ( c1.WorkEmail__c, cp11.WorkEmail__c ) ;
		}
	}
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Phone & Contact with Email
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Three_Prospect ()
	{
		System.debug ( '----------------------------------- TEST THREE PROSPECT-----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Contact cc1 = getContactPhone ( 1, prospectRecordTypeID ) ;
		insert cc1 ;
		
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		insert cp11 ;
		
		//  Duplicate prospects, second contact
		Contact cp12 = getContactPhone ( 1, prospectRecordTypeID ) ;
		cp12.title = 'Tester' ;
		insert cp12 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Prospect' ;
		
		dafrL.add ( dafr1 ) ;

		Contact cc2 = getContactEmail ( 2, prospectRecordTypeID ) ;
    	cc2.phone = '00' + 2 + '-00' + 2 + '-000' + 1 ;
		insert cc2 ;
		
		//  Duplicate prospects, first contact
		Contact cp21 = getContactEmail ( 2, prospectRecordTypeID ) ;
    	cp21.WorkEmail__c = 'test' + 2 + '@work' + 2 + '.com' ;
    	cp21.Description = 'Mergy Merge' ;
		insert cp21 ;
		
		//  Duplicate prospects, second contact
		Contact cp22 = getContactEmail ( 2, prospectRecordTypeID ) ;
		cp22.title = 'Tester' ;
		insert cp22 ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr2 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr2.cID = cc2.ID ;
        dafr2.cType = 'Prospect' ;
		
		dafrL.add ( dafr2 ) ;
		
		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
    
	//  --------------------------------------------------------------------------------------
	//  ------------ Test @InvocableMethod, Contact with Phone & Contact with Email - LOTS
	//  --------------------------------------------------------------------------------------
	static testmethod void test_Six_Prospect ()
	{
		System.debug ( '----------------------------------- TEST SIX PROSPECT -----------------------------------'  ) ;
		
		Test.startTest () ;
		
		//  Record types
    	String prospectRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Prospect', 'Contact' ) ;

		//  Main list to send in
		list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> dafrL = new list<DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest> () ;
		
		//  First contact
		Contact cc1 = getContactPhone ( 1, prospectRecordTypeID ) ;
		insert cc1 ;
		
        list<Contact> dL = new list<Contact> () ;
        
		//  Duplicate prospects, first contact
		Contact cp11 = getContactPhone ( 1, prospectRecordTypeID ) ;
    	cp11.WorkEmail__c = 'test' + 1 + '@work' + 1 + '.com' ;
    	cp11.Description = 'Mergy Merge' ;
		dL.add ( cp11 ) ;
        
        //  LOTS!
        for ( Integer i = 0 ; i < 10 ; i ++ )
        {
            Contact cpX = getContactPhone ( 1, prospectRecordTypeID ) ;
            cpX.title = 'Tester' ;
            dL.add ( cpX ) ;
        }
		
        //  Insert
		insert dL ;
		
		//  Add to list
		DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest dafr1 = new DeDuplicationActionFunctionExpanded.DeDuplicationActionFunctionExpandedRequest () ;
		dafr1.cID = cc1.ID ;
        dafr1.cType = 'Prospect' ;
		
		dafrL.add ( dafr1 ) ;

		//  Run function!
		DeDuplicationActionFunctionExpanded.deduplicateContact ( dafrL ) ;
		
		Test.stopTest () ;
	}
*/
}