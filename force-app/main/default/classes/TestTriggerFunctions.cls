@isTest
private class TestTriggerFunctions 
{
    //  -------------------------------------
    //  ---------- Lead conditions ----------
    //  -------------------------------------
    
    static testMethod void testLeadCase_Empty ()
    {
        Lead l = new Lead () ;
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_Null ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = null ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_Stupid ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'do do do' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_CustomerSupport ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Customer Support' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_OpenAccount ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Open Account' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_Contributions ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Contributions' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_Mortgages ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Mortgages' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_ConsumerMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Consumer Mortgage' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_MultiFamilyMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'MultiFamily Mortgage' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_StayConnected ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Stay Connected' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_UNITY_Visa ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'UNITY Visa' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_Connections_Matter ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Connections Matter' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_RSVP ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Grand ReOpening' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_Waive ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Waive' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_YesInOurBackyard ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Backyard' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_HomeLoanTrust ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Home Loan Trust' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    static testMethod void testLeadCase_FirstTime ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'First Time Home Buyer' ;
        
        Case c = TriggerFunctions.createWebsiteCaseFromLead ( l ) ;
    }
    
    //  -------------------------------------
    //  ---------- Task Conditions ----------
    //  -------------------------------------

    static testMethod void testLeadTask_Empty ()
    {
        Lead l = new Lead () ;
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_Null ()
    {
        Lead l = new Lead () ;
        l.Website_Form__c = null ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_Stupid ()
    {
        Lead l = new Lead () ;

        l.Website_Form__c = 'do do do' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_CustomerSupport ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Customer Support' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_OpenAccount ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Open Account' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_Contributions ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Contributions' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_Mortgages ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Mortgages' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_ConsumerMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Consumer Mortgage' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_MultiFamilyMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'MultiFamily Mortgage' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_StayConnected ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Stay Connected' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }

    static testMethod void testLeadTask_UNITYVisa ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'UNITY Visa' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }

    static testMethod void testLeadTask_ConnectionsMatter ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Connections Matter' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_RSVP ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Grand ReOpening' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_Waive ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Waive' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_YesInOurBackyard ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Backyard' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_HomeLoanTrust ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Home Loan Trust' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    static testMethod void testLeadTask_FirstTime ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'First Time Home Buyer' ;
        
        Task t = TriggerFunctions.createWebsiteTaskFromLead ( l ) ;
    }
    
    //  --------------------------------------------
    //  ---------- Opportunity Conditions ----------
    //  --------------------------------------------

    static testMethod void testLeadOpportunity_Empty ()
    {
        Lead l = new Lead () ;
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_Null ()
    {
        Lead l = new Lead () ;
        l.Website_Form__c = null ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_Stupid ()
    {
        Lead l = new Lead () ;

        l.Website_Form__c = 'do do do' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_CustomerSupport ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Customer Support' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_OpenAccount ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Open Account' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_Contributions ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Contributions' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_Mortgages ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Mortgages' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_ConsumerMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Consumer Mortgage' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_MultiFamilyMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'MultiFamily Mortgage' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_StayConnected ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Stay Connected' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }

    static testMethod void testLeadOpportunity_UNITYVisa ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'UNITY Visa' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }

    static testMethod void testLeadOpportunity_ConnectionsMatter ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Connections Matter' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_RSVP ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Grand ReOpening' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_Waive ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Waive' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_YesInOurBackyard ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Backyard' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_HomeLoanTrust ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Home Loan Trust' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_FirstTime ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'First Time Home Buyer' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    static testMethod void testLeadOpportunity_OneTransaction ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'OneTransaction' ;
        
        oneOpportunity__c oo = TriggerFunctions.createOpportunityFromLead ( l ) ;
    }
    
    //  -----------------------------------------
    //  ---------- Workshop Conditions ----------
    //  -----------------------------------------
    private static void setupTesting ()
    {
        Workshop__c w1 = new Workshop__c () ;
        w1.Name = 'Crenshaw RSVP' ;
        
        insert w1 ;
        
        Workshop__c w2 = new Workshop__c () ;
        w2.Name = 'OneTransaction' ;
        
        insert w2 ;
        
        //  Settings
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.Crenshaw_RSVP_Workshop_ID__c = w1.ID ;
    	ws.Workshop_Contact_Record_Type__c = 'Prospect' ;
        ws.OneTransaction_Workshop_ID__c = w2.ID ;
        
        insert ws ;
    }

    static testMethod void testLeadAttendee_Empty ()
    {
        setupTesting () ;
            
        Lead l = new Lead () ;
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_Null ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        l.Website_Form__c = null ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_Stupid ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;

        l.Website_Form__c = 'do do do' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_CustomerSupport ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Customer Support' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_OpenAccount ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Open Account' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_Contributions ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Contributions' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_Mortgages ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Mortgages' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_ConsumerMortgage ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Consumer Mortgage' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_MultiFamilyMortgage ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'MultiFamily Mortgage' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_StayConnected ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Stay Connected' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }

    static testMethod void testLeadAttendee_UNITYVisa ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'UNITY Visa' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }

    static testMethod void testLeadAttendee_ConnectionsMatter ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Connections Matter' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_RSVP ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'Grand ReOpening' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    static testMethod void testLeadAttendee_OT ()
    {
        setupTesting () ;
        
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'OneTransaction' ;
        
        Workshop_Attendee__c wa = TriggerFunctions.createAttendeeFromLead ( l ) ;
    }
    
    //  -------------------------------------
    //  ------------ Other Crap -------------
    //  -------------------------------------
    static testmethod void testUpdateLeadFromLead ()
    {
        Lead l = new lead () ;
        Lead iL = new lead () ;
        
        Lead uL = TriggerFunctions.updateLeadFromLead ( l , iL ) ;
    }

    static testmethod void testUpdateContactFromLead ()
    {
        Contact c1 = new Contact () ;
        c1.LeadSource = 'TEST' ;
        
        Lead iL1 = new lead () ;
        iL1.LeadSource = 'Other' ;
        
        Contact uC1 = TriggerFunctions.updateContactFromLead ( c1 , iL1 ) ;
        
        System.assertEquals ( uC1.LeadSource, 'Other' ) ;
        
        Contact c2 = new Contact () ;
        c2.LeadSource = 'TEST' ;
        
        Lead iL2 = new lead () ;
        
        Contact uC2 = TriggerFunctions.updateContactFromLead ( c2 , iL2 ) ;
        
        System.assertEquals ( uC2.LeadSource, 'TEST' ) ;
    }
    
    static testmethod void testCreateContactFromLead ()
    {
        Lead l = new Lead () ;
        
        l.Website_Form__c = 'UNITY Visa' ;
        l.FirstName = 'Test' ;
        l.LastName = 'User' ;
        
        Contact c = TriggerFunctions.createContactFromLead ( l ) ;
    }
}