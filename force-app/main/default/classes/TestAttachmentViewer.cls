@isTest
public class TestAttachmentViewer 
{
	public static testmethod void testONE ()
    {
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'User' ;
        
        insert c1 ;
        
		Attachment a1 = new Attachment () ;
		a1.Name = 'Utility Bill' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = c1.id ;
		a1.IsPrivate = false ;
        
        insert a1 ;
        
        list<Attachment> aL = AttachmentViewer.getAttachmentList ( c1.ID ) ;
	}
    
	public static testmethod void testTWO ()
    {
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'User' ;
        
        insert c1 ;
        
        ContentVersion cv = new ContentVersion () ;
        cv.ContentURL='http://www.google.com/' ;
        cv.Title = 'Google.com' ;
        
        insert cv ;
        
        ContentVersion xcv = [
            SELECT ID, ContentDocumentID
            FROM ContentVersion
            WHERE ID = :cv.ID
        ] ;
        
        ContentDocumentLink cdl = new ContentDocumentLink () ;
        cdl.ContentDocumentId = xcv.ContentDocumentID ;
        cdl.LinkedEntityId = c1.ID ;
        cdl.ShareType = 'V' ;
        insert cdl ;
        
        list<ContentDocument> xL = AttachmentViewer.getContentDocumentList ( c1.ID ) ;
	}
}