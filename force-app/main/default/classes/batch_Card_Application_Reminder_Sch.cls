global class batch_Card_Application_Reminder_Sch implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Card_Application_Reminder b = new batch_Card_Application_Reminder () ;
		Database.executeBatch ( b ) ;
   }
}