@isTest
public with sharing class test_batch_DeDuplication_Schedule 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Batch_Process_Settings__c insertCustomSetting ()
    {
        Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;

        bps.Name = 'OneUnited_Batch' ;        
        bps.ACH_GL_Account__c = '1001000100' ;
        bps.Application_Cashbox__c = '666' ;
        bps.Batch_Email__c = 'lmattera@oneunited.com' ;
        bps.Collateral_GL_Account__c = '1001000101' ;
        bps.Deposit_ePay_GL_Account__c = '1001000102' ;
        bps.Email_Enabled__c = false ;
        bps.Routing_Number__c = '01100127' ;
        bps.UNITY_Visa_ePay_GL_Account__c = '1001000103' ;
        bps.Batch_DeDeduplication_Count__c = 200 ;
        
        insert bps ;
        
        return bps ;
    }

	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
	//  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	static testmethod void something ()
	{
        insertCustomSetting () ;
        
		Test.startTest();

		batch_DeDuplication_Schedule myClass = new batch_DeDuplication_Schedule ();   
		String chron = '0 0 23 * * ?';        
		system.schedule('Test Sched', chron, myClass);
		
		Test.stopTest();
	}
}