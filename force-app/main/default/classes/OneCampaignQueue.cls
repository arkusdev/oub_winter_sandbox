public class OneCampaignQueue implements Queueable
{
    private list<Contact> cL ;
    
	public OneCampaignQueue ()
    {
        
    }
    
    public OneCampaignQueue ( list<Contact> xL )
    {
        cL = xL ;
    }
    
    public void execute ( QueueableContext context ) 
    {
        map<String,OneCampaign__c> xocM = getCampaignMap () ;
        
        list<OneCampaign_Member__c> ocmL = new list<OneCampaign_Member__c> () ;
        
        for ( Contact c : cL )
        {
            OneCampaign_Member__c ocm = new OneCampaign_Member__c () ;
            ocm.Contact__c = c.ID ;
            
            if ( String.isNotBlank ( c.OneTransaction_Focus__c ) )
            {
                String key = 'MC OneTransaction ' + c.OneTransaction_Focus__c ;
                
                if ( xocM.containsKey ( key.toLowerCase () ) )
                    ocm.Campaign__c = xocM.get ( key.toLowerCase () ).ID ;
            }
            
            ocmL.add ( ocm ) ;
        }
        
        if ( ( ocmL != null ) && ( ocmL.size () > 0 ) )
        {
            try
            {
                insert ocmL ;
            }
            catch ( Exception e )
            {
                // ?
            }
        }
    }
    
    private map<String,OneCampaign__c> getCampaignMap ()
    {
        map<String,OneCampaign__c> ocM = new map<String,OneCampaign__c> () ;
        
        list<OneCampaign__c> ocL = new list<OneCampaign__c> () ;
        
        try
        {
            ocL = [
                SELECT ID, Name
                FROM OneCampaign__c
            ] ;
        }
       	catch ( DMLException e )
        {
            ocL = null ;
        }
        
        if ( ( ocL != null ) && ( ocL.size () > 0 ) )
        {
            for ( OneCampaign__c oc : ocL )
            {
                ocM.put ( oc.name.toLowerCase (), oc ) ;
            }
        }
        
        return ocM ;
    }
}