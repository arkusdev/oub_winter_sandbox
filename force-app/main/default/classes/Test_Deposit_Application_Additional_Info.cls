@isTest
public with sharing class Test_Deposit_Application_Additional_Info 
{
    /* -----------------------------------------------------------------------------------
	 * Account
	 * ----------------------------------------------------------------------------------- */
    private static Account getAccount ()
    {
        Account a = new Account () ;
        a.Name = 'Insight Customers - Individuals' ;
        
        insert a ;
        
        return a ;
    }
    
    /*  ----------------------------------------------------------------------------------------
	 * Test with NOTHING passed in
     *  ---------------------------------------------------------------------------------------- */
	static testmethod void test_No_Info ()
	{
		System.debug ( '-------------------------------- NO INFO --------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Deposit_Application_Additional_Info ;
		Test.setCurrentPage ( pr1 ) ;
		
		Deposit_Application__c da = Deposit_Application_Utils.getApplicationWithData ( 1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( da ) ;
		
		//  Feed the standard controller to the page controller
		Deposit_Application_Additional_Info c = new Deposit_Application_Additional_Info ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 0 ) ;
	}
	
    /*  ----------------------------------------------------------------------------------------
	 * Tests with an existing application passed in
     *  ---------------------------------------------------------------------------------------- */
	static testmethod void test_Session_Application ()
	{
		System.debug ( '-------------------------------- SESSION --------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Deposit_Application_Additional_Info ;
		Test.setCurrentPage ( pr1 ) ;
		
		getAccount () ;
        Deposit_Application__c da1 = Deposit_Application_Utils.getApplicationWithData ( 1 ) ;
    	da1.Current_Status__c = 'REVIEW' ;
    	da1.Identity_Verification_Response__c = 'Review (Blah Blah Blah)' ;
		
		update da1 ;
		
		// Reselect ?!
		Deposit_Application__c da1x = [
			SELECT ID, External_Key__c, 
			Application_Processing_Status__c, Additional_Information_Required__c, 
			Current_Status__c, Confirmation_Number__c, Application_Status__c
			FROM Deposit_Application__c
			WHERE ID = :da1.ID
		] ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create standard controller object from the application, pass in the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( da1x ) ;
		
		//  Feed the standard controller to the page controller
		Deposit_Application_Additional_Info c = new Deposit_Application_Additional_Info ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 1000 ) ;
		System.assertEquals ( c.docTypeListSize, 2 ) ; // requires 2 docs
		System.assertEquals ( c.appField, da1x.Confirmation_Number__c ) ;
		
		//  Trigger the upload, no attachments
		PageReference pr2 = c.upload () ;
		
		//  There should be errors in this case
		System.assertEquals ( c.flStepNumber, 1002 ) ; // Error Step!
		System.assertEquals ( c.errorName, true ) ;
		System.assertEquals ( c.errorFile, true ) ;
		System.assertEquals ( c.docTypeListSize, 2 ) ; // No document size change
		
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Utility Bill' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = da1x.id ;
		a1.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a1 ;
		
		//  Trigger the upload with attachment
		PageReference pr3 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // Success step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 1 ) ;  // One less document required!
		
		//  End of testing
		Test.stopTest () ;
	}
	
    /*  ----------------------------------------------------------------------------------------
	 * Tests with an existing application passed in
     *  ---------------------------------------------------------------------------------------- */
	static testmethod void test_Pass_In_Application ()
	{
		System.debug ( '-------------------------------- PASS IN --------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Deposit_Application_Additional_Info ;
		Test.setCurrentPage ( pr1 ) ;
		
		getAccount () ;
        Deposit_Application__c da = Deposit_Application_Utils.getApplicationWithData ( 1 ) ;
    	da.Current_Status__c = 'REVIEW' ;
    	da.Identity_Verification_Response__c = 'Review (Blah Blah Blah)' ;
    	
		update da ;
		
		//  Load attachment for prior test
		Attachment a = new Attachment () ;
		a.Name = 'Phone Bill' ;
		Blob bodyBlob = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a.body = bodyBlob ;
		a.ParentId = da.id ;
		a.IsPrivate = false ;
		
		insert a ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', da.id );
        ApexPages.currentPage().getParameters().put ( 'kid', da.External_Key__c );
        
		Deposit_Application__c eda = new Deposit_Application__c () ;
		
		//  Create standard controller object from the application, pass in the blank application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eda ) ;
		
		//  Feed the standard controller to the page controller
		Deposit_Application_Additional_Info c = new Deposit_Application_Additional_Info ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( c.flStepNumber, 1000 ) ;
		System.assertEquals ( c.docTypeListSize, 2 ) ; // Remaining number of required documents
		System.assertEquals ( c.appField, da.Confirmation_Number__c ) ;
		
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Utility Bill' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = da.id ;
		a1.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a1 ;
		
		//  Trigger the upload with attachment
		PageReference pr2 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 1001 ) ;  // Success step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 1 ) ;  // One less document required!
		
		//  Create FINAL attachment
		Attachment a2 = new Attachment () ;
		a2.Name = 'ID (Driver Lic./State ID)' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.ParentId = da.id ;
		a2.IsPrivate = false ;

		//  Write attachment to controller like page would		
		c.attachment = a2 ;
		
		//  Trigger the upload with attachment
		PageReference pr4 = c.upload () ;

		//  There should NOT be errors in this case
		System.assertEquals ( c.flStepNumber, 2000 ) ;  // FINAL step!
		System.assertEquals ( c.errorName, false ) ;
		System.assertEquals ( c.errorFile, false ) ;
		System.assertEquals ( c.docTypeListSize, 0 ) ;  // All docs!
		
		//  End of testing
		Test.stopTest () ;
	}
    
    /*  ----------------------------------------------------------------------------------------
	 * Tests with the other (ugh!) application
     *  ---------------------------------------------------------------------------------------- */
    public static testmethod void testOOPS ()
    {
		//  Point to page to test
		PageReference pr1 = Page.Deposit_Application_Additional_Info ;
		Test.setCurrentPage ( pr1 ) ;
        
        one_application__c o = one_utils.getApplicationWithData ( false ) ;
        o.FIS_Decision_Code__c = 'F003' ;
        o.Upload_Attachment_Key__c = '0uy8fgbq23r80gyb1280g' ;
        insert o ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'aid', o.id );
        ApexPages.currentPage().getParameters().put ( 'kid', o.Upload_Attachment_Key__c );
        
		Deposit_Application__c eda = new Deposit_Application__c () ;
		
		//  Create standard controller object from the application, pass in the blank application
		ApexPages.StandardController sc = new ApexPages.StandardController ( eda ) ;
		
		//  Feed the standard controller to the page controller
		Deposit_Application_Additional_Info c = new Deposit_Application_Additional_Info ( sc ) ;
		
		//  Should be in redirect
        System.assertEquals ( c.flStepNumber, 9999 ) ;
        
        //  Do the redirect
        c.redirect () ;
        
		//  End of testing
		Test.stopTest () ;
    }
}