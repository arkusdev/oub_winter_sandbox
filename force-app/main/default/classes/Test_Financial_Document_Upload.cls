@isTest
public with sharing class Test_Financial_Document_Upload
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
				
		Branch__c b = getBranch () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'test'+ i + '.person' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		insert c ;
		
		return c ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return an account
	 * ----------------------------------------------------------------------------------- */
	private static Account getAccount ( Integer i )
	{
		Account a = new Account () ;
		a.Name = 'Test' + i + 'Account' + i ;
		a.FederalTaxID__c = 'FA-000' + i ;
		
		insert a ;
		
		return a ; 
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a financial account
	 * ----------------------------------------------------------------------------------- */
	private static Financial_Account__c getFinancialAccount ( Contact c, Account a )
	{
		String loanRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Loan', 'Financial_Account__c' ) ;
		
		Branch__c b = getBranch () ;
		
		Financial_Account__c f = new Financial_Account__c () ;
		f.ACCTNBR__c = '1111333355559999' ;
		f.CURRMIACCTTYPCD__c = 'QB01' ;
		f.MJACCTTYPCD__c = 'MTG' ;
		f.OPENDATE__c = Datetime.now ().date () ;
		f.BRANCHORGNBR__c = b.ID ;
		f.RecordTypeID = loanRecordTypeId ;
		
		if ( c != null )
			f.TAXRPTFORPERSNBR__c = c.Id ;
			
		if ( a != null )
			f.TAXRPTFORORGNBR__c = a.Id ;
		
		insert f ;
		
		return f ;
	}
	
	public static testmethod void testZero ()
	{
		System.debug ( '------------------------------ Zero ------------------------------' ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		Financial_Document_Upload du = new Financial_Document_Upload ( sc ) ;
		
		System.assertEquals ( du.thisYear, System.Today().year() ) ;
		System.assertEquals ( du.priorYear, System.Today().addYears(-1).year() ) ;
		System.assertEquals ( du.priorPriorYear, System.Today().addYears(-2).year() ) ;
	}
	
	public static testmethod void testZeroLockoutZero ()
	{
		System.debug ( '------------------------------ Zero Lockout Zero ------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Financial_Document_Upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Load a contact
		Contact c1 = getContact ( 1 ) ;
		Financial_Account__c fac1 = getFinancialAccount ( c1, null ) ;
		fac1.Document_Upload_Lockout_Date__c = system.now ().addMinutes ( -5 ) ;
		update fac1 ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		Financial_Document_Upload du = new Financial_Document_Upload ( sc ) ;
		
		//  GOOD Data
		du.accountNumber = '1111333355559999' ;
		du.taxID = '0001' ;
		
		//  FINDEM
		du.findAccount () ;
		
		//  WTF
		system.assertEquals ( du.dStepNumber, 1003 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void testZeroLockoutOne ()
	{
		System.debug ( '------------------------------ Zero Lockout One ------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Financial_Document_Upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Load a contact
		Contact c1 = getContact ( 1 ) ;
		Financial_Account__c fac1 = getFinancialAccount ( c1, null ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		Financial_Document_Upload du = new Financial_Document_Upload ( sc ) ;
		
		//  Ready to start
		system.assertEquals ( du.dStepNumber, 1000 ) ;
		
		//  SPECIFIC BAD DATA #1
		du.accountNumber = '1111333355559999' ;
		du.taxID = 'NOPE1' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1002 ) ;
		
		//  SPECIFIC BAD DATA #2
		du.accountNumber = '1111333355559999' ;
		du.taxID = 'NOPE2' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1002 ) ;
		
		//  SPECIFIC BAD DATA #3
		du.accountNumber = '1111333355559999' ;
		du.taxID = 'NOPE3' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1003 ) ;
		
		//  End of testing
		Test.stopTest () ;
		
		//  Reselect
		Financial_Account__c faX = [
			SELECT ID, Document_Upload_Lockout_Date__c
			FROM Financial_Account__c
			WHERE ID = :fac1.ID
		] ;
		
		//  Check Results
		system.assertNotEquals ( faX.Document_Upload_Lockout_Date__c, null ) ;
	}
	
	public static testmethod void testOneContact ()
	{
		System.debug ( '------------------------------ One Contact ------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Financial_Document_Upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Load a contact
		Contact c1 = getContact ( 1 ) ;
		Financial_Account__c fac1 = getFinancialAccount ( c1, null ) ;
		fac1.Document_Upload_Lockout_Date__c = system.now ().addDays ( -2 ) ; //  Should pass!
		update fac1 ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		Financial_Document_Upload du = new Financial_Document_Upload ( sc ) ;
		
		//  Ready to start
		system.assertEquals ( du.dStepNumber, 1000 ) ;
		
		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1001 ) ;
		system.assertEquals ( du.errorAccountNumber, true ) ;
		system.assertEquals ( du.errorTaxID, true ) ;
		
		//  BAD DATA
		du.accountNumber = 'NYET' ;
		du.taxID = 'NOPE' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1002 ) ;
		
		//  BAD DATA #2
		du.accountNumber = '1111333355559999' ;
		du.taxID = 'NOPE' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1002 ) ;
		
		//  GOOD Data
		du.accountNumber = '1111333355559999' ;
		du.taxID = '0001' ;
		
		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1100 ) ;
		
		//  Attachment test
		du.upload () ;
		
		//  Check Results
		system.assertEquals ( du.errorNotes, true ) ;
		system.assertEquals ( du.errorDocType, true ) ;
		system.assertEquals ( du.errorName, true ) ;
		system.assertEquals ( du.errorFile, true ) ;
		
		//  Set up Ticket
		du.ticket.Description__c = 'Test Description' ;
		du.ticket.Financial_Doc_Type__c = 'Other Documents' ;
		
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Test Thingy' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.IsPrivate = false ;
		a1.Description = 'Attachment Description' ;

		//  Write attachment to controller like page would		
		du.attachment = a1 ;
		
		//  Attachment test
		du.upload () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1200 ) ;
		
		//  Reclick!
		du.confirm () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1100 ) ;
		
		//  End of testing
		Test.stopTest () ;
		
		//  Reselect
		Financial_Account__c faX = [
			SELECT ID, Document_Upload_Lockout_Date__c
			FROM Financial_Account__c
			WHERE ID = :fac1.ID
		] ;
		
		//  Check Results
		system.assertEquals ( faX.Document_Upload_Lockout_Date__c, null ) ;
	}
	
	public static testmethod void testTwoAccount ()
	{
		System.debug ( '------------------------------ Two Account ------------------------------' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.Financial_Document_Upload ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Load a contact
		Account a2 = getAccount ( 2 ) ;
		Financial_Account__c fac2 = getFinancialAccount ( null, a2 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty ticket to pass in
		Ticket__c t = new Ticket__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( t ) ;
		
		//  Feed the standard controller to the page controller
		Financial_Document_Upload du = new Financial_Document_Upload ( sc ) ;
		
		//  Ready to start
		system.assertEquals ( du.dStepNumber, 1000 ) ;
		
		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1001 ) ;
		system.assertEquals ( du.errorAccountNumber, true ) ;
		system.assertEquals ( du.errorTaxID, true ) ;
		
		//  bad data #1
		du.accountNumber = 'NYET' ;
		du.taxID = 'NOPE' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1002 ) ;
		
		//  BAD DATA #2
		du.accountNumber = '1111333355559999' ;
		du.taxID = 'NOPE' ;

		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1002 ) ;
		
		//  GOOD Data
		du.accountNumber = '1111333355559999' ;
		du.taxID = '0002' ;
		
		//  FINDEM
		du.findAccount () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1100 ) ;
		
		//  Attachment test
		du.upload () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1101 ) ;
		system.assertEquals ( du.errorNotes, true ) ;
		system.assertEquals ( du.errorDocType, true ) ;
		system.assertEquals ( du.errorName, true ) ;
		system.assertEquals ( du.errorFile, true ) ;
		
		//  Set up Ticket
		du.ticket.Description__c = 'Test Description' ;
		du.ticket.Financial_Doc_Type__c = 'Other Documents' ;
		
		//  Create new attachment
		Attachment a1 = new Attachment () ;
		a1.Name = 'Test Thingy' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.IsPrivate = false ;
		a1.Description = 'Attachment Description' ;

		//  Write attachment to controller like page would		
		du.attachment = a1 ;
		
		//  Attachment test
		du.upload () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1200 ) ;
		
		//  Reclick!
		du.confirm () ;
		
		//  Check Results
		system.assertEquals ( du.dStepNumber, 1100 ) ;
		
		//  End of testing
		Test.stopTest () ;
		
		//  Reselect
		Financial_Account__c faX = [
			SELECT ID, Document_Upload_Lockout_Date__c
			FROM Financial_Account__c
			WHERE ID = :fac2.ID
		] ;
		
		//  Check Results
		system.assertEquals ( faX.Document_Upload_Lockout_Date__c, null ) ;
	}
}