global class batch_Deactivate_Advocate_Users implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;
    
    private static final String JOB_TYPE = 'Salesforce Batch Apex' ;
    private static final String JOB_NAME = batch_Advocate_SWIM_Reward.class.getName () ;
    global String status ;

    global batch_Deactivate_Advocate_Users ()
    {
        status = 'WARN' ;
        
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- START --------------------' ) ;
        
		String query ;
		query  = '' ;
        query += 'SELECT ' ;
        query += 'Id ';
        query += 'FROM User ';
        query += 'WHERE IsActive = TRUE ';
        query += 'AND UserType = \'CspLitePortal\' ' ;
        query += 'AND Profile.Name = \'OneUnited Advocate Apps User\' ';
        
        if ( ( bps != null ) && ( bps.Advocate_User_Days_Inactive__c != null ) && ( bps.Advocate_User_Days_Inactive__c > 0.0 ) )
        {
            Datetime lastLoginDate = System.now ().addDays ( Integer.valueOf ( -bps.Advocate_User_Days_Inactive__c ) ) ;
            String lastLoginDateFormat = lastLoginDate.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') ;
            
	        query += 'AND ( ( Contact.Has_Active_Account__c = FALSE ) ' ;
            query += 'OR ( LastLoginDate <= ' + lastLoginDateFormat + ' ) ) ' ;
        }
        else
        {
	        query += 'AND Contact.Has_Active_Account__c = FALSE ' ;
        }
        
        query += '' ;
        
        System.debug ( 'QQQ :: ' + query ) ;
        
		return Database.getQueryLocator ( query ) ;
    }
    
	global void execute ( Database.Batchablecontext bc, list<User> uL )
	{
		System.debug ( '-------------------- EXECUTE --------------------' ) ;
        
        if ( ( uL != null ) && ( uL.size () > 0 ) )
        {
            for ( User u : uL )
            {
                //  Set users to inactive
                u.IsActive = false ;
            }
            
            //  Update user records
            update uL ;
        }
    }
    
	global void finish ( Database.Batchablecontext bc )
	{
		System.debug ( '-------------------- FINISH --------------------' ) ;
        
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
      	OrgWideEmailAddress o = [
      		SELECT ID
      		FROM OrgWideEmailAddress
      		WHERE Address = 'customersupport@oneunited.com'
            ORDER BY ID
            LIMIT 1
      	] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c,
				'helpdesk@OneUnited.com' 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email,
				'helpdesk@OneUnited.com' 
			} ;
		}
   
        if ( ( a.Status.equalsIgnoreCase ( 'Failed' ) ) || ( a.NumberOfErrors > 0 ) )
            status = 'ERROR' ;
        else if ( a.Status.equalsIgnoreCase ( 'Aborted' ) )
            status = 'WARN' ;
        else if ( ( a.TotalJobItems >= 0 ) && ( a.NumberOfErrors == 0 ) )
            status = 'GOOD' ;
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( '[' + status + '][' + JOB_NAME + '][' + JOB_TYPE + '] Advocate Batch SWIM Reward' ) ;
   
		if ( o != null )
			mail.setOrgWideEmailAddressId ( o.Id ) ;
		
		String message = '' ;
		message += '<h1>Results from deactivating Advocate users without active accounts:</h1>' ;
		message += '<p>The batch processed ' + a.TotalJobItems +' batches ' ;
		if ( a.NumberOfErrors > 0 )
			message += 'with '+ a.NumberOfErrors + ' failures.' ;
        else
            message += ' with no failures.' ;
		message += '</p>' ;
		message += '<br/>' ;

   		mail.setPlainTextBody ( '' ) ;  // WTB no "null"
		mail.setHtmlBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
    }
}