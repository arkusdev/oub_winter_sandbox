@isTest (SeeAllData=true)
public with sharing class Test_Batch_SWMFileTicket 
{
	static testmethod void oppTest ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c = new Contact () ;
		c.FirstName = 'Test11' ;
		c.LastName = 'User11' ;
		c.Email = 'test11@user11.com' ;
		
		insert c ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c.Id ;
		fa.CREDITLIMITAMT__c = 250 ;
		
		insert fa ; 
		
		one_application__c o = new one_application__c () ;
		
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		
		o.Contact__c = c.Id;
		
		o.First_Name__c = c.FirstName ;
		o.Last_Name__c = c.LastName ;
		o.Email_Address__c = c.Email ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		
		o.Referred_By_Contact__c = c.Id;
		o.Entered_By_Contact__c = c.Id;

		o.FIS_Application_ID__c = '111222333' ;
		
		o.FIS_Decision_Code__c = 'P012' ;
		
		o.Funding_Account_Number__c = '12345679' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.Funding_Routing_Number__c = '123456789' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		
		o.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		o.Trial_Deposit_1__c = 7 ;
		o.Trial_Deposit_2__c = 11 ;
		
		o.ACH_Funding_Status__c = 'Originated' ;
		
		insert o ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Approved for Refund' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		t.Application__c = o.Id ;
		t.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		
		insert t ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_SWMFileTicket b = new batch_SWMFileTicket () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}
}