@isTest
public class test_batch_Advocate_SWIM_Reward 
{
	/* --------------------------------------------------------------------------------
	 * Custom settings
	 * -------------------------------------------------------------------------------- */
	private static Batch_Process_Settings__c insertBatchCustomSetting ( String settleDays )
    {
        Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;

        bps.Name = 'OneUnited_Batch' ;        
        bps.ACH_GL_Account__c = '1001000100' ;
        bps.ACH_Outgoing_Account__c = '1001000101' ;
        bps.Application_Cashbox__c = '666' ;
        bps.Batch_Email__c = 'lmattera@oneunited.com' ;
        bps.Collateral_GL_Account__c = '1001000102' ;
        bps.Deposit_ePay_GL_Account__c = '1001000103' ;
        bps.Email_Enabled__c = false ;
        bps.Routing_Number__c = '01100127' ;
        bps.UNITY_Visa_ePay_GL_Account__c = '1001000104' ;
        bps.SWIM_Debit_Card_Settlement_Days__c = settleDays ;
        bps.Advocate_Reward_GL_Account__c = '1001000105' ;
        bps.Advocate_Reward_Amount__c = 25.00 ;
        bps.Advocate_Reward_Cashbox__c = '666' ;
        
        insert bps ;
        
        return bps ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i, Branch__c b, String customerRecordTypeID )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'test'+ i + '.person' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.Branch__c = b.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Setup, financial account
	 * -------------------------------------------------------------------------------- */
    private static Financial_Account__c getFinancialAccount ( String acctnbr, Contact c, String rt, String major )
    {
        Financial_Account__c fa = new Financial_Account__c () ;
        
		fa.recordTypeId = rt ;
		fa.MJACCTTYPCD__c = major ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'OneUnited Test Account' ;
		fa.NOTEBAL__c = 1000 ;
		fa.TAXRPTFORPERSNBR__c = c.Id ;
        fa.ACCTNBR__c = acctnbr ;
		
        return fa ;
    }
    
    private static Financial_Account_Contact_Role__c getFACR ( Financial_Account__c fa, Contact c, String xtype )
    {
        Financial_Account_Contact_Role__c facr = new Financial_Account_Contact_Role__c () ;
		facr.Contact__c = c.ID ;
        facr.Financial_Account__c = fa.ID ;
        facr.Financial_Account_Role__c = xtype ;
        facr.External_ID__c = fa.ACCTNBR__c + '-' + c.TaxID__c ;
        
        return facr ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  Advocate
	//  --------------------------------------------------------------------------------------
    private static Advocate_Reward__c setupAdvocateCrap ()
    {
        Advocate_Activity__c aa1 = new Advocate_Activity__c () ;
        aa1.Name = 'Referral' ;
        aa1.API_Name__c = 'Referral' ;
        aa1.Activity_Points__c = 500.0 ;
        aa1.Activity_Type__c = 'Referral' ;
        aa1.Public_Use__c = true ;
        aa1.Description__c = 'Referral' ;
        aa1.Internal_Description__c = 'Referral' ;
        
        insert aa1 ;
        
        Advocate_Reward__c ar1 = new Advocate_Reward__c () ;
        ar1.Name = 'The_Hook_Up' ;
        ar1.Public_Use__c = true ;
        ar1.Description__c = 'ABS123' ;
        ar1.Internal_Description__c = 'ARS123' ;
        ar1.API_Name__c = 'The_Hook_Up' ;
        
        insert ar1 ;
        
        return ar1 ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  ONE
	//  --------------------------------------------------------------------------------------
	public static testmethod void ONE ()
    {
        // ---------- SETUP ----------
        Advocate_Reward__c ar1 = setupAdvocateCrap () ;
        insertBatchCustomSetting ( '0' ) ;
        
        Branch__c b1 = getBranch () ;
        insert b1 ;
        
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit', 'Financial_Account__c' ) ;
        
        // ---------- REFERRER ----------
        Contact c1 = getContact ( 1, b1, customerRecordTypeID ) ;
        insert c1 ;
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        
        Financial_Account__c f1a = getFinancialAccount ( '1001231234', c1, rtID, 'CK' ) ;
        faL.add ( f1a ) ;
        
        Financial_Account__c f1b = getFinancialAccount ( '1001231235', c1, rtID, 'SAV' ) ;
        faL.add ( f1b ) ;
        
        insert faL ;
        
        list<Financial_Account_Contact_Role__c> facrL = new list<Financial_Account_Contact_Role__c> () ;
        
        Financial_Account_Contact_Role__c facr1a = getFACR ( faL [ 0 ], c1, 'Tax Owner' ) ;
        facrL.add ( facr1a ) ;
        
        Financial_Account_Contact_Role__c facr1b = getFACR ( faL [ 1 ], c1, 'Tax Owner' ) ;
        facrL.add ( facr1b ) ;
        
        insert facrL ;
        
        c1.Personal_Checking_Accounts__c = 1.0 ;
        c1.Personal_Savings_Accounts__c = 1.0 ;
        update c1 ;
        
        Referral_Code__c rc1 = [SELECT ID, Name FROM Referral_Code__c WHERE Contact__c = :c1.ID LIMIT 1 ] ;
        
        System.assertNotEquals ( rc1, null ) ; // <--- HAZ Referral Code ?!
        
        // ---------- REFERREE ----------
        Contact c2 = getContact ( 2, b1, customerRecordTypeID ) ;
        insert c2 ;
        
        Financial_Account__c f2 = getFinancialAccount ( '1003213210', c2, rtID, 'SAV' ) ;
        f2.Advocate_Referral_Code__c = rc1.ID ;
        f2.Advocate_Referral_Code_Text__c = rc1.Name ;
        insert f2 ;

        c2.Personal_Savings_Accounts__c = 1.0 ;
        update c2 ;
        
        // ---------- HACK! ----------
        Advocate_Reward_Achieved__c ara1 = new Advocate_Reward_Achieved__c () ;
        ara1.Advocate_Reward__c = ar1.ID ;
        ara1.Contact_Referred__c = c2.ID ;
       	ara1.Financial_Account__c = f2.ID ;
        ara1.Status__c = 'New' ;
        ara1.Contact__c = c1.ID ;
        insert ara1 ;
        
        // ---------- GO ?!? ----------
        Test.startTest () ;
        
        batch_Advocate_SWIM_Reward basr = new batch_Advocate_SWIM_Reward () ;
        Database.executeBatch ( basr ) ;
        
        Test.stopTest () ;
    }

	//  --------------------------------------------------------------------------------------
	//  TWO
	//  --------------------------------------------------------------------------------------
	public static testmethod void TWO ()
    {
        // ---------- SETUP ----------
        Advocate_Reward__c ar1 = setupAdvocateCrap () ;
        insertBatchCustomSetting ( '0' ) ;
        
        Branch__c b1 = getBranch () ;
        insert b1 ;
        
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		String rtID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposit', 'Financial_Account__c' ) ;
        
        // ---------- REFERRER ----------
        Contact c1 = getContact ( 1, b1, customerRecordTypeID ) ;
        insert c1 ;
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        
        Financial_Account__c f1a = getFinancialAccount ( '1001231234', c1, rtID, 'CK' ) ;
        faL.add ( f1a ) ;
        
        Financial_Account__c f1b = getFinancialAccount ( '1001231235', c1, rtID, 'SAV' ) ;
        faL.add ( f1b ) ;
        
        insert faL ;
        
        c1.Personal_Checking_Accounts__c = 1.0 ;
        c1.Personal_Savings_Accounts__c = 1.0 ;
        update c1 ;
        
        Referral_Code__c rc1 = [SELECT ID, Name FROM Referral_Code__c WHERE Contact__c = :c1.ID LIMIT 1 ] ;
        
        System.assertNotEquals ( rc1, null ) ; // <--- HAZ Referral Code ?!
        
        // ---------- REFERREE ----------
        Contact c2 = getContact ( 2, b1, customerRecordTypeID ) ;
        insert c2 ;
        
        Financial_Account__c f2 = getFinancialAccount ( '1003213210', c2, rtID, 'SAV' ) ;
        f2.Advocate_Referral_Code__c = rc1.ID ;
        f2.Advocate_Referral_Code_Text__c = rc1.Name ;
        insert f2 ;

        c2.Personal_Savings_Accounts__c = 1.0 ;
        update c2 ;
        
        // ---------- HACK! ----------
        Advocate_Reward_Achieved__c ara1 = new Advocate_Reward_Achieved__c () ;
        ara1.Advocate_Reward__c = ar1.ID ;
        ara1.Contact_Referred__c = c2.ID ;
       	ara1.Financial_Account__c = f2.ID ;
        ara1.Status__c = 'New' ;
        ara1.Contact__c = c1.ID ;
        insert ara1 ;
        
        // ---------- GO ?!? ----------
        Test.startTest () ;
        
        batch_Advocate_SWIM_Reward basr = new batch_Advocate_SWIM_Reward () ;
        Database.executeBatch ( basr ) ;
        
        Test.stopTest () ;
    }
}