public with sharing class card_stock_order_v2 
{
    // cardType = 'ATM Card' or 'Debit Card'
    @AuraEnabled(cacheable=true)
    public static String getCards(String cardType) {
        List<Card_Design__mdt> cardDesigns = [SELECT Design_Code__c, Image_URL__c, Type__c, Order__c, DeveloperName
                                              FROM Card_Design__mdt 
                                              WHERE Type__c = :cardType 
                                              	AND Active__c = true 
                                              ORDER BY Order__c ASC];
        System.debug(cardDesigns);
        
        return JSON.serialize( cardDesigns );
    }
    
    @AuraEnabled
    public static String submitTicket(Map<String, Object> info) {
        try
        {
            Contact c = getContactById( getCurrentUserContactId() );
            
            if ( c != null )
            {
                String last4Digits = (String) info.get('last4Digits');
                String cardType = (String) info.get('cardType');
                
                /*
                if ( verifyLast4(c.Id, last4Digits, cardType) )
                {
				*/
                    Ticket__c t = new Ticket__c();
                    t.Status__c = 'Received' ;
                    t.Reason_Action_Requested__c = 'Customer Requested' ;
                    t.Maintenance_Action__c = 'Re-Issue Card' ;
                    t.Website_Request__c = true ;
                    t.Card_Maintenance_Type__c = 'Consumer Debit - Maintenance' ;
                    t.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Card Maintenance Request', 'Ticket__c' ) ;
                    
                    // Contact fields
                    t.Contact__c = c.ID;
                    t.Street_Address__c = c.MailingStreet;
                    t.City__c = c.MailingCity;                
                    t.State__c = c.MailingState;
                    if ( c.MailingPostalCode != null ) // need to add this b/c there's no exception handling for it
                    {
                        t.Zip_Code__c = OneUnitedUtilities.cleanZipCode ( c.MailingPostalCode );
                    }
                    t.Email_Address__c = c.Email;
                    t.Date_of_Birth__c = c.Birthdate;
                    
                    if ( c.Name != null )
                    {
                        List<String> names = c.Name.split(' ', 2);
                        
                        t.First_Name__c = names[0];
                        t.Last_Name__c = names[1];
                    }
                    
                    // Information passed
                    t.Card_Number__c = 'XXXXXXXXXXXX' + last4Digits ;
                    // t.Card_Number__c = last4Digits;
                    t.Card_Type__c = cardType;
                    t.Plastic_Type__c = (String) info.get('plastic'); // (name of card, i.e. Mona, Amir, Classic, Lady, Doonie, Justice, King, Queen, Black Classic)
                    
                    insert t;
                    
                    Ticket__c t2 = [SELECT Id, Name FROM Ticket__c WHERE Id = :t.Id];
                    
                    // Return the ticket confirmation #
                    return t2.Name;
                /*
                }
                else
                {
                    System.debug('submitTicket --- invalid last 4 digits');
                    return 'Invalid last 4 digits';
                }
				*/
            }
            else
            {
                System.debug('submitTicket --- No contact found. Cannot get user context.');
                return '';
            }
        }
        catch (Exception e)
        {
            System.debug('submitTicket --- error: ' + e);
            return '';
        }
    }
    
    @AuraEnabled(cacheable=true)
    // card type = 'ATM Card' or 'Debit Card'
    public static String getServices(String cardType) {
        try
        {
            String ct;
            String contactId = getCurrentUserContactId();
            
            String rti = Schema.SObjectType.Service__c.RecordTypeInfosByDeveloperName.get('Agreement').RecordTypeId;
            
            List<Service__c> servList = [SELECT Card_Number__c
                                        FROM Service__c
                                        WHERE Contact__c = :contactId
                                        	AND RecordTypeId = :rti
                                        	AND ( Status__c = 'ACTIVE' OR Status__c = 'Issued' )
                                        	AND Type__c = :cardType];
            
            List<String> allCardsLast4 = new List<String>();
            if ( servList.size() != 0 )
            {
                for ( Service__c s : servList )
                {
                    if ( String.isNotBlank ( s.Card_Number__c ) )
	                    allCardsLast4.add( s.Card_Number__c.substring( s.Card_Number__c.length()-4 ) );
                    else
                        System.debug ( 'Card Number missing ?!?' ) ;
                }
            }
            
            return JSON.serialize( allCardsLast4 );
        }
        catch (Exception e)
        {
            System.debug('getAssocServices --- did not find any service records!');
            System.debug('getAssocServices --- error: ' + e);
            return null;
        }
    }
    
    /*
    public static Boolean verifyLast4(String contactId, String last4Digits, String cardType) {
		Boolean isValidLast4 = false;

		List<String> allCardsLast4 = getLast4List(contactId, cardType);

		if ( allCardsLast4.size() != 0 )
        {
        	for ( String s : allCardsLast4 )
            {
                if ( s == last4Digits )
                {
                    isValidLast4 = true;
                }
            }
        }
        
        return isValidLast4;
    }
    
    // Returns List<String> of the last 4 digits of all contact's card numbers,
    // which are retrieved by looking for associated Service__c records
    public static List<String> getLast4List(String contactId, String cardType) {
        try
        {
            String ct;
            
            if ( cardType == 'ATM' )
            {
                ct = 'ATM Card';
            }
            else
            {
                ct = 'Debit Card';
            }
            
            String rti = Schema.SObjectType.Service__c.RecordTypeInfosByDeveloperName.get('Agreement').RecordTypeId;
            
            List<Service__c> servList = [SELECT Card_Number__c
                                        FROM Service__c
                                        WHERE Contact__c = :contactId
                                        	AND RecordTypeId = :rti
                                        	AND ( Status__c = 'ACTIVE' OR Status__c = 'Issued' )
                                        	AND Type__c = :ct];
            
            List<String> allCardsLast4 = new List<String>();
            if ( servList.size() != 0 )
            {
                for ( Service__c s : servList )
                {
                    allCardsLast4.add( s.Card_Number__c.substring( s.Card_Number__c.length()-4 ) );
                }
            }
            
            return allCardsLast4;
        }
        catch (Exception e)
        {
            System.debug('getAssocServices --- did not find any service records!');
            System.debug('getAssocServices --- error: ' + e);
            return null;
        }
    }
	*/
    
    public static String getCurrentUserContactId() {
        try
        {
            User user = [SELECT ContactID FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
            return user.ContactId;
        }
        catch (Exception e)
        {
            System.debug('getCurrentUserContactId --- error: ' + e);
            return null;
        }
    }
    
    public static Contact getContactById(String contactId) {
        try
        {
            Contact c = [SELECT Name, MailingStreet, MailingCity, MailingState, MailingPostalCode, Email, Birthdate
                        FROM Contact
                        Where Id = :contactId];
            return c;
        }
        catch (Exception e)
        {
            System.debug('getContactById --- error: ' + e);
            return null;
        }
    }
}