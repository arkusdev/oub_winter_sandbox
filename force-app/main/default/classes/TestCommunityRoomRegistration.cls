@isTest
public with sharing class TestCommunityRoomRegistration 
{
	static testmethod void testOneTicketEmpty ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Community_Room_Registration crr = new Community_Room_Registration ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( crr.cmStepNumber, 1000 ) ;
		
		//  Empty ticket
		Ticket__c t = crr.ticket ;
		
		//  Submit
		crr.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( crr.cmStepNumber, 1001 ) ;		
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testTwoTicketBad ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Community_Room_Registration crr = new Community_Room_Registration ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( crr.cmStepNumber, 1000 ) ;
		
		//  Load the ticket with bad data
		Ticket__c t = crr.ticket ;
		t.Zip_Code__c = 'xyzab' ;
		t.Phone__c = 'xyz-abc-hjkl' ;
		t.Email_Address__c = 'none@none.com' ;
		crr.pickDate = '10/10/2001' ;
		t.Beginning_Time__c = '10:30 AM PST' ;
		t.Ending_Time__c = '9:30 AM PST' ;
		
		//  Submit
		crr.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( crr.cmStepNumber, 1001 ) ;		
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testThreeTicketGood ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		Community_Room_Registration crr = new Community_Room_Registration ( stdC ) ;
		
		//  We start at 1000
		System.assertEquals ( crr.cmStepNumber, 1000 ) ;
		
		//  Load the ticket
		Ticket__c t = crr.ticket ;
		
		t.Name_of_Organization__c = 'Test Organization' ;
		t.Street_Address__c = '123 Test Street' ;
		t.City__c = 'Test City' ;
		t.State__c = 'CA' ;
		t.Zip_Code__c = '13245-6597' ;
		
		t.First_Name__c = 'Test' ;
		t.Last_Name__c = 'User' ;
		t.Phone__c = '123-677-6749' ;
		t.Email_Address__c = 'test@user.com' ;
		crr.confirmEmail = 'test@user.com' ;
		
		crr.pickDate = '12/31/2095' ;
		t.Beginning_Time__c = '9:30 AM PST' ;
		t.Ending_Time__c = '2:30 PM PST' ;
		t.Purpose_of_Meeting__c = 'Test Meeting' ;
		t.Number_of_People_Expected__c = 25 ;
		
		//  Submit
		crr.submitTicket () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( crr.cmStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
}