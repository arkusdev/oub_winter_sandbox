global class batch_NewAccountEmail_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Debit_Card_Email b = new batch_Debit_Card_Email () ;
		Database.executeBatch ( b ) ;
   	}
}