public class HMDAReportExportHelper 
{
    private static final String CARRIAGE_RETURN = '\n' ;
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Constructor
	 *  -------------------------------------------------------------------------------------------------- */   
    public HMDAReportExportHelper ()
    {
        // ?
    }

    /*  --------------------------------------------------------------------------------------------------
	 *  Generate a file
	 *  -------------------------------------------------------------------------------------------------- */
    public static Attachment generateHMDAFile ( list<HMDA_Report__c> hrL, ID parentID )
    {
        list<String> recordLines = new list<String> () ;
        
        String hmdaFile = '' ;
        hmdaFile += getHeader () + CARRIAGE_RETURN ;
        
        for ( HMDA_Report__c hr : hrL )
            hmdaFile += createRecord ( hr ) + CARRIAGE_RETURN ;
        
        //  File name
        Datetime dt = System.now () ;
        String fileName = 'HMDA_Export_' + dt.format ( 'yyyyMMddhhmmss' ) + '.CSV' ;
        
        Attachment a = new Attachment () ;
        a.ParentId = parentID ;
        a.Name = fileName ;
        a.ContentType = 'Text/csv' ;
        a.Body = Blob.valueOf ( hmdaFile ) ;
        
        return a ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Generate a record
	 *  -------------------------------------------------------------------------------------------------- */
    private static String createRecord ( HMDA_Report__c hr )
    {
        String hmdaExportLine = '' ;

        hmdaExportLine += nullToBlankQuoteComma ( hr.Application_Number__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.Applicants_Name__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.PROPSTREET__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.PROPSTATE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.PROPZIP__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.Request_Date__c ) ;

        hmdaExportLine += convertDateQuoteComma ( hr.Request_Date__c ) ;

		hmdaExportLine += nullToBlankQuoteComma ( hr.LNTYPE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.Property_Type__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.LNPURPOSE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.Occupancy__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.LNAMOUNT__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.PREAPPR__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.ACTION__c ) ;

        hmdaExportLine += convertDateQuoteComma ( hr.ACTDATE__c ) ;

        hmdaExportLine += '"' + '' + '",' ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.STCODE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.CNTYCODE__c ) ;

		hmdaExportLine += nullToBlankQuoteComma ( hr.CENSUSTRCT__c ) ;
        hmdaExportLine += convertMultiPicklist ( hr.APETH__c, hr.AP_Hispanic_Latino_ETH__c ) ;
        hmdaExportLine += convertMultiPicklist ( hr.CAPETH__c, hr.CAP_Hispanic_Latino_ETH__c ) ;
        hmdaExportLine += convertMultiPicklist ( hr.APRACE__c, hr.AP_Asian_Race__c, hr.AP_Hawaiian_Race__c ) ;
        hmdaExportLine += convertMultiPicklist ( hr.CAPRACE__c, hr.CAP_Asian_Race__c, hr.CAP_Hawaiian_Race__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.APSEX__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.CAPSEX__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.TINCOME__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.PURCHTYPE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.DENIALR__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // DENIAL 2
        hmdaExportLine += '"' + '' + '",' ; // DENIAL 3
        hmdaExportLine += '"' + '' + '",' ; // APR
		hmdaExportLine += nullToBlankQuoteComma ( hr.SPREAD__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // Lockdate?
		hmdaExportLine += nullToBlankQuoteComma ( hr.LOAN_TERM__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.HOEPA__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.LIENSTAT__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // Matdate
		hmdaExportLine += nullToBlankQuoteComma ( hr.NOTE_RATE__c ) ;
        
		hmdaExportLine += nullToBlankQuoteComma ( hr.PNTSFEES__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // LTV
		hmdaExportLine += nullToBlankQuoteComma ( hr.CLTV__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // debt ratio
		hmdaExportLine += nullToBlankQuoteComma ( hr.COMB_RATIO__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // Loan program
		hmdaExportLine += nullToBlankQuoteComma ( hr.Marital_Status__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // co?
		hmdaExportLine += nullToBlankQuoteComma ( hr.APL_AGE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.CO_APL_AGE__c ) ;
        hmdaExportLine += '"' + '' + '",' ;  // pre-paid finance
        hmdaExportLine += '"' + '' + '",' ;  // broker fee
		hmdaExportLine += nullToBlankQuoteComma ( hr.BrokerCompPaidBy__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // branch ID
        hmdaExportLine += '"' + '' + '",' ; // branch Name
        hmdaExportLine += '"' + '' + '",' ; // officer id
        hmdaExportLine += '"' + '' + '",' ; // officer name

		hmdaExportLine += nullToBlankQuoteComma ( hr.LOANREP__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // Name?
        hmdaExportLine += '"' + '' + '",' ; // processor
        hmdaExportLine += '"' + '' + '",' ; // processor name
        hmdaExportLine += '"' + '' + '",' ; // underwriter
        hmdaExportLine += '"' + '' + '",' ; // underwriter name
        hmdaExportLine += '"' + '' + '",' ; // broker
        hmdaExportLine += '"' + '' + '",' ; // broker name
        hmdaExportLine += '"' + '' + '",' ; // udef1
        hmdaExportLine += '"' + '' + '",' ; // udef1 name
        hmdaExportLine += '"' + '' + '",' ; // udef2
        hmdaExportLine += '"' + '' + '",' ; // udef2 name
		hmdaExportLine += nullToBlankQuoteComma ( hr.INCLV__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.Channel__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // discount points
        hmdaExportLine += '"' + '' + '",' ; // doc type
		hmdaExportLine += nullToBlankQuoteComma ( hr.DPTS_DL__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.Channel__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // house prp
        hmdaExportLine += '"' + '' + '",' ; // hud 1400
        hmdaExportLine += '"' + '' + '",' ; // annual income

        hmdaExportLine += '"' + '' + '",' ; // loan grade
        hmdaExportLine += '"' + '' + '",' ; // orig dl
        hmdaExportLine += '"' + '' + '",' ; // orig fee
        hmdaExportLine += '"' + '' + '",' ; // penalty
        hmdaExportLine += '"' + '' + '",' ; // prospect
        hmdaExportLine += '"' + '' + '",' ; // latitude
        hmdaExportLine += '"' + '' + '",' ; // longitude
        hmdaExportLine += '"' + '' + '",' ; // limits
        hmdaExportLine += '"' + '' + '",' ; // month debit
        hmdaExportLine += '"' + '' + '",' ; // net worth
        hmdaExportLine += '"' + '' + '",' ; // BCOMPPCT
		hmdaExportLine += nullToBlankQuoteComma ( hr.AMORTTYPE__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.INITADJMOS__c ) ;
		hmdaExportLine += nullToBlankQuoteComma ( hr.APPTAKENBY__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // CAPPTAKENBY
		hmdaExportLine += nullToBlankQuoteComma ( hr.OCCUPYURLA__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // Amort Term
		hmdaExportLine += nullToBlankQuoteComma ( hr.APPRVALUE__c ) ;
    
        hmdaExportLine += nullToBlankQuoteComma ( hr.PREPAY_MOS__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.APCRSCORE__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.CAPCRSCORE__c ) ;
        hmdaExportLine += '"' + '' + '",' ;
        hmdaExportLine += '"' + '' + '",' ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.TotalOriginationCharges__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // upb
        hmdaExportLine += '"' + '' + '",' ; // io
        hmdaExportLine += '"' + '' + '",' ; // pmi
        hmdaExportLine += '"' + '' + '",' ; // prime
        hmdaExportLine += '"' + '' + '",' ; // sold date
        hmdaExportLine += '"' + '' + '",' ; // lnk second
        hmdaExportLine += '"' + '' + '",' ; // FAS_140
        hmdaExportLine += '"' + '' + '",' ; // QM
        hmdaExportLine += '"' + '' + '",' ; // Servicing
        hmdaExportLine += '"' + '' + '",' ; // LN AMOUNT PR
        hmdaExportLine += nullToBlankQuoteComma ( hr.LNAMOUNTFL__c ) ;
        
    	hmdaExportLine += nullToBlankQuoteComma ( hr.UniversalLoanIdentifier__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.APOtherHispanicLatino__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.CAPOtherHispanicLatino__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.APEthnicMethod__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.CAPEthnicMethod__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.APTribe__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.CAPTribe__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.APOtherAsian__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.CAPOtherAsian__c ) ;
    	hmdaExportLine += nullToBlankQuoteComma ( hr.APOtherPacificIslander__c ) ;
        
        hmdaExportLine += nullToBlankQuoteComma ( hr.CAPOtherPacificIslander__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.APRaceMethod__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.CAPRaceMethod__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.APGenderMethod__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.CAPGenderMethod__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // DENIALR4
        hmdaExportLine += nullToBlankQuoteComma ( hr.DenialOtherDescription__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // ConstructionMethod
        hmdaExportLine += '"' + '' + '",' ; // ApplicationChannel
        hmdaExportLine += '"' + '' + '",' ; // ApplicationPayable

        hmdaExportLine += '"' + '' + '",' ; // APCreditProviderOtherDescription
        hmdaExportLine += '"' + '' + '",' ; // CAPCreditProviderOtherDescription
        hmdaExportLine += '"' + '' + '",' ; // AUS_TYPE1
        hmdaExportLine += nullToBlankQuoteComma ( hr.AUS_Decision__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // AUS_TYPE2
        hmdaExportLine += '"' + '' + '",' ; // AUS_DECISION2
        hmdaExportLine += '"' + '' + '",' ; // AUS_TYPE3
        hmdaExportLine += '"' + '' + '",' ; // AUS_DECISION3
        hmdaExportLine += '"' + '' + '",' ; // AUS_TYPE4
        hmdaExportLine += '"' + '' + '",' ; // AUS_DECISION4
        hmdaExportLine += '"' + '' + '",' ; // AUS_TYPE5
        
        hmdaExportLine += '"' + '' + '",' ; // AUS_DECISION5
	    hmdaExportLine += nullToBlankQuoteComma ( hr.AUS_TYPE_OTHER__c ) ;
	    hmdaExportLine += nullToBlankQuoteComma ( hr.AUS_DECISION_OTHER__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.LenderCredits__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.BalloonIndicator__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.InterestOnlyIndicator__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.NegativeAmortizationIndicator__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.ReverseMortgageIndicator__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.LOCIndicator__c ) ;

        hmdaExportLine += nullToBlankQuoteComma ( hr.OtherNonAmortizingIndicator__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.BusinessCommercialIndicator__c ) ;
        hmdaExportLine += '"' + '' + '",' ; // ManufacturedHomePropertyInterest
        hmdaExportLine += '"' + '' + '",' ; // ManufacturedHomePropertyType
        hmdaExportLine += nullToBlankQuoteComma ( hr.MFAffordableUnitsCount__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.UnitsCount__c ) ;
        hmdaExportLine += nullToBlankQuoteComma ( hr.TotalLoanCosts__c ) ;

        return hmdaExportLine ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Return the header
	 *  -------------------------------------------------------------------------------------------------- */   
    private static String getHeader ()
    {
        String x = '' ;
        
		x += 'APLNNO,APPNAME,PROPSTREET,PROPCITY,PROPSTATE,PROPZIP,APDATE,LNTYPE,PROPTYPE,LNPURPOSE,OCCUPANCY,LNAMOUNT,PREAPPR,ACTION,ACTDATE,MACODE,STCODE,CNTYCODE,' ;
        x += 'CENSUSTRCT,APETH,CAPETH,APRACE,CAPRACE,APSEX,CAPSEX,TINCOME,PURCHTYPE,DENIALR1,DENIALR2,DENIALR3,APR,SPREAD,LOCKDATE,LOAN_TERM,HOEPA,LIENSTAT,MATDATE,NOTE_RATE,' ;
        x += 'PNTSFEES,LTV,CLTV,DEBT_RATIO,COMB_RATIO,LOAN_PROG,MARITAL,MARITALC,APL_AGE,CO_APL_AGE,PRPAID_FIN,BROKER_FEE,BrokerCompPaidBy,BRANCHID,BRANCHNAME,OFFICERID,OFFICERNAME,' ;
        x += 'LOANREP,LOANREPNAME,PROCESSOR,PRONAME,UNDERWRTR,UWNAME,BROKER,BROKERNAME,UDEF1,UDEF1NAME,UDEF2,UDEF2NAME,INCLVL,CHANNEL,DISC_PNT,DOC_TYPE,DPTS_DL,HOUSEPRP,HUD_1400,INC_UWRT,' ;
        x += 'LN_GRADE,ORIG_DL,ORIG_FEE,PENALTY,PROSPECT,LATITUDE,LONGITUDE,LIMITS,MNTHDEBT,NETWORTH,BCOMPPCT,AMORTTYPE,INITADJMOS,APPTAKENBY,CAPPTAKENBY,OCCUPYURLA,AMORT_TERM,APPRVALUE,' ;
        x += 'PREPAY_MOS,APCRSCORE,CAPCRSCORE,APCRPROV,CAPCRPROV,TotalOriginationCharges,UPB_AMOUNT,IO_MONTHS,PMI,PRIME,SOLD_DATE,LNK_SECOND,FAS_140,QM,SERVICING,LNAMOUNTPR,LNAMOUNTFL,' ;
        x += 'UniversalLoanIdentifier,APOtherHispanicLatino,CAPOtherHispanicLatino,APEthnicMethod,CAPEthnicMethod,APTribe,CAPTribe,APOtherAsian,CAPOtherAsian,APOtherPacificIslander,' ;
        x += 'CAPOtherPacificIslander,APRaceMethod,CAPRaceMethod,APGenderMethod,CAPGenderMethod,DENIALR4,DenialOtherDescription,ConstructionMethod,ApplicationChannel,ApplicationPayable,' ;
        x += 'APCreditProviderOtherDescription,CAPCreditProviderOtherDescription,AUS_TYPE1,AUS_DECISION1,AUS_TYPE2,AUS_DECISION2,AUS_TYPE3,AUS_DECISION3,AUS_TYPE4,AUS_DECISION4,AUS_TYPE5,' ;
        x += 'AUS_DECISION5,AUS_TYPE_OTHER,AUS_DECISION_OTHER,LenderCredits,BalloonIndicator,InterestOnlyIndicator,NegativeAmortizationIndicator,ReverseMortgageIndicator,LOCIndicator,' ;
        x += 'OtherNonAmortizingIndicator,BusinessCommercialIndicator,ManufacturedHomePropertyInterest,ManufacturedHomePropertyType,MFAffordableUnitsCount,UnitsCount,TotalLoanCosts' ;
        
        return x ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Date convert
	 *  -------------------------------------------------------------------------------------------------- */   
    private static String convertDateQuoteComma ( DateTime dt )
    {
        String s = '' ;
        
        if ( dt != null )
            s = convertDate ( dt ) ;
        
        return '"' + s + '",' ;
    }
    
    private static String convertDate ( DateTime dt )
    {
        String s = '' ;
        
        if ( dt != null )
        	s = convertDate ( date.newinstance ( dt.year(), dt.month(), dt.day () ) ) ;
        
        return s ;
    }
    
    private static String convertDate ( Date d )
    {
    	String s = '' ;
        
        if ( d != null )
        {
            String sMonth = String.valueof ( d.month () ) ;
            String sDay = String.valueof ( d.day () ) ;
    
            if ( sMonth.length () == 1 )
                sMonth = '0' + sMonth;
            
            if ( sDay.length () == 1 )
                sDay = '0' + sDay;
           
	        s = String.valueof ( d.year () ) + sMonth + sDay ;
        }
        
        return s ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Everything else
	 *  -------------------------------------------------------------------------------------------------- */   
    private static String nullToBlankQuoteComma ( Object o )
	{
        String xs = '' ;
        
        if ( o != null )
        {
            if ( o instanceof String )
            {
                String s = (String) o ;
                
                if ( String.isNotBlank ( s ) )
                {
                    xs =  '"' + s + '"' ;
                }
            }
            else
            {
            	xs = String.valueOf ( o ) ;
            }
        }
        
        return xs + ',' ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
	 *  Multi-Picklist
	 *  -------------------------------------------------------------------------------------------------- */   
    private static String convertMultiPicklist ( String p1 )
    {
        return convertMultiPicklist ( p1, null, null ) ;
    }
    
    private static String convertMultiPicklist ( String p1, String p2 )
    {
        return convertMultiPicklist ( p1, p2, null ) ;
    }
    
    private static String convertMultiPicklist ( String p1, String p2, String p3 )
    {
        String mp = '' ;
        
        if ( String.isNotBlank ( p1 ) )
        	mp += p1.replaceAll ( ';', '|' ) ;
        
        if ( String.isNotBlank ( p2 ) )
        	mp += p2.replaceAll ( ';', '|' ) ;
        
        if ( String.isNotBlank ( p2 ) )
        	mp += p2.replaceAll ( ';', '|' ) ;
        
        return mp + ',' ;
    }
}