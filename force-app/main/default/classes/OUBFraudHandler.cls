public class OUBFraudHandler 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Settings
     *  -------------------------------------------------------------------------------------------------- */
    private List<OUB_Velocity_Check__mdt> oubVCL ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Application Variables
     *  -------------------------------------------------------------------------------------------------- */
    private one_application__c app ;
    private Integer applicantCoApplicant ;

    /*  --------------------------------------------------------------------------------------------------
     *  Record Types
     *  -------------------------------------------------------------------------------------------------- */
    private String unityVisaRT ;
    private String depositsRT ;
    private String recordTypeName ;
    
    /* -----------------------------------------------------------------------------------
	 * Codes to strings
	 * ----------------------------------------------------------------------------------- */
    Map<String,String> decisionCodeMap ;
    
    /* -----------------------------------------------------------------------------------
	 * Whitelists
	 * ----------------------------------------------------------------------------------- */
    map<String,boolean> ipWhitelistMap ;
    
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public OUBFraudHandler ()
    {
        /* -----------------------------------------------------------------------------------
         * creating the decision code map
         * ----------------------------------------------------------------------------------- */
        List<FIS_Decision_Code_Mapping__mdt> decisionCodeMapping = [SELECT Decision_Message__c,DeveloperName FROM FIS_Decision_Code_Mapping__mdt ];
        decisionCodeMap = new Map<String,String>();
        for(FIS_Decision_Code_Mapping__mdt dcm : decisionCodeMapping){
            decisionCodeMap.put(dcm.DeveloperName, dcm.Decision_Message__c);
        }
        
        //  Record Types!
		List<String> rtNames = new List<String> () ;
		rtNames.add ( 'UNITY Visa' ) ;
        rtNames.add ( 'Deposits' ) ;
        
		List<RecordType> rtL = OneUnitedUtilities.getRecordTypeList ( rtNames ) ;
        
        //  Record Type IDs
        unityVisaRT = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'UNITY Visa', 'one_Application__c' ) ;
        depositsRT = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Deposits', 'one_Application__c' ) ;
    }
        
    /*  --------------------------------------------------------------------------------------------------
     *  Application object - MUST HAVE AN ID!
     *  -------------------------------------------------------------------------------------------------- */
    public void setApplication ( one_Application__c application )
    {
        app = application ;
        
        recordTypeName = 'UNITY Visa' ;
        
        if ( app.RecordTypeId == depositsRT )
            recordTypeName = 'Deposits' ;
        
        //  Metadata #1
        try
        {
            oubVCL = [
                SELECT Type__c, Decision_Code__c, Field__c, Exceeds_Count__c, Days__c
                FROM OUB_Velocity_Check__mdt
                WHERE Record_Type__c = :recordTypeName
                AND Active__c = true
                ORDER BY Exceeds_Count__c DESC
            ] ;
        }
        catch ( Exception e)
        {
            oubVCL = null ;
        }
        
        //  Metadata #2
        ipWhitelistMap = new map<String,boolean> () ;
        
        list<OUB_Velocity_Check_Whitelist__mdt> ovcwL = null ;
        
        try
        {
            ovcwL = [
                SELECT ID, Type__c, Value__c 
                FROM OUB_Velocity_Check_Whitelist__mdt
                WHERE type__c = 'Visitor IP'
                AND Active__c = true
            ] ;
        }
        catch ( Exception e )
        {
            ovcwL = null ;
        }
        
        if ( ( ovcwL != null ) && ( ovcwL.size () > 0 ) )
        {
            for ( OUB_Velocity_Check_Whitelist__mdt ovcw : ovcwL )
                ipWhitelistMap.put ( ovcw.Value__c, true ) ;
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Return application with updates
     *  -------------------------------------------------------------------------------------------------- */
    public one_application__c getApplication ()
    {
        return app ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Master
     *  -------------------------------------------------------------------------------------------------- */
    public String checkFraud ()
    {
        String finalFraud = 'PASS' ;
        
        String hazFraud1 = checkOUBTag () ;
        System.debug ( 'Fraud 1 : ' + hazFraud1 ) ;
        
        if ( ! hazFraud1.equalsIgnoreCase ( 'FAIL' ) )
        {
            String hazFraud2 = checkVelocityThreshold () ;
	        System.debug ( 'Fraud 2 : ' + hazFraud2 ) ;
            
            if ( hazFraud2.equalsIgnoreCase ( 'FAIL' ) )
	            finalFraud = 'FAIL' ;
            else if ( hazFraud1.equalsIgnoreCase ( 'WARN' ) || hazFraud2.equalsIgnoreCase ( 'WARN' ) )
                finalFraud = 'WARN' ;
            else
                finalFraud = 'PASS' ;
        }
        else
        {
            finalFraud = 'FAIL' ;
        }
        
        if ( Test.isRunningTest () )
        {
            if ( ( String.isNotBlank ( app.Email_Address__c ) ) && ( app.Email_Address__c.equalsIgnoreCase ( 'jimoubfail@fail.com' ) ) )
                  finalFraud = 'FAIL' ;
        }
        
        System.debug ( 'Fraud F : ' + finalFraud ) ;
        return finalFraud ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Velocity Threshold
     *  -------------------------------------------------------------------------------------------------- */
    private String checkVelocityThreshold ()
    {
        String status = 'PASS' ;
        app.OUB_Fraud_Disposition__c = 'PASS' ;
        
        // ------ IP Check -----
        list<one_application__c> appL = null ;
        
        if ( ( oubVCL != null ) && ( oubVCL.size () > 0 ) )
        {
            try
            {
                for ( OUB_Velocity_Check__mdt ovc : oubVCL )
                {
                    System.debug ( 'Field ::' + ovc.Field__c + ' Days + ' + ovc.Days__c ) ;
                    
                    if ( ovc.Field__c.equalsIgnoreCase ( 'Visitor IP' ) )
                    {
                        if ( ovc.Days__c == 0 )
                        {
                            appL = [
                                SELECT ID, Name, Visitor_IP__c
                                FROM one_application__c
                                WHERE Visitor_IP__c = :app.Visitor_IP__c
                                AND RecordTypeID = :app.RecordTypeId
                                AND IsDeleted = false
                            ] ;
                        }
                        else
                        {
                            Datetime startDate = getLimiter ( Integer.valueOf ( ovc.Days__c ) ) ;
                            
                            appL = [
                                SELECT ID, Name, Visitor_IP__c
                                FROM one_application__c
                                WHERE Visitor_IP__c = :app.Visitor_IP__c
                                AND IsDeleted = false
                                AND RecordTypeID = :app.RecordTypeId
                                AND CreatedDate > :startDate
                            ] ;
                        }
                    }
                    else if ( ovc.Field__c.equalsIgnoreCase ( 'ACH Funding' ) )
                    {
                        if ( ( String.isNotBlank ( app.Funding_Options__c ) ) && ( app.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) ) 
                        {
                            if ( ovc.Days__c == 0 )
                            {
                                appL = [
                                    SELECT ID, Name, Funding_Account_Number__c, Funding_Routing_Number__c
                                    FROM one_application__c
                                    WHERE Funding_Account_Number__c = :app.Funding_Account_Number__c
                                    AND Funding_Routing_Number__c = :app.Funding_Routing_Number__c
                                    AND Funding_Options__c = 'E- Check (ACH)'
                                    AND IsDeleted = false
                                    AND RecordTypeID = :app.RecordTypeId
                                ] ;
                            }
                            else
                            {
                                Datetime startDate = getLimiter ( Integer.valueOf ( ovc.Days__c ) ) ;
                                
                                appL = [
                                    SELECT ID, Name, Funding_Account_Number__c, Funding_Routing_Number__c
                                    FROM one_application__c
                                    WHERE Funding_Account_Number__c = :app.Funding_Account_Number__c
                                    AND Funding_Routing_Number__c = :app.Funding_Routing_Number__c
                                    AND Funding_Options__c = 'E- Check (ACH)'
                                    AND RecordTypeID = :app.RecordTypeId
                                    AND IsDeleted = false
                                    AND CreatedDate > :startDate
                                ] ;
                            }
                        }
                    }
                    else if ( ovc.Field__c.equalsIgnoreCase ( 'Email' ) )
                    {
                        if ( ovc.Days__c == 0 )
                        {
                            appL = [
                                SELECT ID, Name, Email_Address__c
                                FROM one_application__c
                                WHERE Email_Address__c = :app.Email_Address__c
                                AND RecordTypeID = :app.RecordTypeId
                                AND IsDeleted = false
                            ] ;
                        }
                        else
                        {
                            Datetime startDate = getLimiter ( Integer.valueOf ( ovc.Days__c ) ) ;
                            
                            appL = [
                                SELECT ID, Name, Email_Address__c
                                FROM one_application__c
                                WHERE Email_Address__c = :app.Email_Address__c
                                AND IsDeleted = false
                                AND RecordTypeID = :app.RecordTypeId
                                AND CreatedDate > :startDate
                            ] ;
                        }
                    }
                    else if ( ovc.Field__c.equalsIgnoreCase ( 'Street Address' ) )
                    {
                        String jimThing = one_Utils.applicationAddressMatcher ( app ) ;
                        
                        if ( ovc.Days__c == 0 )
                        {
                            appL = [
                                SELECT ID, Name, Address_Match__c
                                FROM one_application__c
                                WHERE Address_Match__c = :jimThing
                                AND RecordTypeID = :app.RecordTypeId
                                AND IsDeleted = false
                            ] ;
                        }
                        else
                        {
                            Datetime startDate = getLimiter ( Integer.valueOf ( ovc.Days__c ) ) ;
                            
                            appL = [
                                SELECT ID, Name, Address_Match__c
                                FROM one_application__c
                                WHERE Address_Match__c = :jimThing
                                AND IsDeleted = false
                                AND RecordTypeID = :app.RecordTypeId
                                AND CreatedDate > :startDate
                            ] ;
                        }
                    }

                    if ( ( appL != null ) && ( appL.size () > 0 ) )
                    {
	                    System.debug ( 'Return Size  ::' + appL.size () ) ;
                        
                        if ( appL.size () > ovc.Exceeds_Count__c )
                        {
                            app.FIS_Decision_Code__c = ovc.Decision_Code__c ;
			                app.FIS_Decision__c = decisionCodeMap.get ( app.FIS_Decision_Code__c ) ;
                            
                            if ( ( ovc.Type__c.equalsIgnoreCase ( 'Failure' ) ) && ( ! isWhitelisted () ) )
                            {
                                app.OUB_Fraud_Disposition__c = 'FAIL' ;
                                app.OUB_Fraud_Disposition_Reason__c = 'Failure Velocity ' + ovc.Field__c + ' check.' ;
                                status = 'FAIL' ;
                                return status ;
                            }
                            else if ( ( ovc.Type__c.equalsIgnoreCase ( 'Warning' ) ) && ( ! isWhitelisted () ) )
                            {
                                app.OUB_Fraud_Disposition__c = 'WARN' ;
                                app.OUB_Fraud_Disposition_Reason__c = 'Warning Velocity ' + ovc.Field__c + ' check.' ;
                                status = 'WARN' ;
                                return status ;
                            }
                            
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                // ?
            }
        }
        
        return status ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Velocity Threshold
     *  -------------------------------------------------------------------------------------------------- */
    private String checkOUBTag ()
    {
        String status = 'PASS' ;
        app.OUB_Fraud_Disposition__c = 'PASS' ;
        
        // ------ IP Check -----
        list<Application_Tag__c> atL = null ;
     
        try
        {
            if ( ( String.isNotBlank ( app.Funding_Options__c ) ) && ( app.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) ) 
            {
                atL = [
                    SELECT ID, Name, Application_IP_Address__c, Tag_Type__c,
                    Application_Funding_Account__c, Application_Routing_Number__c,
                    Application_SSN__c, Application_Co_SSN__c,
                    Application_Email_Address__c
                    FROM Application_Tag__c
                    WHERE 
                    ( Application_IP_Address__c = :app.Visitor_IP__c
                    AND Tag_Type__c = 'Block IP Address'
                    AND Application_IP_Address__c != NULL )
                    OR
                    ( Application_Funding_Account__c = :app.Funding_Account_Number__c 
                    AND Application_Routing_Number__c = :app.Funding_Routing_Number__c 
                    AND Tag_Type__c = 'Block Funding Account'
                    AND Application_Funding_Account__c != NULL
                    AND Application_Routing_Number__c != NULL )
                    OR
                    ( Application_Email_Address__c = :app.Email_Address__c
                    AND Tag_Type__c = 'Block Email'
                    AND Application_Email_Address__c != NULL )
                    OR
                    ( Application_SSN__c = :app.SSN__c
                    AND Tag_Type__c = 'Block Tax ID' 
                    AND Application_SSN__c != NULL )
                    OR
                    ( Application_SSN__c = :app.Co_Applicant_SSN__c
                    AND Tag_Type__c = 'Block Tax ID' 
                    AND Application_Co_SSN__c != NULL )
                    OR
                    (
                        Application_Co_SSN__c != NULL
                        AND
                        (
                            ( Application_Co_SSN__c = :app.SSN__c
                            AND Tag_Type__c = 'Block Tax ID' 
                            AND Application_SSN__c != NULL )
                            OR
                            ( Application_Co_SSN__c = :app.Co_Applicant_SSN__c
                            AND Tag_Type__c = 'Block Tax ID' 
                            AND Application_Co_SSN__c != NULL )
                        )
                    )
                ] ;
            }
            else
            {
                atL = [
                    SELECT ID, Name, Application_IP_Address__c, Tag_Type__c,
                    Application_Funding_Account__c, Application_Routing_Number__c,
                    Application_SSN__c, Application_Co_SSN__c,
                    Application_Email_Address__c
                    FROM Application_Tag__c
                    WHERE 
                    ( Application_IP_Address__c = :app.Visitor_IP__c
                    AND Tag_Type__c = 'Block IP Address'
                    AND Application_IP_Address__c != NULL )
                    OR
                    ( Application_Email_Address__c = :app.Email_Address__c
                    AND Tag_Type__c = 'Block Email'
                    AND Application_Email_Address__c != NULL )
                    OR
                    ( Application_SSN__c = :app.SSN__c
                    AND Tag_Type__c = 'Block Tax ID' 
                    AND Application_SSN__c != NULL )
                    OR
                    ( Application_SSN__c = :app.Co_Applicant_SSN__c
                    AND Tag_Type__c = 'Block Tax ID' 
                    AND Application_Co_SSN__c != NULL )
                    OR
                    (
                        Application_Co_SSN__c != NULL
                        AND
                        (
                            ( Application_Co_SSN__c = :app.SSN__c
                            AND Tag_Type__c = 'Block Tax ID' 
                            AND Application_SSN__c != NULL )
                            OR
                            ( Application_Co_SSN__c = :app.Co_Applicant_SSN__c
                            AND Tag_Type__c = 'Block Tax ID' 
                            AND Application_Co_SSN__c != NULL )
                        )
                    )
                ] ;
            }
        }
        catch ( Exception e )
        {
            // ?
        }
        
        if ( ( atL != null ) && ( atL.size () > 0 ) )
        {
            boolean hazBlock = false ;
            String blockReason = '' ;
            
            //  Double check, just to be safe
            for ( Application_Tag__c at : atL )
            {
                if ( ( at.Tag_Type__c.equalsIgnoreCase ( 'Block Email' ) ) &&
                     ( String.isNotBlank ( at.Application_Email_Address__c ) ) )
                {
                 	blockReason = 'Application block TAG for ' + at.Tag_Type__c + ' :: (' + at.Name + ') ' ;
                    hazBlock = true ;
                }
                
                else if ( ( at.Tag_Type__c.equalsIgnoreCase ( 'Block IP Address' ) ) &&
                          ( String.isNotBlank ( at.Application_IP_Address__c ) ) )
                {
                 	blockReason = 'Application block TAG for ' + at.Tag_Type__c + ' :: (' + at.Name + ') ' ;
                    hazBlock = true ;
                }
                
                else if ( ( at.Tag_Type__c.equalsIgnoreCase ( 'Block Funding Account' ) ) &&
                          ( String.isNotBlank ( at.Application_Funding_Account__c ) ) &&
                          ( String.isNotBlank ( at.Application_Routing_Number__c ) ) )
                {
                 	blockReason = 'Application block TAG for ' + at.Tag_Type__c + ' :: (' + at.Name + ') ' ;
                    hazBlock = true ;
                }
                
                else if ( ( at.Tag_Type__c.equalsIgnoreCase ( 'Block Tax ID' ) ) &&
                          ( String.isNotBlank ( at.Application_SSN__c ) ) )
                {
                 	blockReason = 'Application block TAG for ' + at.Tag_Type__c + ' :: (' + at.Name + ') ' ;
                    hazBlock = true ;
                }
                
                else if ( ( at.Tag_Type__c.equalsIgnoreCase ( 'Block Tax ID' ) ) &&
                          ( String.isNotBlank ( at.Application_Co_SSN__c ) ) )
                {
                 	blockReason = 'Application block TAG for ' + at.Tag_Type__c + ' :: (' + at.Name + ') ' ;
                    hazBlock = true ;
                }
            }
            
            if ( hazBlock == true )
            {
                app.FIS_Decision_Code__c = 'D504' ;
                app.FIS_Decision__c = decisionCodeMap.get ( app.FIS_Decision_Code__c ) ;
                app.OUB_Fraud_Disposition__c = 'FAIL' ;
                    
                if ( String.isNotBlank ( blockReason ) )
                    app.OUB_Fraud_Disposition_Reason__c = blockReason ;
                else
	                app.OUB_Fraud_Disposition_Reason__c = 'Application block TAG for IP/Funding/Email' ;
                
                status = 'FAIL' ;
            }
        }
        
        return status ;
    }

    /*  --------------------------------------------------------------------------------------------------
     *  Stupid salesforce date math
     *  -------------------------------------------------------------------------------------------------- */
    private Datetime getLimiter ( Integer days )
    {
        Datetime dt = Datetime.now().addDays ( -days ) ;
        Datetime dt2 = Datetime.newInstanceGMT ( dt.yearGMT(), dt.monthGMT(), dt.dayGMT(), 24, 00, 00 ) ;
        
        return dt2 ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Whitelist
     *  -------------------------------------------------------------------------------------------------- */
    private boolean isWhitelisted ()
    {
        boolean whitelist = false ;
        
        if ( ipWhitelistMap.containsKey ( app.Visitor_IP__c ) )
            whitelist = true ;
        
        return whitelist ;
    }
}