public class IP_Risk_Helper {

	private one_application__c app {get;set;}
    private Application_Tag__c apptag { get;set; }
    private String cardData {get;set;}
    
    private String endpoint {get;set;}
    private String chargebackEndpoint {get;set;}
	private String urlStart {get;set;}
	private String clientCert {get;set;}
	private String appId {get;set;}
	private String apiKey {get;set;}
	
	public IP_Risk_Helper ( one_application__c a, String cardData )
    {
		this.app = a;
        this.appTag = null ;
        
        if ( String.isNotBlank ( cardData ) )
        {
            if ( cardData.length () > 6 )
                this.cardData = cardData.left ( 6 ) ;
			else
	            this.cardData = cardData ;
        }
        
        loadSettings () ;
	}
    
	public IP_Risk_Helper(one_application__c a)
    {
		this.app = a;
        this.appTag = null ;
        this.cardData = null ;
        
        loadSettings () ;
	}
    
	public IP_Risk_Helper(Application_Tag__c at)
    {
		this.app = null;
        this.appTag = at ;
        this.cardData = null ;
        
        loadSettings () ;
	}
    
    private void loadSettings ()
    {
		IP_Risk_Setting__mdt settings = [SELECT Api_ID__c,Api_Key__c,Endpoint__c,Chargeback_Endpoint__c FROM IP_Risk_Setting__mdt WHERE DeveloperName = 'IP_Risk_Settings' LIMIT 1];
		this.endpoint = settings.Endpoint__c;
        this.chargebackEndpoint = settings.Chargeback_Endpoint__c ;
		this.appId = settings.Api_ID__c;
        this.apiKey = settings.Api_Key__c;
    }
    
    public void getRiskScore(){
        try{
            String method = 'POST';
            
            String body = getRequestBody();
            
            HTTPResponse response = sendRequest(method,body);
            
            if(response.getStatusCode()==200){
                
                //success
                setResponseValuesRiskScore(response);
                System.debug(response.getBody());
            }else{
                this.app.minFraud_Warning_Code__c = response.getStatus();
                this.app.minFraud_Warning__c = 'Error Calling minFraud Service: ' + response.getBody();
                throw new applicationException('Error Calling minFraud Service- Status: ' + response.getStatus() + '|' + response.getStatusCode() +'. Message: '+ response.getBody());	
            }
        }catch(applicationException ae){
            System.debug(ae.getMessage() + ' - line: ' + ae.getLineNumber());
        }catch(Exception e){
            this.app.minFraud_Warning_Code__c = '-1';
            this.app.minFraud_Warning__c = 'Error in the controller: ' + e.getMessage() + ' - line: ' + e.getLineNumber();
        }
        
	}
    
    private String getRequestBody() {
        String coAppPreFix = '';
        String phone = '' ;
        
        //  Set up the generator
  		JSONGenerator bodyGenerator = JSON.createGenerator ( true ) ;

  		//  Create the object
  		bodyGenerator.writeStartObject();

        
		//  Start device 
  		bodyGenerator.writeFieldName ( 'device' ) ;
  		bodyGenerator.writeStartObject();

  		//  Device data
		bodyGenerator.writeStringField ( 'ip_address', getApplicationField( coAppPreFix , 'Visitor_IP__c') ) ;
		
        if (String.isNotEmpty(getApplicationField( coAppPreFix , 'Visitor_User_Agent__c'))) {
            bodyGenerator.writeStringField ( 'user_agent', getApplicationField( coAppPreFix , 'Visitor_User_Agent__c') ) ;
        }
		if (String.isNotEmpty(getApplicationField( coAppPreFix , 'Visitor_Accept_Language__c'))) {
			bodyGenerator.writeStringField ( 'accept_language', getApplicationField( coAppPreFix , 'Visitor_Accept_Language__c') ) ;
		}
		
  		//End device
  		bodyGenerator.writeEndObject();

        
		//  Start event 
  		bodyGenerator.writeFieldName ( 'event' ) ;
  		bodyGenerator.writeStartObject();

  		//  Event data
		bodyGenerator.writeStringField ( 'transaction_id', getApplicationField( coAppPreFix , 'Application_ID__c') ) ;
		bodyGenerator.writeStringField ( 'shop_id', 'Salesforce' ) ;
		bodyGenerator.writeStringField ( 'type', 'purchase' ) ;
		
  		//End event
  		bodyGenerator.writeEndObject();

        if (String.isNotEmpty(getApplicationField( coAppPreFix , 'Email_Address__c'))) {
            //  Start Email  
            bodyGenerator.writeFieldName ( 'email' ) ;
            bodyGenerator.writeStartObject();
    
            //  Email  data
            bodyGenerator.writeStringField ( 'domain', getApplicationField( coAppPreFix , 'Email_Address__c').split('@').get(1) ) ;

            //End Email 
            bodyGenerator.writeEndObject();
        }
        
        System.debug ( 'IP Risk #1 : ' + app.Funding_Options__c ) ;
        System.debug ( 'IP Risk #2 : ' + app.Billing_Address__c ) ;        
        
		//  Start billing 
  		bodyGenerator.writeFieldName ( 'billing' ) ;
  		bodyGenerator.writeStartObject();

  		//  billing data
  		//  
  		//  **********
  		//  NOTE: We need to update this section to use the credit card billing address
  		//        Rather than the primary address in cases where they applicant is using
  		//        Debit Card Funding
  		//  **********
  		if ( String.isNotBlank ( app.Billing_Address__c ) )
        {
            if ( ( app.Funding_Options__c.equalsIgnoreCase ( 'Debit Card' ) ) && ( app.Billing_Address__c.equalsIgnoreCase ( 'Other' ) ) )
            {
                System.debug ( 'IP Risk : Debit Card - Other' ) ;
                bodyGenerator.writeStringField ( 'address', getApplicationField( coAppPreFix , 'Billing_Street_Address__c') ) ;
                if ( String.isNotBlank ( getApplicationField ( coAppPreFix , 'Billing_Street_Address_2__c') ) )
	                bodyGenerator.writeStringField ( 'address_2', getApplicationField( coAppPreFix , 'Billing_Street_Address_2__c') ) ;
                bodyGenerator.writeStringField ( 'city', getApplicationField( coAppPreFix , 'Billing_City__c') ) ;
                bodyGenerator.writeStringField ( 'region', getApplicationField( coAppPreFix , 'Billing_State__c') ) ;
                bodyGenerator.writeStringField ( 'country', 'US') ;
                bodyGenerator.writeStringField ( 'postal', getApplicationField( coAppPreFix , 'Billing_Zip_Code__c') ) ;
            }
            else if ( ( app.Funding_Options__c.equalsIgnoreCase ( 'Debit Card' ) ) && ( app.Billing_Address__c.equalsIgnoreCase ( 'Same as Mailing Address' ) ) )
            {
                System.debug ( 'IP Risk : Debit Card - Mailing' ) ;
                bodyGenerator.writeStringField ( 'address', getApplicationField( coAppPreFix , 'Mailing_Street_Address__c') ) ;
                if ( String.isNotBlank ( getApplicationField ( coAppPreFix , 'Mailing_Street_Address_2__c') ) )
	                bodyGenerator.writeStringField ( 'address_2', getApplicationField( coAppPreFix , 'Mailing_Street_Address_2__c') ) ;
                bodyGenerator.writeStringField ( 'city', getApplicationField( coAppPreFix , 'Mailing_City__c') ) ;
                bodyGenerator.writeStringField ( 'region', getApplicationField( coAppPreFix , 'Mailing_State__c') ) ;
                bodyGenerator.writeStringField ( 'country', 'US') ;
                bodyGenerator.writeStringField ( 'postal', getApplicationField( coAppPreFix , 'Mailing_Zip_Code__c') ) ;
            }
            else
            {
                System.debug ( 'IP Risk : EVERYTHING ELSE - Billing address' ) ;
                bodyGenerator.writeStringField ( 'address', getApplicationField( coAppPreFix , 'Street_Address__c') ) ;
                if ( String.isNotBlank ( getApplicationField ( coAppPreFix , 'Street_Address_2__c') ) )
	                bodyGenerator.writeStringField ( 'address_2', getApplicationField( coAppPreFix , 'Street_Address_2__c') ) ;
                bodyGenerator.writeStringField ( 'city', getApplicationField( coAppPreFix , 'City__c') ) ;
                bodyGenerator.writeStringField ( 'region', getApplicationField( coAppPreFix , 'State__c') ) ;
                bodyGenerator.writeStringField ( 'country', 'US') ;
                bodyGenerator.writeStringField ( 'postal', getApplicationField( coAppPreFix , 'Zip_Code__c') ) ;
            }
        }
        else
        {
            System.debug ( 'IP Risk : EVERYTHING ELSE' ) ;
            bodyGenerator.writeStringField ( 'address', getApplicationField( coAppPreFix , 'Street_Address__c') ) ;
            if ( String.isNotBlank ( getApplicationField ( coAppPreFix , 'Street_Address_2__c') ) )
                bodyGenerator.writeStringField ( 'address_2', getApplicationField( coAppPreFix , 'Street_Address_2__c') ) ;
            bodyGenerator.writeStringField ( 'city', getApplicationField( coAppPreFix , 'City__c') ) ;
            bodyGenerator.writeStringField ( 'region', getApplicationField( coAppPreFix , 'State__c') ) ;
            bodyGenerator.writeStringField ( 'country', 'US') ;
            bodyGenerator.writeStringField ( 'postal', getApplicationField( coAppPreFix , 'Zip_Code__c') ) ;
        }
        
        
        // Only one phone
        phone = getApplicationField( coAppPreFix , 'Phone__c') ;
        if ( String.isNotBlank ( phone ) )
        {
            // ? phone
            phone = phone.replace ( '-', '' ) ;
            phone = phone.replace ( '(', '' ) ;
            phone = phone.replace ( ')', '' ) ;
            phone = phone.replace ( ' ', '' ) ;
            
            if ( phone.length () >= 10 )
            {
                phone = phone.mid ( 0, 3 ) + '-' + phone.mid ( 3, 3 ) + '-' + phone.mid ( 6, 4 ) ;
            
                bodyGenerator.writeStringField ( 'phone_number', phone ) ;
                bodyGenerator.writeStringField ( 'phone_country_code', '1' ) ;
            }
        }
        
        //End billing
  		bodyGenerator.writeEndObject();

		//  Start shipping 
  		bodyGenerator.writeFieldName ( 'shipping' ) ;
  		bodyGenerator.writeStartObject();
		
  		//  shipping data
		if (String.isNotEmpty(getApplicationField( coAppPreFix , 'Mailing_Street_Address__c'))) {
            bodyGenerator.writeStringField ( 'address', getApplicationField( coAppPreFix , 'Mailing_Street_Address__c') ) ;
            if ( String.isNotBlank ( getApplicationField ( coAppPreFix , 'Mailing_Street_Address_2__c') ) )
                bodyGenerator.writeStringField ( 'address_2', getApplicationField( coAppPreFix , 'Mailing_Street_Address_2__c') ) ;
            bodyGenerator.writeStringField ( 'city', getApplicationField( coAppPreFix , 'Mailing_City__c') ) ;
            bodyGenerator.writeStringField ( 'region', getApplicationField( coAppPreFix , 'Mailing_State__c') ) ;
			bodyGenerator.writeStringField ( 'country', 'US') ;
            bodyGenerator.writeStringField ( 'postal', getApplicationField( coAppPreFix , 'Mailing_Zip_Code__c') ) ;
        } else {
            bodyGenerator.writeStringField ( 'address', getApplicationField( coAppPreFix , 'Street_Address__c') ) ;
            if ( String.isNotBlank ( getApplicationField ( coAppPreFix , 'Street_Address_2__c') ) )
                bodyGenerator.writeStringField ( 'address_2', getApplicationField( coAppPreFix , 'Street_Address_2__c') ) ;
            bodyGenerator.writeStringField ( 'city', getApplicationField( coAppPreFix , 'City__c') ) ;
            bodyGenerator.writeStringField ( 'region', getApplicationField( coAppPreFix , 'State__c') ) ;
			bodyGenerator.writeStringField ( 'country', 'US') ;
            bodyGenerator.writeStringField ( 'postal', getApplicationField( coAppPreFix , 'Zip_Code__c') ) ;
        }

        // Only one phone
        if ( String.isNotBlank ( phone ) )
        {
            bodyGenerator.writeStringField ( 'phone_number', phone ) ;
            bodyGenerator.writeStringField ( 'phone_country_code', '1' ) ;
        }
        
        //End shipping
  		bodyGenerator.writeEndObject();

		//  Start order 
  		bodyGenerator.writeFieldName ( 'order' ) ;
  		bodyGenerator.writeStartObject();

  		//  order data
		bodyGenerator.writeStringField ( 'amount', getApplicationField( coAppPreFix , 'RequestedCreditLimit__c') ) ;
		bodyGenerator.writeStringField ( 'currency', 'USD') ;
		bodyGenerator.writeStringField ( 'affiliate_id', getApplicationField( coAppPreFix , 'Reservation_Vendor_ID__c') ) ;
		bodyGenerator.writeStringField ( 'subaffiliate_id', getApplicationField( coAppPreFix , 'Reservation_Affiliate_Vendor__c') ) ;
		
        //End order
  		bodyGenerator.writeEndObject();
        
        //  
  		//  **********
  		//  NOTE: Post discussion with legal, consider adding:
  		//  
  		//        Payment Node
  		//        Credit Card Node
  		//        
  		//        For now, we will start without
  		//  **********
  		//
  		if ( String.isNotBlank ( cardData ) )
        {
            bodyGenerator.writeFieldName ( 'credit_card' ) ;
            bodyGenerator.writeStartObject();
			bodyGenerator.writeStringField ( 'issuer_id_number', cardData ) ;
            bodyGenerator.writeEndObject();
        }
        
        
  		//End of object
  		bodyGenerator.writeEndObject();

        System.debug ( '--------------------------------- JSON -----------------------------------' ) ;
        System.debug ( bodyGenerator.getAsString() ) ;
        System.debug ( '--------------------------------- JSON -----------------------------------' ) ;
        
        return bodyGenerator.getAsString().replaceAll('\r\n','').replaceAll('\n','');
        
    }
    
	private HTTPResponse sendRequest(String method, String body)
    {
        String contentType = 'application/vnd.maxmind.com-minfraud-insights+json; charset=UTF-8; version=2.0' ;
        return sendRequest ( this.endpoint, method, contentType, body ) ;
    }
    
	private HTTPResponse sendRequest(String endpoint, String method, String contentType, String body)
    {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod(method);
        req.setTimeout(60000);
        req.setHeader ('Content-Type',contentType);
        req.setHeader ('Accept','application/json');
        
        Blob headerValue = Blob.valueOf(this.appId + ':' + this.apiKey);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        
        if(!String.isEmpty(body)){
        	req.setBody(body);
        }

        Http http = new Http();
        
        HTTPResponse res;
        if(Test.isRunningTest()){
            res = new HTTPResponse();
            String exampleBody = '{ "id": "5bc5d6c2-b2c8-40af-87f4-6d61af86b6ae", "risk_score": 0.01, "credits_remaining": 1212, "disposition": {"action": "accept","reason": "custom_rule"}, "ip_address": { "risk": 0.01, "city": { "confidence": 25, "geoname_id": 54321, "names": { "de": "Los Angeles", "en": "Los Angeles", "es": "Los Ángeles", "fr": "Los Angeles", "ja": "ロサンゼルス市", "pt-BR": "Los Angeles", "ru": "Лос-Анджелес", "zh-CN": "洛杉矶" } }, "continent": { "code": "NA", "geoname_id": 123456, "names": { "de": "Nordamerika", "en": "North America", "es": "América del Norte", "fr": "Amérique du Nord", "ja": "北アメリカ", "pt-BR": "América do Norte", "ru": "Северная Америка", "zh-CN": "北美洲" } }, "country": { "confidence": 75, "geoname_id": 6252001, "is_high_risk": true, "iso_code": "US", "names": { "de": "USA", "en": "United States", "es": "Estados Unidos", "fr": "États-Unis", "ja": "アメリカ合衆国", "pt-BR": "Estados Unidos", "ru": "США", "zh-CN": "美国" } }, "location": { "accuracy_radius": 20, "latitude": 37.6293, "local_time": "2015-04-26T01:37:17-08:00", "longitude": -122.1163, "metro_code": 807, "time_zone": "America/Los_Angeles" }, "postal": { "code": "90001", "confidence": 10 }, "registered_country": { "geoname_id": 6252001, "iso_code": "US", "names": { "de": "USA", "en": "United States", "es": "Estados Unidos", "fr": "États-Unis", "ja": "アメリカ合衆国", "pt-BR": "Estados Unidos", "ru": "США", "zh-CN": "美国" } }, "represented_country": { "geoname_id": 6252001, "iso_code": "US", "names": { "de": "USA", "en": "United States", "es": "Estados Unidos", "fr": "États-Unis", "ja": "アメリカ合衆国", "pt-BR": "Estados Unidos", "ru": "США", "zh-CN": "美国" }, "type": "military" }, "subdivisions": [ { "confidence": 50, "geoname_id": 5332921, "iso_code": "CA", "names": { "de": "Kalifornien", "en": "California", "es": "California", "fr": "Californie", "ja": "カリフォルニア", "ru": "Калифорния", "zh-CN": "加州" } } ], "traits": { "autonomous_system_number": 1239, "autonomous_system_organization": "Linkem IR WiMax Network", "domain": "example.com", "is_anonymous_proxy": true, "is_satellite_provider": true, "isp": "Linkem spa", "ip_address": "1.2.3.4", "organization": "Linkem IR WiMax Network", "user_type": "traveler" } }, "credit_card": { "issuer": { "name": "Bank of America", "matches_provided_name": true, "phone_number": "800-732-9194", "matches_provided_phone_number": true }, "brand": "Visa", "country": "US", "is_issued_in_billing_address_country": true, "is_prepaid": true, "type": "credit" }, "device": { "confidence": 99, "id": "7835b099-d385-4e5b-969e-7df26181d73b", "last_seen": "2016-06-08T14:16:38Z" }, "email": { "is_free": false, "is_high_risk": true }, "shipping_address": { "is_high_risk": true, "is_postal_in_city": true, "latitude": 37.632, "longitude": -122.313, "distance_to_ip_location": 15, "distance_to_billing_address": 22, "is_in_ip_country": true }, "billing_address": { "is_postal_in_city": true, "latitude": 37.545, "longitude": -122.421, "distance_to_ip_location": 100, "is_in_ip_country": true }, "warnings": [ { "code": "INPUT_INVALID", "warning": "Encountered value at /shipping/city that does not meet the required constraints", "input_pointer": "/shipping/city" } ] }';

            if ( ( String.isNotBlank ( app.Email_Address__c ) ) && ( app.Email_Address__c.equalsIgnoreCase ( 'jimminfail@fail.com' ) ) )
					exampleBody = '{ "id": "5bc5d6c2-b2c8-40af-87f4-6d61af86b6ae", "risk_score": 0.01, "credits_remaining": 1212, "disposition": {"action": "reject","reason": "custom_rule"}, "ip_address": { "risk": 0.01, "city": { "confidence": 25, "geoname_id": 54321, "names": { "de": "Los Angeles", "en": "Los Angeles", "es": "Los Ángeles", "fr": "Los Angeles", "ja": "ロサンゼルス市", "pt-BR": "Los Angeles", "ru": "Лос-Анджелес", "zh-CN": "洛杉矶" } }, "continent": { "code": "NA", "geoname_id": 123456, "names": { "de": "Nordamerika", "en": "North America", "es": "América del Norte", "fr": "Amérique du Nord", "ja": "北アメリカ", "pt-BR": "América do Norte", "ru": "Северная Америка", "zh-CN": "北美洲" } }, "country": { "confidence": 75, "geoname_id": 6252001, "is_high_risk": true, "iso_code": "US", "names": { "de": "USA", "en": "United States", "es": "Estados Unidos", "fr": "États-Unis", "ja": "アメリカ合衆国", "pt-BR": "Estados Unidos", "ru": "США", "zh-CN": "美国" } }, "location": { "accuracy_radius": 20, "latitude": 37.6293, "local_time": "2015-04-26T01:37:17-08:00", "longitude": -122.1163, "metro_code": 807, "time_zone": "America/Los_Angeles" }, "postal": { "code": "90001", "confidence": 10 }, "registered_country": { "geoname_id": 6252001, "iso_code": "US", "names": { "de": "USA", "en": "United States", "es": "Estados Unidos", "fr": "États-Unis", "ja": "アメリカ合衆国", "pt-BR": "Estados Unidos", "ru": "США", "zh-CN": "美国" } }, "represented_country": { "geoname_id": 6252001, "iso_code": "US", "names": { "de": "USA", "en": "United States", "es": "Estados Unidos", "fr": "États-Unis", "ja": "アメリカ合衆国", "pt-BR": "Estados Unidos", "ru": "США", "zh-CN": "美国" }, "type": "military" }, "subdivisions": [ { "confidence": 50, "geoname_id": 5332921, "iso_code": "CA", "names": { "de": "Kalifornien", "en": "California", "es": "California", "fr": "Californie", "ja": "カリフォルニア", "ru": "Калифорния", "zh-CN": "加州" } } ], "traits": { "autonomous_system_number": 1239, "autonomous_system_organization": "Linkem IR WiMax Network", "domain": "example.com", "is_anonymous_proxy": true, "is_satellite_provider": true, "isp": "Linkem spa", "ip_address": "1.2.3.4", "organization": "Linkem IR WiMax Network", "user_type": "traveler" } }, "credit_card": { "issuer": { "name": "Bank of America", "matches_provided_name": true, "phone_number": "800-732-9194", "matches_provided_phone_number": true }, "brand": "Visa", "country": "US", "is_issued_in_billing_address_country": true, "is_prepaid": true, "type": "credit" }, "device": { "confidence": 99, "id": "7835b099-d385-4e5b-969e-7df26181d73b", "last_seen": "2016-06-08T14:16:38Z" }, "email": { "is_free": false, "is_high_risk": true }, "shipping_address": { "is_high_risk": true, "is_postal_in_city": true, "latitude": 37.632, "longitude": -122.313, "distance_to_ip_location": 15, "distance_to_billing_address": 22, "is_in_ip_country": true }, "billing_address": { "is_postal_in_city": true, "latitude": 37.545, "longitude": -122.421, "distance_to_ip_location": 100, "is_in_ip_country": true }, "warnings": [ { "code": "INPUT_INVALID", "warning": "Encountered value at /shipping/city that does not meet the required constraints", "input_pointer": "/shipping/city" } ] }';                

            res.setBody(exampleBody);
            res.setStatusCode(200);
            res.setStatus('Success');
        }else{
            res = http.send(req);
        }
        
        return res;
    }
    
    public void reportChargeback ()
    {
        try{
            String method = 'POST';
	        String contentType = 'application/json' ;
            
            String body = getChargebackBody();
            
            HTTPResponse response = sendRequest( chargebackEndpoint, method, contentType, body ) ;
            
            if(response.getStatusCode()==204)
            {
             	this.apptag.Sent_to_MinFraud__c = true ;
                System.debug(response.getBody());
            }else{
                this.appTag.minFraud_Warning_Code__c = response.getStatus();
                this.appTag.minFraud_Warning__c = 'Error Calling minFraud Chargeback Service: ' + response.getBody();
                throw new applicationException('Error Calling minFraud Service- Status: ' + response.getStatus() + '|' + response.getStatusCode() +'. Message: '+ response.getBody());	
            }
        }catch(applicationException ae){
            System.debug(ae.getMessage() + ' - line: ' + ae.getLineNumber());
        }catch(Exception e){
            this.appTag.minFraud_Warning_Code__c = '-1';
            this.appTag.minFraud_Warning__c = 'Error in the controller: ' + e.getMessage() + ' - line: ' + e.getLineNumber();
        }
    }
    
    private String getChargebackBody ()
    {
        String coAppPreFix = '';
        
        //  Set up the generator
  		JSONGenerator bodyGenerator = JSON.createGenerator ( true ) ;

  		//  Create the object
  		bodyGenerator.writeStartObject();
        
        //  Required
		bodyGenerator.writeStringField ( 'ip_address', apptag.Application_IP_Address__c ) ;

        //  Optionals
		bodyGenerator.writeStringField ( 'tag', appTag.minFraud_Tag_Type__c ) ;
		bodyGenerator.writeStringField ( 'minfraud_id', appTag.Application_minFraud_ID__c ) ;
		        
  		//End of object
  		bodyGenerator.writeEndObject();

        System.debug ( '--------------------------------- JSON -----------------------------------' ) ;
        System.debug ( bodyGenerator.getAsString() ) ;
        System.debug ( '--------------------------------- JSON -----------------------------------' ) ;
        
        return bodyGenerator.getAsString().replaceAll('\r\n','').replaceAll('\n','');
    }

    private void setResponseValuesRiskScore (HTTPResponse response) {
        
        try{
            Map<String, Object> minFraudResponseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            this.app.minFraud_ID__c=getJSONString(minFraudResponseMap, 'id');
            //system.debug(getJSONString(minFraudResponseMap,'risk_score'));
            this.app.minFraud_Risk_Score__c=getJSONDecimal(minFraudResponseMap,'risk_score');
            
            Map<String, Object> billingAddressMap = (Map<String, Object>)minFraudResponseMap.get('billing_address');
            this.app.minFraud_Billing_Distance_to_IP__c=getJSONMiles(billingAddressMap,'distance_to_ip_location');
            
            Map<String, Object> shippingAddressMap = (Map<String, Object>)minFraudResponseMap.get('shipping_address');
            this.app.minFraud_Shipping_Distance_to_IP__c=getJSONMiles(shippingAddressMap,'distance_to_ip_location');
            this.app.minFraud_Shipping_Distance_to_Billing__c=getJSONMiles(shippingAddressMap,'distance_to_billing_address');
            this.app.minFraud_Shipping_High_Risk__c=getJSONString(shippingAddressMap, 'is_high_risk');
            
            Map<String, Object> dispositionMap = (Map<String, Object>)minFraudResponseMap.get('disposition');
            this.app.minFraud_Disposition_Reason__c=getJSONString(dispositionMap, 'reason');
            this.app.minFraud_Disposition__c=getJSONString(dispositionMap, 'action').toUpperCase();
            
            Map<String, Object> ipAddressMap = (Map<String, Object>)minFraudResponseMap.get('ip_address');
            this.app.minFraud_IP_Risk_Score__c=getJSONDecimal(ipAddressMap,'risk');
            
            if (ipAddressMap != null) {
                Map<String, Object> countryMap = (Map<String, Object>)ipAddressMap.get('country');
                this.app.minFraud_IP_High_Risk_Countr__c=getJSONString(countryMap, 'is_high_risk');
                
                if (countryMap != null) {
                    Map<String, Object> countryNamesMap = (Map<String, Object>)countryMap.get('names');
                    this.app.minFraud_IP_Country__c=getJSONString(countryNamesMap, 'en');
                }
                                
                Map<String, Object> cityMap = (Map<String, Object>)ipAddressMap.get('city');
                if (cityMap != null) {
                    Map<String, Object> cityNamesMap = (Map<String, Object>)cityMap.get('names');
                    this.app.minFraud_IP_City__c=getJSONString(cityNamesMap, 'en');

                }
                List<Object> statesList = (List<Object>)ipAddressMap.get('subdivisions');
                if (statesList != null && statesList[0] != null ){
                    Map<String, Object> statesMap = (Map<String, Object>)statesList[0];
                    if (statesMap != null) {
                        Map<String, Object> statesNamesMap = (Map<String, Object>)statesMap.get('names');
                        this.app.minFraud_IP_State__c=getJSONString(statesNamesMap, 'en');
                    }
                }
                Map<String, Object> postalMap = (Map<String, Object>)ipAddressMap.get('postal');
                if (postalMap != null) {
                    this.app.minFraud_IP_Zip__c=getJSONString(postalMap, 'code');
                }
                
                Map<String, Object> traitsMap = (Map<String, Object>)ipAddressMap.get('traits');
                if (traitsMap != null) {
                    this.app.minFraud_IP_User_Type__c=getJSONString(traitsMap, 'user_type');
                    this.app.minFraud_IP_ISP__c=getJSONString(traitsMap, 'isp');
                    this.app.minFraud_IP_Org__c=getJSONString(traitsMap, 'organization');
                    this.app.minFraud_IP_Legit_Proxy__c=getJSONString(traitsMap, 'is_legitimate_proxy');
                    this.app.minFraud_IP_domain__c=getJSONString(traitsMap, 'domain');
                }
            }
            
        }catch(Exception e){
            this.app.minFraud_Warning_Code__c = '-1';
            this.app.minFraud_Warning__c = 'Error in the controller';
            this.app.minFraud_Warning__c = 'Error parsing details from response. Message: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
            throw new applicationException('Error parsing details from response. Message: ' + e.getMessage() + ' Line: ' + e.getLineNumber());		
        }
    }
    
    private String getJSONString(Map<String, Object> inputMap, String fieldName) {
        if (inputMap != null) {
			Object tempObj = inputMap.get(fieldName);
			if (tempObj != null)
                return String.valueOf(tempObj);
        }
        return '';
    }
    
    private Decimal getJSONDecimal(Map<String, Object> inputMap, String fieldName) {
        String tempStr=getJSONString(inputMap,fieldName);
        if (String.isEmpty(tempStr)) {
			tempStr='0';
        }        	
        return Decimal.valueOf(tempStr);
    }
    
    
    private Decimal getJSONMiles(Map<String, Object> inputMap, String fieldName) {
        return getJSONDecimal(inputMap,fieldName)/1.60934;
    }
    
    
	private String getApplicationField(String coAppPrefix, String fieldName){
		if(app.get(coAppPrefix + fieldName)!=null){
			return String.valueOf( app.get(coAppPrefix + fieldName) );
		}
		return '';
	}
 
	private class applicationException extends Exception {}

}