public with sharing class UNITY_Visa_Heatmap 
{
    //  List of applications
    public List<One_application__c> unityVisaApps
    {
        get ;        
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public UNITY_Visa_Heatmap ( ApexPages.Standardcontroller stdC )
    {
        unityVisaApps = new List<One_application__c> () ;
        
        unityVisaApps = 
        [
            SELECT ID, 
            Location__Latitude__s, Location__Longitude__s, Street_Address__c, City__c, State__c, ZIP_Code__c,
            Co_Applicant_Street_Address__c, Co_Applicant_City__c, Co_Applicant_State__c, Co_Applicant_ZIP_Code__c
            FROM One_application__c
            WHERE Application_Status__c = 'APPROVED'
            ORDER BY Application_Approved_Date__c DESC
            LIMIT 1000
        ] ;
    }
}