@isTest
public with sharing class test_chatter_post 
{
	static testmethod void testOne ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u1 = new User () ;
		u1.firstName = 'Test' ;
		u1.lastName = 'User' ;
		u1.email = 'test1@user.com' ;
		u1.username = 'test1user1@user.com' ;
		u1.alias = 'tuser1' ;
		u1.CommunityNickname = 'tuser1' ;
		u1.TimeZoneSidKey = 'GMT' ;
		u1.LocaleSidKey = 'en_US' ;
		u1.EmailEncodingKey = 'UTF-8' ;
		u1.ProfileId = p.id ;
		u1.LanguageLocaleKey = 'en_US' ;
		u1.Branch__c = 'Crenshaw' ;
		insert u1 ;
		
		CollaborationGroup cg = new CollaborationGroup () ;
		cg.Name = 'Test Group' ;
		cg.CollaborationType = 'Public' ;
		insert cg ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference prcp = Page.chatter_post ;
		Test.setCurrentPage ( prcp ) ;
		
		Test.startTest() ;
		
		//  Create standard controller object from the application
		FeedItem fi = new FeedItem () ;
		ApexPages.StandardController sc = new ApexPages.StandardController ( fi ) ;
		
		//  Feed the standard controller to the page controller
		chatter_post cp = new chatter_post ( sc ) ;
		
		cp.cgId = cg.id ;
		cp.fi.Type = 'TextPost' ;
		cp.fi.ParentId = u1.Id ;
		cp.fi.CreatedById = u1.Id ;
		cp.fi.Title = 'Test Post' ;
		cp.fi.Body = 'Test Body' ;
		
		cp.postFeed () ;
		
		Test.stopTest() ;
	}
}