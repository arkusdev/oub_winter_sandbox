public with sharing class one_application_ob_registration
{
    /*  ----------------------------------------------------------------------------------------
    *  Private stuff
    *  ---------------------------------------------------------------------------------------- */
    private one_settings__c settings ;
    private DIHelper dih = new DIHelper() ;
    private String queryString ;
    
    /*  ----------------------------------------------------------------------------------------
    *  update flags
    *  ---------------------------------------------------------------------------------------- */
    private boolean updateApplication = false ; // i.e. update application obj. in SF
    
    /*  ----------------------------------------------------------------------------------------
    *  Redirects
    *  ---------------------------------------------------------------------------------------- */
    private PageReference pageAction ;
    
    /*  ----------------------------------------------------------------------------------------
    *  Template Name/ETV
    *  ---------------------------------------------------------------------------------------- */
    // template is set directly in .vfp b/c only using one template
  	public String templateName { public get; private set; }
    
    /*  ----------------------------------------------------------------------------------------
    *  Page objects
    *  ---------------------------------------------------------------------------------------- */
    public one_application__c o { public get; private set; }
    public String appField { public get; private set; } // application id
    
    public String loginId { public get; public set; } // user's choice for OB loginId
    public String userPw { public get; public set; } // user's choice for OB pw
    public Integer regAttempts { public get; private set; }
    public Integer verifyAttempts { public get; private set; }
    public Integer verifyIdSetting { public get; private set; }
    
    public String existingLoginId { public get; private set; }
    
    // Variables for customer verification information
    public String last4SSN { public get; public set; }
    //public String mmn { public get; public set; } // mother's maiden name
        
    //  Step control for panels on page
    public Integer vStepNumber { public get; private set; }
    public List<Integer> errorStepNums { public get; private set; }
    public boolean vStep1001 { public get; private set; }
    public boolean vStep1002 { public get; private set; }
    public boolean vStep1003 { public get; private set; }
    public boolean vStep1004 { public get; private set; }
    public boolean vStep1005 { public get; private set; }
    public boolean vStep1006 { public get; private set; }
    
    public boolean ssnError { public get; private set; }
    public boolean ssnMatch { public get; private set; }
    //public boolean mmnError { public get; private set; }
    //public boolean mmnMatch { public get; private set; }
    
    //  Timeout
    public Integer timeout { public get ; private set ; }
    
    //  DisclosureURL
    public String ratesURLDisclosure 
    { 
        get ; 
        private set ; 
    }
    
    //  Live Agent
    public String liveAgentButton
    {
        get ; 
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
    *  Rakuten tracking pixel
    *  ---------------------------------------------------------------------------------------- */
    public String rakutenTrackingPixel
    {
        get ; 
        private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
    *  Security
    *  ---------------------------------------------------------------------------------------- */
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;
    
    /*  ----------------------------------------------------------------------------------------
    *  Main Controller
    *  ---------------------------------------------------------------------------------------- */
    public one_application_ob_registration ( ApexPages.Standardcontroller stdC )
    {
        templateName = 'Deposit_Application_Template_new';
        
        existingLoginId = null;
        
        ssnError = false;
        ssnMatch = true;
       	//mmnError = false;
        //mmnMatch = true;

        //  --------------------------------------------------------------------------------------------------
        //  Default rate disclosure
        //  --------------------------------------------------------------------------------------------------
        ratesURLDisclosure = one_utils.getDisclosureURL () ;
        
        //  --------------------------------------------------------------------------------------------------
        //  Live Agent Button
        //  --------------------------------------------------------------------------------------------------
        if ( ( one_settings__c.getInstance('settings').Live_Agent_Enable__c != null ) &&
            ( one_settings__c.getInstance('settings').Live_Agent_Enable__c == true ) )
        {
            liveAgentButton = one_settings__c.getInstance('settings').Live_Agent_Button_ID__c ;
        }
        else
        {
            liveAgentButton = null ;
        }
        
        regAttempts = 0;
        verifyAttempts = 0;
        verifyIdSetting = 3;
        
        //  Default, go nowhere
        pageAction = null ;
        
        //  Controls which panel we display, default of 6000 (invalid)
        vStepNumber = 6000 ;
        
        //  First check for application in session
        this.o = (one_application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
        String tid = null ;
        
        //  Load Settings
        //  Note: DI Settings are loaded in DI Helper class
        settings = one_settings__c.getInstance ( 'settings' ) ;
        
        //  If no session present, use querystring instead
        if ( ( o == null ) || ( o.Id == null ) )
        {
            System.debug ( '========== No session info found ==========' ) ;
            
            aid = ApexPages.currentPage().getParameters().get('aid') ;  // applicationID
            kid = ApexPages.currentPage().getParameters().get('kid') ;  // the randomly-generated key associated w/ aid
            tid = ApexPages.currentPage().getParameters().get('tid') ;  // parameter indicating whether or not the user came from application page or link in email
            
            //  Reset object
            o = null ;
            
            //  Both need to be passed in
            if ( ( aid != null ) && ( kid != null ) )
            {
                o = one_utils.getApplicationFromId ( aid ) ;
                
                if ( o != null )
                {
                    //  Key must match record on ID passed in
                    if ( ! kid.equals ( o.Upload_Attachment_key__c ) )
                    {
                        System.debug ( '========== Key does not match ==========' ) ;
                        o = null ;
                    }
                }
            }
        }
        else
        {
            System.debug ( '========== Using existing session info ==========' ) ;
            
            aid = o.Id ;
            kid = o.Upload_Attachment_Key__c ;
        }
        
        //  Load Settings
        //  Note: DI Settings are loaded in DI Helper class
        settings = one_settings__c.getInstance ( 'settings' ) ;
        
        //  Check again for things existing
        if ( ( o != null ) && ( aid != null ) && ( kid != null ) && ( settings != null ) )
        {
            queryString = settings.Direct_From_App__c ;
            ApexPages.currentPage().getHeaders().put ( 'CF-Validation', queryString ) ;
            
            System.debug ( '========== Override Rate Disclosure ==========' ) ;
            if ( String.isNotBlank ( o.Product__c ) && String.isNotBlank ( o.SubProduct__c ) && String.isNotBlank ( o.Plan__c ) )
                ratesURLDisclosure = one_utils.getDisclosureURL ( o.Product__c, o.SubProduct__c, o.Plan__c ) ;
            
            System.debug ( '========== Tracking Pixel ==========' ) ;
            
            Account a1 = o.Reservation_Vendor_ID__r ;
            Account a2 = o.Reservation_Affiliate_Vendor__r ;
            
            if ( ( a1 != null ) && ( a1.Tracking_pixel__c != null ) )
            {
                if ( a1.Tracking_Pixel__c )
                {
                    one_referral oneR = new one_referral () ;
                    oneR.loadThirdPartyData ( o.id ) ;
                    rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( o.Name ) ;
                }
            }
            else if ( ( a2 != null ) && ( a2.Tracking_pixel__c != null ) )
            {
                if ( a2.Tracking_Pixel__c )
                {
                    one_referral oneR = new one_referral () ;
                    oneR.loadThirdPartyData ( o.id ) ;
                    rakutenTrackingPixel = oneR.getRakutenTrackingLinkUNITYVisa ( o.Name ) ;
                }
            } 
            
            verifyIdSetting = settings.Verify_ID_Attempts__c.intValue() ;
            
            System.debug ( '========== Valid information found ==========' ) ;
            // System.debug('o.application_status: ' + o.Application_Status__c);
            // System.debug('is not blank: ' + String.isNotBlank(o.Application_Status__c));
            // System.debug('equalsIgnoreCase: ' + o.Application_Status__c.equalsIgnoreCase('APPROVED') + '----- or declined: ' + o.Application_Status__c.equalsIgnoreCase('DECLINED'));
            
            if ( ( String.isNotBlank ( o.Application_Status__c ) ) && ( o.Application_Status__c.equalsIgnoreCase ( 'DECLINED' ) ) )
            {
                System.debug ( '========== DECLINE CHECK ==========' ) ;
                vStepNumber = 6000 ;
            }
            else if ( ( String.isNotBlank ( o.FIS_Decision_Code__c ) ) &&  ( ( o.FIS_Decision_Code__c.startsWithIgnoreCase('A') ) || ( o.FIS_Decision_Code__c.equalsIgnoreCase('P089') ) ) )
            {
                System.debug ( '========== APPROVE/Decision Code P089 check ==========' ) ;
                
                //  Application ID for page
                appField = null ;
                if ( String.isNotBlank ( o.Application_ID__c ) )
                    appField = o.Application_ID__c ;
                
                if ( alreadyRegistered() || o.Applicant_OB_Registered__c != null )
                {
                    System.debug('User is already registered. Display redirect panel.');
                    vStepNumber = 8000;
                }
                
                else if ( ( tid != null ) && ( tid == settings.Direct_From_App__c ) ) // i.e. they came directly from the application
                {
                    System.debug('User came directly from the application page. Bypassing identity verification step.');
                    vStepNumber = 9000;
                }
                
                else if ( o.OB_Reg_Lockout__c != null )
                {
                    System.debug('User is locked out of Online Banking form because of too many ID verification attempts. Display unsuccessful registration panel.');
                    vStepNumber = 6000;
                } 
                
                else
                {
                    vStepNumber = 5000;
                    timeout = 10 * 1000 * 60;
                }
    		}
            
            System.debug ( '========== AT THE END ==========' ) ;
        }
    }
    
    /*  ----------------------------------------------------------------------------------------
    *  Page actions
    *  ---------------------------------------------------------------------------------------- */
    
    public boolean verifyID ()
    {
        boolean verified = false;
        vStepNumber = 5000;
        
        ssnError = false;
        ssnMatch = true;
       	//mmnError = false;
        //mmnMatch = true;
        
        if ( String.isBlank(last4SSN) )
        {
            ssnError = true;
        }
        else 
        {
            if ( ( !last4SSN.isNumeric() ) || ( last4SSN.length() != 4 ) )
            {
                ssnError = true;
            }
            
            else if ( last4SSN != o.SSN__c.right(4) )
            {
                ssnMatch = false;
            }
        }

        /*
        if ( String.isBlank(mmn) || ( !mmn.isAlpha() ) )
        {
            mmnError = true;
        }
        else if ( !mmn.equalsIgnoreCase(o.MothersMaidenName__c) )
        {
            mmnMatch = false;
        }
		*/
        
        if ( !ssnError && ssnMatch ) // && !mmnError && mmnMatch )
        {
            verified = true;
        }

        return verified;
        
    }
    
    public boolean verifyEntries ()
    {
        boolean verifyEntries = false;
        
        errorStepNums = new List<Integer>();
        
        // Run checks
        if ( loginId == null || String.isBlank(loginId) )
        {
            System.debug('loginId is empty');
            errorStepNums.add(1001);
        }
        else 
        {
            loginId = loginId.deleteWhitespace();
            
            if ( dih.loginIdExists(loginId) == true )
            {
                System.debug('loginId already exists.');
                errorStepNums.add(1002);
            }
            if ( dih.loginMeetsReqs(loginId) == false )
            {
                System.debug('loginId does not meet requirements.');
                errorStepNums.add(1003);
            }
            if ( ( loginId == o.SSN__c ) || ( loginId == o.SSN__c.replaceAll('-', '') ) )
            {
                System.debug('loginId cannot equal ssn.');
                errorStepNums.add(1004);
            }
        }
        
        
        if ( String.isBlank(userPw) || userPw == null )
        {
            System.debug('userPw is empty.');
            errorStepNums.add(1005);
        }
        else
        {
            userPw = userPw.trim();
            
            if ( !dih.pwMeetsReqs(userPw) )
            {
                System.debug('userPw does not meet requirements.');
                errorStepNums.add(1006);
            }
        }
        
        vStep1001 = errorStepNums.contains(1001);
        vStep1002 = errorStepNums.contains(1002);
        vStep1003 = errorStepNums.contains(1003);
        vStep1004 = errorStepNums.contains(1004);
        vStep1005 = errorStepNums.contains(1005);
        vStep1006 = errorStepNums.contains(1006); 
        
        
        if ( !vStep1001 && !vStep1002 && !vStep1003 && !vStep1004 && !vStep1005 && !vStep1006 )
        {
            verifyEntries = true;
        }
        
        return verifyEntries;
    }
    
    public PageReference registerUsers ()
    {
        if ( String.isNotBlank ( queryString ) )
	        ApexPages.currentPage().getHeaders().put ( 'CF-Validation', queryString ) ;
        
        PageReference pr = null;
        regAttempts++;
        boolean verified;
        
        if ( regAttempts >= 5 )
        {
            o.OB_Reg_Lockout__c = DateTime.now();
            
            System.debug('====== Updating application info. ======');
            System.debug('====== Setting OB Reg Lockout: true ======');
            access.updateObject ( o ) ;
        }
        
        // Handle verification portion first
        if ( vStepNumber == 5000 )
        {
            boolean entries = verifyEntries();
            boolean verifyId = verifyID();
            
            if ( !( ( verifyAttempts > 0 ) && verifyId ) )
            {
                verifyAttempts++;
                
                if ( verifyAttempts >= verifyIdSetting )
                {
                    o.OB_Reg_Lockout__c = DateTime.now();
                    
                    System.debug('====== Updating application info. ======');
                    System.debug('====== Setting OB Reg Lockout: true ======');
		            access.updateObject ( o ) ;
                }
            }
            
           	verified = verifyId && entries;
        }
        else if ( vStepNumber == 9000 )
        {
            verified = verifyEntries();
        }
        
        // Then handle registration portion
        if ( verified )
        {
            vStepNumber = 2000; // success -- end panel
            
            boolean regSuccess = false;
            
            System.debug('vStepNumber == 2000. trying to register user...');
            
            //System.debug('o.Application_ID__c = ' + o.Id);
            
            try
            {
                regSuccess = dih.registerUsersOnlineBanking( o.Id, loginId, userPw );  
            }
            catch ( System.CalloutException e1 )
            {
                System.debug ( 'Callout Error!' ) ;
                System.debug ( e1 ) ;
                
                try
                {
                    regSuccess = dih.registerUsersOnlineBanking( o.Id, loginId, userPw );			
                }
                catch ( System.CalloutException e2 )
                {
                    System.debug ( 'Second Callout Error!' ) ;
                    System.debug ( e2 ) ;
                }
            }
            
            /*}
            else
            {
            	vStepNumber = 6000;
            }
			*/
            
            if ( regSuccess )
            {
                o.Applicant_OB_Registered__c = DateTime.now();
                updateApplication = true;
            }
            else if ( !regSuccess && dih.statusCode == 409 ) // i.e. DI returns 'already registered' message
            {
                vStepNumber = 6000;
                
                // Stamps the application field Applicant_OB_Registered__c w/ current datetime to indicate that the account has been registered
                o.Applicant_OB_Registered__c = DateTime.now();
                updateApplication = true;
            }
            else
            {
                vStepNumber = 6000;
            }
            
            
            System.debug('registration success: ' + regSuccess);
            
            if ( updateApplication )
            {
                System.debug('====== Determining application\'s associated contact. ======');
                
                List<one_application__c> contactIdL = [SELECT Contact__c
                            FROM one_application__c
                            WHERE ID = :o.ID] ;
            
                ID contactID = contactIdL [ 0 ].Contact__c ;
                
                if ( contactID != null )
                {
                    System.debug('====== Associated contact found. ======');
                    System.debug('====== Creating associated service record. ======');
                
                    Service__c s = new Service__c();
                    s.Alternate_Login_ID__c = loginId;
                    s.Type__c = 'Online Banking';
                    s.RecordTypeId = Schema.SObjectType.Service__c.RecordTypeInfosByDeveloperName.get('Online_Services').RecordTypeId ; //Online Services
                    s.Status__c = 'ACTIVE';
                    s.Contact__c = contactID;
                    s.Activation_Date__c = Date.today();
                    
                    System.debug('====== Inserting service record. ======');
                    access.insertObject ( s ) ;
                }
                
                System.debug('====== Updating application info. ======');
                System.debug('====== Setting timestamp for Online Banking Registration for the associated application. ======');
	            access.updateObject ( o ) ;
            }
    
            System.debug ( '========== AT THE END OF REGISTRATION ==========' ) ;
        }
        
        return pr ;
    }
    
    /*  ----------------------------------------------------------------------------------------
    *  Timeout
    *  ---------------------------------------------------------------------------------------- */
    public pageReference pageTimeout ()
    {
        if ( String.isNotBlank ( queryString ) )
	        ApexPages.currentPage().getHeaders().put ( 'CF-Validation', queryString ) ;
        
        PageReference pr = null ;
        
        if ( ( o != null ) && ( o.Application_ID__c != null ) && ( verifyAttempts < verifyIdSetting ) )
        {
            vStepNumber = 5000 ; // go back to verification form if application still tied to page
        }
        else
        {
            vStepNumber = 6000 ;
        }
        
        return pr ;
    }
    
    /*  ----------------------------------------------------------------------------------------
    *  Redirect
    *  ---------------------------------------------------------------------------------------- */
    public PageReference goPageAction ()
    {
        if ( String.isNotBlank ( queryString ) )
	        ApexPages.currentPage().getHeaders().put ( 'CF-Validation', queryString ) ;
        
        if ( ( o != null ) && ( updateApplication ) )
        {
            System.debug('====== Updating application info. ======');
            access.updateObject ( o ) ;
        }
        
        return pageAction ;
    }
    
    /*  ----------------------------------------------------------------------------------------
    *  Page classes
    *  ---------------------------------------------------------------------------------------- */
    
    /*  ----------------------------------------------------------------------------------------
    *  Internal Functions
    *  ---------------------------------------------------------------------------------------- */
    
    // returns true if applicant is already registered for Online Banking
    public boolean alreadyRegistered() 
    {
        List<one_application__c> contactIdL = [SELECT Contact__c
                      	FROM one_application__c
                   		WHERE ID = :o.ID] ;
        
        ID contactID = contactIdL [ 0 ].Contact__c ;
        
        List<Service__c> s;
        
        if ( contactId == null )
        {
            System.debug('alreadyRegistered() ----- no associated contact');
            return false;
        }
        else
        {
			s = [SELECT Alternate_Login_ID__c, Name, Type__c, Status__c, RecordTypeId
        		FROM Service__c
                WHERE Service__c.Contact__c = :contactId
                	AND Service__c.Status__c = 'ACTIVE' AND Service__c.Type__c = 'Online Banking'];
        
            if (s.size() == 0)
            {
                System.debug('alreadyRegistered() ----- no associated active, online banking services');
                return false;
            }
    		else 
            {
                if ( s[0].Alternate_Login_ID__c != null )
                {
                    existingLoginId = s[0].Alternate_Login_ID__c;
                }
                
                System.debug('alreadyRegistered() ----- associated application contact has active, online banking service record');
                return true;
            }
      	}
    }
}