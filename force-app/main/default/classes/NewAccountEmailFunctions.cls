public class NewAccountEmailFunctions 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Settings
	 *  -------------------------------------------------------------------------------------------------- */	
	private static final String API_KEY = 'o-zuUl0_dDiq6OT87S1Agg' ;
	
	private static final String ONEUNITED_FROM_EMAIL = 'noreplies@oneunited.com' ;
	private static final String ONEUNITED_FROM_NAME = 'OneUnited Bank' ;
	private static final String SETTINGS = 'Andera' ;
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Deposit Application Additional Info message
	 *  -------------------------------------------------------------------------------------------------- */	
	private static boolean sendNewDebitCardMessage ( Service__c [] sL )
	{
		System.debug ( '========== Creating JSON Object ==========' ) ;
		
		//  Set up the generator
		JSONGenerator g = JSON.createGenerator ( true ) ;
		
		//  Create the object
		g.writeStartObject () ;
		
		//  API KEY
		g.writeStringField ( 'key', API_KEY ) ;
		
		//  Template
		g.writeStringField ( 'template_name', 'new-debit-card-email' ) ;
		
		g.writeFieldName ( 'template_content' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'OneUnited Bank Template' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;
		
		//  Start of message		
		g.writeFieldName ( 'message' ) ;
		g.writeStartObject () ;

		//  Message contents
		g.writeStringField ( 'from_email', ONEUNITED_FROM_EMAIL ) ;
		g.writeStringField ( 'from_name', ONEUNITED_FROM_NAME ) ;
		
		//  To stuff, always an array!
		g.writeFieldName ( 'to' ) ;
		g.writeStartArray () ;
		
		for ( Service__c s : sL )
		{
			//  Applicant
			g.writeStartObject () ;
			g.writeStringField ( 'email', s.Contact__r.Email ) ;
			g.writeEndObject () ;
		}
		
		g.writeEndArray () ;
		
		g.writeStringField ( 'subject', 'Your Debit Card is in the Mail' ) ;
		g.writeStringField ( 'subaccount', 'NEWACCOUNT' ) ;
		
		// GLOBAL Merge vars
		g.writeFieldName ( 'global_merge_vars' ) ;
		g.writeStartArray () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:COMPANY' ) ;
		g.writeStringField ( 'content', 'OneUnited Bank' ) ;
		g.writeEndObject () ;
		g.writeStartObject () ;
		g.writeStringField ( 'name', 'LIST:Description' ) ;
		g.writeStringField ( 'content', '' ) ;
		g.writeEndObject () ;
		g.writeEndArray () ;
		
		// LOCAL Merge vars
		g.writeFieldName ( 'merge_vars' ) ;
		g.writeStartArray () ;
		
		for ( Service__c s : sL )
		{
			//  Applicant!
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', s.Contact__r.Email ) ;
/*			g.writeFieldName ( 'vars' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeStringField ( 'name', 'mergeApplicationID' ) ;
			g.writeStringField ( 'content', appIDMessageText ) ;
			g.writeEndObject () ;
			g.writeEndArray () ; */
			g.writeEndObject () ;
		}
		
		g.writeEndArray () ;
		
		//  Campaign
		g.writeStringField ( 'google_analytics_campaign', 'OneUnited Debit Card New Account' ) ;
		
		//  Tags
		g.writeFieldName ( 'tags' ) ;
		g.writeStartArray () ;
		g.writeString ( 'new-account' ) ;
		g.writeEndArray () ;
		
		//  Googly
		g.writeFieldName ( 'google_analytics_domains' ) ;
		g.writeStartArray () ;
		g.writeString ( 'oneunited.com' ) ;
		g.writeString ( 'www.oneunited.com' ) ;
		g.writeString ( 'unityvisa.com' ) ;
		g.writeString ( 'www.unityvisa.com' ) ;
		g.writeEndArray () ;
		
		//  Recipient Meta data
		g.writeFieldName ( 'recipient_metadata' ) ;
		g.writeStartArray () ;
		
		for ( Service__c s : sL )
		{
			//  Applicant!
			g.writeStartObject () ;
			g.writeStringField ( 'rcpt', s.Contact__r.Email ) ;
			g.writeFieldName ( 'values' ) ;
			g.writeStartArray () ;
			g.writeStartObject () ;
			g.writeEndObject () ;
			g.writeEndArray () ;
			g.writeEndObject () ;
		}
		
		g.writeEndArray () ;
		
		//  End of Message
		g.writeEndObject () ;
		
		//  End of object
		g.writeEndObject () ;
		
		System.debug ( '========== JSON OUTPUT START ==========' ) ;
		System.debug ( g.getAsString () ) ;
		System.debug ( '========== JSON OUTPUT END ==========' ) ;
		
        return sendHTTPRequest ( g.getAsString () ) ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Sends the email
	 *  -------------------------------------------------------------------------------------------------- */	
	private static boolean sendHTTPRequest ( String js )
	{
		System.debug ( '========== Setting up HTTP Request Object ==========' ) ;
		HttpRequest request = new HttpRequest() ;
	
		request.setEndPoint ( 'https://mandrillapp.com/api/1.0/messages/send-template.json' ) ;
		request.setMethod ( 'POST' ) ;
		request.setHeader ( 'Content-Type', 'application/json' ) ;
		request.setBody ( js ) ;

	    System.debug ( '========== Calling method ==========' ) ;
	    
	    boolean success = true ;    
	    Http http = new Http () ;
	    HTTPResponse res = null ;
	    
	    try
	    {
			res = http.send ( request ) ;
	    }
	    catch ( System.CalloutException e )
	    {
	    	success = false ;
	     	System.debug ( 'HTTP Request fail. :(' ) ;
	    }
	
	    System.debug ( '========== RETURN START ==========' ) ;
	    if ( res != null )
			System.debug ( res.getBody () ) ;
        System.debug ( '========== RETURN END ==========' ) ;
        
        return success ;
	}
	
	/*  --------------------------------------------------------------------------------------------------
	 *  Load Data and send emails 
	 *  -------------------------------------------------------------------------------------------------- */
	public static boolean sendNewDebitCardEmails ( list<Service__c> sL )
	{
		boolean success = true ;
		
		/*  --------------------------------------------------------------------------------------------------
		 *   New Debit Card Emails
		 *  -------------------------------------------------------------------------------------------------- */	
        if ( ( sL != null ) && ( sL.size () > 0 ) )
        {
            System.debug ( '===== Sending ' + sL.size () + ' New Debit Card Email(s) =====' ) ;
            
            success = sendNewDebitCardMessage ( sL ) ;
        }
		
		return success ;
	}
}