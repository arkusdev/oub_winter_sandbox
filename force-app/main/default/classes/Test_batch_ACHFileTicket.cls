@isTest
public with sharing class Test_batch_ACHFileTicket 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Branch__c b = getBranch () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'x' + i + 'test'+ i + 'person@testing.com' ;
		c.phone = '00' + i + '-00' + i + '-000' + i ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.TaxID__c = '00' + i + '0' + i + '000' + i ;
		c.Branch__c = b.ID ;
		
		insert c ;
		
		return c ;
	}
	
	static testmethod void oppTest ()
	{
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c1 = getContact ( 1 ) ;
		
		String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card', 'Financial_Account__c' ) ;
		
		Financial_Account__c fa = new Financial_Account__c () ;
		fa.recordTypeId = creditCardRecordTypeId ;
		fa.MJACCTTYPCD__c = 'VISA' ;
		fa.CURRACCTSTATCD__c = 'ACT' ;
		fa.PRODUCT__c = 'UNITY Visa Secured Card' ;
		fa.NOTEBAL__c = 0 ;
		fa.TAXRPTFORPERSNBR__c = c1.Id ;
		fa.CREDITLIMITAMT__c = 1000.00 ;
		
		insert fa ;
		
		one_application__c o = new one_application__c () ;
		
		o.Number_of_Applicants__c = 'I will be applying individually' ;
		
		o.Contact__c = c1.Id;
		
		o.First_Name__c = c1.FirstName ;
		o.Last_Name__c = c1.LastName ;
		o.Email_Address__c = c1.Email ;
		o.Gross_Income_Monthly__c = 10000.00 ;
		
		o.Referred_By_Contact__c = c1.Id;
		o.Entered_By_Contact__c = c1.Id;

		o.FIS_Application_ID__c = '111222333' ;
		o.Upload_Attachment_Key__c = 'abcdefghijklmnopqrstuvwxyz1234' ;
		
		o.FIS_Decision_Code__c = 'F003' ;
		
		o.Funding_Account_Number__c = '12345679' ;
		o.Funding_Routing_Number__c = '123456789' ;
		o.Funding_Account_Type__c = 'Checking' ;
		o.Funding_Bank_Name__c = 'Test Bank' ;
		o.Funding_Options__c = 'E- Check (ACH)' ;
		o.ACH_Funding_Status__c = 'Originated' ;
		
		o.Trial_Deposit_1__c = 7 ;
		o.Trial_Deposit_2__c = 11 ;
		
		insert o ;
		
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Refund Approver' ) ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		Map<String,String> groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		String ticketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Credit Card Refund', 'Ticket__c' ) ;
		String groupId = groupMap.get ( 'Credit Card Refund Approver' ) ;

		Ticket__c t = new Ticket__c () ;
		t.Status__c = 'Move to Refund' ;
		t.recordTypeId = ticketRecordTypeId ;
		t.Financial_Account__c = fa.Id ;
		t.Application__c = o.Id ;
		t.Approved_Refund_Amount__c = fa.CREDITLIMITAMT__c ;
		t.OwnerId = groupId ;
		t.Application_Refund_Source__c = 'Holding' ;
		t.Card_Blocked__c = true ;
		
		insert t ;
		
		String batchTicketRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'CS Batch File', 'Ticket__c' ) ;
		String batchGroupId = groupMap.get ( 'Credit Card Batch Processing' ) ;

		Ticket__c bt = new Ticket__c () ;
		bt.Status__c = 'Processed' ;
		bt.recordTypeId = batchTicketRecordTypeId ;
		bt.Batch_Type__c = 'SWM' ;
		bt.ownerId = batchGroupId ;
		
		insert bt ;
		
		t.SWM_Batch_Ticket__c = bt.id ;
		
		update t ;

		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_ACHFileTicket b = new batch_ACHFileTicket () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
	}

}