@isTest
public class Test_batch_Deposit_SWIM_Settlement 
{
	/* --------------------------------------------------------------------------------
	 * Custom settings
	 * -------------------------------------------------------------------------------- */
	private static Batch_Process_Settings__c insertBatchCustomSetting ( String settleDays )
    {
        Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;

        bps.Name = 'OneUnited_Batch' ;        
        bps.ACH_GL_Account__c = '1001000100' ;
        bps.ACH_Outgoing_Account__c = '1001000101' ;
        bps.Application_Cashbox__c = '666' ;
        bps.Batch_Email__c = 'lmattera@oneunited.com' ;
        bps.Collateral_GL_Account__c = '1001000102' ;
        bps.Deposit_ePay_GL_Account__c = '1001000103' ;
        bps.Email_Enabled__c = false ;
        bps.Routing_Number__c = '01100127' ;
        bps.UNITY_Visa_ePay_GL_Account__c = '1001000104' ;
        bps.SWIM_Debit_Card_Settlement_Days__c = settleDays ;
        
        insert bps ;
        
        return bps ;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Creates a generic application I use for testing.
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, decimal amount )
	{
		one_application__c app = new one_application__c ();
        app.RecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Deposits', 'one_application__c' ) ;
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = '666-01-1111' ;
        app.Phone__c = c1.Phone ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
		
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = '666-02-1111' ;
            app.Co_Applicant_Phone__c = c2.Phone ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Funding_Options__c = 'Debit Card' ;
		app.Gross_Income_Monthly__c = 10000.00 ;

		app.FIS_Application_ID__c = '111222333' ;
		app.FIS_Decision_Code__c = 'A001' ;
        app.RequestedCreditLimit__c = amount ;
        app.FIS_Credit_Limit__c = amount ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
		
		return app ;
	}
    
    private static application_transaction__c getApplicationTransaction ( one_application__c app )
    {
        application_transaction__c at = new application_transaction__c () ;
        at.Application__c = app.ID ;
        at.Amount__c = app.RequestedCreditLimit__c ;
        at.Status__c = 'Settled' ;
        at.Settled_Amount__c = app.FIS_Credit_Limit__c ;
        
        return at ;
    }

	private static Online_Deposit_Application_Account__c getODAA ( one_application__c app, String acctnbr, decimal initialDeposit )
    {
        Online_Deposit_Application_Account__c odaa = new Online_Deposit_Application_Account__c () ;
        odaa.Application__c = app.ID ;
        odaa.COCC_Account_Number__c = acctnbr ;
        odaa.Initial_Funding_amount__c = initialDeposit ;
        odaa.Major_Account_Type_Code__c = 'XYZ' ;
        odaa.Minor_Account_Type_Code__c = 'MI12' ;
        
        return odaa ;    
    }
    
	public static testmethod void testOneSingle ()
    {
        insertBatchCustomSetting ( '2' ) ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        one_Application__c app = getApplicationForTests ( c1, null, 2500.00 ) ;
        insert app ;
        
        application_transaction__c at = getApplicationTransaction ( app ) ;
        insert at ;
        at.Transaction_Settled_Date_Time__c = System.now().addDays ( -3 ) ;
        update at ;
        
        Online_Deposit_Application_Account__c odaa1 = getODAA ( app, '111222333', 1000.00 ) ;
        insert odaa1 ;
        
        Online_Deposit_Application_Account__c odaa2 = getODAA ( app, '222333444', 2500.00 ) ;
        insert odaa2 ;
        
        Test.startTest () ;
        
        batch_Deposit_SWIM_Settlement bass = new batch_Deposit_SWIM_Settlement () ;
        Database.executeBatch ( bass ) ;
        
        Test.stopTest () ;
    }
}