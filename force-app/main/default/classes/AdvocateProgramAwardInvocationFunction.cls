global class AdvocateProgramAwardInvocationFunction 
{
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes method(s)
	 *  -------------------------------------------------------------------------------------------------- */	
	@InvocableMethod ( label='Advocate Program Award Creation' )
	global static List<AdvocateProgramAwardResult> advocateProgramAward ( List<AdvocateProgramAwardRequest> requests ) 
	{
		//  Not used?
		List<AdvocateProgramAwardResult> aparL = new List<AdvocateProgramAwardResult> () ;
        
        // -------------------------------------------- LOAD --------------------------------------------         
		set<ID> cS = new set<ID> () ; // <- check
        set<String> aL = new set<String> () ;
        set<String> bL = new set<String> () ;
        set<String> rL = new set<String> () ;
        set<String> cbL = new set<String> () ;

        for ( AdvocateProgramAwardRequest apar : requests )
        {
            // WOT
            if ( apar.requestType.equalsIgnoreCase ( 'Activity' ) )
            {
                aL.add ( apar.requestName ) ;
            }
            else if ( apar.requestType.equalsIgnoreCase ( 'Badge' ) )
            {
                bL.add ( apar.requestName ) ;
                
                // DUPES!
                if ( apar.contactID != null )
                	cbL.add ( apar.contactID ) ;
            }
            else if ( apar.requestType.equalsIgnoreCase ( 'Reward' ) )
            {
                rL.add ( apar.requestName ) ;
            }
            
            cS.add ( apar.contactID ) ;
        }
        
        map<ID,Contact> cMap = null ;
        
        try
        {
            cMap = new Map <ID, Contact> (
            [
                SELECT ID, Advocate_Level__r.ID, Advocate_Level__r.Name
                FROM Contact
                WHERE ID IN :cS
            ]) ;
        }
        catch ( Exception e )
        {
            // ?
        }
        
        map<String,Advocate_Activity__c> aaM = new map<String,Advocate_Activity__c> () ;
        map<String,Advocate_Badge__c> abM = new map<String,Advocate_Badge__c> () ;
        map<String,map<String,boolean>> abdM = new map<String,map<String,boolean>> () ;
        map<String,Advocate_Reward__c> arM = new map<String,Advocate_Reward__c> () ;
        
        try
        {
            if ( ( aL != null ) && ( aL.size () > 0 ) )
            {
                // ----- # 1 -----
                list<Advocate_Activity__c> wtfL = [
                    SELECT ID, API_Name__c, CreatedDate
                    FROM Advocate_Activity__c
                    WHERE API_Name__c IN :aL
                ] ;
                
                for ( Advocate_Activity__c wtf : wtfL )
                {
                    if ( ! aaM.containsKey ( wtf.API_Name__c ) )
                    {
                        aaM.put ( wtf.API_Name__c, wtf ) ;
                    }
                    else
                    {
                        if ( aaM.get ( wtf.API_Name__c ).CreatedDate < wtf.CreatedDate )
                        {
                            aaM.put ( wtf.API_Name__c, wtf ) ;
                        }
                    }
                }
            }
            
            if ( ( bL != null ) && ( bL.size () > 0 ) )
            {
                // ----- # 1 -----
                list<Advocate_Badge__c> wtfL = [
                    SELECT ID, API_Name__c, CreatedDate
                    FROM Advocate_Badge__c
                    WHERE API_Name__c IN :bL
                ] ;
                
                for ( Advocate_Badge__c wtf : wtfL )
                {
                    if ( ! abM.containsKey ( wtf.API_Name__c ) )
                    {
                        abM.put ( wtf.API_Name__c, wtf ) ;
                    }
                    else
                    {
                        if ( abM.get ( wtf.API_Name__c ).CreatedDate < wtf.CreatedDate )
                        {
                            abM.put ( wtf.API_Name__c, wtf ) ;
                        }
                    }
                }
                
                // ----- # 2 ----- DUPLICATEs!
                list<Advocate_Badge_Awarded__c> wtfDL = [
                    SELECT ID, API_Name__c, Contact__c
                    FROM Advocate_Badge_Awarded__c
                    WHERE Contact__c IN :cbL
                ] ;
                
                for ( Advocate_Badge_Awarded__c wtfD : wtfDL )
                {
                    if ( ! abdM.containsKey ( wtfD.Contact__c ) )
                    {
                        map<String,boolean> xMap = new map<String,boolean> () ;
                        xMap.put ( wtfD.API_Name__c, true ) ;
                        abdM.put ( wtfD.Contact__c, xMap ) ;
                    }
                    else
                    {
                        abdM.get ( wtfD.Contact__c ).put ( wtfD.API_Name__c, true ) ;
                    }
                }

            }
            
            if ( ( rL != null ) && ( rL.size () > 0 ) )
            {
                // ----- # 1 -----
                list<Advocate_Reward__c> wtfL = [
                    SELECT ID, API_Name__c, CreatedDate
                    FROM Advocate_Reward__c
                    WHERE API_Name__c IN :rL
                ] ;
                
                for ( Advocate_Reward__c wtf : wtfL )
                {
                    if ( ! arM.containsKey ( wtf.API_Name__c ) )
                    {
                        arM.put ( wtf.API_Name__c, wtf ) ;
                    }
                    else
                    {
                        if ( arM.get ( wtf.API_Name__c ).CreatedDate < wtf.CreatedDate )
                        {
                            arM.put ( wtf.API_Name__c, wtf ) ;
                        }
                    }
                }
            }
        }
        catch ( DMLException e )
        {
            // ?
        }
        
		// -------------------------------------------- PARSE --------------------------------------------         
        list<Advocate_Activity_Achieved__c> aaaL = new list<Advocate_Activity_Achieved__c> () ;
        list<Advocate_Badge_Awarded__c> abaL = new list<Advocate_Badge_Awarded__c> () ;
        map<String,map<String,boolean>> abaM = new map<String,map<String,boolean>> () ;
        list<Advocate_Reward_Achieved__c> araL = new list<Advocate_Reward_Achieved__c> () ;
        
        for ( AdvocateProgramAwardRequest apar : requests )
        {
            // WOT
            if ( apar.contactID != null )
            {
                if ( apar.requestType.equalsIgnoreCase ( 'Activity' ) )
                {
                    if ( aaM.containsKey ( apar.requestName ) )
                    {
                        Advocate_Activity_Achieved__c aaa = new Advocate_Activity_Achieved__c () ;
                        aaa.Advocate_Activity__c = aaM.get ( apar.requestName ).ID ;
                        aaa.Contact__c = apar.ContactID ;
                        
                        aaaL.add ( aaa ) ;
                    }
                }
                else if ( apar.requestType.equalsIgnoreCase ( 'Badge' ) )
                {
                    //  ----- First check, do they have the badge already?
                    boolean addBadge = true ;
                    if ( abdM.containsKey ( apar.ContactID ) )
                        if ( abdM.get ( apar.ContactID ).containsKey ( apar.requestName )  )
                            if ( abdM.get ( apar.ContactID ).get ( apar.requestName ) == true )
                                addBadge = false ;
                        
                    if ( addBadge == true )
                    {
                        // ----- Second check, are we trying to award it twice?
                        boolean hazBadge = true ;
                        if ( ! abaM.containsKey ( apar.ContactID ) )
                        {
                            map<String,boolean> xMap = new map<String,boolean> () ;
                            xMap.put ( apar.requestName, true ) ;
                            abaM.put ( apar.ContactID, xMap ) ;
                            hazBadge = false ;
                        }
                        else
                        {
                            if ( ! abaM.get ( apar.ContactID ).containsKey ( apar.requestName ) )
                            {
                                abaM.get ( apar.ContactID ).put ( apar.requestName, true ) ;
                                hazBadge = false ;
                            }
                        }
                        
                        if ( ( ! hazBadge ) && ( abM.containsKey ( apar.requestName ) ) )
                        {
                            Advocate_Badge_Awarded__c aba = new Advocate_Badge_Awarded__c () ;
    
                            aba.Badge__c = abM.get ( apar.requestName ).ID ;
                            aba.Contact__c = apar.ContactID ;
                            
                            abaL.add ( aba ) ;
                        }
                    }
                }
                else if ( apar.requestType.equalsIgnoreCase ( 'Reward' ) )
                {
                    boolean rewardEligible = true ;
                    
                    if ( cMap.containsKey ( apar.ContactID ) )
                    {
                        if ( String.isNotBlank ( cMap.get ( apar.ContactID ).Advocate_Level__r.Name ) )
                        {
	                        if ( cMap.get ( apar.ContactID ).Advocate_Level__r.Name.equalsIgnoreCase ( 'Non Participant' ) )
                                rewardEligible = false ;
                        }
                    }
                    
                    if ( ( arM.containsKey ( apar.requestName ) ) && ( rewardEligible == true ) )
                    {
                        Advocate_Reward_Achieved__c ara = new Advocate_Reward_Achieved__c () ;
                        ara.Advocate_Reward__c = arM.get ( apar.requestName ).ID ;
                        ara.Contact__c = apar.ContactID ;
                        
                        araL.add ( ara ) ;
                    }
                }
            }
        }
        
		// -------------------------------------------- FINAL --------------------------------------------         
        try
        {
	        if ( aaaL.size () > 0 )
                insert aaaL ;
            
	        if ( abaL.size () > 0 )
                insert abaL ;
            
	        if ( araL.size () > 0 )
                insert araL ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
		// -------------------------------------------- LOL WUT --------------------------------------------         
        return aparL ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for input
	 *  -------------------------------------------------------------------------------------------------- */	
	global class AdvocateProgramAwardRequest 
	{
	    @InvocableVariable ( label='Contact ID' required=true )
	    public ID contactID ;
        
	    @InvocableVariable ( label='Award Type' required=true )
	    public String requestType ;
        
	    @InvocableVariable ( label='Award API Name' required=true )
	    public String requestName ;
    }
    
	/*  --------------------------------------------------------------------------------------------------
	 *  Invoke classes for output
	 *  -------------------------------------------------------------------------------------------------- */	
	global class AdvocateProgramAwardResult 
	{
	    @InvocableVariable
	    public ID xID ;
	}
}