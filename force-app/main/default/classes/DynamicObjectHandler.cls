public class DynamicObjectHandler 
{
    // This class acts as a controller for the DynamicObjectViewer component
    
    /* -----------------------------------------------------------------------------------
	 *  Object Type
	 * ----------------------------------------------------------------------------------- */
    private static String getObjectType(sObject newObj) 
    {
        String objType = newObj.getSObjectType().getDescribe().getName();
        return objType ;
    }
    
    /* -----------------------------------------------------------------------------------
	 *  Gets the fields for an object in a string
	 * ----------------------------------------------------------------------------------- */
	static public list<String> getFields ( sObject newObj )
    {
        List<String> accessibleFields = new List<String>();
        Map<String, Schema.SObjectField> fieldMap = newObj.getSObjectType().getDescribe().fields.getMap () ;
        
        for ( String fieldName : fieldMap.keyset () )
        	accessibleFields.add ( fieldName ) ;
        
        return accessibleFields ;
    }

    /* -----------------------------------------------------------------------------------
	 *  Gets the fields for an object, plus a where clause
	 * ----------------------------------------------------------------------------------- */
    public static String getFieldQuery ( sObject newObj, String whereClause, String orderBy ) 
    {
        String theQuery = '' ;
        theQuery += 'SELECT ' + joinList ( getFields ( newObj ), ', ') ;
        
        theQuery += ' FROM ' + getObjectType ( newObj ) ;

        if ( String.isNotBlank ( whereClause ) )
        	theQuery += ' WHERE ' + whereClause ;
        
        if ( String.isNotBlank ( orderBy ) )
            theQuery += ' ORDER BY ' + orderBy ;
                          
        return theQuery ;    	
    }
    
    /* -----------------------------------------------------------------------------------
	 *  Gets the updatable fields for an object
	 * ----------------------------------------------------------------------------------- */
    public static List<String> getUpdateableFields ( sObject newObj )
    {
        List<String> accessibleFields = new List<String>();
        Map<String, Schema.SobjectField> fields = newObj.getSObjectType().getDescribe().fields.getMap();
            
        for (String s : fields.keySet()) 
        {
            if ((s != 'Name') && (fields.get(s).getDescribe().isUpdateable () ) ) 
            {
                accessibleFields.add(s);
            }
        }
        
        return accessibleFields ;
    }
    
    /* -----------------------------------------------------------------------------------
	 *  Gets the updatable fields for an object, plus a where clause
	 * ----------------------------------------------------------------------------------- */
    public static String getUpdateableFieldQuery ( sObject newObj, String whereClause, String orderBy ) 
    {
    	return getUpdateableFieldQuery ( newObj, whereClause, orderBy, null ) ;
    }
    
    public static String getUpdateableFieldQuery ( sObject newObj, String whereClause, String orderBy, list<String> otherFieldList ) 
    {
        String theQuery = '' ;
        theQuery += 'SELECT ' + joinList ( getUpdateableFields ( newObj ), ', ') ;
        
        if ( ( otherFieldList != null ) && ( otherFieldList.size () > 0 ) )
        	theQuery += ', ' + joinList ( otherFieldList, ', ' ) ;
        
        theQuery += ' FROM ' + getObjectType ( newObj ) ;

        if ( String.isNotBlank ( whereClause ) )
        	theQuery += ' WHERE ' + whereClause ;
        
        if ( String.isNotBlank ( orderBy ) )
            theQuery += ' ORDER BY ' + orderBy ;
                          
        return theQuery ;    	
    }
    
    /* -----------------------------------------------------------------------------------
	 *  Join an Apex List of fields into a SELECT fields list string
	 * ----------------------------------------------------------------------------------- */
    private static String joinList(List<String> theList, String separator) 
    {
        if (theList == null)   { return null; }
        if (separator == null) { separator = ''; }

        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) 
        {
            if(null != item) 
            {
                if(firstItem){ firstItem = false; }
                else { joined += separator; }
                joined += item;
            }
        }
        return joined;
    }
    
    /* -----------------------------------------------------------------------------------
	 *  Join an Apex List of fields into a SELECT fields list string with Quotes
	 * ----------------------------------------------------------------------------------- */
    public static String joinListQuoted(List<String> theList, String separator) 
    {
        if (theList == null)   { return null; }
        if (separator == null) { separator = ''; }

        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) 
        {
            if(null != item) 
            {
                if(firstItem){ firstItem = false; }
                else { joined += separator; }
                joined += '\'' + item + '\'';
            }
        }
        return joined;
    }
}