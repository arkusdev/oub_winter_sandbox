global class batch_DeDuplication_Schedule implements Schedulable
{
	private Batch_Process_Settings__c bps ;

	global void execute ( SchedulableContext sc ) 
   	{
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
        integer recordCount = 0 ;
        
        if ( bps != null )
        {
            if ( ( bps.Batch_DeDeduplication_Count__c != null ) && ( bps.Batch_DeDeduplication_Count__c > 0 ) )
                recordCount = bps.Batch_DeDeduplication_Count__c.intValue () ;
        }
        
		batch_DeDuplication b = new batch_DeDuplication () ;
        
        System.debug ( 'Record batch limit :: ' + recordCount ) ;
        
        if ( recordCount > 0 )
			Database.executeBatch ( b, recordCount ) ;
        else
			Database.executeBatch ( b ) ;
   	}
}