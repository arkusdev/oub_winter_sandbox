public class runRTDXPasswordChangeQueueable implements Queueable, Database.AllowsCallouts
{
    public runRTDXPasswordChangeQueueable () 
	{
        // empty
    }

    public void execute(QueueableContext context) 
	{
        one_settings__c settings = one_settings__c.getInstance ( 'settings' ) ;
        
        //  Set up and log in
        RTDXHelper rh = new RTDXHelper () ;
        RTDXHelper.CardData rhcdL = rh.logIN ( '1234' ) ;

        if ( rhcdL.responseCode.equalsIgnoreCase ( '00' ) ) 
        {
            String newPassword = one_utils.getRandomString ( 3 ) + '#' + one_utils.getRandomString ( 4 ) ;
    
            RTDXHelper.CardData rhcdPW = rh.changePassword ( newPassword ) ;
            
            //  Success ?!?
            if ( rhcdPW.responseCode.equalsIgnoreCase ( '00' ) )
            {
                settings.FIS_RTDX_Old_Password__c = settings.FIS_RTDX_Password__c ;
                settings.FIS_RTDX_Password__c = newPassword ;
                settings.FIS_RTDX_Password_Expire_Days__c = rhcdPW.daysLeft ;
                
                try
                {
                    update settings ;
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }
            else
            {
                System.debug ( 'Failed to change password?!?' ) ;
            }
        }
        else
        {
            System.debug ( 'Failed to log in ?!?!?' ) ;
        }
    }
}