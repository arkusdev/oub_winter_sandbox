@isTest
public class test_batch_Advocate_Calculator 
{
    /* -----------------------------------------------------------------------------------
	 * Load and return a branch
	 * ----------------------------------------------------------------------------------- */
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		return b ;
	}
	
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i, Branch__c b, String customerRecordTypeID )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'Person' + i ;
		c.Email = 'test'+ i + '.person' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.Branch__c = b.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		return c ;
	}
	
	
	//  --------------------------------------------------------------------------------------
	//  Advocate
	//  --------------------------------------------------------------------------------------
    private static void setupAdvocateCrap ()
    {
        Advocate_Activity__c aa1 = new Advocate_Activity__c () ;
        aa1.Name = 'TEST' ;
        aa1.Activity_Points__c = 100.0 ;
        aa1.API_Name__c = 'Test' ;
        aa1.Public_Use__c = true ;
        
        insert aa1 ;
        
        Advocate_Reward__c ar2 = new Advocate_reward__c () ;
        ar2.Name = 'Reward2' ;
        ar2.Public_Use__c = true ;
        ar2.API_Name__c = 'Reward2' ;
        
        insert ar2 ;
        
        Advocate_Level__c aL3 = new Advocate_Level__c () ;
        aL3.Name = 'Level3' ;
        aL3.Achievement_Points__c = 200.0 ;
        aL3.Public_Use__c = true ;
        insert aL3 ;
        
        Advocate_Level__c aL2 = new Advocate_Level__c () ;
        aL2.Name = 'Level2' ;
        aL2.Next_Level__c = aL3.ID ;
        aL2.Achievement_Points__c = 100.0 ;
        aL2.Public_Use__c = true ;
        aL2.Advocate_Reward__c = ar2.ID ;
        insert aL2 ;
        
        Advocate_Level__c aL1 = new Advocate_Level__c () ;
        aL1.Name = 'Level1' ;
        aL1.Next_Level__c = aL2.ID ;
        aL1.Achievement_Points__c = 0.0 ;
        aL1.Public_Use__c = true ;
        insert aL1 ;
        
        Advocate_Level__c aL0 = new Advocate_Level__c () ;
        aL0.Name = 'Level0' ;
        aL0.Next_Level__c = null ;
        aL0.Achievement_Points__c = 0.0 ;
        aL0.Public_Use__c = false ;
        insert aL0 ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * TEST!
	 * ----------------------------------------------------------------------------------- */
    public static testmethod void ZERO ()
    {
        setupAdvocateCrap () ;
        
        Branch__c b1 = getBranch () ;
        insert b1 ;
        
        Advocate_Level__c aL0 = [ SELECT ID FROM Advocate_Level__c WHERE Name = 'Level0' ] ;
        
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		
		Contact c1 = getContact ( 1, b1, customerRecordTypeID ) ;
        c1.Advocate_Level__c = aL0.ID ;
        insert c1 ;
        
 		Test.startTest () ;
		
        batch_Advocate_Calculator b = new batch_Advocate_Calculator () ;
        Database.executeBatch ( b ) ;
                
        Test.stopTest () ;
    }
    
    public static testmethod void SINGLE ()
    {
        setupAdvocateCrap () ;
        
        Branch__c b1 = getBranch () ;
        insert b1 ;
        
        Advocate_Level__c aL1 = [ SELECT ID FROM Advocate_Level__c WHERE Name = 'Level1' ] ;
        
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
		
		Contact c1 = getContact ( 1, b1, customerRecordTypeID ) ;
        c1.Advocate_Level__c = aL1.ID ;
        insert c1 ;
        
        Advocate_Activity__c aa1 = [ SELECT ID FROM Advocate_Activity__c WHERE Name = 'TEST' ] ;
        
        Advocate_Activity_Achieved__c aaa1 = new Advocate_Activity_Achieved__c () ;
        aaa1.Contact__c = c1.ID ;
        aaa1.Advocate_Activity__c = aa1.ID ;
        
        insert aaa1 ;
        
        Contact cx0 = [ SELECT Advocate_Next_Level_Has_Reward__c, Reached_Next_Advocate_Level__c FROM Contact WHERE ID = :c1.ID ] ;
        
        system.assertEquals ( cx0.Reached_Next_Advocate_Level__c, true ) ;
        
 		Test.startTest () ;
		
        batch_Advocate_Calculator b = new batch_Advocate_Calculator () ;
        Database.executeBatch ( b ) ;
                
        Test.stopTest () ;
        
        Contact cx1 = [ SELECT Advocate_Next_Level_Has_Reward__c, Reached_Next_Advocate_Level__c FROM Contact WHERE ID = :c1.ID ] ;
        
        system.assertEquals ( cx1.Reached_Next_Advocate_Level__c, false ) ;
	}
    
    public static testmethod void YOHOHOHO ()
    {
        setupAdvocateCrap () ;
        
        Branch__c b1 = getBranch () ;
        insert b1 ;
        
        Advocate_Level__c aL1 = [ SELECT ID FROM Advocate_Level__c WHERE Name = 'Level1' ] ;
        
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
        
        list<Contact> cL = new list<Contact> () ;
        
        for ( Integer i = 0 ; i < 100 ; i ++ )
        {
            Contact c = getContact ( i, b1, customerRecordTypeID ) ;
            c.Advocate_Level__c = aL1.ID ;
            cL.add ( c ) ;
        }
        
        insert cL ;
        
        Advocate_Activity__c aa1 = [ SELECT ID FROM Advocate_Activity__c WHERE Name = 'TEST' ] ;
        
        list<Advocate_Activity_Achieved__c> aaaL = new list<Advocate_Activity_Achieved__c> () ;
        
        for ( Integer i = 0 ; i < 100 ; i ++ )
        {
            Advocate_Activity_Achieved__c aaa = new Advocate_Activity_Achieved__c () ;
            aaa.Contact__c = cL.get ( i ).ID ;
            aaa.Advocate_Activity__c = aa1.ID ;
            
            aaaL.add ( aaa ) ;
        }
        
        insert aaaL ;
        
 		Test.startTest () ;
		
        batch_Advocate_Calculator b = new batch_Advocate_Calculator () ;
        Database.executeBatch ( b ) ;
                
        Test.stopTest () ;
	}
}