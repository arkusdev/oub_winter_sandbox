@isTest
public class oneTransactionLoadLeadTest 
{
    private static void setupTesting ()
    {
        list<OneCampaign__c> ocL = new list<OneCampaign__c> () ;
        OneCampaign__c oc1 = new OneCampaign__c () ;
        oc1.name = 'MC OneTransaction A Will' ;
        oc1.Active__c = true ;
        ocL.add ( oc1 ) ;
        
        insert ocL ;
        
        Workshop__c w1 = new Workshop__c () ;
        w1.Name = 'Crenshaw RSVP' ;
        
        insert w1 ;
        
        Workshop__c w2 = new Workshop__c () ;
        w1.Name = 'OneTransaction' ;
        
        insert w2 ;
        
        //  Settings
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.Crenshaw_RSVP_Workshop_ID__c = w1.id ;
    	ws.Workshop_Contact_Record_Type__c = 'Prospect' ;
        ws.OneTransaction_Workshop_ID__c = w2.id ;
        
        insert ws ;
        
        Account a = new account () ;
        a.name = 'Insight Customers - Individuals' ;
        insert a ;
        
        //  OTHER Settings
        one_settings__c s = new one_settings__c () ;
        s.Name = 'settings';
        s.Account_ID__c = a.ID ;
        
        insert s ;
    }
    
	public static testmethod void testZero ()
    {
        setupTesting () ; 
        
        Test.startTest() ;
        
        oneTransactionLoadLeadController all = new oneTransactionLoadLeadController () ;
        all.go () ;
        
        Test.stopTest() ;
    }
    
    public static testmethod void testOne ()
    {
        setupTesting () ;
        
        Test.startTest () ;
        
        ApexPages.currentPage().getParameters().put ( 'fN', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'lN', 'Special' ) ;
        ApexPages.currentPage().getParameters().put ( 'eN', 'Kevin@Special.com' ) ;
        ApexPages.currentPage().getParameters().put ( 'cL', 'A Will' ) ;
        ApexPages.currentPage().getParameters().put ( 'eM', 'Kevin Special' ) ;

        oneTransactionLoadLeadController all = new oneTransactionLoadLeadController () ;
        all.go () ;
        
        Test.stopTest() ;
    }

    public static testmethod void testTwo ()
    {
        Account a = new Account () ;
        a.Name = 'Test Account' ;
        insert a ;
        
        Contact c1 = new Contact () ;
        c1.FirstName = 'Kevin' ;
        c1.LastName = 'Special' ;
        c1.Email = 'Kevin@Special.com' ;
        c1.AccountId = a.ID ;
        insert c1 ;
        
        setupTesting () ;
        
        Test.startTest () ;
        
        ApexPages.currentPage().getParameters().put ( 'fN', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'lN', 'Special' ) ;
        ApexPages.currentPage().getParameters().put ( 'eN', 'Kevin@Special.com' ) ;
        ApexPages.currentPage().getParameters().put ( 'cL', 'A Will' ) ;
        ApexPages.currentPage().getParameters().put ( 'eM', 'Kevin Special' ) ;

        ApexPages.currentPage().getParameters().put ( 'UTM_Content__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Campaign__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Medium__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Source__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Term__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_VisitorID__c', 'Kevin' ) ;

        oneTransactionLoadLeadController all = new oneTransactionLoadLeadController () ;
        all.go () ;
        
        Test.stopTest() ;
    }
    
    public static testmethod void testThree ()
    {
        Lead l1 = new lead () ;
        l1.FirstName = 'Kevin' ;
        l1.LastName = 'Special' ;
        l1.Email = 'Kevin@Special.com' ;
        l1.Company = 'Kevin\'s Special' ;
        insert l1 ;
        
        setupTesting () ;
        
        Test.startTest () ;
        
        ApexPages.currentPage().getParameters().put ( 'fN', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'lN', 'Special' ) ;
        ApexPages.currentPage().getParameters().put ( 'eN', 'Kevin@Special.com' ) ;
        ApexPages.currentPage().getParameters().put ( 'cL', 'A Will' ) ;
        ApexPages.currentPage().getParameters().put ( 'eM', 'Kevin Special' ) ;

        ApexPages.currentPage().getParameters().put ( 'UTM_Content__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Campaign__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Medium__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Source__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_Term__c', 'Kevin' ) ;
        ApexPages.currentPage().getParameters().put ( 'UTM_VisitorID__c', 'Kevin' ) ;

        oneTransactionLoadLeadController all = new oneTransactionLoadLeadController () ;
        all.go () ;
        
        Test.stopTest() ;
    }
}