global class batch_Application_SWIM_Settlement implements Database.Batchable<SObject>, Database.Stateful
{
    //  Settings
	private Batch_Process_Settings__c bps ;

    //  Groups & RecordType
	private Map<String,String> groupMap ;
	private List<RecordType> rtL ;
    
	//  Counters & Ticket ID
	global Integer accountCount ;
	global Decimal accountSum ;
	global String ticketId ;
	
	global batch_Application_SWIM_Settlement ()
	{
        //  Custom settings
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
        
        //  Groups
		List<String> groupList = new List<String> () ;
		groupList.add ( 'Credit Card Batch Processing' ) ;
		
		groupMap = OneUnitedUtilities.getGroupMap ( groupList ) ;
		
		//  Set up the record types we need for later
		List<String> rtNameList = new List<String> () ;
		rtNameList.add ( 'CS Batch File' ) ;
		
		rtL = OneUnitedUtilities.getRecordTypeList ( rtNameList ) ; 
        
        // Sums and crap
        accountCount = 0 ;
        accountSum = 0.0 ;
    }
		
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query = '' ;
        query += 'SELECT ID, ' ;
        query += 'Amount__c, Status__c, Settled_Amount__c, ' ;
        query += 'Application__c, ' ;
        query += 'Application__r.Application_ID__c, ' ;
        query += 'Application__r.Last_Name__c ' ;
        query += 'FROM ' ;
        query += 'Application_Transaction__c ' ;
        query += 'WHERE ' ;
        query += 'Application__r.RecordType.DeveloperName = \'UNITY_Visa\' ' ;
        query += 'AND ' ;
        query += 'SWIM_Settlement_Ticket__c = NULL ' ;
		query += 'AND ' ;
		query += 'Status__c = \'Settled\' ' ;
		query += 'AND ' ;
		query += 'Application__c != NULL ' ;
        
        if ( ( bps != null ) && ( bps.SWIM_Debit_Card_Settlement_Days__c != null ) )
        {
			query += 'AND ' ;
            query += 'Transaction_Settled_Date_Time__c < LAST_N_DAYS:' + bps.SWIM_Debit_Card_Settlement_Days__c + ' ' ;
        }
        
        query += '' ;
		
		return Database.getQueryLocator ( query ) ;
    }
    
	global void execute ( Database.Batchablecontext bc, List<Application_Transaction__c> atL )
	{
        if ( atL.size () > 0 )
        {
            COCCSWIMHelper csh = new COCCSWIMHelper () ;
            csh.setCashboxFromSettings () ;
            
            //  First loop
			for ( Application_Transaction__c at : atL )
			{
				accountCount ++ ;
				accountSum += at.Settled_Amount__c ;
            
                one_Application__c o = at.Application__r ;
                String fixName = o.Last_Name__c.replace ( '\'', '-' ) ;
                String description = 'DEP VISA ' + o.Application_ID__c + ' -- ' + fixName ;
		        csh.createSWIMDoubleRecord ( bps.UNITY_Visa_ePay_GL_Account__c, bps.Collateral_GL_Account__c, at.Settled_Amount__c, o.Application_ID__c, description ) ;
			}
            
            //  File name
	        Datetime dt = System.now () ;
			String fileName = '1218_UVSETTLEDEBIT_' + dt.format ( 'yyyyMMddHHmmss' ) + '.SWM' ;
            
            //  Create the overall ticket
			Ticket__c batchTicket = new Ticket__c () ;
			
			batchTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
			batchTicket.Status__c = 'New' ;
			batchTicket.Batch_Account_Total__c = accountCount ;
			batchTicket.Batch_Amount_Total__c = accountSum ;
			batchTicket.ownerId = groupMap.get ( 'Credit Card Batch Processing' ) ;
	        batchTicket.Is_Batch_Refund__c = false ;
			batchTicket.Batch_Type__c = 'SWM DEBIT' ;
            batchTicket.Batch_File_Name__c = fileName ;
            batchTicket.Description__c = 'BATCH UNITY Visa SWIM Settlement' ;
            
			insert batchTicket ;
			
			//  Attachment ?!
			Attachment a = new Attachment () ;
            a.ParentId = batchTicket.ID ;
            a.Name = fileName ;
            a.ContentType = 'Text/txt' ;
            a.Body = Blob.valueOf ( csh.getSWIMFile ().escapeHTML4 () ) ;
            
            insert a ;
            
            //  Update original transactions with new batch ticket
			List<Application_Transaction__c> nATL = new List<Application_Transaction__c> () ;
            
			for ( Application_Transaction__c at : atL )
			{
				at.SWIM_Settlement_Ticket__c = batchTicket.id ;
				
				nATL.add ( at ) ;
			}
			
			update nATL ;
			
			ticketId = batchTicket.id ;
        }
        
        System.debug ( 'Found ' + accountCount + ' transactions with a value of ' + formatMoney ( accountSum ) ) ;
    }
    
   	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
      	OrgWideEmailAddress o = [
      		SELECT ID
      		FROM OrgWideEmailAddress
      		WHERE Address = 'customersupport@oneunited.com'
      	] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage cMail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c,
				'helpdesk@OneUnited.com' 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email,
				'helpdesk@OneUnited.com' 
			} ;
		}
   
		cMail.setToAddresses ( toAddresses ) ;
		cMail.setSubject ( 'Batch UNITY Visa Debit SWM File Job Results :: ' + a.Status ) ;
		
		if ( o != null )
			cMail.setOrgWideEmailAddressId ( o.Id ) ;
		
		String message = '' ;
		message += '<h1>Results from creating the newest UNITY Visa Debit SWM batch file:</h1>' ;
		message += '<p>The batch processed ' + a.TotalJobItems +' batches ' ;
		if ( a.NumberOfErrors > 0 )
			message += 'with '+ a.NumberOfErrors + ' failures.' ;
		message += '</p>' ;
		message += '<p>There are ' + accountCount + ' account(s) totalling ' + formatMoney ( accountSum ) + ' in the batch.</p>' ;
		message += '<br/>' ;
		
		if ( ( accountCount > 0 ) && ( accountSum > 0.0 ) && ( String.isNotBlank ( ticketId ) ) )
		{
			System.URL sURL = URL.getSalesforceBaseURL () ;
			String urly = sURL.toExternalForm () + '/' + ticketId ;
			message += '<p>You can set the batch at:</p>' ;
			message += '<a href="'+ urly + '">' + urly + '</a>' ;
		}
   
   		cMail.setPlainTextBody ( '' ) ;  // WTB no "null"
		cMail.setHtmlBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { cMail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { cMail } ) ;
			}
		}
	}
	
	private String formatMoney ( Decimal x )
	{
		Decimal dollars;
		Decimal cents;
		dollars = x.intValue();
		cents = x - x.intValue();
		cents = cents.setScale(2);  // it is possible to store repeating decimals in sfdc currency fields…I found out the hard way.
		String amtText = '$' + dollars.format() + cents.toPlainString().substring(1) ;
		return amtText ;		
	}
}