public class OneUnitedUtilities 
{
	/* -----------------------------------------------------------------------------------
	 *  Internal users, should never change
	 * ----------------------------------------------------------------------------------- */
	private static Map<String,User> guestUserMap ;
	private static Map<String,User> apiUserMap ;
	 
	/* -----------------------------------------------------------------------------------
	 *  Gets the record type ID for a given name
	 *  DEPRECATED - Different objects can have the same name, use the new method
	 * ----------------------------------------------------------------------------------- */
	static public String getRecordTypeId ( String recordTypeName )
	{
		String recordTypeId = '' ;
		
		List<RecordType> rtL = new List<RecordType> () ;
		
		rtL = [
			SELECT ID 
			FROM RecordType 
			WHERE Name = :recordTypeName
			AND isActive = true
		] ;
	
		//  Returns the first record type if found more than one
		if ( ( rtL != null ) && ( rtL.size () > 0 ) )
		{
			RecordType rt = rtL.get ( 0 ) ;
			recordTypeId = rt.id ;
		}
		
		return recordTypeId ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Gets the record type IDs for list of given names
	 *  POST PROCESSING REQUIRED DUE TO OVERLAPPING SOBJECTS
	 * ----------------------------------------------------------------------------------- */
	static public List<RecordType> getRecordTypeList ( List<String> recordTypeNames )
	{
		List<RecordType> rtL = null ;
		
		rtL = [
			SELECT ID, Name, SObjectType
			FROM RecordType 
			WHERE Name IN :recordTypeNames
			AND isActive = true
		] ;
		
		return rtL ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Pass in a List and Sobject name to get ID from the list
	 * ----------------------------------------------------------------------------------- */
	static public String getRecordTypeIdFromList ( List<RecordType> rtL, String recordTypeName, String sObjectName )
	{
		String recordTypeId = null ;
		
		for ( RecordType rt : rtL )
		{
			if ( ( rt.Name.equalsIgnoreCase ( recordTypeName ) ) && ( rt.SOBjectType.equalsIgnoreCase ( sObjectName ) ) )
			{
				recordTypeId = rt.Id ;
				break ;
			}
		}
		
		return recordTypeId ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Gets the record type ID for a given name and object
	 * ----------------------------------------------------------------------------------- */
	static public String getRecordTypeIdForObject ( String recordTypeName, String sObjectType )
	{
		String recordTypeId = '' ;
		
		List<RecordType> rtL = new List<RecordType> () ;
		
		rtL = [
			SELECT ID 
			FROM RecordType 
			WHERE Name = :recordTypeName
			AND SObjectType = :sObjectType
			AND IsActive = true
		] ;
	
		//  Returns the first record type if found more than one
		if ( ( rtL != null ) && ( rtL.size () > 0 ) )
		{
			RecordType rt = rtL.get ( 0 ) ;
			recordTypeId = rt.id ;
		}
		
		return recordTypeId ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Gets the group ID for a given name
	 * ----------------------------------------------------------------------------------- */
	static public String getGroupId ( String groupName )
	{
		String groupId = '' ;
		
		List<Group> gL = new List<Group> () ;
		
		gL = [
			SELECT ID FROM Group WHERE Name = :groupName
		] ;
	
		if ( ( gL != null ) && ( gL.size () > 0 ) )
		{
			Group g = gL.get ( 0 ) ;
			groupId = g.id ;
		}
		
		return groupId ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Returns a map of IDs for a list of group names
	 * ----------------------------------------------------------------------------------- */
	static public Map<String,String> getGroupMap ( List<String> groupNames )
	{
		Map<String,String> groupMap = null ;
		
		List<Group> gL = new List<Group> () ;
		
		gL = [
			SELECT ID, Name FROM Group WHERE Name IN :groupNames
		] ;
		
		if ( ( gL != null ) && ( gL.size () > 0 ) )
		{
			groupMap = new Map<String,String> () ;
			
			for ( Group g : gL )
			{
				System.debug ( g.Name + ' -- ' + g.Id ) ;
				groupMap.put ( g.Name, g.Id ) ;
			}
		}
		
		return groupMap ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Check to see if the String is a user
	 * ----------------------------------------------------------------------------------- */
	static public boolean isUser ( String id )
	{
		boolean valid = false ;
		
		if ( ( String.isNotBlank ( id ) ) && ( id.startsWith ( '005' ) ) )
			valid = true ;
		
		return valid ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Check to see if the String is a group
	 * ----------------------------------------------------------------------------------- */
	static public boolean isGroup ( String id )
	{
		boolean valid = false ;
		
		if ( ( String.isNotBlank ( id ) ) && ( id.startsWith ( '00G' ) ) )
			valid = true ;
		
		return valid ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Email validation
	 * ----------------------------------------------------------------------------------- */
	static public boolean isValidEmail ( String emailAddress )
	{
		boolean valid = true ;
		
        Pattern p ;
        Matcher m ;
       
        // First check is <something>@<alpha-numeric>.<alpha-numeric>
        p = Pattern.compile ( '(.*)@[a-zA-Z0-9]+\\.[a-zA-Z0-9.]+[a-zA-Z]\\Z' ) ;
        m = p.matcher ( emailAddress ) ;
		
		if ( ! m.find () )
            valid = false ;
            
        // Second check is <non-alpha-numeric><something>
        p = Pattern.compile ( '\\A[^a-zA-Z0-9]' ) ;
        m = p.matcher ( emailAddress ) ;

        if ( m.find () )
            valid = false ;
        
        // Third check is <non-alpha-numeric>@
        p = Pattern.compile ( '[^a-zA-Z0-9]@' ) ;
        m = p.matcher ( emailAddress ) ;
        
        if ( m.find () )
            valid = false ;

        // Fourth check is <only-numeric>@
        p = Pattern.compile ( '^[0-9]+@' ) ;
        m = p.matcher ( emailAddress ) ;
        
        if ( m.find () )
            valid = false ;

        // Fifth check is ','

        if ( emailAddress.contains ( ',' ) )
            valid = false ;

		//  Garbage matches we constantly get
		if ( ( emailAddress.contains ( '@none.com' ) ) ||
		     ( emailAddress.contains ( 'none@' ) ) ||
		     ( emailAddress.contains ( 'na@' ) ) ||
		     ( emailAddress.contains ( '@na.com' ) ) || 
			 ( emailAddress.contains ( '@2.3' ) )
		   )
			valid = false ;	
		
			
		return valid ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Phone validation - checks for 10 or 11 digits
	 * ----------------------------------------------------------------------------------- */
	static public boolean isValidPhoneNumber ( String phoneNumber )
	{
		boolean valid = true ;
		
		String cPhoneNumber = phoneNumber.replaceAll ( '\\D','' ) ;
		
		if ( ( cPhoneNumber.length () != 10 ) && ( cPhoneNumber.length () != 11 ) )
			valid = false ;
		
		return valid ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Clean phone number
	 * ----------------------------------------------------------------------------------- */
	static public String cleanPhoneNumber ( String phoneNumber )
	{
		String cPhoneNumber = phoneNumber.replaceAll ( '\\D','' ) ;
		
		//  Strip leading '1'
		if ( cPhoneNumber.length () == 11 )
			cPhoneNumber = cPhoneNumber.right ( 10 ) ;
		
		cPhoneNumber = cPhoneNumber.subString ( 0, 3 ) + '-' + cPhoneNumber.subString ( 3, 6 ) + '-' + cPhoneNumber.subString ( 6 ) ;
				
		return cPhoneNumber ;	
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Zip validation - checks for 5 or 9 digits
	 * ----------------------------------------------------------------------------------- */
	static public boolean isValidZipCode ( String zipCode )
	{
		boolean valid = true ;
		
		String cZipCode = zipCode.replaceAll ( '\\D','' ) ;
		
		if ( ( cZipCode.length () != 5 ) && ( cZipCode.length () != 9 ) )
			valid = false ;
		
		return valid ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Clean zip code
	 * ----------------------------------------------------------------------------------- */
	static public String cleanZipCode ( String zipCode )
	{
		String cZipCode = zipCode.replaceAll ( '\\D','' ) ;
		
		if ( cZipCode.length () == 9 )
			cZipCode = cZipCode.subString ( 0, 5 ) + '-' + cZipCode.subString ( 5 ) ;
		
		return cZipCode ;
	}
	 
	/* -----------------------------------------------------------------------------------
	 *  Look up contact via email
	 * ----------------------------------------------------------------------------------- */
	public static Contact lookupContact ( String emailAddress )
	{
		Contact c = null ;
		
		List<Contact> cL = [
			SELECT ID, FirstName, LastName, Email, MailingState
			FROM Contact
			WHERE email = :emailAddress
 			ORDER BY CreatedDate
 		] ;
		
		if ( ( cL != null ) && ( cL.size () > 0 ) )
			c = cL [ 0 ] ;
		
		return c ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Look up map of contacts via email list
	 * ----------------------------------------------------------------------------------- */
	public static Map<String,Contact> lookupContactMap ( List<String> emailAddresses )
	{
		Map<String,Contact> mC = new Map<String,Contact> () ;
		
		List<Contact> cL = [
			SELECT ID, Email, MailingState
			FROM Contact
			WHERE email IN :emailAddresses
 			ORDER BY CreatedDate
 		] ;
		
		for ( Contact c: cL )
		{
			if ( ! mC.containsKey ( c.Email ) )
				mC.put ( c.Email, c ) ;
		}
		
		return mC ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Day of week - Sunday is 6! ( Salesforce uses Monday->Sunday week)
	 * ----------------------------------------------------------------------------------- */
	public static integer getDayOfWeek ( Date d )
	{
		date x = d.toStartofWeek () ;
		integer dayOfWeek = x.daysBetween ( d ) ;
		
		return dayOfWeek ;
	}
		
	/* -----------------------------------------------------------------------------------
	 *  Check for guest user
	 * ----------------------------------------------------------------------------------- */
	public static boolean isGuestUser ( String inID ) 
	{
		boolean isGuest = false ;
		
		if ( guestUserMap == null )
		{
			guestUserMap = new Map<String,User> ( [
				SELECT ID, Name
				FROM User
				WHERE Name LIKE '%Guest%'
			] ) ;
		}
		
		if ( ( guestUserMap != null ) && ( guestUserMap.containsKey ( inID ) ) )
		{
			isGuest = true ;
		}
		
		return isGuest ;
	}

	/* -----------------------------------------------------------------------------------
	 *  Check for API user
	 * ----------------------------------------------------------------------------------- */
	public static boolean isAPIUser ( String inID ) 
	{
		boolean iSAPI = false ;
		
		if ( apiUserMap == null )
		{
			apiUserMap = new Map<String,User> ( [
				SELECT ID, Name
				FROM User
				WHERE Name = 'SalesForce Automation'
			] ) ;
		}
		
		if ( ( apiUserMap != null ) && ( apiUserMap.containsKey ( inID ) ) )
		{
			iSAPI = true ;
		}
		
		return iSAPI ;
	}
    
	/* -----------------------------------------------------------------------------------
	 *  Get list of users
	 * ----------------------------------------------------------------------------------- */
    private static List<User> getUserList ()
    {
        List<User> uL = null ;
        
		try
		{
			uL = [
				SELECT ID,
				Name,
				Branch__c,
				Email,
				Department,
                UserRole.ID,
                UserRole.Name,
                ManagerID,
                Manager.Name,
                Manager.ID,
                Manager.Email,
				Is_NMLO__c,
                UserType
				FROM
				User
				WHERE
				IsActive = true
                AND
                UserType = 'Standard'
			] ;
        }
		catch ( Exception e )
		{
			System.debug ( 'User Select error?' ) ;
			uL = null ;
		}
        
        return uL ;
    }
	
	/* -----------------------------------------------------------------------------------
	 *  Get all active users
	 * ----------------------------------------------------------------------------------- */
	public static Map<String,User> getActiveUsers ()
	{
        Map<String,User> uMap = null ;
        
        List<User> uL = getUserList () ;
        
        if ( ( uL != null ) && ( uL.size () > 0 ) )
        {
            uMap = new Map<String,User> () ;
            for ( User u : uL )
            {
                uMap.put ( u.ID, u ) ;
            }
        }
		
		return uMap ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Get all active users by Email
	 * ----------------------------------------------------------------------------------- */
	public static Map<String,User> getActiveUsersByEmail ()
	{
        Map<String,User> uMap = null ;
        
        List<User> uL = getUserList () ;
        
        if ( ( uL != null ) && ( uL.size () > 0 ) )
        {
            uMap = new Map<String,User> () ;
            for ( User u : uL )
            {
                uMap.put ( u.Email, u ) ;
            }
        }
		
		return uMap ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Get list of User IDs belonging to a group by name
	 * ----------------------------------------------------------------------------------- */
	public static List<String> getUserIdsFromGroupName ( String groupName )
	{
		String groupId = getGroupId ( groupName ) ;
		
		return getUserIdsFromGroupId ( groupId ) ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Get list of User IDs belonging to a group by name
	 * ----------------------------------------------------------------------------------- */
	public static List<String> getUserIdsFromGroupId ( String groupId )
	{
		List<String> userIdList = null ;
			
		if ( String.isNotEmpty ( groupId ) )
		{
			List<GroupMember> gmList = [
				SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: groupId
			] ;
			
			if ( ( gmList != null ) && ( gmList.size () > 0 ) ) 
			{
				userIdList = new List<String> () ;
				
				for ( GroupMember gm : gmList )
				{
					if ( isUser ( gm.UserOrGroupId ) )
						userIdList.add ( gm.UserOrGroupId ) ;
				}
			}
		}
		
		return userIdList ;
	}
	
	/* -----------------------------------------------------------------------------------
	 *  Get Current Subscribers from an object
	 * ----------------------------------------------------------------------------------- */
	public static Map<String,String> getCurrentSubscribers ( String Id )
	{
		Map<String,String> userIdMap = null ;
		
		List<EntitySubscription> esL = [
			SELECT SubscriberId, ParentId
			FROM EntitySubscription
			WHERE ParentId = :Id 
			LIMIT 1000
		] ;
		
		if ( ( esL != null ) && ( esL.size () > 0 ) )
		{
			userIdMap = new Map<String, String> () ;
			
			for ( EntitySubscription e : esL )
			{
				userIdMap.put ( e.SubscriberId, e.ParentId ) ;
			}
		}
		
		return userIdMap ;
	}
    
	/* -----------------------------------------------------------------------------------
	 *  Get Current Subscribers from an object
	 * ----------------------------------------------------------------------------------- */
	public static Map<String,Map<String,String>> getCurrentSubscribers ( List<String> idL )
	{
        
		map<String,Map<String,String>> returnMap = new map<String,Map<String,String>> () ;
		
		List<EntitySubscription> esL = [
			SELECT SubscriberId, ParentId
			FROM EntitySubscription
			WHERE ParentId IN :idL
			LIMIT 1000
		] ;
		
		if ( ( esL != null ) && ( esL.size () > 0 ) )
		{
			for ( EntitySubscription e : esL )
			{
                Map<String,String> xMap ;
                if ( returnMap.containsKey ( e.ParentID ) )
                {
                    xMap = returnMap.get ( e.ParentId ) ;
                }
                else
                {
                    xMap = new Map<String,String> () ;
                    returnMap.put ( e.ParentId, xMap ) ;
                }
                
				xMap.put ( e.SubscriberId, e.ParentId ) ;
			}
		}
		
		return returnMap ;
	}
    
	/* -----------------------------------------------------------------------------------
	 *  Get Contact by name and email
	 * ----------------------------------------------------------------------------------- */
    public static Contact getContactByNameAndEmail ( String firstName, String lastName, String email )
    {
        list<Contact> cL = null ;
        Contact c = null ;
        
        try
        {
            cL = [
                SELECT ID, Name, Email
                FROM Contact
                WHERE Email = :email
                AND FirstName = :firstName
                AND LastName = :lastName
                ORDER BY ID
                DESC
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( cL != null ) && ( cL.size () > 0 ))
            c = cL [ 0 ] ;
        
        return c ;
    }
    
	/* -----------------------------------------------------------------------------------
	 *  Get Default account
	 * ----------------------------------------------------------------------------------- */
    public static Account getDefaultAccount ()
    {
        list<Account> aL = null ;
        Account a = null ;
        
        try
        {
            aL = [
                SELECT ID
                FROM Account
                WHERE Name = 'Insight Customers - Individuals'
                LIMIT 1
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( aL != null ) && ( aL.size () > 0 ) )
            a = aL [ 0 ] ;
        
        return a ;
    }
    
	/* -----------------------------------------------------------------------------------
	 *  Get Default business account
	 * ----------------------------------------------------------------------------------- */
    public static Account getDefaultBusinessAccount ()
    {
        list<Account> aL = null ;
        Account a = null ;
        
        // YES THERE IS A TYPO
        try
        {
            aL = [
                SELECT ID
                FROM Account
                WHERE Name = 'Insight Customers - Busineses'
                LIMIT 1
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( aL != null ) && ( aL.size () > 0 ) )
            a = aL [ 0 ] ;
        
        return a ;
    }
    
	/* -----------------------------------------------------------------------------------
	 *  Get Stack trace as string
	 * ----------------------------------------------------------------------------------- */
    public static String parseStackTrace ( Exception ex )
    {
        return parseStackTrace ( ex, null ) ;
    }
    
    public static String parseStackTrace ( Exception ex, Object o )
    {
        String error = '\n' ;
        
        if ( ex != null )
        {
            error += 'Exception :: ' + ex.getTypeName () + '\n' ;
            error += 'Message   :: ' + ex.getMessage () + '\n' ;
            error += 'Cause     :: ' + ex.getCause () + '\n' ;
            error += 'Type Name :: ' + ex.getTypeName() + '\n' ;
            error += 'Line #    :: ' + ex.getLineNumber () + '\n' ;
            
            error += '-------------------- STACK TRACE --------------------\n' ;
            error += ex.getStackTraceString () + '\n' ;
            error += '-----------------------------------------------------\n' ;
            
            if ( ex instanceof DMLException )
            {
                DMLException de = (DMLException) ex ;
                
	            error += '-------------------- DML RESULTS --------------------\n' ;
                for(integer i = 0; i < de.getNumDML(); i++)
                {
                    error += 'Item    : ' + de.getDMLIndex ( i ) + '\n' ;
                    error += 'field   : ' + de.getDmlFields ( i ) + '\n' ;
                    error += 'name    : ' + de.getDmlFieldNames ( i ) + '\n' ;
                    error += 'message : ' + de.getDMLMessage ( i ) + '\n' ;
                    error += 'status  : ' + de.getDmlStatusCode ( i ) + '\n' ;
                    error += 'type    : ' + de.getDmlType ( i ) + '\n' ;
                }
            	error += '-----------------------------------------------------\n' ;
            }
        }
        
        return error ;
    }
    
	/* -----------------------------------------------------------------------------------
	 *  Get Stack trace as string
	 * ----------------------------------------------------------------------------------- */
    public static void sendBatchHTMLEmail ( String to, String subject, String body )
    {
        sendBatchHTMLEmail ( new String[] { to }, subject, body ) ;
    }
    
    public static void sendBatchHTMLEmail ( String[] to, String subject, String body )
    {
        sendBatchHTMLEmail ( to, null, subject, body ) ;
    }
    
    public static void sendBatchHTMLEmail ( String[] to, String[] cc, String subject, String body )
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
        
        mail.setToAddresses ( to ) ;
        
        if ( cc != null )
            mail.setCcAddresses ( cc ) ;
        
        mail.setSenderDisplayName ( 'Batch Process' ) ;
        mail.setReplyTo ( 'noreply@oneunited.com' ) ;

        mail.setSubject ( subject ) ;
        
   		mail.setPlainTextBody ( '' ) ;  // WTB no "null"
        mail.setHtmlBody ( body ) ;
        
        mail.setUseSignature ( false ) ;
        
        if ( ! Test.isRunningTest () )
        	Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
    }
}