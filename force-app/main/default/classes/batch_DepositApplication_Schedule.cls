global class batch_DepositApplication_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_DepositApplication_Update b = new batch_DepositApplication_Update () ;
		Database.executeBatch ( b ) ;
   }    
}