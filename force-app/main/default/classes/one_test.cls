@isTest
public with sharing class one_test 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;

    public static void insertCustomSetting(){
    	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        
        insert s;
    }

    @isTest
    public static void test_one_application_send(){
        insertCustomSetting();
        one_application__c app = one_utils.getApplicationWithData(false);
        insert app;
        ApexPages.StandardController sc = new ApexPages.standardController(app);
        
        one_application_send controller = new one_application_send(sc);
        
        PageReference pr = controller.sendData();
        system.assert(pr != null);
    }
    
    @isTest
    public static void test_one_utils_getRoutingNumbers(){
        one_routing_number__c rn = new one_routing_number__c();
        rn.Bank_Name__c = 'BANK NAME';
        rn.Routing_Name__c = 'A ROUTING';
        insert rn;
        List<string> listNames = new List<string>();
        listNames.add(rn.Routing_Name__c);
        Map<string, one_routing_number__c> rnTest = one_utils.getRoutingNumbers(listNames);
        system.assert(rnTest.size() == 1);
        system.assert(rnTest.get(rn.Routing_Name__c).Bank_Name__c == rn.Bank_Name__c);
    }
    
    @isTest
    public static void test_one_utils_sendApplicationForm(){
        insertCustomSetting();
        one_application__c app = one_utils.getApplicationWithData(true);
        insert app;
        List<Id> lst = new List<Id>();
        lst.add(app.Id);
        one_Application.findContactsForApplications(lst);
        one_utils.sendApplicationForm(app);
        //Case c = [Select Id from Case Where one_application__c =: app.Id];
        system.assert(true);
    }
    
    @isTest
    public static void test_one_utils_getFixedApplication(){
        insertCustomSetting();
        one_application__c app = one_utils.getApplicationWithData(true);
        system.assert(one_utils.getFixedApplication(app, false) != null);
    }
    
    @isTest
    public static void test_one_utils_getRandomString ()
    {
        insertCustomSetting () ;
    	String thingy = one_utils.getRandomString ( 30 ) ;
    	
    	system.assertNotEquals ( thingy, null ) ;
    }
    
    @isTest
    public static void test_getQRCode () 
    {
        insertCustomSetting () ;
        
        String thingy = one_utils.getDocUploadQRCode ( '2132456', '654654654' ) ;
    	
    	system.assertNotEquals ( thingy, null ) ;
    }
    
    @isTest
    public static void test_one_utils_getDefaultCampaignID ()
    {
        insertCustomSetting () ;
    	String thingy = one_utils.getDefaultCampaignID () ;
    	
    	system.assertNotEquals ( thingy, null ) ;
    }
    
    @isTest
    public static void test_one_utils_getAffiliateAccountLookup ()
    {
        insertCustomSetting () ;
        
        Account a1 = new Account () ;
        a1.name = 'Test Parent Account' ;
        
        insert a1 ;
        
        Account a2 = new Account () ;
        a2.name = 'Test Account' ;
        a2.Affiliate_ID__c = '321654' ;
        a2.ParentId = a1.id ;
        
        insert a2 ;
        
    	String thingy = one_utils.getAffiliateAccountLookup ( '321654', a1.id ) ;
    	
    	system.assertNotEquals ( thingy, null ) ;
    }
    
    @isTest
    public static void test_one_application_success_controller(){
        insertCustomSetting();
        Date d = Date.today().addYears(-20);
        one_application__c app = new one_application__c();
        app.First_Name__c = 'Test';
        app.Last_Name__c = 'Test_2';
        app.DOB__c = d;
        app.Street_Address__c = 'test address';
        app.City__c = 'test city';
        app.State__c = 'AL';
        app.ZIP_Code__c = '12633';
        app.Employer__c = 'emp';
        app.Position_Occupation__c = 'positon';
        app.Gross_Income_Monthly__c = 33;
        app.Additional_Income_Y_N__c = 'Yes';
        app.Employment_How_Long_mths__c = 11;
        app.Employment_How_Long_yrs__c = 11;
        app.ReservationID__c = '132456789' ;
        insert app;
        
        system.assertNotEquals(app.id, null);

        System.currentPageReference().getParameters().put('type','1');
        System.currentPageReference().getParameters().put('aid',app.id);
        
        one_application_success_controller controller = new one_application_success_controller();

        system.assert(true);
    }
    
    @isTest
    public static void test_one_Application_yes(){
        insertCustomSetting();
        one_Application a = new one_Application();
        a.app = one_utils.getApplicationWithData(true);
        a.stepNumber = 1;
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.stepNumber = 2;
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.stepNumber = 3;
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.stepNumber = 4;
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.stepNumber = 5;
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.stepNumber = 6;
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.jumpStep = '2';
        a.jumpToStep();
        a.jumpToStepExtra();
        a.goPreviousStep();
        a.sendApplicationWebService();
        a.goToErrorPage1();
        a.goToErrorPage2();
        a.goToSuccessPage();
        one_Application.findRoutingNumberRecords3('asdsadsada');
        system.assert(a != null);
    }
    @isTest
    public static void test_one_Application_no(){
        insertCustomSetting();
        one_Application a = new one_Application();
        one_application__c app = one_utils.getApplicationWithData(false);
        //app.SSN__c = '1234567';
        a.app = app;
        app.Employer_ZIP_Code__c = null;
        a.in_Page_variables = ',Mailing_ZIP_Code__c,1,Employer_ZIP_Code__c,1';
        a.stepNumber = 1;
        a.addOrRemoveFieldsInPage();
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.goNextStep();
        a.addOrRemoveFieldsInPage();
        a.goNextStep();
        a.app_isACHselected = true;
        a.addOrRemoveFieldsInPage();
        a.jumpStep = '2';
        a.jumpToStep();
        a.jumpToStepExtra();
        a.goPreviousStep();
        a.sendApplicationWebService();
        a.goToErrorPage1();
        a.goToErrorPage2();
        a.goToSuccessPage();
        one_Application.findRoutingNumberRecords3('asdsadsada');
        system.assert(a != null);
    }
    
    @isTest
    public static void test_one_Application_previous(){
        insertCustomSetting();
        one_Application a = new one_Application();
        a.stepNumber = 1;
        a.jumpStep = '3';
        a.skipStepA = 1;
        a.jumpToStep();
        system.assert(a != null);
    }
    
     @isTest
    public static void test_one_Application_sendData(){
        wwwExperianComFis.FaultDetail fd = new wwwExperianComFis.FaultDetail();
        wwwExperianComFis.Codes c = new wwwExperianComFis.Codes();
        wwwExperianComFis.ScoreRsn sr = new wwwExperianComFis.ScoreRsn();
        wwwExperianComFis.RushCard rc = new wwwExperianComFis.RushCard();
        wwwExperianComFis.PrimaryRsnCode prc = new wwwExperianComFis.PrimaryRsnCode();
        wwwExperianComFis.BureauResponse br = new wwwExperianComFis.BureauResponse();
        wwwExperianComFis.Officer o = new wwwExperianComFis.Officer();
        wwwExperianComFis.AutoPay ap = new wwwExperianComFis.AutoPay();
        wwwExperianComFis.ATM atm = new wwwExperianComFis.ATM();
        wwwExperianComFis.LOSRequest lr = new wwwExperianComFis.LOSRequest();
        wwwExperianComFis.Fault f = new wwwExperianComFis.Fault();
        insertCustomSetting();
        one_application__c app = one_utils.getApplicationWithData(false);
        insert app;
        ApexPages.StandardController appStd = new ApexPages.StandardController(app);
        one_application_send a = new one_application_send(appStd );
        a.sendData();
        system.assert(a != null);
    }
    
    @isTest
    public static void test_one_certificates()
    {
		one_CertificateApplication__c oc = new one_CertificateApplication__c();
		oc.Certificate__c = 'xxxxxx';
		oc.Active__c = true;
		oc.Password__c = '88889999';
		insert oc;
		system.assert(oc.Id != null);
		
		one_CertificateApplication__c oc2 = new one_CertificateApplication__c();
		oc2.Certificate__c = 'xxxxxx';
		oc2.Active__c = true;
		oc2.Password__c = '88889999';
		insert oc2;
		
		delete oc;
    }
    
    @isTest
    public static void test_one_Application_new_session_with_referral ()
    {
        insertCustomSetting () ;
        
		//  Point to page to test
		PageReference pr = Page.one_application_start ;
		Test.setCurrentPage ( pr ) ;
		
		Test.StartTest();
		
		//  Pass in parameters manually
		ApexPages.currentPage().getParameters().put ( 'cid', '321654987' );
		ApexPages.currentPage().getParameters().put ( 'cvid', '001R000000peKIS' );
		ApexPages.currentPage().getParameters().put ( 'kid', APPLICATION_ADVANCED_MODE );

		//  Get application		
        one_application__c a = new one_application__c () ;
        
		Test.StopTest();
    }
    
    @isTest
    public static void test_additional_doc_language_etc ()
    {
    	List<String> thingy = one_utils.getDocTypeList ( 'P006' ) ;
    	map<String,String> cM = one_utils.codesAdditionalLanguage ;
    	List<SelectOption> cL = one_utils.docTypeItems ;
    }
    
    @isTest
    public static void test_one_application_prior_applications_no_CoApplicant ()
    {
        insertCustomSetting () ;
    	
        one_Application__c a = one_utils.getApplicationWithData ( false ) ;
        
        insert a ;
        
        boolean hasApp ;
        List<String> sId ;

        //  Applicant only
        hasApp = one_utils.hasExistingApplications ( '123-45-6789' ) ;
        system.assert ( hasApp == true ) ;
        
        // No match
        hasApp = one_utils.hasExistingApplications ( '123-54-6789' ) ;
        system.assert ( hasApp == false ) ;
    }
    
    @isTest
    public static void test_one_application_prior_applications_CoApplicant ()
    {
        insertCustomSetting () ;
    	
        one_Application__c a = one_utils.getApplicationWithData ( true ) ;
        
        insert a ;
        
        boolean hasApp ;
        List<String> sId ;

        //  Applicant only, doesn't match
        hasApp = one_utils.hasExistingApplications ( '123-45-6789' ) ;
        system.assert ( hasApp == false ) ;
        
        // Co-Applicant only, doesn't match
        hasApp = one_utils.hasExistingApplications ( '987-65-4321' ) ;
        system.assert ( hasApp == false ) ;
        
        // Applicant & Co-applicant
        hasApp = one_utils.hasExistingApplications ( '123-45-6789', '987-65-4321' ) ;
        system.assert ( hasApp == true ) ;
        
        // Applicant & Co-applicant reverssed
        hasApp = one_utils.hasExistingApplications ( '987-65-4321','123-45-6789' ) ;
        system.assert ( hasApp == false ) ;
    }
    
    @isTest
    public static void test_other_random_things ()
    {
    	Test.startTest () ;
    	
        insertCustomSetting () ;
    	
		//  Point to page to test
		PageReference pr1 = Page.one_application_start ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'scode', 'xyz' ) ;
        ApexPages.currentPage().getParameters().put ( 'refcnt', 'abc' ) ;
        ApexPages.currentPage().getParameters().put ( 'entcnt', '123' ) ;
        ApexPages.currentPage().getParameters().put ( 'kid', APPLICATION_ADVANCED_MODE ) ;
        
        one_Application oApp = new one_Application () ;
        
        oApp.checkHTTSconnection () ;
        
        Test.stopTest () ;
    }
}