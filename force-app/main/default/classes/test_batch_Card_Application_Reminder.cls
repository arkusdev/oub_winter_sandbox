@isTest
public with sharing class test_batch_Card_Application_Reminder 
{
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
    public static testmethod void testOne ()
    {
		setBatchSettings () ;
		
    	String xID = OneUnitedUtilities.getRecordTypeIdForObject ( 'ATM Card Request', 'Ticket__c' ) ;
    	
    	Ticket__c t = new Ticket__c () ;
    	t.RecordTypeID = xID ;
    	t.Status__c = 'Clarification' ;
    	t.Last_Status_Change__c = Date.Today().addDays(-14) ;
    
    	insert t ;
    	
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_Card_Application_Reminder b = new batch_Card_Application_Reminder () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
    }
}