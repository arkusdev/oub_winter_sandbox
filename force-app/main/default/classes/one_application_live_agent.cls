public class one_application_live_agent 
{
    public String liveAgentJSURL { public get; private set; }
    public String liveAgentChatURL { public get; private set; }
    public String liveAgentCompanyID { public get; private set; }
    public String liveAgentDeploymentID { public get; private set; }
    
    public boolean liveAgentActive { public get ; private set ; }
    
    //  --------------------------------------------------------------------------------------------------
    //  Live Agent setup
    //  --------------------------------------------------------------------------------------------------\
	public one_application_live_agent ()
    {
        liveAgentActive = true ;
        
        one_settings__c settings = one_settings__c.getInstance ( 'settings' ) ;
        
        if ( settings != null )
        {
            if ( settings.Live_Agent_Enable__c == true )
            {
                if ( settings.Live_Agent_JS_URL__c != null )
                    liveAgentJSURL = settings.Live_Agent_JS_URL__c ;
                else
                    liveAgentActive = false ;
                
                if ( settings.Live_Agent_Chat_URL__c != null )
                    liveAgentChatURL = settings.Live_Agent_Chat_URL__c ;
                else
                    liveAgentActive = false ;
                
                if ( settings.Live_Agent_Company_ID__c != null )
                    liveAgentCompanyID = settings.Live_Agent_Company_ID__c ;
                else
                    liveAgentActive = false ;
                
                if ( settings.Live_Agent_Deployment_ID__c != null )
                    liveAgentDeploymentID = settings.Live_Agent_Deployment_ID__c ;
                else
                    liveAgentActive = false ;
            }
            else
            {
                liveAgentActive = false ;
            }
        }
        else
        {
            liveAgentActive = false ;
        }
    }
}