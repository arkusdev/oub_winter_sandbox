@isTest
public class TestCorporateNewsExtender 
{
	/* --------------------------------------------------------------------------------
	 * User
	 * -------------------------------------------------------------------------------- */
	private static User getUser ()
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test1' ;
		u.lastName = 'User1' ;
		u.email = 'test1@user.com' ;
		u.username = 'test1user1@user.com' ;
		u.alias = 'tuser1' ;
		u.CommunityNickname = 'tuser1' ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		return u ;
	}
    
	/* --------------------------------------------------------------------------------
	 * Timesheet?
	 * -------------------------------------------------------------------------------- */
    private static Corporate_News__c getCorporateNews (  )
    {
        Corporate_News__c te = new Corporate_News__c () ;
        te.Publish_Date__c = Date.today () ;
        
        return te ;
    }
    
	/* --------------------------------------------------------------------------------
	 * TEST!
	 * -------------------------------------------------------------------------------- */
	static testmethod void INONE ()
	{
        //  Setup
        User u1 = getUser () ;
        insert u1 ;
        
        Corporate_News__c te = getCorporateNews (  ) ;
        insert te ;
        
		//  Point to page to test
		PageReference pr1 = Page.OneUnited_News ;
		Test.setCurrentPage ( pr1 ) ;
		
        //  Go!
        Test.startTest () ;
        
		System.runAs ( u1 )
		{
            
            //  Create class
            Corporate_News_Extender cce = new Corporate_News_Extender (  ) ;
            
            //  Test?
            String foo = cce.getCorporateNewsId () ;
            
            
        }
        
		//  End of testing
		Test.stopTest () ;
    }
    
}