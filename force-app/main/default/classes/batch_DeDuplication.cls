global class batch_DeDuplication implements Database.Batchable<SObject>, Database.Stateful
{
	private Batch_Process_Settings__c bps ;

    private static final String JOB_TYPE = 'Salesforce Batch Apex' ;
    private static final String JOB_NAME = batch_DeDuplication.class.getName () ;
    global String status ;

	global batch_DeDuplication ()
	{
        status = 'WARN' ;
        
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query ;
		
		query = '' ;
		query += 'SELECT ID, RecordType.Name ' ;
		query += 'FROM Contact ' ;
		query += 'WHERE DeDuplication_Flag__c = true ' ;
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
    }
    
	global void execute ( Database.Batchablecontext bc, List<Contact> incL )
	{
        list<ID> pL = new list<ID> () ;
        list<ID> cL = new list<ID> () ;
        list<ID> xL = new list<ID> () ;
        
        if ( ( incL != null ) && ( incL.size () > 0 ) )
        {
            for ( Contact c : incL )
            {
                xL.add ( c.ID ) ;
                
                if ( c.RecordType.Name.startsWithIgnoreCase ( 'C' ) )
                    cL.add ( c.ID ) ;
                else if ( c.RecordType.Name.startsWithIgnoreCase ( 'P' ) )
                    pL.add ( c.ID ) ;
            }
            
            if ( ( cL != null ) && ( cL.size () > 0 ) )
	            DeDuplicationActionFunctionExpanded.dedupeContactCustomer ( cL ) ;
            
            if ( ( pL != null ) && ( pL.size () > 0 ) )
	            DeDuplicationActionFunctionExpanded.dedupeContactProspect ( pL ) ;
        }
        
        list<Contact> maybeL = new list<Contact> () ;
        
        try
        {
            maybeL = [
                SELECT ID
                FROM Contact
                WHERE ID IN :xL 
            ] ;
            
            if ( ( maybeL != null ) && ( maybeL.size () > 0 ) ) 
            {
                for ( Contact c : maybeL )
                {
                    c.DeDuplication_Flag__c = false ;
                }
            }
            
            update maybeL ;
        }
        catch ( DMLException e )
        {
    	    status = 'ERROR' ;
        }
        
        status = 'GOOD' ;
    }
    
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;

		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
        if ( ( a.Status.equalsIgnoreCase ( 'Failed' ) ) || ( a.NumberOfErrors > 0 ) )
            status = 'ERROR' ;
        else if ( a.Status.equalsIgnoreCase ( 'Aborted' ) )
            status = 'WARN' ;
        else if ( ( a.TotalJobItems == 0 ) && ( a.NumberOfErrors == 0 ) )
            status = 'GOOD' ;
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( '[' + status + '][' + JOB_NAME + '][' + JOB_TYPE + '] Batch Contact DeDuplication' ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
        if ( bps != null )
        {
            if ( ( bps.Batch_DeDeduplication_Count__c != null ) && ( bps.Batch_DeDeduplication_Count__c > 0 ) )
                message += '\nIndividual batches were limited to ' + bps.Batch_DeDeduplication_Count__c + ' records.\n' ;
        }
        
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}