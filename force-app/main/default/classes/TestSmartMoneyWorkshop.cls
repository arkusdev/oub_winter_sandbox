@isTest (SeeAllData=true)
public with sharing class TestSmartMoneyWorkshop 
{
	
	private static Branch__c getBranch ()
	{
		return getBranch ( 'MA ' ) ;
	}

	private static Branch__c getBranch ( String state )
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = state ;
		
		insert b ;
		
		return b ;
	}
	
	private static Workshop__c setupWorkshop ()
	{
		return setupWorkshop ( 'MA', 'RSVP' ) ;	
	}
	
	private static Workshop__c setupWorkshop ( String state, String xtype )
	{
		Branch__c b = getBranch () ;
		
		Workshop__c w = new Workshop__c () ;
		
		w.Description__c = 'Test Workshop' ;
		w.Title__c = 'Test Workshop' ;
		w.Event_Start__c = System.now () ;
		w.Event_End__c = System.now () ;
		w.Name = 'Test Workshop' ;
		w.Branch__c = b.ID ;
		w.Public_Event__c = true ;
		w.State__c = state ;
		w.Type__c = xType ;
		
		insert w ;
		
		return w ;
	}

	private static Contact setupContact () 
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'something@other.com' ;
		
		insert c ;
		
		return c ;
	}
	
	static testmethod void testNoWID ()
	{
		//  Point to page to test
		PageReference pr1 = Page.SmartMoneyWorkshop ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        SmartMoneyWorkshop smw = new SmartMoneyWorkshop ( sc ) ;
        
        //  Check drop down ?
        List<SelectOption> thingy = smw.getItems () ;
        
        //  Dropdown count
		System.assertEquals ( thingy.size(), 14 ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1500 ) ;

		//  End of testing
		Test.stopTest () ;
	}

	static testmethod void testNoExistingContactNoUTM ()
	{
		//  Load test workshop
		Workshop__c w = setupWorkshop () ;
		
		//  Point to page to test
		PageReference pr1 = Page.SmartMoneyWorkshop ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        SmartMoneyWorkshop smw = new SmartMoneyWorkshop ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1000 ) ;
		
		//  Empty websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1001 ) ;
		System.assertEquals ( smw.e_firstName, true ) ;
		System.assertEquals ( smw.e_lastName, true ) ;
		System.assertEquals ( smw.e_phone, true ) ;

		//  Fill out form bad emails
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'f@y' ;
		smw.cEmail = 'something@else.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		
		//  Incorrect websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1001 ) ;		
		System.assertEquals ( smw.e_firstName, false ) ;		
		System.assertEquals ( smw.e_lastName, false ) ;		
		System.assertEquals ( smw.e_email1, true ) ;
		System.assertEquals ( smw.e_emailDiff, true ) ;
		System.assertEquals ( smw.e_phone, false ) ;
		
		//  Fill out form incorrect emails
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.cEmail = 'something@else.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		
		//  Incorrect websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1001 ) ;		
		System.assertEquals ( smw.e_firstName, false ) ;		
		System.assertEquals ( smw.e_lastName, false ) ;		
		System.assertEquals ( smw.e_email1, false ) ;
		System.assertEquals ( smw.e_emailDiff, true ) ;
		System.assertEquals ( smw.e_phone, false ) ;
		
		//  Fill out form bad phone
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.cEmail = 'something@other.com' ;
		smw.wa.Phone_Number__c = '123456x890' ;
		
		//  Incorrect websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1001 ) ;		
		System.assertEquals ( smw.e_firstName, false ) ;		
		System.assertEquals ( smw.e_lastName, false ) ;		
		System.assertEquals ( smw.e_email1, false ) ;
		System.assertEquals ( smw.e_emailDiff, false ) ;
		System.assertEquals ( smw.e_phone, true ) ;
		
		//  Fill out form correctly
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.cEmail = 'something@other.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		
		//  Correct websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 2000 ) ;		

		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testNoExistingContactUTM ()
	{
		//  Load test workshop
		Workshop__c w = setupWorkshop () ;
		
		//  Point to page to test
		PageReference pr1 = Page.SmartMoneyWorkshop ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        SmartMoneyWorkshop smw = new SmartMoneyWorkshop ( sc ) ;
		
		//  Random UTM crap
		smw.utmCampaign = 'a' ;
		smw.utmContent = 'b' ;
		smw.utmMedium = 'c' ;
		smw.utmSource = 'd' ;
		smw.utmTerm = 'e' ;
		smw.utmVisitorId = 'f' ;
        
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1000 ) ;
		
		//  Empty websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1001 ) ;
		System.assertEquals ( smw.e_firstName, true ) ;
		System.assertEquals ( smw.e_lastName, true ) ;
		System.assertEquals ( smw.e_phone, true ) ;

		//  Fill out form incorrect emails
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.cEmail = 'something@else.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		
		//  Incorrect websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 1001 ) ;
		System.assertEquals ( smw.e_firstName, false ) ;
		System.assertEquals ( smw.e_lastName, false ) ;
		System.assertEquals ( smw.e_email1, false ) ;	
		System.assertEquals ( smw.e_emailDiff, true ) ;
		System.assertEquals ( smw.e_phone, false ) ;
		
		//  Fill out form correctly
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		smw.cEmail = 'something@other.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		
		//  Correct websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 2000 ) ;		

		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testExistingContactNoUTM ()
	{
		//  Load test workshop
		Workshop__c w = setupWorkshop () ;
		
		//  Load test Contact
		Contact c = setupContact () ;
		
		//  Point to page to test
		PageReference pr1 = Page.SmartMoneyWorkshop ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        SmartMoneyWorkshop smw = new SmartMoneyWorkshop ( sc ) ;

		//  Fill out form correctly
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		smw.cEmail = 'something@other.com' ;
		
		//  Random UTM crap
		smw.utmCampaign = 'a' ;
		smw.utmContent = 'b' ;
		smw.utmMedium = 'c' ;
		smw.utmSource = 'd' ;
		smw.utmTerm = 'e' ;
		smw.utmVisitorId = 'f' ;
		
		//  Correct websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testExistingContactUTM ()
	{
		//  Load test workshop
		Workshop__c w = setupWorkshop () ;
		
		//  Load test Contact
		Contact c = setupContact () ;
		
		//  Point to page to test
		PageReference pr1 = Page.SmartMoneyWorkshop ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        SmartMoneyWorkshop smw = new SmartMoneyWorkshop ( sc ) ;

		//  Fill out form correctly
		smw.wa.First_Name__c = 'Test' ;
		smw.wa.Last_Name__c = 'User' ;
		smw.wa.Email_Address__c = 'something@other.com' ;
		smw.cEmail = 'something@other.com' ;
		smw.wa.Phone_Number__c = '1234567890' ;
		
		//  Random UTM crap
		smw.utmCampaign = 'a' ;
		smw.utmContent = 'b' ;
		smw.utmMedium = 'c' ;
		smw.utmSource = 'd' ;
		smw.utmTerm = 'e' ;
		smw.utmVisitorId = 'f' ;
		
		//  Correct websiteform test
		smw.submit () ;
		
		//  Check the resulting controller object
		System.assertEquals ( smw.fStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
}