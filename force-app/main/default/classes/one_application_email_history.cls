public with sharing class one_application_email_history 
{
    private static final String API_KEY = 'o-zuUl0_dDiq6OT87S1Agg' ;
    
    public one_application__c o { get; private set; }
    public List<MandrillJSONSearchResponse> mjrList { get ; private set; } 
    
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public one_application_email_history ( ApexPages.Standardcontroller stdC )
    {
        //  Get ID that is passed in, use it to get email address
        String appID = ((one_application__c) stdC.getRecord ()).id ;
        
        if ( appID != null )
        {
            System.debug ( '========== Getting application Info ==========' ) ;
            o = [ SELECT Id, Name, First_Name__c, Last_Name__c, Email_Address__c, Co_Applicant_Email_Address__c, RecordType.DeveloperName 
                  FROM one_application__c 
                  WHERE Id = :appID ] ;
            
            if ( o != null )
            {
                String rType = 'UNITY_Visa' ;
                RecordType rt = o.RecordType ;
                if ( String.isNotBlank ( rt.DeveloperName ) )
                    rType = rt.DeveloperName ;
                
                String apiCall = emailJSONQuery ( o.Email_Address__c, o.Co_Applicant_Email_Address__c, rType ) ;
                System.debug ( 'JSON --> ' + apiCall ) ;
                
                String response = sendHTTPRequest ( apiCall ) ;
                
                if ( String.isNotBlank ( response ) )
                {
                    System.debug ( response ) ;
                    mjrList = parseJSONRequest ( response ) ;
                }
            }
        }
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Construct the JSON query
     *  -------------------------------------------------------------------------------------------------- */
    private static String emailJSONQuery ( String email, String cEmail, String rType )
    {
        System.debug ( '========== Generating JSON Object ==========' ) ;
        
        //  Set up the generator
        JSONGenerator g = JSON.createGenerator ( true ) ;
        
        //  Create the object
        g.writeStartObject () ;
        
        //  API KEY
        g.writeStringField ( 'key', API_KEY ) ;
        
        //  Query
        if ( String.isNotBlank ( cEmail ) ) 
        	g.writeStringField ( 'query', 'email:' + email + ' OR email:' + cEmail ) ;
        else
        	g.writeStringField ( 'query', 'email:' + email ) ;
        
        //  FROM Block
        g.writeFieldName ( 'senders' ) ;
        g.writeStartArray () ;

        //  Message contents
        if ( rType.equalsIgnoreCase ( 'UNITY_Visa' ) )
	        g.writeString ( 'noreplies@unityvisa.com' ) ;
        else
	        g.writeString ( 'noreplies@oneunited.com' ) ;
        
        //  END FROM Block
        g.writeEndArray () ;
        
		//  Date time        
		Datetime todayDate = Datetime.now () ;
		String todayDateS = todayDate.format ( 'YYYY-MM-dd' ) ;

 		Datetime priorAgoDate = todayDate.addMonths ( -2 ) ;
		String priorAgoDateS = priorAgoDate.format ( 'YYYY-MM-dd' ) ;

        g.writeStringField ( 'date_from', priorAgoDateS ) ;
        g.writeStringField ( 'date_to', todayDateS ) ;
        
        //  End of object
        g.writeEndObject () ;
        
        return g.getAsString () ;
    }   
     
    /*  --------------------------------------------------------------------------------------------------
     *  Sends the JSON Request
     *  -------------------------------------------------------------------------------------------------- */   
    private static String sendHTTPRequest ( String js )
    {
        String response = null ;
        
        System.debug ( '========== Setting up HTTP Request Object ==========' ) ;
        HttpRequest request = new HttpRequest() ;

        request.setEndPoint ( 'https://mandrillapp.com/api/1.0/messages/search.json' ) ;
        request.setMethod ( 'POST' ) ;
        request.setHeader ( 'Content-Type', 'application/json' ) ;
        request.setBody ( js ) ;
        request.setTimeout ( 30000 ) ;

        System.debug ( '========== Calling method ==========' ) ;
        Http http = new Http () ;
        HTTPResponse res = null ;
        try
        {
            res = http.send ( request ) ;
        }
        catch ( System.CalloutException e )
        {
            System.debug ( 'HTTP Request fail. :(' ) ;
	        System.debug ( '========== ERROR ==========' ) ;
            System.debug ( e ) ;
    	    System.debug ( '========== ERROR ==========' ) ;
        }

        System.debug ( '========== Returning response ==========' ) ;
        if ( res != null )
            response = res.getBody () ;
            
        return response ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Parse the return JSON query
     *  -------------------------------------------------------------------------------------------------- */
    private static List<MandrillJSONSearchResponse> parseJSONRequest ( String js )
    {
        JSONParser parser = JSON.createParser ( js ) ;
        
        List<MandrillJSONSearchResponse> mjList = new List<MandrillJSONSearchResponse> () ;
        while ( parser.nextToken() != null ) 
        {
            if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
            {
            	MandrillJSONSearchResponse mjr = (MandrillJSONSearchResponse) parser.readValueAs ( MandrillJSONSearchResponse.class ) ;

            	mjList.add ( mjr ) ;
            }
        }
        
        return mjList ;
    } 
    
    /*  --------------------------------------------------------------------------------------------------
     *  Classes used to parse JSON
     *  -------------------------------------------------------------------------------------------------- */
    public class MandrillJSONSearchResponse
    {
        public Long ts { get; set; }
        public String state { get; set; }
        public String subject { get; set; }
        public String email { get; set; }
        public String[] tags { get; set; }
        public Integer opens { get; set; }
        public Integer clicks { get; set; }
        public MandrillJSONSearchResponseSMTPEvents[] smtp_events { get; set; }
        
        public String subaccount { get; set; }
        public String[] resends { get; set; }
//		public String _id ;  ** BROKEN - Apex can't handle variables starting with _ **
        
        public String sender { get; set; }
        public String template { get; set; }
        
//		public MandrillJSONSearchResponseMetadata[] metadata { get; set; }  ** BROKEN - Mandrill only returns an array some of the time! **
        
        public MandrillJSONSearchResponseOpens[] opens_detail { get; set; }
        public MandrillJSONSearchResponseClicks[] clicks_detail { get; set; }
        
        public String getTS
        {
        	get
        	{
	        	return datetime.newInstance ( ts * 1000 ).format ( 'MM/dd/yyyy hh:mm:ss a' ) ;
        	}
        }
    }
    
    public class MandrillJSONSearchResponseSMTPEvents
    {
        public Integer ts { get; set; }
        public String type { get; set; }
        public String diag { get; set; }
        public String source_ip { get; set; }
        public String destination_ip { get; set; }
        public Integer size { get; set; }
    }
    
/*  BROKEN - SEE ABOVE!
    public class MandrillJSONSearchResponseMetadata
    {
        public String salesforceApplicationId { get; set; }
        public String FISAppId { get; set; }
        public String salesforceContactId { get; set; }
    }
*/

    public class MandrillJSONSearchResponseOpens
    {
        public Integer ts { get; set; }
        public String ip { get; set; }
        public String location { get; set; }
        public String ua { get; set; }
    }

    public class MandrillJSONSearchResponseClicks
    {
        public Integer ts { get; set; }
        public String url { get; set; }
        public String ip { get; set; }
        public String location { get; set; }
        public String ua { get; set; }
    }
}