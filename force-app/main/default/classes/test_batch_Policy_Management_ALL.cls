@isTest
public with sharing class test_batch_Policy_Management_ALL 
{
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
	private static User getUser ( String thingy )
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
		u.firstName = 'Test' + thingy ;
		u.lastName = 'User' + thingy ;
		u.email = 'test' + thingy + '@user.com' ;
		u.username = 'test' + thingy + 'user' + thingy + '@user.com' ;
		u.alias = 'tuser' + thingy ;
		u.CommunityNickname = 'tuser' + thingy ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
		
		insert u ;
		
		return u ;
	}
	
	private static Policy_Management__c getPolicy ( User u, Date d )
	{
		Policy_Management__c pm = new Policy_Management__c () ;
		
		pm.Name = 'Test Policy' ;
		pm.OwnerId = u.ID ;
		pm.Review_Frequency__c = 'Annual' ;
		pm.Last_Board_Approval_Date__c = d ;
		
		return pm ;
	}
	
    public static testmethod void testMonthlyStatus ()
    {
		setBatchSettings () ;
		
    	User u1 = getUser ( '1' ) ;
		Date d = Date.today ().addYears ( -1 ).addDays ( 10 ) ;
		System.debug ( 'DATE:: '+ d ) ;

		Policy_Management__c pm = getPolicy ( u1, d ) ;
		insert pm ;
		
		Test.startTest () ;
		
		batch_PM_Monthly_Status bms = new batch_PM_Monthly_Status () ;
		Database.executeBatch ( bms ) ;
		
		Test.stopTest () ;
    }
    
    public static testmethod void test60DayWarning ()
    {
		setBatchSettings () ;
		
    	User u1 = getUser ( '1' ) ;
		Date d = Date.today ().addYears ( -1 ).addDays ( 61 ) ;
		System.debug ( 'DATE:: '+ d ) ;

		Policy_Management__c pm = getPolicy ( u1, d ) ;
		insert pm ;
		
		Test.startTest () ;
		
		batch_PM_60_Day_Warning b60 = new batch_PM_60_Day_Warning () ;
		Database.executeBatch ( b60 ) ;
		
		Test.stopTest () ;
    }
}