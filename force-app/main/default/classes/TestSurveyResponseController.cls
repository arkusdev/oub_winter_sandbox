@isTest
public with sharing class TestSurveyResponseController 
{
	private static OneSurvey__c getSurvey ()
	{
		OneSurvey__c os = new OneSurvey__c () ;
		os.Active__c = true ;
		os.Feedback_Type__c = '1 - 5' ;
		os.Start_Date__c = Date.today () -1 ;
		os.End_Date__c = Date.today () +1 ;
		
		return os ;
	}
	
	private static Web_Settings__c createCustomSettings ( String surveyID )
	{
        //  Settings
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.Default_Survey_ID__c = surveyID ;
        ws.Survey_Answers__c = 'Rating_1_5__c' ;
        
        insert ws ;
		return ws ;
	}
	
	private static Branch__c getBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		insert b ;
		
		return b ;
	}
	
	private static Contact getContact ( String thingy )
	{
		Branch__c b = getBranch () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' + thingy ;
		c.LastName = 'Person' + thingy ;
		c.Email = 'test' + thingy +'.person' + thingy + '@testing' + thingy + '.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.Branch__c = b.ID ;
		
		insert c ;
		
		return c ;		
	}
	
	public static testmethod void test_EVERYTHING_EMPTY ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Fail page?
		System.assertEquals ( src.step, 9999 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_NO_SURVEY_BAD_DEFAULT_SURVEY ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Load survey into custom settings
		Web_Settings__c ws = createCustomSettings ( null ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Fail page?
		System.assertEquals ( src.step, 9999 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_NO_SURVEY_DEFAULT_SURVEY ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create a survey
		OneSurvey__c os = getSurvey () ;
        insert os ;
		
		//  Load survey into custom settings
		Web_Settings__c ws = createCustomSettings ( os.ID ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  Default page?
		System.assertEquals ( src.step, 1000 ) ;	
		
		//  Updates
		src.UTM_Campaign = 'A' ;
		src.sr.Rating_1_5__c = '2' ;
		src.sr.Feedback__c = 'lalala' ;
		
		//  submit
		src.submit () ;
		
		//  Success page?
		System.assertEquals ( src.step, 5000 ) ;
		
		//  Check for updates
		Survey_Response__c srNEW = [
			SELECT ID,Feedback__c,Rating_1_5__c, UTM_Campaign__c
			FROM Survey_Response__c
			WHERE ID = :src.sr.ID
		]  ;
		
		System.assertEquals ( srNEW.Rating_1_5__c, '2' ) ;
		System.assertEquals ( srNEW.Feedback__c, 'lalala' ) ;
		System.assertEquals ( srNEW.UTM_Campaign__c, 'A' ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_PASS_BAD_SURVEY_BAD_DEFAULT ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Load survey into custom settings
		Web_Settings__c ws = createCustomSettings ( null ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameter(s)
        ApexPages.currentPage().getParameters().put ( 'Survey__c', 'NYETNEINNON' );

		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Fail page?
		System.assertEquals ( src.step, 9999 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_PASS_BAD_SURVEY ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create a survey
		OneSurvey__c os = getSurvey () ;
        insert os ;
		
		//  Load survey into custom settings
		Web_Settings__c ws = createCustomSettings ( os.ID ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameter(s)
        ApexPages.currentPage().getParameters().put ( 'Survey__c', 'NYETNEINNON' );

		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Fail page?
		System.assertEquals ( src.step, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_PASS_ONLY_SURVEY ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create a survey
		OneSurvey__c os = getSurvey () ;
        insert os ;

		//  Load survey into custom settings
		Web_Settings__c ws = createCustomSettings ( os.ID ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameter(s)
        ApexPages.currentPage().getParameters().put ( 'Survey__c', os.id );
        
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Initial page?
		System.assertEquals ( src.step, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_PASS_SURVEY_UND_CRAP ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create a survey
		OneSurvey__c os = getSurvey () ;
        insert os ;
		
		//  Load survey into custom settings
		Web_Settings__c ws = createCustomSettings ( os.ID ) ;
		
		//  Get a contact
		Contact cA = getContact ( 'A' ) ; 
				
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameter(s)
        ApexPages.currentPage().getParameters().put ( 'Survey__c', os.id );
        ApexPages.currentPage().getParameters().put ( 'Rating_1_5__c', '3' );
        ApexPages.currentPage().getParameters().put ( 'contact__c', cA.id );
        ApexPages.currentPage().getParameters().put ( 'uid', 'something' );
        
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Initial page?
		System.assertEquals ( src.step, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	public static testmethod void test_PASS_SURVEY_UND_BAD_CRAP ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create a survey
		OneSurvey__c os = getSurvey () ;
        insert os ;
		
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameter(s)
        ApexPages.currentPage().getParameters().put ( 'Survey__c', os.id );
        ApexPages.currentPage().getParameters().put ( 'contact__c', 'nyahnyahnyah' );
        
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
		//  Initial page?
		System.assertEquals ( src.step, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}

	public static testmethod void test_PASS_SURVEY_UND_CRAP_SUBMIT ()
	{
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Create a survey
		OneSurvey__c os = getSurvey () ;
        insert os ;
		
		//  Get a contact
		Contact cA = getContact ( 'A' ) ; 
				
		//  Point to page to test
		PageReference pr1 = Page.SurveyResponsePage ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Pass in parameter(s)
        ApexPages.currentPage().getParameters().put ( 'Survey__c', os.id );
        ApexPages.currentPage().getParameters().put ( 'Rating_1_5__c', '1' );
        ApexPages.currentPage().getParameters().put ( 'contact__c', cA.id );
        
		//  Empty response?
		Survey_Response__c sr = new Survey_Response__c () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( sr ) ;
		
		//  Create controller
		SurveyResponseController src = new SurveyResponseController ( sc ) ;
		
		//  DML!
		src.setupSurveyResponse () ;
		
        // RANDOM CRAP
        list<SelectOption> foo1 = src.rating_1_10 ;
        list<SelectOption> foo2 = src.rating_1_5 ;
        list<SelectOption> foo3 = src.rating_y_n ;
        list<SelectOption> foo4 = src.survey_response ;
        
		//  Initial page?
		System.assertEquals ( src.step, 2000 ) ;
		
		String srID = src.sr.ID ;
		
		//  Check for initials
		Survey_Response__c srNEW1 = [
			SELECT ID,Rating_1_5__c,Testimonial_Disclosure__c,Contact__c
			FROM Survey_Response__c
			WHERE ID = :srID 
		]  ;
		
		System.assertEquals ( srNEW1.Rating_1_5__c, '1' ) ;
		System.assertEquals ( srNEW1.Testimonial_Disclosure__c, false ) ;
		System.assertEquals ( srNEW1.Contact__c, cA.id ) ;

		//  Updates
		src.rating_1_5_select = '2' ;
        src.feedback = 'lalala' ;
		
		//  submit
		src.submit () ;
		
		//  Success page?
		System.assertEquals ( src.step, 5000 ) ;
		
		//  Check for updates
		Survey_Response__c srNEW2 = [
			SELECT ID,Feedback__c,Rating_1_5__c,Testimonial_Disclosure__c,Contact__c
			FROM Survey_Response__c
			WHERE ID = :srID 
		]  ;
		
		System.assertEquals ( srNEW2.Rating_1_5__c, '2' ) ;
		System.assertEquals ( srNEW2.Feedback__c, 'lalala' ) ;
		System.assertEquals ( srNEW2.Testimonial_Disclosure__c, true ) ;
		System.assertEquals ( srNEW2.Contact__c, cA.id ) ;

		//  End of testing
		Test.stopTest () ;
	}
}