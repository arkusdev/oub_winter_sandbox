public class GeoLocationUtilities 
{
    private static one_settings__c settings ;
    
    public GeoLocationUtilities ()
    {
        //  Empty ?!?
    }
    
	/*
	 *  Singular method call constructs a simple list and calls the multiple version
	 */
    static public void setApplicationLocation ( String appId )
    {
    	List<String> aL = new List<String> () ;
    	aL.add ( appId ) ;
    	
    	setApplicationLocations ( aL ) ;
    }
    
	@future ( callout=true ) // <- Headaches!
    static public void setApplicationLocations ( List<String> ids )
    {
        settings = one_settings__c.getInstance ( 'settings' ) ;
        
        // gather account info
        List<one_application__c> oL = [SELECT Id, Street_Address__c, City__c, State__c, ZIP_Code__c FROM one_application__c WHERE id IN :ids ] ;
        List<one_application__c> oNew = new List<one_application__c> () ;

		for ( one_application__c o : oL )
		{
	        // create an address string
	        String address = '';
	        if ( o.Street_Address__c != null )
	            address += o.Street_Address__c +', ';
	
	        if ( o.City__c != null )
	            address += o.City__c +', ';
	
	        if (o.State__c != null)
	            address += o.State__c +' ';
	
	        if (o.ZIP_Code__c != null)
	            address += o.ZIP_Code__c +', ';
	
	        address += 'USA' ;
	
	        address = EncodingUtil.urlEncode ( address.replace ( '#', '' ), 'UTF-8' ) ;
	
	        // build callout
	        Http h = new Http();
	        HttpRequest req = new HttpRequest();
            
            String gooApiKey = '' ;
            
            if ( ( settings != null ) && ( String.isNotBlank ( settings.Google_API_Key__c ) ) )
            	gooApiKey = settings.Google_API_Key__c ;
            
	        req.setEndpoint ( 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false' + '&key=' + gooApiKey ) ;
	        req.setMethod ( 'GET' ) ;
	        req.setTimeout ( 60000 ) ;
	
	        try
	        {
	        	//  Blanks!
	            double lat = null;
	            double lng = null;
	            String county = null ;
	            String neighborhood = null ;
	            
	            // callout
	            HttpResponse res = h.send ( req ) ;
	            
		        if ( res != null )
		        {
					System.debug ( '!!->' + res.getBody () ) ;
	
		            //  Load response
		            GoogleGeocode gg = GoogleGeocode.parse ( res.getBody () ) ;
		            
		            //  Check location
		            List<GoogleGeocode.Results> rL = gg.results ;
	
					if ( ( rL != null ) && ( rL.size () > 0 ) )
					{
						//  Found?!?
						boolean foundData = false ;
						
						//  Loop!
						for ( GoogleGeocode.Results r : rL )
						{
				            //  Get the address components
				            List<GoogleGeocode.Address_components> acL = r.address_components ;
						
							if ( ( acL != null ) && ( acL.size () > 0 ) ) 
							{
					            for ( GoogleGeocode.Address_components ac : acL )
					            {
					            	//  Array of types, we're checking against one of them for the right value
					            	List<String> tL = ac.types ;
					            	
									if ( ( tL != null ) && ( tL.size () > 0 ) )
									{
						            	for ( String t : tL )
						            	{
						            		if ( ( String.isNotBlank ( t ) ) && ( t.equalsIgnoreCase ( 'postal_code' ) ) )
						            		{
						            			String postalCode = ac.long_name ;
						            			if ( postalCode.equalsIgnoreCase ( o.ZIP_Code__c ) )
						            			{ 
						            				foundData = true ;
						            			}
						            		}
						            		
						            		if ( ( String.isNotBlank ( t ) ) && ( t.equalsIgnoreCase ( 'administrative_area_level_2' ) ) )
						            		{
						            			county = ac.long_name ;
						            		}
						            		
						            		if ( ( String.isNotBlank ( t ) ) && ( t.equalsIgnoreCase ( 'neighborhood' ) ) )
						            		{
						            			neighborhood = ac.long_name ;
						            		}
						            	}
									}
					            }
							}
							
							//  Get the location
							GoogleGeocode.Location l = r.geometry.location ;
							
				            if ( l != null )
				            {
				            	lat = l.lat ;
				            	lng = l.lng ;
				            }
				            
				            //  Valid?
				            if ( foundData )
				            {
				            	System.debug ( 'Found data!' ) ;
				            	break ;
				            }
				            else
				            {
				            	System.debug ( 'Rejected data, resetting, going to next one.' ) ;
				            	lat = null ;
				            	lng = null ;
				            	county = null ;
				            	neighborhood = null ;
				            }
						}
					}
		            
		            System.debug ( '1: ' + lat ) ;
		            System.debug ( '2: ' + lng ) ;
		            System.debug ( '3: ' + county ) ;
		            System.debug ( '4: ' + neighborhood ) ;

		            if ( ( lat != null ) && ( lng != null ) )
		            {
			            //  Update coordinates if we get back
		                o.Location__Latitude__s = lat ;
		                o.Location__Longitude__s = lng ; 
		            }
		            else
		            {
		            	//  No co-ordinates found, flag so we don't keep trying to update it
		            	o.Unknown_Location__c = true ;
		            }
		            
		            if ( String.isNotEmpty ( county ) ) o.County__c = county ;
		            if ( String.isNotEmpty ( neighborhood ) ) o.neighborhood__c = neighborhood ;
				            
		            System.debug ( 'A1: ' + o.Location__Latitude__s ) ;
		            System.debug ( 'A2: ' + o.Location__Longitude__s ) ;
		            System.debug ( 'A3: ' + o.County__c ) ;
		            System.debug ( 'A4: ' + o.neighborhood__c ) ;
		            
	                oNew.add ( o ) ;
		        }
	        } 
	        catch ( Exception e ) 
	        {
	        	System.debug ( 'HTTP Request fail. :(' ) ;
	        	System.debug ( e ) ;
	        }
		}
		
		//  Update all applications
		update ( oNew ) ;
    }
}