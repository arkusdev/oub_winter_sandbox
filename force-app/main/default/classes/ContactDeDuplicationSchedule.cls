global class ContactDeDuplicationSchedule implements Schedulable 
{
   global void execute ( SchedulableContext sc ) 
   {
		ContactDeDuplication cdd = new ContactDeDuplication () ;
		Database.executeBatch ( cdd ) ;
   }
}