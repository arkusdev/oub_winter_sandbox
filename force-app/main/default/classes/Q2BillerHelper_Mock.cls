@isTest
global class Q2BillerHelper_Mock implements HttpCalloutMock
{
    // thingys
    public static String returnOptionalIdentifier = '0x301g5415g14gt1' ;
    
    // Implement this interface method
    global HTTPResponse respond ( HTTPRequest req ) 
    {
        // Create a fake response
        HttpResponse res = new HttpResponse () ;
        res.setStatusCode ( 200 ) ;
        res.setStatus ( 'Success' ) ;
        
		// Endpoint can change ...
		if ( req.getEndpoint().contains ( 'create-token' ) )
		{
            //  Set up the generator
            JSONGenerator g = JSON.createGenerator ( true ) ;
            
            //  Create the object
            g.writeStartObject () ;
            
			g.writeStringField ( 'token', 'abc123xyz456def789' ) ;
            
            
            g.writeEndObject () ;
            
	        res.setBody ( g.getAsString () ) ;
        }
		else if ( req.getEndpoint().contains ( 'client' ) )
		{
            //  Set up the generator
            JSONGenerator g = JSON.createGenerator ( true ) ;
            
            //  Create the object
            g.writeStartObject () ;
            
			g.writeStringField ( 'clientId', '789def456xyz123abc' ) ;
            
            g.writeEndObject () ;
            
	        res.setBody ( g.getAsString () ) ;
        }
		else if ( req.getEndpoint().contains ( 'list' ) )
		{
	        res.setBody ( '{ "data": [ { "_id": "580e3754dec21e861b5a3718", "type": "card", "identifier": "' + returnOptionalIdentifier + '", "status": "verified", "default": true, "name": "Sean Hill", "card": { "number": "XXXX4242", "brand": "visa", "type": "credit", "expDate": "2016-12-01T12:00:00.000Z" } }, { "_id": "580e3e11dec21e861b5a3719", "type": "bank", "identifier": "optional_custom_identifier", "status": "declined", "name": "Sean Hill", "bank": { "routing": "XXXX2531", "account": "XXXX4124", "type": "checking" } } ] }' ) ;
        }
		else if ( req.getEndpoint().contains ( 'payment' ) )
		{
            //  Set up the generator
            JSONGenerator g = JSON.createGenerator ( true ) ;
            
            //  Create the object
            g.writeStartObject () ;
            
			g.writeStringField ( 'paymentMethodId', '111aaa222bbb333ccc' ) ;
            
            g.writeEndObject () ;
            
	        res.setBody ( g.getAsString () ) ;
        }
        
        return res ;
    }
}