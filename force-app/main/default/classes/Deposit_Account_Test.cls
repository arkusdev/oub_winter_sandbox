@isTest
public class Deposit_Account_Test {
    
    private static void insertCustomSetting(){
        
        // Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = '' ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.ACH_Validation_Threshold__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = true ;
        s.Last_Batch_Email_Sent__c = Date.today ().addDays ( -7 ) ;
        
        insert s;
    }
    private static Deposit_Account_Controller initializeController(Boolean jointly){
        ApexPages.currentPage().getHeaders().put('USER-AGENT', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko');
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '198.0.0.1');
        ApexPages.currentPage().getHeaders().put('True-Client-IP', '198.0.0.1');
        ApexPages.currentPage().getHeaders().put('CF-Connecting-IP', '198.0.0.1');
        
        Contact c = new Contact () ;
        c.FirstName = 'Test' ;
        c.LastName = 'Name' ;
        c.Email = 'test@test.com' ;
        insert c ;
        
        ApexPages.currentPage().getParameters().put ( 'scode', 'test' );
        ApexPages.currentPage().getParameters().put ( 'refcnt', c.id );
        ApexPages.currentPage().getParameters().put ( 'entcnt', c.id );
        
        Deposit_Account_Controller depAccountController = new Deposit_Account_Controller();
        
        //fill all the fields of the application
        depAccountController.app.First_Name__c = 'Test';
        depAccountController.app.Last_Name__c = 'Test';
        depAccountController.app.Email_Address__c = 'test@test.com';
        depAccountController.app.Over_18__c = true;
        if(jointly){
            depAccountController.app.Number_of_Applicants__c = 'I will be applying jointly with another person';           
        }else{
            depAccountController.app.Number_of_Applicants__c = 'I will be applying individually';           
        }
        depAccountController.app.Disclosures__c = true;
        depAccountController.app.Referred_By__c = 'Radio';
        depAccountController.app.Phone__c = '1234567890';
        
        return depAccountController;
    }
    
    private static void fillStep4(Deposit_Account_Controller controller, Boolean jointly){
        
        controller.app.SSN__c = '987-65-4321';
        controller.app.DOB__c = Date.newInstance(1980,10,11);
        controller.app.MothersMaidenName__c = 'Test';
        controller.app.Street_Address__c = 'Test Avenue';
        controller.app.City__c = 'Test';
        controller.app.State__c = 'TE';
        controller.app.ZIP_Code__c = '12345';
        controller.app.How_Long_yrs__c = 12;
        controller.app.How_Long_mths__c = 0;
        controller.app.Mailing_Address_Same__c = 'No';
        controller.app.Mailing_Street_Address__c = 'Test';
        controller.app.Mailing_City__c = 'Test';
        controller.app.Mailing_State__c = 'TE';
        controller.app.Mailing_ZIP_Code__c = '12345';
        
        controller.app.PrimID_Number__c = '999999999';
        controller.app.PrimID_State__c = 'TE';
        controller.app.PrimID_Issue_Date__c = Date.newInstance(2010,12,12);
        controller.app.PrimID_Expiration_Date__c = Date.newInstance(2025,12,12);
        
        controller.app.Employment_Status__c = 'Employed';
        controller.app.Employer__c = 'Test';
        
        if(jointly){
            controller.app.Co_Applicant_Email_Address__c = 'test2@test.com';
            controller.app.Co_Applicant_First_Name__c = 'Test2';
            controller.app.Co_Applicant_Last_Name__c = 'Test2';
            controller.app.Co_Applicant_SSN__c = '987-65-4322';
            controller.app.Co_Applicant_DOB__c = Date.newInstance(1980,10,11);
            controller.app.Co_Applicant_MothersMaidenName__c = 'Test2';
            controller.app.Co_Applicant_Street_Address__c = 'Test Avenue 2';
            controller.app.Co_Applicant_City__c = 'Test2';
            controller.app.Co_Applicant_State__c = 'TE';
            controller.app.Co_Applicant_ZIP_Code__c = '12345';
            controller.app.Co_Applicant_Phone__c = '1234567890';
            controller.app.Co_Applicant_How_Long_yrs__c = 12;
            controller.app.Co_Applicant_How_Long_mths__c = 0;
            controller.app.Is_Co_App_Same_Current_Address__c = 'No';
            
            controller.app.PrimID_Co_Applicant_Number__c = '999999998';
            controller.app.PrimID_Co_Applicant_State__c = 'TE';
            controller.app.PrimID_Co_Applicant_Issue_Date__c = Date.newInstance(2010,12,12);
            controller.app.PrimID_Co_Applicant_Expiration_Date__c = Date.newInstance(2025,12,12);
            
            controller.app.Co_Applicant_Employment_Status__c = 'Employed';
            controller.app.Co_Applicant_Employer__c = 'Test';
        }
    }
    
    testMethod static void goThroughFlow1(){
        
        Lead l = new lead () ;
        l.firstName = 'Test' ;
        l.LastName = 'Test' ;
        l.Email = 'test@test.com' ;
        l.Company = 'Nyet' ;
        l.LeadSource = 'Other' ;
        insert l ;
        
        insertCustomSetting();
        ApexPages.currentPage().getCookies().put ( 'ks_Cookie' , new Cookie('ks_Cookie', 'Test#Test#test@test.com# ', null, 1440, false));
        
        Deposit_Account_Controller depAccountController = initializeController(false);
        
        //go to step 2
        depAccountController.goNextStep();
        
        System.assertEquals(2,depAccountController.stepNumber);
        
        //Select All the Checking Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : depAccountController.checkingAccounts){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }

		//try going to step 3 but will received an error because some funding amount is lower than the minimum
        depAccountController.checkingAccounts[0].funding = 0;
        depAccountController.goNextStep();
        System.assertEquals(2,depAccountController.stepNumber);
        
        //repair funding amount
        depAccountController.checkingAccounts[0].funding = 1000;
        
        //go to step 3
        depAccountController.goNextStep();
        
        //should be using card
        System.assert(depAccountController.cardSelected);

        //manually set overdraft options to 'Y'
        for(Online_Deposit_Application_Account__c chAcc : depAccountController.checkingAccountsForOverdraft){
            chAcc.Opt_In_Status__c = 'Y';
        }
                
        //go to step 4
        depAccountController.goNextStep();
        
        System.assertEquals(4,depAccountController.stepNumber);
        
        //fill step number four individually
        fillStep4(depAccountController,false);
        
        //try going to step 5 but will received an error because some required field is empty
        depAccountController.app.SSN__c = '';
        depAccountController.goNextStep();
        System.assertEquals(4,depAccountController.stepNumber);
        
        //try going to step 5 but will received an error because some field value is not valid
        depAccountController.app.SSN__c = '1234';
        depAccountController.goNextStep();
        System.assertEquals(4,depAccountController.stepNumber);
        
        //recover field
        depAccountController.app.SSN__c = '987-65-4321';
        
        //go to step 5
        depAccountController.goNextStep();
                
        System.assertEquals(5,depAccountController.stepNumber);
      
        //fill card fields
        depAccountController.cardNumber = '9999999999999999';
        depAccountController.cardSecurityCode = '999';
        depAccountController.cardExpirationMonth = '12';
        depAccountController.cardExpirationYear = '2079';
        depAccountController.app.Credit_Report_Authorization__c = true;
        depAccountController.addressSelected = 'alternate';
        depAccountController.fundAccountWithCard();
        depAccountController.addressSelected = 'mailing';
        depAccountController.fundAccountWithCard();
        depAccountController.addressSelected = 'primary';
        depAccountController.fundAccountWithCard();
        
        
        depAccountController.app.Credit_Report_Authorization__c = true;
        one_routing_number__c  rn = new one_routing_number__c ();
        rn.Routing_Name__c  = '051403122';
        insert rn;
        depAccountController.app.Funding_Routing_Number__c = '051403122';
        depAccountController.app.Funding_Account_Number__c = '23456789';
        depAccountController.app.Funding_Account_Type__c = 'Checking';
        depAccountController.app.Requested_ACH_Withdrawal_Date__c = System.today() + 7;

        //go to end of the flow
        depAccountController.goNextStep();
        
        depAccountController.calloutToQualifile();

        List<Id> applicationsIds = new List<Id>();
        applicationsIds.add(depAccountController.app.Id);

        runCOCC_Callouts_Queueable queueableCOCC = new runCOCC_Callouts_Queueable(applicationsIds);
        System.enqueueJob(queueableCOCC);

        ///added for coverage
        depAccountController.app.Number_of_Applicants__c = 'I will be applying jointly with another person';
        COCC_Helper cocc = new COCC_Helper(depAccountController.app);
        cocc.createPersonsUnityVisa();
        String x = cocc.getPersonNumber () ;
        String y = cocc.getCoAppPersonNumber () ;
    }
    
    testMethod static void goThroughFlow2(){
        insertCustomSetting();
        Deposit_Account_Controller depAccountController = initializeController(true);
        
        //go to step 2
        depAccountController.goNextStep();
        
        System.assertEquals(2,depAccountController.stepNumber);
        
        //Select All the Checking Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : depAccountController.checkingAccounts){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //go to step 3
        depAccountController.goNextStep();
        
        //should be using card
        System.assert(depAccountController.cardSelected);
        
        //manually set overdraft options to 'Y'
        for(Online_Deposit_Application_Account__c chAcc : depAccountController.checkingAccountsForOverdraft){
            chAcc.Opt_In_Status__c = 'Y';
        }
                
        //go to step 4
        depAccountController.goNextStep();
        
        System.assertEquals(4,depAccountController.stepNumber);
        
        //fill step number four jointly
        fillStep4(depAccountController,true);
   
        //go to step 5
        depAccountController.goNextStep();
                
        System.assertEquals(5,depAccountController.stepNumber);
        
        depAccountController.app.Credit_Report_Authorization__c = true;
        one_routing_number__c  rn = new one_routing_number__c ();
        rn.Routing_Name__c  = '051403122';
        insert rn;
        depAccountController.app.Funding_Routing_Number__c = '051403122';
        depAccountController.app.Funding_Account_Number__c = '23456789';
        depAccountController.app.Funding_Account_Type__c = 'Checking';
        depAccountController.app.Requested_ACH_Withdrawal_Date__c = System.today() + 7;

        //go to end of the flow
        depAccountController.goNextStep();
        
        depAccountController.calloutToQualifile();

        Test.StartTest();   
            ChangeRecordTypeUnityVisaBatch crtuvb = new ChangeRecordTypeUnityVisaBatch('');
            ID batchprocessid = Database.executeBatch(crtuvb);
        Test.StopTest();


    }
    
    testMethod static void testVisualGettersAndSetters(){
        
        insertCustomSetting();
        Deposit_Account_Controller depAccountController = initializeController(false);
        
        depAccountController.getAddressStateOptions();
        depAccountController.getAddressOptions();
        depAccountController.getcardMonthOptions();
        depAccountController.getcardYearOptions();
        depAccountController.getcardYearOptions();
        
        depAccountController.idTypeChange();
        depAccountController.emStatusChange();
        depAccountController.emStatusChangeCo();
        depAccountController.changeToBankDeposit();
        depAccountController.changeToCardOption();
        depAccountController.getOverdraftOptions();
        
        one_routing_number__c  rn = new one_routing_number__c ();
        rn.Routing_Name__c  = '051403122';
        insert rn;
        Deposit_Account_Controller.findRoutingNumberRecords('051403122');
    }
    
    testMethod static void testSuccessPageController1(){
        
        insertCustomSetting();
        
        ApexPages.currentPage().getHeaders().put('USER-AGENT', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko');
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '198.0.0.1');
        
        one_application__c app = one_utils.getApplicationWithData(true);
        app.FIS_Decision_Code__c = 'P030';
        app.QualiFile_Product_Strategy__c = 'A0100001';
        app.RecordTypeID = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId;
        app.Upload_Attachment_key__c = one_utils.getRandomString (30) ;
        insert app;
        
        Cookie jCookie = new Cookie ( 'j_Cookie', app.ID , null, -1, false ) ;

		ApexPages.currentPage().setCookies ( new Cookie[] { jCookie } ) ;
                                     
        System.currentPageReference().getParameters().put('aid',app.Id);
        System.currentPageReference().getParameters().put('kid',app.Upload_Attachment_key__c);
        
        Online_Deposit_Application_Account__c checkingAcc = new Online_Deposit_Application_Account__c();
        checkingAcc.Product_Name__c = 'UNITY E-Gold Checking ';
        checkingAcc.Initial_Funding_amount__c = 500;
        checkingAcc.Direct_Deposit__c = true;
        checkingAcc.Online_Banking__c = true;
        checkingAcc.Telephone_Banking__c = true;
        checkingAcc.Debit_card__c = true;
        checkingAcc.E_Statement__c = true;
        checkingAcc.Category__c = 'Checking';
        checkingAcc.Major_Account_Type_Code__c = 'CK'; 
        checkingAcc.Minor_Account_Type_Code__c = 'UEGC';
        checkingAcc.Application__c = app.Id;
        checkingAcc.Card_Code__c = 'PRIM';

        insert checkingAcc;

        Deposit_Account_Success_Controller controller = new Deposit_Account_Success_Controller();
                
        controller.amount = 50;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 150;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 15000;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

		controller.decision = 'removeCheckingAcc';
        controller.saveNewAccounts();

        controller.decision = 'Cancel';
        controller.saveNewAccounts();

		app.FIS_Decision_Code__c = 'P912';
		upsert app;
        
        Deposit_Account_Success_Controller controller2 = new Deposit_Account_Success_Controller();
    }
    
    testMethod static void testSuccessPageController1a(){
        
        insertCustomSetting();
        
        ApexPages.currentPage().getHeaders().put('USER-AGENT', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko');
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '198.0.0.1');
        
        one_application__c app = one_utils.getApplicationWithData(true);
        app.FIS_Decision_Code__c = 'P030';
        app.QualiFile_Product_Strategy__c = 'A0100001';
        app.RecordTypeID = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId;
        app.Upload_Attachment_key__c = one_utils.getRandomString (30) ;
        insert app;
        
        Cookie jCookie = new Cookie ( 'j_Cookie', app.ID , null, -1, false ) ;

		ApexPages.currentPage().setCookies ( new Cookie[] { jCookie } ) ;
                                     
        System.currentPageReference().getParameters().put('aid',app.Id);
        System.currentPageReference().getParameters().put('kid',app.Upload_Attachment_key__c);
        
        Online_Deposit_Application_Account__c checkingAcc = new Online_Deposit_Application_Account__c();
        checkingAcc.Product_Name__c = 'UNITY E-Gold Checking ';
        checkingAcc.Initial_Funding_amount__c = 500;
        checkingAcc.Direct_Deposit__c = true;
        checkingAcc.Online_Banking__c = true;
        checkingAcc.Telephone_Banking__c = true;
        checkingAcc.Debit_card__c = true;
        checkingAcc.E_Statement__c = true;
        checkingAcc.Category__c = 'Checking';
        checkingAcc.Major_Account_Type_Code__c = 'CK'; 
        checkingAcc.Minor_Account_Type_Code__c = 'UEGC';
        checkingAcc.Application__c = app.Id;
        checkingAcc.Card_Code__c = 'PRIM';

        insert checkingAcc;

        Deposit_Account_Success_Controller controller = new Deposit_Account_Success_Controller();
                
        controller.amount = 50;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 150;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 15000;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

		controller.decision = 'removeCheckingAcc';
        controller.saveNewAccounts();

        controller.decision = 'Cancel';
        controller.saveNewAccounts();

        app.FIS_Decision_Code__c = 'P089';
		upsert app;
        
        Deposit_Account_Success_Controller controller3 = new Deposit_Account_Success_Controller();
    }
    
    testMethod static void testSuccessPageController2(){
        
        insertCustomSetting();
        
        ApexPages.currentPage().getHeaders().put('USER-AGENT', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko');
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '198.0.0.1');
        
        one_application__c app = one_utils.getApplicationWithData(true);
        app.FIS_Decision_Code__c = 'P030';
        app.QualiFile_Product_Strategy__c = 'A0100001';
        app.RecordTypeID = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId;
        app.Upload_Attachment_key__c = one_utils.getRandomString (30) ;
        insert app;
        
        Cookie jCookie = new Cookie ( 'j_Cookie', app.ID , null, -1, false ) ;

		ApexPages.currentPage().setCookies ( new Cookie[] { jCookie } ) ;
                                     
        System.currentPageReference().getParameters().put('aid',app.Id);
        System.currentPageReference().getParameters().put('kid',app.Upload_Attachment_key__c);
        
        Online_Deposit_Application_Account__c checkingAcc = new Online_Deposit_Application_Account__c();
        checkingAcc.Product_Name__c = 'UNITY E-Gold Checking ';
        checkingAcc.Initial_Funding_amount__c = 500;
        checkingAcc.Direct_Deposit__c = true;
        checkingAcc.Online_Banking__c = true;
        checkingAcc.Telephone_Banking__c = true;
        checkingAcc.Debit_card__c = true;
        checkingAcc.E_Statement__c = true;
        checkingAcc.Category__c = 'Checking';
        checkingAcc.Major_Account_Type_Code__c = 'CK'; 
        checkingAcc.Minor_Account_Type_Code__c = 'UEGC';
        checkingAcc.Application__c = app.Id;
        checkingAcc.Card_Code__c = 'PRIM';

        insert checkingAcc;

        Deposit_Account_Success_Controller controller = new Deposit_Account_Success_Controller();
                
        controller.amount = 50;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 150;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 15000;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

		controller.decision = 'removeCheckingAcc';
        controller.saveNewAccounts();

        controller.decision = 'Cancel';
        controller.saveNewAccounts();

		app.FIS_Decision_Code__c = 'F003';
		upsert app;
        
        Deposit_Account_Success_Controller controller4 = new Deposit_Account_Success_Controller();
        
        controller4.sendToAdditionalInfoPage();
    }
    
    testMethod static void testSuccessPageController3(){
        
        insertCustomSetting();
        
        ApexPages.currentPage().getHeaders().put('USER-AGENT', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko');
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '198.0.0.1');
        
        one_application__c app = one_utils.getApplicationWithData(true);
        app.FIS_Decision_Code__c = 'P030';
        app.QualiFile_Product_Strategy__c = 'A0100001';
        app.RecordTypeID = Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId;
        app.Upload_Attachment_key__c = one_utils.getRandomString (30) ;
        insert app;
        
        Cookie jCookie = new Cookie ( 'j_Cookie', app.ID , null, -1, false ) ;

		ApexPages.currentPage().setCookies ( new Cookie[] { jCookie } ) ;
                                     
        System.currentPageReference().getParameters().put('aid',app.Id);
        System.currentPageReference().getParameters().put('kid',app.Upload_Attachment_key__c);
        
        Online_Deposit_Application_Account__c checkingAcc = new Online_Deposit_Application_Account__c();
        checkingAcc.Product_Name__c = 'UNITY E-Gold Checking ';
        checkingAcc.Initial_Funding_amount__c = 500;
        checkingAcc.Direct_Deposit__c = true;
        checkingAcc.Online_Banking__c = true;
        checkingAcc.Telephone_Banking__c = true;
        checkingAcc.Debit_card__c = true;
        checkingAcc.E_Statement__c = true;
        checkingAcc.Category__c = 'Checking';
        checkingAcc.Major_Account_Type_Code__c = 'CK'; 
        checkingAcc.Minor_Account_Type_Code__c = 'UEGC';
        checkingAcc.Application__c = app.Id;
        checkingAcc.Card_Code__c = 'PRIM';

        insert checkingAcc;

        Deposit_Account_Success_Controller controller = new Deposit_Account_Success_Controller();
                
        controller.amount = 50;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 150;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

        controller.amount = 15000;
        controller.decision = 'Yes';
        controller.saveNewAccounts();

		controller.decision = 'removeCheckingAcc';
        controller.saveNewAccounts();

        controller.decision = 'Cancel';
        controller.saveNewAccounts();

		app.FIS_Decision_Code__c = 'D005';
		upsert app;
        
        Deposit_Account_Success_Controller controller5 = new Deposit_Account_Success_Controller();

		app.FIS_Decision_Code__c = 'E005';
		upsert app;
        
        Deposit_Account_Success_Controller controller6 = new Deposit_Account_Success_Controller();
    }
    
    testMethod static void goThroughFlowAdvocate()
    {
        Contact c = new Contact () ;
        c.firstName = 'Test' ;
        c.lastName = 'User' ;
        c.email = 'test@user.com' ;
        insert c ;
        
        Referral_Code__c rc = new Referral_Code__c () ;
        rc.Contact__c = c.ID ;
        rc.Name = 'JIMTEST' ;
        insert rc ;
        
        insertCustomSetting();
        ApexPages.currentPage().getCookies().put ( 'apcd_Cookie' , new Cookie('apcd_Cookie', 'JIMTEST', null, 1440, false));
        
        Deposit_Account_Controller depAccountController = initializeController(false);
        
        //go to step 2
        depAccountController.goNextStep();
        
        System.assertEquals(2,depAccountController.stepNumber);
        
        //Select All the Checking Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : depAccountController.checkingAccounts){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //Select All the savings Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : depAccountController.savingAccounts){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //Select All the CD Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : depAccountController.certificatesOfDeposit){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //go to step 3
        depAccountController.goNextStep();
        
    }
    
    testMethod static void MOARTESTS()
    {
        insertCustomSetting();
        
        Account a1 = new account () ;
        a1.Name = 'Foo' ;
        insert a1 ;
        
        Contact c1 = new Contact () ;
        c1.FirstName = 'Test' ;
        c1.LastName = 'Test' ;
        c1.email = 'test@test.com' ;
        c1.AccountId = a1.ID ;
        c1.LeadSource = 'Other' ;
        insert c1 ;
        
        Test.startTest () ;
        
        Deposit_Account_Controller dap1 = initializeController ( true ) ;
        
        ApexPages.currentPage().getParameters().put ( 'scode', null );
        ApexPages.currentPage().getParameters().put ( 'refcnt', null );
        ApexPages.currentPage().getParameters().put ( 'entcnt', null );
        
        dap1.app.Over_18__c = false ;

        dap1.goNextStep () ;
        
        dap1.app.Over_18__c = true ;
        dap1.app.Disclosures__c = false ;

        dap1.goNextStep () ;
        
        dap1.app.Disclosures__c = true ;
        dap1.app.Is_Co_App_Same_Current_Address__c = 'yes' ;
        
        //  Step 1
        dap1.goNextStep () ;
        
        //  Step 2
        dap1.goNextStep () ;
        
        //Select All the Checking Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : dap1.checkingAccounts){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //Select All the savings Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : dap1.savingAccounts){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //Select All the CD Accounts
        for(Deposit_Account_Controller.innerProduct chAcc : dap1.certificatesOfDeposit){
            chAcc.isSelected = true;
            chAcc.funding = 1000;
        }
        
        //  Step 3
        dap1.goNextStep () ;
        
        //  Step 4
        dap1.goNextStep () ;
        
        Test.stopTest () ;
    }
    
    testMethod static void testCOCCHelper ()
    {
        insertCustomSetting();
        
        Account a1 = new account () ;
        a1.Name = 'Foo' ;
        insert a1 ;
        
        one_application__c app = one_utils.getApplicationWithData(true);
        
        Contact c = new Contact () ;
        c.FirstName = app.First_Name__c ;
        c.LastName = app.Last_Name__c ;
        c.TaxID__c = app.SSN__c.replace ( '-', '' ) ;
        c.Phone = app.Phone__c ;
        c.Email = app.Email_Address__c ;
        c.AccountId = a1.ID ;
        
        insert c ;
        
        insert app ;
        
        Test.startTest () ;
        
        cocc_helper ch = new cocc_helper ( app )  ;
        
        ch.createPersonsUnityVisa () ;
        
        Test.stopTest () ;
    }
}