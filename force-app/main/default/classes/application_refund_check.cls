public with sharing class application_refund_check 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
    public Ticket__c inTicket { public get ; private set ; }
    
    public boolean hasTicket { public get ; private set ; }

    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public application_refund_check ( ApexPages.Standardcontroller stdC )
	{
		hasTicket = false ;
		
		String aid = ApexPages.currentPage().getParameters().get('id') ;
		
		List<Ticket__c> tL = [
			SELECT ID, 
			Application__c, Account__c, Financial_Account__c
			FROM Ticket__c
			WHERE ID = :aid
		] ;
		
		if ( ( tL != null ) && ( tL.size () > 0 ) )
			inTicket = tL.get ( 0 ) ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Page actions
     *  ---------------------------------------------------------------------------------------- */
	public PageReference checkTicket () 
	{
		if ( inTicket != null )
		{
			boolean isFirst = true ;
			
			String query = '' ;
			query += 'SELECT ID ' ;
			query += 'FROM Ticket__c ' ;
			query += 'WHERE ID != \'' + inTicket.Id + '\' ' ;
			query += 'AND ( ' ;
		
			if ( inTicket.Application__c != null )
			{
				isFirst = false ;
				query += 'Application__c = \'' + inTicket.Application__c + '\' ' ;
			}
			
			if ( inTicket.Account__c != null )
			{
				if ( ! isFirst )
					query += 'OR ' ;
				else
					isFirst = false ;
					
				query += 'Account__c = \'' + inTicket.Account__c + '\' ' ;
			}
			
			if ( inTicket.Financial_Account__c != null )
			{
				if ( ! isFirst )
					query += 'OR ' ;
				else
					isFirst = false ;
					
				query += 'Financial_Account__c = \'' + inTicket.Financial_Account__c + '\' ' ;
			}
			
			query += ') ' ;
			query += '' ;
			
			List<Ticket__c> tL = Database.query ( query ) ;
			
			if ( ( tL != null ) && ( tL.size () > 0 ) )
				hasTicket = true ;
		}
		
		return null ;
	}
}