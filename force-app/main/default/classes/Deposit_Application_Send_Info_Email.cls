public with sharing class Deposit_Application_Send_Info_Email 
{
	private final Deposit_Application__c da ;
	
	public boolean emailInStatus
	{
		get ;
		private set ;
	}
	
    
    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */   
    public Deposit_Application_Send_Info_Email ( ApexPages.Standardcontroller stdC )
    {
    	ApexPages.Standardcontroller theController = stdC ;
    	
		//  Get ID that is passed in, use it to get email address
        String appId = ((Deposit_Application__c) theController.getRecord ()).id ;
        
        if ( String.isNotBlank ( appId ) )
        {
	       	da = [ 
	       		SELECT ID, Application_Processing_Status__c
				FROM Deposit_Application__c 
				WHERE id = :appId 
				] ;
        }
    }
    
    public PageReference sendEmail ()
    {
    	emailInStatus = false ;
    	
		if ( ( da != null ) && ( String.isNotBlank ( da.Application_Processing_Status__c ) ) )
		{
        	if ( da.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) 
        	{
	        	List<String> appL = new List<String> () ;
	        	appL.add ( da.Id ) ;
	        		
	        	DepositApplicationEmailFunctions.sendDepositApplicationAdditionalInfoEmails ( appL ) ;
	        	emailInStatus = true ;
        	}
		}
        
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference ( '/' + da.Id ) ;
        pageRef.setRedirect ( true ) ;
        return pageRef ;
    }
}