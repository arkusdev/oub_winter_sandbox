global class Email2Ticket implements Messaging.InboundEmailHandler
{
    private static List<RecordType> ticketRTL ;
    private map<String,Group> groupMap ;

    public String[] toAddresses {
        get; set;
    }
    
    public Email2Ticket() 
    {
		List<String> foo = new List<String> () ;
		foo.add ( 'Support' ) ;
		foo.add ( 'Fax' ) ;
		foo.add ( 'Accounts Payable' ) ;
        
		ticketRTL = OneUnitedUtilities.getRecordTypeList ( foo ) ;
        
        groupMap = new map<String,Group> () ;
        list<Group> gL = null ;
        
        try
        {
            gL = [
                SELECT ID, DeveloperName
                FROM GROUP
                WHERE Type='Queue' 
            ] ;
        }
        catch ( Exception e )
        {
            gL = null ;
        }
        
        for ( Group g : gL )
        {
            groupMap.put ( g.DeveloperName, g ) ;
        }
    }
    
	@testVisible private String SubLine;
	@testVisible private String MsgBody;

    private String FindWingdings(String input)
    {
        try
        {
            Matcher thing = Pattern.compile('[^\\w\\s\\p{Punct}]').matcher(input);
        
            if(thing.find())
            {
                input = thing.replaceAll(' ');
            } 
        }
        catch(Exception e)
        {
            // ?
        }
        
        return input;
    }
    
    //Attachement Methods using ContentVersion and with documentlinks
    private ContentVersion createContentVersion (String subject, String name, blob body)
    {
        String exceptionMessage ;
        //Splits name by '.'
        List<String> fileext = name.split('\\.',2);
        System.debug('The Stringext is:::-------'+fileext[1]);
        
        ContentVersion contentversion = new ContentVersion();
        try
        { 
            contentversion.ContentLocation = 'S' ;
            contentversion.PathOnClient = fileext[0] + '.' + fileext[1]  ;
            contentversion.Title = name;
            contentversion.VersionData = body;
        }
        catch ( Exception e )
        {
			system.debug('########### Rejected' );
            exceptionMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            
            contentversion =null;
        }
           
        return contentversion; 
    }
    
    private ContentDocumentLink CreatesContentDocumentLink(Id contentDocumentId, Id parentId)
    {
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ShareType = 'V'; 
        contentDocumentLink.Visibility = 'InternalUsers';
        return contentDocumentLink;
	}

    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
    {        
        Messaging.InboundEmailResult Finale = new Messaging.InboundEmailResult();
        
        Ticket__c emailTicket = null;
        emailTicket = new Ticket__c();
        
        //list of all the emails from fax queue metadata
        List<Fax_queue__mdt> faxqueue = null ;
       	try
        {
	        faxqueue = [SELECT Email__c, Queue_Name__c, Short_Email__c, Record_Type__c
                        FROM Fax_queue__mdt];
        }
        catch ( DMLException e )
        {
           System.debug('Null FaxQueue');
        }
        
        //adding all the emails converted into string into a map
        Map<String, Fax_queue__mdt> m10 = new Map<String, Fax_queue__mdt>();
        
        if ( ( faxqueue != null ) && ( faxqueue.size () > 0 ) )
        {
            for(Fax_queue__mdt queueemail : faxqueue){
                 m10.put(queueemail.Email__c,queueemail);
            }
        }
        
        Fax_queue__mdt fq = null ;
        
        //comparing recepient email address and the ones we got from the 
        
        if(String.isNotBlank(email.subject))
        {
            SubLine = FindWingdings(email.subject);
        }
        else
        {
            SubLine = 'Could not parse subject line';
        }
        
        emailTicket.Subject__c = SubLine; // Add the ticket subject
        
        if(String.isNotBlank(email.plainTextBody))
        {
            MsgBody = FindWingdings(email.plainTextBody);
        }
        else
        {
            MsgBody = 'Could not parse message body';
        }
        
        emailTicket.Description__c = MsgBody;
        
        System.debug ( 'Support record type' ) ;
        emailTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( ticketRTL, 'Support','Ticket__c');
        
        String[] toadds = email.toAddresses;
        
        if ( toadds != null ) 
        {
            // System.debug('Toaddress are: '+toadds);
            for(Integer i = 0; i< toadds.size(); i++)
            {
                if ( m10.containsKey ( toadds[i] ))
                {
                    fq = m10.get ( toadds[i] ) ;
                    
                    group gid = groupMap.get ( fq.Queue_Name__c ) ;
                    
                    if ( gid != null )
                    {
                        System.debug ( 'Group record type :: ' + gid.ID ) ;
                        
                        if ( String.isNotBlank ( fq.Record_Type__c ) )
                        {
	                        emailTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( ticketRTL, fq.Record_Type__c, 'Ticket__c' ) ;
    	                    System.debug ( fq.Record_Type__c + ' --> ' + emailTicket.recordTypeID ) ;
                        }
                        else
                        {
	                        emailTicket.RecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( ticketRTL, 'Fax', 'Ticket__c' ) ;
    	                    System.debug ( fq.Record_Type__c + ' --> DEFAULTED :: ' + emailTicket.RecordTypeId ) ;
                        }
                        
                        emailTicket.OwnerId = gid.Id ;
                        emailTicket.Email_To_Address__c = fq.Short_Email__c ;
                        break;
                    }
                }
                
                if ( fq != null )
                {
                    emailTicket.Email_Address__c = FindWingdings(email.fromAddress); 
                }
            }
        }
        
        insert emailTicket;
        
        if(email.binaryAttachments != null && email.binaryAttachments.size() > 0)
        {
            List<ContentVersion> contentversions = new List<ContentVersion>();
            
            for(integer foo = 0; foo < email.binaryAttachments.size(); foo++)
            {
                ContentVersion contentVersion = createContentVersion(email.Subject ,email.binaryAttachments[foo].Filename , email.binaryAttachments[foo].body);
                contentversions.add(contentVersion);
            }
            insert contentversions;
            
            List<ContentDocumentLink> contentdocumentlinks = new List<ContentDocumentLink>();
            try{
                
                contentversions = [SELECT Id, ContentDocumentId, FileExtension FROM ContentVersion WHERE Id IN :contentversions];
                for ( ContentVersion v : contentversions ) {
                    System.debug('The FileType is:::-------'+v.FileExtension);
                    ContentDocumentLink contentDocumentLink = CreatesContentDocumentLink(v.ContentDocumentId,emailTicket.Id);
                    contentdocumentlinks.add(contentDocumentLink);
                }
                insert contentdocumentlinks;
                
            }catch(DmlException e){
                System.debug('Error getting contentversion data  and inserting link' + e);
            }
        }
        Finale.success = true;
        return Finale;
    }
}