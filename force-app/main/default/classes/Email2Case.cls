global class Email2Case implements Messaging.InboundEmailHandler
{
    public Email2Case() 
    {
        // empty
    }
    
    global String TestFname;
    
    global String TestLname;
        
    global String AttachmentName;
    
    global String TestBody;
    
    global Contact TestContact;
    
    global String TestAddress;
    
    public String FindWingdings(String input)
    {
        try
        {
            Matcher thing = Pattern.compile('[^\\w\\s\\p{Punct}]').matcher(input);
        
            if(thing.find())
            {
                input = thing.replaceAll(' ');
            }
        }
        catch(Exception e)
        {
            return input;
        }
        
        
        return input;
    }
    	//contact lookup
    public static Contact getContactDetails ( String firstName, String lastName, String email )
    {
        list<Contact> cL = null ;
        Contact c = null ;
        
        try
        {
            cL = [
                SELECT ID, Name, Email
                FROM Contact
                WHERE Email = :email
                AND FirstName = :firstName
                AND LastName = :lastName
                ORDER BY ID
                DESC
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( cL != null ) && ( cL.size () > 0 ))
            c = cL [ 0 ] ;
        
        return c ;
    }
    
    public static ContentVersion createContentVersion (String subject, String name, blob body)
    {
        String exceptionMessage ;
        ContentVersion contentversion = new ContentVersion();
        try
        { 
            contentversion.ContentLocation = 'S' ;
            contentversion.PathOnClient = name;
            contentversion.Title = name;
            contentversion.VersionData = body;
            
            return contentversion;
        }
        catch ( Exception e )
        {
			system.debug('########### Rejected' );
            exceptionMessage = OneUnitedUtilities.parseStackTrace ( e ) ;
            contentversion =null;
            return contentversion;
        }
    }
    
    private static ContentDocumentLink CreatesContentDocumentLink(Id contentDocumentId, Id parentId){
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ShareType = 'V'; 
        contentDocumentLink.Visibility = 'AllUsers';
        return contentDocumentLink;
	}

    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
    {        
        Messaging.InboundEmailResult Finale = new Messaging.InboundEmailResult();
        
        Case emailCase = new Case();
        String SenderF = '';
        String SenderFname;
        boolean unfilled = false;
        String SenderL = '';
        String SenderLname;
        boolean HasFirst = false;
        
        String SubLine = FindWingdings(email.subject);
        emailCase.Subject = Subline;
        List<String> NameBuff;
        if(String.isNotBlank(email.fromName))
        {
            String NameThing = FindWingdings(email.fromName);
            NameBuff = NameThing.split('\\s+');
        }
        else
        {
            SenderF = '(No First Name Given)';
            SenderL = '(No Surname Given)';
        }
        
        if(NameBuff != null && !NameBuff.isEmpty())
        {
            if(NameBuff.size() == 2)
            {
                SenderF = NameBuff[0];
                SenderL = NameBuff[1];
            }
            else if(NameBuff.size() >= 3 && (NameBuff.contains('jr') || NameBuff.contains(' jr') || NameBuff.contains('Jr') || NameBuff.contains(' Jr')))
            {//________handling the use of jr________        
                NameBuff.remove((NameBuff.size())-1);
            }
            else if(NameBuff.size() >= 3 && (NameBuff.contains('sr') || NameBuff.contains(' sr') || NameBuff.contains('Sr') || NameBuff.contains(' Sr')))
            {//________handling the use of sr________        
                NameBuff.remove((NameBuff.size())-1);
            }
            else if(NameBuff.size() >= 3)
            {
                Integer FirstInit;
                Integer LastInit;
                for(Integer i = 0; i < NameBuff.size(); i++)
                {
                    Matcher Caps = Pattern.compile('\\p{Upper}').matcher(NameBuff[i]);
                    if(FirstInit == null && Caps.find())
                    {
                        FirstInit = i;
                    }
                    else if(Caps.find())
                    {
                        LastInit = i;
                    }
                }
                if(FirstInit != null)
                {
                    for(Integer i = 0; i < LastInit; i++)
                    {
                        SenderF = SenderF+NameBuff[i];
                        HasFirst = true;
                    }                    
                }
                if(LastInit != null)
                {
                    for(Integer i = LastInit; i < NameBuff.size(); i++)
                    {
                        SenderL = SenderL+NameBuff[i];
                    }
                }
                
                
            }
            else if(NameBuff.size() == 1)
            {
                SenderF = NameBuff[0];
                SenderL = '(No Surname Given)';
            }
            else
            {
                SenderF = '(No First Name Given)';
                SenderL = '(No Surname Given)';
            }
            
        }
        if(!String.isBlank(SenderF) && HasFirst && SenderF.containsWhitespace())
        {
            //________handling whitespaces________
            SenderF.remove(' ');            
        }       

        if(String.isNotBlank(SenderF) && HasFirst)
        {// Check for blank
            SenderF = FindWingdings(SenderF);
            TestFname = SenderF;
        }
		if(String.isNotBlank(SenderL) && !SenderL.equalsIgnoreCase('(No Surname Given)'))
        {
            SenderL = FindWingdings(SenderL);
            if(SenderL.containsWhitespace())
            {
                Matcher blanks = Pattern.compile('\\p{Space}').matcher(SenderF);// Create a pattern that hones in on WS's
                blanks.replaceAll(''); // Remove Whitespaces using the pattern/matcher
            }
            TestLname = SenderL;
        }
        
//		________final check________
        SenderFname = SenderF;
        
        TestFname = SenderFname;
        

//________________Sender Last name error management____________________    
        if(SenderL.equalsIgnoreCase('(No Surname Given)'))
        {
            SenderLname = '(No Surname Given)';
            TestLname = SenderLname;
            //System.debug('Last name came out blank.');
        }
        else
        {
            TestLname = SenderL;
            SenderLname = SenderL;
        }
        if(!String.isBlank(SenderF))
        {
            SenderFname = SenderF;
        }
        
//________________Character Limit enforcement and error handling________________
        // Using a char buffer to enforce the character limit and error czech but keep messages
        String buffer;
        if(String.isNotBlank(email.plainTextBody))
        {
            buffer = FindWingdings(email.plainTextBody);
        }
        else if(String.isNotBlank(email.htmlBody))
        {
            buffer = email.htmlBody;
        }
        else
        {
            buffer = '(No Message Body)';
        }
        TestBody = buffer;
        emailCase.Description = buffer;
         
        //contact     
        
// ________END OF SECTION________        
        String SenderAddress = envelope.fromAddress;
        TestAddress = SenderAddress;

//___________Contact_____
    	Contact contact =  getContactDetails( SenderFname, SenderLname , SenderAddress);
        if(contact != null){
            emailCase.ContactId = Contact.Id;
        }        
 
        
//________________Finalize The Case________________
        insert emailCase;       
  	     
       if(email.binaryAttachments != null && email.binaryAttachments.size() > 0)
        {
            List<ContentVersion> contentversions = new List<ContentVersion>();
            
            for(integer foo = 0; foo < email.binaryAttachments.size(); foo++)
            {
                ContentVersion contentVersion = createContentVersion(emailCase.Subject ,email.binaryAttachments[foo].Filename , email.binaryAttachments[foo].body);
          	  	contentversions.add(contentVersion);
            }
            insert contentversions;
            
            List<ContentDocumentLink> contentdocumentlinks = new List<ContentDocumentLink>();
            try{
                
                contentversions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :contentversions];
            	 for ( ContentVersion v : contentversions ) {
                	ContentDocumentLink contentDocumentLink = CreatesContentDocumentLink(v.ContentDocumentId,emailCase.Id);
        			contentdocumentlinks.add(contentDocumentLink);
            	}
            	insert contentdocumentlinks;
                
            }catch(DmlException e){
                System.debug('Error getting contentversion data  and inserting link' + e);
            }
            
        }  
          
        Finale.success = true;
       	return Finale;
    }

}