public class ApplicationTagCalloutQueue implements Queueable, Database.AllowsCallouts
{
    private List<String> appTagIDList ;
    
    public static final integer CALLOUT_LIMIT = 9 ;
    
    public ApplicationTagCalloutQueue ( List<String> atL ) 
    {
        this.appTagIDList = atL ;
    }
    
    public void execute(QueueableContext context) 
    {
        List<Application_Tag__c> atL = null ;
        List<Application_Tag__c> atU = new list<Application_Tag__c> () ;
        
        try
        {
        	atL = [
                SELECT ID, Application__c,
                minFraud_Tag_Type__c, Application_minFraud_ID__c, Application_IP_Address__c
                FROM Application_Tag__c
                WHERE ID IN :appTagIDList
            ] ;
        }
        catch ( Exception e )
        {
            // ?
        }
        
        if ( ( atL != null ) && ( atL.size () > 0 ) )
        {
            for ( Application_Tag__c at : atL )
	        {
                IP_Risk_helper ip = new IP_Risk_helper ( at ) ;
                ip.reportChargeback () ;
                
                atU.add ( at ) ;
            }
        }
        
        if ( ( atU != null ) && ( atU.size () > 0 ) )
            update atU ;
    }
}