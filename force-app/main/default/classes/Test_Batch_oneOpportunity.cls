@isTest
public with sharing class Test_Batch_oneOpportunity 
{
	static private void setBatchSettings ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
		
		insert bps ;
	}
	
	private static Contact getContact ()
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' ;
		c.LastName = 'Person' ;
		c.Email = 'test.person@testing.com' ;
        c.Phone = '123-456-7890' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		
		insert c ;
		
		return c ;		
	}
	
	private static oneOpportunity__c getOpportunity ( Contact c, String product )
	{		
		String rType = OneUnitedUtilities.getRecordTypeId ( 'Deposit Opportunity' ) ;
		
		oneOpportunity__c oo = new oneOpportunity__c () ;
		oo.Opportunity_Contact__c = c.id ;
		oo.RecordTypeId = rType ;
		oo.First_Name__c = c.FirstName + ' ' + c.LastName ;
		oo.Status__c = 'Open' ;
		oo.Interested_in_Product__c = product ;
		oo.Home_Phone__c = c.Phone ;
		
		insert oo ;
		
		return oo ;
	}
	
	private static Task getTask ( String id, String status )
	{
		Task t = new Task () ;
		t.whatId = id ;
		t.Status = status ;
		t.Subject = 'Test Task' ;
		t.Priority = 'Normal' ;
		
		insert t ;
		
		return t ;
	}
	
	static testmethod void oppTest ()
	{
		setBatchSettings () ;
		
		//  --------------------------------------------------
		//  Setup
		//  --------------------------------------------------
		Contact c = getContact () ;
				
		String rtId = OneUnitedUtilities.getRecordTypeId ( 'Deposit Opportunity' ) ;
		
		oneOpportunity__c oo1 = getOpportunity ( c, 'Savings' ) ;
		Task t1 = getTask ( oo1.id, 'Open' ) ;
		
		oneOpportunity__c oo2 = getOpportunity ( c, 'Savings' ) ;
		Task t2 = getTask ( oo1.id, 'Completed' ) ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		Test.startTest() ;
		
		batch_oneOpportunity b = new batch_oneOpportunity () ;
		Database.executeBatch ( b ) ;
		
		Test.stopTest() ;
		
		//  --------------------------------------------------
		//  Confirmation
		//  --------------------------------------------------
		
	}
}