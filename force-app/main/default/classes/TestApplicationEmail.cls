@isTest
public class TestApplicationEmail 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;

	/* --------------------------------------------------------------------------------
	 * Settings
	 * -------------------------------------------------------------------------------- */
    private static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group();
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Rates_URL__c = 'https://www.oneunited.com/disclosures/unity-visa' ;
        s.Bill_Code__c = 'BLC0001' ;
        s.Terms_ID__c = 'C0008541' ;
        s.Live_Agent_Button_ID__c = '573R0000000WXYZ' ;
        s.Live_Agent_Chat_URL__c = 'https://d.la2cs.salesforceliveagent.com/chat' ;
        s.Live_Agent_Company_ID__c = '00DR0000001yR1K' ;
        s.Live_Agent_Deployment_ID__c = '572R00000000001' ;
        s.Live_Agent_JS_URL__c = 'https://c.la2cs.salesforceliveagent.com/content/g/js/35.0/deployment.js' ;
        s.Live_Agent_Enable__c = false ;
        
        //  FIS STUFF
        s.FIS_Aquirer_ID__c = '059187' ;
        s.FIS_Authentication_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/authen' ;
        s.FIS_IDA_Config_Key__c = 'idaaliaskey' ;
        s.FIS_IDA_Endpoint__c = 'https://penleyincqa.penleyinc.com/fissoap1/services/ida' ;
        s.FIS_Location_ID__c = 'UNITY Visa Application' ;
        s.FIS_Staging_Flag__c = true ;
        s.FIS_User_ID__c = '02280828' ;
        s.FIS_User_Password__c = 'Z26ZjcZdZAiZXaZ' ;
        
        insert s;
    }
    
	/* --------------------------------------------------------------------------------
	 * Setup, Contact
	 * -------------------------------------------------------------------------------- */
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '13245600' + String.valueOf ( x ) ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
		
		return c ;
	}
	
	/* --------------------------------------------------------------------------------
	 * Creates a generic application I use for testing.
	 * -------------------------------------------------------------------------------- */
	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, ID recordTypeID )
	{
		one_application__c app = new one_application__c ();
        app.RecordTypeId = recordTypeID ;
		
		app.Contact__c = c1.Id;
		
		app.Number_of_Applicants__c = 'I will be applying individually' ;
        
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
        app.DOB__c = c1.Birthdate ;
        app.SSN__c = '666-01-1111' ;
        String p1 = one_utils.formatPhone ( c1.Phone ) ;
        app.Phone__c = p1 ;
        
        app.Street_Address__c = c1.MailingStreet ;
        app.City__c = c1.MailingCity ;
        app.State__c = c1.MailingState ;
        app.ZIP_Code__c = c1.MailingPostalCode ;
        app.Mailing_Address_Same__c = 'Yes' ;
        
        app.PrimID_Number__c = 'X00001' ;
        app.PrimID_State__c = 'CA' ;
		
        app.FIS_Decision_Code__c = 'F050' ;
        app.minFraud_Disposition__c = 'MANUAL_REVIEW' ;
        app.minFraud_ID__c = '1234' ;
        
		if ( c2 != null )
		{
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
            
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
            app.Co_Applicant_DOB__c = c2.Birthdate ;
            app.Co_Applicant_SSN__c = '666-02-1111' ;
	        String p2 = one_utils.formatPhone ( c2.Phone ) ;
            app.Co_Applicant_Phone__c = p2 ;
            
            app.Co_Applicant_Street_Address__c = c2.MailingStreet ;
            app.Co_Applicant_City__c = c2.MailingCity ;
            app.Co_Applicant_State__c = c2.MailingState ;
            app.Co_Applicant_ZIP_Code__c = c2.MailingPostalCode ;
            
            app.PrimID_Co_Applicant_Number__c = 'X00002' ;
            app.PrimID_Co_Applicant_State__c = 'CA' ;
            
			app.OFAC_CoApplicant_Decision__c = 'PASS' ;
            app.OFAC_CoApplicant_Run_Datetime__c = Datetime.now () ;
            app.OFAC_CoApplicant_Transaction_ID__c = '11223344' ;

            app.IDV_CoApplicant_Decision__c = 'PASS' ;
            app.IDV_CoApplicant_Run_DateTime__c = System.now () ;
            app.IDV_CoApplicant_Transaction_ID__c = '11223344' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Funding_Options__c = 'E- Check (ACH)' ;
		app.Gross_Income_Monthly__c = 10000.00 ;

		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        app.OUB_Fraud_Disposition__c = 'WARN' ;
        
        app.OFAC_Decision__c = 'FAIL' ;
        app.OFAC_Transaction_ID__c = '1324' ;
        app.OFAC_CoApplicant_Decision__c = 'FAIL' ;
        app.OFAC_CoApplicant_Transaction_ID__c = '1343' ;
        
        app.IDV_Decision__c = 'FAIL' ;
        app.IDV_Transaction_ID__c = '1234' ;
        app.IDV_CoApplicant_Decision__c = 'FAIL' ;
        app.IDV_CoApplicant_Transaction_ID__c = '3435' ;
        
        app.QualiFile_Decision__c = 'Review' ;
        app.QualiFile_CoApp_Decision__c = 'Review' ;
        
        app.Photo_ID_Decision__c = 'WARN' ;
        
		return app ;
	}

	public static testmethod void testONE ()
    {
        //  Setup
        insertCustomSetting () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        Ticket__c t = new Ticket__c () ;
        insert t ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Unity Visa', 'one_application__c' ) ;
        
        one_Application__c app = getApplicationForTests ( c1, c2, rt ) ;
        app.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
        app.Trial_Deposit_Batch_Date__c = Date.today () ;
        app.Trial_Deposit_Trace_ID__c = '112233' ;
        app.Trial_Deposit_Batch_Ticket__c = t.ID ;
        
        insert app ;
        
        Test.startTest () ;
        
		one_application__c appX = ApplicationEmail.getApplication ( app.ID ) ;
        
        map<String,boolean> fooMap = ApplicationEmail.getEmailStates ( appX ) ;
        System.assertEquals ( fooMap.get ( 'sendTrialEmail' ), true ) ;
        
        ApplicationEmail.sendTrialDepositEmail ( app ) ;
        
        Test.stopTest () ;
    }
    
	public static testmethod void testTWO ()
    {
        //  Setup
        insertCustomSetting () ;
        
        Contact c1 = getContact ( 1 ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2 ) ;
        insert c2 ;
        
        Ticket__c t = new Ticket__c () ;
        insert t ;
        
        ID rt = OneUnitedUtilities.getRecordTypeIdForObject ( 'Unity Visa', 'one_application__c' ) ;
        
        one_Application__c app = getApplicationForTests ( c1, c2, rt ) ;
        app.ACH_Funding_Status__c = 'Send Trial Deposit' ;
        app.Trial_Deposit_Batch_Date__c = Date.today () ;
        app.Trial_Deposit_Trace_ID__c = '112233' ;
        app.Trial_Deposit_Batch_Ticket__c = t.ID ;
        
        insert app ;
        
        Test.startTest () ;
        
		one_application__c appX = ApplicationEmail.getApplication ( app.ID ) ;
        
        map<String,boolean> fooMap = ApplicationEmail.getEmailStates ( appX ) ;
        System.assertEquals ( fooMap.get ( 'sendTrialEmail' ), true ) ;
        
        ApplicationEmail.sendTrialDepositEmail ( app ) ;
        
        Test.stopTest () ;
    }
}