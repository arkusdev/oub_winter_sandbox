@isTest
public class Test_batch_Deactivate_Advocate_Users 
{
    @testSetup
	static void setup ()
	{
		Batch_Process_Settings__c bps = new Batch_Process_Settings__c () ;
		bps.Name = 'OneUnited_Batch' ;
		bps.Batch_Email__c = 'test@oneunited.com' ;
		bps.Email_Enabled__c = true ;
        bps.Advocate_User_Days_Inactive__c = 120.0 ;
		
		insert bps ;
	}
    
	private static Branch__c createBranch ()
	{
		Branch__c b = new Branch__c () ;
		b.Name = 'TEST Branch' ;
		b.On_boarding_Journey__c = false ;
		b.State_CD__c = 'MA' ;
		
		return b ;
	}
    
    private static Account createAccount ( Integer i )
    {
        Account a = new Account () ;
        a.Name = 'Account ' + i ;
        
        return a ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * Load and return a contact
	 * ----------------------------------------------------------------------------------- */
	private static Contact createContact ( Integer i, Account a, Branch__c b, String customerRecordTypeID )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + i ;
		c.LastName = 'User' + i ;
		c.Email = 'test'+ i + '.user' + i + '@testing.com' ;
		
		c.MailingStreet = '100 Franklin Street' ;
		c.MailingCity = 'Boston' ;
		c.MailingState = 'MA' ;
		c.MailingPostalCode = '02110' ;
		c.Branch__c = b.ID ;
        c.AccountId = a.ID ;
		c.RecordTypeID = customerRecordTypeID ;
		
		return c ;
	}
    
    private static UserRole getUserRole ()
    {
        UserRole ur = [
            SELECT ID
            FROM UserRole
            WHERE Name = 'Administrator'
        ] ;
        
        return ur ;
    }
    
    private static Profile getProfile ( String name )
    {
		Profile p = [
            SELECT ID 
            FROM Profile 
            WHERE Name = :name
        ] ;
        
        return p ;
    }

    private static User createUser ( Contact c, Profile p, UserRole ur )
    {        
        User u = new User () ;
        u.ProfileId = p.ID ;
        u.TimeZoneSidKey = 'GMT' ;
        u.LocaleSidKey = 'en_US' ;
        u.EmailEncodingKey = 'UTF-8' ;
        u.IsActive = true ;
        u.LanguageLocaleKey = 'en_US' ;
        
        if ( ur != null )
            u.UserRoleId = ur.ID ;

        if ( c.ID != null )
	        u.ContactId = c.ID ;
        
        u.FirstName = c.FirstName ;
        u.LastName = c.LastName ;
        u.Email = c.Email ;
        u.Username = c.Email ;
        
        String alias = c.firstName.charAt ( 0 ) + c.LastName ;
        u.Alias = alias.left ( 8 ) ;
        u.CommunityNickname = u.Alias ;
        
        return u ;
    }
    
    public static testmethod void TEST_BATCH ()
    {
		String customerRecordTypeID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Customer', 'Contact' ) ;
        
        Account a1 = createAccount ( 1 ) ;
        insert a1 ;
        
        Branch__c b1 = createBranch () ;
        insert b1 ;
        
		Contact c1 = createContact ( 1, a1, b1, customerRecordTypeID ) ; // DO NOT INSERT!
        
		Contact c2 = createContact ( 2, a1, b1, customerRecordTypeID ) ;
        insert c2 ;
        
        User uWTF = [ SELECT ID FROM User WHERE Name = 'SalesForce Automation' ] ;
        
        User u1 ;
        System.runAS ( uWTF )
        {
            UserRole ur1 = getUserRole () ;
            Profile p1 = getProfile ( 'System Administrator' ) ;
            
            u1 = createUser ( c1, p1, ur1 ) ;
            insert u1 ;
        }

        User u2 ;
        System.runAS ( u1 )
        {
	        Profile p2 = getProfile ( 'OneUnited Advocate Apps User' ) ;
        
            u2 = createUser ( c2, p2, null ) ;
            insert u2 ;
        }
        
        Test.startTest () ;
        
        batch_Deactivate_Advocate_Users b = new batch_Deactivate_Advocate_Users () ;
  		Database.executeBatch ( b ) ;
        
        Test.stopTest () ;
        
        User u2x = [
            SELECT ID, IsActive
            FROM User
            WHERE ID = :u2.ID
        ] ;
        
        System.assertEquals ( u2x.IsActive, false ) ;
    }
}