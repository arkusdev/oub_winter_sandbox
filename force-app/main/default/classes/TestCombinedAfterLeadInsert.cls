@isTest
private class TestCombinedAfterLeadInsert
{
    private static void setupTesting ()
    {
        list<OneCampaign__c> ocL = new list<OneCampaign__c> () ;
        OneCampaign__c oc1 = new OneCampaign__c () ;
        oc1.name = 'MC OneTransaction A Will' ;
        oc1.Active__c = true ;
        ocL.add ( oc1 ) ;
        
        insert ocL ;
        
        Workshop__c w1 = new Workshop__c () ;
        w1.Name = 'Crenshaw RSVP' ;
        
        insert w1 ;
        
        Workshop__c w2 = new Workshop__c () ;
        w1.Name = 'OneTransaction' ;
        
        insert w2 ;
        
        //  Settings
        Web_Settings__c ws = new Web_Settings__c () ;
        ws.Name = 'Web Forms' ;
        ws.Crenshaw_RSVP_Workshop_ID__c = w1.id ;
    	ws.Workshop_Contact_Record_Type__c = 'Prospect' ;
        ws.OneTransaction_Workshop_ID__c = w2.id ;
        
        insert ws ;
    }
    
    static testMethod void notAWebsiteLead () 
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test' ;
        l.LastName = 'Test' ;
        l.Company = 'NOYOU' ;
        l.Email = 'test@test.com' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        insert l ;
    }
    
    static testMethod void incomingExistingContact_Case ()
    {
        Contact c = new Contact () ;
        c.FirstName = 'Test1' ;
        c.LastName = 'Test1' ;
        c.Email = 'test@test.com' ;
        
        insert c ;
        
        Lead l = new Lead () ;
        l.FirstName = 'Test1' ;
        l.LastName = 'Test1' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test@test.com' ;
        l.Website_Form__c = 'Customer Support' ;
        
        insert l ;      
    }

    static testMethod void incomingExistingLead_Case ()
    {
        Lead pL = new Lead () ;
        pL.Title = 'Mr.' ;
        pL.FirstName = 'Test2' ;
        pL.LastName = 'Test2' ;
        pL.Company = 'NOYOU' ;
        pL.Email = 'test2@test.com' ;
        
        insert pL ;
        
        Lead l = new Lead () ;
        l.FirstName = 'Test2' ;
        l.LastName = 'Test2' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        l.Country = 'USA' ;
        l.MobilePhone = '123-456-7890' ;
        
        l.Description = 'FooBar' ;
        
        l.Email = 'test2@test.com' ;
        l.Website_Form__c = 'Mortgages' ;
        
        insert l ;
    }

    static testMethod void incomingNewLead_Case ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test99' ;
        l.LastName = 'Test99' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Email = 'test99@test.com' ;
        l.Website_Form__c = 'Consumer Mortgage' ;
        
        insert l ;
    }
    
    static testMethod void incomingNewLead_Case_2 ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test1' ;
        l.LastName = 'Test1' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test@test.com' ;
        l.Website_Form__c = 'Contributions' ;
        
        insert l ;
    }

    static testMethod void incomingNewLead_Case_3 ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test1' ;
        l.LastName = 'Test1' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test@test.com' ;
        l.Website_Form__c = 'MultiFamily Mortgage' ;
        
        insert l ;
    }

    static testMethod void incomingExistingContact_Task ()
    {
        Contact c = new Contact () ;
        c.FirstName = 'Test1' ;
        c.LastName = 'Test1' ;
        c.Email = 'test@test.com' ;
        
        insert c ;
        
        Lead l = new Lead () ;
        l.FirstName = 'Test1' ;
        l.LastName = 'Test1' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test@test.com' ;
        l.Website_Form__c = 'Open Account' ;
        
        insert l ;
    }

    static testMethod void incomingExistingLead_Task ()
    {
        Lead pL = new Lead () ;
        pL.FirstName = 'Test2' ;
        pL.LastName = 'Test2' ;
        pL.Company = 'NOYOU' ;
        pL.Email = 'test2@test.com' ;
        
        insert pL ;
        
        Lead l = new Lead () ;
        l.FirstName = 'Test2' ;
        l.LastName = 'Test2' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test2@test.com' ;
        l.Website_Form__c = 'Open Account' ;
        
        insert l ;
    }

    static testMethod void incomingNewLead_Task ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test99' ;
        l.LastName = 'Test99' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test99@test.com' ;
        l.Website_Form__c = 'Stay Connected' ;
        
        insert l ;
    }

    static testMethod void incomingNewLead_Task_2 ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test99' ;
        l.LastName = 'Test99' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test99@test.com' ;
        l.Website_Form__c = 'UNITY Visa' ;
        
        insert l ;
    }
    
    static testMethod void incomingNewLead_Task_3 ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test98' ;
        l.LastName = 'Test98' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Street = '1313 Test Street' ;
        l.City = 'Test City' ;
        l.State = 'CA' ;
        l.PostalCode = '90210' ;
        
        l.Email = 'test98@test.com' ;
        l.Website_Form__c = 'Connections Matter' ;
        
        insert l ;
    }
    
    static testMethod void incomingNewLead_Workshop_Attendee ()
    {
    	setupTesting () ;
    	
        Lead l = new Lead () ;
        l.FirstName = 'Test97' ;
        l.LastName = 'Test97' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Email = 'test97@test.com' ;
        l.Website_Form__c = 'Grand ReOpening' ;
        
        insert l ;
    }
    
    static testMethod void preExistingLead ()
    {
    	setupTesting () ;
    	
        Lead l = new Lead () ;
        l.Title = 'Mr.' ;
        l.FirstName = 'Test97' ;
        l.LastName = 'Test97' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Email = 'test97@test.com' ;
        
        insert l ;
        
        Lead l2 = new Lead () ;
        l2.Title = 'Mz.' ;
        l2.FirstName = 'Test97' ;
        l2.LastName = 'Test97' ;
        l2.Company = 'NOYOU' ;
        l2.Phone = '123-456-7890' ;
        
        l2.Email = 'test97@test.com' ;
        l2.Website_Form__c = 'Grand ReOpening' ;
        
        insert l2 ;
    }
    
    static testMethod void preExistingContact ()
    {
    	setupTesting () ;
    	
        Contact c = new Contact () ;
        c.FirstName = 'Test97' ;
        c.LastName = 'Test97' ;
        
        c.Email = 'test97@test.com' ;
        
        insert c ;
        
        Lead l2 = new Lead () ;
        l2.FirstName = 'Test97' ;
        l2.LastName = 'Test97' ;
        l2.Company = 'NOYOU' ;
        l2.Phone = '123-456-7890' ;
        
        l2.Email = 'test97@test.com' ;
        l2.Website_Form__c = 'Grand ReOpening' ;
        
        insert l2 ;
    }
    
    static testmethod void smsPhoneHeadache ()
    {
        Lead l = new Lead () ;
        l.FirstName = 'Test' ;
        l.LastName = 'User' ;
        l.Company = 'Nyet' ;
        l.MobilePhone = '123-456-7890' ;
        
        insert l ;
        
        l.MobilePhone = '890-567-1234' ;
        
        update l ;
    }
    
    static testMethod void incomingNewLead_OneTransaction ()
    {
    	setupTesting () ;
    	
        test.startTest() ;
        
        Lead l = new Lead () ;
        l.FirstName = 'Test97' ;
        l.LastName = 'Test97' ;
        l.Company = 'NOYOU' ;
        l.Phone = '123-456-7890' ;
        
        l.Email = 'testOT1@testOT.com' ;
        l.Website_Form__c = 'OneTransaction' ;
        l.OneTransaction_Focus__c = 'A Will' ;
        
        insert l ;
        
        test.stopTest() ;
        
        Contact c1 = [
            SELECT ID, Website_Form__c, OneTransaction_Focus__c
            FROM Contact
            WHERE Email = 'testOT1@testOT.com'
        ] ;
        
        OneCampaign_Member__c ocm1 = [
            SELECT ID
            FROM OneCampaign_Member__c
            WHERE Contact__c = :c1.ID
        ] ;
        
        System.assertNotEquals ( ocm1, null ) ;
    }
}