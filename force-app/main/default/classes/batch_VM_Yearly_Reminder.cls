global class batch_VM_Yearly_Reminder implements Database.Batchable<SObject>, Database.Stateful
{
	private Vendor_Management_Settings__c vms ;
	private Batch_Process_Settings__c bps ;
	
	global batch_VM_Yearly_Reminder ()
	{
		vms = Vendor_Management_Settings__c.getInstance ( 'VM_Settings' ) ;	
		bps = Batch_Process_Settings__c.getInstance ( 'OneUnited_Batch' ) ;
	}

	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query ;
		
		query  = '' ;
		query += 'SELECT ID, ' ;
		query += 'Name, ' ;
		query += 'OwnerID, ' ;
		query += 'Vendor_Status__c ' ;
		query += 'FROM ' ;
		query += 'Vendor_Management__c ' ;
		query += 'WHERE ' ;
        query += 'RecordType.Name <> \'Service Provider\' ' ;
		if ( ( vms != null ) && ( String.isNotBlank ( vms.Batch_Exclude_Status__c ) ) )
			query += 'AND Vendor_Status__c <> \'' + vms.Batch_Exclude_Status__c + '\' ' ;
		else
			query += 'AND Vendor_Status__c <> \'Closed\' ' ;
		query += 'ORDER BY ' ;
		query += 'OwnerID ' ;
		
		query += '' ;
		
		return Database.getQueryLocator ( query ) ;
	}
	
	global void execute ( Database.Batchablecontext bc, List<Vendor_Management__c> iL )
	{
		//  User System Map
		Map<String,User> uMap = OneUnitedUtilities.getActiveUsers () ;

		//  Ouput Map of distinct users who get the email
		Map<String, User> oMap = new Map<String, User> () ;
		
		for ( Vendor_Management__c vm : iL )
		{
			User u = uMap.get ( vm.OwnerID ) ;
			
			if ( u != null )
			{
				if ( ! oMap.containsKey ( u.Id ) )
				{
					oMap.put ( u.Id, u ) ;
				}
			}
		}
		
		List<Messaging.SingleEmailMessage> mL = new List<Messaging.SingleEmailMessage> () ;
		
		//  User loop
		for ( String ID : oMap.keyset () )
		{
			User u = oMap.get ( ID ) ;
		
			//  Create message for each user
			Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage () ;
			
			List<String> toAddresses = new List<String> () ;
			
			if ( vms != null )
			{
				if ( String.IsNotBlank ( vms.Email_Override__c ) )
					toAddresses.add ( vms.Email_Override__c ) ;
				else
					toAddresses.add ( u.Email ) ;
					
				if ( String.IsNotBlank ( vms.Email_CC__c ) )
					toAddresses.add ( vms.Email_CC__c ) ;
			}
			else
			{
				toAddresses.add ( u.Email ) ;
			}
			
			m.setToAddresses ( toAddresses ) ;
			m.setSubject ( 'Yearly GLBA Control Assessment Notice' ) ;
	
			String message ;

			message  = '' ;
			message += '<html>' ;
			message += '<body>' ;
			message += '<p>' + u.Name + ',</p>';
			message += '<p>This is your annual reminder to complete your GLBA department and GLBA vendor control assessments.' ;
			message += 'The GLBA task force reports on the findings in these control assessments on an annual basis ' ;
			message += 'and the report will be presented to the board in July.</p>' ;

			message += '<p>The current control assessments for your department can be found in the Vendor Management Center ' ;
			message += 'in Salesforce:</p>' ;
			
			message += URL.getSalesforceBaseUrl ().toExternalForm () + '/' ;
			
			if ( ( vms != null ) && ( String.IsNotBlank ( vms.Landing_ID__c ) ) )
				message += vms.Landing_ID__c ;
			
			message += '<br><br>' ;
			message += '<p>Please ensure that your control assessments are up to date and all vendors requiring an ' ;
			message += 'assessment are accurately documented.</p>' ;
			
			message += '<p>Thank you.</p>' ;
			
			message += '<p>OneUnited Bank Privacy Officer</p>' ;
			message += '</body>' ;
			message += '</html>' ;
			
			message += '' ;
			//  Set the message
			m.setHTMLBody ( message ) ;
			
			//  Add to list
			mL.add ( m ) ;
		}
			
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
			Messaging.sendEmail ( mL ) ;
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( bps != null ) && ( String.isNotBlank ( bps.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				bps.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex VM Yearly Reminder Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( bps != null )
			{
				//  Only send email if enabled in custom settings
				if ( bps.Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}