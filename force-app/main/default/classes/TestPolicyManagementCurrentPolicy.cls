@isTest
public with sharing class TestPolicyManagementCurrentPolicy 
{
	public static testmethod void ONE ()
	{
		Policy_Management__c pm = new Policy_Management__c () ;
		pm.Name = 'TEST Policy' ;
		pm.Policy_ID__c = 123 ;
		
		insert pm ;
		
 		Attachment a1 = new Attachment () ;
		
		a1.Name = 'THINGY1.PDF' ;
		Blob bodyBlob1 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a1.body = bodyBlob1 ;
		a1.ParentId = pm.id ;
		a1.IsPrivate = false ;
		a1.ContentType = 'application/pdf' ;
		
		insert a1 ;
		
		Attachment a2 = new Attachment () ;
		
		a2.Name = 'THINGY2_BDA.PDF' ;
		Blob bodyBlob2 = Blob.valueOf ( 'Unit Test Attachment Body' ) ; 
		a2.body = bodyBlob2 ;
		a2.ParentId = pm.id ;
		a2.IsPrivate = false ;
		a2.ContentType = 'application/pdf' ;
		
		insert a2 ;
		
		//  --------------------------------------------------
		//  Testing
		//  --------------------------------------------------
		
		//  Point to page to test
		PageReference pr1 = Page.Policy_Management_Current_Policy ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( pm ) ;
        
		//  Feed the standard controller to the page controller
		PolicyManagementCurrentPolicy pfc = new PolicyManagementCurrentPolicy ( sc ) ;
		
		System.assertNotEquals ( pfc.displayBDA, null ) ;
		System.assertNotEquals ( pfc.loadFile (), null ) ;
		
		Test.stopTest () ;
	}
}