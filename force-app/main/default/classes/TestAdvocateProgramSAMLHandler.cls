@isTest
public class TestAdvocateProgramSAMLHandler 
{
	/* --------------------------------------------------------------------------------
	 * Miscellaneous
	 * -------------------------------------------------------------------------------- */
    private static Profile getProfile ()
    {
		Profile p = [
            SELECT ID
            FROM Profile
            WHERE Name = 'OneUnited Advocate Identity User'
        ] ;
        
        return p ;
    }
    
    private static Network getNetwork () 
    {
        Network n = [
            SELECT ID
            FROM Network 
//            WHERE Name = 'Customer'
              WHERE Name = 'Advocate'
        ] ;
        
        return n ;
    }
    
    private static UserRole getUserRole ()
    {
        UserRole ur = [
            SELECT ID
            FROM UserRole
//            WHERE Name = 'Customer'
            WHERE Name = 'Advocate'
        ] ;
        
        return ur ;
    }
    
	private static User getUser ()
	{
		Profile p = [
			SELECT id FROM Profile WHERE Name = 'System Administrator'
		] ;
		
		User u = new User () ;
        
		u.firstName = 'Test1' ;
		u.lastName = 'User1' ;
		u.email = 'test1@user.com' ;
		u.username = 'test1user1@user.com' ;
		u.alias = 'tuser1' ;
		u.CommunityNickname = 'tuser1' ;
		u.TimeZoneSidKey = 'GMT' ;
		u.LocaleSidKey = 'en_US' ;
		u.EmailEncodingKey = 'UTF-8' ;
		u.ProfileId = p.id ;
		u.LanguageLocaleKey = 'en_US' ;
        
		return u ;
	}
    
    private static Account getAccount ( Integer i )
    {
        Account a = new Account () ;
        a.name = 'Test ' +  i ;
        
        return a ;
    }
    
	private static Contact getContact ( Integer i )
	{
		Contact c = new Contact () ;
        
        String x = '' ;
        if ( i < 10 )
            x = '0' + i ;
        else
            x = '' + i ;
        
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
        c.Birthdate = Date.newInstance ( 1911, 01, i ) ;
        c.Phone = '132-456-00' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
        c.TaxID__c = '666' + x + '1111' ;
        
        c.MailingStreet = x + ' Street' ;
        c.MailingCity = 'City ' + x ;
        c.MailingState = 'CA' ;
        c.MailingPostalCode = '901' + x ;
        		
		return c ;
    }

	/* --------------------------------------------------------------------------------
	 * TEST - CREATE
	 * -------------------------------------------------------------------------------- */
    public static testmethod void JITCreateUser ()
    {
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.AccountId = a1.ID ;
        insert c1 ;
                
        ReferralCodeGenerator.generateReferralCode ( c1 ) ;
        
        Test.startTest () ;
        
        AdvocateProgramSAMLHandler ocsh = new AdvocateProgramSAMLHandler () ;
        
        map<String,String> attributes = new map<String,String> () ;
        attributes.put ( 'USER_ID', c1.TaxID__c ) ;
        
        Network n = getNetwork () ;
        
        User u1 = ocsh.createUser ( null, n.ID, null, c1.TaxID__c, attributes, 'xyz123abc456' ) ;
        insert u1 ;
        
        System.assertNotEquals ( u1, null ) ;
        System.assertEquals ( u1.FederationIdentifier, c1.TaxID__c ) ;
        
        Test.stopTest () ;
    }

	/* --------------------------------------------------------------------------------
	 * TEST - UPDATE - Active
	 * -------------------------------------------------------------------------------- */
    public static testmethod void JITUpdateUserActive ()
    {
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.AccountId = a1.ID ;
        insert c1 ;
        
        ReferralCodeGenerator.generateReferralCode ( c1 ) ;
        
        User uWTF = [ SELECT ID FROM User WHERE Name = 'SalesForce Automation' ] ;
        
        System.runas ( uWTF )
        {
            Profile p = getProfile () ;
            
            User u1 = getUser () ;
            u1.ProfileId = p.ID ;
            u1.ContactId = c1.ID ;
            u1.FederationIdentifier = c1.TaxID__c ;
            insert u1 ;
            
            Test.startTest () ;
            
            AdvocateProgramSAMLHandler ocsh = new AdvocateProgramSAMLHandler () ;
            
            map<String,String> attributes = new map<String,String> () ;
            attributes.put ( 'USER_ID', c1.TaxID__c ) ;
            
            Network n = getNetwork () ;
            
            ocsh.updateUser ( null, null, n.ID, null, c1.TaxID__c, attributes, 'xyz123abc456' ) ;
            
            Test.stopTest () ;
        }
    }
    
	/* --------------------------------------------------------------------------------
	 * TEST - UPDATE - Inactive
	 * -------------------------------------------------------------------------------- */
    public static testmethod void JITUpdateUserInactive ()
    {
        Account a1 = getAccount ( 1 ) ;
        insert a1 ;
        
        Contact c1 = getContact ( 1 ) ;
        c1.AccountId = a1.ID ;
        insert c1 ;
        
        ReferralCodeGenerator.generateReferralCode ( c1 ) ;
        
        User uWTF = [ SELECT ID FROM User WHERE Name = 'SalesForce Automation' ] ;
        
        User u1 ;
        System.runas ( uWTF )
        {
            Profile p = getProfile () ;
            
            u1 = getUser () ;
            u1.ProfileId = p.ID ;
            u1.ContactId = c1.ID ;
            u1.FederationIdentifier = c1.TaxID__c ;
            u1.IsActive = false ;
            insert u1 ;
            
            Test.startTest () ;
            
            AdvocateProgramSAMLHandler ocsh = new AdvocateProgramSAMLHandler () ;
            
            map<String,String> attributes = new map<String,String> () ;
            attributes.put ( 'USER_ID', c1.TaxID__c ) ;
            
            Network n = getNetwork () ;
            
            ocsh.updateUser ( null, null, n.ID, null, c1.TaxID__c, attributes, 'xyz123abc456' ) ;
            
            Test.stopTest () ;
        }
        
        User u1x = [
            SELECT ID, isActive
            FROM User
            WHERE ID = :u1.ID
            ] ;
        
        System.assertEquals ( u1x.IsActive, true ) ;
    }
}