public with sharing class application_Send_minFraud_Decision 
{
    /*  --------------------------------------------------------------------------------------------------
     *  Input
     *  -------------------------------------------------------------------------------------------------- */
    public String overrideReason { public get ; public set ; }
    public String overrideReasonError { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Form & Button display
     *  -------------------------------------------------------------------------------------------------- */
    public boolean canOverride { public get ; private set ; }
    public boolean overrideButton { public get ; private set ; }
    public boolean declineButton { public get ; private set ; }
    public boolean LOSOverride { public get ; private set ; }

    /*  --------------------------------------------------------------------------------------------------
     *  Application / Record Type IDs
     *  -------------------------------------------------------------------------------------------------- */
    private one_application__c app;

    /*  --------------------------------------------------------------------------------------------------
     *  Constructor
     *  -------------------------------------------------------------------------------------------------- */
    public application_Send_minFraud_Decision ( ApexPages.StandardController controller )
    {
        //  DEFAULTS
        overrideReason = 'Passed MinFraud' ;
        overrideReasonError = null ;
        
        canOverride = false ;
        overrideButton = false ;
        declineButton = false ;
        LOSOverride = false ;
        
        //  Get (possibly incomplete) application 
        this.app = (one_application__c) controller.getRecord () ;
        
        //  Reload application
        this.app = one_utils.getApplicationFromId ( app.Id ) ;
        
        //  Check for LOS - SHOULD NEVER HAPPEN
        if ( String.isNotBlank ( app.FIS_Application_ID__c ) )
        {
            LOSOverride = true ;
        }
        else
        {
            //  Have they been overridden?
            if ( ( String.isNotBlank ( app.minFraud_Disposition__c ) ) &&
                ( app.minFraud_Disposition__c.equalsIgnoreCase ( 'MANUAL_REVIEW' ) ) &&
                ( app.minFraud_Override_Datetime__c == null ) &&
                ( app.minFraud_Override_User__c == null ) )
            {
                //  Always decline
                declineButton = true ;
                
                //  Do they have a valid ID to override?
                if ( String.isNotBlank ( app.minFraud_ID__c ) )
                {
                    canOverride = true ;
                    overrideButton = true ;
                }
            }
        }
    }

    /*  --------------------------------------------------------------------------------------------------
     *  Page action, run the override
     *  -------------------------------------------------------------------------------------------------- */
    public pageReference runOverride ()
    {
        overrideReasonError = null ;
        
        if ( String.isEmpty ( overrideReason ) )
            overrideReasonError = 'Please enter an override reason' ;
        
        PageReference pr = null ;
        
        if ( String.isNotEmpty ( overrideReason ) )
        {
	        //  Return back to original application
            pr = new PageReference ( '/' + app.Id ) ;
            pr.setRedirect ( true ) ;
            
            //  Set up helper object
            FISHelper fish = new FISHelper () ;
            fish.setApplication ( app ) ;
            
            //  Override MinFRaud
            fish.overrideMinFraud ( overrideReason ) ;
            
            //  Jim's all-in-one special
            fish.runEverything () ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Decline
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference declineApplication ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        try
        {
            app.FIS_Decision_Code__c = 'D050' ;
            System.debug('Update app debugging 2');
            update app ;
        }
        catch ( Exception e )
        {
            System.debug ( 'WTF?!?' ) ;
        }
        
        return pr ;
    }
    
    /*  --------------------------------------------------------------------------------------------------
     *  Page action, Cancel ?!?
     *  -------------------------------------------------------------------------------------------------- */
    public PageReference cancel ()
    {
        PageReference pr = new pageReference ( '/' + app.Id ) ;
        pr.setRedirect ( true ) ;
        
        return pr ;
    }
}