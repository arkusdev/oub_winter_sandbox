@isTest
public with sharing class TestBankEventRegistration 
{
    private static void insertCustomSetting () 
    {
    	Web_Settings__c ws = new Web_Settings__c () ;
    	ws.Name = 'Web Forms' ;
    	ws.Access_Control_Origin__c = 'https://www.oneunited.com' ;
    	ws.Workshop_Contact_Record_Type__c = 'Prospect' ;
    	insert ws ;
    }
    
	private static Branch__c getBranch ()
	{
		return getBranch ( null ) ;
	}
	
	private static Branch__c getBranch ( String state )
	{
		// Workshops are at a branch
		Branch__c b = new Branch__c () ;
		b.Name = 'Test Branch' ;
		b.Address_A__c = '123 Test Street' ;
		b.City__c = 'Test City' ;
		b.State_CD__c = state ;
		b.Zip__c = '10001' ;
		
		insert b ;
		
		return b ;
	} 
	
	private static Workshop__c getWorkshop ( String x )
	{
		return getWorkshop ( x, null ) ;
	}
	
	private static Workshop__c getWorkshop ( String x, String y )
	{
		Branch__c b = getBranch ( y ) ;
				
		//  Create the workshop referring to the branch
		Workshop__c w = new Workshop__c () ;
		
		w.Name = 'Test Workshop ' + x ;
		w.Branch__c = b.id ;
		w.Public_Event__c = true ;
		w.Event_Start__c = System.now () ;
		w.Event_End__c = System.now ().addHours ( 1 ) ;
		w.State__c = y ;
		w.type__c = 'Smart Money' ;
		
		w.Event_Start__c = Datetime.now ().addDays ( 1 ) ;
		w.Event_End__c = Datetime.now ().addDays ( 1 ).addHours ( 1 ) ;
		
		insert w ;
		
		return w ;
	} 

	private static Contact setupContact () 
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'something@other.com' ;
		
		insert c ;
		
		return c ;
	}
	
	static testmethod void testNoWID ()
	{
		insertCustomSetting () ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Load test workshop
		Workshop__c w = getWorkshop ( 'C', 'CA' ) ;
		
		//  Empty controller
		ApexPages.Standardcontroller stdC = null ;
		
		//  Load class
		BankEventRegistration ber = new BankEventRegistration ( stdC ) ;
		
		//  Default to 3000 - display all events
		System.assertEquals ( ber.fStepNumber, 3000 ) ;
		
		//  Doesn't do anything - explicitly for code coverage!
		ber.pickWorkshop () ;
		
		//  End of testing
		Test.stopTest () ;
	}

	static testmethod void testBadWID ()
	{
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.BankEventRegistration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', 'BADWIDNOGO' );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        BankEventRegistration ber = new BankEventRegistration ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( ber.fStepNumber, 1500 ) ;
			
		//  End of testing
		Test.stopTest () ;
	}

	static testmethod void testNoExistingContactNoUTM ()
	{
		insertCustomSetting () ;
		
		//  Load test workshop
		Workshop__c w = getWorkshop ( 'C', 'CA' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.BankEventRegistration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        BankEventRegistration ber = new BankEventRegistration ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( ber.fStepNumber, 1000 ) ;
		
		//  Empty websiteform test
		ber.submit () ;
		
		//  Load the attendee object
		ber.wa.First_Name__c = 'Test' ;
		ber.wa.Last_Name__c = 'User' ;
		ber.wa.Phone_Number__c = '1234567890' ;
		ber.wa.Email_Address__c = 'something@other.com' ;
		ber.wa.Address__c = '1313 Mockingbird Lane' ;
		ber.wa.City__c = 'Boston' ;
		ber.wa.State__c = 'MA' ;
		ber.wa.Zip_Code__C = '12345' ;

		//  Submit
		ber.submit () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( ber.fStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testExistingContactNoUTM ()
	{
		insertCustomSetting () ;
		
		//  Load test workshop
		Workshop__c w = getWorkshop ( 'C', 'CA' ) ;
		
		//  Load test Contact
		Contact c = setupContact () ;
		
		//  Point to page to test
		PageReference pr1 = Page.BankEventRegistration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        BankEventRegistration ber = new BankEventRegistration ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( ber.fStepNumber, 1000 ) ;
		
		//  Empty websiteform test
		ber.submit () ;
		
		//  Load the attendee object
		ber.wa.First_Name__c = c.FirstName ;
		ber.wa.Last_Name__c = c.LastName ;
		ber.wa.Phone_Number__c = '1234567890' ;
		ber.wa.Email_Address__c = c.Email ;
		ber.wa.Address__c = '1313 Mockingbird Lane' ;
		ber.wa.City__c = 'Boston' ;
		ber.wa.State__c = 'MA' ;
		ber.wa.Zip_Code__C = '12345' ;

		//  Submit
		ber.submit () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( ber.fStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}

	static testmethod void testNoExistingContactUTM ()
	{
		insertCustomSetting () ;
		
		//  Load test workshop
		Workshop__c w = getWorkshop ( 'C', 'CA' ) ;
		
		//  Point to page to test
		PageReference pr1 = Page.BankEventRegistration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        BankEventRegistration ber = new BankEventRegistration ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( ber.fStepNumber, 1000 ) ;
		
		//  Empty websiteform test
		ber.submit () ;
		
		//  Load the attendee object
		ber.wa.First_Name__c = 'Test' ;
		ber.wa.Last_Name__c = 'User' ;
		ber.wa.Phone_Number__c = '1234567890' ;
		ber.wa.Email_Address__c = 'something@other.com' ;
		ber.wa.Address__c = '1313 Mockingbird Lane' ;
		ber.wa.City__c = 'Boston' ;
		ber.wa.State__c = 'MA' ;
		ber.wa.Zip_Code__C = '12345' ;

		//  Random UTM crap
		ber.utmCampaign = 'a' ;
		ber.utmContent = 'b' ;
		ber.utmMedium = 'c' ;
		ber.utmSource = 'd' ;
		ber.utmTerm = 'e' ;
		ber.utmVisitorId = 'f' ;
		
		//  Submit
		ber.submit () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( ber.fStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
	
	static testmethod void testExistingContactUTM ()
	{
		insertCustomSetting () ;
		
		//  Load test workshop
		Workshop__c w = getWorkshop ( 'C', 'CA' ) ;
		
		//  Load test Contact
		Contact c = setupContact () ;
		
		//  Point to page to test
		PageReference pr1 = Page.BankEventRegistration ;
		Test.setCurrentPage ( pr1 ) ;
		
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Pass in parameter manually
        ApexPages.currentPage().getParameters().put ( 'wid', w.id );
        
        //  Pointless empty workshop
        Workshop__c ew = new Workshop__c () ;
        
		//  Create standard controller object from the application
		ApexPages.StandardController sc = new ApexPages.StandardController ( ew ) ;

        //  Constructor
        BankEventRegistration ber = new BankEventRegistration ( sc ) ;
		
		//  Check the resulting controller object
		System.assertEquals ( ber.fStepNumber, 1000 ) ;
		
		//  Empty websiteform test
		ber.submit () ;
		
		//  Load the attendee object
		ber.wa.First_Name__c = c.FirstName ;
		ber.wa.Last_Name__c = c.LastName ;
		ber.wa.Phone_Number__c = '1234567890' ;
		ber.wa.Email_Address__c = c.Email ;
		ber.wa.Address__c = '1313 Mockingbird Lane' ;
		ber.wa.City__c = 'Boston' ;
		ber.wa.State__c = 'MA' ;
		ber.wa.Zip_Code__C = '12345' ;

		//  Random UTM crap
		ber.utmCampaign = 'a' ;
		ber.utmContent = 'b' ;
		ber.utmMedium = 'c' ;
		ber.utmSource = 'd' ;
		ber.utmTerm = 'e' ;
		ber.utmVisitorId = 'f' ;
		
		//  Submit
		ber.submit () ;
		
		//  Check to see we're in the right step
		System.assertEquals ( ber.fStepNumber, 2000 ) ;
		
		//  End of testing
		Test.stopTest () ;
	}
}