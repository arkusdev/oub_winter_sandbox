public with sharing class PublicEventCalendar 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
    public List<CalendarEvent> calEventList
    {
    	public get ;
    	private set ;
    }
    
    public String calEventJSON
    {
    	public get ;
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public PublicEventCalendar ()
	{
		calEventList = new List<CalendarEvent> () ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  On Load, ICAL
     *  ---------------------------------------------------------------------------------------- */
	public PageReference pageLoadICAL () 
	{
        //  Load from page
        String state = ApexPages.currentPage().getParameters().get('state') ;

		//  Defaults
		Date startDate = Date.newInstance ( Date.today().year()-1, 01, 01 ) ;
		Date endDate = Date.newInstance ( Date.today().year()+5, 01, 01 ) ;
		calEventJSON = null ;
		
		loadData ( startDate, endDate, state ) ;
		
		return null ;
	}

    /*  ----------------------------------------------------------------------------------------
     *  On Load, ICAL, Single
     *  ---------------------------------------------------------------------------------------- */
	public PageReference pageLoadICALSingle () 
	{
		String eID = ApexPages.currentPage().getParameters().get('eID') ;
		String state = ApexPages.currentPage().getParameters().get('state') ;
		
		//  Defaults
		calEventJSON = null ;
		
		loadDataSingle ( eID, state ) ;
		
		return null ;
	}

    /*  ----------------------------------------------------------------------------------------
     *  On Load, JSON
     *  ---------------------------------------------------------------------------------------- */
	public PageReference pageLoadJSON () 
	{
		//  Defaults
		Date startDate = null ;
		Date endDate = null ;
		
		Web_Settings__c ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;
		
		if ( ws != null )
		{
			String accessControlURL = ws.Access_Control_Origin__c ;
		
			if ( String.isNotBlank ( accessControlURL ) )
				ApexPages.currentPage().getHeaders().put ( 'Access-Control-Allow-Origin', accessControlURL ) ;
		}
				
		List<Workshop__c> workshopList = null ;
		
		String sD = ApexPages.currentPage().getParameters().get('start') ;
		
		if ( String.isNotBlank ( sD ) )
		{
			try
			{
				startDate = Date.valueOf ( sD ) ;
			}
			catch ( Exception e )
			{
				System.debug ( 'Bad start date passed in, ignoring' ) ;
				startDate = null ;
			} 
		}
		
		String eD = ApexPages.currentPage().getParameters().get('end') ;
		
		if ( String.isNotBlank ( eD ) )
		{
			try
			{
				endDate = Date.valueOf ( eD ) ;
			}
			catch ( Exception e )
			{
				System.debug ( 'Bad end date passed in, ignoring' ) ;
				endDate = null ;
			} 
		}
		
		String state = ApexPages.currentPage().getParameters().get('state') ;
		
		if ( startDate == null )
			startDate = Date.newInstance ( Date.today().year()-1, 01, 01 ) ;
			
		if ( endDate == null )
			endDate = Date.newInstance ( Date.today().year()+5, 01, 01 ) ;

		System.debug ( 'Start : ' + startDate ) ;
		System.debug ( 'End   : ' + endDate ) ;
		System.debug ( 'State : ' + state ) ;

		loadData ( startDate, endDate, state ) ;

		setFullCalendarJSONHACKBAD () ;

		return null ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  On Load, JSON, Single
     *  ---------------------------------------------------------------------------------------- */
	public PageReference pageLoadJSONSingle () 
	{
		//  Defaults
		Web_Settings__c ws = Web_Settings__c.getInstance ( 'Web Forms' ) ;
		
		if ( ws != null )
		{
			String accessControlURL = ws.Access_Control_Origin__c ;
		
			if ( String.isNotBlank ( accessControlURL ) )
				ApexPages.currentPage().getHeaders().put ( 'Access-Control-Allow-Origin', accessControlURL ) ;
		}
				
		List<Workshop__c> workshopList = null ;
		
		String eID = ApexPages.currentPage().getParameters().get('eID') ;
		
		String state = ApexPages.currentPage().getParameters().get('state') ;
		
		loadDataSingle ( eID, state ) ;

		setFullCalendarJSONHACKBAD () ;

		return null ;
	}
    
    /*  ----------------------------------------------------------------------------------------
     *  Data loader
     *  ---------------------------------------------------------------------------------------- */
    private void loadData ( Date startDate, Date endDate, String state )
    {
		List<Workshop__c> workshopList = null ;
		
		try
		{
			if ( String.isNotBlank ( state ) )
			{
				workshopList = [
					SELECT ID, Name, CreatedDate,
					Title__c, Workshop_Description__c, Description__c,
					Type__c, Branch_Name__c, Branch__r.State_CD__c,
					Address__c, Event_Start__c, Event_End__c, Timezone__c
					FROM Workshop__c
					WHERE Public_Event__c = true
					AND Event_Start__c >= :startDate
					AND Event_End__c <= :endDate
					AND Branch__r.State_CD__c = :state
					ORDER BY Event_Start__c
				] ;
			}
			else
			{
				workshopList = [
					SELECT ID, Name, CreatedDate,
					Title__c, Workshop_Description__c, Description__c,
					Type__c, Branch_Name__c, Branch__r.State_CD__c,
					Address__c, Event_Start__c, Event_End__c, Timezone__c
					FROM Workshop__c
					WHERE Public_Event__c = true
					AND Event_Start__c >= :startDate
					AND Event_End__c <= :endDate
					ORDER BY Event_Start__c
				] ;
			}
		}
		catch ( Exception e )
		{
			System.debug ( 'Exception?!?' ) ;
		}
			
		if ( ( workshopList != null ) && ( workshopList.size () > 0 ) )
		{
			System.debug ( 'Found ' + workshopList.size () + ' events to expose' ) ;
			
			for ( Workshop__c w : workshopList )
			{
				CalendarEvent ce = new CalendarEvent () ;
				
				ce.id = w.ID ;
				ce.uid = convertUID ( w.Event_Start__c ) + '-' + w.ID + '@oneunited.com' ;
				ce.Name = w.Name ;
				ce.Branch = w.Branch_Name__c ;
				ce.title = w.Title__c ;
				ce.createdDateTime = convertISO8601 ( w.CreatedDate, state ) ;
				ce.createdDateTimeICAL = convertICAL ( w.CreatedDate, state ) ;
				ce.startDateTime = convertISO8601 ( w.Event_Start__c, state ) ;
				ce.startDateTimeICAL = convertICAL ( w.Event_Start__c, state ) ;
				ce.endDateTime = convertISO8601 ( w.Event_End__c, state ) ;
				ce.endDateTimeICAL = convertICAL ( w.Event_End__c, state ) ;
				ce.description = w.Workshop_Description__c ;
				ce.url = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/oneunitedweb/BankEventPreRegistration?wid=' + w.ID ;
				ce.allDayEvent = 'false' ;
				ce.color = getColorFromEventType ( w.Type__c ) ;
				
				calEventList.add ( ce ) ;
			}
		}
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Data loader, SINGLE
     *  ---------------------------------------------------------------------------------------- */
    private void loadDataSingle ( ID eID, String state )
    {
		List<Workshop__c> workshopList = null ;
		
		try
		{
            workshopList = [
                SELECT ID, Name, CreatedDate,
                Title__c, Workshop_Description__c, Description__c,
                Type__c, Branch_Name__c, Branch__r.State_CD__c,
                Address__c, Event_Start__c, Event_End__c, Timezone__c
                FROM Workshop__c
                WHERE Public_Event__c = true
                AND ID = :eID
            ] ;
		}
		catch ( Exception e )
		{
			System.debug ( 'Exception?!?' ) ;
		}
			
		if ( ( workshopList != null ) && ( workshopList.size () > 0 ) )
		{
			System.debug ( 'Found ' + workshopList.size () + ' events to expose' ) ;
			
			for ( Workshop__c w : workshopList )
			{
				CalendarEvent ce = new CalendarEvent () ;
				
				ce.id = w.ID ;
				ce.uid = convertUID ( w.Event_Start__c ) + '-' + w.ID + '@oneunited.com' ;
				ce.Name = w.Name ;
				ce.Branch = w.Branch_Name__c ;
				ce.title = w.Title__c ;
				ce.createdDateTime = convertISO8601 ( w.CreatedDate, state ) ;
				ce.createdDateTimeICAL = convertICAL ( w.CreatedDate, state ) ;
				ce.startDateTime = convertISO8601 ( w.Event_Start__c, state ) ;
				ce.startDateTimeICAL = convertICAL ( w.Event_Start__c, state ) ;
				ce.endDateTime = convertISO8601 ( w.Event_End__c, state ) ;
				ce.endDateTimeICAL = convertICAL ( w.Event_End__c, state ) ;
				ce.description = w.Workshop_Description__c ;
				ce.url = 'https%3A//' + ApexPages.currentPage().getHeaders().get('Host') + '/oneunitedweb/BankEventPreRegistration?wid=' + w.ID ;
				ce.allDayEvent = 'false' ;
				ce.color = getColorFromEventType ( w.Type__c ) ;
                
                if ( String.isNotBlank ( state ) )
                {
                    if ( state.equalsIgnoreCase ( 'MA' ) )
                        ce.TimeZone = 'America/New_York' ;
                    else if ( state.equalsIgnoreCase ( 'CA' ) )
                        ce.TimeZone = 'America/Los_Angeles' ;
                    else if ( state.equalsIgnoreCase ( 'FL' ) )
                        ce.TimeZone = 'America/New_York' ;
                }
				
				calEventList.add ( ce ) ;
			}
		}
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Hacked INVALID JSON Setup for FullCalendar
     *  FullCalendar requires quotes around crap from a JSON feed
     *  NO QUOTES when loaded directly! NEVER quote value for ALLDAY option!
     *  ---------------------------------------------------------------------------------------- */
	public void setFullCalendarJSONHACKBAD ()
	{
		//  MANUAL JSON BECAUSE ITS FUCKED
		String g = '' ;
		boolean firstTime = true ;
		
		g += '[\n' ;
		
		for ( CalendarEvent ce : calEventList )
		{
			if ( firstTime )
			{
				firstTime = false ;
			}
			else
			{
				g += ',\n' ;
			}
			
			g += '{\n' ;
			g += '"id": "' + ce.ID + '",\n' ;
			g += '"title": "' + ce.Title + ' - ' + ce.Branch + '",\n' ;
			g += '"start": "' + ce.startDateTime + '",\n' ;
			g += '"end": "' + ce.endDateTime + '",\n' ;
			g += '"url": "' + ce.url + '",\n' ;
			g += '"description" : "' + ce.description + '",\n' ;
			
			//  Without this, we use the default color
			if ( String.isNotBlank ( ce.color ) )
				g += '"color": "' + ce.color + '",\n' ;
				
			g += '"allDay": ' + ce.allDayEvent + '\n' ;
			g += '}\n' ;
		}
		
		g += ']\n' ;
		
		calEventJSON = g ;
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Local functions
     *  ---------------------------------------------------------------------------------------- */
    private String convertISO8601 ( DateTime dt, String state )
    {
    	String rt = null ;
    	
    	if ( String.isNotBlank ( state ) )
    	{
    		if ( state.equalsIgnoreCase ( 'MA' ) )
	    		rt = dt.format ( 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'America/New_York' ) ;
    		else if ( state.equalsIgnoreCase ( 'CA' ) )
	    		rt = dt.format ( 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'America/Los_Angeles' ) ;
    		else if ( state.equalsIgnoreCase ( 'FL' ) )
	    		rt = dt.format ( 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'America/New_York' ) ;
    	}
    	else
    	{
    		rt = dt.format ( 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') ;
    	}
    	
    	return rt ;
    }
    
    private String convertUID ( DateTime dt )
    {
    	return dt.format ( 'yyyyMMdd\'T\'HHmmssSSS\'Z\'') ;
    }

	//  Assume iCAL for IOS ... iOS Actually uses a SLIGHTLY DIFFERENT date format!    
    private String convertICAL ( DateTime dt, String state )
    {
    	String rt = null ;
        
    	if ( String.isNotBlank ( state ) )
    	{
    		if ( state.equalsIgnoreCase ( 'MA' ) )
	    		rt = dt.format ( 'yyyyMMdd\'T\'HHmmss', 'America/New_York' ) ;
    		else if ( state.equalsIgnoreCase ( 'CA' ) )
	    		rt = dt.format ( 'yyyyMMdd\'T\'HHmmss', 'America/Los_Angeles' ) ;
    		else if ( state.equalsIgnoreCase ( 'FL' ) )
	    		rt = dt.format ( 'yyyyMMdd\'T\'HHmmss', 'America/New_York' ) ;
    	}
    	else
    	{
    		rt = dt.format ( 'yyyyMMdd\'T\'HHmmss') ;
    	}
        
        return rt ;
    }
    
    private String getColorFromEventType ( String thingy )
    {
    	String hexColor = null ;
    	
    	if ( String.isNotBlank ( thingy ) )
    	{
			if ( thingy.equalsIgnoreCase ( 'At Branch, Third Party, Registration Required' ) )
				hexColor = '#c68e00' ;
			else if ( thingy.equalsIgnoreCase ( 'At Branch, Third Party, Registration Not Required' ) )
				hexColor = '#c68e00' ;
			else if ( thingy.equalsIgnoreCase ( 'Not At Branch, Third Party, Registration Required' ) )
				hexColor = '#5c5684' ;
			else if ( thingy.equalsIgnoreCase ( 'Not At Branch, Third Party, Registration Not Required' ) )
				hexColor = '#5c5684' ;
			else if ( thingy.equalsIgnoreCase ( 'Not At Branch, Third Party, CLOSED' ) )
				hexColor = '#557372' ;
    	}
				
    	return hexColor ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Local object for translating to page
     *  ---------------------------------------------------------------------------------------- */
    public class CalendarEvent
    {
    	public String id { public get ; private set ; }
    	public String uid { public get ; private set ; }
    	public String Name { public get ; private set ; }
    	public String title { public get ; private set ; }
    	public String branch { public get ; private set ; }
    	public String createdDateTime { public get ; private set ; }
    	public String createdDateTimeICAL { public get ; private set ; }
    	public String startDateTime { public get ; private set ; } 
    	public String startDateTimeICAL { public get ; private set ; } 
    	public String endDateTime { public get ; private set ; }
    	public String endDateTimeICAL { public get ; private set ; }
    	public String description { public get ; private set ; }
    	public String url { public get ; private set ; }
    	public String alldayEvent { public get ; private set ; }
    	public String color { public get ; private set ; }
        public String timeZone { public get ; private set ; }
    }
}