@isTest
public class TestServiceTrigger 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Account getAccount ( Integer x )
    {
        Account a = new Account () ;
        a.Name = 'Test' + x ;
        
        return a ;
    }
    
	private static Contact getContact ( Integer x, Account a )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
        c.AccountId = a.ID ;
		
		return c ;
	}
    
	//  --------------------------------------------------------------------------------------
	//  Advocate
	//  --------------------------------------------------------------------------------------
    private static void setupAdvocateCrap ()
    {
        list<Advocate_Badge__c> abL = new list<Advocate_Badge__c> () ;
        
        Advocate_Badge__c ab1 = new Advocate_Badge__c () ;
        ab1.API_Name__c = 'I_Rock_KING' ;
        ab1.Description__c = 'XYZ123' ;
        ab1.Badge_Points__c = 100 ;
        
        abL.add ( ab1 ) ;
        
        Advocate_Badge__c ab2 = new Advocate_Badge__c () ;
        ab2.API_Name__c = 'Get_the_Card' ;
        ab2.Description__c = 'XYZ123' ;
        ab2.Badge_Points__c = 1000 ;
        
        abL.add ( ab2 ) ;
        
        Advocate_Badge__c ab5 = new Advocate_Badge__c () ;
        ab5.API_Name__c = 'Move_Your_Mind' ;
        ab5.Description__c = 'XYZ123' ;
        ab5.Badge_Points__c = 1000 ;
        
        abL.add ( ab5 ) ;
        
        insert abL ;
        
        list<Advocate_Activity__c> aaL = new list<Advocate_Activity__c> () ;
        
        Advocate_Activity__c aa1 = new Advocate_Activity__c () ;
        aa1.API_Name__c = 'Notifications' ;
        aa1.Activity_Points__c = 100 ;
        aa1.Name = 'Bleah' ;
        aa1.Description__c = 'ABC123' ;
        
        aaL.add ( aa1 ) ;
        
        Advocate_Activity__c aa2 = new Advocate_Activity__c () ;
        aa2.API_Name__c = 'Tablet_Banking' ;
        aa2.Activity_Points__c = 100 ;
        aa2.Name = 'Bleah' ;
        aa2.Description__c = 'ABC123' ;
        
        aaL.add ( aa2 ) ;
        
        Advocate_Activity__c aa3 = new Advocate_Activity__c () ;
        aa3.API_Name__c = 'LinkLine' ;
        aa3.Activity_Points__c = 100 ;
        aa3.Name = 'Bleah' ;
        aa3.Description__c = 'ABC123' ;
        
        aaL.add ( aa3 ) ;
        
        Advocate_Activity__c aa4 = new Advocate_Activity__c () ;
        aa4.API_Name__c = 'Online_Banking' ;
        aa4.Description__c = 'XYZ123' ;
        aa4.Activity_Points__c = 100 ;
        
        aaL.add ( aa4 ) ;
        
        Advocate_Activity__c aa5 = new Advocate_Activity__c () ;
        aa5.API_Name__c = 'Free_Bill_Pay' ;
        aa5.Description__c = 'XYZ123' ;
        aa5.Activity_Points__c = 100 ;
        
        aaL.add ( aa5 ) ;
        
        insert aaL ;
    }
	
	//  --------------------------------------------------------------------------------------
	//  TEST - Advocate
	//  --------------------------------------------------------------------------------------
	public static testmethod void Advocate ()
    {
		String aID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Agreement', 'Service__c' ) ;
        String osID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Online Services', 'Service__c' ) ;
        
        setupAdvocateCrap () ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        Contact c1 = getContact ( 1, a ) ;
        insert c1 ;
        
        list<Service__c> sL = new list<Service__c> () ;
        
        Service__c s1 = new Service__c () ;
        s1.Contact__c = c1.ID ;
        s1.RecordTypeId = aID ;
        s1.Status__c = 'Issued' ;
        s1.Type__c = 'ATM Card' ;
        s1.Plastic_Type__c = 'KING' ;
        s1.Award_Received__c = false ;
         
        sL.add ( s1 ) ;
        
        Service__c s2 = new Service__c () ;
        s2.Contact__c = c1.ID ;
        s2.RecordTypeId = aID ;
        s2.Status__c = 'Active' ;
        s2.Type__c = 'ATM Card' ;
        s2.Plastic_Type__c = 'KING' ;
        s2.Award_Received__c = false ;
         
        sL.add ( s2 ) ;
        
        Service__c s3 = new Service__c () ;
        s3.Contact__c = c1.ID ;
        s3.RecordTypeId = osID ;
        s3.Status__c = 'Active' ;
        s3.Type__c = 'Online Banking' ;
        s3.Award_Received__c = false ;
         
        sL.add ( s3 ) ;
        
        Service__c s4 = new Service__c () ;
        s4.Contact__c = c1.ID ;
        s4.RecordTypeId = osID ;
        s4.Status__c = 'Active' ;
        s4.Type__c = 'Bill Pay' ;
        s4.Award_Received__c = false ;
         
        sL.add ( s4 ) ;
        
        Service__c s5 = new Service__c () ;
        s5.Contact__c = c1.ID ;
        s5.RecordTypeId = osID ;
        s5.Status__c = 'Active' ;
        s5.Type__c = 'Mobile Banking' ;
        s5.Award_Received__c = false ;
         
        sL.add ( s5 ) ;
        
        Service__c s6 = new Service__c () ;
        s6.Contact__c = c1.ID ;
        s6.RecordTypeId = osID ;
        s6.Status__c = 'Active' ;
        s6.Type__c = 'Text Message Banking' ;
        s6.Award_Received__c = false ;
         
        sL.add ( s6 ) ;
        
        Service__c s7 = new Service__c () ;
        s7.Contact__c = c1.ID ;
        s7.RecordTypeId = osID ;
        s7.Status__c = 'Active' ;
        s7.Type__c = 'Tablet Banking' ;
        s7.Award_Received__c = false ;
         
        sL.add ( s7 ) ;
        
        Service__c s8 = new Service__c () ;
        s8.Contact__c = c1.ID ;
        s8.RecordTypeId = aID ;
        s8.Status__c = 'Active' ;
        s8.Type__c = 'Linkline' ;
        s8.Award_Received__c = false ;
         
        sL.add ( s8 ) ;
        
        insert sL ;
        
        Set<ID> idS = new set<ID> () ;
        
        for ( Service__c s : sL )
        {
            idS.add ( s.ID ) ;
        }
        
        list<Service__c> xsL = [
            SELECT ID, Award_Received__c
            FROM Service__c
            WHERE ID IN :idS 
        ] ;
        
        boolean hazReceived = true ;
        
        for ( Service__c s : xsL )
        {
            if ( s.Award_Received__c == false )
                hazReceived = false ;
        }
        
        System.assertEquals ( hazReceived, true ) ;
        
        list<Advocate_Badge_Awarded__c> abaL = [
            SELECT ID
            FROM Advocate_Badge_Awarded__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( abaL.size (), 0 ) ;
        
        list<Advocate_Activity_Achieved__c> aaaL = [
            SELECT ID
            FROM Advocate_Activity_Achieved__c
            WHERE Contact__c = :c1.ID 
        ] ;
        
        System.assertNotEquals ( aaaL.size (), 0 ) ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  TEST - Services - Insert
	//  --------------------------------------------------------------------------------------
	public static testmethod void Services_Insert ()
    {
 		String aID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Agreement', 'Service__c' ) ;
        String osID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Online Services', 'Service__c' ) ;
       
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        Contact c1 = getContact ( 1, a ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2, a ) ;
        insert c2 ;
        
        Contact c3 = getContact ( 3, a ) ;
        insert c3 ;
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        
        Financial_Account__c fa1 = new Financial_Account__c () ;
        fa1.TAXRPTFORPERSNBR__c = c2.ID ;
        fa1.MJACCTTYPCD__c = 'CK' ;
        fa1.CURRACCTSTATCD__c = 'ACT' ;
        fa1.CURRMIACCTTYPCD__c = 'UECK' ;
        
        faL.add ( fa1 ) ;
        
        Financial_Account__c fa2 = new Financial_Account__c () ;
        fa2.TAXRPTFORPERSNBR__c = c2.ID ;
        fa2.MJACCTTYPCD__c = 'SAV' ;
        fa2.CURRACCTSTATCD__c = 'ACT' ;
        fa2.CURRMIACCTTYPCD__c = 'UECS' ;
        
        faL.add ( fa2 ) ;
        
        Financial_Account__c fa3 = new Financial_Account__c () ;
        fa3.TAXRPTFORPERSNBR__c = c3.ID ;
        fa3.MJACCTTYPCD__c = 'CK' ;
        fa3.CURRACCTSTATCD__c = 'ACT' ;
        fa3.CURRMIACCTTYPCD__c = 'UECK' ;
        
        faL.add ( fa3 ) ;
        
        Financial_Account__c fa4 = new Financial_Account__c () ;
        fa4.TAXRPTFORPERSNBR__c = c3.ID ;
        fa4.MJACCTTYPCD__c = 'SAV' ;
        fa4.CURRACCTSTATCD__c = 'ACT' ;
        fa4.CURRMIACCTTYPCD__c = 'UECS' ;
        
        faL.add ( fa4 ) ;
        
        insert faL ;
        
        Test.startTest () ;
        
        list<Service__c> sL = new list<Service__c> () ;
        
        Service__c s1 = new Service__c () ;
        s1.Contact__c = c1.ID ;
        s1.recordTypeId = osID ;
        s1.Status__c = 'Active' ;
        s1.Type__c = 'online banking' ;
        
        sL.add ( s1 ) ;
        
        Service__c s2 = new Service__c () ;
        s2.Contact__c = c1.ID ;
        s2.recordTypeId = osID ;
        s2.Status__c = 'Active' ;
        s2.Type__c = 'mobile banking' ;
        
        sL.add ( s2 ) ;
        
        Service__c s3 = new Service__c () ;
        s3.Contact__c = c2.ID ;
        s3.RecordTypeId = aID ;
        s3.Status__c = 'Active' ;
        s3.Type__c = 'debit card' ;
        s3.Primary_Checking__c = fa1.ID ;
        s3.Primary_Savings__c = fa2.ID ;
        
        sL.add ( s3 ) ;
        
        Service__c s4 = new Service__c () ;
        s4.Contact__c = c2.ID ;
        s4.RecordTypeId = aID ;
        s4.Status__c = 'Active' ;
        s4.Type__c = 'atm card' ;
        s4.Primary_Checking__c = fa3.ID ;
        s4.Primary_Savings__c = fa4.ID ;
        
        sL.add ( s4 ) ;
        
        insert sL ;
        
        Test.stopTest () ;
        
        Contact cX = [
            SELECT ID, Online_Banking_Active__c, Mobile_Banking_Active__c
            FROM Contact
            WHERE ID = :c1.ID 
        ] ;
        
        System.assertEquals ( cX.Online_Banking_Active__c, true ) ;
        System.assertEquals ( cX.Mobile_Banking_Active__c, true ) ;
        
        Financial_Account__c fa1X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[0].ID 
        ] ;
       
        System.assertEquals ( fa1X.Active_Card__c, true ) ;
        
        Financial_Account__c fa2X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[1].ID 
        ] ;
       
        System.assertEquals ( fa2X.Active_Card__c, true ) ;
        
        Financial_Account__c fa3X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[2].ID 
        ] ;
       
        System.assertEquals ( fa3X.Active_Card__c, true ) ;
        
        Financial_Account__c fa4X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[3].ID 
        ] ;
       
        System.assertEquals ( fa4X.Active_Card__c, true ) ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  TEST - Services - Update
	//  --------------------------------------------------------------------------------------
	public static testmethod void Services_Update ()
    {
 		String aID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Agreement', 'Service__c' ) ;
        String osID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Online Services', 'Service__c' ) ;
       
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        Contact c1 = getContact ( 1, a ) ;
        insert c1 ;
        
        Contact c2 = getContact ( 2, a ) ;
        insert c2 ;
        
        Contact c3 = getContact ( 3, a ) ;
        insert c3 ;
        
        list<Financial_Account__c> faL = new list<Financial_Account__c> () ;
        
        Financial_Account__c fa1 = new Financial_Account__c () ;
        fa1.TAXRPTFORPERSNBR__c = c2.ID ;
        fa1.MJACCTTYPCD__c = 'CK' ;
        fa1.CURRACCTSTATCD__c = 'ACT' ;
        fa1.CURRMIACCTTYPCD__c = 'UECK' ;
        
        faL.add ( fa1 ) ;
        
        Financial_Account__c fa2 = new Financial_Account__c () ;
        fa2.TAXRPTFORPERSNBR__c = c2.ID ;
        fa2.MJACCTTYPCD__c = 'SAV' ;
        fa2.CURRACCTSTATCD__c = 'ACT' ;
        fa2.CURRMIACCTTYPCD__c = 'UECS' ;
        
        faL.add ( fa2 ) ;
        
        Financial_Account__c fa3 = new Financial_Account__c () ;
        fa3.TAXRPTFORPERSNBR__c = c3.ID ;
        fa3.MJACCTTYPCD__c = 'CK' ;
        fa3.CURRACCTSTATCD__c = 'ACT' ;
        fa3.CURRMIACCTTYPCD__c = 'UECK' ;
        
        faL.add ( fa3 ) ;
        
        Financial_Account__c fa4 = new Financial_Account__c () ;
        fa4.TAXRPTFORPERSNBR__c = c3.ID ;
        fa4.MJACCTTYPCD__c = 'SAV' ;
        fa4.CURRACCTSTATCD__c = 'ACT' ;
        fa4.CURRMIACCTTYPCD__c = 'UECS' ;
        
        faL.add ( fa4 ) ;
        
        insert faL ;
        
        Test.startTest () ;
        
        list<Service__c> sL = new list<Service__c> () ;
        
        Service__c s1 = new Service__c () ;
        s1.Contact__c = c1.ID ;
        s1.recordTypeId = osID ;
        s1.Status__c = 'Issued' ;
        s1.Type__c = 'online banking' ;
        
        sL.add ( s1 ) ;
        
        Service__c s2 = new Service__c () ;
        s2.Contact__c = c1.ID ;
        s2.recordTypeId = osID ;
        s2.Status__c = 'Issued' ;
        s2.Type__c = 'mobile banking' ;
        
        sL.add ( s2 ) ;
        
        Service__c s3 = new Service__c () ;
        s3.Contact__c = c2.ID ;
        s3.RecordTypeId = aID ;
        s3.Status__c = 'Issued' ;
        s3.Type__c = 'debit card' ;
        s3.Primary_Checking__c = fa1.ID ;
        s3.Primary_Savings__c = fa2.ID ;
        
        sL.add ( s3 ) ;
        
        Service__c s4 = new Service__c () ;
        s4.Contact__c = c2.ID ;
        s4.RecordTypeId = aID ;
        s4.Status__c = 'Issued' ;
        s4.Type__c = 'atm card' ;
        s4.Primary_Checking__c = fa3.ID ;
        s4.Primary_Savings__c = fa4.ID ;
        
        sL.add ( s4 ) ;
        
        insert sL ;
        
        for ( Integer i = 0 ; i < sL.size () ; i++ )
            sL [ i ].Status__c = 'Active' ;
        
        update sL ;
        
        Test.stopTest () ;
        
        Contact cX = [
            SELECT ID, Online_Banking_Active__c, Mobile_Banking_Active__c
            FROM Contact
            WHERE ID = :c1.ID 
        ] ;
        
        System.assertEquals ( cX.Online_Banking_Active__c, true ) ;
        System.assertEquals ( cX.Mobile_Banking_Active__c, true ) ;
        
        Financial_Account__c fa1X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[0].ID 
        ] ;
       
        System.assertEquals ( fa1X.Active_Card__c, true ) ;
        
        Financial_Account__c fa2X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[1].ID 
        ] ;
       
        System.assertEquals ( fa2X.Active_Card__c, true ) ;
        
        Financial_Account__c fa3X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[2].ID 
        ] ;
       
        System.assertEquals ( fa3X.Active_Card__c, true ) ;
        
        Financial_Account__c fa4X = [
            SELECT ID, Active_Card__c
            FROM Financial_Account__c
            WHERE ID = :faL[3].ID 
        ] ;
       
        System.assertEquals ( fa4X.Active_Card__c, true ) ;
    }
    
    
	//  --------------------------------------------------------------------------------------
	//  TEST - Services - Insert - BULK
	//  --------------------------------------------------------------------------------------
	public static testmethod void Services_Insert_BULK ()
    {
        Integer recordsize = 200 ;
        
 		String aID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Agreement', 'Service__c' ) ;
       
        Advocate_Badge__c ab1 = new Advocate_Badge__c () ;
        ab1.API_Name__c = 'I_Rock_KING' ;
        ab1.Description__c = 'XYZ123' ;
        ab1.Badge_Points__c = 100 ;

        insert ab1 ;
        
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        list<Contact> cL = new list<Contact> () ;
        
        for ( integer i = 0 ; i < recordsize ; i ++ )
        {
	        Contact c = getContact ( i, a ) ;
            cL.add ( c ) ;
        }
        
        insert cL ;
        
        Test.startTest () ;
        
        list<Service__c> sL = new list<Service__c> () ;
        
        for ( integer i = 0 ; i < recordsize ; i ++ )
        {
            Service__c s1 = new Service__c () ;
            s1.recordTypeId = aID ;
            s1.Status__c = 'Active' ;
            s1.Type__c = 'debit card' ;
            s1.Plastic_Type__c = 'KING' ;
            s1.Contact__c = cL [ i ].ID ;
            
            sL.add ( s1 ) ;
        }
        
        insert sL ;
        
        Test.stopTest () ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  TEST - Services - JIM
	//  --------------------------------------------------------------------------------------
	public static testmethod void Services_Insert_JIM ()
    {
 		String aID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Agreement', 'Service__c' ) ;
        String osID = OneUnitedUtilities.getRecordTypeIdForObject ( 'Online Services', 'Service__c' ) ;
       
        Account a = getAccount ( 1 ) ;
        insert a ;
        
        // ----------------------------------------------- 1 -----------------------------------------------
        Contact c1 = getContact ( 1, a ) ;
        insert c1 ;
        
        Service__c s1 = new Service__c () ;
        s1.Contact__c = c1.ID ;
        s1.recordTypeId = osID ;
        s1.Status__c = 'Active' ;
        s1.Type__c = 'online banking' ;
        insert s1 ;
        
        c1.Active_ATM_Card__c = false ;
        c1.Active_Debit_Card__c = false ;
        c1.Online_Banking_Active__c = false ;
        c1.Mobile_Banking_Active__c = false ;
        update c1 ;
        
        Contact c1x = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c1.ID
        ] ;
        
        System.assertEquals ( c1x.Mobile_Banking_Active__c, false ) ;
        System.assertEquals ( c1x.Online_Banking_Active__c, false ) ;
        
        // ----------------------------------------------- 2 -----------------------------------------------
        Contact c2 = getContact ( 2, a ) ;
        insert c2 ;
        
        Service__c s2 = new Service__c () ;
        s2.Contact__c = c2.ID ;
        s2.recordTypeId = osID ;
        s2.Status__c = 'Active' ;
        s2.Type__c = 'mobile banking' ;
        insert s2 ;
        
        c2.Active_ATM_Card__c = false ;
        c2.Active_Debit_Card__c = false ;
        c2.Online_Banking_Active__c = false ;
        c2.Mobile_Banking_Active__c = false ;
        update c2 ;
        
        Contact c2x = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c2.ID
        ] ;
        
        System.assertEquals ( c2x.Mobile_Banking_Active__c, false ) ;
        System.assertEquals ( c2x.Online_Banking_Active__c, false ) ;
        
        // ----------------------------------------------- 3 -----------------------------------------------
        Contact c3 = getContact ( 3, a ) ;
        insert c3 ;
        
        Service__c s3 = new Service__c () ;
        s3.Contact__c = c3.ID ;
        s3.recordTypeId = aID ;
        s3.Status__c = 'Active' ;
        s3.Type__c = 'debit card' ;
        insert s3 ;
        
        c3.Active_ATM_Card__c = false ;
        c3.Active_Debit_Card__c = false ;
        c3.Online_Banking_Active__c = false ;
        c3.Mobile_Banking_Active__c = false ;
        update c3 ;
        
        Contact c3x = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c3.ID
        ] ;
        
        System.assertEquals ( c3x.Active_ATM_Card__c, false ) ;
        System.assertEquals ( c3x.Active_Debit_Card__c, false ) ;

        // ----------------------------------------------- 4 -----------------------------------------------
        Contact c4 = getContact ( 4, a ) ;
        insert c4 ;
        
        Service__c s4 = new Service__c () ;
        s4.Contact__c = c4.ID ;
        s4.recordTypeId = aID ;
        s4.Status__c = 'Active' ;
        s4.Type__c = 'atm card' ;
        insert s4 ;
        
        c4.Active_ATM_Card__c = false ;
        c4.Active_Debit_Card__c = false ;
        c4.Online_Banking_Active__c = false ;
        c4.Mobile_Banking_Active__c = false ;
        update c4 ;
        
        Contact c4x = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c4.ID
        ] ;
        
        System.assertEquals ( c4x.Active_ATM_Card__c, false ) ;
        System.assertEquals ( c4x.Active_Debit_Card__c, false ) ;
        
        // ----------------------------------------------- GO -----------------------------------------------
        test.startTest () ;
        
        list<Service__c> sL = new list<Service__c> () ;
        
        s1.GUUID__c = 'JIMBOB' ;
        sL.add ( s1 ) ;
        
        s2.GUUID__c = 'JIMBOB' ;
        sL.add ( s2 ) ;
        
        s3.GUUID__c = 'JIMBOB' ;
        sL.add ( s3 ) ;
        
        s4.GUUID__c = 'JIMBOB' ;
        sL.add ( s4 ) ;
        
        update sL ;
        
        test.stopTest () ;
        
        // ----------------------------------------------- Results -----------------------------------------------
        Contact c1xx = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c1.ID
        ] ;
        
        System.assertEquals ( c1xx.Online_Banking_Active__c, true ) ;
        
        Contact c2xx = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c2.ID
        ] ;
        
        System.assertEquals ( c2xx.Mobile_Banking_Active__c, true ) ;
        
        Contact c3xx = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c3.ID
        ] ;
        
        System.assertEquals ( c3xx.Active_Debit_Card__c, true ) ;
        
        Contact c4xx = [
            SELECT ID, Active_ATM_Card__c, Active_Debit_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
            FROM Contact
            WHERE ID = :c4.ID
        ] ;
        
        System.assertEquals ( c4xx.Active_ATM_Card__c, true ) ;
    }
}