@isTest
public with sharing class TestEmailActionFunction 
{
	//  --------------------------------------------------------------------------------------
	//  SETUP
	//  --------------------------------------------------------------------------------------
	private static Contact getContact ( Integer x )
	{
		Contact c = new Contact () ;
		c.FirstName = 'Test' + x ;
		c.LastName = 'User' + x ;
		c.Email = 'test' + x + '@user' + x + '.com' ;
		
		insert c ;
		
		return c ;
	}
	
	private static Account getAccount ( Integer x )
	{
		Account a = new Account () ;
		a.Name = 'Test' + x + ' Account' + x ;
		a.Business_Email__c = 'test' + x + '@user' + x + '.com' ;
		
		insert a ;
		
		return a ;
	}
	
	//  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	public static testmethod void test_ONE_Contact ()
    {
		Contact c11 = getContact ( 11 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr1 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr1.contactID = c11.ID ;
		sr1.TemplateName = 'online-banking-signup-template' ;
		sr1.mergeVarName1 = 'CN1' ;
		sr1.mergeVarValue1 = 'CV1' ;
		sr1.mergeVarName2 = 'CN2' ;
		sr1.mergeVarValue2 = 'CV2' ;
		sr1.mergeVarName3 = 'CN3' ;
		sr1.mergeVarValue3 = 'CV3' ;
		sr1.mergeVarName4 = 'CN4' ;
		sr1.mergeVarValue4 = 'CV4' ;
		sr1.mergeVarName5 = 'CN5' ;
		sr1.mergeVarValue5 = 'CV5' ;
		 
		Contact c12 = getContact ( 12 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr2 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr2.contactID = c12.ID ;
		sr2.TemplateName = 'online-banking-benefits-template' ;
        sr2.idType = 'Contact' ;
		
		Contact c13 = getContact ( 13 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr3 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr3.contactID = c13.ID ;
		sr3.TemplateName = 'online-banking-signup-template' ;
        sr3.surveyYN = 'Y' ;
		 
		List<EmailActionFunction.SendOnlineBankingRequest> srL = new List<EmailActionFunction.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		srL.add ( sr2 ) ;
		srL.add ( sr3 ) ;
		
		List<EmailActionFunction.SendOnlineBankingResult> srR = EmailActionFunction.sendOnlineBankingEmail ( srL ) ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	public static testmethod void test_TWO_Account ()
    {
		Account a11 = getAccount ( 11 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr1 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr1.contactID = a11.ID ;
        sr1.idType = 'Account' ;
		sr1.TemplateName = 'online-banking-signup-template' ;
		sr1.mergeVarName1 = 'CN1' ;
		sr1.mergeVarValue1 = 'CV1' ;
		sr1.mergeVarName2 = 'CN2' ;
		sr1.mergeVarValue2 = 'CV2' ;
		sr1.mergeVarName3 = 'CN3' ;
		sr1.mergeVarValue3 = 'CV3' ;
		sr1.mergeVarName4 = 'CN4' ;
		sr1.mergeVarValue4 = 'CV4' ;
		sr1.mergeVarName5 = 'CN5' ;
		sr1.mergeVarValue5 = 'CV5' ;
		 
		Account a12 = getAccount ( 12 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr2 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr2.contactID = a12.ID ;
		sr2.TemplateName = 'online-banking-benefits-template' ;
        sr2.idType = 'Account' ;
		
		Account a13 = getAccount ( 13 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr3 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr3.contactID = a13.ID ;
		sr3.TemplateName = 'online-banking-signup-template' ;
        sr3.idType = 'Account' ;
        sr3.surveyYN = 'Y' ;
        
		List<EmailActionFunction.SendOnlineBankingRequest> srL = new List<EmailActionFunction.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		srL.add ( sr2 ) ;
		srL.add ( sr3 ) ;
		
		List<EmailActionFunction.SendOnlineBankingResult> srR = EmailActionFunction.sendOnlineBankingEmail ( srL ) ;
    }
    
	//  --------------------------------------------------------------------------------------
	//  TEST
	//  --------------------------------------------------------------------------------------
	public static testmethod void test_THREE_BOTH ()
    {
		Account a11 = getAccount ( 11 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr1 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr1.contactID = a11.ID ;
        sr1.idType = 'Account' ;
		sr1.TemplateName = 'online-banking-signup-template' ;
		sr1.mergeVarName1 = 'CN1' ;
		sr1.mergeVarValue1 = 'CV1' ;
		sr1.mergeVarName2 = 'CN2' ;
		sr1.mergeVarValue2 = 'CV2' ;
		sr1.mergeVarName3 = 'CN3' ;
		sr1.mergeVarValue3 = 'CV3' ;
		sr1.mergeVarName4 = 'CN4' ;
		sr1.mergeVarValue4 = 'CV4' ;
		sr1.mergeVarName5 = 'CN5' ;
		sr1.mergeVarValue5 = 'CV5' ;
		 
		Contact c12 = getContact ( 12 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr2 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr2.contactID = c12.ID ;
		sr2.TemplateName = 'online-banking-benefits-template' ;
        sr2.idType = 'Contact' ;
		
		Account a13 = getAccount ( 13 ) ;
		EmailActionFunction.SendOnlineBankingRequest sr3 = new EmailActionFunction.SendOnlineBankingRequest () ;
		sr3.contactID = a13.ID ;
		sr3.TemplateName = 'online-banking-signup-template' ;
        sr3.idType = 'Account' ;
        sr3.surveyYN = 'Y' ;
		
		List<EmailActionFunction.SendOnlineBankingRequest> srL = new List<EmailActionFunction.SendOnlineBankingRequest> () ;
		srL.add ( sr1 ) ;
		srL.add ( sr2 ) ;
		srL.add ( sr3 ) ;
		
		List<EmailActionFunction.SendOnlineBankingResult> srR = EmailActionFunction.sendOnlineBankingEmail ( srL ) ;
    }
}