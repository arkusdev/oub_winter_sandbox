global class batch_Advocate_SWIM_Reward_Schedule implements Schedulable
{
	global void execute ( SchedulableContext sc ) 
   	{
		batch_Advocate_SWIM_Reward b = new batch_Advocate_SWIM_Reward () ;
		Database.executeBatch ( b ) ;
   }
}