public without sharing class AdvocateProgramDisplayController {
/*    @AuraEnabled
    public static String getKeepAliveURL ()
    {
        User user = [SELECT ID, ContactID, SmallPhotoUrl, FullPhotoUrl, FederationIdentifier
                     FROM User
                     WHERE ID = :UserInfo.getUserId() 
                     LIMIT 1];
        
        Cache.OrgPartition orgPart = Cache.Org.getPartition ( 'local.Community' ) ;
        String thingy = EncodingUtil.convertToHex ( Crypto.generateDigest ( 'SHA1', Blob.valueOf ( user.FederationIdentifier + 'keepAliveURL' ) ) ) ;
        String keepAliveURL = (String) orgPart.get ( thingy ) ;
        
		return keepAliveURL ;
    }*/
    
    @AuraEnabled
    public static Map<String,String> getOverallProgress (){
        Map<Integer,String> MONTHNAMEMAP = new Map<Integer, String> { 1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May', 6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October', 11=>'November', 12=>'December' };
        Map<String,String> xMap = new Map<String, String>() ;

        User user = [SELECT ID, ContactID, SmallPhotoUrl, FullPhotoUrl, FederationIdentifier
                     FROM User
                     WHERE ID = :UserInfo.getUserId() 
                     LIMIT 1];
        
        xMap.put('SmallPhotoUrl', user.SmallPhotoUrl);
        xMap.put('FullPhotoUrl', user.FullPhotoUrl);

        if (String.isNotBlank(user.ContactID)) {
            Contact c = new Contact();

            List<Contact> cL = [SELECT ID, FirstName, LastName, Advocate_Timestamp__c,
                                Advocate_Activity_Total_Points__c, Award_Badges__c, Advocate_Level_Image__c,
                                Facebook_Handle__c, Twitter_Handle__c, Instagram_Handle__c, Total_Advocate_Points__c, Advocate_Referral_Code_Rewards__c 
                                FROM Contact
                                WHERE ID = :user.ContactID 
                                LIMIT 1] ;

            if (cL.size () > 0) {
                c = cL[0];
                String name = '';
                if (String.isNotBlank(c.FirstName))
                    name += c.FirstName ;
                
                if (String.isNotBlank(c.LastName))
                    name += ' ' + c.LastName ;
                
                xMap.put('Name', name);
                
                if (c.Advocate_Timestamp__c != null)
                    xMap.put('Advocate_Timestamp__c', MONTHNAMEMAP.get(c.Advocate_Timestamp__c.month()) + ' ' + c.Advocate_Timestamp__c.year());
                
                if (c.Advocate_Activity_Total_Points__c != null)
                    xMap.put('Advocate_Activity_Total_Points__c', c.Advocate_Activity_Total_Points__c.toPlainString());
                
                if (c.Advocate_Activity_Total_Points__c != null)
                    xMap.put('Total_Advocate_Points__c', c.Total_Advocate_Points__c.toPlainString());
                
                if (c.Advocate_Referral_Code_Rewards__c != null)
                    xMap.put('Advocate_Referral_Code_Rewards__c', c.Advocate_Referral_Code_Rewards__c.toPlainString());
                
                if (String.isNotBlank(c.Advocate_Level_Image__c))
                    xMap.put('Advocate_Level_Image__c', c.Advocate_Level_Image__c);
                
                if (c.Award_Badges__c != null)
                    xMap.put('Award_Badges__c', c.Award_Badges__c.toPlainString());

                xMap.put('Facebook_Handle__c', c.Facebook_Handle__c);
                xMap.put('Twitter_Handle__c', c.Twitter_Handle__c);
                xMap.put('Instagram_Handle__c', c.Instagram_Handle__c);
                xMap.put('Playlists_Completed__c', getPlaylistsCompleted(c.Id));
            }
        }
        
        return xMap ;
    }
    
    public static String getPlaylistsCompleted(String contactId) {
        String result = '0';

        List<AggregateResult> profiles = [SELECT SUM(Completed_Playlists__c) completed
                                          FROM Everfi_Profile__c
                                          WHERE Contact__c =:contactId];

        if (profiles.size() > 0 && profiles[0].get('completed') != null) {
            result = String.valueOf(profiles[0].get('completed'));
        }
        return result;
    }

    @AuraEnabled
    public static Advocate_Level__c getCurrentLevel() {
        Advocate_Level__c result = new Advocate_Level__c();
        String contactId = getCurrentUserContactId();

        List<Contact> contacts = [SELECT Advocate_Level__r.Name, Advocate_Level__r.Image_URL__c,
                                  Advocate_Level__r.Next_Level__r.Name, Advocate_Level__r.Next_Level__r.Achievement_Points__c,
                                  Advocate_Level__r.Next_Level__r.Image_URL__c
                                  FROM Contact
                                  WHERE Id =:contactId];

        if (contacts.size() > 0) {
            result = contacts[0].Advocate_Level__r;
        }
        return result;
    }

    @AuraEnabled
    public static Map<String,String> getReferralInfo () {
        Map<String,String> result = new Map<String,String> () ;
        String contactId = getCurrentUserContactId();
        List<Referral_Code__c> codes = [SELECT ID, Name
                                        FROM Referral_Code__c
                                        WHERE Contact__c =:contactId
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1];

        if (codes.size () > 0) {
            result.put('ReferralCode', codes[0].Name);
            result.put('ReferralURL', 'oneunited.com/referral?' + codes[0].Name);
        }
        
        return result ;
    }

    @AuraEnabled
    public static List<Advocate_Level__c> getAllLevels () {
        return [SELECT Name, Achievement_Points__c, Image_URL__c, Level_Image__c
                FROM Advocate_Level__c
                WHERE Public_Use__c = TRUE
                ORDER BY Achievement_Points__c];
    }

    @AuraEnabled
    public static List<Advocate_Badge__c> getAllBadges () {
        return [SELECT Id, Name, Image_URL__c, Description__c, Badge_Points__c
                FROM Advocate_Badge__c
                WHERE Public_Use__c = TRUE
                ORDER BY Name ASC];
    }

    @AuraEnabled
    public static List<Advocate_Badge__c> getMyBadges (Boolean recent) {
        List<Advocate_Badge__c> result = new List<Advocate_Badge__c>();
        String contactId = getCurrentUserContactId();
        String query = 'SELECT Badge__r.Id, Badge__r.Name, Badge__r.Image_URL__c, Badge__r.Description__c, Badge__r.Badge_Points__c ';
        query += 'FROM Advocate_Badge_Awarded__c ';
        query += 'WHERE Contact__c =:contactId ';

        if (recent) {
            query += 'ORDER BY CreatedDate DESC LIMIT 3';
        } else {
            query += 'ORDER BY Badge__r.Name ASC';
        }

        for (Advocate_Badge_Awarded__c awarded : Database.query(query)) {
            result.add(awarded.Badge__r);
        }

        return result ;
    }
    
    @AuraEnabled
    public static List<Advocate_Activity__c> getMyPoints (Boolean recent) {
        List<Advocate_Activity__c> result = new List<Advocate_Activity__c>();
        String contactId = getCurrentUserContactId();
        String query = 'SELECT Advocate_Activity_Points__c, Advocate_Activity__r.Id, Advocate_Activity__r.Name, ';
        query += 'Advocate_Activity__r.Activity_Points__c, Advocate_Activity__r.Description__c ';
        query += 'FROM Advocate_Activity_Achieved__c ';
        query += 'WHERE Contact__c = :contactId ';

        if (recent) {
            query += 'ORDER BY CreatedDate DESC LIMIT 3';
        } else {
            query += 'ORDER BY Advocate_Activity__r.Activity_Points__c DESC';
        }

        try {
            for (Advocate_Activity_Achieved__c achieved : Database.query(query)) {
                // return actual value for the advocate
                achieved.Advocate_Activity__r.Activity_Points__c = achieved.Advocate_Activity_Points__c;
                result.add(achieved.Advocate_Activity__r);
            }
        } catch (Exception ex) {
            throw new System.AuraHandledException(ex.getMessage());
        }

        return result ;
    }

    @AuraEnabled
    public static List<Advocate_Activity__c> getAllPoints () {
        List<Advocate_Activity__c> result = [SELECT Id, Name, Activity_Points__c, Description__c
                                             FROM Advocate_Activity__c
                                             WHERE Public_Use__c = TRUE
                                             ORDER BY Activity_Points__c DESC] ;

        return result ;
    }

    @AuraEnabled
    public static List<Advocate_Reward__c> getMyRewards (Boolean recent) {
        List<Advocate_Reward__c> result = new List<Advocate_Reward__c>();
        String contactId = getCurrentUserContactId();
        system.debug(contactId);
        String query = 'SELECT Advocate_Reward__r.Id, Advocate_Reward__r.Image_URL__c, ';
        query += 'Advocate_Reward__r.Name, Advocate_Reward__r.Description__c ';
        query += 'FROM Advocate_Reward_Achieved__c ';
        query += 'WHERE Contact__c=:contactID ';
        if (recent) {
            query += 'ORDER BY CreatedDate DESC LIMIT 3';
        } else {
            query += 'ORDER BY Advocate_Reward__r.Name ASC';
        }

        for (Advocate_Reward_Achieved__c award : Database.query(query)) {
            result.add(award.Advocate_Reward__r);
        }

        return result;
    }
    
    @AuraEnabled
    public static List<Advocate_Reward__c> getAllRewards () {
        List<Advocate_Reward__c> result = [SELECT Id, Image_URL__c, Name, Description__c
                                           FROM Advocate_Reward__c
                                           WHERE Public_Use__c = TRUE
                                           ORDER BY Sort_Order__c ASC, Name ASC];

        return result ;
    }

    public static String getCurrentUserContactId() {
        User user = [SELECT ContactID FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
        return user.ContactId;
    }

    @AuraEnabled
    public static Contact getContact() {
        Contact result = new Contact();
        String contactID = getCurrentUserContactId();
        
        if(user.ContactId != null) {
            result = [SELECT Facebook_Handle__c, Twitter_Handle__c, Instagram_Handle__c, Advocate_Level__c FROM Contact WHERE Id = :contactID];
        }

        return result;

    }

    @AuraEnabled
    public static Contact saveContact(Contact con) {
        try {
            update con;
        } catch(Exception ex) {
            throw new AuraHandledException('Error: ' + ex.getMessage());
        }
        
        return con;
    }
    
    @AuraEnabled
    public static void saveAttachment(String base64Data, String contentType) {
        try {
            User user = [SELECT ID, ContactID, SmallPhotoUrl, FullPhotoUrl, FederationIdentifier
                         FROM User
                         WHERE ID = :UserInfo.getUserId() 
                         LIMIT 1];
            
            boolean hazPhoto = false ;
            
            if ( ! user.SmallPhotoUrl.containsIgnoreCase ( 'profilephoto/005/T' ) )
                hazPhoto = true ;
                            
            ConnectApi.BinaryInput photoFileInput = new ConnectApi.BinaryInput(EncodingUtil.base64Decode(base64Data), contentType, String.valueOf(UserInfo.getUserId()));
            ConnectApi.UserProfiles.setPhoto(Network.getNetworkId(), UserInfo.getUserId(), photoFileInput);
            
            if ( ! hazPhoto )
                awardActivity ( user.ContactID ) ;
            
        } catch(Exception ex) {
            system.debug(ex.getMessage());
            throw new AuraHandledException('Error: ' + ex.getMessage());
        }
    }
    
	@future ( callout=true ) // <- Headaches!
    private static void awardActivity ( ID cID )
    {
        List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> reallyLongNameList = new List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
        reallyLongName.ContactID = cID ;
        reallyLongName.requestType = 'Activity' ;
        reallyLongName.requestName = 'Picture_Perfect' ;
        
        reallyLongNameList.add ( reallyLongName ) ;
        
        if ( ( reallyLongNameList != null ) && ( reallyLongNameList.size () > 0 ) )
            AdvocateProgramAwardInvocationFunction.advocateProgramAward ( reallyLongNameList ) ;
    }

    @AuraEnabled
    public static String assignLevelIfEmtpy () {
        Contact contact = getContact();
        String result = contact.Advocate_Level__c;

        if (String.isBlank(contact.Advocate_Level__c)) {
            List<AdvocateLevelSetting__mdt> settings = [SELECT AdvocateId__c FROM AdvocateLevelSetting__mdt WHERE MasterLabel = 'Default' LIMIT 1];
            if (settings.size() > 0) {
                String defaultID = settings[0].AdvocateId__c;
                contact.Advocate_Level__c = defaultID;
                try {
                    update contact;
                    result = defaultID;
                } catch (Exception ex) {
                    throw new System.AuraHandledException(ex.getMessage());
                }
            } else {
                result = 'Error: default settings not found';
            }
        }

        return result;
    }
}