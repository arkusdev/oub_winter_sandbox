@isTest
public with sharing class TestPublicEventCalendar 
{
    private static void insertCustomSetting () 
    {
    	Web_Settings__c ws = new Web_Settings__c () ;
    	ws.Name = 'Web Forms' ;
    	ws.Access_Control_Origin__c = 'https://www.oneunited.com' ;
    	insert ws ;
    }
    
	private static Branch__c getBranch ()
	{
		return getBranch ( null ) ;
	}
	
	private static Branch__c getBranch ( String state )
	{
		// Workshops are at a branch
		Branch__c b = new Branch__c () ;
		b.Name = 'Test Branch' ;
		b.Address_A__c = '123 Test Street' ;
		b.City__c = 'Test City' ;
		b.State_CD__c = state ;
		b.Zip__c = '10001' ;
		
		insert b ;
		
		return b ;
	} 
	
	private static Workshop__c getWorkshop ( String x )
	{
		return getWorkshop ( x, null ) ;
	}
	
	private static Workshop__c getWorkshop ( String x, String y )
	{
		Branch__c b = getBranch ( y ) ;
				
		//  Create the workshop referring to the branch
		Workshop__c w = new Workshop__c () ;
		
		w.Name = 'Test Workshop ' + x ;
		w.Branch__c = b.id ;
		w.Public_Event__c = true ;
		w.Event_Start__c = System.now () ;
		w.Event_End__c = System.now ().addHours ( 1 ) ;
		w.State__c = y ;
		w.type__c = 'Smart Money' ;
		
		w.Event_Start__c = Datetime.now () ;
		w.Event_End__c = Datetime.now ().addHours ( 1 ) ;
		
		insert w ;
		
		return w ;
	} 
	
	static testmethod void testCalendarControllerICALnoState ()
	{
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarICAL ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		Workshop__c w1 = getWorkshop ( 'A' ) ;
		Workshop__c w2 = getWorkshop ( 'B' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadICAL () ;
		
		Test.stopTest () ;
	}

	static testmethod void testCalendarControllerICALState ()
	{
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarICAL ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		Workshop__c w1 = getWorkshop ( 'A', 'CA' ) ;
		Workshop__c w2 = getWorkshop ( 'B', 'FL' ) ;
		Workshop__c w3 = getWorkshop ( 'C', 'MA' ) ;
        ApexPages.currentPage().getParameters().put ( 'state', w1.State__c ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadICAL () ;
		
		Test.stopTest () ;
	}

	static testmethod void testCalendarControllerICALSingle ()
	{
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarICAL ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		Workshop__c w1 = getWorkshop ( 'A' ) ;
        
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'eID', w1.ID ) ;
        ApexPages.currentPage().getParameters().put ( 'state', w1.State__c ) ;
        
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadICALSingle () ;
		
		Test.stopTest () ;
	}

	static testmethod void testCalendarControllerJSONALL ()
	{
		//  Generic settings
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarJSON ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		Workshop__c w1 = getWorkshop ( 'C', 'CA' ) ;
		Workshop__c w2 = getWorkshop ( 'D', 'FL' ) ;
		Workshop__c w3 = getWorkshop ( 'E', 'MA' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSON () ;
		
		Test.stopTest () ;
	}

	static testmethod void testCalendarControllerJSONSingleState ()
	{
		//  Generic settings
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarJSON ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		Workshop__c w1 = getWorkshop ( 'C', 'CA' ) ;
        
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'eID', w1.ID ) ;
        ApexPages.currentPage().getParameters().put ( 'state', w1.State__c ) ;
        
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSONSingle () ;
		
		Test.stopTest () ;
	}

	static testmethod void testCalendarControllerJSONRange ()
	{
		//  Generic settings
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarJSON ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'start', '2010-01-01' ) ;
        ApexPages.currentPage().getParameters().put ( 'end', '2020-01-01' ) ;
        
		Workshop__c w1 = getWorkshop ( 'C' ) ;
		Workshop__c w2 = getWorkshop ( 'D' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSON () ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testCalendarControllerJSONBad ()
	{
		//  Generic settings
		insertCustomSetting () ;
		
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarJSON ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'start', 'BAD' ) ;
        ApexPages.currentPage().getParameters().put ( 'end', 'FAIL' ) ;
        
		Workshop__c w1 = getWorkshop ( 'C' ) ;
		Workshop__c w2 = getWorkshop ( 'D' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSON () ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testCalendarControllerJSONStateCA ()
	{
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarICAL ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'state', 'CA' ) ;
        
		Workshop__c w1 = getWorkshop ( 'A', 'MA' ) ;
		Workshop__c w2 = getWorkshop ( 'B', 'CA' ) ;
		Workshop__c w3 = getWorkshop ( 'C', 'FL' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSON () ;
		
		Test.stopTest () ;
	}

	static testmethod void testCalendarControllerJSONStateFL ()
	{
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarICAL ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'state', 'FL' ) ;
        
		Workshop__c w1 = getWorkshop ( 'A', 'MA' ) ;
		Workshop__c w2 = getWorkshop ( 'B', 'CA' ) ;
		Workshop__c w3 = getWorkshop ( 'C', 'FL' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSON () ;
		
		Test.stopTest () ;
	}
	
	static testmethod void testCalendarControllerJSONStateMA ()
	{
		//  Point to page to test
		PageReference pr1 = Page.PublicEventCalendarICAL ;
		Test.setCurrentPage ( pr1 ) ;
		
		Test.startTest () ;
		
		//  Pass in parameters manually
        ApexPages.currentPage().getParameters().put ( 'state', 'MA' ) ;
        
		Workshop__c w1 = getWorkshop ( 'A', 'MA' ) ;
		Workshop__c w2 = getWorkshop ( 'B', 'CA' ) ;
		Workshop__c w3 = getWorkshop ( 'C', 'FL' ) ;
		
		PublicEventCalendar pce = new PublicEventCalendar () ;
		
		pce.pageLoadJSON () ;
		
		Test.stopTest () ;
	}
}