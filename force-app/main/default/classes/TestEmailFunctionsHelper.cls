@isTest
public with sharing class TestEmailFunctionsHelper 
{
	private static Contact getContact ( String x )
	{
		Contact c = new Contact () ;
		
		c.FirstName = 'Test' + x ;
		c.LastName = 'Applicant' + x ;
		c.Email = c.FirstName + '.' + c.LastName + '@' + c.FirstName + c.LastName + '.com' ;
		
		insert c ;
		
		return c ;
	}
	
	/*
	 * Creates a generic application I use for testing.
	 */	
    private static one_application__c getApplicationForTests ( Contact c1, Contact c2 )
    {
        return getApplicationForTests ( c1, c2, 'E- Check (ACH)' ) ;
    }

	private static one_application__c getApplicationForTests ( Contact c1, Contact c2, String fundingOption )
	{
		one_application__c app = new one_application__c ();
		
		app.Contact__c = c1.Id;
		
		app.First_Name__c = c1.FirstName ;
		app.Last_Name__c = c1.LastName ;
		app.Email_Address__c = c1.Email ;
		app.Number_of_Applicants__c = 'I will be applying individually' ;
		
		if ( c2 != null )
		{
			app.Co_Applicant_Contact__c = c2.Id;
			app.Co_Applicant_First_Name__c = c2.FirstName ;
			app.Co_Applicant_Last_Name__c = c2.LastName ;
			app.Co_Applicant_Email_Address__c = c2.Email ;
			app.Number_of_Applicants__c = 'I will be applying jointly with another person' ;
		}
		
		app.Referred_By_Contact__c = c1.Id;
		app.Entered_By_Contact__c = c1.Id;

		app.Funding_Options__c = fundingOption ;
		app.Gross_Income_Monthly__c = 10000.00 ;

		app.FIS_Application_ID__c = null ;
		app.FIS_Decision_Code__c = null ;
		
		app.UTM_Campaign__c = 'A%20B' ;
		app.UTM_Content__c = 'A%20B' ;
		app.UTM_Medium__c = 'A%20B' ;
		app.UTM_Source__c = 'A%20B' ;
		app.UTM_Term__c = 'A%20B' ;
		app.UTM_VisitorID__c = 'A%20B' ;
		
		app.Unknown_Location__c = true ;
		app.Invalid_Email_Address__c = true ;
		app.Invalid_Co_Applicant_Email_Address__c = true ;
        app.Duplicate_Funding_Account__c = false ;
        app.Duplicate_IP_Address__c = false ;
        
        app.minFraud_ID__c = '1234' ;
        app.OFAC_Transaction_ID__c = '1234' ;
		
		return app ;
	}

	public static testmethod void testAdditionalInfo ()
    {
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Reuse settings obejct
		one_test_new.insertCustomSetting () ;
		
        EmailFunctionsHelper.additionalMap = new map<String, boolean> () ;
        
		Contact c1 = getContact ( '1' ) ;
		Contact c2 = getContact ( '2' ) ;
		
		one_application__c o = getApplicationForTests ( c1, c2 ) ;
//        o.CreatedDate = Date.today().addDays ( -3 ) ;
        insert o ;
        
        o.FIS_Decision_Code__c = 'F003' ;
        o.Application_Processing_Status__c = 'Additional Documentation Needed' ;
        update o ;
        
        one_application__c ox = one_utils.getApplicationFromId ( o.ID ) ;

        EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.additionalMap, o.ID ) ;
        EmailFunctionsHelper.sendTimeBasedAdditionalInfoEmail ( ox ) ;
		
		//  End of testing
		Test.stopTest () ;
    }
    
	public static testmethod void testTrial ()
    {
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Reuse settings obejct
		one_test_new.insertCustomSetting () ;
		
        EmailFunctionsHelper.trialMap = new map<String, boolean> () ;
        
		Contact c1 = getContact ( '1' ) ;
		Contact c2 = getContact ( '2' ) ;
		
		one_application__c o = getApplicationForTests ( c1, c2 ) ;
//        o.CreatedDate = Date.today().addDays ( -3 ) ;
        insert o ;
        
        o.FIS_Decision_Code__c = 'P501' ;
        o.Funding_Options__c = 'E- Check (ACH)' ;
        o.ACH_Funding_Status__c = 'Verify Trial Deposit' ;
        update o ;
        
        EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, o.ID ) ;
        one_application__c ox = one_utils.getApplicationFromId ( o.ID ) ;
        
		EmailFunctionsHelper.sendTimeBasedTrialEmail ( ox ) ;
		
		//  End of testing
		Test.stopTest () ;
    }
    
	public static testmethod void testFunding ()
    {
		//  Separate out HTTP call from app insertion to avoid callout error
		Test.startTest () ;
		
		//  Reuse settings obejct
		one_test_new.insertCustomSetting () ;
		
        EmailFunctionsHelper.fundingMap = new map<String, boolean> () ;
                
		Contact c1 = getContact ( '1' ) ;
		Contact c2 = getContact ( '2' ) ;
		
		one_application__c o = getApplicationForTests ( c1, c2 ) ;
//        o.CreatedDate = Date.today().addDays ( -3 ) ;
        insert o ;
        
        o.FIS_Decision_Code__c = 'P089' ;
        o.Funding_Options__c = 'Mail' ;
        update o ;
        
        one_application__c ox = one_utils.getApplicationFromId ( o.ID ) ;
        System.debug ( 'STATUS :: ' + ox.Application_Status__c ) ;
        
        EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, o.ID ) ;
		EmailFunctionsHelper.sendTimeBasedFundingEmail ( ox ) ;
		
		//  End of testing
		Test.stopTest () ;
    }
}