@isTest
public with sharing class TestWorkshopAttendeeTrigger 
{
	/*
	 *  Setup for testing
	 */
    public static void insertCustomSetting ()
    {
    	Web_Settings__c s = new Web_Settings__c () ;
    	s.Name = 'Web Forms' ;
    	s.Workshop_Contact_Record_Type__c = 'Prospect' ;
    	
    	insert s ;
    }
    
	private static Branch__c getBranch ()
	{
		// Workshops are at a branch
		Branch__c b = new Branch__c () ;
		b.Name = 'Test Branch' ;
		b.Address_A__c = '123 Test Street' ;
		b.City__c = 'Test City' ;
		b.State_CD__c = 'TS' ;
		b.Zip__c = '10001' ;
		
		insert b ;
		
		return b ;
	} 
	
	private static Workshop__c getWorkshop ()
	{
		Branch__c b = getBranch () ;
				
		//  Create the workshop referring to the branch
		Workshop__c w = new Workshop__c () ;
		
		w.Name = 'Test Workshop' ;
		w.Branch__c = b.id ; 
		
		insert w ;
		
		return w ;
	} 
	
	/*
	 * Insert a workshop with contact.
	 */	
	public static testmethod void stupidTestOne ()
	{
		//  Settings
		insertCustomSetting () ;
		
		//  Gets
		Branch__c b = getBranch () ;
		Workshop__c w = getWorkshop () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'test@user.com' ;
		
		insert c ;
		
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		wa.Workshop_ID__c = w.id ;
		wa.Email_Address__c = 'test@user.com' ;
		wa.First_Name__c = 'Test1' ;
		wa.Last_Name__c = 'User1' ;
		wa.Contact__c = c.id ;
		
		insert wa ;
		
		wa.First_Name__c = 'Test2' ;
		wa.Last_Name__c = 'User2' ;
		
		update wa ;
	}
	
	/*
	 * Insert a workshop without a contact.
	 */	
	public static testmethod void stupidTestTwo ()
	{
		//  Settings
		insertCustomSetting () ;
		
		//  Gets
		Branch__c b = getBranch () ;
		Workshop__c w = getWorkshop () ;
		
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		wa.Workshop_ID__c = w.id ;
		wa.Email_Address__c = 'test@user.com' ;
		wa.First_Name__c = 'Test1' ;
		wa.Last_Name__c = 'User1' ;
		
		insert wa ;
	}
	
	/*
	 * Insert a workshop to find a contact.
	 */	
	public static testmethod void stupidTestThree ()
	{
		//  Settings
		insertCustomSetting () ;
		
		//  Gets
		Branch__c b = getBranch () ;
		Workshop__c w = getWorkshop () ;
		
		Contact c = new Contact () ;
		c.FirstName = 'Test' ;
		c.LastName = 'User' ;
		c.Email = 'test@user.com' ;
		
		insert c ;
		
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		wa.Workshop_ID__c = w.id ;
		wa.Email_Address__c = 'test@user.com' ;
		wa.First_Name__c = 'Test' ;
		wa.Last_Name__c = 'User' ;
		
		insert wa ;
	}
	
	/*
	 * Insert a workshop to find a contact.
	 */	
	public static testmethod void stupidTestFour ()
	{
		//  Settings
		insertCustomSetting () ;
		
		//  Gets
		Branch__c b = getBranch () ;
		Workshop__c w = getWorkshop () ;
		
		Contact c1 = new Contact () ;
		c1.FirstName = 'Test' ;
		c1.LastName = 'User' ;
		c1.Email = 'test@user.com' ;
		
		insert c1 ;
		
		Contact c2 = new Contact () ;
		c2.FirstName = 'Test' ;
		c2.LastName = 'User' ;
		c2.Email = 'test@user.com' ;
		
		insert c2 ;
		
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		wa.Workshop_ID__c = w.id ;
		wa.Email_Address__c = 'test@user.com' ;
		wa.First_Name__c = 'Test' ;
		wa.Last_Name__c = 'User' ;
		
		insert wa ;
	}
	
	/*
	 * Insert a workshop to find a contact.
	 */	
	public static testmethod void stupidTestFive ()
	{
		//  Settings
		insertCustomSetting () ;
		
		//  Gets
		Branch__c b = getBranch () ;
		Workshop__c w = getWorkshop () ;
		
		Workshop_Attendee__c wa = new Workshop_Attendee__c () ;
		wa.Workshop_ID__c = w.id ;
		wa.First_Name__c = 'Test' ;
		wa.Last_Name__c = 'User' ;
		
		insert wa ;
	}
}