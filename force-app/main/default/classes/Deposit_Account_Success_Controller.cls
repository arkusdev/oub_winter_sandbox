public with sharing class Deposit_Account_Success_Controller 
{
    /*  ----------------------------------------------------------------------------------------
     *  Security ?!@?
     *  ---------------------------------------------------------------------------------------- */
	private static ApplicationHelper access = ApplicationHelper.getInstance () ;

    /*  ----------------------------------------------------------------------------------------
     *  Parameters
     *  ---------------------------------------------------------------------------------------- */
    private String tid = null;
    public Boolean obRegActivated { public get; private set; }
    
    public String result {get;set;}
    public one_application__c app {get; private set;}
    public List<Online_Deposit_Application_Account__c> listOfAccounts {get;private set;}
    public List<Online_Deposit_Application_Account__c> checkingAccs {get; private set;}
    public List<Online_Deposit_Application_Account__c> notCheckingAccs {get;private set;}
    public String previouslySelectedCheckingAccount {get;private set;}
    public String decision {get;set;}
    public List<SelectOption> options {get;private set;}
    private List<Deposit_Account_Product__mdt> productsMDT {get;set;}
    public Decimal amount {get;set;}
    private Decimal maxAmount {get;set;}
    
    public String liveAgentButton { public get; private set; }
    public String ratesURLDisclosure { get; set; }
    public String obURL { public get; private set; }
    
    public boolean displayCartCheckout { public get ; private set ; }
    public String cartCheckoutJSON { public get ; private set ; }
    public string recordUserJSON { public get; private set; }
    
    public Deposit_Account_Success_Controller()
    {
        obRegActivated = false;
        
		//  --------------------------------------------------------------------------------------------------
		//  JIM cart checkout for google tag tracking
		//  --------------------------------------------------------------------------------------------------
		displayCartCheckout = false ;
        Cookie jCookie = ApexPages.currentPage().getCookies().get ( 'j_Cookie' ) ;
        
        try{
            //  --------------------------------------------------------------------------------------------------
            //  Live Agent Button
            //  --------------------------------------------------------------------------------------------------
            if ( ( one_settings__c.getInstance('settings').Live_Agent_Enable__c != null ) &&
                ( one_settings__c.getInstance('settings').Live_Agent_Enable__c == true ) )
            {
                liveAgentButton = one_settings__c.getInstance('settings').Live_Agent_Button_ID__c ;
            }
            else
            {
                liveAgentButton = null ;
            }
            
            if ( one_settings__c.getInstance('settings').Direct_From_App__c != null )
            {
                tid = one_settings__c.getInstance('settings').Direct_From_App__c ;
            }
            
            if ( one_settings__c.getInstance('settings').OB_Reg_Activated__c != null )
            {
                obRegActivated = one_settings__c.getInstance('settings').OB_Reg_Activated__c ;
            }
            
            //  --------------------------------------------------------------------------------------------------
            //  Rate disclosure URL
            //  --------------------------------------------------------------------------------------------------
            String appId;
            String kId;
            if(system.currentPageReference().getParameters().get('aid') != null){	
                appId = System.currentPageReference().getParameters().get('aid');
            }
            if(system.currentPageReference().getParameters().get('kid') != null){   
                kId = System.currentPageReference().getParameters().get('kid');
            } 
            if(!String.isEmpty(appId) && !String.isEmpty(kId)){
                List<one_application__c> appAuxs = new List<one_application__c>();            
                appAuxs = [
                    SELECT ID, Name, FIS_Application_ID__c, FIS_Decision_Code__c, FIS_Credit_Limit__c, Application_ID__c,
                    First_Name__c, Last_Name__c, Email_Address__c, DOB__c, Phone__c, City__c, State__c, Zip_Code__c,
                    Co_Applicant_First_Name__c, Co_Applicant_Last_Name__c, Co_Applicant_Email_Address__c, 
                    Entered_By_Contact__c, Entered_By_Contact__r.Email, 
                    Referred_By_Contact__c, Referred_By_Contact__r.Email,
                    Reservation_Vendor_ID__c, Reservation_Vendor_ID__r.Name,  Reservation_Vendor_ID__r.Tracking_Pixel__c,
                    Reservation_Affiliate_Vendor__c, Reservation_Affiliate_Vendor__r.Name,  Reservation_Affiliate_Vendor__r.Tracking_Pixel__c,
                    Product__c, SubProduct__c, Plan__c, Upload_Attachment_key__c,QualiFile_Product_Strategy__c,RequestedCreditLimit__c
                    FROM one_application__c 
                    WHERE id = : appId AND Upload_Attachment_key__c = :kId AND RecordType.Name = 'Deposits'
                    LIMIT 1
                ];
                
                if(appAuxs.size() > 0){
                    
                    app = appAuxs[0];
                    if ( String.isNotBlank ( app.Product__c ) && String.isNotBlank ( app.SubProduct__c ) && String.isNotBlank ( app.Plan__c ) ){
                        ratesURLDisclosure = one_utils.getDisclosureURL ( app.Product__c, app.SubProduct__c, app.Plan__c ) ;
                    }
                    
                    List<Qualifile_Response_Mapping__mdt> qrm = [SELECT Required_Product_Codes__c,Checking_Accounts_Selected__c,Savings_Accounts_Selected__c,Certificates_Accounts_Selected__c,Response_Type__c 
                                                                 FROM Qualifile_Response_Mapping__mdt 
                                                                 WHERE FIS_Decision_Code__c = :app.FIS_Decision_Code__c];

                    if(qrm.size()>0 && qrm[0].Response_Type__c == 'Decline'){
                        //decline
                        result = 'decline';
                    /*
                      }else if(qrm.size()>0 && qrm[0].Response_Type__c == 'Review'){
                        //review
                        if(app.FIS_Decision_Code__c == 'P801' || app.FIS_Decision_Code__c == 'P802' || app.FIS_Decision_Code__c == 'P881' || app.FIS_Decision_Code__c == 'P882'){
                            //send it to the additional info page
                            result = 'sendToAdditionalInfoPage';
                        }else{
                            result = 'review'; 
                        }
                    */
                    }else if(qrm.size()>0 && qrm[0].Response_Type__c == 'Counter Offer'){
                        //approved with product offers
                        result = 'approvedWithPOffers';
                        previouslySelectedCheckingAccount = 'checking account you selected';
                        if(qrm.size()>0){
                            String reqProductsCodesStr = qrm[0].Required_Product_Codes__c;
                            Set<String> reqProductsCodes = new Set<String>( reqProductsCodesStr.split(',') );
                            productsMDT = [SELECT MasterLabel,Product_Code__c,Major_Account_Type_Code__c,Debit_Card_Agreement__c,ATM_Card_Agreement__c,Account_Available_Options__c,Category__c,Minimum_Funding__c,Maximum_Funding__c,Link__c,Interest_Rate__c,Description__c 
                                           FROM Deposit_Account_Product__mdt 
                                           WHERE Product_Code__c IN :reqProductsCodes 
                                           ORDER BY Minimum_Funding__c DESC];

                            listOfAccounts = [SELECT Product_Name__c,Major_Account_Type_Code__c,COCC_Account_Number__c,Minor_Account_Type_Code__c,Initial_Funding_amount__c,Category__c,Card_Code__c,ATM_Card__c,Direct_Deposit__c,E_Statement__c,Online_Banking__c,Telephone_Banking__c,Debit_card__c FROM  Online_Deposit_Application_Account__c WHERE Application__c = :app.Id];
                            options = new List<SelectOption>();
                            options.add( new SelectOption('','-- None --') );
                            options.add( new SelectOption('Yes','Yes to U2 E-Checking') );
                            maxAmount = 0;
                            checkingAccs = new List<Online_Deposit_Application_Account__c>();
                            notCheckingAccs = new List<Online_Deposit_Application_Account__c>();
                            for(Online_Deposit_Application_Account__c acc : listOfAccounts){
                                if(acc.Category__c == 'Checking'){
                                    checkingAccs.add(acc);
                                    maxAmount += acc.Initial_Funding_amount__c;
                                }else{
                                    notCheckingAccs.add(acc);
                                }
                            }
                            amount = maxAmount;
                            if(checkingAccs.size()>0 && notCheckingAccs.size()>0){
                                previouslySelectedCheckingAccount = checkingAccs[0].Product_Name__c;
                                options.add( new SelectOption('removeCheckingAcc','Remove the Checking Account') );
                            }
                            options.add( new SelectOption('Cancel','Cancel my Application') );

                        }else{
                            result = 'error';
                            System.debug('Could not find Accounts for the product strategy ' + app.QualiFile_Product_Strategy__c);
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'We are having some issues right now. Please try again later'));
                        }
                    }else if(app.FIS_Decision_Code__c.startsWith('V')){
                        result = 'Withdraw';  
                    }else if(app.FIS_Decision_Code__c == 'P912'){
                        result = 'waitingForCOCC';    
                    }else if(app.FIS_Decision_Code__c == 'P089'){
                        //approved
                        obURL = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/application/one_application_ob_registration?aid=' + appId + '&kid=' + kId + '&tid=' + tid;
                        result = 'approved';
                        listOfAccounts = [SELECT Product_Name__c,Major_Account_Type_Code__c,COCC_Account_Number__c,Minor_Account_Type_Code__c,Initial_Funding_amount__c,Category__c,Card_Code__c,ATM_Card__c,Direct_Deposit__c,E_Statement__c,Online_Banking__c,Telephone_Banking__c,Debit_card__c FROM  Online_Deposit_Application_Account__c WHERE Application__c = :app.Id];
                    }else if(app.FIS_Decision_Code__c.startsWith('F') || app.FIS_Decision_Code__c.startsWith('P')){
                        result = 'review';
                    }else if(app.FIS_Decision_Code__c.startsWith('E')){
                        result = 'review';
                    }else if(app.FIS_Decision_Code__c.startsWith('O')){
                        result = 'review';
                    }else if(app.FIS_Decision_Code__c.startsWith('D')){
                        result = 'decline';
                    }
                    
                    //  --------------------------------------------------------------------------------------------------
                    //  JIM user for google tag tracking
                    //  --------------------------------------------------------------------------------------------------
                    recordUserJSON = one_utils.generateUserJSON ( app ) ;
                    
                    //  --------------------------------------------------------------------------------------------------
                    //  JIM cart checkout for google tag tracking
                    //  --------------------------------------------------------------------------------------------------
                    if ( jCookie != null )
                    {
                        String jFoo = jCookie.getValue() ;
                        
                        if ( jFoo.equalsIgnoreCase ( app.ID ) )
                            displayCartCheckout = true ;
                        
                        //  Remove the cookie
                        Cookie jNYETCookie = new Cookie ( 'j_Cookie', '', null, 0, false ) ;
                        ApexPages.currentPage().setCookies ( new Cookie[] { jNYETCookie } ) ;
                    }
                    
                    if ( displayCartCheckout )
                        cartCheckoutJSON = one_utils.generateCartCheckoutJSON ( app ) ;
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Application ID.'));
                }
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Application ID.'));
            }
        }catch(Exception e){
            System.debug(e.getMessage() + ' - line - ' + e.getLineNumber());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There was an error when loading the result of your application.'));
        }
    }
    
    public PageReference saveNewAccounts(){
        displayCartCheckout = false ;
        Savepoint sp = Database.setSavepoint();
 
        try{
            if(String.isEmpty(decision)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You need to select how to proceed.'));
            }else if(decision == 'Yes'){
                Decimal minAmount = productsMDT[0].Minimum_Funding__c;
                Decimal maxAccAmount = productsMDT[0].Maximum_Funding__c;
                //save new checking account and delete the other ones
                if(amount<minAmount){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The amount must be greater than or equal to $' + minAmount));                    
                }else if(maxAccAmount!=0 && amount>maxAccAmount){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The amount must be less than or equal to $' + maxAccAmount));                    
                }else if(amount<=maxAmount){
                    Boolean hasDebit = false;
                    Boolean hasEStatement = false;
                    String cardCode = '';
                    for(Online_Deposit_Application_Account__c chAcc : checkingAccs){
                        if(chAcc.Debit_card__c){
                            hasDebit = true;
                            cardCode = chAcc.Card_Code__c;
                        }
                        if(chAcc.E_Statement__c){
                            hasEStatement = true;
                        }
                    }

                    //create the second change checking account
                    Online_Deposit_Application_Account__c secondChanceCheckingAcc = new Online_Deposit_Application_Account__c();
                    secondChanceCheckingAcc.Product_Name__c = productsMDT[0].MasterLabel;
                    secondChanceCheckingAcc.Initial_Funding_amount__c = amount;
                    secondChanceCheckingAcc.Direct_Deposit__c = true;
                    secondChanceCheckingAcc.Online_Banking__c = true;
                    secondChanceCheckingAcc.Telephone_Banking__c = true;
                    secondChanceCheckingAcc.Debit_card__c = hasDebit;
                    secondChanceCheckingAcc.E_Statement__c = hasEStatement;
                    secondChanceCheckingAcc.Category__c = 'Checking';
                    secondChanceCheckingAcc.Major_Account_Type_Code__c = productsMDT[0].Major_Account_Type_Code__c; 
                    secondChanceCheckingAcc.Minor_Account_Type_Code__c = productsMDT[0].Product_Code__c;
                    secondChanceCheckingAcc.Application__c = app.Id;
                    secondChanceCheckingAcc.Card_Code__c = cardCode;
                    
                    if ( hasDebit )
                        secondChanceCheckingAcc.Agreement__c = productsMDT[0].Debit_Card_Agreement__c ;
                    else
                        secondChanceCheckingAcc.Agreement__c = productsMDT[0].ATM_Card_Agreement__c ;

//                    insert secondChanceCheckingAcc;
					access.insertObject ( secondChanceCheckingAcc ) ;
//                    delete checkingAccs;
					access.deleteObjects ( checkingAccs ) ;                    

                    app.FIS_Decision_Code__c = 'P912';
                    app.FIS_Credit_Limit__c = amount ;
                    
//  	            update app;
					access.updateObject ( app ) ;

                    result = 'waitingForCOCC';
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The amount cannot be greater than the sum of all the funding amounts from the checking accounts you previously selected ($'+maxAmount+').'));
                }

            }else if(decision == 'removeCheckingAcc'){
                //remove all the checking accs
                List<Online_Deposit_Application_Account__c> savingsAccs = new List<Online_Deposit_Application_Account__c>();
                for(Online_Deposit_Application_Account__c acc : listOfAccounts){
                    if(acc.Category__c=='Savings'){
                        //select the default ATM card code
                        acc.Card_Code__c = 'ABLK';
                        savingsAccs.add(acc);
                    }
                }
                if(!Test.isRunningTest()){
//                    delete checkingAccs;
					access.deleteObjects ( checkingAccs ) ;
                }
//                update savingsAccs;
                access.updateObjects ( savingsAccs ) ;
                
                app.FIS_Decision_Code__c = 'P912';
//                update app;
				access.updateObject ( app ) ;
                
                result = 'waitingForCOCC';
            }else if(decision == 'Cancel'){
                //cancel application
                app.FIS_Decision_Code__c = 'V002';
//              update app;
				access.updateObject ( app ) ;
                result = 'Withdraw';
            }            
        }catch(Exception e){
            Database.rollback( sp );
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        
        return null;
    }

    public PageReference sendToAdditionalInfoPage(){
        PageReference pr = Page.application_success;
        pr.getParameters().put('type','IDV' ) ;
        pr.getParameters().put ( 'aid', this.app.Id ) ;
        pr.getParameters().put ( 'kid', this.app.Upload_Attachment_Key__c ) ;
        return pr;
    }
}