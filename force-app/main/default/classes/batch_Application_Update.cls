global class batch_Application_Update implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts
{
	private one_settings__c os ;
    
    global batch_Application_Update ()
    {
		os = one_settings__c.getInstance ( 'settings' ) ;
    }
    
	global Database.QueryLocator start ( Database.Batchablecontext bc )
	{
		String query = '' ;
        
		query += 'SELECT ID, CreatedDate, ' ;
        query += 'Application_Processing_Status__c, Invalid_Email_Address__c, Funding_Options__c, FIS_Decision_Code__c, ' ;
        query += 'Pre_Bureau_Disposition__c, Post_Bureau_Disposition__c, RecordType.DeveloperName ' ;
		query += 'FROM one_Application__c ' ;
		query += 'WHERE ' ;
		query += '( Application_Status__c <> \'DECLINED\' AND Application_Status__c <> \'APPROVED\' AND Application_Status__c <> \'VOID\' ) ' ;
		query += 'AND ' ;
		query += '( Last_Updated__c = NULL OR Last_Updated__c < YESTERDAY ) ' ;
		query += 'AND ' ;
		query += '( FIS_Application_ID__c = NULL ) ' ;
		query += '' ;
        
		return Database.getQueryLocator ( query ) ;
	}

	global void execute ( Database.Batchablecontext bc, list<one_Application__c> oL )
	{
		System.debug ( '------------------ BATCH EXCUTE START ------------------' ) ;
        
        Date d = Date.today () ;
        
		if ( EmailFunctionsHelper.approvalMap == null )
        	EmailFunctionsHelper.approvalMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.fundingMap == null )
        	EmailFunctionsHelper.fundingMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.additionalMap == null )
        	EmailFunctionsHelper.additionalMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.trialMap == null )
        	EmailFunctionsHelper.trialMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.warningMap == null )
        	EmailFunctionsHelper.warningMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.denyMap == null )
        	EmailFunctionsHelper.denyMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.withdrawalMap == null )
        	EmailFunctionsHelper.withdrawalMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.declineIDVMap == null )
        	EmailFunctionsHelper.declineIDVMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.declineOFACMap == null )
        	EmailFunctionsHelper.declineOFACMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.declineFCRAMap == null )
        	EmailFunctionsHelper.declineFCRAMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.achRejectionMap == null )
        	EmailFunctionsHelper.achRejectionMap = new map<String, boolean> () ;
        
		if ( EmailFunctionsHelper.counterOfferMap == null )
        	EmailFunctionsHelper.counterOfferMap = new map<String, boolean> () ;
        
		List<String> lFundingIDs = new List<String> () ;
		List<String> lApprovedIDs = new List<String> () ;
		List<String> lAdditionalInfoIDs = new List<String> () ;
		List<String> lTrialIDs = new List<String> () ;
		List<String> lWarningIDs = new List<String> () ;
		List<String> lDeniedIDs = new List<String> () ;
		List<String> lGeoLocationIDs = new List<String> () ;
        List<String> lWithdrawalIDs = new List<String> () ;
        List<String> lDeclineIDVIDs = new List<String> () ;
        List<String> lDeclineOFACIDs = new List<String> () ;
        List<String> lDeclineFCRAIDs = new List<String> () ;
        List<String> lACHRejectIDs = new List<String> () ;
        List<String> lCounterOfferIDs = new List<String> () ;
				        
		if ( ( oL != null ) && ( oL.size () > 0 ) )
		{
			for ( one_Application__c o : oL )
			{
				o.Last_Updated__c = d ;
                
                //  Date calculations
                date cd = o.CreatedDate.date () ;
                Integer diff = Math.abs ( cd.daysBetween ( d ) ) ;
                
                System.debug ( 'Diff : ' + diff ) ;
                
                boolean hazAdditionalInfoEmail = EmailFunctionsHelper.sendTimeBasedAdditionalInfoEmail ( o ) ;
                
                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.additionalMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) && hazAdditionalInfoEmail )
                    lAdditionalInfoIDs.add ( o.Id ) ;
                
                boolean hazTrialEmail = EmailFunctionsHelper.sendTimeBasedTrialEmail ( o ) ;
                
                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) && hazTrialEmail )
                    lTrialIDs.add ( o.Id ) ;

                boolean hazFundingEmail = EmailFunctionsHelper.sendTimeBasedFundingEmail ( o ) ;
                
                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) && hazFundingEmail )
                    lFundingIDs.add ( o.Id ) ;
                
                //  --------------------------------------------------------------
                //  Handle Certain statuses outside of FIS or in the New LOS Path
                //  --------------------------------------------------------------
                if ( ( String.isBlank ( o.FIS_Application_ID__c ) || LOSHelper.isNewLOS ( o ) ) && ( o.CreatedDate != null ) )
                {
                    if ( o.RecordType.DeveloperName.equalsIgnoreCase ( 'deposits' ) )
                    {
                        //  Compliance
                        if ( ( diff >= 50 ) && ( diff < 55 ) )
                        {
                            if ( String.isBlank ( o.FIS_Decision_Code__c ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P014' ) ) )
                                o.FIS_Decision_Code__c = 'P064' ;
                        }
                        //  Void
                        else if ( diff >= 60 )
                        {
                            if ( String.isBlank ( o.FIS_Decision_Code__c ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'V004' ) ) )
                            {
                                o.FIS_Decision_Code__c = 'V004' ;
                                
                                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.withdrawalMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) )
                                    lWithdrawalIDs.add ( o.ID ) ;
                            }
                        }
                    }
                    else
                    {
                        //  Compliance
                        if ( ( diff >= 20 ) && ( diff < 25 ) )
                        {
                            if ( String.isBlank ( o.FIS_Decision_Code__c ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P014' ) ) )
                                o.FIS_Decision_Code__c = 'P064' ;
                        }
                        //  Void
                        else if ( diff >= 25 )
                        {
                            if ( String.isBlank ( o.FIS_Decision_Code__c ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'V004' ) ) )
                            {
                                o.FIS_Decision_Code__c = 'V004' ;
                                
                                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.withdrawalMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) )
                                    lWithdrawalIDs.add ( o.ID ) ;
                            }
                        }
                    }
                    
                    // Quiz abandon
                    if ( ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                         ( o.Application_Processing_Status__c.equalsIgnoreCase ( 'Quiz Abandoned' ) ) )
                    {
                        o.FIS_Decision_Code__c = 'D006' ;
                        o.Denial_Email_Sent__c = true ;
                        
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineIDVMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) )
                            lDeclineIDVIDs.add ( o.ID ) ;
                    }
                    
                    // Quiz present, too long
                    if ( ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                         ( o.Application_Processing_Status__c.equalsIgnoreCase ( 'Quiz Present' ) ) &&
                         ( diff >= 1 ) )
                    {
                        o.FIS_Decision_Code__c = 'D006' ;
                        o.Denial_Email_Sent__c = true ;
                        
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineIDVMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) )
                            lDeclineIDVIDs.add ( o.ID ) ;
                    }
                }
            }

            //  -----------------------------------------------------------------------------------
            //  Combined Email list
            //  -----------------------------------------------------------------------------------
            System.debug ( 'Sending Emails? - F:' + lFundingIDs.size () + ' I:' + lAdditionalInfoIDs.size () + ' T:' + lTrialIDs.size () ) ;
            
            if ( ( lFundingIds.size () > 0 ) || 
                 ( lAdditionalInfoIDs.size () > 0 ) ||
                 ( lTrialIDs.size () > 0 ) ||
                 ( lWithdrawalIDs.size () > 0 ) ||
                 ( lDeclineIDVIds.size () > 0 )
               )
            {
                //  This one is NOT a future call!
                ApplicationEmailFunctions.sendEmailsBATCH ( null, lFundingIDs, lAdditionalInfoIDs, lTrialIDs, 
                                                            null, null, lWithdrawalIDs, lDeclineIDVIds, null, 
                                                            null, null, null, null, null, null, null ) ;
            }

            //  -----------------------------------------------------------------------------------
            //  FINALLY
            //  -----------------------------------------------------------------------------------
            try
			{
				update oL ;
			}
			catch ( DMLException e )
			{
				System.debug ( 'Exception updating :: ' + e.getMessage () ) ;
			}
            
		}
		else
		{
			System.debug ( 'No applications found to update.' ) ;
		}
		
		System.debug ( '------------------ BATCH EXCUTE END ------------------' ) ;
	}
	
	global void finish ( Database.Batchablecontext bc )
	{
		// Get the ID of the AsyncApexJob representing this batch job
		// from Database.BatchableContext.
		// Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = [
			SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
			TotalJobItems, CreatedBy.Email
			FROM AsyncApexJob WHERE Id =
			:BC.getJobId()
		] ;
      
		// Send an email to the Apex job's submitter notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage () ;
   
		String[] toAddresses ;
		if ( ( os != null ) && ( String.isNotBlank ( os.Batch_Email__c ) ) )
		{
			toAddresses = new String [] 
			{
				os.Batch_Email__c 
			} ;
		}
		else
		{
			toAddresses = new String [] 
			{
				a.CreatedBy.Email 
			} ;
		}
   
		mail.setToAddresses ( toAddresses ) ;
		mail.setSubject ( 'Apex Application Batch Job :: ' + a.Status ) ;
   
		String message = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.\n' ;
        
        if ( ( os.Batch_Job_Max_Count__c != null ) && ( os.Batch_Job_Max_Count__c > 0 ) )
            message +=  '\nIndividual batches were limited to ' + os.Batch_Job_Max_Count__c + ' records.\n' ;
   
		mail.setPlainTextBody ( message ) ;
	   
		//  Don't email if running test
		if ( ! Test.isRunningTest () )
		{
			if ( os != null )
			{
				//  Only send email if enabled in custom settings
				if ( os.Batch_Email_Enabled__c == true )
					Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
			else
			{
				//  Always send if no custom settings found
				Messaging.sendEmail ( new Messaging.SingleEmailMessage[] { mail } ) ;
			}
		}
	}
}