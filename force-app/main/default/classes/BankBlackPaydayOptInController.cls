public with sharing class BankBlackPaydayOptInController {
    
    // Type = 'CHECKING' or 'SAVINGS' (handled in client-side controller)
    @AuraEnabled(cacheable=true)
    public static String getAllAcctInfo(String type) {
        try
        {
            Map<String, Map<String, Object> > allInfo = new Map<String, Map<String, Object> >();
            
            // First get complete list of DI accounts
            List<DIHelper.account> diFaList = getDIFiAccounts(type);
            
            if ( diFaList != null )
            {
                for ( DIHelper.account acc : diFaList )
                {
                    // Only add the account to the list if it's not closed
                    if ( !acc.accountStatus.equalsIgnoreCase('closed') )
                    {
                        Map<String, Object> accInfo = new Map<String, Object>();
                        accInfo.put('accountId', acc.accountId);
                        accInfo.put('fiCustomerGUID', acc.fiCustomerGUID);
                        accInfo.put('accountNumber', acc.accountNumber);
                        accInfo.put('category', acc.category);
                        accInfo.put('accountType', acc.accountType);
                        accInfo.put('ownershipType', acc.ownershipType);
                        accInfo.put('accountStatus', acc.accountStatus);
                        accInfo.put('displayAccountNumber', acc.displayAccountNumber);
                        
                        // Use accountNumber as the id in order to link to SF FA object
                        allInfo.put(acc.accountNumber, accInfo);
                    }
                }
            }
            
            // Next get Salesforce Financial Account object ids (if there is an existing corresponding account)
            // Add id to existing map
//            List<Financial_Account__c> sfAccts = new List<Financial_Account__c>();
            map<String,Financial_Account__c> sfAcctsMap = new map<String,Financial_Account__c> () ;
            if ( type.equalsIgnoreCase('checking') )
            {
                System.debug('sfAccts -- looking for associated SF FA checking accts');
//                sfAccts = getSFFiAccounts('CK');
                sfAcctsMap = getSFFiAccountsMap('CK');
            }
            else if ( type.equalsIgnoreCase('savings') )
            {
                System.debug('sfAccts -- looking for associated SF FA savings accts');
//                sfAccts = getSFFiAccounts('SAV');
                sfAcctsMap = getSFFiAccountsMap('SAV');
            }
            
            //  Rewrite to handle missing finanical acounts.
            //  Old code commented out below.
			if ( diFaList != null )
            {
                for ( String k : allInfo.keySet() )
                {
                    Map<String, Object> updatedAccInfo = allInfo.get(k);
                    
                    updatedAccInfo.put('faAcctnbr', k);
                    updatedAccInfo.put('contactId', getCurrentUserContactId() );
                    updatedAccInfo.put('BBPaydayOptInStatus', 'Inactive');
                    updatedAccInfo.put('BBPaydayOptInVal', false);    
                    updatedAccInfo.put('BBPaydayDisabled', false);
                    updatedAccInfo.put('BBPaydayComms', 'Push notification');
                    
                    if ( sfAcctsMap.containsKey ( k ) )
                    {
                        Financial_Account__c fa = sfAcctsMap.get ( k ) ;
                        
                        updatedAccInfo.put('faId', fa.Id);
                        updatedAccInfo.put('faAcctnbr', fa.ACCTNBR__c);
                        
                        if ( fa.TAXRPTFORPERSNBR__c != null )
                        {
                            updatedAccInfo.put('contactId', fa.TAXRPTFORPERSNBR__c);
                        }
                        else
                        {
                            updatedAccInfo.put( 'contactId', getCurrentUserContactId() );
                        }
                        
                        
                        if ( fa.BankBlack_Payday_OptIn__c != null )
                        {
                            updatedAccInfo.put('BBPaydayOptInStatus', fa.BankBlack_Payday_OptIn__c); // reference to picklist field
                            
                            if ( fa.BankBlack_Payday_OptIn__c.equalsIgnoreCase('Inactive') || fa.BankBlack_Payday_OptIn__c.equalsIgnoreCase('Opted Out') )
                            {
                                updatedAccInfo.put('BBPaydayOptInVal', false); // used to flip the toggle on the component
                            }
                            else
                            {
                                updatedAccInfo.put('BBPaydayOptInVal', true);
                            }
                            
                        }
                        else
                        {
                            updatedAccInfo.put('BBPaydayOptInStatus', 'Inactive');
                            updatedAccInfo.put('BBPaydayOptInVal', false);    
                        }
                        
                        if ((( fa.BankBlack_Payday_Disabled__c != null ) && (fa.BankBlack_Payday_Disabled__c)) || 
                            (( fa.Business_Account__c != null ) && (fa.Business_Account__c)))
                        {
                            updatedAccInfo.put('BBPaydayDisabled', true);
                        }
                        else
                        {
                            updatedAccInfo.put('BBPaydayDisabled', false);
                        }
                        
                        if ( fa.BankBlack_Payday_Comms_OptIn__c != null )
                        {
                            updatedAccInfo.put('BBPaydayComms', fa.BankBlack_Payday_Comms_OptIn__c);
                        }
                        else
                        {
                            updatedAccInfo.put('BBPaydayComms', 'Push notification');
                        }
                        
                        // Update displayed check value for Toggle if there are recent associated Payday Responses
                        boolean recentCheckVal = getCheckValue(fa.Id);
                        if ( recentCheckVal != null )
                        {
                            updatedAccInfo.put( 'BBPaydayOptInVal', recentCheckVal );
                        }
                        
                        // Update displayed comms value for Comms Opt-in if there are recent associated Payday Responses
                        String recentCommsVal = getCommsValue(fa.Id);
                        if ( recentCommsVal != null )
                        {
                            updatedAccInfo.put( 'BBPaydayComms', recentCommsVal );
                        }
                    }
                    
                    allInfo.put(k, updatedAccInfo);
                }
            }            
            
/*            
            if ( sfAccts.size() != 0 )
            {
                System.debug('sfAccts not empty');
                for ( Financial_Account__c fa : sfAccts )
                {
                    System.debug('Current sf fa record: ' + fa.Id);
                    System.debug('Current sf fa acctNum: ' + fa.ACCTNBR__c);
                    
                    for ( String k : allInfo.keySet() )
                    {
                        System.debug('Current allInfo key: ' + k);
                        
                        if ( k.equalsIgnoreCase(fa.ACCTNBR__c) )
                        {
                            System.debug('key equals account number.');
                            Map<String, Object> updatedAccInfo = allInfo.get(k);
                            updatedAccInfo.put('faId', fa.Id);
                            updatedAccInfo.put('faAcctnbr', fa.ACCTNBR__c);
                            
                            if ( fa.TAXRPTFORPERSNBR__c != null )
                            {
                                updatedAccInfo.put('contactId', fa.TAXRPTFORPERSNBR__c);
                            }
                            else
                            {
                                updatedAccInfo.put( 'contactId', getCurrentUserContactId() );
                            }
                            
                            
                            if ( fa.BankBlack_Payday_OptIn__c != null )
                            {
                                updatedAccInfo.put('BBPaydayOptInStatus', fa.BankBlack_Payday_OptIn__c); // reference to picklist field
                                
                                if ( fa.BankBlack_Payday_OptIn__c.equalsIgnoreCase('Inactive') || fa.BankBlack_Payday_OptIn__c.equalsIgnoreCase('Opted Out') )
                                {
                                    updatedAccInfo.put('BBPaydayOptInVal', false); // used to flip the toggle on the component
                                }
                                else
                                {
                                    updatedAccInfo.put('BBPaydayOptInVal', true);
                                }
                                
                            }
                            else
                            {
                                updatedAccInfo.put('BBPaydayOptInStatus', 'Inactive');
                                updatedAccInfo.put('BBPaydayOptInVal', false);    
                            }
                            
                            if ((( fa.BankBlack_Payday_Disabled__c != null ) && (fa.BankBlack_Payday_Disabled__c)) || 
                                (( fa.Business_Account__c != null ) && (fa.Business_Account__c)))
                            {
                                updatedAccInfo.put('BBPaydayDisabled', true);
                            }
                            else
                            {
                                updatedAccInfo.put('BBPaydayDisabled', false);
                            }
                            
                            if ( fa.BankBlack_Payday_Comms_OptIn__c != null )
                            {
                                updatedAccInfo.put('BBPaydayComms', fa.BankBlack_Payday_Comms_OptIn__c);
                            }
                            else
                            {
                                updatedAccInfo.put('BBPaydayComms', 'Push notification');
                            }
                            
                            // Update displayed check value for Toggle if there are recent associated Payday Responses
                            boolean recentCheckVal = getCheckValue(fa.Id);
                            if ( recentCheckVal != null )
                            {
                                updatedAccInfo.put( 'BBPaydayOptInVal', recentCheckVal );
                            }
                            
                            // Update displayed comms value for Comms Opt-in if there are recent associated Payday Responses
                            String recentCommsVal = getCommsValue(fa.Id);
                            if ( recentCommsVal != null )
                            {
                                updatedAccInfo.put( 'BBPaydayComms', recentCommsVal );
                            }
                            
                            allInfo.put(k, updatedAccInfo);
                        }
                    }
                }
            }
            else if ( diFaList != null ) // Have DI accounts, but no associated Salesforce FA objects
            {
                System.debug('sfAccts empty');
                
                for ( String k : allInfo.keySet() )
                {
                    Map<String, Object> updatedAccInfo = allInfo.get(k);
                    
                    updatedAccInfo.put('faAcctnbr', k);
                    updatedAccInfo.put( 'contactId', getCurrentUserContactId() );
                    updatedAccInfo.put('BBPaydayOptInStatus', 'Inactive');
                    updatedAccInfo.put('BBPaydayOptInVal', false);    
                    updatedAccInfo.put('BBPaydayDisabled', false);
                    updatedAccInfo.put('BBPaydayComms', 'Push notification');
                    
                    allInfo.put(k, updatedAccInfo);
                }
            }
*/            
            List < Map<String, Object> > allInfoList = new List< Map<String, Object> >();
            
            for ( String k2 : allInfo.keySet() )
            {
                allInfoList.add( allInfo.get(k2) );
            }
            
            System.debug('------ allInfoList items: ------');
            for ( Map<String, Object> item : allInfoList )
            {
                System.debug(item);
            }
            
            return JSON.serialize( allInfoList );
        }
        catch ( Exception e )
        {
            System.debug('optIn --- getAllAcctInfo --- error: ' + e);
            return null;
        }
    }
    
    // Type = 'CK' or 'SAV'
    private static map<String,Financial_Account__c> getSFFiAccountsMap(String type) 
    {
        map<String,Financial_Account__c> famap = new map<String,Financial_Account__c> () ;
        
        list<Financial_Account__c> faL = getSFFiAccounts(type) ;
        
        if ( ( faL != null ) && ( faL.size () > 0 ) )
        {
            for ( Financial_Account__c fa : faL )
            {
                faMap.put ( fa.acctnbr__c, fa ) ;
            }
        }
        
        return famap ;
    }
    
    public static List<Financial_Account__c> getSFFiAccounts(String type) {
        try
        {
            String currentContactId = getCurrentUserContactId();
    
            // Get Financial Accounts for which current user is 2ndary account holder
            List<Financial_Account_Contact_Role__c> facrList = [SELECT Financial_Account__c 
                                                                FROM Financial_Account_Contact_Role__c
                                                                WHERE Contact__c = :currentContactId
                                                                AND Financial_Account_Role__c = 'NonTax Signator'];
                    
            List<String> secondaryAcctIds = new List<String>();
            if ( facrList.size() != 0 )
            {
                for ( Financial_Account_Contact_Role__c facr : facrList )
                {
                    secondaryAcctIds.add(facr.Financial_Account__c);
                }
            }
            
            List<Financial_Account__c> sfFaList = new List<Financial_Account__c>();
            sfFaList = [SELECT Id, PRODUCT__c, Account_Number_Masked__c, ACCTNBR__c, TAXRPTFORPERSNBR__c, Business_Account__c, 
                               BankBlack_Payday_Disabled__c, BankBlack_Payday_Comms_OptIn__c, BankBlack_Payday_OptIn__c
                               FROM Financial_Account__c
                               WHERE (  ( ( Financial_Account__c.TAXRPTFORPERSNBR__c = :currentContactId ) // '0035000002JAcFgAAL' ) // <---- Mary Test contact for testing; should be ":currentContactId"
                                      OR ( Id IN :secondaryAcctIds ) )
                               AND MJACCTTYPCD__c = :type )];
            
            return sfFaList;
        }
        catch ( Exception e )
        {
            System.debug('optIn --- getSFFiAccounts --- error: ' + e);
            return null;
        }
    }
    
    /*
    @AuraEnabled(cacheable=true)
    public static String getSFFaInfo(String acctNumsString) {
        String currentContactId = getCurrentUserContactId();
        Type idArrType = Type.forName('List<string>');
        List<String> acctNums = (List<String>) JSON.deserialize(acctNumsString, idArrType);
        
        List<Financial_Account__c> sfFaList = new List<Financial_Account__c>();
        sfFaList = [SELECT Id, ACCTNBR__c, BankBlack_Payday_OptIn__c
                   FROM Financial_Account__c
                   WHERE Financial_Account__c.TAXRPTFORPERSNBR__c = '0035000002JAcFgAAL' // <---- Mary Test for testing; need to change this back ":currentContactId"
                   AND ACCTNBR__c IN :acctNums];
        
        System.debug('--- getSFFaIds --- sfFaList ---');
        System.debug(sfFaList);

        return JSON.serialize( sfFaList );
    }
	*/
    
    // Type = 'CHECKING' or 'SAVINGS' (handled in client-side controller)
    public static List<DIHelper.account> getDIFiAccounts(String type) { 
        try
        {
            Contact c = getCurrentContact();
            DIHelper dih = new DIHelper();        
            List<DIHelper.account> diFaList = new List<DIHelper.account>();
            boolean getCustSuccess = false;
            
            System.debug('getCurrentContact --- c: ' + c);
            if ( c != null )
            {
                if ( c.TaxID__c != null )
                {
                	getCustSuccess = dih.getCustomer(c.TaxID__c); // dih.getCustomer('042108937'); // <---- Mary Test's ssn for testing; need to change this back "dih.getCustomer(c.TaxID__c);"
                }
            }
            
            if ( getCustSuccess )
            {
                System.debug('Successfully retrieved customer information from DI.');
                if ( dih.customer.idType.equalsIgnoreCase('GUID') )
                {
                    String fiCustomerGUID = dih.customer.idValue;
                    
                    boolean getAcctsSuccess = dih.getAccounts(fiCustomerGUID);
                    
                    if ( getAcctsSuccess )
                    {
                        System.debug('Successfully retrieved customer accounts from DI.');
                        diFaList = dih.accounts;
                    }
                    else
                    {
                        System.debug('Failed to retrieve customer accounts from DI.');
                    }
                }
            }
            else
            {
                System.debug('Failed to retrieve customer information from DI.');
            }
            
            if ( Test.isRunningTest() )
            {
                System.debug('Running test... will insert fake DI accounts');
                DIHelper.account a = new DIHelper.account( '123', 'abc', '123456', '', 'checking', 'primary', 'open', '*456' );
                diFaList.add(a);
                a = new DIHelper.account( '234', 'abc', '234567', '', 'savings', 'primary', 'open', '*567' );
                diFaList.add(a);
            }
            
            List<DIHelper.account> diFaTypedList = new List<DIHelper.account>();
            
            if ( diFaList != null )
            {
                for ( DIHelper.account acc : diFaList )
                {
                    // adds accounts with matching type to only return checking or savings
                    if ( acc.accountType.equalsIgnoreCase(type) )
                    {
                        diFaTypedList.add( acc );
                    }
                }
            }
            
            // For testing
            for ( DIHelper.account acc2 : diFaTypedList )
            {
                System.debug('--- getDIFiAccounts --- acc ---');
                System.debug(acc2);
            }
            
           //  return JSON.serialize( diFaTypedList );
           return diFaTypedList;
        }
        catch ( Exception e )
        {
            System.debug('optIn --- getDIFiAccounts --- error: ' + e);
            return null;
        }
    }
    
    public static String getCurrentUserContactId() {
        try
        {
            User user = [SELECT ContactID FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
            return user.ContactId;
        }
        catch (Exception e)
        {
            return null;
        }
    }
    
    public static Contact getCurrentContact() {
        try
        {
            Id contactID = getCurrentUserContactId();
            System.debug('getCurrentUserContactId: '  + contactID);
            
            Contact c = [SELECT TaxID__c
                        FROM Contact
                        WHERE Id = :contactID];
    
            return c;
        }
        catch ( Exception e )
        {
            System.debug('getCurrentContact ------ exception: ' + e);
            return null;
        }
    }
    
    public static Boolean getCheckValue(String faId) {
        try
        {
            Boolean checkValue;
            
            List<Payday_Response__c> assocPaydayResponses = getPaydayResponses(faId, Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_OptIn').RecordTypeId); // only the toggle optIn record type responses
            
            if (assocPaydayResponses.size() != 0)
            {
                System.debug('Found >= 1 associated Payday Response records.');
                
                if ( assocPaydayResponses[0].OptIn_Status__c.equalsIgnoreCase('Pending') )
                {
                    checkValue = true;
                }
                else
                {
                    checkValue = false;
                }
            }
            else
            {
                System.debug('Found no associated Payday Response records.');
            }
            
            return checkValue;
        }
        catch ( Exception e )
        {
            System.debug('optIn --- getCheckValue --- error: ' + e);
            return null;
        }
    }
    
    public static String getCommsValue(String faId) {
        try
        {
            String commsValue;
            
            List<Payday_Response__c> assocPaydayResponses = getPaydayResponses(faId, Schema.SObjectType.Payday_Response__c.RecordTypeInfosByDeveloperName.get('Payday_Comms_OptIn').RecordTypeId); // only the toggle optIn record type responses
            
            if (assocPaydayResponses.size() != 0)
            {
                System.debug('Found >= 1 associated Payday Response records.');
                
                commsValue = assocPaydayResponses[0].Comms_Channels__c;
            }
            else
            {
                System.debug('Found no associated Payday Response records.');
            }
            
            return commsValue;
        }
        catch ( Exception e )
       	{
            System.debug('optIn --- getCommsValue --- error: ' + e);
            return '';
        }
    }
    
    // Returns Payday Response objects associated with a particular Financial Account
    // only pulls records of a certain type that were created today and weren't processed
    public static List<Payday_Response__c> getPaydayResponses(String faId, String type) {
        try
        {
            Date dt = Date.today();
            
            List<Payday_Response__c> responseList = new List<Payday_Response__c>();
            responseList = [SELECT OptIn_Status__c, Processing_Status__c, Response_Date__c, Financial_Account__c, Comms_Channels__c 
                           FROM Payday_Response__c
                           WHERE Financial_Account__c = :faId
                           AND Day_Only(Response_Date__c) = :dt 
                           AND Processing_Status__c = 'New'
                           AND RecordTypeId = :type 
                           ORDER BY Id Desc ]; // in order to quickly determine most recent toggle or comms channel value
            
            return responseList;
        }
        catch ( Exception e )
        {
            System.debug('optIn --- getPaydayResponses --- error: ' + e);
            return null;
        }
    }
    
}