@isTest
public class TestAdvocateProgramDisplayController  {
    @testSetup
    private static void setup() {
        User user = createUser();
        User me = [SELECT Id FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
        System.runAs(me) {
            Contact c = getTestContact();
			assignLevel(c);
            assignReferralInfo(c);
            associateRewards(c);
            associatePoints(c);
            associateBadges(c);
        }
    }
    
    private static Contact createContact() {
        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact c = new Contact(
            FirstName='TestContact',
            LastName='TestAdvocateProgramDisplayController',
            Advocate_Timestamp__c=System.now(),
            AccountId=a.Id,
            Facebook_Handle__c='TestHandle'
        );
        insert c;
        return c;
    }

    private static Contact getTestContact() {
        Contact result = new Contact();
        List<Contact> cs = [SELECT Id, Advocate_Level__c, Twitter_Handle__c
                            FROM Contact
                            WHERE FirstName = 'TestContact'
                            AND LastName='TestAdvocateProgramDisplayController'];
        if (cs.size() > 0) {
            result = cs[0];
        }
        return result;
    }

    private static User createUser () {
        Contact c = createContact();
		Profile p = [SELECT id FROM Profile WHERE Name = 'OneUnited Advocate Identity User'];

		User u = new User(
            FirstName = 'Test1',
            LastName = 'UserAdvocateProgramDisplayController',
            Email = 'test1@ouuser.com',
            Username = 'test1user1@ouuser.com',
            Alias = 'dsplctrl',
            CommunityNickname = 'tuser1',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = p.id,
            LanguageLocaleKey = 'en_US',
            IsActive=true,
            ContactId=c.Id
        );
        insert u;
        return u;
	}

    private static User getTestUser() {
        User result = new User();
        List<User> us = [SELECT Id, Username, LastName, Email, Alias,
                         CommunityNickname,TimeZoneSidKey, LocaleSidKey,
                         EmailEncodingKey, ProfileId, LanguageLocaleKey,
                         IsProfilePhotoActive
                         FROM User
                         WHERE Alias='dsplctrl'];

        if (us.size() > 0) {
            result = us[0];
        }
        return result;
    }

    private static void assignLevel(Contact c) {
        Advocate_Level__c al = new Advocate_Level__c();
        List<Advocate_Level__c> us = [SELECT Id
                         FROM Advocate_Level__c
                         WHERE Name='Advocate'];

        if (us.size() > 0) {
            al = us[0];
        } else {
            al.Name = 'Advocate';
            insert al;
        }
        
        c.Advocate_Level__c = al.Id;
        update c;
    }

    private static void assignReferralInfo(Contact c) {
        Referral_Code__c code = new Referral_Code__c();
        code.Name = 'BB34TX';
        code.Contact__c = c.Id;
        insert code;
    }

    private static void associateRewards(Contact c) {
        List<Advocate_Reward__c> rewards = createRewards();
        List<Advocate_Reward_Achieved__c> rewardedList = new List<Advocate_Reward_Achieved__c>();
        
        for (Advocate_Reward__c reward : rewards) {
            Advocate_Reward_Achieved__c rewarded = new Advocate_Reward_Achieved__c();
            rewarded.Advocate_Reward__c = reward.Id;
            rewarded.Contact__c = c.Id;
            rewardedList.add(rewarded);
        }

        insert rewardedList;
    }

    private static void associatePoints(Contact c) {
        List<Advocate_Activity__c> points = createPoints();
        List<Advocate_Activity_Achieved__c> achievedList = new List<Advocate_Activity_Achieved__c>();
        
        for (Advocate_Activity__c point : points) {
            Advocate_Activity_Achieved__c achieved = new Advocate_Activity_Achieved__c();
            achieved.Advocate_Activity__c = point.Id;
            achieved.Contact__c = c.Id;
            achievedList.add(achieved);
        }

        insert achievedList;
    }

    private static void associateBadges(Contact c) {
        List<Advocate_Badge__c> badges = createBadges();
        List<Advocate_Badge_Awarded__c> awardedList = new List<Advocate_Badge_Awarded__c>();
        
        for (Advocate_Badge__c badge : badges) {
            Advocate_Badge_Awarded__c awarded = new Advocate_Badge_Awarded__c();
            awarded.Badge__c = badge.Id;
            awarded.Contact__c = c.Id;
            awardedList.add(awarded);
        }

        insert awardedList;
    }

    private static List<Advocate_Reward__c> createRewards() {
        List<Advocate_Reward__c> result = new List<Advocate_Reward__c>();

        for (Integer i = 0; i < 4; i++) {
            result.add(new Advocate_Reward__c(Name='R'+i,Description__c='R'+i));
        }

        insert result;
        return result;
    }

    private static List<Advocate_Activity__c> createPoints() {
        List<Advocate_Activity__c> result = new List<Advocate_Activity__c>();

        for (Integer i = 0; i < 4; i++) {
            result.add(new Advocate_Activity__c(Name='A'+i,Activity_Points__c=100+i));
        }

        insert result;
        return result;
    }

    private static List<Advocate_Badge__c> createBadges() {
        List<Advocate_Badge__c> result = new List<Advocate_Badge__c>();

        for (Integer i = 0; i < 4; i++) {
            result.add(new Advocate_Badge__c(Name='B'+i,Description__c='B'+i));
        }

        insert result;
        return result;
    }

    @isTest
    private static void testOverallProgress() {
        Map<String,String> result = new Map<String,String>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getOverallProgress();
        }
        System.assertEquals(result.get('Name'), 'TestContact TestAdvocateProgramDisplayController');
    }

    @isTest
    private static void testCurrentLevel() {
        Advocate_Level__c result = new Advocate_Level__c();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getCurrentLevel();
        }
        System.assertEquals(result.Name, 'Advocate');
    }

    @isTest
    private static void testReferralInfo() {
        Map<String,String> result = new Map<String,String>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getReferralInfo();
        }
        System.assertEquals(result.get('ReferralCode'), 'BB34TX');
        System.assertEquals(result.get('ReferralURL'), 'oneunited.com/referral?BB34TX');
    }

    @isTest
    private static void testAllLevels() {
        List<Advocate_Level__c> result = AdvocateProgramDisplayController.getAllLevels();

        System.assertNotEquals(result, NULL);
    }

    @isTest
    private static void testAllBadges() {
        List<Advocate_Badge__c> result = AdvocateProgramDisplayController.getAllBadges();

        System.assertNotEquals(result, NULL);
    }

    @isTest
    private static void testAllPoints() {
        List<Advocate_Activity__c> result = AdvocateProgramDisplayController.getAllPoints();

        System.assertNotEquals(result, NULL);
    }

    @isTest
    private static void testAllRewards() {
        List<Advocate_Reward__c> result = AdvocateProgramDisplayController.getAllRewards();

        System.assertNotEquals(result, NULL);
    }

	@isTest
    private static void testSocialContact() {
        Contact result = new Contact();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getContact();
        }

        System.assertEquals(result.Facebook_Handle__c, 'TestHandle');    
    }

    @isTest
    private static void testAllMyRewards() {
        List<Advocate_Reward__c> result = new List<Advocate_Reward__c>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getMyRewards(false);
        }
        System.assertNotEquals(result, NULL);
        System.assertEquals(result.size(), 4);
    }

    @isTest
    private static void testMyRecentRewards() {
        List<Advocate_Reward__c> result = new List<Advocate_Reward__c>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getMyRewards(true);
        }
        System.assertNotEquals(result, NULL);
        System.assertEquals(result.size(), 3);
    }

    @isTest
    private static void testAllMyPoints() {
        List<Advocate_Activity__c> result = new List<Advocate_Activity__c>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getMyPoints(false);
        }
        System.assertNotEquals(result, NULL);
        System.assertEquals(result.size(), 4);
    }

    @isTest
    private static void testMyRecentPoints() {
        List<Advocate_Activity__c> result = new List<Advocate_Activity__c>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getMyPoints(true);
        }
        System.assertNotEquals(result, NULL);
        System.assertEquals(result.size(), 3);
    }

    @isTest
    private static void testAllMyBadges() {
        List<Advocate_Badge__c> result = new List<Advocate_Badge__c>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getMyBadges(false);
        }
        System.assertNotEquals(result, NULL);
        System.assertEquals(result.size(), 4);
    }

    @isTest
    private static void testMyRecentBadges() {
        List<Advocate_Badge__c> result = new List<Advocate_Badge__c>();
        User u = getTestUser();
        System.runAs(u) {
            result = AdvocateProgramDisplayController.getMyBadges(true);
        }
        System.assertNotEquals(result, NULL);
        System.assertEquals(result.size(), 3);
    }

    @isTest
    private static void testSaveContact() {
        String handle = 'NewlyCreatedTestHandle';
        Contact c = getTestContact();
        c.Twitter_Handle__c = handle;
        Test.startTest();
        AdvocateProgramDisplayController.saveContact(c);
        Test.stopTest();
        
        c = getTestContact();
        System.assertEquals(c.Twitter_Handle__c, handle);
    }

    @isTest
    private static void testAssignLevelIfEmtpy() {
        Test.startTest();

        /*Advocate_Level__c advLevel = [SELECT Id
                                      FROM Advocate_Level__c
                                      WHERE Name='Advocate'];*/

        /*if (settings.size() < 1) {
            AdvocateLevelSetting__mdt mdt = new AdvocateLevelSetting__mdt(MasterLabel='Default',AdvocateId__c=advLevel.Id);
            insert mdt;
        }*/

        String result = '';
        Contact contact = getTestContact();
        contact.Advocate_Level__c = NULL;        
        update contact;

        System.runAs(getTestUser()) {
            result = AdvocateProgramDisplayController.assignLevelIfEmtpy();
        }

        Test.stopTest();

        contact = getTestContact();
        AdvocateLevelSetting__mdt mtd = [SELECT AdvocateId__c FROM AdvocateLevelSetting__mdt WHERE MasterLabel = 'Default' LIMIT 1];
        System.assertEquals(mtd.AdvocateId__c, contact.Advocate_Level__c);
        System.assertEquals(mtd.AdvocateId__c, result);
    }

    @isTest
    private static void testSaveAttachmentException() {
        Boolean catched = false;
        // base64Data = 'Prueba de codificación BASE64!'
        String base64Data = 'UHJ1ZWJhIGRlIGNvZGlmaWNhY2nDs24gQkFTRTY0IQ==\n';
        String contentType = 'image/png';

        System.runAs(getTestUser()) {
            Test.startTest();
            try {
                // call should fail because the data cannot be interpreted as an image
                AdvocateProgramDisplayController.saveAttachment(base64Data, contentType);
            } catch (Exception ex) {
                catched = true;
            }
            Test.stopTest();
        }

        System.assert(catched);
    }
}