public with sharing class one_application_file_upload 
{
    /*  ----------------------------------------------------------------------------------------
     *  Page objects
     *  ---------------------------------------------------------------------------------------- */
 	public one_application__c o 
 	{ 
 		get; 
 		private set; 
 	}
 	
 	//  QR Code to allow site transfer to mobile device
 	public String qrCode 
 	{
 		get; 
 		private set; 
 	}
 	
 	public String appField
 	{
 		get;
 		private set;
 	}
 	
 	//  Attachment object to upload
	public Attachment attachment 
	{
		get 
		{
			if ( attachment == null )
				attachment = new Attachment () ;
			return attachment ;
		}
		
		set ;
	}
 
	//  Step control for panels on page
	public Integer flStepNumber
	{
		get ;
		private set ;
	}

	//  Additional language based on decision code
    public String codeAdditionalLanguage
    {
        get ;
		private set ;
    }
    
	//  List for document type drop down
    public List<SelectOption> docTypeItemDropDown
    {
        get
        {
	        return one_utils.docTypeItems ;
        }
        
		private set ;
    }
    
	//  List of needed documents
    public List<String> docTypeList
    {
        get ;
		private set ;
    }
    
    // Count of needed documents
    public Integer docTypeListSize
    {
    	get
    	{
    		Integer dtl = 0 ;
    		
    		if ( docTypeList != null )
    			dtl = docTypeList.size () ;
    			
    		return dtl ;
    	}
    	private set ;
    }
    
    /*  ----------------------------------------------------------------------------------------
     *  Errors
     *  ---------------------------------------------------------------------------------------- */
    public boolean errorName
    {
    	get ;
    	private set ;
    }
     
    public boolean errorFile
    {
    	get ;
    	private set ;
    }
     
    /*  ----------------------------------------------------------------------------------------
     *  Main Controller
     *  ---------------------------------------------------------------------------------------- */
    public one_application_file_upload ( ApexPages.Standardcontroller stdC )
	{
		//  Controls which panel we display, default of 0
		flStepNumber = 0 ;
		
        //  First check for application in session
        o = (one_application__c) stdC.getRecord () ;
        
        String aid = null ;
        String kid = null ;
 
 		//  If no session present, use querystring instead
        if ( ( o == null ) || ( o.Id == null ) )
		{
            System.debug ( '========== No session info found ==========' ) ;
            
			aid = ApexPages.currentPage().getParameters().get('aid') ;  // Can't use ID, salesforce picks it up internally!
			kid = ApexPages.currentPage().getParameters().get('kid') ;  // Don't want to call it key...
			
			o = null ;

			//  Both need to be passed in
			if ( ( aid != null ) && ( kid != null ) )
			{
				o = [ SELECT Id,Name,FIS_Application_ID__c,FIS_Decision_Code__c,Upload_Attachment_key__c FROM one_application__c WHERE Id = :aid ] ;

				//  Key must match record on ID passed in
				if ( ! kid.equals ( o.Upload_Attachment_key__c ) )
				{
		            System.debug ( '========== Key does not match ==========' ) ;
					o = null ;
				}
			}
		}
		else
		{
            System.debug ( '========== Using existing session info ==========' ) ;
            
			aid = o.Id ;
			kid = o.Upload_Attachment_Key__c ;
		}
		
		//  Check again for things existing
		if ( ( o != null ) && ( aid != null ) && ( kid != null ) )
		{
            System.debug ( '========== Valid application found ==========' ) ;
            
			//  Set up QR Code, need the full URL
			qrCode = one_utils.getDocUploadQRCode ( aid, kid, 125, 125 ) ;
			
			//  Application ID for page
			appField = null ;
			if ( String.isNotBlank ( o.FIS_Application_ID__c ) )
				appField = o.FIS_Application_ID__c ;
				
			//  Get the additional language needed
			codeAdditionalLanguage = one_utils.codesAdditionalLanguage.get ( o.FIS_Decision_Code__c ) ;
			
			//  Get the list of documents to display (copy since we'll be modifying it)
			docTypeList = one_utils.getDocTypeList ( o.FIS_Decision_Code__c ) ;
			
			if ( docTypeList != null )
			{
				//  Load attachments separately
				Attachment[] aL = [SELECT Name FROM Attachment WHERE ParentID = :aid ] ;
				
				//  Check the attachments (if any!)
				if ( aL != null )
				{
					for ( Attachment oA : aL )
					{
						System.debug ( 'Found attachement! - ' + oA.name ) ;
						Integer j = 0 ;
						while ( j < docTypeList.size () )
						{
							if ( docTypeList [j] == oA.name )
								docTypeList.remove ( j ) ;
							j++ ;
						}
					}
				}
				
				//  Set the step number
				flStepNumber = 1000 ;
			}
			else
			{
				System.debug ( 'No documents found for this code!' ) ;
			}
			
		}
	}
	
    /*  ----------------------------------------------------------------------------------------
     *  Validate & Upload the file
     *  ---------------------------------------------------------------------------------------- */
	public PageReference upload () 
	{
		boolean isValid = true ;
		
		//  Start with validation
		if ( String.isBlank ( attachment.name ) )
		{
			errorName = true ;
			isValid = false ;
		}
		
		if ( attachment.body == null )
		{
			errorFile = true ;
			isValid = false ;
		}
		
		if ( ! isValid )
		{
	        System.debug ( '========== Inputs not Valid! ==========' ) ;
	        flStepNumber = 1002 ;
			return null ;
		}
		
		attachment.OwnerId = UserInfo.getUserId () ;
		attachment.ParentId = o.Id ;    // the record the file is attached to
		attachment.IsPrivate = false ;  // Or the guest user can't query it!
 
		try 
		{
	        System.debug ( '========== Inserting Attachment ==========' ) ;
			insert attachment ;
			
	        System.debug ( '========== Flipping application status ==========' ) ;
	        o.Application_Processing_Status__c = 'Additional Documentation Received' ;
	        update o ;
		}
		catch (DMLException e)
		{
			System.debug ( '========== Error Inserting! ==========' ) ;

			return null;
		}
		finally 
		{
	        System.debug ( '========== Final stuff ==========' ) ;
	        
	        //  Set panel to success upload
			flStepNumber = 1001 ;
			
			//  Remove document from list
			Integer j = 0 ;
			while ( j < docTypeList.size () )
			{
				if ( docTypeList [j] == attachment.name )
					docTypeList.remove ( j ) ;
				j++ ;
			}
			
			//  If all documents are gone, go to overall success
			if ( docTypeList.size () == 0 )
				flStepNumber = 2000 ;
			
			//  Clear errors if any
			errorName = false ;
    		errorFile = false ;
			
			//  Reset attachment
			attachment = new Attachment () ; 
		}
		
		return null;
	}
}