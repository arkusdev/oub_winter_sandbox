@isTest
public class Email2TicketTest 
{
    static testMethod void TestBasicFunction() //√
    {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        // setup the data for the email
        email.subject = 'Test Contact Applicant';
        email.fromname = 'Firstname Lastname';
        env.fromAddress = 'jmarriner@oneunited.com';
        email.plainTextBody = 'This string will test that the message body is being parsed';
        
        // add an attachment, added an attachement->new
        Messaging.InboundEmail.BinaryAttachment attachment1 = new Messaging.InboundEmail.BinaryAttachment();
        attachment1.body = blob.valueOf('my attachment text');
        attachment1.fileName = 'textfile.txt';
        attachment1.mimeTypeSubType = 'text/plain';
        //insert attachment1;
        
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment1 };
        
        // call the email service class and test it with the data in the testMethod
        Email2Ticket emailProcess = new Email2Ticket();
        emailProcess.handleInboundEmail(email, env);
        
        // Verify the message body
        System.assertEquals('This string will test that the message body is being parsed', emailProcess.MsgBody);
        
        // Verify the subject line
        System.assertEquals('Test Contact Applicant', emailProcess.SubLine);
    }
    
    static testMethod void TestEmpties() //√
    {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        // setup the data for the email
        email.subject = null;
        email.fromname = 'Firstname Lastname';
        env.fromAddress = 'jmarriner@oneunited.com';
        email.plainTextBody = null;
                
        // add an attachment, added an attachement->new
        Messaging.InboundEmail.BinaryAttachment attachment1 = new Messaging.InboundEmail.BinaryAttachment();
        attachment1.body = blob.valueOf('my attachment text');
        attachment1.fileName = 'textfile.txt';
        attachment1.mimeTypeSubType = 'text/plain';
        //insert attachment1;
                
        // call the email service class and test it with the data in the testMethod
        Email2Ticket emailProcess = new Email2Ticket();
        emailProcess.handleInboundEmail(email, env);

        // Verify the message body ->new
        System.assertEquals('Could not parse message body', emailProcess.MsgBody);
        
        // Verify the subject line
        System.assertEquals('Could not parse subject line', emailProcess.SubLine);
    }
    
    static testMethod void TestGroup() //√
    {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       
        // setup the data for the email
        email.subject = 'Test Contact Applicant';
        email.fromname = 'Firstname Lastname';
      
        email.plainTextBody = 'This string will test that the message body is being parsed';
        
        Fax_queue__mdt fq = [
            SELECT Email__C
            FROM Fax_queue__mdt
            WHERE DeveloperName = 'UNITYVisaFax'
        ] ;
        
        email.toAddresses = new String[]{fq.Email__c};
        
        Test.startTest() ;

        // add an attachment, added an attachement->new
        Messaging.InboundEmail.BinaryAttachment attachment1 = new Messaging.InboundEmail.BinaryAttachment();
        attachment1.body = blob.valueOf('my attachment text');
        attachment1.fileName = 'textfile.txt';
        attachment1.mimeTypeSubType = 'text/plain';
        //insert attachment1;
        
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment1 };
     
        // call the email service class and test it with the data in the testMethod
        Email2Ticket emailProcess = new Email2Ticket();
        emailProcess.handleInboundEmail(email, env);
        
        // Verify the message body
        System.assertEquals('This string will test that the message body is being parsed', emailProcess.MsgBody);
        
        // Verify the subject line
        System.assertEquals('Test Contact Applicant', emailProcess.SubLine);
        
        Test.stopTest() ;
    }
}