public class runVoidUSAePayTransactionsQueueable implements Queueable, Database.AllowsCallouts {
    
    private List<String> applicationsIds;
    private boolean isRunTransaction;
    private Map<String,Decimal> amounts;
    
    public runVoidUSAePayTransactionsQueueable(List<String> applicationsIds,Map<String,Decimal> amountMap,boolean isRun) {
        this.applicationsIds = applicationsIds;
        this.amounts = amountMap;
        this.isRunTransaction = isRun;
    }
    
    public void execute(QueueableContext context) {
        
        Set<Application_Transaction__c> lapptToUpdate = new Set<Application_Transaction__c>();
        List<Application_Transaction__c> lapptToCreate = new List<Application_Transaction__c>();
        Set<Id> lapptToUpdateIds = new Set<Id>();
        Set<Id> lapptToCreateFromIds = new Set<Id>();
        List<Application_Transaction__c> lappt;
	    list<one_application__c> appListToUpdate = new list<one_application__c> () ;
          
        USAePay_Setting__mdt setting = [SELECT Pin_No_CVV__c, SourceKey_No_CVV__c, Transaction_Window__c FROM USAePay_Setting__mdt WHERE DeveloperName ='Settings_Unity_Visa' LIMIT 1];
        
        for(String appId:applicationsIds){
            System.debug([SELECT Application__c,Application_RecordType_DeveloperName__c,CreatedDate,Status__c,Customer_Number__c ,Payment_Method_ID__c,Transaction_Ref_Num__c FROM Application_Transaction__c WHERE Application__c= :appId ]);
            if(!Test.isRunningTest()){
            	lappt = [SELECT Application__c,Application_RecordType_DeveloperName__c,CreatedDate,Status__c,Customer_Number__c ,Payment_Method_ID__c,Transaction_Ref_Num__c,
                         Transaction_Settled_Date_Time__c, Unable_to_Void_Transaction_Date_Time__c
                         FROM Application_Transaction__c WHERE Application__c = :appId AND Status__c!='Voided' AND Status__c!='Failed' 
                         ORDER BY CreatedDate DESC LIMIT 1];
            }else{
                lappt = [SELECT Application__c,Application_RecordType_DeveloperName__c,CreatedDate,Status__c,Customer_Number__c ,Payment_Method_ID__c,Transaction_Ref_Num__c,
                         Transaction_Settled_Date_Time__c, Unable_to_Void_Transaction_Date_Time__c
                         FROM Application_Transaction__c WHERE Application__c= :appId 
                         ORDER BY CreatedDate DESC LIMIT 1];
            }
            if(lappt.size()>0){
                String settingName='Settings_Unity_Visa';
                if((lappt[0].Application_RecordType_DeveloperName__c!=null) && 
                   (lappt[0].Application_RecordType_DeveloperName__c.equalsIgnoreCase('DEPOSITS'))) {
                    settingName='Settings_Deposit_Account';
                }
                
        		USAePay_SOAP_Actions usaepayActions = new USAePay_SOAP_Actions(settingName);
                
                usaepayActions.transactionParameters.refNum = lappt[0].Transaction_Ref_Num__c;
                if(!isRunTransaction || ((lappt[0].CreatedDate + setting.Transaction_Window__c > System.now()) && lappt[0].Status__c != 'Expired' ) ){
                    //not expired
                    
                    USAePay_SOAP_Actions.ResponseObject response;
                    
                    if(!lapptToUpdateIds.contains(lappt[0].Id)){

                        if(!isRunTransaction){
                            
                            if ( ! lappt[0].Status__c.equalsIgnoreCase ( 'Settled' ) )
                            {
                            	response = usaepayActions.voidTransaction () ; 
                            }
                            else
                            {
                                //  Same day before 11 PM, attempt to void
                                if ( ( lappt[0].Transaction_Settled_Date_Time__c != null ) &&
                                     ( lappt[0].Unable_to_Void_Transaction_Date_Time__c == null ) )
                                {
                                    Datetime cdt = getCutoffGMT ( lappt[0].Transaction_Settled_Date_Time__c ) ;
                                    
                                    //  Cut off datetime of settle date has to be ahead of right now.
                                    if ( cdt > System.now () )
                                    {
                                        response = usaepayActions.voidTransaction(); 
                                    }
                                    else
                                    {
                                        response = new USAePay_SOAP_Actions.ResponseObject('CANNOT_VOID',9999, 'Cannot void this transction, submit refund instead', '' );
                                    }
                                }
                                else
                                {
                                    response = new USAePay_SOAP_Actions.ResponseObject('CANNOT_VOID',9999, 'Cannot void this transction, submit refund instead', '' );
                                }
                            }
                        }else{
                            usaepayActions.transactionParameters.amount = String.valueOf(amounts.get(appId));
                            response = usaepayActions.captureTransaction(); 
                        }
                        
                        if(response.statusCode == 200 && !Test.isRunningTest()){
                            
                            //SUCCESS
                            
                            if(isRunTransaction){
                                lappt[0].Status__c = 'Settled';
                                lappt[0].Settled_Amount__c = amounts.get(appId);
                                lappt[0].Transaction_Settled_Date_Time__c = System.now();
                            }else{
                                lappt[0].Status__c = 'Voided';
                                lappt[0].Transaction_Void_Date_Time__c = System.now();
                            }
                            
                            lapptToUpdate.add(lappt[0]);
                            lapptToUpdateIds.add(lappt[0].Id);
                        }
                        else if ( ( response.statusCode == 9999 ) && ( ! isRunTransaction ) )
                        {
                            lappt[0].Unable_to_Void_Transaction_Date_Time__c = System.now () ;
                            
                            lapptToUpdate.add(lappt[0]);
                            lapptToUpdateIds.add(lappt[0].Id);
                            
                            one_application__c o = new one_application__c () ;
                            o.ID = lappt[0].application__c ;
                            o.Application_Processing_Status__c = 'Unable to Void Transaction' ;
                            appListToUpdate.add ( o ) ;
                        }
                        else
                        {
                            //ERROR
                            if(isRunTransaction){lappt[0].Error_Msg__c = 'Error while capturing transaction - ' + response.statusCode + ' - ' + response.response;}else{lappt[0].Error_Msg__c = 'Error while voiding transaction - ' + response.statusCode + ' - ' + response.response;}
                            lappt[0].Transaction_Failed_Date_Time__c = System.now();
                            lappt[0].Status__c = 'Failed';
                            if(!lapptToUpdateIds.contains(lappt[0].Id)){lapptToUpdate.add(lappt[0]);lapptToUpdateIds.add(lappt[0].Id);}
                            
                            //try new transaction
                            if(isRunTransaction){
                                usaepayActions.paymentMethod.custNum = lappt[0].Customer_Number__c;
                                usaepayActions.paymentMethod.methodID = lappt[0].Payment_Method_ID__c;
                                usaepayActions.transactionParameters.amount = String.valueOf(amounts.get(appId));
                                response = usaepayActions.runCustomerTransaction(); 
                                
                                ////Creating the Application Transaction record
                                Application_Transaction__c appTransaction = new Application_Transaction__c();
                                appTransaction.Customer_Number__c  = usaepayActions.paymentMethod.custNum;
                                appTransaction.Payment_Method_ID__c  = usaepayActions.paymentMethod.methodID;
                                appTransaction.Settled_Amount__c  = amounts.get(appId);
                                appTransaction.Amount__c  = amounts.get(appId);
                                appTransaction.Application__c = lappt[0].Application__c;
                                if ( lappt[0].contact__c != null ) appTransaction.Contact__c = lappt[0].contact__c ;
                                
                                if(response.statusCode == 200){
                                    //SUCCESS
                                    appTransaction.Transaction_Ref_Num__c  = response.response; //transaction id
                                    appTransaction.Status__c  = 'Settled';
                                    appTransaction.Transaction_Authorized_Date_Time__c = System.now();
                                    appTransaction.Transaction_Settled_Date_Time__c = System.now();
                                    
                                    lapptToCreate.add(appTransaction);
                                }else{
                                    //ERROR
                                    appTransaction.Status__c  = 'Failed';
                                    appTransaction.Transaction_Failed_Date_Time__c = System.now();
                                    appTransaction.Error_Msg__c = 'Error while trying to create new transaction (Second try) - ' + response.statusCode + ' - ' + response.response;
                                    
                                    lapptToCreate.add(appTransaction);
                                }
                            }
                        }
                    }
                }else{
                    //expired, need to create new transaction
                    lappt[0].Status__c = 'Expired';
                    lappt[0].Transaction_Expired_Date_Time__c = System.now();
                    if(!lapptToUpdateIds.contains(lappt[0].Id)){lapptToUpdate.add(lappt[0]);lapptToUpdateIds.add(lappt[0].Id);}
                    
                    USAePay_SOAP_Actions.ResponseObject response;
                    
                    usaepayActions.paymentMethod.custNum = lappt[0].Customer_Number__c;
                    usaepayActions.paymentMethod.methodID = lappt[0].Payment_Method_ID__c;
                    usaepayActions.transactionParameters.amount = String.valueOf(amounts.get(appId));
                    response = usaepayActions.runCustomerTransaction(); 
                    
                    if(!lapptToCreateFromIds.contains(lappt[0].Id)){
                        //Creating the Application Transaction record
                        Application_Transaction__c appTransaction = new Application_Transaction__c();
                        appTransaction.Customer_Number__c  = usaepayActions.paymentMethod.custNum;
                        appTransaction.Payment_Method_ID__c  = usaepayActions.paymentMethod.methodID;
                        appTransaction.Settled_Amount__c  = amounts.get(appId);
                        appTransaction.Amount__c  = amounts.get(appId);
                        appTransaction.Application__c = lappt[0].Application__c;
                        if ( lappt[0].contact__c != null ) appTransaction.Contact__c = lappt[0].contact__c ;
                        
                        if(response.statusCode == 200 ){
                            //SUCCESS
                            appTransaction.Transaction_Ref_Num__c  = response.response; //transaction id
                            appTransaction.Status__c  = 'Settled';
                            appTransaction.Transaction_Authorized_Date_Time__c = System.now();
                            appTransaction.Transaction_Settled_Date_Time__c = System.now();
                            
                            lapptToCreate.add(appTransaction);
                            lapptToCreateFromIds.add(lappt[0].Id);
                        }else{
                            //ERROR
                            appTransaction.Status__c  = 'Failed';
                            appTransaction.Transaction_Failed_Date_Time__c = System.now();
                            appTransaction.Error_Msg__c = 'Error while trying to create new transaction from expired - ' + response.statusCode + ' - ' + response.response;
                            
                            lapptToCreate.add(appTransaction);
                            lapptToCreateFromIds.add(lappt[0].Id);
                        }
                    }
                }
            }
        }
        
        update new List<Application_Transaction__c>(lapptToUpdate);
        insert lapptToCreate;
        
        if ( appListToUpdate.size () > 0 )
	        update appListToUpdate ;
    }
    
    private Datetime getCutoffGMT ( Datetime dt )
    {
        Datetime cdt = Datetime.newInstanceGMT ( dt.yearGMT(), dt.monthGMT(), dt.dayGMT(), 04, 00, 00 ) ;
        
        if ( dt.hourGmt() >= 4 )
	        cdt = cdt.addDays ( 1 ) ;
        
        return cdt ;
    }
}