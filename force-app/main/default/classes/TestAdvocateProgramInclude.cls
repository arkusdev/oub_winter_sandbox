@isTest
public class TestAdvocateProgramInclude 
{
	private static final String APPLICATION_ADVANCED_MODE = '3297thuipwgyb8gfh314putgh24' ;
	
    public static void insertCustomSetting ()
    {	
    	// Group
        Group g = new Group () ;
        g.Name = 'queueTest';
        g.Type = 'Queue';
        insert g;
        
        Account a = new Account();
        a.Name = 'AccountTest';
        insert a;
        
        //  Referral partner account
        Account ar = new Account () ;
        ar.Name = 'Referral Test' ;
        ar.FederalTaxID__c = '132456789' ;
        insert ar ;
        
        //  Settings
        one_settings__c s = new one_settings__c ();
        s.Name = 'settings';
        s.UDAPLaws__c = 'Y';
        s.TypeProcessingCode__c = 15;
        s.SystemID__c = 'P';
        s.SubProduct__c = '000';
        s.Product__c = 'VSC';
        s.Plan__c = '000';
        s.OverlimitOptInIndicator__c = 'N';
        s.org_namespace__c = '';
        s.InstID__c = '000000000';
        s.EntrySourceCode__c = 'CINT';
        s.CorpID__c = '385169';
        s.Contact_Record_type_API_name__c = 'Prospect';
        s.Case_Owner_ID__c = g.Id;
        s.Bin__c = Decimal.valueOf('450.459');
        s.AssociationID__c = '06';
        s.ApplicationSourceCode__c = '001';
        s.Account_ID__c = a.Id;
        s.Application_Advanced_Mode__c = APPLICATION_ADVANCED_MODE ;
        s.Trial_Deposit_Validation_Limit__c = 2500.00 ;
        s.Default_Referral_Partner__c = ar.Id ;
        s.Application_Submission__c = true ;
        s.Referral_Tracking_Hold_Days__c = 7 ;
        s.Rakuten_Tracking_ID__c = 'TESTID' ;
        s.Rates_URL__c = 'https://www.test.com' ;
        
        insert s;
    }

	static testmethod void testAdvocateINCLUDE ()
	{
		insertCustomSetting () ;
     
        Test.startTest () ;
        
        Contact c = new Contact () ;
        c.firstName = 'Test' ;
        c.lastName = 'User' ;
        c.email = 'test@user.com' ;
        insert c ;
        
        Referral_Code__c rc = new Referral_Code__c () ;
        rc.Contact__c = c.ID ;
        rc.Name = 'JIMTEST' ;
        insert rc ;
        
		//  Parameters
		ApexPages.currentPage().getParameters().put ( 'apcd', 'JIMTEST' ) ;

		//  Testing...
		AdvocateProgramIncludeController apic = new AdvocateProgramIncludeController () ;
		
		//  asserts
		System.assertEquals ( apic.validAdvocateCode, true ) ;
		System.assertEquals ( apic.advocateProgramCode, 'JIMTEST' ) ;
		
		Test.stopTest () ;
    }
}