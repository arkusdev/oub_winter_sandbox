public with sharing class OneUnitedChatterController 
{
    @AuraEnabled
    public static String getGroupID ()
    {
        String groupID ;
        
        list<CollaborationGroup> cgL = null ;
        
        try
        {
            cgL = [
                SELECT ID
                FROM CollaborationGroup
                WHERE Name = 'All OneUnited Bank'
                LIMIT 1
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( cgL != null ) && ( cgL.size () > 0 ) )
        {
            groupID = cgL.get ( 0 ).ID ;
        }
        
        return groupID ;
    }
}