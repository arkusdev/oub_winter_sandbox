public class ContactQueue implements Queueable 
{
    private list<Automated_Email__c> aeL ;
    private list<OneCampaign_Member__c> ocmL ;
    
    public ContactQueue ()
    {
        
    }
    
    public ContactQueue ( list<Automated_Email__c> xL )
    {
        aeL = xL ;
    }
    
    public ContactQueue ( list<OneCampaign_Member__c> xL )
    {
        ocmL = xL ;
    }
    
    public void execute(QueueableContext context) 
    {
        try
        {
            if ( ( aeL != null ) && ( aeL.size () > 0 ) )
                insert aeL ;
            
            if ( ( ocmL != null ) && ( ocmL.size () > 0 ) )
                insert ocmL ;
        }
        catch (Exception e)
        {
            // ?
        }
    }
}