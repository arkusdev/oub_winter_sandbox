({
	doInit : function(component, event, helper) {
		helper.loadContact(component);
	},
	doSave: function(component, event, helper) {
		helper.saveRecord(component);
	},
	doCloseModal: function(component, event, helper) {
		helper.closeModal(component);
	}
})