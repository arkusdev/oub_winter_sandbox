({
	closeModal : function(component) {		
		component.find("overlayLib").notifyClose();
	},
	loadContact : function(component) {
		var action = component.get ( "c.getContact" ) ;
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set ( 'v.con', response.getReturnValue() ) ;
            } else {
                console.log('Problem getting map, response state: ' + state);
            }
        });        
        $A.enqueueAction(action);
	},
	saveRecord : function(component) {		
		var toastEvent = $A.get("e.force:showToast");
		var action = component.get ( "c.saveContact" ) ;
		action.setParams({
            "con": component.get('v.con')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
            	toastEvent.setParams({
			        "title": "Success!",
			        "type": "success",
			        "message": "Your social media details were updated!"
			    });
			    toastEvent.fire();
                component.set ( 'v.con', response.getReturnValue() ) ;
                component.find("overlayLib").notifyClose();
                console.log('contact', response.getReturnValue());
            } else if (state == "ERROR") {
            	var errors = action.getError();                
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);
                    toastEvent.setParams({
				        "title": "Error",
				        "type": "error",
				        "message": "An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues."
				    });
				    toastEvent.fire();
                }
            }
        });        
        $A.enqueueAction(action);
	}
})