({
	getDebitCards : function(component, event, helper) {
		var action = component.get('c.getCards');
        action.setParams({cardType:'Debit Card'});
        
        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            
            if (state === 'SUCCESS') {
                data = JSON.parse( response.getReturnValue() );
                component.set('v.debitCards', data);
                console.log('got debit card designs');
                
                //console.log(data);
            } else {
                console.error(Object.assign({},response.getError()));
                console.log('get debit cards - error');
            }
        });
        $A.enqueueAction(action);
	},
    
    getAtmCards : function(component, event, helper) {
        var action = component.get('c.getCards');
        action.setParams({cardType:'ATM Card'});
        
        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            
            if (state === 'SUCCESS') {
                data = JSON.parse( response.getReturnValue() );
                component.set('v.atmCards', data);
                console.log('got atm card designs');
                
                //console.log(data);
            } else {
                console.error(Object.assign({},response.getError()));
                console.log('get atm cards - error');
            }
        });
        $A.enqueueAction(action);
    },
    
    getDebitLast4List : function(component, event, helper) {
        var action = component.get('c.getServices');
        action.setParams({cardType:'Debit Card'});
        
        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            
            if (state === 'SUCCESS') {
                data = JSON.parse( response.getReturnValue() );
                component.set('v.debitLast4List', data);
                console.log('got debitLast4List');
                
                //console.log(data);
            } else {
                console.error(Object.assign({},response.getError()));
                console.log('get debitLast4List - error');
            }
        });
        $A.enqueueAction(action);
    },
    
    getAtmLast4List : function(component, event, helper) {
        var action = component.get('c.getServices');
        action.setParams({cardType:'ATM Card'});
        
        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            
            if (state === 'SUCCESS') {
                data = JSON.parse( response.getReturnValue() );
                component.set('v.atmLast4List', data);
                console.log('got atmLast4List');
                
                //console.log(data);
            } else {
                console.error(Object.assign({},response.getError()));
                console.log('get atmLast4List - error');
            }
        });
        $A.enqueueAction(action);
    }
})