({
	init: function(component, event, helper) {
		helper.getDebitCards(component, event, helper);
        helper.getAtmCards(component, event, helper);
        helper.getDebitLast4List(component, event, helper);
        helper.getAtmLast4List(component, event, helper);
	},
    
    handleLast4Select: function(component, event, helper) {
        // Change label & value
        var selectedMenuItemValue = event.getParam("value");
        //console.log('selectedMenuItemValue: ' + selectedMenuItemValue);
        var buttonMenu = component.find('cardNumLast4');
        buttonMenu.set('v.label', selectedMenuItemValue);
        buttonMenu.set('v.value', selectedMenuItemValue);
        
        // Remove error display
        $A.util.removeClass( buttonMenu, 'button-menu-error' );
        component.set('v.last4Error', false);
        component.set('v.selectAgain', false);
    },
    
    handleCardSelect: function(component, event, helper) {
        // Reset last4Digits menu
        var last4Menu = component.find('cardNumLast4');
        if ( last4Menu != null )
        {
            last4Menu.set('v.label', '-- None --');
            last4Menu.set('v.value', '-- None --');
        }
        $A.util.removeClass( last4Menu, 'button-menu-error' );
        component.set('v.last4Error', false);
        component.set('v.selectAgain', false);
        
        // Change label & value
        var selectedMenuItemValue = event.getParam("value");
        var buttonMenu = component.find('cardTypeChoice');
        buttonMenu.set('v.label', selectedMenuItemValue);
        buttonMenu.set('v.value', selectedMenuItemValue);
        
        if ( selectedMenuItemValue == '-- None --')
        {
            component.set('v.showCards', false);
        }
        else
        {
        	component.set('v.showCards', true);
        }
    },
    
    /*
    validInput: function(component, event, helper) {
        var last4 = document.getElementById('cardNumLast4');
        
   		if (last4.value.length > last4.maxLength)
        {
            last4.value = last4.value.slice(0, last4.maxLength);
        }
        
        if (last4.value.length > 0)
        {
            document.getElementById('required-error-msg').setAttribute('style','display: none;');
            document.getElementById('invalid-error-msg').setAttribute('style','display: none;');
            
            if ( document.getElementById('last4Container').classList.contains('slds-has-error') )
            {
                document.getElementById('last4Container').classList.remove('slds-has-error');
            }
        }
    },
    */
    
    cardClick: function(component, event, helper) {
        var cards = document.querySelectorAll("button[name='cardChoice']"), i;
        // var last4Digits = document.getElementById('cardNumLast4').value;
        var last4Digits = component.find('cardNumLast4').get('v.value');
        //console.log('last4Digits: ' + last4Digits);
        //console.log('cards: ' + cards);
        
        component.set('v.cardSelected', event.currentTarget.id);

        // Toggle error display for last4Digits menu
        // if (  ( last4Digits == null ) || ( last4Digits == '' ) || ( last4Digits.length != 4 ) )
        if ( isNaN(last4Digits) )
        {
            // Remove selected display for all cards
            for (i = 0; i < cards.length; i++) {
                cards[i].classList.remove('selected');
            }
            
            component.set('v.selectAgain', true);
            // document.getElementById('last4Container').classList.add('slds-has-error');
            $A.util.addClass( component.find('cardNumLast4'), 'button-menu-error' );

            component.set('v.last4Error', true);
            
            /*
            if ( ( last4Digits == null ) || ( last4Digits == '' ) )
            {
                document.getElementById('required-error-msg').setAttribute('style','display: inline-block;');
            }
            else if ( last4Digits.length != 4 )
            {
                document.getElementById('invalid-error-msg').setAttribute('style', 'display: inline-block;');
            }
            */
        }
        else
        {
            // Toggle selected display for all cards
            for (i = 0; i < cards.length; i++) {
                //console.log(cards[i].id);
                if ( cards[i].id == event.currentTarget.id )
                {
                    cards[i].classList.add('selected');
                }
                else 
                {
                    cards[i].classList.remove('selected');
                }
            }
            
            // Remove error display
            $A.util.removeClass( component.find('cardNumLast4'), 'button-menu-error' );
            component.set('v.last4Error', false);
            component.set('v.selectAgain', false);
            
            // Show confirmation modal
            component.set('v.showModal', true);
        }
    },
    
    submit: function(component, event, helper) {
        component.set('v.showModal', false);
        
        // var last4Digits = document.getElementById('cardNumLast4').value;
        var last4Digits = component.find('cardNumLast4').get('v.value');
        var cardSelected = component.get('v.cardSelected');
        
        component.set('v.loading', true);
        
        var cardType = component.find('cardTypeChoice').get('v.value');
        var plastic = cardSelected;
        
        if ( cardType == 'ATM' )
        {
            plastic = plastic.slice(0, plastic.length-4);
        }
        
        // Developer Names don't match picklist choices on Ticket object
        if ( plastic == 'Liberty' )
        {
            plastic = 'Lady';
        }
        else if ( plastic == 'Black_Classic')
        {
            plastic = 'Black Classic';
        }
        
        var infoMap = {};
        
        infoMap["last4Digits"] = last4Digits;
        infoMap["cardType"] = cardType;
        infoMap["plastic"] = plastic;
        
        var action = component.get('c.submitTicket');
        action.setParams({info:infoMap});
        
        action.setCallback(this, function(response) {
            var state, confirmNum;
            state = response.getState();
            
            if (state === 'SUCCESS') {
                confirmNum = response.getReturnValue();
                console.log('successfully submitted ticket');
                
                if ( ( confirmNum != null ) && ( confirmNum != '' ) && ( confirmNum != 'Invalid last 4 digits' ) )
                {
                    //console.log('confirmation #: ' + confirmNum);
                    
                    // toggle display
                    component.set('v.confirmNum', confirmNum);
                    component.set('v.completedForm', true);
                }
                else 
                {
                    console.log('submitTicket --- no ticket name/confirmation number returned');
                    // alert('Sorry, something went wrong while processing your request.');
                    component.set('v.error', true);
                }
                /*
                    else
                    {
                        console.log('submitTicket --- last 4 digits entered incorrect');
                        component.set('v.digitsError', true);
                    }
                    */
                } else {
                    console.error(Object.assign({},response.getError()));
                    console.log('submitTicket error');
                    // alert('Sorry, something went wrong while processing your request.');
                    component.set('v.error', true);
                }
                
                component.set('v.loading', false);
            });
        $A.enqueueAction(action);        
    },
    
    closeModal: function(component, event, helper) {
        component.set('v.showModal', false);
    },
    
    closeError: function(component, event, helper) {
        component.set('v.digitsError', false);
    }
})