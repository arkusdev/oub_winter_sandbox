({
	doInit : function(component, event, helper) {
		
        // Prepare the action to load account record
        var action = component.get("c.getApplication");
        action.setParams({"appID": component.get("v.recordId")});
        
        // Load application
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.app", response.getReturnValue());
                
                var action2 = component.get("c.getOverrides");
                action2.setParams({"app": component.get("v.app")});
        
                // actionception load overrides
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        component.set("v.ov", response.getReturnValue());
                    } else {
                        console.log('Problem getting overrides, response state: ' + state);
                    }
                });
                
                $A.enqueueAction(action2);
            } else {
                console.log('Problem getting application, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	},
    
	handleOverride: function(component, event, helper) {
        // check via the helper if the info is correct
        if(helper.validateOverride(component)) {
             
             // Prepare the action
             var saveOverride = component.get("c.runMinfraudOverride");
             saveOverride.setParams({
                 "app": component.get("v.app"),
                 "overrideReason": component.get("v.reason")
             });
             
			// 	Configure the response handler for the action
 			saveOverride.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Application Minfraud Overridden",
                        "message": "The application's Minfraud hit was overridden."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem overriding application, response state: ' + state);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });
             
             // Send the request to override the application
             $A.enqueueAction(saveOverride);
         }
    },
    
	handleDecline: function(component, event, helper) {
        // check via the helper if the info is correct
        if(helper.validateDecline(component)) {
             
             // Prepare the action
             var saveDecline = component.get("c.declineApplicationMinfraud");
             saveDecline.setParams({
                 "app": component.get("v.app"),
                 "declineReason": component.get("v.reason")
             });
             
			// 	Configure the response handler for the action
 			saveDecline.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {         
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Application Decline",
                        "message": "The application was declined."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem declining application, response state: ' + state);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });
             
             // Send the request to override the application
             $A.enqueueAction(saveDecline);
         }
    }
})