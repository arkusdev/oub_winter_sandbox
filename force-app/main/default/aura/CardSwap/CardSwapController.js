({
	doInit : function(component, event, helper) {

        // Prepare the action to load account record
        var action = component.get("c.getSwapURL");
        
        // Load application
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log ( 'State :: ' + state ) ;
            if(state === "SUCCESS") {
                var foo = response.getReturnValue () ;
                component.set("v.url", foo );
                console.log ( 'URL :: ' + foo ) ;
                
                window.open ( foo,'_top' ) ;
            }
        });
    	
        $A.enqueueAction(action);
    }
})