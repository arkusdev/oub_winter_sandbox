({
	checkExistingForm: function(component, event, helper) {
        try
        {
            var faId = component.get("v.faId");
            var account = component.get("v.account");
            var contactId = component.get("v.contactId");
            
            if ( ( contactId != null ) && ( account.accountNumber != null ) )
            {
                var action = component.get("c.getExistingTicket");
                action.setParams({contactId:contactId,faAcctNum:account.accountNumber});
                
                action.setCallback(this, function(response) {
                    var state, tid;
                    state = response.getState();
                    
                    if (state === 'SUCCESS') {
                        console.log('successfully ran getExistingTicket.');
                        
                        if ( response.getReturnValue() != null )
                        {
                            tid = response.getReturnValue().Id;
                            
                            if ( tid != null ) // non-null tid
                            {
                                //console.log('successfully retrieved ticket id.');
                                //console.log('tid: ' + tid);
                                component.set("v.existingForm", true);
                                component.set("v.existingTid", tid);
                            }
                        }
                    } 
                    else // error --- getExistingTicket
                    {
                        console.error(Object.assign({},response.getError()));
                    }
                });
                $A.enqueueAction(action);
            }
            else if ( faId != null )
            {
                //console.log('faId not null.')
                var action = component.get("c.getFinancialAccountById");
                action.setParams({faId:faId});
                
                action.setStorable();
                action.setCallback(this, function(response) {
                    var state, faAcctNum, contactId;
                    state = response.getState();
                    
                    if (state === 'SUCCESS') {
                        if ( response.getReturnValue() != null ) // non-null financial account
                        {
                            console.log('successfully retrieved financial account!')
                            faAcctNum = response.getReturnValue().ACCTNBR__c;
                            contactId = response.getReturnValue().TAXRPTFORPERSNBR__c;
                            //console.log('faAcctNum: ' + faAcctNum);
                            //console.log('contactId: ' + contactId);
                            
                            if ( contactId == null )
                            {
                                contactId = $User.contactId;
                                //console.log('contactId: ' + contactId);
                            }
                            
                            var action2 = component.get("c.getExistingTicket");
                            action2.setParams({contactId:contactId,faAcctNum:faAcctNum});
                            
                            action2.setCallback(this, function(response2) {
                                var state2, tid;
                                state2 = response2.getState();
                                
                                if (state2 === 'SUCCESS') {
                                    console.log('successfully ran getExistingTicket.');
                                    
                                    if ( response2.getReturnValue() != null )
                                    {
                                        tid = response2.getReturnValue().Id;
                                    
                                        if ( tid != null ) // non-null tid
                                        {
                                            //console.log('successfully retrieved ticket id.');
                                            //console.log('tid: ' + tid);
                                            component.set("v.existingForm", true);
                                            component.set("v.existingTid", tid);
                                        }
                                    }
                                } 
                                else // error --- getExistingTicket
                                {
                                    console.error(Object.assign({},response2.getError()));
                                }
                            });
                            $A.enqueueAction(action2);
                        }
                    } 
                    else // error --- getFinancialAccountById
                    {
                        console.error(Object.assign({},response.getError()));
                    }
                });
                $A.enqueueAction(action);
            }
            else if ( account.accountNumber != null )
            {
                //console.log('Trying to find FA by accountnumber; faId is null.');
                var action = component.get("c.getFinancialAccountByAcctNum");
                action.setParams({acctNum:account.accountNumber});
                
                action.setStorable();
                action.setCallback(this, function(response) {
                    var state, faAcctNum, contactId;
                    state = response.getState();
                    
                    if (state === 'SUCCESS') {
                        if ( response.getReturnValue() != null ) // non-null financial account
                        {
                            console.log('successfully retrieved financial account!')
                            faAcctNum = response.getReturnValue().ACCTNBR__c;
                            contactId = response.getReturnValue().TAXRPTFORPERSNBR__c;
                            //console.log('faAcctNum: ' + faAcctNum);
                            //console.log('contactId: ' + contactId);
                            
                            var action2 = component.get("c.getExistingTicket");
                            action2.setParams({contactId:contactId,faAcctNum:faAcctNum});
                            
                            action2.setCallback(this, function(response2) {
                                var state2, tid;
                                state2 = response2.getState();
                                
                                if (state2 === 'SUCCESS') {
                                    console.log('successfully ran getExistingTicket.');
                                    
                                    if ( response2.getReturnValue() != null )
                                    {
                                        tid = response2.getReturnValue().Id;
                                    
                                        if ( tid != null ) // non-null tid
                                        {
                                            //console.log('successfully retrieved ticket id.');
                                            //console.log('tid: ' + tid);
                                            component.set("v.existingForm", true);
                                            component.set("v.existingTid", tid);
                                        }
                                    }
                                } 
                                else // error --- getExistingTicket
                                {
                                    console.error(Object.assign({},response2.getError()));
                                }
                            });
                            $A.enqueueAction(action2);
                        }
                    } 
                    else // error --- getFinancialAccountByAcctNum
                    {
                        console.error(Object.assign({},response.getError()));
                    }
                });
                $A.enqueueAction(action);
            }
        }
        catch ( error )
        {
            console.error('checkExistingForm --- error: ' + error);
        }
    },
    
    // Opens existing direct deposit form if associated ticket exists
    getExistingForm: function(component, event, helper) {
        try
        {
            var tid = component.get("v.existingTid");
            
            if ( tid != null )
            {
                 window.open('https://' + window.location.href.split('/')[2] + '/advocate/direct_deposit_form?tid='+tid);
                //  window.open('https://dev4-oneunited.cs50.force.com/application/direct_deposit_form?tid='+tid);
            }
            else
            {
                console.log('No existing ticket id --- pdf icon displayed in error.');
            }
        }
        catch (error)
        {
            console.error('getExistingForm helper --- error: ' + error);
        }
    }
})