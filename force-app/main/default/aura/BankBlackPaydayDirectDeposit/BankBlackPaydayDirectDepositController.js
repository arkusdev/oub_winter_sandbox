({
    initBBPaydayDirectDeposit: function(component, event, helper) {
        // Checks for existing ticket/direct deposit form, and displays the pdf icon if true
        try
        {
        	helper.checkExistingForm(component, event, helper);
        }
        catch ( error )
        {
            console.error('initDirectDeposit --- error: ' + error);
        }
        
        /*
        const svgSource = '<svg class="slds-icon" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" id="gdoc">' +
                          	'<path d="M3.4 0c-1 0-1.9.9-1.9 1.9v20.2c0 1 .9 1.9 1.9 1.9h17.2c1 0 1.9-.9 1.9-1.9V7.6L15.4 0h-12z"' +
       						'fill-rule="evenodd" clip-rule="evenodd" fill="#0055CC"></path>' +
       						'<path d="M5.3 14h8.1v.8H5.3zm0 1.8h8.1v.8H5.3zm0 1.8h8.1v.8H5.3zm0 1.8h4.6v.8H5.3z" fill="#fff"></path>' +
							'<g fill-rule="evenodd" clip-rule="evenodd">' +
                                '<path d="M22.5 7.6V8h-4.8s-2.4-.5-2.3-2.5c0 0 .1 2.1 2.2 2.1h4.9z" fill="#2D6FE4"></path>' +
                        		'<path d="M15.4 0v5.5c0 .6.4 2.1 2.3 2.1h4.8L15.4 0z" opacity=".5" fill="#fff"></path>' +
            				'</g></svg>';
    	component.find('mySvg').set('v.value', svgSource);
        */
    },
    
   	openModal: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", true);
   },
  
    closeModal: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
    },
    
    closeAlert: function(component, event, helper) {
        component.set("v.isAlertOpen", false);
    },
    
    // calls custom controller method to generate ticket with all necessary information for direct deposit form
    // passes ticket information to VF page that will render the direct deposit form
    submit: function(component, event, helper) {
        try
        {
            // Check that all required fields are entered before allowing user to continue
            var companyName = component.find("companyName").get("v.value");
            var amountChoice = component.find("amountChoice").get("v.value");
            
            if( companyName != null && companyName != "" )
            { 
                // Set isModalOpen attribute to false
                component.set("v.isModalOpen", false);
                
                // Submit information inputted to direct deposit form generation
                var account = component.get("v.account");
                var contactId = component.get("v.contactId");
                var faId = component.get("v.faId");
                var signature = component.get("v.electronicSig");
                
                /*
                    console.log('companyName: ' + component.find("companyName").get("v.value"));
                    console.log('amount: ' + component.find("amount").get("v.value"));
                    console.log('eid: ' + component.find("employeeId").get("v.value"));
                    */
                
                var eid;
                
                if ( component.find("employeeId").get("v.value") != null )
                {
                    eid = component.find("employeeId").get("v.value");
                }
                
                var info = {};
                
                info["companyName"] = companyName;
                info["employeeId"] = eid;
                info["amountChoice"] = amountChoice;
                if ( component.find("amountVal") != null )
                {
                    info["amountVal"] = component.find("amountVal").get("v.value");
                }
                info["contactId"] = contactId;
                info["acctNum"] = account.accountNumber;
                info["acctType"] = account.accountType; // from DI account info -- will be 'checking' or 'savings'
                info["faId"] = faId;
                if ( signature == 'Yes' )
                {
                    info["signature"] = true;
                }
                else
                {
                    info["signature"] = false;
                }
                //console.log('info[\'amount\'] --- ' + info["amount"]);
                
                var action = component.get('c.generateTicket');
                action.setParams({info:info});
                
                // action.setStorable();
                action.setCallback(this, function(response) {
                    var state, tid;
                    state = response.getState();
                    if (state === 'SUCCESS') {
                        tid = response.getReturnValue();
                        
                        if ( tid !== '' )
                        {
                            window.open('https://' + window.location.href.split('/')[2] + '/advocate/direct_deposit_form?tid='+tid);
                            //window.open('https://dev4-oneunitedbank.cs50.force.com/advocate/direct_deposit_form?tid='+tid);
                        }
                        else
                        {
                            console.log('generateTicket --- something went wrong while processing the request');
                            window.alert('Sorry, something went wrong while processing your request.');
                        }
                    } else {
                        console.error(Object.assign({},response.getError()));
                    }
                });
                $A.enqueueAction(action);
            } else
            {
                if ( amountChoice != '100%' )
                {
                	component.find('amountVal').showHelpMessageIfInvalid();
                }
                component.find('companyName').showHelpMessageIfInvalid();
            }
        }
        catch (error)
        {
            console.error('submit --- error: ' + error);
        }
    },
    
    getExistingForm: function(component, event, helper) {
        try
        {
            // Opens direct deposit form
            helper.getExistingForm(component, event, helper);
        }
        catch ( error )
        {
            console.error('getExistingForm --- error: ' + error);
        }
    },
    
    handleSelect: function(component, event, helper) {
		var selectedMenuItemValue = event.getParam("value");
        
        var buttonMenu = component.find('amountChoice');
        buttonMenu.set('v.label', selectedMenuItemValue);
        // event.getSource().set("v.label", selectedMenuItemLabel);
        
        if ( selectedMenuItemValue == 'Other % of Check' )
        {
            component.set('v.showAmountBox', true);
            component.set('v.setAmountLabel', 'Set % Amount');
        }
        else if ( selectedMenuItemValue == 'Set $ Amount' )
        {
            component.set('v.showAmountBox', true);
            component.set('v.setAmountLabel', 'Set $ Amount');
        }
        else
        {
            component.set('v.showAmountBox', false);
            component.find('amountVal', '100');
        }
    },
    
    sigSelect: function(component, event, helper) {
		var selectedMenuItemValue = event.getParam("value");
        
        var buttonMenu = component.find('elecSig');
        buttonMenu.set('v.label', selectedMenuItemValue);
        // event.getSource().set("v.label", selectedMenuItemLabel);
        
        component.set('v.electronicSig', selectedMenuItemValue);
    }
})