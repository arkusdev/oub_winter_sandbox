({
    getMock: function() {
        var mock = [{
            Id: 'AAxxEG34f0gsdYAsp',
            Activity_Points__c: 1000,
            Type__c: 'Move the Move Badge'
        }, {
            Id: 'AAxxEG34f1gsdYAsp',
            Activity_Points__c: 1000,
            Type__c: '#BankBlack Challenge Badge'
        }, {
            Id: 'AAxxEG34f2gsdYAsp',
            Activity_Points__c: 500,
            Type__c: 'Completed a Playlist'
        }];
        return mock;
    }
})