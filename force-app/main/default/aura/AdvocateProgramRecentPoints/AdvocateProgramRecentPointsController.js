({
	doInit : function(cmp, evt, h) {
        var action = cmp.get("c.getMyPoints");
        action.setParams({recent:true});
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if(state === "SUCCESS") {
                data = response.getReturnValue();
                cmp.set("v.pL", data);
            } else {
                console.log('Problem getting recent point list, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
    },
    navigate: function (cmp, evt, h) {
        var eUrl = $A.get('e.force:navigateToURL');
        eUrl.setParams({
            'url': '/points'
        });
        eUrl.fire();
    },
    openDetail: function(cmp, evt, h) {
        var itemId = evt.currentTarget.dataset.pointid;
        var point = cmp.get('v.pL').find(function(x){ return x.Id === itemId });
        var item = {
            name: point.Name,
            description: point.Description__c,
            value: point.Activity_Points__c
        };

        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})