({  
    toggleChange : function(component, event, helper) {
        try
        {
            var itemId = component.get("v.faId");
            var acctNbr = component.get('v.faAcctnbr'); 
            //console.log('toggleChange --- itemId: ' + itemId);
    
            //var checkCmp = event.getSource();
            //console.log('toggleChange --- checkCmp: ' + checkCmp);
            //var checkVal = event.get("v.checked");
            //console.log('toggleChange --- checkVal = ' + checkVal);
            var checkVal = event.target.checked;
            
            var commsValueX=component.get("v.commsValue");
            //console.log('toggleChange --- commsValueX: ' + commsValueX);
            /*
            var buttonMenu = document.getElementById('buttonMenu-' + itemId);
            
            // Change the toggle display
            var toggleSwitch = document.getElementById('switch-' + itemId);
            if ( checkVal == true ){
                toggleSwitch.classList.remove('active');
                buttonMenu.classList.add('disabled');
            } else {
                toggleSwitch.classList.add('active');
                buttonMenu.classList.remove('disabled');
            }
            */
            
            // toggle the display
            component.set('v.checkValue', checkVal);
            
            // Calls custom controller to create new Payday_Response__c record
            var action = component.get('c.createPaydayResponse');
            action.setParams({faId:itemId, check:checkVal, commsOption:commsValueX,acctNbr:acctNbr});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    //console.log('successfully created payday response');
                } else {
                    console.error(Object.assign({},response.getError()));
                    //console.log('payday response error');
                }
            });
            $A.enqueueAction(action);
            
            // Also calls the toggleEven method to process necessary updates
            var action2 = component.get('c.processToggleEven');
            action2.setParams({faId:itemId});
            action2.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    //console.log('successfully processed toggleEven');
                } else {
                    console.error(Object.assign({},response.getError()));
                    //console.log('toggleEven error');
                }
            });
            $A.enqueueAction(action2);
            
            // Toggles '...process in 24 hours...' message
            var toggleMsg = document.getElementById('toggleMsg-' + itemId);
            //console.log('toggleChange --- toggleMsg: ' + toggleMsg);
            toggleMsg.setAttribute("style", "padding-top: 10px; text-align: left;");
        }
        catch ( error )
        {
            console.error('toggleChange --- error: ' + error);
        }
	},
    
    handleSelect: function (component, event, helper) {
        try
        {
            // This will contain the index (position) of the selected lightning:menuItem
            //var button = event.currentTarget; // currentTarget = button
            var selectedMenuItemValue = event.getParam("value");
            //var itemId = event.getSource().get("v.name").toString().substring(11); // source = buttonMenu
            var itemId = component.get('v.faId'); 
            var acctNbr = component.get('v.faAcctnbr'); 
            //console.log('itemId: ' + itemId);
            //console.log('selectedMenuItem: ' + selectedMenuItemValue);
            
            /*
            var buttonMenu = event.getSource().get("v.name");
            console.log('buttonMenu: ' + buttonMenu);
            console.log('event.getSource(): ' + event.getSource());
            console.log( 'event.getParam: ' + event.detail.menuItem );
            */
    
            // Set the value of the button to reflect the user's choice
            event.getSource().set("v.label", selectedMenuItemValue);
            
            // Calls custom controller to create new Payday_Response__c record
            var action = component.get('c.createPaydayCommsResponse');
            action.setParams({faId:itemId, commsOption:selectedMenuItemValue, acctNbr:acctNbr});
            action.setCallback(this, function(response) {
                var state = response.getState();
                var data;
                if (state === 'SUCCESS') {
                    //console.log('successfully created payday comms response');
                    data = response.getReturnValue();
                    
                    if ( data != null )
                    {
                        // Also calls server-side controller method to process necessary updates
                        var action2 = component.get('c.processComms');
                        action2.setParams({faId:itemId, pdrId:data});
                        action2.setCallback(this, function(response) {
                            var state = response.getState();
                            if (state === 'SUCCESS') {
                                //console.log('successfully processed processComms');
                            } else {
                                console.error(Object.assign({},response.getError()));
                                //console.log('processComms error');
                            }
                        });
                        $A.enqueueAction(action2);
                    }
                    else
                    {
                        //console.log('payday response id returned null');
                    }
                    
                } else {
                    console.error(Object.assign({},response.getError()));
                    //console.log('payday comms response error');
                }
            });
            $A.enqueueAction(action);
            
            // Toggles '...process in 24 hours...' message
            var commsMsg = document.getElementById('toggleMsg-' + itemId);
            //console.log('handleSelect --- commsMsg: ' + commsMsg);
            commsMsg.setAttribute("style", "padding-top: 10px; text-align: left");
        }
        catch (error)
        {
            console.error('handleSelect --- error: ' + error);
        }
    },
})