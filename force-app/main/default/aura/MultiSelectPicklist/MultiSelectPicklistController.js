/**
 * Created by vickalgupta.
 */
({
	doInit : function(component, event, helper) {
		// @TODO: Anything on component load.
	},

    onOptionSelect : function(component, event, helper){
    	var currentOptions = component.get("v.options");
        var selectedOption = component.get("v.selectedOpt");
        var selectedPills = component.get("v.selectedPills");
        var newOptions = [];
        if (selectedPills.length >=5) {
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Only 5 Decline Reasons can be selected. Please remove a Decline Reason to select another one.",
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            for (var i=0; i < currentOptions.length; i++) {
                if (selectedOption != currentOptions[i]) {
                    newOptions.push(currentOptions[i]);
                }
                if ((selectedOption == currentOptions[i]) && selectedPills.indexOf(selectedOption)<0) {
                    selectedPills.push(selectedOption);
                }
            }
            component.set("v.options", newOptions);
            component.set("v.selectedPills", selectedPills);
            if (component.get("v.options").length > 0) {
                component.set("v.defaultSelected", 'Select an option');
                component.set("v.isAllSelected", false);
            } else {
                component.set("v.defaultSelected", 'None');
                component.set("v.isAllSelected", true);
            }
    
            var selectedValuesEvent = component.getEvent("multiSelectEvent");
            selectedValuesEvent.setParams({"selectedValues": selectedPills});
            selectedValuesEvent.fire();
        }
   },

    doRemovePills : function(component, event, helper){
       helper.removePills(component, event);
    }

})