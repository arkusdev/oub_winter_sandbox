({
	doInit : function(cmp, evt, h) {
        var action = cmp.get("c.getMyBadges");
        action.setParams({recent:true});
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if(state === "SUCCESS") {
                data = response.getReturnValue();
                cmp.set("v.bL", data);
            } else {
                console.log('Problem getting recent badge list, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
    },
    navigate: function (cmp, evt, h) {
        var eUrl = $A.get('e.force:navigateToURL');
        eUrl.setParams({
            'url': '/badges'
        });
        eUrl.fire();
    },
    openDetail: function(cmp, evt, h) {
        var itemId = evt.currentTarget.dataset.badgeid;
        var badge = cmp.get('v.bL').find(function(x){ return x.Id === itemId });
        var item = {
            name: badge.Name,
            description: badge.Description__c,
            imgUrl: badge.Image_URL__c,
            badgePoints: Number.parseInt(badge.Badge_Points__c) > 0 ? badge.Badge_Points__c : undefined
        };

        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})