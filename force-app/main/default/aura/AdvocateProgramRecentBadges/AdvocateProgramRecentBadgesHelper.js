({
    getMock: function() {
        var mock = [{
            Id: 'AAxxEG34f0gsdYAsp',
            Advocate_Badge_Name__c: 'Made the Move',
            Advocate_Badge_Image__c: '<img src="/customer/resource/1534431824000/AdvocateProgram/assets/badge1.png" alt=" " border="0"/>'
        }, {
            Id: 'AAxxEG34f1gsdYAsp',
            Advocate_Badge_Name__c: '#BankBlack Challenge',
            Advocate_Badge_Image__c: '<img src="/customer/resource/1534431824000/AdvocateProgram/assets/badge2.png" alt=" " border="0"/>'
        }, {
            Id: 'AAxxEG34f2gsdYAsp',
            Advocate_Badge_Name__c: 'Completed a Playlist',
            Advocate_Badge_Image__c: '<img src="/customer/resource/1534431824000/AdvocateProgram/assets/badge3.png" alt=" " border="0"/>'
        }];
        return mock;
    },
    transformImgSrc: function(data) {
        var div = document.createElement('Div');
        data.forEach(function(x) {
            div.innerHTML = x.Advocate_Badge_Image__c;
            x.Advocate_Badge_Image__c = div.firstChild.src;
        });
    }
})