({
    setOptions: function (cmp) {
        var options = [
            {'label':'All Rewards', 'value':'all'},
            {'label':'My Rewards', 'value':'my'}
        ];
        cmp.set('v.options', options);
    },
    getRewards: function(cmp, filter) {
        var action;
        if (filter === 'all') {
            action = cmp.get('c.getAllRewards');
        } else if (filter === 'my') {
            action = cmp.get('c.getMyRewards');
            action.setParams({recent:false});
        }

        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                cmp.set('v.rewards', data);
            } else {
                
            }
        });
        $A.enqueueAction(action);
	}
})