({
	init: function(cmp, evt, h) {
        h.setOptions(cmp);
		h.getRewards(cmp, 'all');
	},
    filterRewards: function(cmp, evt, h) {
        var filter = evt.getParam('value');
        h.getRewards(cmp, filter);
    },
    openDetail: function(cmp, evt, h) {
        var itemId = evt.currentTarget.dataset.rewardid;
        var reward = cmp.get('v.rewards').find(function(x){ return x.Id === itemId });
        var item = {
            name: reward.Name,
            description: reward.Description__c,
            imgUrl: reward.Image_URL__c
        };

        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})