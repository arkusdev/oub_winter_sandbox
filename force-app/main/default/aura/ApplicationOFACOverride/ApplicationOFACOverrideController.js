({
	doInit : function(component, event, helper) {
		
        // Prepare the action to load account record
        var action = component.get("c.getApplication");
        action.setParams({"appID": component.get("v.recordId")});
        
        // Load application
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.app", response.getReturnValue());
                
                var action2 = component.get("c.getOverrides");
                action2.setParams({"app": component.get("v.app")});
        
                // actionception load overrides
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        component.set("v.ov", response.getReturnValue());
                    } else {
                        console.log('Problem getting overrides, response state: ' + state);
                    }
                });
                
                $A.enqueueAction(action2);
            } else {
                console.log('Problem getting application, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	},
    
	handleAppOverride: function(component, event, helper) {
        // check via the helper if the info is correct
        if(helper.validateOverride(component)) {
             
             // Prepare the action
             var saveAppOverride = component.get("c.runOFACOverride");
             saveAppOverride.setParams({
                 "app": component.get("v.app"),
                 "overrideReason": component.get("v.reason"),
                 "whichApp": "A"
             });
             
			// 	Configure the response handler for the action
 			saveAppOverride.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {         
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Applicant OFAC Overridden",
                        "message": "The applicant's OFAC was overridden."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem overriding application, response state: ' + state);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });
             
             // Send the request to override the application
             $A.enqueueAction(saveAppOverride);
         }
    },
    
	handleCoAppOverride: function(component, event, helper) {
        // check via the helper if the info is correct
        if(helper.validateOverride(component)) {
             
             // Prepare the action
             var saveCoAppOverride = component.get("c.runOFACOverride");
             saveCoAppOverride.setParams({
                 "app": component.get("v.app"),
                 "overrideReason": component.get("v.reason"),
                 "whichApp": "C"
             });
             
			// 	Configure the response handler for the action
 			saveCoAppOverride.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {         
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Applicant OFAC Overridden",
                        "message": "The co-applicant's OFAC was overridden."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem overriding application, response state: ' + state);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });
             
             // Send the request to override the application
             $A.enqueueAction(saveCoAppOverride);
         }
    },
    
	handleBothOverride: function(component, event, helper) {
        // check via the helper if the info is correct
        if(helper.validateOverride(component)) {
             
             // Prepare the action
             var saveBothOverride = component.get("c.runOFACOverride");
             saveBothOverride.setParams({
                 "app": component.get("v.app"),
                 "overrideReason": component.get("v.reason"),
                 "whichApp": "B"
             });
             
			// 	Configure the response handler for the action
 			saveBothOverride.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {         
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Both OFAC Overridden",
                        "message": "The applicant's and co-applicant's OFAC were overridden."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem overriding application, response state: ' + state);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });
             
             // Send the request to override the application
             $A.enqueueAction(saveBothOverride);
         }
    },
    
	handleDecline: function(component, event, helper) {
        // check via the helper if the info is correct
        if(helper.validateDecline(component)) {
             
             // Prepare the action
             var saveDecline = component.get("c.declineApplicationOFAC");
             saveDecline.setParams({
                 "app": component.get("v.app"),
                 "declineReason": component.get("v.reason")
             });
             
			// 	Configure the response handler for the action
 			saveDecline.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {         
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Application Decline",
                        "message": "The application was declined."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem declining application, response state: ' + state);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });
             
             // Send the request to override the application
             $A.enqueueAction(saveDecline);
         }
    }
})