({
    validateOverride: function ( component ) {
        var validData = true ;
        
        var inputCmp = component.find ( "reason" ) ;
        var value = inputCmp.get ( "v.value" ) ;
        
		if($A.util.isEmpty(value)) {
            validData = false ;
            console.log ( "Missing Override reason!" ) ;
        }
    
        return ( validData ) ;
    },
    
    validateDecline: function ( component ) {
        var validData = true ;
        
        var inputCmp = component.find ( "reason" ) ;
        var value = inputCmp.get ( "v.value" ) ;

		if($A.util.isEmpty(value)) {
            validData = false ;
            console.log ( "Missing Decline reason!" ) ;
        }
    
        return ( validData ) ;
    }
})