({
	doInit : function(cmp, evt, h) {
        var action = cmp.get ( "c.getReferralInfo" ) ;

        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if(state === "SUCCESS") {
                data = response.getReturnValue();
                cmp.set("v.code", data) ;
                h.setSocialURLs(cmp, data);
            } else {
                console.log('Problem getting map, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
	}
})