({
	getMock: function() {
        var mock = {
            ReferralCode: 'BB34TX',
            ReferralURL: 'oneunited.com/referral?BB34TX'
        }
		return mock;
	},
    setSocialURLs: function (cmp, xm) {
        var socialUrlFB, socialUrlTw, twFrame, message;
        message = $A.get('$Label.c.SocialMediaPostMessage');
        socialUrlFB = 'https://www.facebook.com/plugins/share_button.php?';
        socialUrlFB += 'href=https://www.'+ xm.ReferralURL;
        socialUrlFB += '&layout=button';
        socialUrlFB += '&size=small';
        socialUrlFB += '&mobile_iframe=true';
        socialUrlFB += '&width=99';
        socialUrlFB += '&height=28';
        socialUrlFB += '&appId';
        cmp.set('v.socialUrlFB', socialUrlFB);

        /*socialUrlTw = 'https://twitter.com/intent/tweet?';
        socialUrlTw += 'text=' + message;
        socialUrlTw += '&url=https://' + xm.ReferralURL;
        cmp.set('v.socialUrlTw', socialUrlTw);*/
        
        twFrame = 'https://platform.twitter.com/widgets/tweet_button.1025be460f33762a866ea882e1687ff4.en.html#dnt=false';
        twFrame += '&id=twitter-widget-1';
        twFrame += '&size=m';
        twFrame += '&text=' + message;
        twFrame += '&type=share';
        twFrame += '&url=https://www.' + xm.ReferralURL;
        cmp.set('v.socialUrlTw', twFrame);
    }
})