({
    init: function (cmp, evt, h) {
        var item = cmp.get('v.item');
        var div = document.createElement('Div');
        div.innerHTML = item.description;
        item.description = div.innerText;
        cmp.set('v.item', item);
    },
    close: function(cmp, evt, h) {
        cmp.find('overlayLib').notifyClose();
    }
})