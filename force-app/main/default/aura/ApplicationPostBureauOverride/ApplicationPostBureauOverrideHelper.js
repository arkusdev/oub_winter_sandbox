({
    validateOverride: function ( component ) {
        var validData = true ;
        
        var inputCmp = component.find ( "reason" ) ;
        var value = inputCmp.get ( "v.value" ) ;

		if($A.util.isEmpty(value)) {
            validData = false ;
            console.log ( "Missing Override reason!" ) ;
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Please enter an Override Reason",
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    
        return ( validData ) ;
    },
    
    validateDecline: function ( component ) {
        var validData = true ;
        
        var selectedDeclineReasons = component.get("v.selectedDeclineReasons");

		if($A.util.isEmpty(selectedDeclineReasons)) {
            validData = false ;
            console.log ( "Missing Decline reason!" ) ;
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Please select up to 5 Decline Reasons",
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }

        return ( validData ) ;
    },

  	processOverride: function(component, event, helper) {
        // Prepare the action
        var saveOverride = component.get("c.overrideApplicationPostBureau");
        var reasonCmp = component.find("reason");
        var reason = reasonCmp.get("v.value");
        var creditLimitCmp = component.find("creditLimit");
        var creditLimit = creditLimitCmp.get("v.value");

        saveOverride.setParams({
            "app": component.get("v.app"),
            "overrideReason": reason,
            "creditLimit" : creditLimit
        });

        // 	Configure the response handler for the action
        saveOverride.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {         
                // Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Application Override",
                    "message": "The application was overridden."
                });
                
                // Update the UI: show toast, refresh application page
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if (state === "ERROR") {
                console.error('Problem overriding application, response state: ' + state);
                // Give an error to the user
                let toastParams = {
                    title: "Error",
                    message: "Error overriding application",
                    type: "error"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
            else {
                console.error('Unknown problem, response state: ' + state);
            }
        });
        
        // Send the request to override the application
        $A.enqueueAction(saveOverride);
    },
    
  	processDecline: function(component, event, helper) {
        // Prepare the action
        var saveDecline = component.get("c.declineApplicationPostBureau");
        var selectedDeclineReasons = component.get("v.selectedDeclineReasons");
        var declineReasonsString = "";
        for (var i=0; i < selectedDeclineReasons.length; i++) {
            declineReasonsString += selectedDeclineReasons[i];
            if (i != selectedDeclineReasons.length - 1) {
                declineReasonsString += ";";
            }
        }

        saveDecline.setParams({
            "app": component.get("v.app"),
            "declineReasons": declineReasonsString
        });
        
        // 	Configure the response handler for the action
        saveDecline.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {         
                // Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Application Decline",
                    "message": "The application was declined."
                });
                
                // Update the UI: show toast, refresh application page
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if (state === "ERROR") {
                console.error('Problem declining application, response state: ' + state);
                // Give an error to the user
                let toastParams = {
                    title: "Error",
                    message: "Error declining application",
                    type: "error"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
            else {
                console.error('Unknown problem, response state: ' + state);
            }
        });
        
        // Send the request to decline the application
        $A.enqueueAction(saveDecline);
    }
})