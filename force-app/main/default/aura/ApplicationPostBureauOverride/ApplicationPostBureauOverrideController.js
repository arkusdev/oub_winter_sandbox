({
	doInit : function(component, event, helper) {
		
        // Prepare the action to load account record
        var action = component.get("c.getApplication");
        action.setParams({"appID": component.get("v.recordId")});
        
        // Load application
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var app = response.getReturnValue();
                component.set("v.app", app);
                component.set("v.creditLimit", app.FIS_Credit_Limit__c) ;
                component.set("v.requestedCreditLimit", app.RequestedCreditLimit__c) ;

                var action2 = component.get("c.isPostBureauOverride");
                action2.setParams({"app": component.get("v.app")});

                // action2 isPostBureauOverride
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        component.set("v.postBureauOverrideAmount", response.getReturnValue());
                        var action3 = component.get("c.getDeclineReasons");
                
                        // action3 getDeclineReasons
                        action3.setCallback(this, function(response) {
                            var state = response.getState();
                            if(state === "SUCCESS") {
                                var result = response.getReturnValue();
                                component.set("v.declineReasons", result);
                            } else {
                                console.error('Problem getting getDeclineReasons, response state: ' + state);
                            }
                        });
                        
                        $A.enqueueAction(action3);
                    } else {
                        console.error('Problem getting isPostBureauOverride, response state: ' + state);
                    }
                });

                $A.enqueueAction(action2);
            } else {
                console.error('Problem getting application, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	},
    
    handleMultiSelectEvent : function(component, event, helper) {
        //Get the Selected values
        var selectedValues = event.getParam("selectedValues");

        //Update the Selected Values  
        component.set("v.selectedDeclineReasons", selectedValues);
    },

    handleSubmit: function(component, event, helper) {
        //  DTI?
        var app = component.get ( "v.app" ) ;
        var overrideWarn = component.get ( "v.overrideWarning" ) ;
        
        // Determine if the user is doing an override or decline
        var actionType = component.get("v.selectedActionType");
        
        // Validate the credit limit first
        var inputCreditLimit = component.get("v.creditLimit");
        var requestedCreditLimit = component.get("v.requestedCreditLimit");
        var maxCreditLimit = component.get("v.postBureauOverrideAmount");
        if (inputCreditLimit > requestedCreditLimit) {
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Credit Limit cannot be higher than the Requested Credit Limit, " + requestedCreditLimit,
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else if ( ( inputCreditLimit == null ) && ( requestedCreditLimit > maxCreditLimit ) )	{
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Credit Limit cannot be higher than your Allowed Credit Limit, " + requestedCreditLimit,
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else if ( inputCreditLimit > maxCreditLimit ) {
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Credit Limit cannot be higher than your Allowed Credit Limit, " + maxCreditLimit,
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
    	} else if (inputCreditLimit < 250) {
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Credit Limit cannot be below $250",
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else if ( ( app.DTI_with_3_Payment__c > 65.0 ) && ( overrideWarn != 2 ) && ( actionType == "Override" ) && ( inputCreditLimit == requestedCreditLimit ) ) { 
            component.set("v.overrideWarning", 1);
        } else {
            // Validate the data for the action type and process it if valid.
            if (actionType == "Override") {
                if (helper.validateOverride(component)) {
                    helper.processOverride(component);
                }
            } else if (actionType == "Decline") {
                if (helper.validateDecline(component)) {
                    helper.processDecline(component);
                }
            } else {
                // Give an error to the user
                let toastParams = {
                    title: "Error",
                    message: "Select an action: Override or Decline",
                    type: "error"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
        }
    },
    
    handleSubmitDTI: function(component, event, helper) {
        component.set("v.overrideWarning", 2);
       	helper.processOverride(component);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "0"  
        component.set("v.overrideWarning", 0);
    },
})