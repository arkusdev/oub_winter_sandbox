({
	init: function(cmp, evt, h) {
		var action = cmp.get('c.getAllLevels');
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                cmp.set('v.levels', data);
            } else {
                
            }
        });
        $A.enqueueAction(action);
	}
})