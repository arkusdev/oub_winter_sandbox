({
	doInit : function(component) {
		// Prepare the action to load account record
        var action = component.get ( "c.getOverallProgress" ) ;

        // Load attachments
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set ( "v.xM", response.getReturnValue() ) ;
            } else {
                console.log('Problem getting map, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	},
	editPhoto : function(component) {
		var modalBody;
        $A.createComponent("c:AdvocateProgramEditPhoto", {},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Edit Photo",
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: "mymodal slds-modal_small cAdvocateProgramEditPhoto",
                                       closeCallback: function() {
                                        $A.get('e.force:refreshView').fire();
                                       }
                                   })
                               }
                           });
	},
	editProfile : function(component, helper) {
		var modalBody;
        $A.createComponent("c:AdvocateProgramEditProfile", {},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Edit Social Media Detalis",
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: "mymodal slds-modal_small cAdvocateProgramEditProfile",
                                       closeCallback: function() {
                                        helper.doInit(component);
                                       }
                                   })
                               }
                           });
	}
})