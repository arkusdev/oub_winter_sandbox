({
	doInit : function(component, event, helper) {
        helper.doInit(component);
    },
    handleEditPhoto : function(component, event, helper) {
        helper.editPhoto(component);
    },
    handleEditProfile : function(component, event, helper) {
        helper.editProfile(component, helper);
    }
})