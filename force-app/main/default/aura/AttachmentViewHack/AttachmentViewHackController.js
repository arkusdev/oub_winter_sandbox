({
	doInit : function(component, event, helper) {
		
        // Prepare the action to load account record
        var action = component.get("c.getAttachmentList");
        action.setParams({"xID": component.get("v.recordId")});
        
        // Load attachments
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.aL1", response.getReturnValue());
                
                var action2 = component.get("c.getContentDocumentList");
		        action2.setParams({"xID": component.get("v.recordId")});
        
                // actionception
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        component.set("v.aL2", response.getReturnValue());
                    } else {
                        console.log('Problem getting content documents, response state: ' + state);
                    }
                });
                
                $A.enqueueAction(action2);
            } else {
                console.log('Problem getting attachment list, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	}
})