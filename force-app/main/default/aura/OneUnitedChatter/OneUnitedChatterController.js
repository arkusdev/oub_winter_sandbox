({
    callServer : function(component, event, helper) {
        var action = component.get("c.getGroupID");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
		        component.set("v.groupID", response.getReturnValue());
            } 
            else {
            	console.log(state);
            }
      	});
    
        $A.enqueueAction(action);
    }
})