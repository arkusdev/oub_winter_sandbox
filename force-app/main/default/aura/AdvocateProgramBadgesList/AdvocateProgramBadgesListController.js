({
	init: function(cmp, evt, h) {
        h.setOptions(cmp);
		h.getBadges(cmp, 'all');
	},
    filterBadges: function(cmp, evt, h) {
        var filter = evt.getParam('value');
        h.getBadges(cmp, filter);
    },
    openDetail: function(cmp, evt, h) {
        var itemId = evt.currentTarget.dataset.badgeid;
        var badge = cmp.get('v.badges').find(function(x){ return x.Id === itemId });
        var item = {
            name: badge.Name,
            description: badge.Description__c,
            imgUrl: badge.Image_URL__c,
            badgePoints: Number.parseInt(badge.Badge_Points__c) > 0 ? badge.Badge_Points__c : undefined
        };

        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})