({
    setOptions: function (cmp) {
        var options = [
            {'label':'All Badges', 'value':'all'},
            {'label':'My Badges', 'value':'my'}
        ];
        cmp.set('v.options', options);
    },
    getBadges: function(cmp, filter) {
        var action;
        if (filter === 'all') {
            action = cmp.get('c.getAllBadges');
        } else if (filter === 'my') {
            action = cmp.get('c.getMyBadges');
            action.setParams({recent:false});
        }

        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                cmp.set('v.badges', data);
            } else {
                console.error(Object.assign({},response.getError()));
            }
        });
        $A.enqueueAction(action);
	}
})