({
    getMock: function() {
        var mock = [{
            Id: 'AAxxEG34f0gsdYAsp',
            Name: 'Bronze Level Advocate',
            Image_URL__c: '/customer/resource/1534431824000/AdvocateProgram/assets/badge1.png'
        }];
        return mock;
    },
    transformImgSrc: function(data) {
        var div = document.createElement('Div');
        data.forEach(function(x) {
            div.innerHTML = x.Advocate_Badge_Image__c;
            x.Advocate_Badge_Image__c = div.firstChild.src;
        });
    }
})