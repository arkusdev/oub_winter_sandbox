({
	doInit : function(cmp, evt, h) {
        var action = cmp.get("c.getMyRewards");
        action.setParams({recent:true});
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if(state === "SUCCESS") {
                data = response.getReturnValue();
                cmp.set("v.rL", data);
            } else {
                console.log('Problem getting recent reward list, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
    },
    navigate: function (cmp, evt, h) {
        var eUrl = $A.get('e.force:navigateToURL');
        eUrl.setParams({
            'url': '/rewards'
        });
        eUrl.fire();
    },
    openDetail: function(cmp, evt, h) {
        var itemId = evt.currentTarget.dataset.rewardid;
        var reward = cmp.get('v.rL').find(function(x){ return x.Id === itemId });
        var item = {
            name: reward.Name,
            description: reward.Description__c,
            imgUrl: reward.Image_URL__c
        };

        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})