({
	getCheckingAccounts : function(component, event, helper) {
        try
        { 
            var action = component.get('c.getAllAcctInfo');
            action.setParams({type:'Checking'});
    
            action.setStorable();
            action.setCallback(this, function(response) {
                var state, data;
                state = response.getState();
                
                if (state === 'SUCCESS') {
                    data = JSON.parse( response.getReturnValue() );
                    component.set('v.checkingAccounts', data);
                    console.log('got checking accounts');
                    
                    //console.log(data);
                    event.stopPropagation();
                    //component.set('v.spinner', false);
                } else {
                    console.error(Object.assign({},response.getError()));
                    console.log('get checking accts - error');
                }
                component.set('v.spinner', false);
            });
            $A.enqueueAction(action);
        }
        catch ( error )
        {
            console.error('getCheckingAccounts --- error: ' + error);
        }
	},
    
    getSavingsAccounts : function(component, event, helper) {
        try
        {
            var action = component.get('c.getAllAcctInfo');
            action.setParams({type:'Savings'});
    
            action.setStorable();
            action.setCallback(this, function(response) {
                var state, data;
                state = response.getState();
                if (state === 'SUCCESS') {
                    data = JSON.parse( response.getReturnValue() );
                    component.set('v.savingsAccounts', data);
                    console.log('got savings accounts');
                    
                    //console.log(data);
                    event.stopPropagation();
                    //component.set('v.spinner', false);
                } else {
                    console.error(Object.assign({},response.getError()));
                    console.log('get savings accts - error');
                }
                component.set('v.spinner', false);
            });
            $A.enqueueAction(action);
        }
        catch ( error )
        {
            console.error('getSavingsAccounts --- error: ' + error);
        }
    },
})