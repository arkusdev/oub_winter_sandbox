({
    init: function(cmp, evt, h) {
        h.setOptions(cmp);
        h.getPoints(cmp, 'all');
    },
    filterPoints: function(cmp, evt, h) {
        var filter = evt.getParam('value');
        h.getPoints(cmp, filter);
    },
    openDetail: function(cmp, evt, h) {
        var itemId = evt.currentTarget.dataset.pointid;
        var point = cmp.get('v.points').find(function(x){ return x.Id === itemId });
        var item = {
            name: point.Name,
            description: point.Description__c,
            value: point.Activity_Points__c
        };

        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})