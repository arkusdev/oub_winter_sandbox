({
    setOptions: function (cmp) {
        var options = [
            {'label':'All Points', 'value':'all'},
            {'label':'My Points', 'value':'my'}
        ];
        cmp.set('v.options', options);
    },
    getPoints: function(cmp, filter) {
        var action;
        if (filter === 'all') {
            action = cmp.get('c.getAllPoints');
        } else if (filter === 'my') {
            action = cmp.get('c.getMyPoints');
            action.setParams({recent:false});
        }

        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                /*data.forEach(function(x) {
                    if (x.Description__c) {
                        var div = document.createElement('Div');
                        div.innerHTML = x.Description__c;
                        x.Description__c = div.firstChild.textContent;
                    }
                })*/
                cmp.set('v.points', data);
            } else {
                
            }
        });
        $A.enqueueAction(action);
    }
})