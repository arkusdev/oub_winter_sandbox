({
	doInit : function(component, event, helper) {
		
        // Prepare the action to load account record
        var action = component.get("c.getApplication");
        action.setParams({"appID": component.get("v.recordId")});
        
        // Load application
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.app", response.getReturnValue());
                
                var action2 = component.get("c.getEmailStates");
                action2.setParams({"app": component.get("v.app")});
        
                // actionception emails
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        component.set("v.ov", response.getReturnValue());
                    } else {
                        console.log('Problem getting email states, response state: ' + state);
                    }
                });
                
                $A.enqueueAction(action2);
            } else {
                console.log('Problem getting application, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	},
    
	handleTrialDepositEmail: function(component, event, helper) {
        
        // Prepare the action
        var sendEmail = component.get("c.sendTrialDepositEmail");
        sendEmail.setParams({
            "app": component.get("v.app")
        });
        
        // 	Configure the response handler for the action
        sendEmail.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                // Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Application Trial Deposit Email Sent",
                    "message": "The applicant has been sent a Trial Deposit Email."
                });
                
                // Update the UI: close panel, show toast, refresh account page
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }
            else if (state === "ERROR") {
                console.log('Problem sending email, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to override the application
        $A.enqueueAction(sendEmail);
    }
})