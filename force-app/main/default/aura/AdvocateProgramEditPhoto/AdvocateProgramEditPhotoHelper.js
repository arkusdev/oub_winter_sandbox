({
	closeModal : function(component) {		
		component.find("overlayLib").notifyClose();
	},
	readFile: function(component, helper, file, upload) {
        if (!file) return;
        if (!file.type.match(/(image.*)/)) {
  			return alert('Image file not supported');
		}
        var reader = new FileReader();
        reader.onloadend = function() {
            var dataURL = reader.result;
            component.set("v.pictureSrc", dataURL);
            if(upload) {
            	helper.upload(component, file, dataURL.match(/,(.*)$/)[1], helper);
            }
            
        };
        reader.readAsDataURL(file);
	},
    
    upload: function(component, file, base64Data, helper) {
    	console.log('uploading');
    	component.set('v.message', 'Uploading photo...');
    	var toastEvent = $A.get("e.force:showToast");
        var action = component.get("c.saveAttachment"); 
        action.setParams({
            base64Data: base64Data, 
            contentType: file.type
        });
	    action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
            	toastEvent.setParams({
			        "title": "Success!",
			        "type": "success",
			        "message": "Your photo was updated!"
			    });
			    toastEvent.fire();
                component.find("overlayLib").notifyClose();
                //$A.get('e.force:refreshView').fire();
            } else if (state == "ERROR") {
            	var errors = action.getError();                
                if (errors[0] && errors[0].message) {                         
                    console.log(errors[0].message);
                    toastEvent.setParams({
				        "title": "Error",
				        "type": "error",
				        "message": "An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues."
				    });
				    toastEvent.fire();
                    component.set('v.message', 'Please select another photo');
                }
            }
        });        
        $A.enqueueAction(action);
    }
})