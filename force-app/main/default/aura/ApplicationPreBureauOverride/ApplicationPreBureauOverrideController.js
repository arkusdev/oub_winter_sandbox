({
	doInit : function(component, event, helper) {
		
        // Prepare the action to load account record
        var action = component.get("c.getApplication");
        action.setParams({"appID": component.get("v.recordId")});
        
        // Load application
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.app", response.getReturnValue());
                
                var action2 = component.get("c.isPreBureauOverride");
                action2.setParams({"app": component.get("v.app")});
        
                // action2 isPreBureauOverride
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        component.set("v.isPreBureauOverride", response.getReturnValue());

                        var action3 = component.get("c.getDeclineReasons");
                
                        // action3 getDeclineReasons
                        action3.setCallback(this, function(response) {
                            var state = response.getState();
                            if(state === "SUCCESS") {
                                var result = response.getReturnValue();
                                component.set("v.declineReasons", result);
                            } else {
                                console.log('Problem getting getDeclineReasons, response state: ' + state);
                            }
                        });
                        
                        $A.enqueueAction(action3);
                    } else {
                        console.log('Problem getting isPreBureauOverride, response state: ' + state);
                    }
                });
                
                $A.enqueueAction(action2);
            } else {
                console.log('Problem getting application, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
	},
    
    handleMultiSelectEvent : function(component, event, helper) {
        //Get the Selected values
        var selectedValues = event.getParam("selectedValues");

        //Update the Selected Values  
        component.set("v.selectedDeclineReasons", selectedValues);
    },

    handleSubmit: function(component, event, helper) {
        // Determine if the user is doing an override or decline
        var actionType = component.get("v.selectedActionType");
        
        // Validate the data for the action type and process it if valid.
        if (actionType == "Override") {
            if (helper.validateOverride(component)) {
                helper.processOverride(component);
            }
        } else if (actionType == "Decline") {
            if (helper.validateDecline(component)) {
                helper.processDecline(component);
            }
        } else {
            // Give an error to the user
            let toastParams = {
                title: "Error",
                message: "Select an action: Override or Decline",
                type: "error"
            };
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        }
    },
})