({
	getMock: function() {
        var mock = {
            Advocate_Level_Image__c: '<img src="/customer/resource/1534431824000/AdvocateProgram/assets/rank_large.png" alt=" " border="0"/>'
        }
		return mock;
	},
    transformImgSrc: function(data) {
        var div = document.createElement('Div');
        div.innerHTML = data.Advocate_Level_Image__c;
        data.Advocate_Level_Image__c = div.firstChild.src;
    }
})