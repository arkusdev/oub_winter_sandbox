({
	doInit : function(cmp, evt, h) {
        var action = cmp.get ( "c.getCurrentLevel" ) ;

        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if(state === "SUCCESS") {
                data = response.getReturnValue();
                cmp.set("v.level", data) ;
            } else {
                console.log('Problem getting map, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
	},
    navigate: function (cmp, evt, h) {
        var eUrl = $A.get('e.force:navigateToURL');
        eUrl.setParams({
            'url': '/levels'
        });
        eUrl.fire();
    },
    showNextLevel: function (cmp, evt, h) {
        var lvl = cmp.get('v.level');
        var item = {
            name: lvl.Next_Level__r.Name,
            description: 'Get ' + lvl.Next_Level__r.Achievement_Points__c + ' Achievement Points.',
            imgUrl: lvl.Next_Level__r.Image_URL__c
        };
        
        var modalBody;
        $A.createComponent('c:AdvocateProgramItemDetailModal',
                           {item: item},
                           function(content, status) {
                               if (status === 'SUCCESS') {
                                   modalBody = content;
                                   cmp.find('overlayLib').showCustomModal({
                                       header: 'Detail',
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: 'cAdvocateProgramItemDetailModal'
                                   });
                               }
                           });
    }
})