trigger WorkshopAttendee on Workshop_Attendee__c (before insert, before update) 
{
	//  Get the recordtype out of the settings object
	String crt = Web_Settings__c.getInstance ( 'Web Forms' ).Workshop_Contact_Record_Type__c ;
	
	//  Load the record type for assignment
	RecordType rt = [ SELECT ID FROM RecordType WHERE Name=:crt AND SobjectType = 'Contact' ] ;
	
	/* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
		/* -----------------------------------------------------------------------------------
		 * LOOP
		 * ----------------------------------------------------------------------------------- */
		for ( Workshop_Attendee__c wa : Trigger.new )
		{
			if ( wa.Contact__c == null )
			{
				System.debug ( 'No existing contact found, querying!' ) ;
				
				List<Contact> cL ;
				
				//  Check for existing contact!
				if ( String.isNotEmpty ( wa.Email_Address__c ) )
					cL = [ SELECT c.id, c.Name, c.Email FROM Contact c WHERE c.Email=:wa.Email_Address__c OR c.WorkEmail__c=:wa.Email_Address__c ORDER BY c.CreatedDate ] ;
				
				if ( ( cL != null ) && ( cL.size () > 0 ) )
				{
					System.debug ( 'Found contact(s), taking the oldest one!' ) ;
					
					//  Take the first one
					Contact c = cL.get ( 0 ) ;
					
					wa.Contact__c = c.Id ;
				}
				else
				{
					System.debug ( 'Create contact!' ) ;
					
					Contact c = new Contact () ;
					c.FirstName = wa.First_Name__c ;
					c.LastName = wa.Last_Name__c ;
					
					if ( String.isNotBlank ( wa.Email_Address__c ) )
						c.Email = wa.Email_Address__c ;
					
					c.Phone = wa.Phone_Number__c ;
					
					c.LeadSource = 'OUB Website Workshop' ;
				
					if ( String.isNotBlank ( rt.ID ) ) 
						c.RecordTypeID = rt.ID ;
					
					insert c ;
					
					wa.Contact__c = c.Id ;
				}
			}
			else
			{
				System.debug ( 'Has contact, doing nothing' ) ;
			}
		}
	}
}