trigger FinancialAccountTrigger on Financial_Account__c ( before insert, after insert, before update, after update ) 
{
	/* -----------------------------------------------------------------------------------
	 * Record Types
	 * ----------------------------------------------------------------------------------- */
	String fDpRecType = FinancialAccountHelper.getDepositRecordTypeID () ;
	String fLnRecType = FinancialAccountHelper.getLoanRecordTypeID ()  ;
	String fCcRecType = FinancialAccountHelper.getCreditCardRecordTypeID ()  ;
    
    //  -----------------------------------------------------------------------------------
    //  Bleah
    //  -----------------------------------------------------------------------------------
    list<Automated_Email__c> aeL = new list<Automated_Email__c> () ;
    list<Advocate_Reward_Achieved__c> araL = new list<Advocate_Reward_Achieved__c> () ;
    list<Contact> cL = new list<Contact> () ;
    list<Financial_Account__c> faL = new list<Financial_Account__c> () ;

    //  -----------------------------------------------------------------------------------
    //  double bleah
    //  -----------------------------------------------------------------------------------
    if ( FinancialAccountHelper.cMap == null )
        FinancialAccountHelper.cMap = new map<String, boolean> () ;
    
    if ( FinancialAccountHelper.awMap == null )
        FinancialAccountHelper.awMap = new map<String, boolean> () ;
    
    /* -----------------------------------------------------------------------------------
	 * INSERT TRIGGER
	 * ----------------------------------------------------------------------------------- */
    if ( Trigger.isInsert )
    {
        /* -----------------------------------------------------------------------------------
         * BEFORE TRIGGER
         * ----------------------------------------------------------------------------------- */
        if ( Trigger.isBefore )
        {
            //  -----------------------------------------------------------------------------------
            //  LOOP
            //  -----------------------------------------------------------------------------------
            for ( Financial_Account__c n : Trigger.new )
            {
                //  -----------------------------------------------------------------------------------
                //  Cleanup
                //  -----------------------------------------------------------------------------------
                if ( n.Days_Overdrawn__c == null )
                    n.Days_Overdrawn__c = 0.0 ;
                
                if ( n.NOTEBAL__c == null )
                    n.NOTEBAL__c = 0.0 ;
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * AFTER TRIGGER
         * ----------------------------------------------------------------------------------- */
        if ( Trigger.isAfter )
        {
            //  -----------------------------------------------------------------------------------
            //  Email Helper setup
            //  -----------------------------------------------------------------------------------
            if ( FinancialAccountHelper.faMap == null )
                FinancialAccountHelper.faMap = new map<String, boolean> () ;
            
            //  -----------------------------------------------------------------------------------
            //  Need contacts
            //  -----------------------------------------------------------------------------------
            set<ID> cSet = new set<ID> () ;
            set<ID> tooMuchMoneyR = new set<ID> () ;
            
            //  -----------------------------------------------------------------------------------
            //  LOOP, Emails
            //  -----------------------------------------------------------------------------------
            for ( Financial_Account__c n : Trigger.new )
            {
                if ( n.TAXRPTFORPERSNBR__c != null )
                    cSet.add ( n.TAXRPTFORPERSNBR__c ) ;
                
                if ( n.Advocate_Referral_Code__c != null )
                    tooMuchMoneyR.add ( n.Advocate_Referral_Code_Contact__c ) ;
            }
            
            //  -----------------------------------------------------------------------------------
            //  Query Contacts
            //  -----------------------------------------------------------------------------------
            map<ID,Contact> cMap = null ;
            
            if ( cSet.size () > 0 )
            {
                try
                {
                    cMap = new map<ID,Contact> ([
                        SELECT ID, FirstName, LastName, Email, Referred_by_Referral_Code__c
                        FROM Contact
                        WHERE ID in :cSet
                        AND Email <> NULL
                    ]) ;
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }
            else
            {
                cMap = new map<ID, Contact> () ;
            }

            //  -----------------------------------------------------------------------------------
            //  Secondary Calc calc
            //  -----------------------------------------------------------------------------------
            map<ID, boolean> tooMuchMoneyMap = new map<ID, boolean> () ; 
            if ( ( tooMuchMoneyR != null ) && ( tooMuchMoneyR.size () > 0 ) )
            {
             	Date wargarhrbl = Date.today () ;
                wargarhrbl.addYears ( -1 ) ;
                
                try
                {
                    list<Contact> cL = [
                        SELECT ID, Rewards_Achieved_Exceeded__c
                        FROM Contact
                        WHERE ID IN :tooMuchMoneyR
                    ] ;
                    
                    set<ID> tooMuchMoneyS = new set<ID> () ;
                    for ( Contact c : cL )
                    {
                        if ( c.Rewards_Achieved_Exceeded__c == true )
                            tooMuchMoneyS.add ( c.ID ) ;
                    }
                    
                    AggregateResult[] arL = [
                        SELECT Contact__c, SUM ( Reward_Value__c )
                        FROM Advocate_Reward_Achieved__c
                        WHERE Contact__c IN :tooMuchMoneyS
                        AND CreatedDate >= :wargarhrbl
                        GROUP BY Contact__c
                    ] ;
                    
                    if ( ( arL != null ) && ( arL.size () > 0 ) ) 
                    {
                        for ( AggregateResult ar : arL )
                        {
                            tooMuchMoneyMap.put ( (ID) ar.get ( 'Contact__c' ), true ) ;
                        }
                    }
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }

            //  -----------------------------------------------------------------------------------
            //  LOOP, Main
            //  -----------------------------------------------------------------------------------
            for ( Financial_Account__c n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  Achievement achieved?!?
                //  --------------------------------------------------------------
                if ( n.Referral_Code_Reward_Criteria__c == true )
                {
                    if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( cMap.containsKey ( n.TAXRPTFORPERSNBR__c ) ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c.Referred_by_Referral_Code__c == null )
                        {
                            Contact xc = new Contact () ;
                            xc.ID = n.TAXRPTFORPERSNBR__c ;
                            xc.Referred_by_Referral_Code__c = n.Advocate_Referral_Code__c ;
                            cL.add ( xc ) ; 
                            
                            if ( ! tooMuchMoneyMap.containsKey ( n.Advocate_Referral_Code_Contact__c ) )
                            {
                                araL.add ( FinancialAccountHelper.getAdvocateReward ( n ) ) ;
                            }
                            else
                            {
                                Financial_Account__c fa = new Financial_Account__c () ;
                                fa.ID = n.ID ;
                                fa.Reward_Limit_Exceeded__c = true ;
                                faL.add ( fa ) ;
                            }
                        }
                    }
                }

                //  --------------------------------------------------------------
                //  Opt In
                //  --------------------------------------------------------------
                if ( ( n.Reg_E_OptIn__c == true ) && ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.CURRMIACCTTYPCD__c != 'ODRP' ) && ( n.Overdraft_Privilege_Status__c == 'A') )
                {
                    Contact c = null ;
                    if ( cMap.containsKey ( n.TAXRPTFORPERSNBR__c ) )
	                    c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getOptInEmail ( n, c ) ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Opt Out
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.CURRMIACCTTYPCD__c != 'ODRP' ) && ( n.Overdraft_Privilege_Status__c == 'C') )
                {
                    Contact c = null ;
                    if ( cMap.containsKey ( n.TAXRPTFORPERSNBR__c ) )
	                    c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getOptOutEmail ( n, c ) ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 10 days
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 10 ) && ( n.Days_Overdrawn__c < 15 ) && ( n.NOTEBAL__c < 0.0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    Contact c = null ;
                    if ( cMap.containsKey ( n.TAXRPTFORPERSNBR__c ) )
	                    c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getAccountOverdrawn10Days ( n, c ) ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 15 days
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 15 ) && ( n.Days_Overdrawn__c < 30 ) && ( n.NOTEBAL__c < 0.0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    Contact c = null ;
                    if ( cMap.containsKey ( n.TAXRPTFORPERSNBR__c ) )
	                    c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getAccountOverdrawn15Days ( n, c ) ) ;
                            }
                        }
                    }
                }

                //  --------------------------------------------------------------
                //  Overdrawn 30 days less 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 30 ) && ( n.Days_Overdrawn__c < 45 ) && ( n.NOTEBAL__c >= -250 ) && ( n.NOTEBAL__c < 0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getAcctOverdrawn30Days ( n, c ) ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 30 days and 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 30 ) && ( n.Days_Overdrawn__c < 45 ) && ( n.NOTEBAL__c <= -250 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getAcctOverdrawn30DaysPlus250 ( n, c ) ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 45 days less 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 45 ) && ( n.NOTEBAL__c >= -250 ) && ( n.NOTEBAL__c < 0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getAcctOverdrawn45Days ( n, c ) ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 45 days and 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 45 ) && ( n.NOTEBAL__c <= -250 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                    
                    if ( c != null )
                    {
                        if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                        {
                            {
                                aeL.add ( FinancialAccountHelper.getAcctOverdrawn45DaysPlus250 ( n, c ) ) ;
                            }
                        }
                    }
                }
            }
        }
    }
    
    /* -----------------------------------------------------------------------------------
	 * UPDATE TRIGGER
	 * ----------------------------------------------------------------------------------- */
    if ( Trigger.isUpdate )
    {
        /* -----------------------------------------------------------------------------------
         * BEFORE TRIGGER
         * ----------------------------------------------------------------------------------- */
        if ( Trigger.isBefore )
        {
            /* -----------------------------------------------------------------------------------
             * LOOP
             * ----------------------------------------------------------------------------------- */
            map<String, Financial_Account__c> idVisa = new map<String, Financial_Account__c> () ;
            map<String, Financial_Account__c> idDep = new map<String, Financial_Account__c> () ;
            map<String, Financial_Account__c> idLoan = new map<String, Financial_Account__c> () ;
            map<String, Financial_Account__c> idDADep = new map<String, Financial_Account__c> () ;
            
            for ( Financial_Account__c n : Trigger.new )
            {
                Date createdDate = n.createdDate.date () ;
                Integer daysSinceCreate = createdDate.daysBetween ( Date.Today() ) ;
                
                //  Unity Visa
                if ( String.isNotBlank ( n.MJACCTTYPCD__c ) )
                {
                    if ( ( n.MJACCTTYPCD__c.equalsIgnoreCase ( 'VISA' ) ) && ( n.Credit_Card_Application__c == NULL ) && ( n.RecordTypeId == fCcRecType ) )
                    {
                        if ( n.TAXRPTFORPERSNBR__c != null )
                            idVisa.put ( n.TAXRPTFORPERSNBR__c, n ) ;
                    }
                    //  Deposits in last 7 days
                    else if ( ( n.RecordTypeId == fDpRecType ) && ( daysSinceCreate < 7 ) )
                    {
                        if ( n.TAXRPTFORPERSNBR__c != null )
                            idDep.put ( n.TAXRPTFORPERSNBR__c, n ) ;
                        
                        //  Deposit Application (Andera) accounts
                        if ( n.ACCTNBR__c != null )
                            idDADep.put ( n.ACCTNBR__c, n ) ;
                    }
                    //  Loans in last 7 days
                    else if ( ( n.RecordTypeId == fLnRecType ) && ( daysSinceCreate < 7 ) )
                    {
                        if ( n.TAXRPTFORPERSNBR__c != null )
                            idLoan.put ( n.TAXRPTFORPERSNBR__c, n ) ;
                    }
                }
            }
        
            /* -----------------------------------------------------------------------------------
             * Link to Credit Card Applications
             * ----------------------------------------------------------------------------------- */
            Set<String> vK = idVisa.keySet () ;
            
            List<one_application__c> oL = null ;
            
            if ( vK.size () > 0 )
            {
                try
                {
                    oL = [
                        SELECT ID, Contact__c, Application_Approved_Date__c
                        FROM one_application__c
                        WHERE application_status__c = 'APPROVED'
                        AND Contact__c IN :vK
                    ] ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'Exception -- ' + e.getMessage () ) ;	
                }
            }
            
            if ( ( oL != null ) && ( oL.size () > 0 ) )
            {
                for ( one_application__c o : oL )
                {
                    Financial_Account__c f = idVisa.get ( o.Contact__c ) ;
                    
                    if ( f != null )
                    {
                        if ( ( o.Application_Approved_Date__c != null ) && ( f.Open_Date__c != null ) )
                        {
                            if ( o.Application_Approved_Date__c.date().daysBetween ( f.Open_Date__c ) <= 7 )
                            {
                                f.Credit_Card_Application__c = o.id ;
                            }
                        }
                    } 
                }
            }
            
            /* -----------------------------------------------------------------------------------
             * Opportunities
             * ----------------------------------------------------------------------------------- */
            list<oneOpportunity__c> ooU = new list<oneOpportunity__c> () ;
            
            /* -----------------------------------------------------------------------------------
             * Link to Deposit Opportunities
             * ----------------------------------------------------------------------------------- */
            String depositOpportunityRecordTypeId = FinancialAccountHelper.getDepositOpportunityRecordTypeId () ;

            Set<String> oDK = idDep.keySet () ;
            
            List<Contact> cDL = null ;
            
            if ( oDK.size () > 0 )
            {
                try
                {
                    cDL = [
                        SELECT ID, Email
                        FROM Contact
                        WHERE ID IN :oDK
                    ] ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'Exception -- ' + e.getMessage () ) ;	
                }
            }
             
            Map<String, Contact> cDM = new Map<String,Contact> () ;
            
            if ( ( cDL != null ) && ( cDL.size () > 0 ) )
            {
                for ( Contact c : cDL )
                {
                    cDM.put ( c.Email, c ) ;
                }
            }
            
            List<oneOpportunity__c> ooDL = FinancialAccountHelper.getOpenDepositOpportunities () ;
            
            if ( ( ooDL != null ) && ( ooDL.size () > 0 ) )
            {
                for ( oneOpportunity__c oo : ooDL )
                {
                    Financial_Account__c f = null ;
                    Contact c = null ;
                    
                    if ( oo.Opportunity_Contact__c != null )
                    {
                        f = idDep.get ( oo.Opportunity_Contact__c ) ;
                    }
                    else if ( ( oo.Email__c != null ) && ( OneUnitedUtilities.isValidEmail ( oo.Email__c ) ) )
                    {
                        c = cDM.get ( oo.Email__c ) ;
                        
                        if ( c != null )
                        {
                            f = idDep.get ( c.Id ) ;
                        }
                    }
                    
                    if ( ( f != null ) && ( oo.MJACCTTYPCD__c == f.MJACCTTYPCD__c ) )
                    {
                        f.Opportunity__c = oo.Id ;
                        
                        oo.Closed_Date__c = Date.today () ;
                        oo.Status__c = 'Account Setup' ;
                        oo.Financial_Account__c = f.id ;
                        
                        if ( c != null )
                            oo.Opportunity_Contact__c = c.Id ;
                        
                        ooU.add ( oo ) ;
                    }
                }
            }
            
            /* -----------------------------------------------------------------------------------
             * Link to Loan Opportunities
             * ----------------------------------------------------------------------------------- */
            String loanOpportunityRecordTypeId = FinancialAccountHelper.getLoanOpportunityRecordTypeId () ;
            Set<String> oLK = idLoan.keySet () ;
            
            List<Contact> cLL = null ;
            
            if ( oLK.size () > 0 )
            {
                try
                {
                    cLL = [
                        SELECT ID, Email
                        FROM Contact
                        WHERE ID IN :oLK
                    ] ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'Exception -- ' + e.getMessage () ) ;	
                }
            }
             
            Map<String, Contact> cLM = new Map<String,Contact> () ;
        
            if ( ( cLL != null ) && ( cLL.size () > 0 ) )
            {
                for ( Contact c : cLL )
                {
                    cLM.put ( c.Email, c ) ;
                }
            }
             
            List<oneOpportunity__c> ooLL = FinancialAccountHelper.getOpenLoanOpportunities () ;
            
            if ( ( ooLL != null ) && ( ooLL.size () > 0 ) )
            {
                for ( oneOpportunity__c oo : ooLL )
                {
                    Financial_Account__c f = null ;
                    Contact c = null ;
                    
                    if ( oo.Opportunity_Contact__c != null )
                    {
                        f = idLoan.get ( oo.Opportunity_Contact__c ) ;
                    }
                    else if ( ( oo.Email__c != null ) && ( OneUnitedUtilities.isValidEmail ( oo.Email__c ) ) )
                    {
                        c = cLM.get ( oo.Email__c ) ;
                        
                        if ( c != null )
                        {
                            f = idLoan.get ( c.Id ) ;
                        }
                    }
                    
                    if ( ( f != null ) && ( oo.MJACCTTYPCD__c == f.MJACCTTYPCD__c ) )
                    {
                        f.Opportunity__c = oo.Id ;
                        
                        oo.Closed_Date__c = Date.today () ;
                        oo.Status__c = 'Account Setup' ;
                        oo.Financial_Account__c = f.id ;
                        
                        if ( c != null )
                            oo.Opportunity_Contact__c = c.Id ;
                        
                        ooU.add ( oo ) ;
                    }
                }
            }
            
            /* -----------------------------------------------------------------------------------
             * Update ALL Opportunities we found
             * ----------------------------------------------------------------------------------- */
            if ( ( ooU != null ) && ( ooU.size () > 0 ) )
            {
                if ( ! System.isQueueable () )
                {
                    FinancialAccountQueue faq = new FinancialAccountQueue ( ooU ) ;
                    System.enqueueJob ( faq ) ;
                }
                else
                {
                    try
                    {
                        update ooU ;
                    }
                    catch ( DMLException e )
                    {
                        // ?
                    }
                }
            }
                
            /* -----------------------------------------------------------------------------------
             * Link to Deposit Application Accounts (Andera)
             * ----------------------------------------------------------------------------------- */
            list<Deposit_Application_Account__c> daaU = new list<Deposit_Application_Account__c> () ;
            
            Set<String> dadK = idDADep.keySet () ;
            
            list<Deposit_Application_Account__c> daaL = null ;
            
            if ( dadK.size () > 0 )
            {
                try
                {
                    daaL = [
                        SELECT ID, Account_Number__c
                        FROM Deposit_Application_Account__c
                        WHERE Account_Number__c IN :dadK
                        AND Financial_Account__c = NULL
                    ] ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'Exception -- ' + e.getMessage () ) ;	
                }
            }
        
            if ( ( daaL != null ) && ( daaL.size () > 0 ) )
            {
                for ( Deposit_Application_Account__c daa : daaL )
                {
                    Financial_Account__c fa = idDADep.get ( daa.Account_Number__c ) ;
                    
                    if ( fa != null )
                    {
                        Deposit_Application_Account__c daaX = new Deposit_Application_Account__c () ;
                        daaX.id = daa.ID ;
                        daaX.Financial_Account__c = fa.ID ;
                        
                        daaU.add ( daaX ) ;
                    } 
                }
            }
            
            if ( ( daaU != null ) && ( daaU.size () > 0 ) )
            {
                if ( ! System.isQueueable () )
                {
                    FinancialAccountQueue faq = new FinancialAccountQueue ( daaU ) ;
                    System.enqueueJob ( faq ) ;
                }
                else
                {
                    try
                    {
                        update daaU ;
                    }
                    catch ( DMLException e )
                    {
                        // ?
                    }
                }
            }
            
            /* -----------------------------------------------------------------------------------
             * Advocate Program ?!?
             * ----------------------------------------------------------------------------------- */
            set<String> apS = new set<String> () ;
            List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> reallyLongNameList = new List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
            
            for ( Financial_Account__c n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Financial_Account__c o = Trigger.oldMap.get ( n.ID ) ;
                
                //  Revert text if it's been blanked out
                if ( ( String.isBlank ( n.Advocate_Referral_Code_Text__c ) ) && ( n.Advocate_Referral_Code__c != null ) && ( String.isNotBlank ( o.Advocate_Referral_Code_Text__c ) ) )
                    n.Advocate_Referral_Code_Text__c = o.Advocate_Referral_Code_Text__c ;
                
                if ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) )
                {
                    n.Advocate_Referral_Code_Text__c = n.Advocate_Referral_Code_Text__c.toUpperCase () ;
                    
                    if ( n.Advocate_Referral_Code__c == NULL )
                    {
                        apS.add ( n.Advocate_Referral_Code_Text__c.toUpperCase () ) ;
                    }
                    
                    if ( ! n.Advocate_Referral_Code_Text__c.equalsIgnoreCase ( o.Advocate_Referral_Code_Text__c ) )
                    {
                        apS.add ( n.Advocate_Referral_Code_Text__c.toUpperCase () ) ;
                    }
                }
                
                //  --------------------------------------------------------------
                //  UNITY Visa?!? - Duplicate of service since service doesn't exist
                //  --------------------------------------------------------------
                if ( n.RecordTypeId == fCcRecType ) 
                {
                    if ( ( String.isNotBlank ( n.CURRACCTSTATCD__c ) ) && ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'ACT' ) ) && ( n.Advocate_UNITY_Visa_Awarded__c == false ) )
                    {
                        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                        reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                        reallyLongName.requestType = 'Badge' ;
                        reallyLongName.requestName = 'Get_the_Card' ;
                        
                        reallyLongNameList.add ( reallyLongName ) ;
                        
                        date openDate = Date.today () ;
                        if ( n.Open_Date__c != null )
                            openDate = n.Open_Date__c ;
                        
                        if ( openDate >= Date.today ().addDays ( -30 ) )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName2 = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName2.ContactID = n.TAXRPTFORPERSNBR__c ;
                            reallyLongName2.requestType = 'Activity' ;
                            reallyLongName2.requestName = 'UNITY_VISA_Secured_Credit_Card' ;
                            
                            reallyLongNameList.add ( reallyLongName2 ) ;
                        }
                        
                        n.Advocate_UNITY_Visa_Awarded__c = true ;
                    }
                }
                
                //  --------------------------------------------------------------
                //  Deposits
                //  --------------------------------------------------------------
                else if ( n.RecordTypeId == fDpRecType ) 
                {
                    if ( ( String.isNotBlank ( n.CURRACCTSTATCD__c ) ) && ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'ACT') ) && ( n.Advocate_UNITY_Visa_Awarded__c == false ) )
                    {
                        date openDate = Date.today () ;
                        if ( n.Open_Date__c != null )
                            openDate = n.Open_Date__c ;
                        
                        if ( openDate >= Date.today ().addDays ( -30 ) )
                        {
                            if ( String.isNotBlank ( n.MJACCTTYPCD__c ) )
                            {
                                if ( n.MJACCTTYPCD__c.equalsIgnoreCase ( 'TD' ) )
                                {
                                    double term = 0.0 ;
                                    if ( n.CURRTERM__c != null )
                                        term = n.CURRTERM__c ;
                                    
                                    if ( term <= 365.0 )
                                    {
                                        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongNameTD = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                        reallyLongNameTD.ContactID = n.TAXRPTFORPERSNBR__c ;
                                        reallyLongNameTD.requestType = 'Activity' ;
                                        reallyLongNameTD.requestName = 'CD_I' ;
                                        
                                        reallyLongNameList.add ( reallyLongNameTD ) ;
                                        n.Advocate_UNITY_Visa_Awarded__c = true ;
                                    }
                                    else
                                    {
                                        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongNameTD = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                        reallyLongNameTD.ContactID = n.TAXRPTFORPERSNBR__c ;
                                        reallyLongNameTD.requestType = 'Activity' ;
                                        reallyLongNameTD.requestName = 'CD_II' ;
                                        
                                        reallyLongNameList.add ( reallyLongNameTD ) ;
                                        n.Advocate_UNITY_Visa_Awarded__c = true ;
                                    }
                                }
                                else
                                {
                                    if ( String.isNotBlank ( n.CURRMIACCTTYPCD__c ) )
                                    {
                                        AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                        reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                        reallyLongName.requestType = 'Activity' ;
                                        reallyLongName.requestName = 'New_Deposit_' + n.CURRMIACCTTYPCD__c ;
                                        
                                        reallyLongNameList.add ( reallyLongName ) ;
                                        n.Advocate_UNITY_Visa_Awarded__c = true ;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            list<Referral_Code__c> rcL = null ;
            
            try
            {
                rcL = [
                    SELECT ID, Name
                    FROM Referral_Code__c
                    WHERE Name IN :apS
                ] ;
            }
            catch ( DMLException e )
            {
                rcL = null ;
            }
            
            if ( ( rcL != null ) && ( rcL.size () > 0 ) )
            {
                map<String,ID> rcM = new map<String,ID> () ;
                
                for ( Referral_Code__c rc : rcL )
                    rcM.put ( rc.Name, rc.ID ) ;
                    
                for ( Financial_Account__c n : Trigger.new )
                {
                    //  Prevent empty referral code if new is blank
//                    if ( String.isBlank ( n.Advocate_Referral_Code_Text__c ) )
//                        n.Advocate_Referral_Code__c = null ;
                
                    //  Place in thingy
                    if ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) && ( rcM.containsKey ( n.Advocate_Referral_Code_Text__c ) ) )
                        n.Advocate_Referral_Code__c = rcM.get ( n.Advocate_Referral_Code_Text__c ) ;
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Add advocacy
            //  -----------------------------------------------------------------------------------
            if ( ( reallyLongNameList != null ) && ( reallyLongNameList.size () > 0 ) )
                AdvocateProgramAwardInvocationFunction.advocateProgramAward ( reallyLongNameList ) ;
        }
        
        /* -----------------------------------------------------------------------------------
         * AFTER TRIGGER
         * ----------------------------------------------------------------------------------- */
        if ( Trigger.isAfter )
        {
            //  -----------------------------------------------------------------------------------
            //  Email Helper setup
            //  -----------------------------------------------------------------------------------
            if ( FinancialAccountHelper.faMap == null )
                FinancialAccountHelper.faMap = new map<String, boolean> () ;
            
            //  -----------------------------------------------------------------------------------
            //  Need contacts
            //  -----------------------------------------------------------------------------------
            set<ID> cSet = new set<ID> () ;
            set<ID> tooMuchMoneyR = new set<ID> () ;
            
            //  -----------------------------------------------------------------------------------
            //  map o application stuff
            //  -----------------------------------------------------------------------------------
            map<ID,one_application__c> oMap = new map<ID,one_Application__c> () ;
            
            //  -----------------------------------------------------------------------------------
            //  LOOP, Update associated applications
            //  -----------------------------------------------------------------------------------
            for ( Financial_Account__c n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  Contacts
                //  --------------------------------------------------------------
                if ( n.TAXRPTFORPERSNBR__c != null )
                    cSet.add ( n.TAXRPTFORPERSNBR__c ) ;
                
                if ( n.Advocate_Referral_Code__c != null )
                    tooMuchMoneyR.add ( n.Advocate_Referral_Code_Contact__c ) ;

                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Financial_Account__c o = Trigger.oldMap.get ( n.ID ) ;
                
                //  --------------------------------------------------------------
                //  Deposit accounts
                //  --------------------------------------------------------------
                if ( ( n.RecordTypeId == fDpRecType ) && ( n.Credit_Card_Application__c != null ) )
                {
                    if ( ( o.CURRACCTSTATCD__c == null ) || ( ( o.CURRACCTSTATCD__c != null ) && ( o.CURRACCTSTATCD__c == 'APPR' ) ) )
                    {
                        if ( ( n.CURRACCTSTATCD__c != null ) && ( n.CURRACCTSTATCD__c == 'ACT' ) )
                        {
                            if ( ( n.NOTEBAL__c != null ) && ( n.NOTEBAL__c > 0.0 ) )
                            {
                                one_application__c app = new one_application__c () ;
                                app.ID = n.Credit_Card_Application__c ;
                                app.FIS_Decision_Code__c = 'A001' ;
                               
                                if ( ! oMap.containsKey ( app.ID ) )
                                    oMap.put ( app.ID, app ) ;
                            }
                        }
                    }
                }
            }
            
            list<one_application__c> oL = new list<one_Application__c> () ;
            
            for ( one_application__c o: oMap.values () ) 
                oL.add ( o ) ;
            
            if ( ( oL != null ) && ( oL.size () > 0 ) )
                update oL ;
            
            /* -----------------------------------------------------------------------------------
             * Advocate Program ?!?
             * ----------------------------------------------------------------------------------- */
            List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> reallyLongNameList = new List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
            list<Referral_Code_Financial_Account__c> rcfaL = new list<Referral_Code_Financial_Account__c> () ;
            
            //  -----------------------------------------------------------------------------------
            //  Secondary Calc calc
            //  -----------------------------------------------------------------------------------
            map<ID, boolean> tooMuchMoneyMap = new map<ID, boolean> () ; 
            if ( ( tooMuchMoneyR != null ) && ( tooMuchMoneyR.size () > 0 ) )
            {
             	Date wargarhrbl = Date.today () ;
                wargarhrbl.addYears ( -1 ) ;
                
                try
                {
                    list<Contact> cL = [
                        SELECT ID, Rewards_Achieved_Exceeded__c
                        FROM Contact
                        WHERE ID IN :tooMuchMoneyR
                    ] ;
                    
                    set<ID> tooMuchMoneyS = new set<ID> () ;
                    for ( Contact c : cL )
                    {
                        if ( c.Rewards_Achieved_Exceeded__c == true )
                            tooMuchMoneyS.add ( c.ID ) ;
                    }
                    
                    AggregateResult[] arL = [
                        SELECT Contact__c, SUM ( Reward_Value__c )
                        FROM Advocate_Reward_Achieved__c
                        WHERE Contact__c IN :tooMuchMoneyS
                        AND CreatedDate >= :wargarhrbl
                        GROUP BY Contact__c
                    ] ;
                    
                    if ( ( arL != null ) && ( arL.size () > 0 ) ) 
                    {
                        for ( AggregateResult ar : arL )
                        {
                            tooMuchMoneyMap.put ( (ID) ar.get ( 'Contact__c' ), true ) ;
                        }
                    }
                }
                catch ( DMLException e )
                {
                    // ?
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  LOOP, Advocate
            //  -----------------------------------------------------------------------------------
            for ( Financial_Account__c n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Financial_Account__c o = Trigger.oldMap.get ( n.ID ) ;
                
                //  --------------------------------------------------------------
                //  Referral codes
                //  --------------------------------------------------------------
                if ( String.isBlank ( o.Advocate_Referral_Code_Text__c ) && ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) ) )
                {
                    if ( n.Advocate_Referral_Code__c != null )
                    {
                        Referral_Code_Financial_Account__c rcfa = new Referral_Code_Financial_Account__c () ;
                        rcfa.Financial_Account__c = n.ID ;
                        rcfa.Advocate_Referral_Code__c = n.Advocate_Referral_Code__c ;
                        
                        rcfaL.add ( rcfa ) ;
                    }
                }
                else if ( ( String.isNotBlank ( o.Advocate_Referral_Code_Text__c ) ) && 
                          ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) ) &&
                          ( ! o.Advocate_Referral_Code_Text__c.equalsIgnoreCase ( n.Advocate_Referral_Code_Text__c ) ) )
                {
                    if ( n.Advocate_Referral_Code__c != null )
                    {
                        Referral_Code_Financial_Account__c rcfa = new Referral_Code_Financial_Account__c () ;
                        rcfa.Financial_Account__c = n.ID ;
                        rcfa.Advocate_Referral_Code__c = n.Advocate_Referral_Code__c ;
                        
                        rcfaL.add ( rcfa ) ;
                    }
                }
                
                //  --------------------------------------------------------------
                //  THINGY, Balance(s)
                //  --------------------------------------------------------------
                if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == fDpRecType ) )
                {
                    if ( ( String.isNotBlank ( n.CURRACCTSTATCD__c ) ) && ( ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'ACT' ) ) || ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'IACT' ) ) || ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'DORM' ) ) ) )
                    {
	                    if ( ( String.isNotBlank ( n.MJACCTTYPCD__c ) ) && ( ! n.MJACCTTYPCD__c.equalsIgnoreCase ( 'TD' ) ) )
                        {
                            //  --------------------------------------------------------------
                            //  AVERAGE
                            //  --------------------------------------------------------------
                            double currentAvg = 0.0 ;
                            if ( n.PRIOR_NOTEMTDAVGBAL__c != null )
                                currentAvg = n.PRIOR_NOTEMTDAVGBAL__c ;
                            
                            double priorAvg = 0.0 ;
                            if ( o.PRIOR_NOTEMTDAVGBAL__c != null )
                                priorAvg = o.PRIOR_NOTEMTDAVGBAL__c ;
                            
                            if ( ( currentAvg >= 1000.0 ) && ( currentAvg != priorAvg ) )
                            {
                                if ( n.TAXRPTFORPERSNBR__c != null )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Activity' ;
                                    reallyLongName.requestName = 'Deposit' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                            }
                        }
                        
                        if ( String.isNotBlank ( n.MJACCTTYPCD__c ) )
                        {
                            //  --------------------------------------------------------------
                            //  Note balance
                            //  --------------------------------------------------------------
                            double currentDay = 0.0 ;
                            if ( n.NOTEBAL__c != null )
                                currentDay = n.NOTEBAL__c ;
                            
                            double priorDay = 0.0 ;
                            if ( ( o.NOTEBAL__c != null ) && ( Date.today () > date.newInstance ( 2018, 10, 10 ) ) )
                                priorDay = o.NOTEBAL__c ;
                            
                            if ( n.MJACCTTYPCD__c.equalsIgnoreCase ( 'CK' ) )
                            {
                                if ( ( priorDay < 100.0 ) && ( currentDay >= 100.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'CK_Rookie' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                                
                                if ( ( priorDay < 1000.0 ) && ( currentDay >= 1000.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'CK_Vet' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                                
                                if ( ( priorDay < 5000.0 ) && ( currentDay >= 5000.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'CK_Boss' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                            }
                            else if ( n.MJACCTTYPCD__c.equalsIgnoreCase ( 'SAV' ) )
                            {
                                if ( ( priorDay < 100.0 ) && ( currentDay >= 100.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'SAV_Rookie' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }

                                if ( ( priorDay < 1000.0 ) && ( currentDay >= 1000.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'SAV_Vet' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                                
                                if ( ( priorDay < 5000.0 ) && ( currentDay >= 5000.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'SAV_Boss' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                            }
                            else if ( n.MJACCTTYPCD__c.equalsIgnoreCase ( 'TD' ) )
                            {
                                if ( ( priorDay < 500.0 ) && ( currentDay >= 500.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'CD_Rookie' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                                
                                if ( ( priorDay < 25000.0 ) && ( currentDay >= 25000.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'CD_Vet' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                                
                                if ( ( priorDay < 100000.0 ) && ( currentDay >= 100000.0 ) )
                                {
                                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                    reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                    reallyLongName.requestType = 'Badge' ;
                                    reallyLongName.requestName = 'CD_Boss' ;
                                    
                                    reallyLongNameList.add ( reallyLongName ) ;
                                }
                            }
                        }
                    }
                }
 
                //  --------------------------------------------------------------
                //  THINGY, SAVE(s)
                //  --------------------------------------------------------------
                if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == fDpRecType ) )
                {
                    if ( ( String.isNotBlank ( n.CURRACCTSTATCD__c ) ) && ( ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'ACT' ) ) || ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'IACT' ) ) || ( n.CURRACCTSTATCD__c.equalsIgnoreCase ( 'DORM' ) ) ) )
                    {
                        if ( ( String.isNotBlank ( n.MJACCTTYPCD__c ) ) && ( n.MJACCTTYPCD__c.equalsIgnoreCase ( 'SAV' ) ) )
                        {
                            if ( ( ! o.Save_for_Life__c ) && ( n.Save_for_Life__c ) )
                            {
                                AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                reallyLongName.requestType = 'Badge' ;
                                reallyLongName.requestName = 'S4L_-_Save_For_Life' ;
                                
                                reallyLongNameList.add ( reallyLongName ) ;
                            }
                            
                            if ( ( String.isBlank ( o.Save_for_Life_Month__c ) ) && ( String.isNotBlank ( n.Save_for_Life_Month__c ) ) )
                            {
                                AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                reallyLongName.requestType = 'Activity' ;
                                reallyLongName.requestName = 'Direct_Savings' ;
                                
                                reallyLongNameList.add ( reallyLongName ) ;
                            }
                            else if ( ( String.isNotBlank ( o.Save_for_Life_Month__c ) ) && 
                                 ( String.isNotBlank ( n.Save_for_Life_Month__c ) ) && 
                                 ( ! n.Save_for_Life_Month__c.equalsIgnoreCase ( o.Save_for_Life_Month__c ) ) )
                            {
                                AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                                reallyLongName.ContactID = n.TAXRPTFORPERSNBR__c ;
                                reallyLongName.requestType = 'Activity' ;
                                reallyLongName.requestName = 'Direct_Savings' ;
                                
                                reallyLongNameList.add ( reallyLongName ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  END LOOP
                //  --------------------------------------------------------------
            }
            
            //  -----------------------------------------------------------------------------------
            //  Add referrals
            //  -----------------------------------------------------------------------------------
            if ( ( rcfaL != null ) && ( rcfaL.size () > 0 ) )
                insert rcfaL ;
            
            //  -----------------------------------------------------------------------------------
            //  Add advocacy
            //  -----------------------------------------------------------------------------------
            if ( ( reallyLongNameList != null ) && ( reallyLongNameList.size () > 0 ) )
                AdvocateProgramAwardInvocationFunction.advocateProgramAward ( reallyLongNameList ) ;
            
            //  -----------------------------------------------------------------------------------
            //  Query Contacts
            //  -----------------------------------------------------------------------------------
            map<ID,Contact> cMap = null ;
            
            try
            {
				cMap = new map<ID,Contact> ([
                    SELECT ID, FirstName, LastName, Email, Referred_by_Referral_Code__c
					FROM Contact
                    WHERE ID in :cSet
                ]) ;
            }
            catch ( DMLException e )
            {
                // ?
            }

            //  -----------------------------------------------------------------------------------
            //  LOOP, Emails/Achivement
            //  -----------------------------------------------------------------------------------
            for ( Financial_Account__c n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Financial_Account__c o = Trigger.oldMap.get ( n.ID ) ;
                
                //  --------------------------------------------------------------
                //  Achievement achieved?!?
                //  --------------------------------------------------------------
                if ( ( n.Referral_Code_Reward_Criteria__c == true ) && ( o.Referral_Code_Reward_Criteria__c == false ) )
                {
                    if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( cMap.containsKey ( n.TAXRPTFORPERSNBR__c ) ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c.Referred_by_Referral_Code__c == null )
                        {
                            Contact xc = new Contact () ;
                            xc.ID = n.TAXRPTFORPERSNBR__c ;
                            xc.Referred_by_Referral_Code__c = n.Advocate_Referral_Code__c ;
                            cL.add ( xc ) ; 
                            
                            if ( ! tooMuchMoneyMap.containsKey ( n.Advocate_Referral_Code_Contact__c ) )
                            {
                                araL.add ( FinancialAccountHelper.getAdvocateReward ( n ) ) ;
                            }
                            else
                            {
                                Financial_Account__c fa = new Financial_Account__c () ;
                                fa.ID = n.ID ;
                                fa.Reward_Limit_Exceeded__c = true ;
                                faL.add ( fa ) ;
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Opt In
                //  --------------------------------------------------------------
                if ( ( n.Reg_E_OptIn__c == true ) && ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.CURRMIACCTTYPCD__c != 'ODRP') && ( n.Overdraft_Privilege_Status__c == 'A') )
                {
                    if ( ( o.Reg_E_OptIn__c != true ) || ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Overdraft_Privilege_Status__c != 'A') )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getOptInEmail ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Opt Out
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.CURRMIACCTTYPCD__c != 'ODRP' ) && ( n.Overdraft_Privilege_Status__c == 'C') )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.CURRMIACCTTYPCD__c == 'ODRP' ) || ( o.Overdraft_Privilege_Status__c != 'C') )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getOptOutEmail ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 10 days
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 10 ) && ( n.Days_Overdrawn__c < 15 ) && ( n.NOTEBAL__c < 0.0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Days_Overdrawn__c < 10 ) || ( o.Days_Overdrawn__c >= 15 ) || ( n.NOTEBAL__c == 0.0 ) || ( o.Forecast_Auto_Close_Date__c == null ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getAccountOverdrawn10Days ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 15 days
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 15 ) && ( n.Days_Overdrawn__c < 30 ) && ( n.NOTEBAL__c < 0.0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Days_Overdrawn__c < 15 ) || ( o.Days_Overdrawn__c >= 30 ) || ( n.NOTEBAL__c == 0.0 ) || ( o.Forecast_Auto_Close_Date__c == null ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getAccountOverdrawn15Days ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 30 days less 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 30 ) && ( n.Days_Overdrawn__c < 45 ) && ( n.NOTEBAL__c >= -250 ) && ( n.NOTEBAL__c < 0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Days_Overdrawn__c < 30 ) || ( o.Days_Overdrawn__c >= 45 ) || ( o.NOTEBAL__c < -250 ) || ( o.NOTEBAL__c > 0 ) || ( n.Forecast_Auto_Close_Date__c == null ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getAcctOverdrawn30Days ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 30 days and 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 30 ) && ( n.Days_Overdrawn__c < 45 ) && ( n.NOTEBAL__c <= -250 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Days_Overdrawn__c < 30 ) || ( o.Days_Overdrawn__c >= 45 ) || ( o.NOTEBAL__c > -250 ) || ( n.Forecast_Auto_Close_Date__c == null ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getAcctOverdrawn30DaysPlus250 ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 45 days less 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 45 ) && ( n.NOTEBAL__c >= -250 ) && ( n.NOTEBAL__c < 0 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Days_Overdrawn__c < 45 ) || ( o.NOTEBAL__c < -250 ) || ( o.NOTEBAL__c > 0 ) || ( n.Forecast_Auto_Close_Date__c == null ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getAcctOverdrawn45Days ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
                
                //  --------------------------------------------------------------
                //  Overdrawn 45 days and 250
                //  --------------------------------------------------------------
                if ( ( n.TAXRPTFORPERSNBR__c != null ) && ( n.MJACCTTYPCD__c == 'CK' ) && ( n.Days_Overdrawn__c >= 45 ) && ( n.NOTEBAL__c <= -250 ) && ( n.Forecast_Auto_Close_Date__c != null ) )
                {
                    if ( ( o.TAXRPTFORPERSNBR__c == null ) || ( o.MJACCTTYPCD__c != 'CK' ) || ( o.Days_Overdrawn__c < 45 ) || ( o.NOTEBAL__c > -250 ) || ( n.Forecast_Auto_Close_Date__c == null ) )
                    {
                        Contact c = cMap.get ( n.TAXRPTFORPERSNBR__c ) ;
                        
                        if ( c != null )
                        {
                            if ( ! FinancialAccountHelper.checkDuplicate ( FinancialAccountHelper.faMap, n.Id ) )
                            {
                                {
                                    aeL.add ( FinancialAccountHelper.getAcctOverdrawn45DaysPlus250 ( n, c ) ) ;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //  -----------------------------------------------------------------------------------
    //  Bleah Bleah
    //  -----------------------------------------------------------------------------------
    if ( ( aeL != null ) && ( aeL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            FinancialAccountQueue faq = new FinancialAccountQueue ( aeL ) ;
            System.enqueueJob ( faq ) ;
        }
        else
        {
            try
            {
                insert aeL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
    
    if ( ( araL != null ) && ( araL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            FinancialAccountQueue faq = new FinancialAccountQueue ( araL ) ;
            System.enqueueJob ( faq ) ;
        }
        else
        {
            try
            {
                insert araL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
    
    if ( ( cL != null ) && ( cL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            FinancialAccountQueue faq = new FinancialAccountQueue ( cL ) ;
            System.enqueueJob ( faq ) ;
        }
        else
        {
            try
            {
                update cL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
    
    if ( ( faL != null ) && ( faL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            FinancialAccountQueue faq = new FinancialAccountQueue ( faL ) ;
            System.enqueueJob ( faq ) ;
        }
        else
        {
            try
            {
                update faL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }

}