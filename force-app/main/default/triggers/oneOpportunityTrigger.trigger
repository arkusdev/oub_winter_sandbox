trigger oneOpportunityTrigger on oneOpportunity__c (after insert, after update, before insert, before update) 
{
    /* -----------------------------------------------------------------------------------
     * HELPER
     * ----------------------------------------------------------------------------------- */
    String depositTypeID = oneOpportunityHelper.getDepositTypeID () ;
    String loanTypeID = oneOpportunityHelper.getLoanTypeID () ;
    Map<String, String> gMap = oneOpportunityHelper.getGroupMap () ;
    Map<ID, User> uMap = oneOpportunityHelper.getUserMap () ;
    Map<String,Branch__c> bNameMap = oneOpportunityHelper.getBranchNameMap () ;
    Map<String,Branch__c> bIDMap = oneOpportunityHelper.getBranchIDMap () ;
    
    //  Check for loan application IDs
    map<String,boolean> laMap = new map<String,boolean> () ;
                
    /* -----------------------------------------------------------------------------------
     * BEFORE TRIGGER
     * ----------------------------------------------------------------------------------- */
    if ( Trigger.isBefore )
    {
        System.debug ( '---------- BEFORE ----------' ) ;
        
        /* -----------------------------------------------------------------------------------
         * SETUP
         * ----------------------------------------------------------------------------------- */
        List<String> cIdState = new List<String> () ;
        List<String> aIdState = new List<String> () ;
        
        for ( oneOpportunity__c n : Trigger.new )
        {
            if ( Trigger.isInsert )
            {
                if ( ( String.isEmpty ( n.State__c ) ) && ( n.Opportunity_Contact__c != null ) )
                {
                    cIdState.add ( n.Opportunity_Contact__c ) ;
                }
                
                if ( ( String.isEmpty ( n.State__c ) ) && ( n.Relationship__c != null ) )
                {
                    aIdState.add ( n.Relationship__c ) ;
                }
            }
        }
        
        //  Load the contacts into a list & reorganize
        List<Contact> cStateList = null ;
        Map<String,String> cStateMap = new Map<String,String> () ;
        
        if ( cIdState.size () > 0 )
        {
			cStateList = [
	            SELECT c.Id, c.MailingState 
	            FROM Contact c 
	            WHERE c.ID IN :cIdState
	        ] ;
	        
	        for ( Contact c : cStateList )
	            cStateMap.put ( c.Id, c.MailingState ) ;    
        }
        
        //  Load the accounts into a list & reorganize
        List<Account> aStateList = null ;
        Map<String,String> aStateMap = new Map<String,String> () ;
        
        if ( aIdState.size () > 0 )
        {
	        aStateList = [
	            SELECT a.Id, a.BillingState 
	            FROM account a 
	            WHERE a.ID IN :aIdState
	        ] ;
	        
	        for ( Account a : aStateList )
	            aStateMap.put ( a.Id, a.BillingState ) ;    
        }
        
        /* -----------------------------------------------------------------------------------
         * LOOP, LOADING ARGH!
         * ----------------------------------------------------------------------------------- */
    	List<String> cList = new List<String> () ;
    	List<String> aList = new List<String> () ;
    	
        for ( oneOpportunity__c n : Trigger.new )
        {
        	if ( n.Opportunity_Contact__c != null )
        		cList.add ( n.Opportunity_Contact__c ) ;
        		
        	if ( n.Relationship__c != null )
        		aList.add ( n.Relationship__c ) ;
        }
        
        Map<String,Contact> cMap = null ;
        if ( cList.size () > 0 )
        {
	        System.debug ( 'Selecting ' + cList.size () + ' contacts ?!?' ) ;
	        
	        cMap = new Map<String,Contact> ([
	        	SELECT ID, Branch__c
	        	FROM Contact
	        	WHERE ID IN :cList
	        ]) ;
        }

        Map<String,Account> aMap = null ;
        if ( aList.size () > 0 )
        {
	        System.debug ( 'Selecting ' + aList.size () + ' accounts ?!?' ) ;
	        
	        aMap = new Map<String,Account> ([
	        	SELECT ID, Branch__c
	        	FROM Account
	        	WHERE ID IN :aList
	        ]) ;
        }
         
        /* -----------------------------------------------------------------------------------
         * LOOP
         * ----------------------------------------------------------------------------------- */
        for ( oneOpportunity__c n : Trigger.new )
        {
            //  ------------------------------------------------------------------------
            //  On Insert
            //  ------------------------------------------------------------------------
            if ( Trigger.isInsert )
            {
            	System.debug ( '---------- BEFORE INSERT ----------' ) ;
                if ( ( String.isEmpty ( n.State__c ) ) && ( n.Opportunity_Contact__c != null ) )
                {
                    String state = cStateMap.get ( n.Opportunity_Contact__c ) ;
                    
                    if ( state != null )
                    {
                        n.State__c = state ;
                    }
                }
                
                if ( ( String.isEmpty ( n.State__c ) ) && ( n.Relationship__c != null ) )
                {
                    String state = aStateMap.get ( n.Relationship__c ) ;
                    
                    if ( state != null )
                    {
                        n.State__c = state ;
                    }
                }
                
                /*
                 * If assigned is blank, set to owner.
                 * If assigned is NOT blank and the group is a person, set the owner to it.
                 */
                String xOwnerId = n.OwnerId ;
                
                if ( ( n.RecordTypeId == depositTypeId ) || ( n.RecordTypeId == loanTypeId ) )
                {
                    // ----
                    // LOANS
                    // ----
                    if ( ( n.RecordTypeId == loanTypeId ) && ( n.Assigned_To__c != null ) )
                    {
                        User u = uMap.get ( n.Assigned_To__c ) ;
                        
                        //  Can't assign a loan to a non-NMLO
                        if ( u.Is_NMLO__c != true )
                        {
                            n.Assigned_To__c = null ;
                        }
                    }
                    
                    if ( ( n.RecordTypeId == loanTypeId ) && ( n.Assigned_To__c == null ) )
                    {
                        System.debug ( '-- Loan Opportunity, no assigned to' ) ;
                        
                        User u = uMap.get ( n.OwnerId ) ;
                        
                        if ( ( u.Loan_Opportunity_Default_Assignment__c != null ) && 
                             ( n.Interested_in_Product__c.equalsIgnoreCase ( 'Single Family Home' ) ) )
                        {
                            System.debug ( '-- Assigning to default' ) ;
                            n.Assigned_To__c = u.Loan_Opportunity_Default_Assignment__c ;
                        }
                        else
                        {
                            if (  String.isNotBlank ( n.Interested_in_Product__c ) )
                            {
                                if ( n.Interested_in_Product__c.equalsIgnoreCase ( 'Single Family Home' ) )
                                {
                                    System.debug ( '-- 1 Single family product, assigning there' ) ;
                                    n.OwnerId = gMap.get ( 'Single Family Loan' ) ;
                                    n.Assigned_To__c = null ;
                                }
                                else if ( ( n.Interested_in_Product__c.equalsIgnoreCase ( 'Multifamily' ) ) || 
                                          ( n.Interested_in_Product__c.equalsIgnoreCase ( 'Commercial Real Estate' ) ) )
                                {
                                    System.debug ( '-- 1 Multifamily product, assigning there' ) ;
                                    n.OwnerId = gMap.get ( 'MultiFamily Loan' ) ;
                                    n.Assigned_To__c = null ;
                                }
                                else if ( n.Interested_in_Product__c.equalsIgnoreCase ( 'Not Specified' ) ) 
                                {
                                    if ( String.isNotBlank ( n.Property_has_more_than_4_units__c ) ) 
                                    {
                                        if ( n.Property_has_more_than_4_units__c.equalsIgnoreCase ( 'No' ) )
                                        {
                                            System.debug ( '-- 2 Single family product, assigning there' ) ;
                                            n.OwnerId = gMap.get ( 'Single Family Loan' ) ;
                                            n.Assigned_To__c = null ;
                                        }
                                        else if ( n.Property_has_more_than_4_units__c.equalsIgnoreCase ( 'Yes' ) )
                                        {
                                            System.debug ( '-- 2 Multifamily product, assigning there' ) ;
                                            n.OwnerId = gMap.get ( 'MultiFamily Loan' ) ;
                                            n.Assigned_To__c = null ;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    // ----
                    // DEPOSIT
                    // ----
                    else if ( ( n.RecordTypeId == depositTypeId ) && ( n.Assigned_To__c == null ) )
                    {
                        System.debug ( '-- Deposit Opportunity, no assigned to' ) ;
                        
                        User u = uMap.get ( xOwnerId ) ;
                        
                        if ( ( u != null ) && ( u.Department != null ) && ( u.Department.contains ( 'Retail' ) ) )
                        {
                            n.Assigned_To__c = n.OwnerId ;
                        }
                        else
                        {
                            String branchId = null ;
                            
                            //  Get the branch from the contact, if it exists
                            if ( n.Opportunity_Contact__c != null )
                            {
                                Contact c = cMap.get ( n.Opportunity_Contact__c ) ;
                                
                                if ( ( c != null ) && ( ! String.isEmpty ( c.Branch__c ) ) )
                                { 
                                    branchId = c.Branch__c ;
                                }
                            }
                            
                            //  Get the relationship (account?)
                            else if ( n.Relationship__c != null )
                            {
                            	Account a = aMap.get ( n.Relationship__c ) ;
                                
                                if ( ( a != null ) && ( ! String.isEmpty ( a.branch__c ) ) )
                                {
                                    branchId = a.branch__c ;
                                }
                            }
                            
                            if ( branchId != null )
                            {
                                Branch__c b = bIDMap.get ( branchId ) ;
                                
                                if ( ( b != null ) && ( b.Branch_Manager__c != null ) )
                                {
                                    n.Assigned_To__c = b.Branch_Manager__c ;
                                }
                            }
                            else
                            {
                                Branch__c b = bNameMap.get ( 'Internet' ) ;
                                
                                if ( ( b != null ) && ( b.Branch_Manager__c != null ) )
                                {
                                    n.Assigned_To__c = b.Branch_Manager__c ;
                                }
                            }
                        }
                    }
                    
                    //  Default to owner otherwise
                    if ( ( n.Assigned_To__c == null ) && ( OneUnitedUtilities.isUser ( n.OwnerId ) ) )
                    {
                        System.debug ( '-- Defaulting assigned-to to the owner.' ) ;
                        n.Assigned_To__c = n.OwnerId ;
                    }
                    
                    //  If there is an assigned to, set to owner
                    else if ( n.Assigned_To__c != null )
                    {
                        n.OwnerId = n.Assigned_To__c ;
                    }
                }
            }
            
            //  ------------------------------------------------------------------------
            //  On update
            //  ------------------------------------------------------------------------
            if ( Trigger.isUpdate )
            {
            	System.debug ( '---------- BEFORE UPDATE ----------' ) ;
                oneOpportunity__c o = Trigger.oldMap.get ( n.ID ) ;
    
                if ( ( n.RecordTypeId == depositTypeId ) || ( n.RecordTypeId == loanTypeId ) )
                {
                    String x = n.OwnerId ;
                    
                    if ( ( String.isBlank ( n.Assigned_To__c ) ) && ( OneUnitedUtilities.isUser ( x ) ) )
                    {
                        n.Assigned_To__c = n.OwnerId ;
                    }
    
                    if ( ( o.Assigned_To__c != n.Assigned_To__c ) && ( String.isNotBlank ( n.Assigned_To__c ) ) )
                    {
                        n.OwnerId = n.Assigned_To__c ;
                    }
                
                    if ( ( OneUnitedUtilities.isUser ( x ) ) && ( o.OwnerId != n.OwnerId ) )
                    {
                        n.Assigned_To__c = n.OwnerId ;
                    }
                }
                
                if ( ( n.Loan_Application__c == null ) && ( n.RecordTypeId == loanTypeId ) )
                {
                    if ( ( String.isBlank ( o.Loan_Application_ID__c ) ) && ( String.isNotBlank ( n.Loan_Application_ID__c ) ) )
                    {
                        if ( ! laMap.containsKey ( n.Loan_Application_ID__c ) )
                            laMap.put ( n.Loan_Application_ID__c, true ) ;
                    }
                    else if ( ( String.isNotBlank ( o.Loan_Application_ID__c ) ) && 
                              ( String.isNotBlank ( n.Loan_Application_ID__c ) ) &&
                              ( ! n.Loan_Application_ID__c.equalsIgnoreCase ( o.Loan_Application_ID__c ) ) )
                    {
                        if ( ! laMap.containsKey ( n.Loan_Application_ID__c) )
                            laMap.put ( n.Loan_Application_ID__c, true ) ;
                    }
                }
            }
        }
        
        if ( Trigger.isUpdate )
        {
            map<String,Loan_Application__c> xMap ;
            if ( ( laMap != null ) && ( laMap.keySet().size () > 0 ) )
                xMap = oneOpportunityHelper.getPossibleLoanAppByID ( new list<String> ( laMap.keySet () ) ) ;
            
            if ( ( xMap != null ) && ( xMap.keyset().size () > 0 ) ) 
            {
                for ( oneOpportunity__c n : Trigger.new )
                {
                    if ( ( n.Loan_Application__c == null ) && ( String.isNotBlank ( n.Loan_Application_ID__c ) ) && ( n.RecordTypeId == loanTypeId ) )
                    {
                        if ( xMap.containsKey ( n.Loan_Application_ID__c ) )
                            n.Loan_Application__c = xMap.get ( n.Loan_Application_ID__c ).ID ;
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Fill in for BRANCH
         * ----------------------------------------------------------------------------------- */
        for ( oneOpportunity__c n : Trigger.new )
        {
            if ( String.isEmpty ( n.Branch__c ) )
            {
                User u = uMap.get ( n.createdById ) ;
                
                if ( ( u != null ) && ( String.isNotEmpty ( u.Branch__c ) ) )
                {
                     Branch__c b = bNameMap.get ( u.Branch__c ) ;
                    
                    if ( b != null )
                        n.Branch__c = b.id ;
                } 
            }
        }
    }
    
    /* -----------------------------------------------------------------------------------
     * AFTER TRIGGER
     * ----------------------------------------------------------------------------------- */
    if ( Trigger.isAfter )
    {
        /* -----------------------------------------------------------------------------------
         * Loan application stuff
         * ----------------------------------------------------------------------------------- */
        map<String,ID> loanAppMap = new map<String,ID> () ;
        
        /* -----------------------------------------------------------------------------------
         * Load prior subscriptions into a map
         * ----------------------------------------------------------------------------------- */
        map<String,List<EntitySubscription>> esMAP = new map <String,List<EntitySubscription>> () ;
         
        List<String> eIDL = new List<String> () ;
        for ( oneOpportunity__c n : Trigger.new )
        	eIDL.add ( n.ID ) ;
        
        if ( eIDL.size () > 0 )
        {
	        List<EntitySubscription> esOLD = [
	            SELECT ID, SubscriberId, ParentId
	            FROM EntitySubscription
	            WHERE ParentId IN :eIDL
	            ORDER BY ParentId
	        ] ;
	        
	        if ( esOLD.size () > 0 )
        	{
		        System.debug ( 'ES LOADING' ) ;
		        for ( EntitySubscription es : esOLD )
		        {
		        	if ( esMAP.containsKey ( es.ParentID ) )
		        	{
		        		System.debug ( 'FOUND : ' + es.ParentID ) ;
		        		esMap.get ( es.ParentID ).add ( es ) ;
		        	}
		        	else
		        	{
		        		System.debug ( 'CREATE : ' + es.ParentID ) ;
		        		List<EntitySubscription> esL = new List<EntitySubscription> () ;
		        		esL.add ( es ) ;
		        		esMap.put ( es.ParentID, esL ) ;
		        	} 
		        }
        	}
        }
        
        /* -----------------------------------------------------------------------------------
         * LOOP
         * ----------------------------------------------------------------------------------- */
        List<EntitySubscription> esL = new List<EntitySubscription> () ; 
        
        for ( oneOpportunity__c n : Trigger.new )
        {
            //  ------------------------------------------------------------------------
            //  On Insert
            //  ------------------------------------------------------------------------
            if ( Trigger.isInsert )
            {
                System.debug ( ' -- AFTER -- Insert!' ) ;
                
                System.debug ( 'New: ID: ' + n.Id ) ;
                System.debug ( 'New: OW: ' + n.OwnerId ) ;
                System.debug ( 'New: CR: ' + n.CreatedById ) ;
                System.debug ( 'New: AS: ' + n.Assigned_To__c ) ;
                
                String x = n.OwnerId ;
                if ( OneUnitedUtilities.isUser ( x ) )
                {
                    System.debug ( 'ADDING - Assigned to a user' ) ;
                    EntitySubscription es = new EntitySubscription () ;
                    es.ParentId = n.Id ;
                    es.SubscriberId = n.OwnerId ;
                    
                    esL.add ( es ) ; 
                }
                
                String y = n.Assigned_to__c ;
                if ( String.isNotEmpty ( y ) )
                {
                    if ( ( n.Assigned_To__c != n.OwnerId ) && ( OneUnitedUtilities.isUser ( y ) ) )
                    {
                        System.debug ( 'ADDING - Assigned to/owner mismatch' ) ;
                        EntitySubscription es = new EntitySubscription () ;
                        es.ParentId = n.Id ;
                        es.SubscriberId = n.Assigned_To__c ;
                        
                        esL.add ( es ) ;
                    }
                }
                
                String z = n.CreatedById ;
                if ( ( n.CreatedById != n.OwnerId ) && ( OneUnitedUtilities.isUser ( z ) ) )
                {
                    System.debug ( 'ADDING - Created by/owner mismatch' ) ;
                    EntitySubscription es = new EntitySubscription () ;
                    es.ParentId = n.Id ;
                    es.SubscriberId = n.CreatedById ;
                    
                    esL.add ( es ) ;
                }
            }
            
            //  ------------------------------------------------------------------------
            //  On update
            //  ------------------------------------------------------------------------
            if ( Trigger.isUpdate )
            {
                System.debug ( '-- AFTER -- Update!' ) ;
                
                //  ------------------------------------------------------------------------
                //  Load Prior
                //  ------------------------------------------------------------------------
                List<EntitySubscription> esO ;
                if ( esMap.containsKey ( n.ID ) )
                	esO = esMap.get ( n.ID ) ;
                else
               		esO = new List<EntitySubscription> () ; 
                
                Map<String, String> esM = new Map<String, String> () ;
                
                for ( EntitySubscription e : esO )
                    esM.put ( e.SubscriberId, e.ParentId ) ;
                
                oneOpportunity__c o = Trigger.oldMap.get ( n.ID ) ;
                
                System.debug ( 'Old: ID: ' + o.Id ) ;
                System.debug ( 'Old: OW: ' + o.OwnerId ) ;
                System.debug ( 'Old: CR: ' + o.CreatedById ) ;
                System.debug ( 'Old: AS: ' + o.Assigned_To__c ) ;
                
                System.debug ( 'New: ID: ' + n.Id ) ;
                System.debug ( 'New: OW: ' + n.OwnerId ) ;
                System.debug ( 'New: CR: ' + n.CreatedById ) ;
                System.debug ( 'New: AS: ' + n.Assigned_To__c ) ;
                
                String x = n.OwnerId ;
                String y = o.OwnerId ;
                
                if ( ( OneUnitedUtilities.isUser ( x ) ) && ( x != y ) )
                {
                    if ( ! esM.containsKey ( n.OwnerId ) )
                    {
                        System.debug ( 'ADDING - old and new user mismatch' ) ;
                        EntitySubscription es = new EntitySubscription () ;
                        es.ParentId = n.Id ;
                        es.SubscriberId = n.OwnerId ;
                        
                        esL.add ( es ) ;
                    }
                    else
                    {
                        System.debug ( 'Already subscribed!' );
                    }
                }
                
                if ( ( n.Assigned_To__c != null ) && ( n.Assigned_To__c != n.OwnerId ) && ( OneUnitedUtilities.isUser ( x ) ) )
                {
                    System.debug ( 'Assigned to <> OWNER' ) ;
                    
                    if ( ( n.Assigned_To__c != o.OwnerId ) || ( n.Assigned_To__c != o.Assigned_To__c ) )
                    {
                        if ( ! esM.containsKey ( n.OwnerId ) )
                        {
                            System.debug ( 'ADDING - Assigned/to owner mismatch' ) ;
                            EntitySubscription es = new EntitySubscription () ;
                            es.ParentId = n.Id ;
                            es.SubscriberId = n.Assigned_To__c ;
                        
                            esL.add ( es ) ;
                        }
                        else
                        {
                            System.debug ( 'Already subscribed!' );
                        }
                    }
                }
                
                //  ------------------------------------------------------------------------
                //  Loan applications
                //  ------------------------------------------------------------------------
                if ( ( String.isBlank ( o.Loan_Application_ID__c ) ) && ( String.isNotBlank ( n.Loan_Application_ID__c ) ) )
                {
                    if ( ! loanAppMap.containsKey ( n.Loan_Application_ID__c ) )
	                    loanAppMap.put ( n.Loan_Application_ID__c, n.ID ) ;
                }
                else if ( ( String.isNotBlank ( o.Loan_Application_ID__c ) ) &&
                          ( String.isNotBlank ( n.Loan_Application_ID__c ) ) &&
                          ( ! n.Loan_Application_ID__c.equalsIgnoreCase ( o.Loan_Application_ID__c ) ) )
                {
                    if ( ! loanAppMap.containsKey ( n.Loan_Application_ID__c ) )
	                    loanAppMap.put ( n.Loan_Application_ID__c, n.ID ) ;
                }
            }
        }
        
        if ( ( ! loanAppMap.isEmpty() ) && ( loanAppMap.keySet().size () > 0 ) )
        {
            list<Loan_Application__c> laL = null ;
            
            try
            {
                laL = [
                    SELECT ID, Mortgagebot_Web_ID__c
                    FROM Loan_Application__c
                    WHERE Mortgagebot_Web_ID__c IN :loanAppMap.keySet ()
                ] ;
            }
            catch ( Exception e )
            {
                // nyet
            }
            
            if ( ( laL != null ) && ( laL.size () > 0 ) )
            {
                list<Loan_Application__c> xlaL = new list<Loan_Application__c> () ;
                
                for ( Loan_Application__c la : laL )
                {
                    if ( loanAppMap.containsKey ( la.Mortgagebot_Web_ID__c ) )
                    {
                        Loan_Application__c xla = new Loan_Application__c () ;
                        xla.ID = la.ID ;
                        xla.Opportunity__c = loanAppMap.get ( la.Mortgagebot_Web_ID__c ) ;
                        
                        xlaL.add ( xla ) ;
                    }
                }
                
                if ( ( xlaL != null ) && ( xlaL.size () > 0 ) )
                    update xlaL ;
            }
        }
        
        if ( ( esL != null ) && ( esL.size () > 0 ) ) 
            insert esL ;
    }
}