trigger CreditReportTrigger on creditchecker__Credit_Report__c ( after update )
{
    /* -----------------------------------------------------------------------------------
	 * randoms
	 * ----------------------------------------------------------------------------------- */
    map<ID,String> hazUpdateMap = new map<ID,String> () ;
    
    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		/* -----------------------------------------------------------------------------------
		 * LOOP
		 * ----------------------------------------------------------------------------------- */
		for ( creditchecker__Credit_Report__c n : Trigger.new )
		{
			creditchecker__Credit_Report__c o = Trigger.oldMap.get ( n.ID ) ;
         
            if ( ( ( String.isBlank ( o.creditchecker__Status__c ) ) || ( ! o.creditchecker__Status__c.equalsIgnoreCase ( 'completed' ) ) ) &&
                 ( ( String.isNotBlank ( n.creditchecker__Status__c ) ) && ( n.creditchecker__Status__c.equalsIgnoreCase ( 'completed' ) ) ) )
            {
                if ( ( n.Application__c != null ) && ( ! hazUpdateMap.containsKey ( n.Application__c ) ) )
                {
					hazUpdateMap.put ( n.Application__c, 'P960' ) ;
                }
            }
            
            if ( ( ( String.isBlank ( o.creditchecker__Status__c ) ) || ( ! o.creditchecker__Status__c.equalsIgnoreCase ( 'Request Failed' ) ) ) &&
                 ( ( String.isNotBlank ( n.creditchecker__Status__c ) ) && ( n.creditchecker__Status__c.equalsIgnoreCase ( 'Request Failed' ) ) ) )
            {
                if ( ( n.Application__c != null ) && ( ! hazUpdateMap.containsKey ( n.Application__c ) ) )
                {
					hazUpdateMap.put ( n.Application__c, 'E950' ) ;
                }
            }
        }
    }
    
    /* -----------------------------------------------------------------------------------
	 * LOS Helper the apps
	 * ----------------------------------------------------------------------------------- */
    if ( ( hazUpdateMap != null ) && ( hazUpdateMap.keySet ().size () > 0 ) ) 
    {
        list<one_application__c> oL = new list<one_application__c> () ;
        
        for ( ID x : hazUpdateMap.keySet () )
        {
            one_application__c o = new one_application__c () ;
            o.ID = x ;
            o.FIS_Decision_Code__c = hazUpdateMap.get ( x ) ;
            
            oL.add ( o ) ;
        }
        
        if ( ( oL != null ) && ( oL.size () > 0 ) )
        {
            try
            {
	            update oL ;
            }
            catch ( Exception e )
            {
                // ?
            }
        }
    }
}