/*
 *  Creates a corresponding case and/or task for leads that have come in from the website.
 *  Note that this DELETES leads that it finds prior emails for!
 */
trigger CombinedAfterLeadInsert on Lead (before insert, after insert, before update, after update)
{
    //  --------------------------------------------------------------
    //  Before
    //  --------------------------------------------------------------
	if ( Trigger.isBefore )
    {
        if ( Trigger.isInsert )
        {
            for ( Lead n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  SMS due to Exact Target stupid formatting
                //  --------------------------------------------------------------
                if ( String.isNotBlank ( n.SMS_Number__c ) )
                {
                    if ( ( n.SMS_Phone__c == null ) || ( ! n.SMS_Phone__c.equalsIgnoreCase ( n.SMS_Number__c ) ) )
                        n.SMS_Phone__c = n.SMS_Number__c ;
                }
            }
        }
        
        if ( Trigger.isUpdate )
        {
            for ( Lead n : Trigger.new )
            {
                //  --------------------------------------------------------------
                //  Prior
                //  --------------------------------------------------------------
				Lead o = Trigger.oldMap.get ( n.ID ) ;
                
                //  --------------------------------------------------------------
                //  SMS due to Exact Target stupid formatting
                //  --------------------------------------------------------------
                if ( String.isNotBlank ( n.SMS_Number__c ) )
                {
                    if ( ( n.SMS_Phone__c == null ) || ( ! n.SMS_Phone__c.equalsIgnoreCase ( n.SMS_Number__c ) ) )
                        n.SMS_Phone__c = n.SMS_Number__c ;
                }
            }
        }
    }
    
    //  --------------------------------------------------------------
    //  After
    //  --------------------------------------------------------------
    if ( Trigger.isAfter )
    {
        if ( Trigger.isInsert )
        {
            System.debug ( '========== Combined After Lead Insert START ==========' ) ;
            
            Set<String> inEmails = new Set<String> () ;
            Set<String> inIds = new Set<String> () ;
            
            System.debug ( '----- Building email/ID list for querying -----' ) ;
            
            //  Setup Loop
            for ( Lead l : Trigger.new )
            {
                //  Only check for valid emails from the website
                if ( ( l.email != null ) && ( l.Website_Form__c != null ) )
                {
                    //  Emails are the main thing to search on
                    inEmails.add ( l.email ) ;
                    
                    //  We need the incoming lead ID to avoid selecting it from existing leads
                    inIds.add ( l.id ) ;
                }
            }
            
            System.debug ( '----- Querying from existing contacts -----' ) ;
            
            Map<String, Contact> cMap = new Map<String, Contact> () ;
            List<Contact> cL = new List<Contact> () ;
            
            try
            {
                cL = [
                    SELECT ID, email
                    FROM contact 
                    WHERE email IN :inEmails
                ] ;
            }
            catch ( DMLException e )
            {
                System.debug ( 'Exception selecting Contacts?!? ' + e ) ;
            }
            
            if ( ( cL != null ) && ( cL.size () > 0 ) )
            {
                System.debug ( 'Loading ' + cL.size () + ' Contacts into map.' ) ;
                for ( Contact c : cL )
                {
                    String foo = c.Email.toLowerCase () ;
                    cMap.put ( foo , c ) ;
                }
            }
            
            System.debug ( '----- Querying from existing leads -----' ) ;
            
            Map<String, Lead> lMap = new Map<String, Lead> () ;
            List<Lead> lL = new List<Lead> () ;
            
            try
            {
                lL = [
                    SELECT ID, email
                    FROM Lead 
                    WHERE email IN :inEmails
                    AND ID NOT IN :inIds
                ] ;
            }
            catch ( DMLException e )
            {
                System.debug ( 'Exception Selecting Leads?!? ' + e ) ;
            }
            
            if ( ( lL != null ) && ( lL.size () > 0 ) )
            {
                System.debug ( 'Loading ' + cL.size () + ' Leads into map.' ) ;
                for ( Lead l : lL )
                {
                    String foo = l.Email.toLowerCase () ;
                    lMap.put ( foo , l ) ;
                }
            }
            
            //  All the new cases go in here for one bulk insert
            List<Case> newCase = new List<Case> () ;
            
            //  All the new tasks go in here for one bulk insert
            List<Task> newTask = new List<Task> () ;
            
            //  All the new attendees go in here
            List<Workshop_Attendee__c> newWA = new List<Workshop_Attendee__c> () ;
            
            //  All the new opportunities go in here
            List<oneOpportunity__c> newOO = new List<oneOpportunity__c> () ;
            
            //  Lead IDs get copied into this one for deletion at the end
            List<Lead> delLead = new List<Lead> () ;
            
            //  Updated contacts go here
            List<Contact> uContact = new List<Contact> () ;
            
            //  Updated Leads go here
            List<Lead> uLead = new List<Lead> () ;
            
            System.debug ( '----- Parsing through incoming leads -----' ) ;
            
            //  MAIN LOOP
            for ( Lead inLead : Trigger.new )
            {
                System.debug ( '--> In Loop' ) ;
                
                boolean convertContact = false ;
                
                //  Again, only check for valid emails from the website
                if ( ( inLead.email != null ) && ( inLead.Website_Form__c != null ) )
                {
                    String llEmail = inLead.email.toLowerCase () ;
                    
                    //  Call external function to prepopulate the case
                    Case nC = TriggerFunctions.createWebsiteCaseFromLead ( inLead ) ;
                    
                    //  Call external function to prepopulate the task
                    Task nT = TriggerFunctions.createWebsiteTaskFromLead ( inLead ) ;
                    
                    //  Call external function to prepopulate the attendee
                    Workshop_Attendee__c nWA = TriggerFunctions.createAttendeeFromLead ( inLead ) ;
                    
                    //  Call external function to prepopulate the opportunity
                    oneOpportunity__c nOO = TriggerFunctions.createOpportunityFromLead ( inLead ) ;
                    
                    System.debug ( '----- Checking against cases -----' ) ;
                    
                    //  Add the case to the bulk insert
                    if ( nC != null )
                    {
                        System.debug ( '----- Returned case from website lead -----' ) ;
                        System.debug ( '----- ' + nC.reason + '-----' ) ;
                        
                        //  Check for pre-existing records.
                        if ( cMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Contact for ' + inLead.email + ' -----' ) ;
                            
                            Contact c = cMap.get ( llEmail ) ;
                            System.debug ( '----- Contact ID: ' + c.Id + ' -----' ) ;
                            
                            nC.contactid = c.Id ;
                            
                            Contact uC = TriggerFunctions.updateContactFromLead ( c, inLead ) ;
                            uContact.add ( uC ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else if ( lMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Lead for ' + inLead.email + ' -----' ) ;
                            
                            Lead l = lMap.get ( llEmail ) ;
                            System.debug ( '----- Lead ID: ' + l.Id + ' -----' ) ;
                            
                            nC.Lead__c = l.Id ;
                            
                            Lead uL = TriggerFunctions.updateLeadFromLead ( l, inLead ) ;
                            uLead.add ( uL ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else
                        {
                            System.debug ( '----- No existing lead found, converting. -----' ) ;
                            
                            Contact c = TriggerFunctions.createContactFromLead ( inLead ) ;
                            nC.contactid = c.Id ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        
                        System.debug ( '----- Adding case -----' ) ;
                        newCase.add ( nC ) ;
                        
                        convertContact = true ;
                    }
                    
                    System.debug ( '----- Checking against tasks -----' ) ;
                    
                    //  Add the task to the bulk insert
                    if ( nT != null )
                    {
                        System.debug ( '----- Returned task from website lead -----' ) ;
                        
                        //  Check for pre-existing records.
                        if ( cMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Contact for ' + inLead.email + ' -----' ) ;
                            
                            Contact c = cMap.get ( llEmail ) ;
                            System.debug ( '----- Contact ID: ' + c.Id + ' -----' ) ;
                            
                            nT.whoId = c.Id ;
                            
                            Contact uC = TriggerFunctions.updateContactFromLead ( c, inLead ) ;
                            uContact.add ( uC ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else if ( lMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Lead for ' + inLead.email + ' -----' ) ;
                            
                            Lead l = lMap.get ( inLead.email ) ;
                            
                            System.debug ( '----- Lead ID: ' + l.Id + ', Converting -----' ) ;
                            Contact c = TriggerFunctions.createContactFromLead ( inLead ) ;
                            nT.whoId = c.Id ;
                            
                            Lead uL = TriggerFunctions.updateLeadFromLead ( l, inLead ) ;
                            uLead.add ( uL ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else
                        {
                            System.debug ( '----- No existing lead found, converting. -----' ) ;
                            
                            Contact c = TriggerFunctions.createContactFromLead ( inLead ) ;
                            nT.whoId = c.Id ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        
                        System.debug ( '----- Adding task -----' ) ;
                        newTask.add ( nT ) ;
                        
                        convertContact = true ;
                    }
                    
                    System.debug ( '----- Checking against attendees -----' ) ;
                    
                    //  Add the attendee to the bulk insert
                    if ( nWA != null )
                    {
                        System.debug ( '----- Returned attendee from website lead -----' ) ;
                        System.debug ( '----- ' + nWA.Workshop_Description__c + '-----' ) ;
                        
                        //  Check for pre-existing records.
                        if ( cMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Contact for ' + inLead.email + ' -----' ) ;
                            
                            Contact c = cMap.get ( llEmail ) ;
                            System.debug ( '----- Contact ID: ' + c.Id + ' -----' ) ;
                            
                            nWA.Contact__c = c.Id ;
                            nWA.Lead__c = null ;
                            
                            Contact uC = TriggerFunctions.updateContactFromLead ( c, inLead ) ;
                            uContact.add ( uC ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else if ( lMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Lead for ' + inLead.email + ' -----' ) ;
                            
                            Lead l = lMap.get ( llEmail ) ;
                            System.debug ( '----- Lead ID: ' + l.Id + ' -----' ) ;
                            
                            nWA.Lead__c = l.Id ;
                            
                            Lead uL = TriggerFunctions.updateLeadFromLead ( l, inLead ) ;
                            
                            System.debug ( '----- Converting updated lead. -----' ) ;
                            Contact c = TriggerFunctions.createContactFromLead ( uL ) ;
                            nWA.Contact__c = c.Id ;
                            nWA.Lead__c = null ;
                            
                            //  Delete found lead
                            delLead.add ( uL ) ;
                            
                            //  Delete incoming lead
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else
                        {
                            System.debug ( '----- No existing lead found, converting. -----' ) ;
                            
                            Contact c = TriggerFunctions.createContactFromLead ( inLead ) ;
                            nWA.Contact__c = c.Id ;
                            nWA.Lead__c = null ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        
                        System.debug ( '----- Adding attendee -----' ) ;
                        newWA.add ( nWA ) ;
                        
                        convertContact = true ;
                    }
                    
                    System.debug ( '----- Checking against opportunities -----' ) ;
                    
                    //  Add the opportunity to the bulk insert
                    if ( nOO != null )
                    {
                        System.debug ( '----- Returned opportunity from website lead -----' ) ;
                        
                        //  Check for pre-existing records.
                        if ( cMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Contact for ' + inLead.email + ' -----' ) ;
                            
                            Contact c = cMap.get ( llEmail ) ;
                            System.debug ( '----- Contact ID: ' + c.Id + ' -----' ) ;
                            
                            nOO.Opportunity_Contact__c = c.Id ;
                            
                            Contact uC = TriggerFunctions.updateContactFromLead ( c, inLead ) ;
                            uContact.add ( uC ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else if ( lMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Lead for ' + inLead.email + ' -----' ) ;
                            
                            Lead l = lMap.get ( inLead.email ) ;
                            
                            System.debug ( '----- Lead ID: ' + l.Id + ' -----' ) ;
                            Lead uL = TriggerFunctions.updateLeadFromLead ( l, inLead ) ;
                            
                            System.debug ( '----- Converting updated lead. -----' ) ;
                            Contact c = TriggerFunctions.createContactFromLead ( uL ) ;
                            nOO.Opportunity_Contact__c = c.Id ;
                            
                            //  Delete found lead
                            delLead.add ( uL ) ;
                            
                            //  Delete incoming lead
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else
                        {
                            System.debug ( '----- No existing lead found, converting. -----' ) ;
                            
                            Contact c = TriggerFunctions.createContactFromLead ( inLead ) ;
                            nOO.Opportunity_Contact__c = c.Id ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        
                        System.debug ( '----- Adding opportunity -----' ) ;
                        newOO.add ( nOO ) ;
                        
                        convertContact = true ;
                    }
                    
                    System.debug ( '----- Checking to see if we did something -----' ) ;
                    
                    if ( ( ! convertContact ) && ( ! inLead.Website_Form__c.equalsIgnoreCase ( 'Unity Visa' ) ) )
                    {
                        System.debug ( 'No task/case condition found, converting anyway, Jim said so' ) ;
                        
                        //  Check for pre-existing records.
                        if ( cMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Contact for ' + inLead.email + ' -----' ) ;
                            
                            Contact c = cMap.get ( llEmail ) ;
                            System.debug ( '----- Contact ID: ' + c.Id + ' -----' ) ;
                            
                            Contact uC = TriggerFunctions.updateContactFromLead ( c, inLead ) ;
                            uContact.add ( uC ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else if ( lMap.containsKey ( llEmail ) )
                        {
                            System.debug ( '----- Found existing Lead for ' + inLead.email + ' -----' ) ;
                            
                            Lead l = lMap.get ( llEmail ) ;
                            
                            System.debug ( '----- Lead ID: ' + l.Id + ' -----' ) ;
                            Lead uL = TriggerFunctions.updateLeadFromLead ( l, inLead ) ;
                            
                            System.debug ( '----- Converting updated lead. -----' ) ;
                            Contact c = TriggerFunctions.createContactFromLead ( uL ) ;
                            
                            //  Delete found lead
                            delLead.add ( uL ) ;
                            
                            //  Delete incoming lead
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                        else
                        {
                            System.debug ( '----- No existing lead found, converting. -----' ) ;
                            
                            Contact c = TriggerFunctions.createContactFromLead ( inLead ) ;
                            
                            Lead xL = new Lead ( id = inLead.id ) ;
                            delLead.add ( xL ) ;
                        }
                    }
                }
            }
            
            System.debug ( 'New Cases: ' + newCase.size () ) ;
            
            if ( newCase.size () > 0 )
            {
                System.debug ( '----- Inserting Case(s) -----' ) ;
                
                try
                {
                    insert newCase ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'newCase Insert ERROR' + e.getMessage () ) ;
                }
            }
            
            System.debug ( 'New Tasks: ' + newTask.size () ) ;
            
            if ( newTask.size () > 0 )
            {
                System.debug ( '----- Inserting Task(s) -----' ) ;
                if ( ! System.isQueueable () )
                {
                    LeadQueue lq = new LeadQueue ( newTask ) ;
                    System.enqueueJob ( lq ) ;
                }
                else
                {
                    try
                    {
                        insert newTask ;
                    }
                    catch ( Exception e )
                    {
                        System.debug ( 'newTask Insert ERROR' + e.getMessage () ) ;
                    }
                }
            }
            
            System.debug ( 'New Attendees: ' + newWA.size () ) ;
            
            if ( newWA.size () > 0 )
            {
                System.debug ( '----- Inserting Attendee(s) -----' ) ;
                
                if ( ! System.isQueueable () )
                {
                    LeadQueue lq = new LeadQueue ( newWA ) ;
                    System.enqueueJob ( lq ) ;
                }
                else
                {
                    try
                    {
                        insert newWA ;
                    }
                    catch ( Exception e )
                    {
                        System.debug ( 'newWA Insert ERROR' + e.getMessage () ) ;
                    }
                }
            }
            
            System.debug ( 'New Opportunities: ' + newOO.size () ) ;
            
            if ( newOO.size () > 0 )
            {
                System.debug ( '----- Inserting Opportunity(s) -----' ) ;
                
                if ( ! System.isQueueable () )
                {
                    LeadQueue lq = new LeadQueue ( newOO ) ;
                    System.enqueueJob ( lq ) ;
                }
                else
                {
                    try
                    {
                        insert newOO ;
                    }
                    catch ( Exception e )
                    {
                        System.debug ( 'newOO Insert ERROR' + e.getMessage () ) ;
                    }
                }
            }
            
            System.debug ( 'New Contacts: ' + uContact.size () ) ;
            
            if ( uContact.size () > 0 )
            {
                System.debug ( '----- Updating Contact(s) -----' ) ;
                
                try
                {
                    update uContact ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'uContact update ERROR' + e.getMessage () ) ;
                }
            }
            
            System.debug ( 'New Leads: ' + uLead.size () ) ;
            
            if ( uLead.size () > 0 )
            {
                System.debug ( '----- Updating Lead(s) -----' ) ;
                
                try
                {
                    update uLead ;
                }
                catch ( Exception e )
                {
                    System.debug ( 'uLead update ERROR' + e.getMessage () ) ;
                }
            }
            
            System.debug ( 'Delete Leads: ' + delLead.size () ) ;
            
            System.debug ( '----- Deleting Lead(s) with already existing records -----' ) ;
            
            Database.DeleteResult[] drA = null ;
            
            try
            {
                drA = Database.delete ( delLead, false ) ;
            }
            catch ( Exception e )
            {
                System.debug ( 'uLead update ERROR' + e.getMessage () ) ;
            }
            
            System.debug ( '----- Checking for possible lead deletion errors -----' ) ;
            
            if ( drA != null )
            {
                for ( Database.DeleteResult dr:drA )
                {
                    if ( ! dr.isSuccess () )
                    {
                        Database.Error err = dr.getErrors () [ 0 ] ;
                        
                        System.debug ( 'ERROR >> ' + err.getStatusCode () + ' -- ' + err.getMessage () ) ;
                    }
                }
            }
            
            System.debug ( '========== Combined After Lead Insert END ==========' ) ;            
        }
    }
}