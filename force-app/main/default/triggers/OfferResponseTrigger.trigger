trigger OfferResponseTrigger on Offer_Response__c (before insert, before update) 
{
    /* -----------------------------------------------------------------------------------
	 * WARIABLES
	 * ----------------------------------------------------------------------------------- */
    set<ID> sC = new set<ID> () ;
    map<ID, Referral_Code__c> rcMap = new Map<ID, Referral_Code__c> () ;
    
    /* -----------------------------------------------------------------------------------
	 * LOOP
	 * ----------------------------------------------------------------------------------- */
	for ( Offer_Response__c n : Trigger.new )
    {
        if ( ( n.Referral_Code__c == null ) && ( n.Contact__c != null ) )
        {
            if ( String.isNotBlank ( n.Offer_Criteria_Object__c ) && ( n.Offer_Criteria_Object__c.equalsIgnoreCase ( 'Referral_Code__c' ) ) )
            {
                if ( ! sC.contains ( n.Contact__c ) )
                    sC.add ( n.Contact__c ) ;
            }
        }
    }

    /* -----------------------------------------------------------------------------------
	 * QUERY
	 * ----------------------------------------------------------------------------------- */
    list<Referral_Code__c> rcL = null ;
    if ( ( sC != null ) && ( sC.size () > 0 ) )
    {
        try
        {
            rcL = [
                SELECT ID, Contact__c, CreatedDate, Financial_Accounts__c
                FROM Referral_Code__c
                WHERE Contact__c IN :sC 
            ] ;
        }
        catch ( DMLException e )
        {
            rcL = null ;
        }
    }
    
    /* -----------------------------------------------------------------------------------
	 * PARSE
	 * ----------------------------------------------------------------------------------- */
    if ( ( rcL != null ) && ( rcL.size () > 0 ) )
    {
        for ( Referral_Code__c rc : rcL )
        {
            if ( ! rcMap.containsKey ( rc.Contact__c ) )
            {
                //  No key, add
                rcMap.put ( rc.Contact__c, rc ) ;
            }
            else
            {
                //  Newer referral code, replace
                //  More financial accounts, replace
                if ( ( rc.CreatedDate > rcMap.get ( rc.Contact__c ).CreatedDate ) || ( rc.Financial_Accounts__c > rcMap.get ( rc.Contact__c ).Financial_Accounts__c ) )
                {
                    rcMap.remove ( rc.Contact__c ) ;
                    rcMap.put ( rc.Contact__c, rc ) ;
                }
            }
        }
    }
    
    /* -----------------------------------------------------------------------------------
	 * RELOAD
	 * ----------------------------------------------------------------------------------- */
	for ( Offer_Response__c n : Trigger.new )
    {
        if ( ( n.Referral_Code__c == null ) && ( n.Contact__c != null ) )
        {
            if ( String.isNotBlank ( n.Offer_Criteria_Object__c ) && ( n.Offer_Criteria_Object__c.equalsIgnoreCase ( 'Referral_Code__c' ) ) )
            {
                if ( rcMap.containsKey ( n.Contact__c ) )
                    n.Referral_Code__c = rcMap.get ( n.Contact__c ).ID ;
            }
        }
    }
}