trigger ProjectTrigger on Project__c ( after insert, after update ) 
{
	/* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		/* -----------------------------------------------------------------------------------
		 * Setup for follows
		 * ----------------------------------------------------------------------------------- */
		List<EntitySubscription> esL = new List<EntitySubscription> () ; 
		
		/* -----------------------------------------------------------------------------------
		 * LOOP
		 * ----------------------------------------------------------------------------------- */
		for ( Project__c n : Trigger.new )
		{
			//  ------------------------------------------------------------------------
			//  On Insert
			//  ------------------------------------------------------------------------
			if ( Trigger.isInsert )
			{
				System.debug ( '-- Insert!' ) ;
				
				System.debug ( 'New: ID: ' + n.Id ) ;
				System.debug ( 'New: OW: ' + n.OwnerId ) ;
				System.debug ( 'New: CR: ' + n.CreatedById ) ;
				
				//  Assigning chatter follow to object created.  Since insert, assuming no prior follow.
				String y = n.CreatedById ;
				if ( ( ! OneUnitedUtilities.isGuestUser ( y ) ) && ( OneUnitedUtilities.isUser ( y ) ) )
				{
					EntitySubscription es = new EntitySubscription () ;
					es.ParentId = n.Id ;
					es.SubscriberId = n.CreatedById ;
					
					esL.add ( es ) ; 
				}
/*				
				String z = n.OwnerId ;
				if ( OneUnitedUtilities.isGroup ( z ) )
				{
					List<String> userIdList = OneUnitedUtilities.getUserIdsFromGroupId ( z ) ;
					if ( ( userIdList != null ) && ( userIdList.size () > 0 ) )
					{
						for ( String userId : userIdList )
						{
							if ( userId != y )
							{
								EntitySubscription es = new EntitySubscription () ;
								es.ParentId = n.Id ;
								es.SubscriberId = userId ;
								
								esL.add ( es ) ;
							}
						}
					}
				}
*/
			}
			
			//  ------------------------------------------------------------------------
			//  On update
			//  ------------------------------------------------------------------------
			if ( Trigger.isUpdate )
			{
				System.debug ( '-- Update!' ) ;
				
				//  ------------------------------------------------------------------------
				//  Load Prior
				//  ------------------------------------------------------------------------
				Map<String, String> esM = OneUnitedUtilities.getCurrentSubscribers ( n.Id ) ;
				
				Project__c o = Trigger.oldMap.get ( n.ID ) ;
				
				System.debug ( 'Old: ID: ' + o.Id ) ;
				System.debug ( 'Old: OW: ' + o.OwnerId ) ;
				
				System.debug ( 'New: ID: ' + n.Id ) ;
				System.debug ( 'New: OW: ' + n.OwnerId ) ;
				
				String newId = n.OwnerId ;
				String oldId = o.OwnerId ;
				
				//  Check the old and new users
				if ( ( ! OneUnitedUtilities.isGuestUser ( newId ) ) && 
				     ( OneUnitedUtilities.isUser ( newId ) ) && 
					 ( ! OneUnitedUtilities.isGuestUser ( oldId ) ) &&
				     ( newId != oldId ) )
				{
					if ( ( esM != null ) && ( ! esM.containsKey ( n.OwnerId ) ) )
					{
						System.debug ( 'ADDING - old and new user mismatch' ) ;
						EntitySubscription es = new EntitySubscription () ;
						es.ParentId = n.Id ;
						es.SubscriberId = n.OwnerId ;
						
						esL.add ( es ) ;
					}
					else
					{
						System.debug ( 'Already subscribed!' );
					}
				}
/*				
				//  Check to see if the new owner is a group/queue
				if ( OneUnitedUtilities.isGroup ( newId ) )
				{
					List<String> userIdList = OneUnitedUtilities.getUserIdsFromGroupId ( newId ) ;
					if ( ( userIdList != null ) && ( userIdList.size () > 0 ) )
					{
						for ( String userId : userIdList )
						{
							if ( ( esM != null ) && ( ! esM.containsKey ( userId ) ) )
							{
								EntitySubscription es = new EntitySubscription () ;
								es.ParentId = n.Id ;
								es.SubscriberId = userId ;
								
								esL.add ( es ) ;
							}
						}
					}
				}
*/
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * Following crap insertion
		 * ----------------------------------------------------------------------------------- */
		if ( ( esL != null ) && ( esL.size () > 0 ) ) 
			insert esL ;
	}

}