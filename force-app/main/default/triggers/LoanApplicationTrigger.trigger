trigger LoanApplicationTrigger on Loan_Application__c (after insert, after update, before insert, before update) 
{
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
        /* -----------------------------------------------------------------------------------
         * SETUP
         * ----------------------------------------------------------------------------------- */
        list<String> ceL = new list<String> () ;
        list<String> ctL = new list<String> () ;
        list<String> aaL = new list<String> () ;
        list<String> faL = new list<String> () ;
        list<String> oeL = new list<String> () ;
        list<String> ocL = new list<String> () ;
        
        /* -----------------------------------------------------------------------------------
         * First lookup, contacts
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
                        
            //  Borrower Contact - Name & Email
            if ( n.Borrower_Contact__c == null )
            {
          		if ( ( String.isNotBlank ( n.Borrower_First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Email__c ) ) )
                {
                    if ( OneUnitedUtilities.isValidEmail ( n.Borrower_Email__c ) )
                    {
                        String lookupString = n.Borrower_First_Name__c + '-' + n.Borrower_Last_Name__c + '-' + n.Borrower_Email__c ;
                        ceL.add ( lookupString ) ;
                    }
                }
                
                if ( String.isNotBlank ( n.Borrower_Tax_ID__c ) )
                    ctL.add ( n.Borrower_Tax_ID__c ) ;
            }                    
            
            //  Co-Borrower Contact
            if ( n.Co_Borrower_Contact__c == null )
            {
          		if ( ( String.isNotBlank ( n.Co_Borrower_First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Co_Borrower_Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Co_Borrower_Email__c ) ) )
                {
                    if ( OneUnitedUtilities.isValidEmail ( n.Borrower_Email__c ) )
                    {
                        String lookupString = n.Co_Borrower_First_Name__c + '-' + n.Co_Borrower_Last_Name__c + '-' + n.Co_Borrower_Email__c ;
                        ceL.add ( lookupString ) ;
                    }
                }
                
                if ( String.isNotBlank ( n.Co_Borrower_Tax_ID__c ) )
                    ctL.add ( n.Co_Borrower_Tax_ID__c ) ;
            }
            
            //  Financial account --> Application_Number__c is SOMETIMES an account number.
            if ( n.financial_account__c == null )
            {
                if ( String.isNotBlank ( n.Application_Number__c ) )
                    faL.add ( n.Application_Number__c ) ;
                
                if ( String.isNotBlank ( n.InsightAccount__c ) )
                    faL.add ( n.InsightAccount__c ) ;
            }
            
            //  Organization 
            if ( n.Organization__c == null )
            {
                if ( String.isNotBlank ( n.Organization_EIN__c ) ) 
                    aaL.add ( n.Organization_EIN__c ) ;
            }
            
            //  Primary Person/Org
            if (( n.Primary_Person__c == null ) && ( n.Primary_Organization__c == null ) )
            {
                if ( String.isNotBlank ( n.PriTaxID__c ) ) {
                    ctL.add ( n.PriTaxID__c ) ;
                    aaL.add ( n.PriTaxID__c ) ;
                }
                
                //  Organization Contact, marketing cloud
                if ( String.isNotBlank ( n.Organization_EIN__c ) ) 
                    ctL.add ( n.Organization_EIN__c ) ;
            }
            
            //  Owner 1 Person/Org
            if (( n.Owner_1_Person__c == null ) && ( n.Owner_1_Organization__c == null ))
            {
                if ( String.isNotBlank ( n.Own1TaxID__c ) ) {
                    ctL.add ( n.Own1TaxID__c ) ;
                    aaL.add ( n.Own1TaxID__c ) ;
                }
            }
            
            //  Owner 2 Person/Org
            if (( n.Owner_2_Person__c == null ) && ( n.Owner_2_Organization__c == null ))
            {
                if ( String.isNotBlank ( n.Own2TaxID__c ) ) {
                    ctL.add ( n.Own2TaxID__c ) ;
                    aaL.add ( n.Own2TaxID__c ) ;
                }
            }
            
            //  Owner 3 Person/Org
            if (( n.Owner_3_Person__c == null ) && ( n.Owner_3_Organization__c == null ))
            {
                if ( String.isNotBlank ( n.Own3TaxID__c ) ) {
                    ctL.add ( n.Own3TaxID__c ) ;
                    aaL.add ( n.Own3TaxID__c ) ;
                }
            }
            
            //  Owner 4 Person/Org
            if (( n.Owner_4_Person__c == null ) && ( n.Owner_4_Organization__c == null ))
            {
                if ( String.isNotBlank ( n.Own4TaxID__c ) ) {
                    ctL.add ( n.Own4TaxID__c ) ;
                    aaL.add ( n.Own4TaxID__c ) ;
                }
            }
            
            
            //  Owner 5 Person/Org
            if (( n.Owner_5_Person__c == null ) && ( n.Owner_5_Organization__c == null ))
            {
                if ( String.isNotBlank ( n.Own5TaxID__c ) ) {
                    ctL.add ( n.Own5TaxID__c ) ;
                    aaL.add ( n.Own5TaxID__c ) ;
                }
            }
            
        }
        
        /* -----------------------------------------------------------------------------------
         * Contact - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,Contact> ceMap = null ;
        if ( ( ceL != null ) && ( ceL.size () > 0 ) )
	        ceMap = LoanApplicationHelper.getPossibleContactsByNameEmail ( ceL ) ;

        /* -----------------------------------------------------------------------------------
         * Contact - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,Contact> ctMap = null ;
        if ( ( ctL != null ) && ( ctL.size () > 0 ) )
	        ctMap = LoanApplicationHelper.getPossibleContactsByTaxID ( ctL ) ;

        /* -----------------------------------------------------------------------------------
         * Account - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,Account> atMap = null ;
        if ( ( aaL != null ) && ( aaL.size () > 0 ) )
	        atMap = LoanApplicationHelper.getPossibleAccountsByFederalTaxID ( aaL ) ;

        /* -----------------------------------------------------------------------------------
         * Financial Accounts - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,Financial_Account__c> faMap = null ;
        if ( ( faL != null ) && ( faL.size () > 0 ) )
	        faMap = LoanApplicationHelper.getPossibleFinancialAccounts ( faL ) ;
        
        /* -----------------------------------------------------------------------------------
         * Load in found data, first round
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
            //  Contact
            if ( n.Borrower_Contact__c == null )
            {
                if ( String.isNotBlank ( n.Borrower_Tax_ID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Borrower_Tax_ID__c ) )
                        {
                            Contact c = ctMap.get ( n.Borrower_Tax_ID__c ) ;
                            n.Borrower_Contact__c = c.ID ;
                        }
                    }
                }
                
                if ( ( n.Borrower_Contact__c == null ) &&
                     ( String.isNotBlank ( n.Borrower_First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Email__c ) ) )
                {
                    if ( ( ceMap != null ) && ( ceMap.keySet().size () > 0 ) )
                    {
                        String key = n.Borrower_First_Name__c + '-' + n.Borrower_Last_Name__c + '-' + n.Borrower_Email__c ;
                        if ( ceMap.containsKey ( key ) )
                        {
                            Contact c = ceMap.get ( key ) ;
                            n.Borrower_Contact__c = c.ID ;
                        }
                    }
                }
            }
            
            if ( n.Co_Borrower_Contact__c == null )
            {
                if ( String.isNotBlank ( n.Co_Borrower_Tax_ID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Co_Borrower_Tax_ID__c ) )
                        {
                            Contact c = ctMap.get ( n.Co_Borrower_Tax_ID__c ) ;
                            n.Co_Borrower_Contact__c = c.ID ;
                        }
                    }
                }
                
                if ( ( n.Co_Borrower_Contact__c == null ) &&
                     ( String.isNotBlank ( n.Co_Borrower_First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Co_Borrower_Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Co_Borrower_Email__c ) ) )
                {
                    if ( ( ceMap != null ) && ( ceMap.keySet().size () > 0 ) )
                    {
                        String key = n.Co_Borrower_First_Name__c + '-' + n.Co_Borrower_Last_Name__c + '-' + n.Co_Borrower_Email__c ;
                        if ( ceMap.containsKey ( key ) )
                        {
                            Contact c = ceMap.get ( key ) ;
                            n.Co_Borrower_Contact__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Financial Accounts
            if ( ( faMap != null ) && ( faMap.keySet().size () > 0 ) )
            {
	            if ( n.financial_account__c == null )
                {
                    if ( faMap.containsKey ( n.Application_Number__c ) )
                    {
                        Financial_Account__c fa = faMap.get ( n.Application_Number__c ) ; 
                        n.financial_account__c = fa.ID ;
                    }
                    
                    if ( faMap.containsKey ( n.InsightAccount__c ) )
                    {
                        Financial_Account__c fa = faMap.get ( n.InsightAccount__c ) ; 
                        n.financial_account__c = fa.ID ;
                    }
                }
            }
            
            //  Organization
            if ( n.Organization__c == null )
            {
                if ( String.isNotBlank ( n.Organization_EIN__c  ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.Organization_EIN__c ) )
                        {
                            Account a = atMap.get ( n.Organization_EIN__c  ) ;
                            n.Organization__c = a.ID ;
                        }
                    }
                }
            }
            
            if ( ( n.Primary_Organization__c == null ) && ( n.Organization__c != null ) && 
                 ( ( String.isNotBlank ( n.Organization_EIN__c ) ) && ( String.isNotBlank ( n.PriTaxID__c ) ) && ( n.Organization_EIN__c.equalsIgnoreCase ( n.PriTaxID__c ) ) ) )
            {
                n.Primary_Organization__c = n.Organization__c ;
            }
            
            //  Primary Organization
            if ( n.Primary_Organization__c == null )
            {
                if ( String.isNotBlank ( n.PriTaxID__c ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.PriTaxID__c ) )
                        {
                            Account c = atMap.get ( n.PriTaxID__c ) ;
                            n.Primary_Organization__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Primary Person
            if ( n.Primary_Person__c == null )
            {
                if ( String.isNotBlank ( n.PriTaxID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.PriTaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.PriTaxID__c ) ;
                            n.Primary_Person__c = c.ID ;
                        }
                    }
                }
                
                if ( ( n.Primary_Person__c == null ) && ( String.isNotBlank ( n.Organization_EIN__c ) ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Organization_EIN__c ) )
                        {
                            Contact c = ctMap.get ( n.Organization_EIN__c ) ;
                            n.Primary_Person__c = c.ID ;
                        }
                    }
                }
                
                if ( ( n.Primary_Person__c == null ) && ( String.isNotBlank ( n.Own1TaxID__c ) ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own1TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own1TaxID__c ) ;
                            n.Primary_Person__c = c.ID ;
                        }
                    }
                }
                
                if ( ( n.Primary_Person__c == null ) && ( String.isNotBlank ( n.Own2TaxID__c ) ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own2TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own2TaxID__c ) ;
                            n.Primary_Person__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 1 Organization
            if ( n.Owner_1_Organization__c == null )
            {
                if ( String.isNotBlank ( n.Own1TaxID__c ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.Own1TaxID__c ) )
                        {
                            Account c = atMap.get ( n.Own1TaxID__c ) ;
                            n.Owner_1_Organization__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 1 Person
            if ( n.Owner_1_Person__c == null )
            {
                if ( String.isNotBlank ( n.Own1TaxID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own1TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own1TaxID__c ) ;
                            n.Owner_1_Person__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 2 Organization
            if ( n.Owner_2_Organization__c == null )
            {
                if ( String.isNotBlank ( n.Own2TaxID__c ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.Own2TaxID__c ) )
                        {
                            Account c = atMap.get ( n.Own2TaxID__c ) ;
                            n.Owner_2_Organization__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 2 Person
            if ( n.Owner_2_Person__c == null )
            {
                if ( String.isNotBlank ( n.Own2TaxID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own2TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own2TaxID__c ) ;
                            n.Owner_2_Person__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 3 Organization
            if ( n.Owner_3_Organization__c == null )
            {
                if ( String.isNotBlank ( n.Own3TaxID__c ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.Own3TaxID__c ) )
                        {
                            Account c = atMap.get ( n.Own3TaxID__c ) ;
                            n.Owner_3_Organization__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 3 Person
            if ( n.Owner_3_Person__c == null )
            {
                if ( String.isNotBlank ( n.Own3TaxID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own3TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own3TaxID__c ) ;
                            n.Owner_3_Person__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 4 Organization
            if ( n.Owner_4_Organization__c == null )
            {
                if ( String.isNotBlank ( n.Own4TaxID__c ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.Own4TaxID__c ) )
                        {
                            Account c = atMap.get ( n.Own4TaxID__c ) ;
                            n.Owner_4_Organization__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 4 Person
            if ( n.Owner_4_Person__c == null )
            {
                if ( String.isNotBlank ( n.Own4TaxID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own4TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own4TaxID__c ) ;
                            n.Owner_4_Person__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 5 Organization
            if ( n.Owner_5_Organization__c == null )
            {
                if ( String.isNotBlank ( n.Own5TaxID__c ) )
                {
                    if ( ( atMap != null ) && ( atMap.keySet().size () > 0 ) )
                    {
                        if ( atMap.containsKey ( n.Own5TaxID__c ) )
                        {
                            Account c = atMap.get ( n.Own5TaxID__c ) ;
                            n.Owner_5_Organization__c = c.ID ;
                        }
                    }
                }
            }
            
            //  Owner 5 Person
            if ( n.Owner_5_Person__c == null )
            {
                if ( String.isNotBlank ( n.Own5TaxID__c ) )
                {
                    if ( ( ctMap != null ) && ( ctMap.keySet().size () > 0 ) )
                    {
                        if ( ctMap.containsKey ( n.Own5TaxID__c ) )
                        {
                            Contact c = ctMap.get ( n.Own5TaxID__c ) ;
                            n.Owner_5_Person__c = c.ID ;
                        }
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Second lookup, this will included contacts added above through name & email
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
            if ( n.Borrower_Contact__c != null )
            {
                ocL.add ( n.Borrower_Contact__c ) ;
            }
            
            if ( n.Co_Borrower_Contact__c != null )
            {
                ocL.add ( n.Co_Borrower_Contact__c ) ;
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Opportunity #1 - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,oneOpportunity__c> ocMap = null ;
        if ( ( ocL != null ) && ( ocL.size () > 0 ) )
	        ocMap = LoanApplicationHelper.getPossibleOpportunitiesByContact ( ocL ) ;
        
        /* -----------------------------------------------------------------------------------
         * Load in found data, second round
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
            //  Opportunity
            if ( ( ocMap != null ) && ( ocMap.keySet().size () > 0 ) )
            {
	            if ( n.Opportunity__c == null )
                {
                    if ( ocMap.containsKey ( n.Borrower_Contact__c ) )
                    {
                        oneOpportunity__c oo = ocMap.get ( n.Borrower_Contact__c ) ;
                        n.Opportunity__c = oo.ID ;
                    }
                    else if ( ocMap.containsKey ( n.Co_Borrower_Contact__c ) )
                    {
                        oneOpportunity__c oo = ocMap.get ( n.Co_Borrower_Contact__c ) ;
                        n.Opportunity__c = oo.ID ;
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Setup keys for lookup, No opportunities found by contact, check email
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
            //  Opportunity --> email
            if ( n.Opportunity__c == null )
            {
                if ( ( String.isNotBlank ( n.Borrower_Email__c ) ) && ( OneUnitedUtilities.isValidEmail ( n.Borrower_Email__c ) ) )
                    oeL.add ( n.Borrower_Email__c ) ;
                
                if ( ( String.isNotBlank ( n.Co_Borrower_Email__c ) ) && ( OneUnitedUtilities.isValidEmail ( n.Co_Borrower_Email__c ) ) )
                    oeL.add ( n.Co_Borrower_Email__c ) ;
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Opportunity #2 - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,oneOpportunity__c> oeMap = null ;
        if ( ( oeL != null ) && ( oeL.size () > 0 ) )
	        oeMap = LoanApplicationHelper.getPossibleOpportunitiesByEmail ( oeL ) ;
        
        /* -----------------------------------------------------------------------------------
         * Load in found data, third round
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
            //  Opportunity
            if ( ( oeMap != null ) && ( oeMap.keySet().size () > 0 ) )
            {
	            if ( n.Opportunity__c == null )
                {
                    if ( oeMap.containsKey ( n.Borrower_Email__c ) )
                    {
                        oneOpportunity__c oo = oeMap.get ( n.Borrower_Email__c ) ;
                        n.Opportunity__c = oo.ID ;
                    }
                    else if ( oeMap.containsKey ( n.Co_Borrower_Email__c ) )
                    {
                        oneOpportunity__c oo = oeMap.get ( n.Co_Borrower_Email__c ) ;
                        n.Opportunity__c = oo.ID ;
                    }
                }
            }
        }
        
        
        /* -----------------------------------------------------------------------------------
         * Extra bits
         * ----------------------------------------------------------------------------------- */
        for ( Loan_Application__c n : Trigger.new )
        {
            if ( String.isBlank ( n.Upload_Attachment_Key__c ) )
                n.Upload_Attachment_Key__c = loan_application_utils.getRandomString ( 30 ) ;
        }
    }

    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
        /* -----------------------------------------------------------------------------------
         * SETUP
         * ----------------------------------------------------------------------------------- */
        String account = OneUnitedUtilities.getDefaultAccount ().ID ;
        String accountBusiness = OneUnitedUtilities.getDefaultBusinessAccount ().ID ;
        
		if ( LoanApplicationHelper.laOOMap == null )
        	LoanApplicationHelper.laOOMap = new map<String, map<ID,Loan_Application__c>> () ;
        
		if ( LoanApplicationHelper.laCMap == null )
        	LoanApplicationHelper.laCMap = new map<String, map<ID,Loan_Application__c>> () ;
        
		if ( LoanApplicationHelper.laAMap == null )
        	LoanApplicationHelper.laAMap = new map<String, map<ID,Loan_Application__c>> () ;
        
        /* -----------------------------------------------------------------------------------
         *  Main Map to Update
         * ----------------------------------------------------------------------------------- */
        map<String, Loan_Application__c> laM = new map<String, Loan_Application__c> () ;
       
        /* -----------------------------------------------------------------------------------
         *  Set up the things we didn't find, Accounts
         * ----------------------------------------------------------------------------------- */
        list<Account> aL = new list<Account> () ;
        map<String,Account> effYouMap = new map<String,Account> () ;
        set<ID> effYouSet = new set<ID> () ;
        
        for ( Loan_Application__c n : Trigger.new )
        {
			if ( n.RecordTypeId == LoanApplicationHelper.getPPPRecordTypeID () )
            {
                if ( n.Organization__c == null )
                {
                    Account a = new Account () ;
                    a.ParentId = accountBusiness ;
                    a.RecordTypeId = LoanApplicationHelper.getCustomerOrgRecordTypeID () ;
                    a.Name = n.Organization_Name__c ;
                    a.BillingStreet = n.Organization_Street__c ;
                    a.BillingCity = n.Organization_City__c ;
                    a.BillingPostalCode = n.Organization_Zip_Code__c ;
                    a.BillingState = n.Organization_State__c ;
                    a.Business_Email__c = n.Organization_Email__c ;
                    a.FederalTaxID__c = n.Organization_EIN__c ;
                    
	                String lookupString = 'O1' + n.Organization_Name__c + '-' + n.Organization_Email__c ;
                    
                    if ( ! LoanApplicationHelper.laAMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laAMap.put ( lookupString, xMap ) ;
                        aL.add ( a ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laAMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laAMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
                else
                {
                    effYouSet.add ( n.Organization__c ) ;
                }
                    
                
                if ( n.Primary_Organization__c == null )
                {
                    if ( ( String.isNotBlank ( n.Organization_EIN__c ) ) && ( String.isNotBlank ( n.PriTaxID__c ) ) )
                    {
                        if ( ! n.Organization_EIN__c.equalsIgnoreCase ( n.PriTaxID__c ) )
                        {
                            Account a = new Account () ;
                            a.ParentId = accountBusiness ;
                            a.RecordTypeId = LoanApplicationHelper.getCustomerOrgRecordTypeID () ;
                            a.Name = n.PriFullName__c ;
                            a.BillingStreet = n.PriAddress1__c ;
                            a.BillingCity = n.Primary_City__c ;
                            a.BillingPostalCode = n.Primary_Zip_Code__c ;
                            a.BillingState = n.Primary_State__c ;
                            a.Business_Email__c = n.Organization_Email__c ;
                            a.FederalTaxID__c = n.PriTaxID__c ;
                            
                            String lookupString = 'O2' + n.PriFullName__c + '-' + n.Organization_Email__c ;
                            
                            if ( ! LoanApplicationHelper.laAMap.containsKey ( lookupString ) )
                            {
                                map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                                xMap.put ( n.ID, n ) ;
                                 
                                LoanApplicationHelper.laAMap.put ( lookupString, xMap ) ;
                                aL.add ( a ) ;
                            }
                            else
                            {
                                if ( ! LoanApplicationHelper.laAMap.get ( lookupString ).containsKey ( n.ID ) )
                                    LoanApplicationHelper.laAMap.get ( lookupString ).put ( n.ID, n ) ;
                            }
                        }
                        else
                        {
                            String lookupString = 'OD' + n.PriFullName__c + '-' + n.Organization_Email__c ;
                            
                            if ( ! LoanApplicationHelper.laAMap.containsKey ( lookupString ) )
                            {
                                map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                                xMap.put ( n.ID, n ) ;
                                
                                LoanApplicationHelper.laAMap.put ( lookupString, xMap ) ;
                            }
                            else
                            {
                                if ( ! LoanApplicationHelper.laAMap.get ( lookupString ).containsKey ( n.ID ) )
                                    LoanApplicationHelper.laAMap.get ( lookupString ).put ( n.ID, n ) ;
                            }
                        }
                    }
                }
                else
                {
                    effYouSet.add ( n.Primary_Organization__c ) ;
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Load accounts
         * ----------------------------------------------------------------------------------- */
        list<Account> effYouAccountList = new list<Account> () ;
        
        try
        {
            effYouAccountList = [
                SELECT ID, Business_Email__c, FederalTaxID__c
                FROM Account
                WHERE ID IN :effYouSet
            ] ;
        }
        catch ( DMLException e )
        {
            effYouAccountList = null ;
        }
        
        if ( ( effYouAccountList != null ) && ( effYouAccountList.size () > 0 ) )
        {
            for ( Account a : effYouAccountList )
            {
                effYouMap.put ( a.Business_Email__c, a ) ;
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Create account records
         * ----------------------------------------------------------------------------------- */
        if ( ( aL != null ) && ( aL.size () > 0 ) )
        {
            boolean success = true ;
            try
            {
	            insert aL ;
            }
            catch ( Exception e )
            {
                success = false ;
            }
            
            if ( success )
            {
                for ( Account a : aL )
                {
                    if ( String.isNotBlank ( a.Business_Email__c ) )
	                    effYouMap.put ( a.Business_Email__c, a ) ;
                    
                    String lookupString1 = 'O1' + a.Name + '-' + a.Business_Email__c ;
                   
                    if ( LoanApplicationHelper.laAMap.containsKey ( lookupString1 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laAMap.get ( lookupString1 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Organization__c = a.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Organization__c = a.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    String lookupString2 = 'O2' + a.Name + '-' + a.Business_Email__c ;
                   
                    if ( LoanApplicationHelper.laAMap.containsKey ( lookupString2 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laAMap.get ( lookupString2 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Primary_Organization__c = a.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Primary_Organization__c = a.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    String lookupString3 = 'OD' + a.Name + '-' + a.Business_Email__c ;
                   
                    if ( LoanApplicationHelper.laAMap.containsKey ( lookupString3 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laAMap.get ( lookupString3 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Primary_Organization__c = la.Organization__c ;  //  Assumption?
                            }
                        }
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Set up the things we didn't find, Contacts/Opportunities
         * ----------------------------------------------------------------------------------- */
        list<Contact> cL = new list<Contact> () ;
        list<oneOpportunity__c> ooL = new list<oneOpportunity__c> () ;

        for ( Loan_Application__c n : Trigger.new )
        {
            if ( n.Borrower_Contact__c == null )
            {
                if ( ( String.isNotBlank ( n.Borrower_First_Name__c ) ) &&
                      ( String.isNotBlank ( n.Borrower_Last_Name__c ) ) &&
                      ( String.isNotBlank ( n.Borrower_Email__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    c.AccountId = account ;

                    c.FirstName = n.Borrower_First_Name__c ;
                    c.LastName = n.Borrower_Last_Name__c ;
                    c.Email = n.Borrower_Email__c ;
                     
                    if ( String.isNotBlank ( n.Borrower_Tax_ID__c ) ) 
                    	c.TaxID__c = n.Borrower_Tax_ID__c ;
                     
	                String lookupString = '1' + n.Borrower_First_Name__c + '-' + n.Borrower_Last_Name__c + '-' + n.Borrower_Email__c ;

                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                	   	LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
            }
            
            if ( n.Co_Borrower_Contact__c == null )
            {
                if ( ( String.isNotBlank ( n.Co_Borrower_First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Co_Borrower_Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Co_Borrower_Email__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    c.AccountId = account ;

                    c.FirstName = n.Co_Borrower_First_Name__c ;
                    c.LastName = n.Co_Borrower_Last_Name__c ;
                    c.Email = n.Co_Borrower_Email__c ;
                     
                    if ( String.isNotBlank ( n.Co_Borrower_Tax_ID__c ) ) 
                     	c.TaxID__c = n.Co_Borrower_Tax_ID__c ;
                     
	                String lookupString = '2' + n.Co_Borrower_First_Name__c + '-' + n.Co_Borrower_Last_Name__c + '-' + n.Co_Borrower_Email__c ;

                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
            	}
            }
            
            if ( n.RecordTypeId == LoanApplicationHelper.getPPPRecordTypeID () )
            {
                if ( ( n.Primary_Person__c == null ) && ( String.isNotBlank ( n.PriFullName__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    
                    c.AccountId = account ;
                    if ( effYouMap.containsKey ( n.Organization_Email__c ) )
                        c.AccountId = effYouMap.get ( n.Organization_Email__c ).ID ;
                    
                    if ( ( String.isNotBlank ( n.PriFirstName__c ) ) && ( String.isNotBlank ( n.PriLastName__c ) ) )
                    {
                        c.FirstName = n.PriFirstName__c ;
                        c.LastName = n.PriLastName__c ;
                    }
                    else
                    {
                        c.LastName = n.PriFullName__c ;
                    }
                    c.email = n.Organization_Email__c ;
                    c.Phone = n.Organization_Phone__c ;
                    
                    if ( String.isNotBlank ( n.PriAddress1__c ) )
                    {
	                    c.MailingStreet = n.PriAddress1__c ;
                        c.MailingCity = n.Primary_City__c ;
                        c.MailingState = n.Primary_State__c ;
                        c.MailingPostalCode = n.Primary_Zip_Code__c ;
                    }
                    else
                    {
	                    c.MailingStreet = n.Organization_Street__c ;
                        c.MailingCity = n.Organization_City__c ;
                        c.MailingState = n.Organization_State__c ;
                        c.MailingPostalCode = n.Organization_Zip_Code__c ;
                    }
                    
                    if ( String.isNotBlank ( n.PriTaxID__c ) )
	                    c.TaxID__c = n.PriTaxID__c ;
                    
	                String lookupString = 'P0' + n.PriFullName__c + '-' + n.Organization_Email__c ;
                    
                    if ( ( String.isNotBlank ( n.PriFirstName__c ) ) && ( String.isNotBlank ( n.PriLastName__c ) ) )
                        lookupString = 'P0' + n.PriFirstName__c + '-' + n.PriLastName__c + '-' + n.Organization_Email__c ;
                    
                    if ( String.isNotBlank ( n.PriTaxID__c ) )
                        lookupString = 'P0' + n.PriTaxID__c + '-' + n.Organization_Email__c ;
                    
                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
                
                if ( ( n.Own1PersonID__c == null ) && ( String.isNotBlank ( n.Own1FullName__c ) ) )
                {
                    if ( ( String.isNotBlank ( n.PriTaxID__c ) ) && ( String.isNotBlank ( n.Own1TaxID__c ) ) && ( ! n.PriTaxID__c.equalsIgnoreCase ( n.Own1TaxID__c ) ) )
                    {
                        Contact c = new Contact () ;
                        c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                        
                        c.AccountId = account ;
                        if ( effYouMap.containsKey ( n.Organization_Email__c ) )
                            c.AccountId = effYouMap.get ( n.Organization_Email__c ).ID ;
                        
                        if ( ( String.isNotBlank ( n.Own1FirstName__c ) ) && ( String.isNotBlank ( n.Own1LastName__c ) ) )
                        {
                            c.FirstName = n.Own1FirstName__c ;
                            c.LastName = n.Own1LastName__c ;
                        }
                        else
                        {
                            c.LastName = n.Own1FullName__c ;
                        }
                        c.email = n.Organization_Email__c ;
                        c.Phone = n.Organization_Phone__c ;
                        
                        if ( String.isNotBlank ( n.Own1Address1__c ) )
                        {
                            c.MailingStreet = n.Own1Address1__c ;
                            c.MailingCity = n.Own1City__c ;
                            c.MailingState = n.Own1State__c ;
                            c.MailingPostalCode = n.Own1Zip__c ;
                        }
                        else
                        {
                            c.MailingStreet = n.Organization_Street__c ;
                            c.MailingCity = n.Organization_City__c ;
                            c.MailingState = n.Organization_State__c ;
                            c.MailingPostalCode = n.Organization_Zip_Code__c ;
                        }
                        
                        if ( String.isNotBlank ( n.Own1TaxID__c ) )
                            c.TaxID__c = n.Own1TaxID__c ;
                        
                        String lookupString = 'P1' + n.Own1FullName__c + '-' + n.Organization_Email__c ;
                        
						if ( ( String.isNotBlank ( n.Own1FirstName__c ) ) && ( String.isNotBlank ( n.Own1LastName__c ) ) )
	                        lookupString = 'P1' + n.Own1FirstName__c + '-' + n.Own1LastName__c + '-' + n.Organization_Email__c ;
                        
                        if ( String.isNotBlank ( n.Own1TaxID__c ) )
                            lookupString = 'P1' + n.Own1TaxID__c + '-' + n.Organization_Email__c ;
                    
                        if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                        {
                            map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                            xMap.put ( n.ID, n ) ;
                             
                            LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                            cL.add ( c ) ;
                        }
                        else
                        {
                            if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                                LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                        }
                    }
                    else
                    {
                        //  Matching tax ID, have to hack it!
                        String lookupString = 'P1!' + n.Own1FullName__c + '-' + n.Organization_Email__c ;
                        
						if ( ( String.isNotBlank ( n.Own1FirstName__c ) ) && ( String.isNotBlank ( n.Own1LastName__c ) ) )
	                        lookupString = 'P1!' + n.Own1FirstName__c + '-' + n.Own1LastName__c + '-' + n.Organization_Email__c ;
                        
                        if ( String.isNotBlank ( n.Own1TaxID__c ) )
                            lookupString = 'P1!' + n.Own1TaxID__c + '-' + n.Organization_Email__c ;
                        
                        if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                        {
                            map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                            xMap.put ( n.ID, n ) ;
                             
                            LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        }
                        else
                        {
                            if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                                LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                        }
                    }
                }
                
                if ( ( n.Own2PersonID__c == null ) && ( String.isNotBlank ( n.Own2FullName__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    
                    c.AccountId = account ;
                    if ( effYouMap.containsKey ( n.Organization_Email__c ) )
                        c.AccountId = effYouMap.get ( n.Organization_Email__c ).ID ;
                    
                    if ( ( String.isNotBlank ( n.Own2FirstName__c ) ) && ( String.isNotBlank ( n.Own2LastName__c ) ) )
                    {
                        c.FirstName = n.Own2FirstName__c ;
                        c.LastName = n.Own2LastName__c ;
                    }
                    else
                    {
                        c.LastName = n.Own2FullName__c ;
                    }
                    c.email = n.Organization_Email__c ;
                    c.Phone = n.Organization_Phone__c ;
                    
                    if ( String.isNotBlank ( n.Own2Address1__c ) )
                    {
	                    c.MailingStreet = n.Own2Address1__c ;
                        c.MailingCity = n.Own2City__c ;
                        c.MailingState = n.Own2State__c ;
                        c.MailingPostalCode = n.Own2Zip__c ;
                    }
                    else
                    {
	                    c.MailingStreet = n.Organization_Street__c ;
                        c.MailingCity = n.Organization_City__c ;
                        c.MailingState = n.Organization_State__c ;
                        c.MailingPostalCode = n.Organization_Zip_Code__c ;
                    }
                    
                    if ( String.isNotBlank ( n.Own2TaxID__c ) )
	                    c.TaxID__c = n.Own2TaxID__c ;
                    
	                String lookupString = 'P2' + n.Own2FullName__c + '-' + n.Organization_Email__c ;
                    
                    if ( ( String.isNotBlank ( n.Own2FirstName__c ) ) && ( String.isNotBlank ( n.Own2LastName__c ) ) )
                        lookupString = 'P2' + n.Own2FirstName__c + '-' + n.Own2LastName__c + '-' + n.Organization_Email__c ;
                    
                    if ( String.isNotBlank ( n.Own2TaxID__c ) )
                        lookupString = 'P2' + n.Own2TaxID__c + '-' + n.Organization_Email__c ;
                    
                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
                
                if ( ( n.Own3PersonID__c == null ) && ( String.isNotBlank ( n.Own3FullName__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    
                    c.AccountId = account ;
                    if ( effYouMap.containsKey ( n.Organization_Email__c ) )
                        c.AccountId = effYouMap.get ( n.Organization_Email__c ).ID ;
                    
                    if ( ( String.isNotBlank ( n.Own3FirstName__c ) ) && ( String.isNotBlank ( n.Own3LastName__c ) ) )
                    {
                        c.FirstName = n.Own3FirstName__c ;
                        c.LastName = n.Own3LastName__c ;
                    }
                    else
                    {
                        c.LastName = n.Own3FullName__c ;
                    }
                    c.email = n.Organization_Email__c ;
                    c.Phone = n.Organization_Phone__c ;
                    
                    if ( String.isNotBlank ( n.Own3Address1__c ) )
                    {
	                    c.MailingStreet = n.Own3Address1__c ;
                        c.MailingCity = n.Own3City__c ;
                        c.MailingState = n.Own3State__c ;
                        c.MailingPostalCode = n.Own3Zip__c ;
                    }
                    else
                    {
	                    c.MailingStreet = n.Organization_Street__c ;
                        c.MailingCity = n.Organization_City__c ;
                        c.MailingState = n.Organization_State__c ;
                        c.MailingPostalCode = n.Organization_Zip_Code__c ;
                    }
                    
                    if ( String.isNotBlank ( n.Own3TaxID__c ) )
	                    c.TaxID__c = n.Own3TaxID__c ;
                    
	                String lookupString = 'P3' + n.Own3FullName__c + '-' + n.Organization_Email__c ;
                    
                    if ( ( String.isNotBlank ( n.Own3FirstName__c ) ) && ( String.isNotBlank ( n.Own3LastName__c ) ) )
                        lookupString = 'P3' + n.Own3FirstName__c + '-' + n.Own3LastName__c + '-' + n.Organization_Email__c ;
                    
                    if ( String.isNotBlank ( n.Own3TaxID__c ) )
                        lookupString = 'P3' + n.Own3TaxID__c + '-' + n.Organization_Email__c ;
                    
                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
                
                if ( ( n.Own4PersonID__c == null ) && ( String.isNotBlank ( n.Own4FullName__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    
                    c.AccountId = account ;
                    if ( effYouMap.containsKey ( n.Organization_Email__c ) )
                        c.AccountId = effYouMap.get ( n.Organization_Email__c ).ID ;
                    
                    if ( ( String.isNotBlank ( n.Own4FirstName__c ) ) && ( String.isNotBlank ( n.Own4LastName__c ) ) )
                    {
                        c.FirstName = n.Own4FirstName__c ;
                        c.LastName = n.Own4LastName__c ;
                    }
                    else
                    {
                        c.LastName = n.Own4FullName__c ;
                    }
                    c.email = n.Organization_Email__c ;
                    c.Phone = n.Organization_Phone__c ;
                    
                    if ( String.isNotBlank ( n.Own4Address1__c ) )
                    {
	                    c.MailingStreet = n.Own4Address1__c ;
                        c.MailingCity = n.Own4City__c ;
                        c.MailingState = n.Own4State__c ;
                        c.MailingPostalCode = n.Own4Zip__c ;
                    }
                    else
                    {
	                    c.MailingStreet = n.Organization_Street__c ;
                        c.MailingCity = n.Organization_City__c ;
                        c.MailingState = n.Organization_State__c ;
                        c.MailingPostalCode = n.Organization_Zip_Code__c ;
                    }
                    
                    if ( String.isNotBlank ( n.Own4TaxID__c ) )
	                    c.TaxID__c = n.Own4TaxID__c ;
                    
	                String lookupString = 'P4' + n.Own4FullName__c + '-' + n.Organization_Email__c ;
                    
                    if ( ( String.isNotBlank ( n.Own4FirstName__c ) ) && ( String.isNotBlank ( n.Own4LastName__c ) ) )
                        lookupString = 'P4' + n.Own4FirstName__c + '-' + n.Own4LastName__c + '-' + n.Organization_Email__c ;
                    
                    if ( String.isNotBlank ( n.Own4TaxID__c ) )
                        lookupString = 'P4' + n.Own4TaxID__c + '-' + n.Organization_Email__c ;
                    
                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
                
                if ( ( n.Own5PersonID__c == null ) && ( String.isNotBlank ( n.Own5FullName__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = LoanApplicationHelper.getProspectRT () ;
                    
                    c.AccountId = account ;
                    if ( effYouMap.containsKey ( n.Organization_Email__c ) )
                        c.AccountId = effYouMap.get ( n.Organization_Email__c ).ID ;
                    
                    if ( ( String.isNotBlank ( n.Own5FirstName__c ) ) && ( String.isNotBlank ( n.Own5LastName__c ) ) )
                    {
                        c.FirstName = n.Own5FirstName__c ;
                        c.LastName = n.Own5LastName__c ;
                    }
                    else
                    {
                        c.LastName = n.Own5FullName__c ;
                    }
                    c.email = n.Organization_Email__c ;
                    c.Phone = n.Organization_Phone__c ;
                    
                    if ( String.isNotBlank ( n.Own5Address1__c ) )
                    {
	                    c.MailingStreet = n.Own5Address1__c ;
                        c.MailingCity = n.Own5City__c ;
                        c.MailingState = n.Own5State__c ;
                        c.MailingPostalCode = n.Own5Zip__c ;
                    }
                    else
                    {
	                    c.MailingStreet = n.Organization_Street__c ;
                        c.MailingCity = n.Organization_City__c ;
                        c.MailingState = n.Organization_State__c ;
                        c.MailingPostalCode = n.Organization_Zip_Code__c ;
                    }
                    
                    if ( String.isNotBlank ( n.Own5TaxID__c ) )
	                    c.TaxID__c = n.Own5TaxID__c ;
                    
	                String lookupString = 'P5' + n.Own5FullName__c + '-' + n.Organization_Email__c ;
                    
                    if ( ( String.isNotBlank ( n.Own5FirstName__c ) ) && ( String.isNotBlank ( n.Own5LastName__c ) ) )
                        lookupString = 'P5' + n.Own5FirstName__c + '-' + n.Own5LastName__c + '-' + n.Organization_Email__c ;
                    
                    if ( String.isNotBlank ( n.Own5TaxID__c ) )
                        lookupString = 'P5' + n.Own5TaxID__c + '-' + n.Organization_Email__c ;
                    
                    if ( ! LoanApplicationHelper.laCMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                        LoanApplicationHelper.laCMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laCMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laCMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
            }
            
            if ( ( n.Opportunity__c == null ) &&
                 ( ( String.isNotBlank ( n.Loan_Status__c ) ) && ( n.Loan_Status__c.equalsIgnoreCase ( 'Open' ) ) ) )
            {
                if ( ( String.isNotBlank ( n.Borrower_First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Email__c ) ) &&
                     ( String.isNotBlank ( n.Borrower_Computed_Phone__c ) ) &&
                     ( String.isNotBlank ( n.Loan_Purpose__c ) ) )
                {
                    oneOpportunity__c oo = new oneOpportunity__c () ;
                    oo.Loan_Application_ID__c = n.Mortgagebot_Web_ID__c ;
                    oo.Loan_Application__c = n.ID ;
                    oo.RecordTypeId = LoanApplicationHelper.getLoanOpportunityRT () ;
                    oo.First_Name__c = n.Borrower_First_Name__c + ' ' + n.Borrower_Last_Name__c ;
                    oo.Email__c = n.Borrower_Email__c ;
                    oo.Purchase_or_Refinance__c = n.Loan_Purpose__c ;
                    oo.Interested_in_Product__c = 'Single Family Home' ;
                    oo.Status__c = 'Open' ;
                     
                    if ( n.Borrower_Contact__c != null )
	                    oo.Opportunity_Contact__c = n.Borrower_Contact__c ;
                     
                    //  One of these needs to be filled in
                    if ( String.isNotBlank ( n.Borrower_Home_Phone__c ) )
                        oo.Home_Phone__c = n.Borrower_Home_Phone__c ;
                    if ( String.isNotBlank ( n.Borrower_Phone__c ) )
                        oo.Work_Phone__c = n.Borrower_Phone__c ;
                    if ( String.isNotBlank ( n.Borrower_Mobile__c ) )
                        oo.Mobile_Phone__c = n.Borrower_Mobile__c ;
                     
                    //  This CANNOT be Blank!
                    if ( String.isBlank ( oo.Home_Phone__c ) )
                    {
                        if ( String.isNotBlank ( n.Borrower_Mobile__c ) )
                            oo.Home_Phone__c = n.Borrower_Mobile__c ;
                         
                        else if ( String.isNotBlank ( n.Borrower_Phone__c ) )
                            oo.Home_Phone__c = n.Borrower_Phone__c ;
                    }
                     
	                String lookupString = '1' + n.Borrower_First_Name__c + ' ' + n.Borrower_Last_Name__c + '-' + n.Borrower_Email__c ;

                    if ( ! LoanApplicationHelper.laOOMap.containsKey ( lookupString ) )
                    {
                        map<ID,Loan_Application__c> xMap = new Map<ID,Loan_Application__c> () ;
                        xMap.put ( n.ID, n ) ;
                        
                        LoanApplicationHelper.laOOMap.put ( lookupString, xMap ) ;
                        ooL.add ( oo ) ;
                    }
                    else
                    {
                        if ( ! LoanApplicationHelper.laOOMap.get ( lookupString ).containsKey ( n.ID ) )
                            LoanApplicationHelper.laOOMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Create contact records
         * ----------------------------------------------------------------------------------- */
        if ( ( cL != null ) && ( cL.size () > 0 ) )
        {
            boolean success = true ;
            try
            {
	            insert cL ;
            }
            catch ( Exception e )
            {
                success = false ;
            }
            
            if ( success )
            {
                for ( Contact c : cL )
                {
                    String lookupString1 = '1' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    String lookupString2 = '2' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    
                    String lookupStringP0 = 'P0' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP0 = 'P0' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP0 = 'P0' + c.TaxID__c + '-' + c.Email ;
                    
                    String lookupStringP1 = 'P1' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP1 = 'P1' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP1 = 'P1' + c.TaxID__c + '-' + c.Email ;
                    
                    String lookupStringP1E = 'P1!' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP1E = 'P1!' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP1E = 'P1!' + c.TaxID__c + '-' + c.Email ;
                    
                    String lookupStringP2 = 'P2' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP2 = 'P2' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP2 = 'P2' + c.TaxID__c + '-' + c.Email ;
                    
                    String lookupStringP3 = 'P3' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP3 = 'P3' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP3 = 'P3' + c.TaxID__c + '-' + c.Email ;
                    
                    String lookupStringP4 = 'P4' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP4 = 'P4' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP4 = 'P4' + c.TaxID__c + '-' + c.Email ;
                    
                    String lookupStringP5 = 'P5' + c.LastName + '-' + c.Email ;
					if ( String.isNotBlank ( c.FirstName ) && ( String.isNotBlank ( c.LastName ) ) ) 
                        lookupStringP5 = 'P5' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                    if ( String.isNotBlank ( c.TaxID__c ) )
                        lookupStringP5 = 'P5' + c.TaxID__c + '-' + c.Email ;
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupString1 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupString1 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Borrower_Contact__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Borrower_Contact__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupString2 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupString2 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Co_Borrower_Contact__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                la.Co_Borrower_Contact__c = c.ID ;
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP0 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP0 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Primary_Person__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Primary_Person__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP1 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP1 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Own1PersonID__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Own1PersonID__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP1E ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP0 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Own1PersonID__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Own1PersonID__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP2 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP2 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Own2PersonID__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Own2PersonID__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP3 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP3 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Own3PersonID__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Own3PersonID__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP4 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP4 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Own4PersonID__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Own4PersonID__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                    
                    if ( LoanApplicationHelper.laCMap.containsKey ( lookupStringP5 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laCMap.get ( lookupStringP5 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Own5PersonID__c = c.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Own5PersonID__c = c.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Create opportunity records
         * ----------------------------------------------------------------------------------- */
        if ( ( ooL != null ) && ( ooL.size () > 0 ) )
        {
            boolean success = true ;
            try
            {
	            insert ooL ;
            }
            catch ( Exception e )
            {
                success = false ;
            }
            
            if ( success )
            {
                for ( oneOpportunity__c oo : ooL )
                {
	                String lookupString1 = '1' + oo.First_Name__c + '-' + oo.Email__c ;
                    
                    if ( LoanApplicationHelper.laOOMap.containsKey ( lookupString1 ) )
                    {
                        map<ID,Loan_Application__c> xMap = LoanApplicationHelper.laOOMap.get ( lookupString1 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Loan_Application__c la = null ;
                            
                            if ( laM.containsKey ( l ) )
                            {
                                la = laM.get ( l ) ;
                                la.Opportunity__c = oo.ID ;
                            }
                            else
                            {
                                la = new Loan_Application__c () ;
                                la.ID = l ;
                                
                                la.Opportunity__c = oo.ID ;
                                
                                laM.put ( la.ID, la ) ;
                            }
                        }
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Update all the applications that need updates
         * ----------------------------------------------------------------------------------- */
        if ( ( laM.keyset ().size () > 0 ) )
        {
            try
            {
                update laM.values () ;
            }
            catch ( exception e )
            {
                // ?
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Check for status changes
         * ----------------------------------------------------------------------------------- */
        map<String, oneOpportunity__c> ooMap = new map<String, oneOpportunity__c> () ;
        
        for ( Loan_Application__c n : Trigger.new )
        {
            if ( Trigger.isInsert )
            {
                if ( n.Opportunity__c != null )
                {
                    if ( ! ooMap.containsKey ( n.Opportunity__c ) )
                    {
                        oneOpportunity__c oo = new oneOpportunity__c () ;
                        oo.ID = n.Opportunity__c ;
                        oo.Loan_Application__c = n.ID ;
                        
                        if ( n.Application_Received__c == true )
                        {
	                        oo.Loan_Application_ID__c = n.Application_Number__c ;
	                        oo.Status__c = 'Application Received' ;
                        }
                        else
                        {
                            oo.Status__c = 'Open' ;
                        }
                        
                        ooMap.put ( n.Opportunity__c, oo ) ;
                    }
                }
            }
            
            if ( Trigger.isUpdate )
            {
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Loan_Application__c o = Trigger.oldMap.get ( n.ID ) ;
                
                if ( n.Opportunity__c != null )
                {
                    if ( ( n.Application_Received__c == true ) && ( o.Application_Received__c == false ) )
                    {
                        if ( ! ooMap.containsKey ( n.Opportunity__c ) )
                        {
                            oneOpportunity__c oo = new oneOpportunity__c () ;
                            oo.ID = n.Opportunity__c ;
                            oo.Loan_Application__c = n.ID ;
                            oo.Loan_Application_ID__c = n.Application_Number__c ;
                            oo.Status__c = 'Application Received' ;
                            
                            ooMap.put ( n.Opportunity__c, oo ) ;
                        }
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Update opportunities
         * ----------------------------------------------------------------------------------- */
        if ( ( ooMap.keyset ().size () > 0 ) )
        {
            list<oneOpportunity__c> oouL = new list<oneOpportunity__c> () ;
            
            for ( String key : ooMap.keySet () )
            {
                oouL.add ( ooMap.get ( key ) ) ;
            }
            
            if ( oouL.size () > 0 )
            {
                try
                {
                    update oouL ;
                }
                catch ( exception e )
                {
                    // ?
                }
            }
        }
    }
}