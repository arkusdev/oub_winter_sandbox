trigger Automated_Email_Trigger on Automated_Email__c (before insert, before update, after update, after insert) {
	List<Automated_Email__c> autoEmails = new List<Automated_Email__c>();
    String rti = Schema.SObjectType.Automated_Email__c.RecordTypeInfosByDeveloperName.get('Push_Notification').RecordTypeId;
    
    // processing done before record is saved
    if ( Trigger.isBefore ) // for update or insert
    {
        //Map<String, String> contactToUsername = new Map<String, String>();
        
        for ( Automated_Email__c ae : Trigger.New )
        {
            if ( ae.RecordTypeId == rti )
            {
                // field validation
                if ( (!ae.Email_Sent__c) && (  ( ae.Status__c == null ) || String.isBlank(ae.Status__c) ) )
                {
                    /* Note: we assume that the merge fields required for the push are stored in corresponding Merge_Var fields:
                        Merge_Var_Name_1__c = 'ddAmount'
                        Merge_Var_Value_1__c is the direct deposit amount
                        Merge_Var_Name_2__c = 'payorName'
                        Merge_Var_Value_2__c is the employer/payor from which the customer/user is receiving their BBPayday direct deposit
                        */ 
                    
                    // change status to 'Processing'
                    ae.Status__c = 'Processing';
                    
                    /*
                    if ( ae.Contact_GUUID__c != null )
                    {
                    	contactToUsername.put( ae.Contact__c, null );
                    }
					*/
                }
            }
        }
        /*
        if ( contactToUsername.values().contains(null) ) // trying to prevent duplicate database query, but we will still have a null in the map if the user does not have an associated username
        {
            try
            {
                // query for usernames
                List<Service__c> servList = [SELECT Contact__c, Alternate_Login_ID__c 
                                             FROM Service__c
                                             WHERE Contact__c IN :contactToUsername.keySet()
                                             AND Status__c = 'Active'
                                             AND Type__c = 'Online Banking'
                                             AND Alternate_Login_ID__c != null
                                             ORDER BY Id ASC]; // the most recently created service record is used to supply the username
                
                for ( Service__c s : servList )
                {
                    contactToUsername.put( s.Contact__c, s.Alternate_Login_ID__c );
                }
            }
            catch ( Exception e )
            {
             	System.debug('Automated_Email_Trigger --- query for usernames --- error: ' + e);   
            }
        }
        */
        
        // updates Merge_Var_Value_3__c to the corresponding user alias, i.e. username for Online Banking,
        // determined by most recently created, active Service record associated with the Contact on the Automated_Email
        /*
        for ( Automated_Email__c ae : Trigger.New )
        {
            if ( ae.RecordTypeId == rti )
            {
                // field validation
                if ( (!ae.Email_Sent__c) && String.isNotBlank(ae.Status__c) && ae.Status__c.equalsIgnoreCase('Processing') ) // because once it hits this part, it will have changed the status to processing already
                {
                    if ( ae.Contact__c != null && ( ae.Merge_Var_Value_3__c == null || String.isBlank(ae.Merge_Var_Value_3__c) ) )
                    {
                        String username = contactToUsername.get( ae.Contact__c );
                        
                        // assign values to Merge_Var_Name_3__c & Merge_Var_Value_3__c
                        if ( username != null && !( String.isBlank(username) ) )
                        {
                            ae.Merge_Var_Name_3__c = 'userAlias';
                            ae.Merge_Var_Value_3__c = username;
                            
                            System.debug('Automated_Email_Trigger --- updated username merge fields');
                        }
                        
                    }
                }
            }
        }
		*/
    }
    
    // processing done after record is saved
	else if ( Trigger.isAfter ) // for update or insert
    {
        for ( Automated_Email__c ae : Trigger.New )
        {
            if ( ae.RecordTypeId == rti )
            {
                Automated_Email__c old; 
                
                if ( Trigger.isUpdate ) // isAfter && isUpdate
                {
                    try
                    {
                        old = Trigger.oldMap.get ( ae.ID ) ; // only available in update & delete
                    }
                    catch ( Exception ex )
                    {
                        System.debug('Automated_Email_Trigger -- getOldError: ' + ex);
                    }
                    
                    if ( old != null ) 
                    {
                        // if it is after an update AND
                        // email is not sent and status changed from blank to 'processing'
                        if ( (!ae.Email_Sent__c) && ( String.isNotBlank(ae.Status__c) ) && ( ae.Status__c.equalsIgnoreCase('Processing') ) && ( ! old.Status__c.equalsIgnoreCase('Processing') ) )
                        {
                            autoEmails.add( ae.clone(true, false, true, true) ); // need to clone b/c records are read only in isAfter context; 2nd param = false means it is making a reference to the original object
                        }
                    }
                }
                else if ( Trigger.isInsert ) // isAfter && isInsert
                {
                    // if it is after an insert (i.e. new record) and email is not sent and status = processing,
                    // which it should be b/c we just set it to that value before it got commited/saved
                    if ( (!ae.Email_Sent__c) && ( String.isNotBlank(ae.Status__c) ) && ( ae.Status__c.equalsIgnoreCase('Processing') ) && ( old == null ) )
                    {
                        autoEmails.add( ae.clone(true, false, true, true) ); 
                    }
                }
            }
        }
    }
    
    if ( autoEmails.size() > 0 )
    {
        // create new instance of Queueable
        BBPaydayPushQueue sendPush = new BBPaydayPushQueue(autoEmails);
        
        // enqueue the job for processing
        ID jobID = System.enqueueJob(sendPush);
    }
}