trigger RetailTaskTrigger on Task (after insert) 
{
	/* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		/* -----------------------------------------------------------------------------------
		 * RETAIL TASK SECTION
		 * ----------------------------------------------------------------------------------- */
		String retailRecordTypeId = OneUnitedUtilities.getRecordTypeId ( 'Retail Task' ) ;
		
		if ( String.isNotBlank ( retailRecordTypeId ) )
		{
			List<Task> retailTaskList = new List<Task> () ;
			
			for ( Task n : Trigger.new )
			{
				if ( ( String.isNotEmpty ( n.recordTypeId ) ) && ( n.RecordTypeId == retailRecordTypeId ) )
				{
					retailTaskList.add ( n ) ;
				}
			}
			
			if ( ( retailTaskList != null ) && ( retailTaskList.size () > 0 ) )
			{
				RetailTaskLogic.handleRetailTaskLogic (retailTaskList ) ;
			}
		}
	}
}