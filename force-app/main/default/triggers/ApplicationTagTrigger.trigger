trigger ApplicationTagTrigger on Application_Tag__c (after insert, after update) 
{
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
		System.debug ( '------------------------------ BEFORE START ------------------------------' ) ;
		
		System.debug ( '------------------------------ BEFORE END ------------------------------' ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		//  -----------------------------------------------------------------------------------
		//  Callouts
		//  -----------------------------------------------------------------------------------
        list<String> calloutTagList = new list<String>();
        
		System.debug ( '------------------------------ AFTER START ------------------------------' ) ;
		
        //  -----------------------------------------------------------------------------------
		//  MAIN LOOP
		//  -----------------------------------------------------------------------------------
		for ( Application_Tag__c n : Trigger.new )
		{
            //  ------------------------------------------------------------------------
            //  On Insert
            //  ------------------------------------------------------------------------
            if ( Trigger.isInsert )
            {
                if ( ( n.Report_to_MinFraud__c == true ) && ( n.Sent_to_MinFraud__c == false ) )
                {
                    calloutTagList.add ( n.ID ) ;
                }
            }
            
            //  ------------------------------------------------------------------------
            //  On update
            //  ------------------------------------------------------------------------
            if ( Trigger.isUpdate )
            {
                Application_Tag__c o = Trigger.oldMap.get ( n.ID ) ; // ?
                
                if ( ( n.Report_to_MinFraud__c == true ) && ( n.Sent_to_MinFraud__c == false ) )
                {
                    calloutTagList.add ( n.ID ) ;
                }
            }    
        }
        
		//  -----------------------------------------------------------------------------------
		//  Callouts
		//  -----------------------------------------------------------------------------------
        List<String> atL = new List<String>();
        
        if ( !System.isQueueable () )
        {
            //  Loop to breakout calls into max allowed
            for ( String appId : calloutTagList )
            {
                atL.add ( appId ) ;
                
                if ( atL.size () == ApplicationTagCalloutQueue.CALLOUT_LIMIT )
                {
                    ApplicationTagCalloutQueue aQ = new ApplicationTagCalloutQueue ( atL );
                    System.enqueueJob ( aQ ) ;
                    atL = new List<String>();
                }
            }
            
            //  Remainder
            if ( atL.size () > 0 )
            {
                ApplicationTagCalloutQueue aQ2 = new ApplicationTagCalloutQueue ( atL ) ;
                System.enqueueJob ( aQ2 ) ;
            }
        }
        
		System.debug ( '------------------------------ AFTER END ------------------------------' ) ;
    }
}