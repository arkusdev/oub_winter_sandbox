trigger TicketTrigger on Ticket__c ( before insert, after insert, before update, after update ) 
{
	/* -----------------------------------------------------------------------------------
	 * Instantiate helper thingys
	 * ----------------------------------------------------------------------------------- */
	TicketTriggerFunctions.uLMap = new map<String,List<String>> () ;
    TicketTriggerFunctions.esMapMap = new map<String,Map<String, String>> () ;
    TicketTriggerFunctions.activeUserMap = new Map<String,User> () ;
	 
    //  -----------------------------------------------------------------------------------
    //  Bleah
    //  -----------------------------------------------------------------------------------
    list<Automated_Email__c> aeL = new list<Automated_Email__c> () ;
    
	/* -----------------------------------------------------------------------------------
	 * Record Types & ETC
	 * ----------------------------------------------------------------------------------- */
	List<String> groupList = new List<String> () ;
	List<String> rtNames = new List<String> () ;
	
	rtNames.add ( 'Prospect' ) ;
	
    //  Genric
    rtNames.add ( 'Support' ) ;
    
	//  Community Room
	rtNames.add ( 'Community Room' ) ;
	groupList.add ( 'Community Room Request' ) ;
	
	//  Social Media Alerts
	rtNames.add ( 'Social Media Alert' ) ;
	groupList.add ( 'Social Media Alert' ) ;
    groupList.add ( 'BBB Complaint' ) ;
    
	//  Credit Card
	rtNames.add ( 'Credit Card Refund' ) ;
	groupList.add ( 'Credit Card Refund Processing' ) ;
	
	//  Credit Card Batch
	rtNames.add ( 'CS Batch File' ) ;
	groupList.add ( 'Credit Card Batch Processing' ) ;
	
	//  Public Website Comment
	rtNames.add ( 'Public Website Comment' ) ;
	groupList.add ( 'Public Website Comment' ) ;
 
	//  Content Request
	rtNames.add ( 'Content Request' ) ;
	groupList.add ( 'Social Team Request' ) ;
 
 	//  Loan something or other
 	rtNames.add ( 'Loan Financials' ) ;
    
    //  Invoices
    rtNames.add ( 'Accounts Payable' ) ;
    groupList.add ( 'Accounts Payable' ) ;
 
    //  Piss Piss Piss
    rtNames.add ( 'PPP' ) ;
    groupList.add ( 'PPP Service' ) ;
    
    //  Generic
    rtNames.add ( 'Document Request' ) ;
    groupList.add ( 'Document Request' ) ;

    //  Generic
    rtNames.add ( 'Survey Email' ) ;
 
 	//  Load to Map - DML Limits!
 	Map<String,String> groupMap = TicketTriggerFunctions.getGroupMap ( groupList ) ;
 	
	String communityRoomGroupId = groupMap.get ( 'Community Room Request' ) ;
	String socialMediaAlertGroupId = groupMap.get ( 'Social Media Alert' ) ;
	String creditCardGroupId = groupMap.get ( 'Credit Card Refund Processing' ) ;
	String creditCardBatchGroupId = groupMap.get ( 'Credit Card Batch Processing' ) ;
	String websiteCommentGroupId = groupMap.get ( 'Public Website Comment' ) ;
    String bbbComplaintGroupId = groupMap.get ( 'BBB Complaint' ) ;    
    String accountsPayableGroupId = groupMap.get ( 'Accounts Payable' ) ;
    String socialTeamGroupId = groupMap.get ( 'Social Team Request' ) ;
    String pppGroupId = groupMap.get ( 'PPP Service' ) ;
    String documentRequestGroupId = groupMap.get ( 'Document Request' ) ;

	//  Load to list for parsing - DML Limits!
	List<recordType> rtL = TicketTriggerFunctions.getRecordTypeList ( rtNames ) ;
 	
    String supportRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Support', 'Ticket__c' ) ; 
    
	String prospectRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Prospect', 'Contact' ) ; 
	String communityRoomRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Community Room', 'Ticket__c' ) ;
	String socialMediaAlertRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Social Media Alert', 'Ticket__c' ) ;
	String creditCardRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Credit Card Refund', 'Ticket__c' ) ;
	String creditCardBatchRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'CS Batch File', 'Ticket__c' ) ;
	String websiteCommentRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Public Website Comment', 'Ticket__c' ) ;
	String loanFinancialsRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Loan Financials', 'Ticket__c' ) ;
	String accountsPayableRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Accounts Payable', 'Ticket__c' ) ;
	String contentRequestRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Content Request', 'Ticket__c' ) ;
 	String pppRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'PPP', 'Ticket__c' ) ;
	String documentRequestRecordTypeId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Document Request', 'Ticket__c' ) ;
    String surveyEmailRecordTyperId = OneUnitedUtilities.getRecordTypeIdFromList ( rtL, 'Survey Email', 'Automated_Email__c' ) ;
    
    //  Users
	TicketTriggerFunctions.activeUserMap = OneUnitedUtilities.getActiveUsers () ;    
    
	/* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
		System.debug ( '------------------------------ BEFORE START ------------------------------' ) ;
		
		/* -----------------------------------------------------------------------------------
		 * LOOP
		 * ----------------------------------------------------------------------------------- */
		
        Set<String> c1x = new Set<String>();
        set<String> c2x = new Set<String> () ;
        
        for(Ticket__c n : Trigger.new)
        {
            if(String.isNotBlank(n.Email_Address__c) && OneUnitedUtilities.isValidEmail(n.Email_Address__c))
            {
                if ( n.Contact__c == null )
                {// Ticket__c.Contact__c is actually a contact Id not a Contact object
                    c1x.add(n.Email_Address__c); // Add all email addresses submitting tickets w/o an associated contact
                }
                
                if ( String.isNotBlank ( n.Email_To_Address__c ) && ( n.Email_To_Address__c.equalsIgnoreCase ( 'pppservice@oneunited.com' ) ) )
                {
                    c2x.add ( n.Email_Address__c ) ;
                }
            }
        }

        list<Contact> c1L = null ;
        
        try
        {
            c1L =[
            	SELECT ID, Email
            	FROM Contact
            	WHERE Email IN :c1x
            	ORDER BY Contact.Created_Date__c
        	] ;// Push in this info for contacts associated with the sender address
	        // Order the list by oldest contact with email address (figure out how to fine tune the filter to age)
        }
        catch ( DMLException e )
        {
            c1L = null ;
        }
        
        map<String,ID> c1M = new map<String,ID> () ;
        
        if ( ( c1L != null ) && ( c1L.size () > 0  ) )
        {
            for(Contact c : c1L)
            {// Manoeuvre the contacts into the map and key them to their address
                if(! c1M.containsKey(c.Email))
                    c1M.put(c.Email, c.Id);
            }
        }
        
        list<Loan_Application__c> laL = null ;
        
        try
        {
            laL = [
            	SELECT ID, Organization_Email__c
            	FROM Loan_Application__c
            	WHERE Organization_Email__c IN :c2x
                ORDER BY ID DESC
        	] ;
        }
        catch ( DMLException e )
        {
            laL = null ;
        }
        
        map<String,ID> laM = new map<String,ID> () ;
        
        if ( ( laL != null ) && ( laL.size () > 0  ) )
        {
            for ( Loan_Application__c la : laL )
            {
                if ( ! laM.containsKey ( la.Organization_Email__c ) )
                    laM.put ( la.Organization_Email__c, la.Id ) ;
            }
        }
        
        //  ------------------------------------------------------------------------
        //  Main loop
        //  ------------------------------------------------------------------------
        for ( Ticket__c n : Trigger.new )
		{
            //  ------------------------------------------------------------------------
			//  Always, lookups
			//  ------------------------------------------------------------------------
            if ( String.isNotBlank ( n.Email_Address__c ) && OneUnitedUtilities.isValidEmail ( n.Email_Address__c ) )
            {
                if ( n.Contact__c == null )
                {
                    if(c1M.containsKey(n.Email_Address__c))
                    {// If the key fits, take the contact ID out of the map and shove it into the ticket
                        n.Contact__c = c1M.get(n.Email_Address__c);
                    }
                }
                
                if ( ( n.Loan_Application__c == null ) && ( String.isNotBlank ( n.Email_To_Address__c ) && ( ( n.Email_To_Address__c.equalsIgnoreCase ( 'pppservice@oneunited.com' ) ) || ( n.Email_To_Address__c.equalsIgnoreCase ( 'ppp@oneunited.com' ) ) ) ) )
                {
                    if ( laM.containsKey ( n.Email_Address__c ) )
                        n.Loan_Application__c = laM.get ( n.Email_Address__c ) ;
                }
            }
            
            //  ------------------------------------------------------------------------
			//  Always, record types
			//  ------------------------------------------------------------------------
			if ( n.RecordTypeId == supportRecordTypeId )
            {
                //  ------------------------------------------------------------------------
                //  PPP Conversion
                //  ------------------------------------------------------------------------
                if ( String.isNotBlank ( n.Email_To_Address__c ) && ( ( n.Email_To_Address__c.equalsIgnoreCase ( 'pppservice@oneunited.com' ) ) || ( n.Email_To_Address__c.equalsIgnoreCase ( 'ppp@oneunited.com' ) ) ) )
                {
                    n.RecordTypeId = pppRecordTypeId ;
                }
            }

            //  ------------------------------------------------------------------------
			//  On Insert
			//  ------------------------------------------------------------------------
			if ( Trigger.isInsert )
			{
				//  Reassign incoming Community Room record types to community room queue
				if ( n.RecordTypeId == communityRoomRecordTypeId )
				{
					//  Set the owner to the group
					n.OwnerId = communityRoomGroupId ;
				}
				
				//  Reassign incoming Social Media Alert record types to Social Media Alert queue
				else if ( n.RecordTypeId == socialMediaAlertRecordTypeId )
				{
                    if ( String.isNotBlank ( n.Social_Site__c) && ( n.Social_Site__c.equalsIgnoreCase ( 'BBB' ) ) )
                    {
                        //  Set the owner to the group
                        n.OwnerId = bbbComplaintGroupId ;
                    }
                    else
                    {
                        //  Set the owner to the group
                        n.OwnerId = socialMediaAlertGroupId ;
                    }
				}
				
				//  Reassign incoming Credit Card record types to Credit Card queue
				else if ( n.RecordTypeId == creditCardRecordTypeId )
				{
					//  Set the owner to the group
					n.OwnerId = creditCardGroupId ;
				}
				
				//  Reassign incoming Credit Card Batch record types to Credit Card batch queue
				else if ( n.RecordTypeId == creditCardBatchRecordTypeId )
				{
					//  Set the owner to the group
					n.OwnerId = creditCardBatchGroupId ;
				}
				
				//  Reassign incoming Public Website Comment record types to Public Website Comment queue
				else if ( n.RecordTypeId == websiteCommentRecordTypeId )
				{
					//  Set the owner to the group
					n.OwnerId = websiteCommentGroupId ;
				}
                
                //  Incoming content requests go to the social team
                else if ( n.RecordTypeId == contentRequestRecordTypeId )
                {
                    n.OwnerId = socialTeamGroupId ;
                }
                
                ///  PPP Program requests go to the ... PPP group
                else if ( n.RecordTypeId == pppRecordTypeId )
                {
                    n.OwnerId = pppGroupId ;
                }
                
                ///  Document Request
                else if ( n.RecordTypeId == documentRequestRecordTypeId )
                {
                    n.OwnerId = documentRequestGroupId ;
                }
			}
            
            //  ------------------------------------------------------------------------
			//  On Update
			//  ------------------------------------------------------------------------
			if ( Trigger.isUpdate )
			{
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Ticket__c o = Trigger.oldMap.get ( n.ID ) ;
                
                //  --------------------------------------------------------------
                //  Switch ownership to accounts payable when approved
                //  --------------------------------------------------------------
                if ( n.RecordTypeId == accountsPayableRecordTypeId )
                {
                    if ( ( n.amount__c != null ) && ( n.amount__c > 0.0 ) )
                    {
                        if ( ( String.isBlank ( o.Status__c ) || 
                             ( ! o.Status__c.equalsIgnoreCase ( 'approved' ) ) ) &&
                             ( String.isNotBlank ( n.Status__c ) &&
                             ( n.Status__c.equalsIgnoreCase ( 'approved' ) ) ) )
                        {
                            n.OwnerId = accountsPayableGroupId ;
                        }
                    }
                }
            }
                        
            if ( ( n.recordTypeId == documentRequestRecordTypeId ) && ( String.isBlank ( n.Upload_Attachment_Key__c ) ) )
                n.Upload_Attachment_Key__c = one_utils.getRandomString ( 30 ) ;
		}
		
		System.debug ( '------------------------------ BEFORE END ------------------------------' ) ;
	}
	
	/* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		System.debug ( '------------------------------ AFTER START ------------------------------' ) ;
		
		/* -----------------------------------------------------------------------------------
		 * Setup for chatter follows
		 * ----------------------------------------------------------------------------------- */
		List<EntitySubscription> esL = new List<EntitySubscription> () ; 
		
        /* -----------------------------------------------------------------------------------
		 * MAP o doom, setup
		 * ----------------------------------------------------------------------------------- */
        List<String> xL = new list<String> () ;
		for ( Ticket__c n : Trigger.new )
		{
            if ( ( n.RecordTypeId == communityRoomRecordTypeId ) || 
                ( n.RecordTypeId == socialMediaAlertRecordTypeId ) ||
                ( n.RecordTypeId == creditCardRecordTypeId ) ||
                ( n.RecordTypeId == websiteCommentRecordTypeId ) 
               )
            {
                if ( OneUnitedUtilities.isUser ( n.OwnerId ) )
	                xL.add ( n.ID ) ;
            }
        }
        
        //  Load map
        map<String,Map<String,String>> esMap ;
        if ( ( xL != null ) && ( xL.size () > 0 ) )
            esMap = OneUnitedUtilities.getCurrentSubscribers ( xL ) ;
        
        /* -----------------------------------------------------------------------------------
		 * LOOP
		 * ----------------------------------------------------------------------------------- */
		for ( Ticket__c n : Trigger.new )
		{
			//  ------------------------------------------------------------------------
			//  On Insert
			//  ------------------------------------------------------------------------
			if ( Trigger.isInsert )
			{
				//  Chatter follow, various record types
				if ( ( n.RecordTypeId == communityRoomRecordTypeId ) || 
				     ( n.RecordTypeId == socialMediaAlertRecordTypeId ) ||
				     ( n.RecordTypeId == creditCardRecordTypeId ) ||
				     ( n.RecordTypeId == websiteCommentRecordTypeId ) 
				   )
				{
					System.debug ( 'New: ID: ' + n.Id ) ;
					System.debug ( 'New: OW: ' + n.OwnerId ) ;
					System.debug ( 'New: CR: ' + n.CreatedById ) ;
					System.debug ( 'New: RT: ' + n.RecordTypeId ) ;
					
					//  Assigning chatter follow to object created.  Since insert, assuming no prior follow.
					String y = n.CreatedById ;
					if ( ( ! OneUnitedUtilities.isGuestUser ( y ) ) && ( OneUnitedUtilities.isUser ( y ) ) )
					{
						EntitySubscription es = new EntitySubscription () ;
						es.ParentId = n.Id ;
						es.SubscriberId = n.CreatedById ;
						
						esL.add ( es ) ; 
					}
					
					String z = n.OwnerId ;
					if ( OneUnitedUtilities.isGroup ( z ) )
					{
                        List<String> userIdList = TicketTriggerFunctions.getUserIdsFromGroupId ( z ) ;
                        
						if ( ( userIdList != null ) && ( userIdList.size () > 0 ) )
						{
							for ( String userId : userIdList )
							{
								if ( ( userId != y ) && ( TicketTriggerFunctions.activeUserMap.containsKey ( userId ) ) )
								{
									EntitySubscription es = new EntitySubscription () ;
									es.ParentId = n.Id ;
									es.SubscriberId = userId ;
									
									esL.add ( es ) ;
								}
							}
						}
					}
				}
                
                //  Automated email thing for doc request ticket
                else if ( n.RecordTypeId == documentRequestRecordTypeId )
                {
                    Automated_Email__c ae = new Automated_Email__c () ;
                    ae.RecordTypeId = surveyEmailRecordTyperId ;
                    ae.Template_Name__c = 'Document_Request' ;
                    ae.Ticket__c = n.ID ;
                    ae.Contact_OR_Account__c = 'Contact' ;
                    ae.Is_A_Survey__c = 'N' ;
                    ae.Contact__c = n.Contact__c ; 
                    ae.First_Name__c = n.First_Name__c ;
                    ae.Last_Name__c = n.Last_Name__c ;
                    ae.Email_Address__c = n.Email_Address__c ;
                    ae.Additional_Info_Special_Instructions__c = n.Documents_Request_Special_Instructions__c ;
                    ae.Additional_Information_Required__c = n.Documents_Required__c ;
                    ae.Upload_ID__c = n.Upload_Attachment_Key__c ;
                    ae.Merge_Var_Name_1__c = 'TICKET' ;
                    ae.Merge_Var_Value_1__c = n.ID ;
                    ae.Merge_Var_Name_5__c = 'UPLOAD_ID' ;
                    ae.Merge_Var_Value_5__c = n.Upload_Attachment_Key__c ;
                    
                    aeL.add ( ae )  ;
                }
			}
			
			//  ------------------------------------------------------------------------
			//  On update
			//  ------------------------------------------------------------------------
			if ( Trigger.isUpdate )
			{
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Ticket__c o = Trigger.oldMap.get ( n.ID ) ;
            
                //  --------------------------------------------------------------
				//  Check the record type - COMMUNITY ROOM and Social Media Alerts
                //  --------------------------------------------------------------
				if ( ( n.RecordTypeId == communityRoomRecordTypeId ) || 
				     ( n.RecordTypeId == socialMediaAlertRecordTypeId ) ||
				     ( n.RecordTypeId == creditCardRecordTypeId ) ||
				     ( n.RecordTypeId == websiteCommentRecordTypeId ) 
				   )
				{
					System.debug ( 'Old: ID: ' + o.Id ) ;
					System.debug ( 'Old: OW: ' + o.OwnerId ) ;
					
					System.debug ( 'New: ID: ' + n.Id ) ;
					System.debug ( 'New: OW: ' + n.OwnerId ) ;
					
					String newId = n.OwnerId ;
					String oldId = o.OwnerId ;
					
					//  Check the old and new users
					if ( ( ! OneUnitedUtilities.isGuestUser ( newId ) ) && 
					     ( OneUnitedUtilities.isUser ( newId ) ) && 
						 ( ! OneUnitedUtilities.isGuestUser ( oldId ) ) &&
					     ( newId != oldId ) )
					{
						if ( ( esMap != null ) && ( esMap.containsKey ( n.ID ) ) )
						{
                            Map<String,String> xMap = esMap.get ( n.ID ) ;
                            
                            if ( ! xMap.containsKey ( newID ) )
                            {
                                System.debug ( 'ADDING - old and new user mismatch' ) ;
                                EntitySubscription es = new EntitySubscription () ;
                                es.ParentId = n.Id ;
                                es.SubscriberId = n.OwnerId ;
                                
                                esL.add ( es ) ;
                            }
						}
						else
						{
							System.debug ( 'Already subscribed!' );
						}
					}
					
					//  Check to see if the new owner is a group/queue
					if ( OneUnitedUtilities.isGroup ( newId ) )
					{
                        List<String> userIdList = TicketTriggerFunctions.getUserIdsFromGroupId ( newId ) ;

                        if ( ( userIdList != null ) && ( userIdList.size () > 0 ) )
						{
							for ( String userId : userIdList )
							{
                                if ( ( esMap != null ) && ( esMap.containsKey ( n.ID ) ) )
                                {
                                    Map<String,String> xMap = esMap.get ( n.ID ) ;
                                    
                                    if ( ( xMap != null ) && ( ! xMap.containsKey ( userId ) ) )
                                    {
                                        if ( TicketTriggerFunctions.activeUserMap.containsKey ( userId ) )
                                        {
                                            EntitySubscription es = new EntitySubscription () ;
                                            es.ParentId = n.Id ;
                                            es.SubscriberId = userId ;
                                            
                                            esL.add ( es ) ;
                                        }
                                    }
                                }
							}
						}
					}
				}
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * Chatter Following crap insertion
		 * ----------------------------------------------------------------------------------- */
        if ( ! test.isRunningTest () )
        {
            if ( ( esL != null ) && ( esL.size () > 0 ) ) 
                insert esL ;
        }
        
		/* -----------------------------------------------------------------------------------
		 * Setup lists for contacts
		 * ----------------------------------------------------------------------------------- */
		List<Ticket__c> ticketNoContactList = new List<Ticket__c> () ;
		List<String> ticketNoContactEmailList = new List<String> () ;
		
		/* -----------------------------------------------------------------------------------
		 * SETUP LOOP, Look for contact
		 * ----------------------------------------------------------------------------------- */
		for ( Ticket__c n : Trigger.new )
		{
			if ( Trigger.isInsert )
			{
				//  Exclude certain ticket types
				if ( ( n.recordTypeId != creditCardRecordTypeId ) &&
				     ( n.recordTypeId != creditCardBatchRecordTypeId ) )
				{
					//  These tickets have no contacts (and no accounts)
					if ( ( n.Contact__c == null ) && 
						 ( n.Account__c == null ) && 
					     ( String.isNotBlank ( n.Email_Address__c ) ) &&
					     ( OneUnitedUtilities.isValidEmail ( n.Email_Address__c ) )
					   )
					{
						ticketNoContactList.add ( n ) ;
						ticketNoContactEmailList.add ( n.Email_Address__c ) ;
					}
				}
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * CONTACT association LOOP
		 * ----------------------------------------------------------------------------------- */
		Map<String,Contact> emailMap = null ;
		
		if ( ticketNoContactEmailList.size () > 0 )
		{
			emailMap = OneUnitedUtilities.lookupContactMap ( ticketNoContactEmailList ) ;
		}
		
		List<Ticket__c> ticketContactList = new List<Ticket__c> () ;
		
		if ( ticketNoContactList.size () > 0 )
		{
			for ( Ticket__c t : ticketNoContactList )
			{
				if  ( ( emailMap != null ) && ( emailMap.containsKey ( t.Email_Address__c ) ) )
				{
					Contact c = emailMap.get ( t.Email_Address__c  ) ;
					System.debug ( 'Found Contact! :: ' + c.Id ) ;
					
					Ticket__c nT = new Ticket__c () ;
					nT.id = t.Id ;
					nT.Contact__c = c.Id ;
					
					if ( String.isBlank ( nT.State__c ) )
						nT.State__c = c.MailingState ;
					
					ticketContactList.add ( nT ) ;
				}
				else
				{
					if ( ( t.RecordTypeId != websiteCommentRecordTypeId ) && ( t.Last_Name__c != null ) )
					{
						System.debug ( 'No Contact found, creating' ) ;
						
						//  No contact found, create prospect in system
						Contact c = new Contact () ;
						c.FirstName = t.First_Name__c ;
						c.LastName = t.Last_Name__c ;
						c.Email = t.Email_Address__c ;
						c.RecordTypeId = prospectRecordTypeId ;
						c.Phone = t.Phone__c ;
						c.MailingStreet = t.Street_Address__c ;
						c.MailingCity = t.City__c ;
						c.MailingState = t.State__c ;
						c.MailingPostalCode = t.Zip_Code__c ;
						
						if ( String.isNotBlank ( t.Account__c ) )
							c.AccountId = t.Account__c ;
						
						// WARNING - DML CAN GO SPLAT HERE!
						insert c ;
							
						//  Associate prospect to ticket
						Ticket__c nT = new Ticket__c () ;
						nT.id = t.Id ;
						nT.Contact__c = c.Id ;
						
						ticketContactList.add ( nT ) ;
					}
					else
					{
						system.debug ( '>> Website comment, not creating contact' ) ;
					}
				}
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * Update tickets with associated contacts
		 * ----------------------------------------------------------------------------------- */
		if ( ticketContactList.size () > 0 )
		{
			update ticketContactList ;
		}

		System.debug ( '------------------------------ STATE CHECK ------------------------------' ) ;
		
		/* -----------------------------------------------------------------------------------
		 * Setup lists for states
		 * ----------------------------------------------------------------------------------- */
		List<Ticket__c> ticketNoState = new List<Ticket__c> () ;
		List<String> ticketNoStateContact = new List<String> () ;
		List<String> ticketNoStateAccount = new List<String> () ;
		
		/* -----------------------------------------------------------------------------------
		 * SETUP LOOP, Look for lack of a state
		 * ----------------------------------------------------------------------------------- */
		for ( Ticket__c n : Trigger.new )
		{
			if ( Trigger.isInsert )
			{
				//  Exclude all credit card related tickets
				if ( ( n.recordTypeId != creditCardRecordTypeId ) &&
				     ( n.recordTypeId != creditCardBatchRecordTypeId ) )
				{
					if ( ( String.isBlank ( n.State__c ) ) &&
					     ( ( n.Contact__c != null ) ||
					       ( n.Account__c != null ) 
					     )
					   )
				    {
					   	ticketNoState.add ( n ) ;
					   	
					   	if ( n.Contact__c != null )
					   		ticketNoStateContact.add ( n.Contact__c ) ;
					   		
					   	if ( n.Account__c != null )
					   		ticketNoStateAccount.add ( n.Account__c ) ;
					}	
				}
			}
		}
		
		Map<ID, Contact> cMap ;
		
		if ( ( ticketNoStateContact != null ) && ( ticketNoStateContact.size () > 0 ) )
		{
			cMap = new Map<ID, Contact> ( [
				SELECT ID, MailingState
				FROM Contact
				WHERE ID IN :ticketNoStateContact
			] ) ;
		}
		
		Map<ID, Account> aMap ;
		
		if ( ( ticketNoStateAccount != null ) && ( ticketNoStateAccount.size () > 0 ) )
		{
			aMap = new Map <ID, Account> ( [
				SELECT ID, BillingState
				FROM Account
				WHERE ID IN :ticketNoStateAccount
			] ) ;
		}
		
		List<Ticket__c> updateTicketList = new List<Ticket__c> () ;
		
		for ( Ticket__c t : ticketNoState )
		{
			//  If contact found, copy state from contact
			if ( ( t.Contact__c != null ) && ( cMap.containsKey ( t.Contact__c ) ) )
			{
				Contact c = cMap.get ( t.Contact__c ) ;
				
				if ( c.MailingState != null )
				{
					Ticket__c nT = new Ticket__c () ;
					nT.id = t.Id ;
					nT.State__c = c.MailingState ;
					
					updateTicketList.add ( nT ) ;
				}
			}
			
			//  If account found, copy state from account
			if ( ( t.Account__c != null ) && ( aMap.containsKey ( t.Account__c ) ) )
			{
				Account a = aMap.get ( t.Account__c ) ;
				
				if ( a.BillingState != null )
				{
					Ticket__c nT = new Ticket__c () ;
					nT.id = t.Id ;
					nT.State__c = a.BillingState ;
					
					updateTicketList.add ( nT ) ;
				}
			}
		}
		
		/* -----------------------------------------------------------------------------------
		 * Update tickets with associated states
		 * ----------------------------------------------------------------------------------- */
		if ( ticketContactList.size () > 0 )
		{
			update ticketContactList ;
		}
		
		/* -----------------------------------------------------------------------------------
		 * Loan Financials 
		 * ----------------------------------------------------------------------------------- */
		map<ID,Ticket__c> tMap = new map<ID,Ticket__c> () ;
		
		System.debug ( '------------------------------ Loan Financials ------------------------------' ) ;
		
		for ( Ticket__c n : Trigger.new )
		{
			if ( Trigger.isUpdate )
			{
				if ( n.recordTypeId == loanFinancialsRecordTypeId )
				{
					Ticket__c o = Trigger.oldMap.get ( n.ID ) ;
					
					if ( ( ! o.Status__c.equalsIgnoreCase ( 'Accepted' ) ) && ( n.Status__c.equalsIgnoreCase ( 'Accepted' ) ) )
					{
						if ( n.Financial_Account__c != null )
						{
							System.debug ( 'Found loan financials liked to account!' ) ;
						
							tMap.put ( n.ID, n ) ;
						}
					}
				}
			}
		}
		
		list<Attachment> oAL ;
		list<Attachment> nAL = new list<Attachment> () ;
		
		if ( tMap.keyset().size() > 0 )
		{
			System.debug ( 'Checking for attachments' ) ;
			
			list<ID> tL = new list<ID> () ;
			
			for ( String tID : tMap.keyset () )
				tL.add ( tID ) ;
			
			try
			{
				oAL = [
			        SELECT SystemModstamp, ParentId, OwnerId, Name, 
			        LastModifiedDate, LastModifiedById, IsPrivate, IsDeleted, 
			        Id, Description, CreatedDate, CreatedById, 
			        ContentType, BodyLength, Body
			        FROM Attachment
					WHERE ParentID IN :tL
					AND IsDeleted = false
				] ;
			}
			catch ( DMLException e )
			{
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ )
			    {
			        // Process exception here
			        System.debug ( e.getDmlMessage ( i ) ) ;
			    }
	    	}
		}
		
		if ( ( oAL != null ) && ( oAL.size () > 0 ) )
		{
			System.debug ( 'Replacing found attachments' ) ;
			
			for ( Attachment oa : oAL )
			{
				Ticket__c t = tMap.get ( oa.ParentID ) ;
				
				if ( ( t != null ) && ( t.Financial_Account__c != null ) )
				{
					Attachment na = oa.clone ( false, true, true, true ) ;
					na.ParentID = t.Financial_Account__c ;
					
					nAL.add ( na ) ;
				}
			}
			
			//  Try to insert cloned objects
			boolean insertSuccess = true ;
			
			try
			{
				insert nAL ;
			}
			catch ( DMLException e )
			{
				insertSuccess = false ;
			    for ( Integer i = 0 ; i < e.getNumDml () ; i++ )
			    {
			        // Process exception here
			        System.debug ( e.getDmlMessage ( i ) ) ;
			    }
			}
			
			//  Only delete if successful
			if ( insertSuccess == true )
			{
				try
				{
					delete oAL ;
				}
				catch ( DMLException e )
				{
				    for ( Integer i = 0 ; i < e.getNumDml () ; i++ )
				    {
				        // Process exception here
				        System.debug ( e.getDmlMessage ( i ) ) ;
				    }
				}
			}
		}
		 
		//  -----------------------------------------------------------------------------------
		//  Application Transaction Helper setup
		//  -----------------------------------------------------------------------------------
		if ( ApplicationTransactionHelper.refundMap == null )
            ApplicationTransactionHelper.refundMap = new map<String, boolean> () ;
        
		if ( ApplicationTransactionHelper.settleMap == null )
            ApplicationTransactionHelper.settleMap = new map<String, boolean> () ;

		if ( ApplicationTransactionHelper.voidMap == null )
            ApplicationTransactionHelper.voidMap = new map<String, boolean> () ;

		/* -----------------------------------------------------------------------------------
		 * Setup for debit card refunds
		 * ----------------------------------------------------------------------------------- */
        Set<Id> applicationsToRefundTransactions = new Set<Id> () ;
        Map<String,Decimal> amountsMap = new Map<String,Integer>();
        
        /* -----------------------------------------------------------------------------------
		 * NEXT LOOP
		 * ----------------------------------------------------------------------------------- */
		System.debug ( '--------------------------- CHECKING REFUND ---------------------------' ) ;
		for ( Ticket__c n : Trigger.new )
		{
 			if ( Trigger.isUpdate )
			{
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Ticket__c o = Trigger.oldMap.get ( n.ID ) ;
            
                //  --------------------------------------------------------------
                //  Credit card batch
                //  --------------------------------------------------------------
                if ( n.RecordTypeId == creditCardBatchRecordTypeId )
                {
                    if ( ( String.isNotBlank ( n.Batch_Type__c ) ) && 
                         ( n.Batch_Type__c.equalsIgnoreCase ( 'SWM DEBIT' ) ) &&
                         ( n.Is_Batch_Refund__c == true ) )
                    {
                        if ( ( String.isNotBlank ( o.Status__c ) ) && ( ! o.Status__c.equalsIgnoreCase ( 'Processed' ) ) )
                        {
	                        if ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'Processed' ) ) )
                            {
                                list<Ticket__c> tL = null ;
                                
                                try
                                {
                                    tL = [
                                        SELECT ID, Approved_Refund_Amount__c,
                                        Application__r.ID
                                        FROM Ticket__c
                                        WHERE SWM_Batch_Ticket__c = :n.ID
                                    ] ;
                                }
                                catch ( DMLException e )
                                {
                                    // ?
                                }
                                
                                if ( ( tL != null ) && ( tL.size () > 0 ) )
                                {
                                    for ( Ticket__c t: tL )
                                    {
		                                if ( ! ApplicationTransactionHelper.checkDuplicate ( ApplicationTransactionHelper.refundMap, t.Id ) )
                                        {
                                            applicationsToRefundTransactions.add ( t.Application__r.ID ) ;
                                            amountsMap.put ( String.valueOf ( t.Application__r.ID ), Integer.valueOf ( t.Approved_Refund_Amount__c ) ) ;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
       	}
        
        ///////////////////////////////////////////////////////////////////////////
        ////////// CAPTURING TRANSACTION FOR REFUNDED APPLICATIONS /////////////////
        List<Application_Transaction__c> lapptToCapture = [
            SELECT Transaction_Ref_Num__c,Application__c 
            FROM Application_Transaction__c 
            WHERE Application__c IN :applicationsToRefundTransactions 
            AND Status__c = 'Settled'
        ] ;
        
        if(lapptToCapture.size()>0){
            List<String> applicationsIds = new List<String>();
            for(Application_Transaction__c apptC:lapptToCapture){
                applicationsIds.add(apptC.Application__c);
                if(applicationsIds.size()==9){
                    runRefundUSAePayTransactionsQueueable queueableCap = new runRefundUSAePayTransactionsQueueable(applicationsIds,amountsMap);
                    System.enqueueJob(queueableCap);
                    applicationsIds = new List<String>();
                }
            }
            if(applicationsIds.size()>0){
                runRefundUSAePayTransactionsQueueable queueableCap2 = new runRefundUSAePayTransactionsQueueable(applicationsIds,amountsMap);
                System.enqueueJob(queueableCap2);
            }
        }
        ///////////////////////////////////////////////////////////////////////////

		System.debug ( '------------------------------ AFTER END ------------------------------' ) ;
	}
    
    //  -----------------------------------------------------------------------------------
    //  Bleah Bleah
    //  -----------------------------------------------------------------------------------
    if ( ( aeL != null ) && ( aeL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            TicketQueue tq = new TicketQueue ( aeL ) ;
            System.enqueueJob ( tq ) ;
        }
        else
        {
            try
            {
                insert aeL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
}