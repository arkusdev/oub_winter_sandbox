trigger one_Certificate_trigger1 on one_CertificateApplication__c (after insert, after update, before delete) {

	if(one_utils.CertificateApplication_allowExecution == false) return;
	
	if(trigger.isDelete && trigger.isBefore){
		for(one_CertificateApplication__c cer : trigger.old){
			if(cer.Active__c){
				cer.addError('Cannot delete the record when it is "Active".');
			}
		}
	}else{
		if(trigger.isUpdate){
			for(one_CertificateApplication__c cer : trigger.new){
				if(cer.Active__c == false && trigger.oldMap.get(cer.Id).Active__c == true){
					cer.addError('You cannot "Deactivate" the record, you must "Activate" a new record instead.');
				}
			}
		}
		
		Set<Id> activeIds = new Set<Id>();
		for(one_CertificateApplication__c cer : trigger.new){
			if(cer.Active__c){
				activeIds.add(cer.Id);
				break;
			}
		}
		
		if(activeIds.size() > 0){
			List<one_CertificateApplication__c> cers = [SELECT Active__c FROM one_CertificateApplication__c WHERE Active__c = true AND Id NOT IN :activeIds];
			
			for(one_CertificateApplication__c cer : cers){
				cer.Active__c = false;
			}
			one_utils.CertificateApplication_allowExecution = false;
			update cers;
			one_utils.CertificateApplication_allowExecution = true;
		}
	}
	
}