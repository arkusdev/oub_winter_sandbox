trigger HMDAReportExportTrigger on HMDA_Report_Export__c (after insert) 
{
    /* -----------------------------------------------------------------------------------
	 * SETUP
	 * ----------------------------------------------------------------------------------- */
    HMDA_Report__c emptyHMDA = new HMDA_Report__c () ;
    list<Attachment> aL = new list<Attachment> () ;
    list<HMDA_Report_Export_Detail__c> hredL = null ;
    
    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
        /* -----------------------------------------------------------------------------------
         * Export record loop
         * ----------------------------------------------------------------------------------- */
        for ( HMDA_Report_Export__c n : Trigger.new )
        {
            //  Generate where clause depending on what is in the object
            //
            String whereClause = '' ;
            
            if ( ( n.Start_Date__c != null ) && ( n.End_Date__c == null ) )
            {
                Datetime blah1 = Datetime.newInstanceGmt ( n.Start_Date__c, Time.newInstance ( 0,0,0,0 ) ) ;
                whereClause = 'DAY_ONLY(CreatedDate) >= ' + blah1.formatGmt ( 'yyyy-MM-dd' ) + ' ' ;
            }
            else if ( ( n.Start_Date__c == null ) && ( n.End_Date__c != null ) )
            {
                Datetime blah2 = Datetime.newInstanceGmt ( n.End_Date__c, Time.newInstance ( 0,0,0,0 ) ) ;
                whereClause = 'DAY_ONLY(CreatedDate) <= ' + blah2.formatGmt ( 'yyyy-MM-dd' ) + ' ' ;
            }
            else if ( ( n.Start_Date__c != null ) && ( n.End_Date__c != null ) )
            {
                Datetime blah1 = Datetime.newInstanceGmt ( n.Start_Date__c, Time.newInstance ( 0,0,0,0 ) ) ;
                Datetime blah2 = Datetime.newInstanceGmt ( n.End_Date__c, Time.newInstance ( 0,0,0,0 ) ) ;
                whereClause = 'DAY_ONLY(CreatedDate) >= ' + blah1.formatGmt ( 'yyyy-MM-dd' ) + ' AND DAY_ONLY(CreatedDate) <= ' + blah2.formatGmt ( 'yyyy-MM-dd' ) + ' ' ;
            }
            
            if ( String.isNotBlank ( n.Status__c ) )
            {
                if ( String.isNotBlank ( whereClause ) )
                	whereClause += 'AND ' ;
                
                whereClause += 'Status__c = \'' + n.Status__c + '\'' ;
            }
            
            //  Dynamically generate the query
            //  
            String hmdaQuery = DynamicObjectHandler.getFieldQuery ( emptyHMDA, whereClause, null ) ;
            
            System.debug ( '-----------------------------------------------------------------------' ) ;
            System.debug ( hmdaQuery ) ;
            System.debug ( '-----------------------------------------------------------------------' ) ;
            
            //  Load the data
            //  
            list<HMDA_Report__c> hrL = null ;
            
            try
            {
                hrL = Database.query ( hmdaQuery ) ;
            }
            catch ( DMLException e )
            {
                // ?
            }
            
            //  Got results?
            //  
            if ( ( hrL != null ) && ( hrL.size () > 0 ) )
            {
                //  Add attachment
                Attachment a = HMDAReportExportHelper.generateHMDAFile ( hrL, n.ID ) ;
                aL.add ( a ) ;
                
                //  Create linky object
                hredL = new list<HMDA_Report_Export_Detail__c> () ;
                
                for ( HMDA_Report__c hr : hrL )
                {
                    HMDA_Report_Export_Detail__c hred = new HMDA_Report_Export_Detail__c () ;
                    hred.HMDA_Report__c = hr.ID ;
                    hred.HMDA_Report_Export__c = n.ID ;
                    
                    hredL.add ( hred ) ;
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Insert attachments
         * ----------------------------------------------------------------------------------- */
        if ( ( aL != null ) && ( aL.size () > 0 ) ) 
        {
            try
            {
                insert aL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Insert attachments
         * ----------------------------------------------------------------------------------- */
        if ( ( hredL != null ) && ( hredL.size () > 0 ) )
        {
            try
            {
//                insert hredL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
}