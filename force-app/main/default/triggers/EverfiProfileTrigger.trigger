trigger EverfiProfileTrigger on Everfi_Profile__c (after insert, before insert, after update, before update) 
{
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
        /* -----------------------------------------------------------------------------------
         * SETUP
         * ----------------------------------------------------------------------------------- */
		list<String> ceL = new list<String> () ;
        
        /* -----------------------------------------------------------------------------------
         * First lookup, contacts
         * ----------------------------------------------------------------------------------- */
        for ( Everfi_Profile__c n : Trigger.new )
        {
            //  Borrower Contact - Name & Email
            if ( n.Contact__c == null )
            {
          		if ( ( String.isNotBlank ( n.First_Name__c ) ) &&
                     ( String.isNotBlank ( n.Last_Name__c ) ) &&
                     ( String.isNotBlank ( n.Email_Address__c ) ) )
                {
                    if ( OneUnitedUtilities.isValidEmail ( n.Email_Address__c ) )
                    {
                        String lookupString = n.First_Name__c + '-' + n.Last_Name__c + '-' + n.Email_Address__c ;
                        ceL.add ( lookupString ) ;
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Contact - Lookup keys & create map(s)
         * ----------------------------------------------------------------------------------- */
        map<String,Contact> ceMap = null ;
        if ( ( ceL != null ) && ( ceL.size () > 0 ) )
	        ceMap = EverfiProfileHelper.getPossibleContactsByNameEmail ( ceL ) ;
        
        /* -----------------------------------------------------------------------------------
         * Load in found data, first round
         * ----------------------------------------------------------------------------------- */
        if ( ( ceMap != null ) && ( ceMap.keySet().size () > 0 ) )
        {
            for ( Everfi_Profile__c n : Trigger.new )
            {
                //  Contact
                if ( n.Contact__c == null )
                {
                    if ( ( String.isNotBlank ( n.First_Name__c ) ) &&
                         ( String.isNotBlank ( n.Last_Name__c ) ) &&
                         ( String.isNotBlank ( n.Email_Address__c ) ) )
	                {
                        String key = n.First_Name__c + '-' + n.Last_Name__c + '-' + n.Email_Address__c ;
                        if ( ceMap.containsKey ( key ) )
                        {
                            Contact c = ceMap.get ( key ) ;
                            n.Contact__c = c.ID ;
                        }
                    }
                }
            }
        }
    }

    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
        /* -----------------------------------------------------------------------------------
         *  Create the things we didn't find
         * ----------------------------------------------------------------------------------- */
        String account = OneUnitedUtilities.getDefaultAccount ().ID ;
        String recordTypeID = EverfiProfileHelper.getProspectRT () ;
        
		if ( EverfiProfileHelper.efMap == null )
        	EverfiProfileHelper.efMap = new map<String, map<ID,Everfi_Profile__c>> () ;
        
        list<Contact> cL = new list<Contact> () ;
        
        for ( Everfi_Profile__c n : Trigger.new )
        {
            if ( n.Contact__c == null )
            {
                if ( ( String.isNotBlank ( n.First_Name__c ) ) &&
                      ( String.isNotBlank ( n.Last_Name__c ) ) &&
                      ( String.isNotBlank ( n.Email_Address__c ) ) )
                {
                    Contact c = new Contact () ;
                    c.RecordTypeId = recordTypeID ;
                    c.AccountId = account ;

                    c.FirstName = n.First_Name__c ;
                    c.LastName = n.Last_Name__c ;
                    c.Email = n.Email_Address__c ;
                    
	                String lookupString = '1' + n.First_Name__c + '-' + n.Last_Name__c + '-' + n.Email_Address__c ;

                    if ( ! EverfiProfileHelper.efMap.containsKey ( lookupString ) )
                    {
                        map<ID,Everfi_Profile__c> xMap = new Map<ID,Everfi_Profile__c> () ;
                        xMap.put ( n.ID, n ) ;
                         
                	   	EverfiProfileHelper.efMap.put ( lookupString, xMap ) ;
                        cL.add ( c ) ;
                    }
                    else
                    {
                        if ( ! EverfiProfileHelper.efMap.get ( lookupString ).containsKey ( n.ID ) )
                            EverfiProfileHelper.efMap.get ( lookupString ).put ( n.ID, n ) ;
                    }
                }
            }
        }
        
        map<String, Everfi_Profile__c> epM = new map<String, Everfi_Profile__c> () ;
        
        if ( ( cL != null ) && ( cL.size () > 0 ) )
        {
            boolean success = true ;
            try
            {
	            insert cL ;
            }
            catch ( Exception e )
            {
                success = false ;
            }
            
            if ( success )
            {
                for ( Contact c : cL )
                {
                    String lookupString1 = '1' + c.FirstName + '-' + c.LastName + '-' + c.Email ;
                   
                    if ( EverfiProfileHelper.efMap.containsKey ( lookupString1 ) )
                    {
                        map<ID,Everfi_Profile__c> xMap = EverfiProfileHelper.efMap.get ( lookupString1 ) ;
                        
                        for ( ID l : xMap.keyset () )
                        {
                            Everfi_Profile__c ef = null ;
                            ef = new Everfi_Profile__c () ;
                            ef.ID = l ;
                            
                            ef.Contact__c = c.ID ;
                            
                            epM.put ( ef.ID, ef ) ;
                        }
                    }
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         *  Update all the applications that need updates
         * ----------------------------------------------------------------------------------- */
        if ( ( epM.keyset ().size () > 0 ) )
        {
            list<Everfi_Profile__c> epL = new list<Everfi_Profile__c> () ;
            
            for ( String key : epM.keySet () )
            {
                epL.add ( epM.get ( key ) ) ;
            }
            
            if ( epL.size () > 0 )
            {
                try
                {
                    update epL ;
                }
                catch ( exception e )
                {
                    // ?
                }
            }
        }
    }
}