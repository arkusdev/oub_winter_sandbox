trigger ServiceTrigger on Service__c ( before insert, after insert, before update, after update )
{
    String agreementRTID = ServiceHelper.getAgreementRTID () ;
    String onlineservicesRTID = ServiceHelper.getOnlineservicesRTID () ;
    
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
        /* -----------------------------------------------------------------------------------
         * SETUP
         * ----------------------------------------------------------------------------------- */
        List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> reallyLongNameList = new List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
		//  -----------------------------------------------------------------------------------
		//  MAIN LOOP
		//  -----------------------------------------------------------------------------------
		for ( Service__c n : Trigger.new )
		{
            //  -----------------------------------------------------------------------------------
            //  Found?
            //  -----------------------------------------------------------------------------------
            boolean hazAward = false ;

            //  -----------------------------------------------------------------------------------
            //  Card Design
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == agreementRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( ( n.Status__c.equalsIgnoreCase ( 'Issued' ) ) || ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( ( n.Type__c.equalsIgnoreCase ( 'ATM Card' ) ) || (  n.Type__c.equalsIgnoreCase ( 'Debit Card' ) ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Badge' ;
                            reallyLongName.requestName = 'I_Rock_' + n.Plastic_Type__c ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Card Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == agreementRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( ( n.Type__c.equalsIgnoreCase ( 'ATM Card' ) ) || (  n.Type__c.equalsIgnoreCase ( 'Debit Card' ) ) ) )
                    {
                        if ( n.Award_Received_Card__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Badge' ;
                            reallyLongName.requestName = 'Get_the_Card' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            n.Award_Received_Card__c = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Online Banking Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == onlineservicesRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'Online Banking' ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Activity' ;
                            reallyLongName.requestName = 'Online_Banking' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Bill Pay Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == onlineservicesRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'Bill Pay' ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Activity' ;
                            reallyLongName.requestName = 'Free_Bill_Pay' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Mobile App Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == onlineservicesRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'Mobile Banking' ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Badge' ;
                            reallyLongName.requestName = 'Move_Your_Mind' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Text Message Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == onlineservicesRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'Text Message Banking' ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Activity' ;
                            reallyLongName.requestName = 'Notifications' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Tablet Banking Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == onlineservicesRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'Tablet Banking' ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Activity' ;
                            reallyLongName.requestName = 'Tablet_Banking' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Linkline Activation
            //  -----------------------------------------------------------------------------------
            if ( ( n.RecordTypeId != null ) && ( n.recordTypeID == agreementRTID ) )
            {
            	if ( String.isNotBlank ( n.Status__c ) && ( n.Status__c.equalsIgnoreCase ( 'Active' ) ) )
                {
                    if ( ( String.IsNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'Linkline' ) ) )
                    {
                        if ( n.Award_Received__c == false )
                        {
                            AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                            reallyLongName.ContactID = n.Contact__c ;
                            reallyLongName.requestType = 'Activity' ;
                            reallyLongName.requestName = 'LinkLine' ;
                            
                            reallyLongNameList.add ( reallyLongName ) ;
                            
                            hazAward = true ;
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Received?
            //  -----------------------------------------------------------------------------------
			if ( hazAward == true )
                n.Award_Received__c = true ;
            
            //  -----------------------------------------------------------------------------------
            //  END LOOP
            //  -----------------------------------------------------------------------------------
        }
        
        //  -----------------------------------------------------------------------------------
        //  Add advocacy
        //  -----------------------------------------------------------------------------------
        if ( ( reallyLongNameList != null ) && ( reallyLongNameList.size () > 0 ) )
            AdvocateProgramAwardInvocationFunction.advocateProgramAward ( reallyLongNameList ) ;
    }
    
    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
        map<ID,Contact> cMap = new map<ID,Contact> () ;
        map<ID,Financial_Account__c> faMap = new map<ID,Financial_Account__c> () ;
        
        //  Jim map
        map<ID, Contact> cjMap = null ;
        Set<ID> cS = new set<ID> () ;
        for ( Service__c n : Trigger.new )
        {
            if ( n.Contact__c != null )
                cS.add ( n.Contact__c ) ;
        }
        
        try
        {
            cjMap = new map<ID, Contact> ([
                SELECT ID, Active_Debit_Card__c, Active_ATM_Card__c, Mobile_Banking_Active__c, Online_Banking_Active__c
                FROM Contact
                WHERE ID IN :cs
            ]) ;
        }
        catch ( Exception e )
        {
            cjMap = new map<ID, Contact> () ;
        }
        
        for ( Service__c n : Trigger.new )
        {
            if ( Trigger.isInsert )
            {
                if ( ( n.recordTypeId == onlineservicesRTID ) && ( n.Contact__c != null ) )
                {
                    //
                    // Online Banking Active
                    // 
                    if ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) )
                    {
                        if ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'online banking' ) ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Online_Banking_Active__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Online_Banking_Active__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                        }
                        
                        //
                        // Mobile Banking active
                        // 
                        else if ( ( String.isNotBlank ( n.Type__c ) ) && 
                                  ( ( n.Type__c.equalsIgnoreCase ( 'mobile banking' ) ) || ( n.Type__c.equalsIgnoreCase ( 'mobile web banking' ) ) || ( n.Type__c.equalsIgnoreCase ( 'tablet banking' ) ) ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Mobile_Banking_Active__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Mobile_Banking_Active__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                        }
                    }
                }
                
                if ( ( n.recordTypeId == agreementRTID ) && ( n.Contact__c != null ) )
                {
                    //
                    //  Debit Cards
                    //  
                    if ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) )
                    {
                        if ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'debit card' ) ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Card_Plastic_Type__c = n.Plastic_Type__c ;
                                cMap.get ( n.Contact__c ).Active_Debit_Card__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Card_Plastic_Type__c = n.Plastic_Type__c ;
                                c.Active_Debit_Card__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                            
                            if ( n.Primary_Checking__c != null )
                            {
                                if ( faMap.containsKey ( n.Primary_Checking__c ) ) 
                                {
                                    faMap.get ( n.Primary_Checking__c ).Active_Card__c = true ;
                                }
                                else
                                {
                                    Financial_Account__c fa = new Financial_Account__c () ;
                                    fa.ID = n.Primary_Checking__c ;
                                    fa.Active_Card__c = true ;
                                    
                                    faMap.put ( n.Primary_Checking__c, fa ) ;
                                }
                            }
                            
                            if ( n.Primary_Savings__c != null )
                            {
                                if ( faMap.containsKey ( n.Primary_Savings__c ) ) 
                                {
                                    faMap.get ( n.Primary_Savings__c ).Active_Card__c = true ;
                                }
                                else
                                {
                                    Financial_Account__c fa = new Financial_Account__c () ;
                                    fa.ID = n.Primary_Savings__c ;
                                    fa.Active_Card__c = true ;
                                    
                                    faMap.put ( n.Primary_Savings__c, fa ) ;
                                }
                            }
                        }
                        
                        else if ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'atm card' ) ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Card_Plastic_Type__c = n.Plastic_Type__c ;
                                cMap.get ( n.Contact__c ).Active_ATM_Card__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Card_Plastic_Type__c = n.Plastic_Type__c ;
                                c.Active_ATM_Card__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                            
                            if ( n.Primary_Checking__c != null )
                            {
                                if ( faMap.containsKey ( n.Primary_Checking__c ) ) 
                                {
                                    faMap.get ( n.Primary_Checking__c ).Active_Card__c = true ;
                                }
                                else
                                {
                                    Financial_Account__c fa = new Financial_Account__c () ;
                                    fa.ID = n.Primary_Checking__c ;
                                    fa.Active_Card__c = true ;
                                    
                                    faMap.put ( n.Primary_Checking__c, fa ) ;
                                }
                            }
                            
                            if ( n.Primary_Savings__c != null )
                            {
                                if ( faMap.containsKey ( n.Primary_Savings__c ) ) 
                                {
                                    faMap.get ( n.Primary_Savings__c ).Active_Card__c = true ;
                                }
                                else
                                {
                                    Financial_Account__c fa = new Financial_Account__c () ;
                                    fa.ID = n.Primary_Savings__c ;
                                    fa.Active_Card__c = true ;
                                    
                                    faMap.put ( n.Primary_Savings__c, fa ) ;
                                }
                            }
                        }
                    }
                }
            }
            
            if ( Trigger.isUpdate )
            {
                Service__c o = Trigger.oldMap.get ( n.ID ) ;
                
                if ( ( n.recordTypeId == onlineservicesRTID ) && ( n.Contact__c != null ) )
                {
                    //
                    // Online Banking Active
                    // 
                    if ( ( ( ( String.isBlank ( o.status__c ) ) || ( ( String.isNotBlank ( o.Status__c ) ) && ( ! o.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) ||
                        ( ( ( String.isBlank ( o.Type__c ) ) || ( ( String.isNotBlank ( o.Type__c ) ) && ( ! o.Type__c.equalsIgnoreCase ( 'online banking' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'online banking' ) ) ) ) 
                       )
                    {
                        if ( cMap.containsKey ( n.Contact__c ) )
                        {
                            cMap.get ( n.Contact__c ).Online_Banking_Active__c = true ;
                        }
                        else
                        {
                            Contact c = new Contact () ;
                            c.ID = n.Contact__c ;
                            c.Online_Banking_Active__c = true ;
                            
                            cMap.put ( n.Contact__c, c ) ;
                        }
                    }
                    else if ( ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) &&
                              ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'online banking' ) ) ) )
                    {
                        if ( ( cjMap.containsKey ( n.Contact__c ) ) && ( cjMap.get ( n.Contact__c ).Online_Banking_Active__c == false ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Online_Banking_Active__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Online_Banking_Active__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                        }
                    }
                        
                    //
                    // Mobile Banking Active
                    // 
                    if ( ( ( ( String.isBlank ( o.status__c ) ) || ( ( String.isNotBlank ( o.Status__c ) ) && ( ! o.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) ||
                        ( ( ( String.isBlank ( o.Type__c ) ) || ( ( String.isNotBlank ( o.Type__c ) ) && ( ! o.Type__c.equalsIgnoreCase ( 'mobile banking' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'mobile banking' ) ) ) ) ||
                        ( ( ( String.isBlank ( o.Type__c ) ) || ( ( String.isNotBlank ( o.Type__c ) ) && ( ! o.Type__c.equalsIgnoreCase ( 'mobile web banking' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'mobile web banking' ) ) ) ) ||
                        ( ( ( String.isBlank ( o.Type__c ) ) || ( ( String.isNotBlank ( o.Type__c ) ) && ( ! o.Type__c.equalsIgnoreCase ( 'tablet banking' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'tablet banking' ) ) ) )
                       )
                    {
                        if ( cMap.containsKey ( n.Contact__c ) )
                        {
                            cMap.get ( n.Contact__c ).Mobile_Banking_Active__c = true ;
                        }
                        else
                        {
                            Contact c = new Contact () ;
                            c.ID = n.Contact__c ;
                            c.Mobile_Banking_Active__c = true ;
                            
                            cMap.put ( n.Contact__c, c ) ;
                        }
                    }
                    else if ( ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) &&
                              ( ( String.isNotBlank ( n.Type__c ) ) && ( ( n.Type__c.equalsIgnoreCase ( 'mobile banking' ) ) || ( n.Type__c.equalsIgnoreCase ( 'mobile web banking' ) ) || ( n.Type__c.equalsIgnoreCase ( 'tablet banking' ) ) ) ) )
                    {
                        if ( ( cjMap.containsKey ( n.Contact__c ) ) && ( cjMap.get ( n.Contact__c ).Mobile_Banking_Active__c == false ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Mobile_Banking_Active__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Mobile_Banking_Active__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                        }
                    }
                }
                
                if ( ( n.recordTypeId == agreementRTID ) && ( n.Contact__c != null ) )
                {
                    //
                    //  Debit Cards
                    //  
                    if ( ( ( ( String.isBlank ( o.status__c ) ) || ( ( String.isNotBlank ( o.Status__c ) ) && ( ! o.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) ||
                        ( ( ( String.isBlank ( o.Type__c ) ) || ( ( String.isNotBlank ( o.Type__c ) ) && ( ! o.Type__c.equalsIgnoreCase ( 'debit card' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'debit card' ) ) ) ) )
                    {
                        if ( cMap.containsKey ( n.Contact__c ) )
                        {
                            cMap.get ( n.Contact__c ).Card_Plastic_Type__c = n.Plastic_Type__c ;
                            cMap.get ( n.Contact__c ).Active_Debit_Card__c = true ;
                        }
                        else
                        {
                            Contact c = new Contact () ;
                            c.ID = n.Contact__c ;
                            c.Card_Plastic_Type__c = n.Plastic_Type__c ;
                            c.Active_Debit_Card__c = true ;
                            
                            cMap.put ( n.Contact__c, c ) ;
                        }
                        
                        if ( n.Primary_Checking__c != null )
                        {
                            if ( faMap.containsKey ( n.Primary_Checking__c ) ) 
                            {
                                faMap.get ( n.Primary_Checking__c ).Active_Card__c = true ;
                            }
                            else
                            {
                                Financial_Account__c fa = new Financial_Account__c () ;
                                fa.ID = n.Primary_Checking__c ;
                                fa.Active_Card__c = true ;
                                
                                faMap.put ( n.Primary_Checking__c, fa ) ;
                            }
                        }
                         
                        if ( n.Primary_Savings__c != null )
                        {
                            if ( faMap.containsKey ( n.Primary_Savings__c ) ) 
                            {
                                faMap.get ( n.Primary_Savings__c ).Active_Card__c = true ;
                            }
                            else
                            {
                                Financial_Account__c fa = new Financial_Account__c () ;
                                fa.ID = n.Primary_Savings__c ;
                                fa.Active_Card__c = true ;
                                
                                faMap.put ( n.Primary_Savings__c, fa ) ;
                            }
                        }
                    }
                    else if ( ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) &&
                              ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'debit card' ) ) ) )
                    {
                        if ( ( cjMap.containsKey ( n.Contact__c ) ) && ( cjMap.get ( n.Contact__c ).Active_Debit_Card__c == false ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Active_Debit_Card__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Active_Debit_Card__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                        }
                    }
                        
                    //
                    //  ATM Cards
                    //  
                    if ( ( ( ( String.isBlank ( o.status__c ) ) || ( ( String.isNotBlank ( o.Status__c ) ) && ( ! o.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) ) ||
                        ( ( ( String.isBlank ( o.Type__c ) ) || ( ( String.isNotBlank ( o.Type__c ) ) && ( ! o.Type__c.equalsIgnoreCase ( 'atm card' ) ) ) ) && 
                         ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'atm card' ) ) ) ) )
                    {
                        if ( cMap.containsKey ( n.Contact__c ) )
                        {
                            cMap.get ( n.Contact__c ).Card_Plastic_Type__c = n.Plastic_Type__c ;
                            cMap.get ( n.Contact__c ).Active_ATM_Card__c = true ;
                        }
                        else
                        {
                            Contact c = new Contact () ;
                            c.ID = n.Contact__c ;
                            c.Card_Plastic_Type__c = n.Plastic_Type__c ;
                            c.Active_ATM_Card__c = true ;
                            
                            cMap.put ( n.Contact__c, c ) ;
                        }
                        
                        if ( n.Primary_Checking__c != null )
                        {
                            if ( faMap.containsKey ( n.Primary_Checking__c ) ) 
                            {
                                faMap.get ( n.Primary_Checking__c ).Active_Card__c = true ;
                            }
                            else
                            {
                                Financial_Account__c fa = new Financial_Account__c () ;
                                fa.ID = n.Primary_Checking__c ;
                                fa.Active_Card__c = true ;
                            }
                        }
                        
                        if ( n.Primary_Savings__c != null )
                        {
                            if ( faMap.containsKey ( n.Primary_Savings__c ) ) 
                            {
                                faMap.get ( n.Primary_Savings__c ).Active_Card__c = true ;
                            }
                            else
                            {
                                Financial_Account__c fa = new Financial_Account__c () ;
                                fa.ID = n.Primary_Savings__c ;
                                fa.Active_Card__c = true ;
                            }
                        }
                    }
                    else if ( ( ( String.isNotBlank ( n.Status__c ) ) && ( n.Status__c.equalsIgnoreCase ( 'active' ) ) ) &&
                              ( ( String.isNotBlank ( n.Type__c ) ) && ( n.Type__c.equalsIgnoreCase ( 'atm card' ) ) ) )
                    {
                        if ( ( cjMap.containsKey ( n.Contact__c ) ) && ( cjMap.get ( n.Contact__c ).Active_ATM_Card__c == false ) )
                        {
                            if ( cMap.containsKey ( n.Contact__c ) )
                            {
                                cMap.get ( n.Contact__c ).Active_ATM_Card__c = true ;
                            }
                            else
                            {
                                Contact c = new Contact () ;
                                c.ID = n.Contact__c ;
                                c.Active_ATM_Card__c = true ;
                                
                                cMap.put ( n.Contact__c, c ) ;
                            }
                        }
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  END LOOP
            //  -----------------------------------------------------------------------------------
        }
        
        //  -----------------------------------------------------------------------------------
        //  Queue teh updates
        //  -----------------------------------------------------------------------------------
        if ( cMap.size () > 0 )
        {
            ServiceQueue sq = new ServiceQueue ( cMap.values () ) ;
            System.enqueueJob ( sq ) ;
        }
        
        if ( faMap.size () > 0 )
        {
            ServiceQueue sq = new ServiceQueue ( faMap.values () ) ;
            System.enqueueJob ( sq ) ;
        }
    }
}