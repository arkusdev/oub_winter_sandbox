trigger DepositApplicationTrigger on Deposit_Application__c (before insert, before update, after insert, after update) 
{
	System.debug ( '------------------------------ TRIGGER START ------------------------------' ) ;
	
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
		System.debug ( '------------------------------ BEFORE START ------------------------------' ) ;
		
		//  -----------------------------------------------------------------------------------
		//  MAIN LOOP
		//  -----------------------------------------------------------------------------------
		for ( Deposit_Application__c n : Trigger.new )
		{
			if ( Trigger.isInsert )
			{
				//  -------------------------------------------------------------------------------------
				//  This section sets Key for attachments if it doesn't already exist
				//  -------------------------------------------------------------------------------------
	            if ( String.isBlank ( n.External_Key__c ) )
					n.External_Key__c = one_utils.getRandomString ( 30 ) ;
					
				//  -------------------------------------------------------------------------------------
				//  Additional Info
				//  -------------------------------------------------------------------------------------
				if ( String.isNotBlank ( n.Current_Status__c ) && ( n.Current_Status__c.equalsIgnoreCase ( 'review' ) ) )
				{
					//  QUALFILE
		            if ( String.isNotBlank ( n.Debit_History_Response__c ) && ( n.Debit_History_Response__c.equalsIgnoreCase ( 'review' ) ) )
		            {
		            	String docs = Deposit_Application_Utils.getMultiPicklistDocs ( n.Debit_History_Strategy_Code__c ) ;
		            	
		            	if ( String.isNotBlank ( docs ) )
		            	{
			            	n.Application_Processing_Status__c = 'Additional Documentation Needed' ;
			            	n.Additional_Information_Required__c = docs ;
                            n.Additional_Doc_Needed_Datetime__c = System.now () ;
		            	}
		            }
		            
		            //  IDV
		            if ( String.isNotBlank ( n.Identity_Verification_Response__c ) && ( n.Identity_Verification_Response__c.startsWithIgnoreCase ( 'review' ) ) )
		            {
		            	n.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		            	n.Additional_Information_Required__c = Deposit_Application_Utils.getMultiPicklistDocs ( 'IDV' ) ;
                        n.Additional_Doc_Needed_Datetime__c = System.now () ;
		            }
				}
			}
			
			if ( Trigger.isUpdate )
			{
				//  --------------------------------------------------------------
				//  Get old data for comparison
				//  --------------------------------------------------------------
				Deposit_Application__c o = Trigger.oldMap.get ( n.ID ) ;
				
				//  -------------------------------------------------------------------------------------
				//  This section sets Key for attachments if it doesn't already exist
				//  -------------------------------------------------------------------------------------
	            if ( String.isBlank ( o.External_Key__c ) )
					n.External_Key__c = one_utils.getRandomString ( 30 ) ;
					
				//  -------------------------------------------------------------------------------------
				//  Additional Info - QUALFILE
				//  -------------------------------------------------------------------------------------
				if ( ( String.isBlank ( o.Current_Status__c ) || ( ! o.Current_Status__c.equalsIgnoreCase ( 'review' ) ) ) && 
				     ( String.isNotBlank ( n.Current_Status__c ) && ( n.Current_Status__c.equalsIgnoreCase ( 'review' ) ) ) )
				{
					//  QUALFILE
		            if ( String.isNotBlank ( n.Debit_History_Response__c ) && ( n.Debit_History_Response__c.equalsIgnoreCase ( 'review' ) ) )
		            {
		            	String docs = Deposit_Application_Utils.getMultiPicklistDocs ( n.Debit_History_Strategy_Code__c ) ;
		            	
		            	if ( String.isNotBlank ( docs ) )
		            	{
			            	n.Application_Processing_Status__c = 'Additional Documentation Needed' ;
			            	n.Additional_Information_Required__c = docs ;
	                        n.Additional_Doc_Needed_Datetime__c = System.now () ;
		            	}
		            }
		            
		            //  IDV
		            if ( String.isNotBlank ( n.Identity_Verification_Response__c ) && ( n.Identity_Verification_Response__c.startsWithIgnoreCase ( 'review' ) ) )
		            {
		            	n.Application_Processing_Status__c = 'Additional Documentation Needed' ;
		            	n.Additional_Information_Required__c = Deposit_Application_Utils.getMultiPicklistDocs ( 'IDV' ) ;
                        n.Additional_Doc_Needed_Datetime__c = System.now () ;
		            }
				}
                
				//  -------------------------------------------------------------------------------------
				//  TRACKING
				//  -------------------------------------------------------------------------------------
				if ( ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                     ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Received' ) ) &&
                     ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Received' ) )
                   )
                {
                    n.Additional_Doc_Received_Datetime__c = System.now () ;
                }
                
				if ( ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                     ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Verified' ) ) &&
                     ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Verified' ) )
                   )
                {
                    n.Additional_Doc_Verified_Datetime__c = System.now () ;
                    n.Additional_Doc_Verified_User__c = UserInfo.getUserId () ;
                }
                
                if ( ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                     ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                     ( ! n.Application_Processing_Status__c.equalsIgnoreCase ( o.Application_Processing_Status__c ) ) )
                {
                    n.Application_Status_Last_Updated__c = System.now () ;
                    n.Application_Status_Update_User__c = UserInfo.getUserId () ;
                }
			}
		}
		
		System.debug ( '------------------------------ BEFORE END ------------------------------' ) ;
	}
	
	/* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		System.debug ( '------------------------------ AFTER START ------------------------------' ) ;
		
		//  -----------------------------------------------------------------------------------
		//  Deposit Application Helper setup
		//  -----------------------------------------------------------------------------------
		if ( DepositApplicationHelper.depositApplicationMap == null )
        	DepositApplicationHelper.depositApplicationMap = new map<String, boolean> () ;
        	
		if ( DepositApplicationHelper.contactMap == null )
        	DepositApplicationHelper.contactMap = new map<String, Contact> () ;
        	
		if ( DepositApplicationHelper.updatedContactMap == null )
        	DepositApplicationHelper.updatedContactMap = new map<String, boolean> () ;
        	
		if ( DepositApplicationHelper.contactTaxIDMap == null )
        	DepositApplicationHelper.contactTaxIDMap = new map<String, ID> () ;
        	
		if ( DepositApplicationHelper.additionalMap == null )
        	DepositApplicationHelper.additionalMap = new map<String, boolean> () ;

		if ( DepositApplicationHelper.denyMap == null )
        	DepositApplicationHelper.denyMap = new map<String, boolean> () ;

		if ( DepositApplicationHelper.fcraDenyMap == null )
        	DepositApplicationHelper.fcraDenyMap = new map<String, boolean> () ;

        //  -----------------------------------------------------------------------------------
		//  Setting Up
		//  -----------------------------------------------------------------------------------
		String recordTypeID = DepositApplicationHelper.getContactProspectRecordTypeId () ;
		
        String accountID = '' ;
        list<Account> aL = null ;
        
        try
        {
            aL = [
                SELECT ID
                FROM Account
                WHERE Name = 'Insight Customers - Individuals'
                LIMIT 1
            ] ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        if ( ( aL != null ) && ( aL.size () > 0 ) )
        {
            accountID = aL [ 0 ].ID ;
        }
        
		//  Missing contact MAP - no duplicates!
		map<String, Deposit_Application__c> checkDALMap = new map<String, Deposit_Application__c> () ;
			
		//  -----------------------------------------------------------------------------------
		//  Loop to see if the application needs a contact associated with it
		//  -----------------------------------------------------------------------------------
		for ( Deposit_Application__c n : Trigger.new )
		{
			//  Main Applicant
			if ( n.Tax_Reported_Contact__c == null )
			{
		    	if ( ! checkDALMap.containsKey ( n.ID ) )
					checkDALMap.put ( n.ID, n ) ;
			}
			
			//  Co Applicant
			if ( ( n.Co_Applicant_Tax_Reported_Contact__c == null ) &&
			     ( ( String.isNotBlank ( n.Co_Applicant_Email__c ) ||
			       ( String.isNotBlank ( n.Co_Applicant_SSN__c ) ) ) ) )
		    {
		    	if ( ! checkDALMap.containsKey ( n.ID ) )
					checkDALMap.put ( n.ID, n ) ;
		    }
		    
		    //  Beneficiary
			if ( ( n.Beneficiary_Tax_Reported_Contact__c == null ) &&
			     ( ( String.isNotBlank ( n.Beneficiary_Email__c ) ||
			       ( String.isNotBlank ( n.Beneficiary_SSN__c ) ) ) ) )
		    {
		    	if ( ! checkDALMap.containsKey ( n.ID ) )
					checkDALMap.put ( n.ID, n ) ;
		    }
		}
		
		//  -----------------------------------------------------------------------------------
		//  Contact check list setup
		//  -----------------------------------------------------------------------------------
		list<String> ssnL = new list<String> () ;
		list<String> emailL = new list<String> () ;
		
		for ( String key : checkDALMap.keyset () )
		{
			Deposit_Application__c da = checkDALMap.get ( key ) ;
			
			//  SSNs
			if ( String.isNotBlank ( da.Applicant_SSN__c ) )
				ssnL.add ( da.Applicant_SSN__c ) ;
				
			if ( String.isNotBlank ( da.Co_Applicant_SSN__c ) )
				ssnL.add ( da.Co_Applicant_SSN__c ) ;
				
			if ( String.isNotBlank ( da.Beneficiary_SSN__c ) )
				ssnL.add ( da.Beneficiary_SSN__c ) ;
				
			//  Emails
			if ( String.isNotBlank ( da.Email__c ) )
				emailL.add ( da.Email__c ) ;
			
			if ( String.isNotBlank ( da.Co_Applicant_Email__c ) )
				emailL.add ( da.Co_Applicant_Email__c ) ;
			
			if ( String.isNotBlank ( da.Beneficiary_Email__c ) )
				emailL.add ( da.Beneficiary_Email__c ) ;
		}
		
		//  -----------------------------------------------------------------------------------
		//  Load contacts into map by tax ID
		//  -----------------------------------------------------------------------------------
		map<String,Contact> tcMap = new map<String,Contact> () ;
		
		if ( ssnL.size () > 0 )
		{
			
			list<Contact> cssnL = [
				SELECT ID, FirstName, LastName, Email, TaxID__c
				FROM Contact
				WHERE TaxID__c IN :ssnL
			] ;
			
			if ( cssnL.size () > 0 )
			{
				System.debug ( '---------- FOUND ' + cssnL.size () + ' Contacts by SSN ----------' ) ;
				
				for ( Contact c : cssnL )
					tcMap.put ( c.TaxID__c, c ) ;
			}
		}
		
		//  -----------------------------------------------------------------------------------
		//  Load contacts into map by email
		//  -----------------------------------------------------------------------------------
		map<String,Contact> ecMap = new map<String,Contact> () ;
		
		if ( emailL.size () > 0 )
		{
			
			list<Contact> ceL = [
				SELECT ID, FirstName, LastName, Email, TaxID__c
				FROM Contact
				WHERE Email IN :emailL
			] ;
			
			if ( ceL.size () > 0 )
			{
				System.debug ( '---------- FOUND ' + ceL.size () + ' Possible Contacts by Email ----------' ) ;
				
				for ( Contact c : ceL )
				{
					String key = c.FirstName + '~' + c.LastName + '~' + c.Email ;
					System.debug ( 'KEY - "' + key + '"' ) ;
					ecMap.put ( key, c ) ;
				}
			}
		}
		
		//  -----------------------------------------------------------------------------------
		//  Sort applications by whether or not they have a contact
		//  -----------------------------------------------------------------------------------
		list<Deposit_Application__c> existDAL = new list<Deposit_Application__c> () ;
		list<Deposit_Application__c> newDAL = new list<Deposit_Application__c> () ;
		
		for ( String key : checkDALMap.keyset () )
		{
			Deposit_Application__c da = checkDALMap.get ( key ) ;
			
			String pKEY = da.First_Name__c + '~' + da.Last_Name__c + '~' + da.Email__c ;
			String cKEY = da.Co_Applicant_First_Name__c + '~' + da.Co_Applicant_Last_Name__c + '~' + da.Co_Applicant_Email__c ;
			String bKEY = da.Beneficiary_First_Name__c + '~' + da.Beneficiary_Last_Name__c + '~' + da.Beneficiary_Email__c ;
			
			System.debug ( 'Checking Application - "' + key + '"' ) ;
			System.debug ( '#1 - "' + pKEY + '"' ) ;
			System.debug ( '#2 - "' + cKEY + '"' ) ;
			System.debug ( '#3 - "' + bKEY + '"' ) ;
			
			boolean found = false ;
			Deposit_Application__c dax = da.clone ( true, true, false, false ) ;
			
			//  Applicant
			if ( tcMap.containsKey ( da.Applicant_SSN__c ) )
			{
				System.debug ( '-- Found Applicant SSN' ) ;
				Contact c = tcMap.get ( da.Applicant_SSN__c ) ;
				
				dax.Tax_Reported_Contact__c = c.ID ;
				
				if ( ! DepositApplicationHelper.contactMap.containsKey ( c.ID ) )
				{
					System.debug ( '#1 Checking Contact ' + c.ID + ' for updates' ) ;
					
					Contact nc = new Contact () ;
					nc.ID = c.ID ;
					
					boolean cUpdate = false ;
					if ( da.Created_DateTime__c != null )
					{
						nc.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
						cUpdate = true ;
					}
					
					if ( cUpdate )
					{
						System.debug ( 'Adding contact for update :: ' + nc.ID ) ;
						DepositApplicationHelper.contactMap.put ( nc.ID, nc ) ;
					}
				}
				else
				{
					System.debug ( 'Contact already updated?!?' ) ;
				}
				
				found = true ;
			}
			else if ( ecMap.containsKey ( pKEY ) )
			{
				System.debug ( '-- Found Applicant Info' ) ;
				Contact c = ecMap.get ( pKEY ) ;
				
				dax.Tax_Reported_Contact__c = c.ID ;
				
				if ( ! DepositApplicationHelper.contactMap.containsKey ( c.ID ) )
				{
					System.debug ( '#2 Checking Contact ' + c.ID + ' for updates' ) ;
					
					Contact nc = new Contact () ;
					nc.ID = c.ID ;
					
					boolean cUpdate = false ;
					if ( c.TaxID__c == null )
					{
						if ( ! DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Applicant_SSN__c ) )
						{
							System.debug ( 'Adding tax ID ' + da.Applicant_SSN__c + ' to contact.' ) ;
							nc.TaxID__c = da.Applicant_SSN__c ;
							DepositApplicationHelper.contactTaxIDMap.put ( da.Applicant_SSN__c, c.ID ) ;
							cUpdate = true ;
						}
						else
						{
							System.debug ( 'SKIPPING tax ID ' + da.Applicant_SSN__c + ', already on a contact?!?' ) ;
						}
					}
					
					if ( da.Created_DateTime__c != null )
					{
						nc.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
						cUpdate = true ;
					}
					
					if ( cUpdate )
					{
						System.debug ( 'Adding contact for update :: ' + nc.ID ) ;
						DepositApplicationHelper.contactMap.put ( nc.ID, nc ) ;
					}
				}
				else
				{
					System.debug ( 'Contact already updated?!?' ) ;
				}
				
				found = true ;
			}
			
			//  Co-Applicant
			if ( tcMap.containsKey ( da.Co_Applicant_SSN__c ) )
			{
				System.debug ( '-- Found Co-Applicant SSN' ) ;
				Contact c = tcMap.get ( da.Co_Applicant_SSN__c ) ;
				
				dax.Co_Applicant_Tax_Reported_Contact__c = c.ID ;
				
				if ( ! DepositApplicationHelper.contactMap.containsKey ( c.ID ) )
				{
					System.debug ( '#3 Checking Contact ' + c.ID + ' for updates' ) ;
					
					Contact nc = new Contact () ;
					nc.ID = c.ID ;
					
					boolean cUpdate = false ;
					if ( da.Created_DateTime__c != null )
					{
						nc.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
						cUpdate = true ;
					}
					
					if ( cUpdate )
					{
						System.debug ( 'Adding contact for update :: ' + nc.ID ) ;
						DepositApplicationHelper.contactMap.put ( nc.ID, nc ) ;
					}
				}
				else
				{
					System.debug ( 'Contact already updated?!?' ) ;
				}
				
				found = true ;
			}
			else if ( ecMap.containsKey ( cKEY ) )
			{
				System.debug ( '-- Found Co-Applicant Info' ) ;
				Contact c = ecMap.get ( cKEY ) ;
				
				dax.Co_Applicant_Tax_Reported_Contact__c = c.ID ;
				
				if ( ! DepositApplicationHelper.contactMap.containsKey ( c.ID ) )
				{
					System.debug ( '#4 Checking Contact ' + c.ID + ' for updates' ) ;
					
					Contact nc = new Contact () ;
					nc.ID = c.ID ;
					
					boolean cUpdate = false ;
					if ( c.TaxID__c == null )
					{
						if ( ! DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Co_Applicant_SSN__c ) )
						{
							System.debug ( 'Adding tax ID ' + da.Co_Applicant_SSN__c + ' to contact.' ) ;
							nc.TaxID__c = da.Co_Applicant_SSN__c ;
							DepositApplicationHelper.contactTaxIDMap.put ( da.Co_Applicant_SSN__c, c.ID ) ;
							cUpdate = true ;
						}
						else
						{
							System.debug ( 'SKIPPING tax ID ' + da.Co_Applicant_SSN__c + ', already on a contact?!?' ) ;
						}
					}
					
					if ( da.Created_DateTime__c != null )
					{
						nc.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
						cUpdate = true ;
					}
					
					if ( cUpdate )
					{
						System.debug ( 'Adding contact for update :: ' + nc.ID ) ;
						DepositApplicationHelper.contactMap.put ( nc.ID, nc ) ;
					}
				}
				else
				{
					System.debug ( 'Contact already updated?!?' ) ;
				}
				
				found = true ;
			}
			
			//  Beneficiary
			if ( tcMap.containsKey ( da.Beneficiary_SSN__c ) )
			{
				System.debug ( '-- Found Beneficiary SSN' ) ;
				Contact c = tcMap.get ( da.Beneficiary_SSN__c ) ;
				
				dax.Beneficiary_Tax_Reported_Contact__c = c.ID ;
				
				if ( ! DepositApplicationHelper.contactMap.containsKey ( c.ID ) )
				{
					System.debug ( '#5 Checking Contact ' + c.ID + ' for updates' ) ;
					
					Contact nc = new Contact () ;
					nc.ID = c.ID ;
					
					boolean cUpdate = false ;
					if ( da.Created_DateTime__c != null )
					{
						nc.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
						cUpdate = true ;
					}
					
					if ( cUpdate )
					{
						System.debug ( 'Adding contact for update :: ' + nc.ID ) ;
						DepositApplicationHelper.contactMap.put ( nc.ID, nc ) ;
					}
				}
				else
				{
					System.debug ( 'Contact already updated?!?' ) ;
				}
				
				found = true ;
			}
			else if ( ecMap.containsKey ( bKEY ) )
			{
				System.debug ( '-- Found Beneficiary Info' ) ;
				Contact c = ecMap.get ( bKEY ) ;
				
				dax.Beneficiary_Tax_Reported_Contact__c = c.ID ;
				
				if ( ! DepositApplicationHelper.contactMap.containsKey ( c.ID ) )
				{
					System.debug ( '#6 Checking Contact for updates' ) ;
					
					Contact nc = new Contact () ;
					nc.ID = c.ID ;
					
					boolean cUpdate = false ;
					if ( c.TaxID__c == null )
					{
						if ( ! DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Beneficiary_SSN__c ) )
						{
							System.debug ( 'Adding tax ID ' + da.Beneficiary_SSN__c + ' to contact.' ) ;
							nc.TaxID__c = da.Beneficiary_SSN__c ;
							DepositApplicationHelper.contactTaxIDMap.put ( da.Beneficiary_SSN__c, c.ID ) ;
							cUpdate = true ;
						}
						else
						{
							System.debug ( 'SKIPPING tax ID ' + da.Beneficiary_SSN__c + ', already on a contact?!?' ) ;
						}
					}
					
					if ( da.Created_DateTime__c != null )
					{
						nc.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
						cUpdate = true ;
					}
					
					if ( cUpdate )
					{
						System.debug ( 'Adding contact for update :: ' + nc.ID ) ;
						DepositApplicationHelper.contactMap.put ( nc.ID, nc ) ;
					}
				}
				else
				{
					System.debug ( 'Contact already updated?!?' ) ;
				}
				
				found = true ;
			}
			
			if ( found )
			{
				System.debug ( '-- Found some crap, adding to update!' ) ;
				if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.depositApplicationMap, dax.ID ) )
					existDAL.add ( dax ) ;				
			}
			else
			{
				System.debug ( '-- No prior found!' ) ;
				if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.depositApplicationMap, da.ID ) )
					newDAL.add ( da ) ;
			}
			
		}
		
		//  -----------------------------------------------------------------------------------
		//  Update - Existing Contacts
		//  -----------------------------------------------------------------------------------
		if ( DepositApplicationHelper.contactMap.keyset().size () > 0 )
		{
			System.debug ( '------------------------- UPDATE existContactMap ?!? -------------------------' ) ;
			
			boolean hasUpdate = false ;
			list<Contact> cL = new list<Contact> () ;
			
			for ( ID id : DepositApplicationHelper.contactMap.keyset () )
			{
				System.debug ( 'Checking ID -- ' + id ) ;
				if ( ! DepositApplicationHelper.updatedContactMap.containsKey ( id ) )
				{
					System.debug ( 'Not already updated, updating' ) ;
					cL.add ( DepositApplicationHelper.contactMap.get ( id ) ) ;
					DepositApplicationHelper.updatedContactMap.put ( id, true ) ;
					hasUpdate = true ;
				}
				else
				{
					System.debug ( 'Already updated, skipping' ) ;
				}
			}
			
			if ( hasUpdate )
				update cL ;
		}
		
		//  -----------------------------------------------------------------------------------
		//  Update - Existing applications
		//  -----------------------------------------------------------------------------------
		if ( existDAL.size () > 0 )
		{
			System.debug ( '------------------------- UPDATE EXISTDAL -------------------------' ) ;
			update existDAL ;
		}
		
		//  -----------------------------------------------------------------------------------
		// Contact Creates, Applicant/Co-Applicant/Beneficiary
		//  -----------------------------------------------------------------------------------
		list<Deposit_Application__c> nDAL1 = new list<Deposit_Application__c> () ;
		list<Deposit_Application__c> nDAL1P = new list<Deposit_Application__c> () ;
		map<String, Contact> cDAM1 = new map<String, Contact> () ;
		
		list<Deposit_Application__c> nDAL2 = new list<Deposit_Application__c> () ;
		list<Deposit_Application__c> nDAL2P = new list<Deposit_Application__c> () ;
		map<String, Contact> cDAM2 = new map<String, Contact> () ;
		
		list<Deposit_Application__c> nDAL3 = new list<Deposit_Application__c> () ;
		list<Deposit_Application__c> nDAL3P = new list<Deposit_Application__c> () ;
		map<String, Contact> cDAM3 = new map<String, Contact> () ;
		
		//  -----------------------------------------------------------------------------------
		//  NEXT LOOP
		//  -----------------------------------------------------------------------------------
		for ( Deposit_Application__c da : newDAL )
		{
			System.debug ( 'Update #0 - Sorting - ' + da.Email__c + ' -- ' + da.Applicant_SSN__c ) ;
			
			if ( da.Tax_Reported_Contact__c == null )
			{
				Deposit_Application__c dax = da.clone ( true, true, false, false ) ;
				nDAL1.add ( dax ) ;
			}
			
			//  Co Applicant
			if ( ( da.Co_Applicant_Tax_Reported_Contact__c == null ) &&
			     ( ( String.isNotBlank ( da.Co_Applicant_Email__c ) ||
			       ( String.isNotBlank ( da.Co_Applicant_SSN__c ) ) ) ) )
	        {
				Deposit_Application__c dax = da.clone ( true, true, false, false ) ;
				nDAL2.add ( dax ) ;
	        }
		    
		    //  Beneficiary
			if ( ( da.Beneficiary_Tax_Reported_Contact__c == null ) &&
			     ( ( String.isNotBlank ( da.Beneficiary_Email__c ) ||
			       ( String.isNotBlank ( da.Beneficiary_SSN__c ) ) ) ) )
	        {
				Deposit_Application__c dax = da.clone ( true, true, false, false ) ;
				nDAL3.add ( dax ) ;
	        }
		}
		
		//  -----------------------------------------------------------------------------------
		//  Deposit Application Update #1
		//  -----------------------------------------------------------------------------------
		if ( nDAL1.size () > 0 )
		{
			for ( Deposit_Application__c da : nDAL1 )
			{
				System.debug ( 'Deposit Application Update #1 - Checking - ' + da.Email__c + ' --> ' + da.Applicant_SSN__c ) ;
				
				if ( ( ! cDAM1.containsKey ( da.Applicant_SSN__c ) ) && 
				     ( ! DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Applicant_SSN__c ) ) )
				{
					Contact c = new Contact () ;
					c.recordTypeID = recordTypeID ;
                    if ( String.IsNotEmpty ( accountID ) ) c.AccountID = accountID ;
					c.FirstName = da.First_Name__c ;
	//				c.MiddleName = da.Middle_Name__c ;
					c.LastName = da.Last_Name__c ;
	//				c.Suffix = da.Suffix__c ;
					c.Email = da.Email__c ;
					c.TaxID__c = da.Applicant_SSN__c ;
					c.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
					
					c.MailingStreet = da.Address__c ;
					c.MailingCity = da.City__c ;
					c.MailingState = da.State__c ;
					c.MailingPostalCode = da.Zip__c ;
					
					c.Phone = da.Daytime_Phone__c ;
					c.Evening_Phone__c = da.Evening_Phone__c ;
					
					cDAM1.put ( da.Applicant_SSN__c, c ) ;
				}
				else
				{
					System.debug ( 'Duplicate SSN found :: ' + da.Applicant_SSN__c + ' CHECKING PRIOR' ) ;
					
					//  If it's in here we've aleady updated it
					if ( DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Applicant_SSN__c ) )
					{
						ID pContactID = DepositApplicationHelper.contactTaxIDMap.get ( da.Applicant_SSN__c ) ;
						
						System.debug ( 'Updating with prior contact' ) ;
						da.Tax_Reported_Contact__c = pContactID ;
						nDAL1P.add ( da ) ;
					}
					else
					{
						System.debug ( 'Genuine duplicate found!' ) ;
					}
				}
			}
			
			if ( nDAL1P.size () > 0 )
			{
				System.debug ( '------------------------- UPDATE nDALP #1 -------------------------' ) ;
				update nDAL1P ;
			}
			
			if ( cDAM1.keySet ().size () > 0 )
			{
				System.debug ( '------------------------- INSERT cDAL #1 -------------------------' ) ;

				list<Contact> cDAL1 = new list<Contact> () ;
				
				for ( String s : cDAM1.keyset () )
				{
					System.debug ( 'Adding key - "' + s + '"' ) ;
					Contact c = cDAM1.get ( s ) ;
					cDAL1.add ( c ) ;
				}
								
				insert cDAL1 ;
				
				for ( Contact c : CDAL1 )
				{
					if ( cDAM1.containsKey ( c.TaxID__c ) )
					{
						cDAM1.get ( c.TaxID__c ).ID = c.ID ;
					}
				}
				
				for ( Deposit_Application__c da : nDAL1 )
				{
					if ( da.Tax_Reported_Contact__c == null )
						da.Tax_Reported_Contact__c = cDAM1.get ( da.Applicant_SSN__c ).ID ;
				}
					
				System.debug ( '------------------------- UPDATE NDAL #1 -------------------------' ) ;
				update nDAL1 ;
			}
		}
		
		//  -----------------------------------------------------------------------------------
		//  Deposit Application Update #2
		//  -----------------------------------------------------------------------------------
		if ( nDAL2.size () > 0 )
		{
			for ( Deposit_Application__c da : nDAL2 )
			{
				System.debug ( 'Deposit Application Update #2 - Checking - ' + da.Co_Applicant_Email__c + ' --> ' + da.Co_Applicant_SSN__c ) ;
				
				//  They could be an applicant on another application?!?
				if ( ( ! cDAM1.containsKey ( da.Co_Applicant_SSN__c ) ) && 
				     ( ! cDAM2.containsKey ( da.Co_Applicant_SSN__c ) ) && 
				     ( ! DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Co_Applicant_SSN__c ) ) )
				{
					Contact c = new Contact () ;
					c.recordTypeID = recordTypeID ;
                    if ( String.IsNotEmpty ( accountID ) ) c.AccountID = accountID ;
					c.FirstName = da.Co_Applicant_First_Name__c ;
	//				c.MiddleName = da.Co_Applicant_Middle_Name__c ;
					c.LastName = da.Co_Applicant_Last_Name__c ;
	//				c.Suffix = da.Co_Applicant_Suffix__c ;
					c.Email = da.Co_Applicant_Email__c ;
					c.TaxID__c = da.Co_Applicant_SSN__c ;
					c.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
					
					c.MailingStreet = da.Co_Applicant_Address__c ;
					c.MailingCity = da.Co_Applicant_City__c ;
					c.MailingState = da.Co_Applicant_State__c ;
					c.MailingPostalCode = da.Co_Applicant_Zip__c ;
					
					c.Phone = da.Co_Applicant_Daytime_Phone__c ;
					c.Evening_Phone__c = da.Co_Applicant_Evening_Phone__c ;
					
					cDAM2.put ( da.Co_Applicant_SSN__c, c ) ;
				}
				else
				{
					System.debug ( 'Duplicate SSN found :: ' + da.Co_Applicant_SSN__c + ' CHECKING PRIOR' ) ;
					
					//  If it's in here we've aleady updated it
					if ( DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Co_Applicant_SSN__c ) )
					{
						ID pContactID = DepositApplicationHelper.contactTaxIDMap.get ( da.Co_Applicant_SSN__c ) ;
						
						System.debug ( 'Updating with prior contact' ) ;
						da.Tax_Reported_Contact__c = pContactID ;
						nDAL2P.add ( da ) ;
					}
					else
					{
						System.debug ( 'Genuine duplicate found!' ) ;
					}
				}
			}
			
			if ( nDAL2P.size () > 0 )
			{
				System.debug ( '------------------------- UPDATE nDALP #2 -------------------------' ) ;
				update nDAL2P ;
			}
			
			if ( cDAM2.keySet ().size () > 0 )
			{
				System.debug ( '------------------------- INSERT cDAL #2 -------------------------' ) ;
				
				list<Contact> cDAL2 = new list<Contact> () ;
				
				for ( String s : cDAM2.keyset () )
				{
					Contact c = cDAM2.get ( s ) ;
					cDAL2.add ( c ) ;
				}
								
				insert cDAL2 ;
				
				for ( Contact c : CDAL2 )
				{
					if ( cDAM2.containsKey ( c.TaxID__c ) )
					{
						cDAM2.get ( c.TaxID__c ).ID = c.ID ;
					}
				}
				
				for ( Deposit_Application__c da : nDAL2 )
				{
					//  If they don't exist in co-applicant set, they may be in applicant set instead!
					if ( cDAM2.containsKey ( da.Co_Applicant_SSN__c ) )
						da.Co_Applicant_Tax_Reported_Contact__c = cDAM2.get ( da.Co_Applicant_SSN__c ).ID ;
					else if ( cDAM1.containsKey ( da.Co_Applicant_SSN__c ) )
						da.Co_Applicant_Tax_Reported_Contact__c = cDAM1.get ( da.Co_Applicant_SSN__c ).ID ;
				}
					
				System.debug ( '------------------------- UPDATE NDAL #2 -------------------------' ) ;
				update nDAL2 ;
			}
		}
		
		//  -----------------------------------------------------------------------------------
		//  Deposit Application Update #3
		//  -----------------------------------------------------------------------------------
		if ( nDAL3.size () > 0 )
		{
			for ( Deposit_Application__c da : nDAL3 )
			{
				System.debug ( 'Deposit Application Update #3 - Checking - ' + da.Beneficiary_Email__c + ' --> ' + da.Beneficiary_SSN__c ) ;
				
				if ( ( ! cDAM3.containsKey ( da.Beneficiary_SSN__c ) ) && 
				     ( ! DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Beneficiary_SSN__c ) ) )
				{
					Contact c = new Contact () ;
					c.recordTypeID = recordTypeID ;
                    if ( String.IsNotEmpty ( accountID ) ) c.AccountID = accountID ;
					c.FirstName = da.Beneficiary_First_Name__c ;
	//				c.MiddleName = da.Beneficiary_Middle_Name__c ;
					c.LastName = da.Beneficiary_Last_Name__c ;
	//				c.Suffix = da.Beneficiary_Suffix__c ;
					c.Email = da.Beneficiary_Email__c ;
					c.TaxID__c = da.Beneficiary_SSN__c ;
					
					c.MailingStreet = da.Beneficiary_Address__c ;
					c.MailingCity = da.Beneficiary_City__c ;
					c.MailingState = da.Beneficiary_State__c ;
					c.MailingPostalCode = da.Beneficiary_Zip__c ;
					c.Date_Last_Deposit_App__c = da.Created_DateTime__c ;
					
					c.Phone = da.Beneficiary_Daytime_Phone__c ;
					c.Evening_Phone__c = da.Beneficiary_Evening_Phone__c ;
				
					cDAM3.put ( da.Beneficiary_SSN__c, c ) ;
				}
				else
				{
					System.debug ( 'Duplicate SSN found :: ' + da.Beneficiary_SSN__c + ' CHECKING PRIOR' ) ;
					
					//  If it's in here we've aleady updated it
					if ( DepositApplicationHelper.contactTaxIDMap.containsKey ( da.Beneficiary_SSN__c ) )
					{
						ID pContactID = DepositApplicationHelper.contactTaxIDMap.get ( da.Beneficiary_SSN__c ) ;
						
						System.debug ( 'Updating with prior contact' ) ;
						da.Tax_Reported_Contact__c = pContactID ;
						nDAL3P.add ( da ) ;
					}
					else
					{
						System.debug ( 'Genuine duplicate found!' ) ;
					}
				}
			}
			
			if ( nDAL3P.size () > 0 )
			{
				System.debug ( '------------------------- UPDATE nDALP #3 -------------------------' ) ;
				update nDAL3P ;
			}
			
			if ( cDAM3.keySet ().size () > 0 )
			{
				System.debug ( '------------------------- INSERT cDAL #3 -------------------------' ) ;
				
				list<Contact> cDAL3 = new list<Contact> () ;
				
				for ( String s : cDAM3.keyset () )
				{
					Contact c = cDAM3.get ( s ) ;
					cDAL3.add ( c ) ;
				}
								
				insert cDAL3 ;
				
				for ( Contact c : cDAL3 )
				{
					if ( cDAM3.containsKey ( c.TaxID__c ) )
					{
						cDAM3.get ( c.TaxID__c ).ID = c.ID ;
					}
				}
				
				for ( Deposit_Application__c da : nDAL3 )
				{
					da.Beneficiary_Tax_Reported_Contact__c = cDAM3.get ( da.Beneficiary_SSN__c ).ID ;
				}
					
				System.debug ( '------------------------- UPDATE NDAL #3 -------------------------' ) ;
				update nDAL3 ;
			}
		}
		
        //  -----------------------------------------------------------------------------------
		//  Setting Up
		//  -----------------------------------------------------------------------------------
		list<String> lAdditionalInfoIDs = new list<String> () ;
		list<String> lDeniedIDs = new list<String> () ;
		list<String> lFCRADeniedIDs = new list<String> () ;
		
		//  -----------------------------------------------------------------------------------
		//  MAIN LOOP ROUND #2 - EMAIL
		//  -----------------------------------------------------------------------------------
		for ( Deposit_Application__c n : Trigger.new )
		{
			//  --------------------------------------------------------------
			//  INSERT
			//  --------------------------------------------------------------
			if ( Trigger.isInsert )
			{
				//  --------------------------------------------------------------
				//  Additional Info Emails
				//  --------------------------------------------------------------
				if ( ( n.Application_Processing_Status__c != null ) &&
				     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
				{
                    System.debug ( 'INSERT Additional Info Email - ADD #1' ) ;
                    
                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.additionalMap, n.Id ) )
                        lAdditionalInfoIDs.add ( n.Id ) ;
				}
				
				//  --------------------------------------------------------------
				//  Declined Emails
				//  --------------------------------------------------------------
				if ( String.isNotBlank ( n.Current_Status__c ) )
				{
					if ( n.Current_Status__c.equalsIgnoreCase ( 'Failed (AFTER REVIEW)' ) )
					{
						if ( String.isNotBlank ( n.Debit_History_Response__c ) && ( ! n.Debit_History_Response__c.equalsIgnoreCase ( 'Review' ) ) )
						{
		                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.denyMap, n.Id ) )
		                    {
			                    System.debug ( 'INSERT DENY Email - ADD #1' ) ;
								lDeniedIDs.add ( n.Id ) ;
		                    }
						}
					}
				}
				
				//  --------------------------------------------------------------
				//  FCRA Declined Emails
				//  --------------------------------------------------------------
				if ( String.isNotBlank ( n.Current_Status__c ) )
				{
					if ( n.Current_Status__c.equalsIgnoreCase ( 'Failed (AFTER REVIEW)' ) )
					{
						if ( String.isNotBlank ( n.Debit_History_Response__c ) && ( n.Debit_History_Response__c.equalsIgnoreCase ( 'Review' ) ) )
						{
		                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.fcraDenyMap, n.Id ) )
		                    {
			                    System.debug ( 'INSERT FCRA DENY Email - ADD #1' ) ;
								lFCRADeniedIDs.add ( n.Id ) ;
		                    }
						}
					}
				}
			}
			
			//  --------------------------------------------------------------
			//  UPDATE
			//  --------------------------------------------------------------
			if ( Trigger.isUpdate )
			{
				//  --------------------------------------------------------------
				//  Get old data for comparison
				//  --------------------------------------------------------------
				Deposit_Application__c o = Trigger.oldMap.get ( n.ID ) ;
				
				//  --------------------------------------------------------------
				//  Additional Info Emails on status change
				//  --------------------------------------------------------------
				if ( ( o.Application_Processing_Status__c == null ) ||
				     ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
				{
					if ( ( n.Application_Processing_Status__c != null ) &&
					     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
					{
	                    System.debug ( 'UPDATE Additional Info Email - Status change - ADD #1' ) ;
	                    
	                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.additionalMap, n.Id ) )
	                        lAdditionalInfoIDs.add ( n.Id ) ;
					}
				}
				else
				//  --------------------------------------------------------------
				//  Additional Info Emails on status change, time Based
				//  --------------------------------------------------------------
				{
					if ( ( n.Application_Processing_Status__c != null ) &&
					     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
					{
						if ( n.Last_Additional_Info_Email_Date__c != null )
						{
						 	date cd = n.Last_Additional_Info_Email_Date__c ;
			
							Integer diff = Math.abs ( cd.daysBetween ( Date.today () ) ) ;
							Long remainder = Math.mod ( diff, 7 ) ;
							
							System.debug ( 'Diff : ' + diff + ' Remainder : ' + remainder ) ;
							
							if ( ( remainder == 0 ) && ( diff <= 8 ) && ( ! cd.isSameDay ( Date.today () ) ) )
							{
								System.debug ( 'Additional Email - Status change time - ADD' ) ;
			                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.additionalMap, n.Id ) )
									lAdditionalInfoIDs.add ( n.Id ) ;
							}
						}
					}
				}
				
				//  --------------------------------------------------------------
				//  Denied Emails on status change
				//  --------------------------------------------------------------
				if ( ( ( o.Current_Status__c == null ) || ( ( o.Current_Status__c != null ) && ( ! o.Current_Status__c.equalsIgnoreCase ( 'Failed (AFTER REVIEW)' ) ) ) ) &&
				 	 ( ( n.Current_Status__c != null ) && ( n.Current_Status__c.equalsIgnoreCase ( 'Failed (AFTER REVIEW)' ) ) ) )
			    {
			   		if ( ( n.Debit_History_Response__c != null ) && ( n.Debit_History_Response__c.equalsIgnoreCase ( 'Review' ) ) )
			   		{
						System.debug ( 'UPDATE FCRA DECLINE Email - ADD #1' ) ;
	                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.fcraDenyMap, n.Id ) )
				   			lFCRADeniedIDs.add ( n.ID ) ;
			   		}
			   		else
			   		{
						System.debug ( 'UPDATE DECLINE Email - ADD #1' ) ;
	                    if ( ! DepositApplicationHelper.checkDuplicate ( DepositApplicationHelper.denyMap, n.Id ) )
			   				lDeniedIDs.add ( n.ID ) ;
			   		}
			    }
			}
		}
		
		//  -----------------------------------------------------------------------------------
		//  Combined Email list
		//  -----------------------------------------------------------------------------------
		System.debug ( 'Sending Emails? - I: ' + lAdditionalInfoIDs.size () + ' D: ' + lDeniedIds.size () + ' DF: ' + lFCRADeniedIDs.size () ) ;
		
		if ( ( lAdditionalInfoIDs.size () > 0 ) ||
		     ( lDeniedIds.size () > 0 ) ||
		     ( lFCRADeniedIDs.size () > 0 ) )
		{
			DepositApplicationEmailFunctions.sendDepositApplicationEmails ( lAdditionalInfoIDs, lDeniedIDs, lFCRADeniedIDs ) ;
		}
		
		System.debug ( '------------------------------ AFTER END ------------------------------' ) ;
	}
}