trigger AdvocateBadgeAwardedTrigger on Advocate_Badge_Awarded__c (before insert, before update) 
{
    /* -----------------------------------------------------------------------------------
	 * Load badgers
	 * ----------------------------------------------------------------------------------- */
   	map<ID,Advocate_Badge__c> abMap = null ;
    
    try
    {
        abMap = new map <ID,Advocate_Badge__c> ([
            SELECT ID, Badge_Points__c
            FROM Advocate_Badge__c 
        ]) ;
    }
    catch ( Exception e )
    {
        // ?
    }
    
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
        Set<ID> cSet = new Set<ID> () ;
        
		//  Setup loop
        for ( Advocate_Badge_Awarded__c n : Trigger.new )
        {
            //  Check for awards with contacts and without referral codes
            if ( ( n.Contact__c != null ) && ( n.Referral_Code__c == null ) )
            {
                cSet.add ( n.Contact__c ) ;
            }
        }
        
        map<ID,List<Referral_Code__c>> rclMap = new map<ID,List<Referral_Code__c>> () ;
        
        //  If we have any, look up the referral code for that contact
        if ( cSet.size () > 0 )
        {
            list<Referral_Code__c> rcL = null ;
            
            //  assuming proper order, will have to test
            try
            {
                rcL = [
                    SELECT ID, Name, Contact__c
                    FROM Referral_Code__c
                    WHERE Contact__c IN :cSet
                    ORDER BY Contact__c, CreatedDate DESC
                    ] ;
            }
            catch ( DMLException e )
            {
                rcL = null ;
            }
            
            //  Sort referral codes returned into hash by contact if there are any
            if ( ( rcL != null ) && ( rcL.size () > 0 ) )
            {
                for ( Referral_Code__c rc : rcL )
                {
                 	if ( rclMap.containsKey ( rc.Contact__c ) )
                    {
                        rclMap.get ( rc.Contact__c ).add ( rc ) ;
                    }
                    else
                    {
                        list<Referral_Code__c> rcx = new list<Referral_Code__c> () ;
                        rcx.add ( rc ) ;
                        
                        rclMap.put ( rc.Contact__c, rcx ) ;
                    }
                }
            }
        }

		//  fix loop
        for ( Advocate_Badge_Awarded__c n : Trigger.new )
        {
            //  Check for awards with contacts and without referral codes
            if ( ( n.Contact__c != null ) && ( n.Referral_Code__c == null ) )
            {
                if ( rclMap.containsKey ( n.Contact__c ) )
                {
                    n.Referral_Code__c = rclMap.get ( n.Contact__c ) [ 0 ].ID ;
                }
            }
            
            //  fix other thing
            if ( ( abMap != null ) && ( n.Awarded_Badge_Points__c == null ) && ( n.Badge__c != null ) )
                n.Awarded_Badge_Points__c = abMap.get ( n.Badge__c ).Badge_Points__c ;
        }
    }
}