trigger EventTrigger on Event ( after insert ) 
{
	/* -----------------------------------------------------------------------------------
	 * Record Types & ETC
	 * ----------------------------------------------------------------------------------- */
	String communityRoomRecordTypeId = OneUnitedUtilities.getRecordTypeIdForObject ( 'Community Room', 'Event' ) ;
	String publicRoomId = Web_Settings__c.getInstance ( 'Web Forms' ).Crenshaw_Community_Room_ID__c ;
	 
	/* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
		/* -----------------------------------------------------------------------------------
		 * Setup for follows
		 * ----------------------------------------------------------------------------------- */
		List<EventRelation> erList = new List<EventRelation> () ; 
		
		/* -----------------------------------------------------------------------------------
		 * LOOP
		 * ----------------------------------------------------------------------------------- */
		for ( Event n : Trigger.new )
		{
			//  ------------------------------------------------------------------------
			//  On Insert
			//  ------------------------------------------------------------------------
			if ( Trigger.isInsert )
			{
				if ( n.RecordTypeId == communityRoomRecordTypeId )
				{
					//  Invite the public community room to the event
					EventRelation er = new EventRelation () ;
					
					er.EventId = n.Id ;
					er.RelationId = publicRoomId ;
					er.Status = 'Accepted' ;
					
					erList.add ( er ) ;
				}
			}
		}
		
		insert erList ;
	}
}