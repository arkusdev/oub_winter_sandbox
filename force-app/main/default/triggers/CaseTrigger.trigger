trigger CaseTrigger on Case (before insert, after insert, after update) 
{
    //  Watson speech to text callouts
    list<ID> wcL = new list<ID> () ;
    
	//  Need the record type
	String lrRT = Schema.SObjectType.Case.RecordTypeInfosByName.get('Loan Referral').RecordTypeId ;
	String psRT = Schema.SObjectType.Case.RecordTypeInfosByName.get('Phone Support').RecordTypeId ;
    
	//  Map of contacts & leads to store date/time and iterate over 
	Map<String,DateTime> contactIDMap = new Map<String,DateTime> () ;
	Map<String,DateTime> leadIDMap = new Map<String,DateTime> () ;
	
    Pattern p = Pattern.compile ( '(.+)\\+1([0-9]+)\\s(.*)' ) ;
    
    if(trigger.isBefore)
    {
        Pattern p = Pattern.compile ( '(.+)\\+1([0-9]+)\\s(.*)' ) ;
        
        Set<String> x = new Set<String> () ;
        Set<String> y = new Set<String> () ;
        for ( Case n : Trigger.new )
		{
            if ( String.isNotBlank(n.SuppliedEmail) && OneUnitedUtilities.isValidEmail(n.SuppliedEmail) && (n.ContactId ==null))
                x.add ( n.SuppliedEmail ) ; // Add all emails w/o an associated contact
            
            if ( ( String.isNotBlank ( n.Subject ) ) && ( n.recordTypeID == psRT ) )
            {
		        Matcher m = p.matcher ( n.Subject ) ;
		        if ( m.matches () )
                {
					String phone = m.group ( 2 ).substring ( 0, 3 ) + '-' + m.group ( 2 ).substring ( 3, 6 ) + '-' + m.group ( 2 ).substring ( 6, 10 ) ;
                    y.add ( phone ) ;
                }
            }
        }
        
        list<Contact> cL1 =[
            SELECT ID, Email
            FROM Contact
            WHERE Email IN :x
            ORDER BY Contact.Created_Date__c
        ] ;// Push in this info for contacts associated with the sender address
        // Order the list by oldest contact with email address (figure out how to fine tune the filter to age)
        map<String,Contact> cM1 = new map<String,Contact> () ;
        
        for ( Contact c : cL1 ) // For each member of cL
        {
            
            //if(OneUnitedUtilities.isValidEmail(c.Email))
            //{ Commented out this conditional because the addresses are already vetted
                if ( ! cM1.containsKey ( c.Email ) )
                cM1.put ( c.Email, c ) ; // Push in the contact (w/ address as key) if the map doesn't already have
            //}                
        }
        
        list<Contact> cL2 = [
            SELECT ID, Phone, MobilePhone
            FROM Contact
            WHERE Phone IN :y
            OR MobilePhone IN :y
        ] ;
        
        map<String,Contact> cM2 = new map<String,Contact> () ;
        For ( Contact c : cL2 )
        {
            if ( ( String.isNotBlank ( c.Phone ) ) && ( ! cM2.containsKey ( c.Phone ) ) )
                cM2.put ( c.Phone, c ) ;
            if ( ( String.isNotBlank ( c.MobilePhone ) ) && ( ! cM2.containsKey ( c.MobilePhone ) ) )
                cM2.put ( c.MobilePhone, c ) ;
        }
        
        for ( Case n : Trigger.new ) // For all cases with new records
        {
            if ( String.isNotBlank(n.SuppliedEmail) && OneUnitedUtilities.isValidEmail(n.SuppliedEmail) && (n.ContactId ==null))
            {// If the email address isn't blank and the contact id is null
                if ( cM1.containsKey ( n.SuppliedEmail ) )// If the address is a key in the map
                    n.ContactID = cM1.get ( n.SuppliedEmail ).ID ; // The contact id for the case equals the Id associated in the map
            }
            
            if ( ( String.isNotBlank ( n.Subject ) ) && ( n.recordTypeID == psRT ) )
            {
		        Matcher m = p.matcher ( n.Subject ) ;
		        if ( m.matches () )
                {
					String phone = m.group ( 2 ).substring ( 0, 3 ) + '-' + m.group ( 2 ).substring ( 3, 6 ) + '-' + m.group ( 2 ).substring ( 6, 10 ) ;
                    
                    if ( cM2.containsKey ( phone ) )
                        n.ContactId = cM2.get ( phone ). ID ;
                }
            }
        }
    }
    
    if ( trigger.isAfter )
    {
        /* -----------------------------------------------------------------------------------
         * LOOP
         * ----------------------------------------------------------------------------------- */
        for ( Case n : Trigger.new )
        {
            Case o = null ;
            if ( Trigger.isUpdate )
            	o = Trigger.oldMap.get ( n.ID ) ;
            
            //  Load the associated contacts and leads into a List
            if ( ( n.RecordTypeId != null ) && ( n.RecordTypeId == lrRT ) )
            {
                System.debug ( 'Found case : "' + n.Id + '" Contact : "' + n.ContactId + '" Lead : "' + n.Lead__c + '"' ) ;
                
                if ( n.ContactId != null )
                {
                    contactIDMap.put ( n.ContactId, n.CreatedDate ) ;
                }
                
                if ( n.Lead__c != null )
                {
                    leadIDMap.put ( n.Lead__c, n.CreatedDate ) ;
                }
            }
            else
            {
                if ( ( String.isNotBlank ( n.Voicemail_Translation_Status__c ) ) && ( n.Voicemail_Translation_Status__c.equalsIgnoreCase ( 'Request Translation' ) ) )
                {
                    if ( o == null )
                    {
                        if ( String.isBlank ( n.Voicemail_Translation__c ) )
                            wcL.add ( n.ID ) ;
                    }
                    else if ( ( String.isBlank ( o.Voicemail_Translation_Status__c ) ) ||
                          ( ( String.isNotBlank ( o.Voicemail_Translation_Status__c ) ) && ( ! o.Voicemail_Translation_Status__c.equalsIgnoreCase ( 'Request Translation' ) ) ) )
                    {
                        if ( String.isBlank ( n.Voicemail_Translation__c ) )
                            wcL.add ( n.ID ) ;
                    }
                }
            }
        }
        
        //  Query the contact list
        if ( contactIDMap.size () > 0 )
        {
            Set<String> cIS = contactIDMap.keySet () ;
            List<Contact> lC = [ Select c.Id, c.Loan_Referral_Date__c FROM Contact c where c.id IN :cIS ] ;
            
            
            for ( Contact c: lC )
            {
                if ( Trigger.isInsert )
                {
                    System.debug ( 'New Case for Contact "' + c.Id + '" Auto Update' ) ;
                    c.Loan_Referral_Date__c = contactIDMap.get ( c.Id ) ;
                }
                
                if ( Trigger.isUpdate )
                {
                    System.debug ( 'Updated Case for Contact "' + c.Id + '" Checking' ) ;
                    if ( c.Loan_Referral_Date__c != null )
                    {
                        if ( contactIDMap.get ( c.Id ) > c.Loan_Referral_Date__c )
                        {
                            c.Loan_Referral_Date__c = contactIDMap.get ( c.Id ) ;
                        }
                    }
                    else
                    {
                        c.Loan_Referral_Date__c = contactIDMap.get ( c.Id ) ;
                    }
                }
            }
            
            update lC ;
        }
        
        //  Query the lead list
        if ( leadIdMap.size () > 0 )
        {
            Set<String> lIS = leadIDMap.keySet () ;
            List<Lead> lL = [ Select l.Id, l.Loan_Referral_Date__c FROM Lead l where l.id IN :lIS ] ;
            
            for ( Lead l: lL )
            {
                if ( Trigger.isInsert )
                {
                    l.Loan_Referral_Date__c = leadIDMap.get ( l.Id ) ;
                }
                
                if ( Trigger.isUpdate )
                {
                    if ( l.Loan_Referral_Date__c != null )
                    {
                        if ( leadIDMap.get ( l.Id ) > l.Loan_Referral_Date__c )
                        {
                            l.Loan_Referral_Date__c = leadIDMap.get ( l.Id ) ;
                        }
                    }
                    else
                    {
                        l.Loan_Referral_Date__c = leadIDMap.get ( l.Id ) ;
                    }
                }
            }
            
            update lL ;
        }
        
        //  ----------------------------------------------------------
        //  Watson callout
        //  ----------------------------------------------------------
        List<String> caseIds = new List<String>();
        if ( ! System.isQueueable () )
        {
            if ( ( wcL != null ) && ( wcL.size () > 0 ) )
            {
                IBM_Watson_Settings__c iws = IBM_Watson_Settings__c.getOrgDefaults () ;
                
                for ( Id appId:wcL ) 
                {
                    caseIds.add(appId);
                    if ( caseIds.size() == iws.API_Callout_Limit__c )
                    {
                        IBMWatsonWAVtoSpeechQueueable qW = new IBMWatsonWAVtoSpeechQueueable ( caseIds ) ;
                        System.enqueueJob ( qW ) ;
                        caseIds = new List<String>();
                    }
                }
            
                if ( caseIds.size () > 0 )
                {
                    IBMWatsonWAVtoSpeechQueueable qw2 = new IBMWatsonWAVtoSpeechQueueable ( caseIds ) ;
                    System.enqueueJob ( qw2 ) ;
                }
            }
        }
    }
}