trigger ContactTrigger on Contact (before insert, after insert, before update, after update ) 
{
    /* -----------------------------------------------------------------------------------
	 * SETUP
	 * ----------------------------------------------------------------------------------- */
    private static ID alsDefaultID = ContactHelper.getAdvocateDefaultID () ;
    private static String customerRTID = ContactHelper.getCustomerRTID () ;
    private static ID alsQuitID = ContactHelper.getAdvocateQuitID () ;
    private static map<String,OneCampaign__c> xocM = ContactHelper.getCampaignMap () ;
    
    list<Automated_Email__c> aeL = new list<Automated_Email__c> () ;
    list<OneCampaign_Member__c> ocmL = new list<OneCampaign_Member__c> () ;
    
    if ( ContactHelper.closedSurveyMap == null )
        ContactHelper.closedSurveyMap = new map<String, boolean> () ;
    
    /* -----------------------------------------------------------------------------------
	 * BEFORE TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isBefore )
	{
        /* -----------------------------------------------------------------------------------
         * LOOP
         * ----------------------------------------------------------------------------------- */
        for ( Contact n : Trigger.new )
        {
			//  --------------------------------------------------------------
            //  SMS due to Exact Target stupid formatting
			//  --------------------------------------------------------------
            if ( String.isNotBlank ( n.SMS_Number__c ) )
            {
                if ( ( n.SMS_Phone__c == null ) || ( ! n.SMS_Phone__c.equalsIgnoreCase ( n.SMS_Number__c ) ) )
                    n.SMS_Phone__c = n.SMS_Number__c ;
            }

            //  --------------------------------------------------------------
			//  Advocate
			//  --------------------------------------------------------------
            if ( n.Advocate_Anniversary_Awarded__c == null )
                n.Advocate_Anniversary_Awarded__c = 0.0 ;
            
            if ( n.Advocate_Deposit_Acct_Last_Awarded__c == null )
                n.Advocate_Deposit_Acct_Last_Awarded__c = 0.0 ;

            if ( alsDefaultID != null )
            {
                if ( ( n.Has_Active_Account__c == true ) && ( n.Advocate__c == true ) && ( n.Advocate_Level__c == null ) )
                	n.Advocate_Level__c = alsDefaultID ;
            }
            
			//  --------------------------------------------------------------
			//  De-duplication / Lead Source
			//  --------------------------------------------------------------
            if ( Trigger.isInsert )
            {
                if ( String.isNotEmpty ( n.LeadSource ) )
                    n.LeadSource_Change_Date__c = System.now () ;
                
                if ( ( String.IsNotBlank ( n.FirstName ) ) && ( String.isNotBlank ( n.LastName ) ) && ( ( String.isNotBlank ( n.Email ) ) || ( String.isNotBlank ( n.Computed_Phone__c ) ) ) )
                {
                    n.Deduplication_Flag__c = true ;
                }
            }
            else if ( Trigger.isUpdate )
            {
				//  Get old data for comparison
				Contact o = Trigger.oldMap.get ( n.ID ) ;
                
                if ( ( String.isNotEmpty ( n.LeadSource ) ) && ( ! n.LeadSource.equalsIgnoreCase ( o.LeadSource ) ) )
                    n.LeadSource_Change_Date__c = System.now () ;
                
                //  Record type change
                if ( ( o.RecordTypeId != null ) && ( n.RecordTypeId != null ) && ( n.RecordTypeId != o.RecordTypeId ) )
                {
                    if ( ( String.IsNotBlank ( n.FirstName ) ) && ( String.isNotBlank ( n.LastName ) ) && ( ( String.isNotBlank ( n.Email ) ) || ( String.isNotBlank ( n.Computed_Phone__c ) ) ) )
                    {
                        n.Deduplication_Flag__c = true ;
                    }
                }
                
                //  Deduplication, Customer
                if ( ( n.RecordTypeId == customerRTID ) ) 
                {
                    //  Name is required to be set
                    if ( ( String.isNotBlank ( n.FirstName ) ) && ( String.isNotBlank ( n.LastName ) ) )
                    {
                        //  Email check
                        if ( ( String.IsBlank ( o.Email ) ) && ( String.isNotBlank ( n.Email ) ) )
                        {
                            n.Deduplication_Flag__c = true ;
                        }
                        else if ( ( String.isNotBlank ( o.Email ) ) && ( String.isNotBlank ( n.Email ) ) && ( ! n.Email.EqualsIgnoreCase ( o.Email ) ) )
                        {
                            n.Deduplication_Flag__c = true ;
                        }
                        
                        //  Phone check
                        if ( ( String.IsBlank ( o.Computed_Phone__c ) ) && ( String.isNotBlank ( n.Computed_Phone__c ) ) )
                        {
                            n.Deduplication_Flag__c = true ;
                        }
                        else if ( ( String.isNotBlank ( o.Computed_Phone__c ) ) && ( String.isNotBlank ( n.Computed_Phone__c ) ) && ( ! n.Computed_Phone__c.EqualsIgnoreCase ( o.Computed_Phone__c ) ) )
                        {
                            n.Deduplication_Flag__c = true ;
                        }
                        
                        //  Force Deduplication!?
                        if ( ( o.DeDuplication_Date__c == null ) && ( n.DeDuplication_Date__c != null ) )
                        {
                            n.Deduplication_Flag__c = true ;
                        }
                        else if ( ( o.DeDuplication_Date__c != null ) && ( n.DeDuplication_Date__c != null ) && ( o.DeDuplication_Date__c != n.DeDuplication_Date__c ) )
                        {
                            n.Deduplication_Flag__c = true ;
                        }
                    }
                }
                
                if ( ( o.Personal_Deposit_Accounts__c != 0 ) && ( n.Personal_Deposit_Accounts__c == 0 ) )
                {
                    n.Active_ATM_Card__c = false ;
                    n.Active_Debit_Card__c = false ;
                    n.Mobile_Banking_Active__c = false ;
                    n.New_Deposit_Journey_Start_Date__c = null ;
                    n.Online_Banking_Active__c = false ;
                }
            }
            
            //  --------------------------------------------------------------
            //  QUIT?!? - Survey
            //  --------------------------------------------------------------
            if ( ( n.RecordTypeId == customerRTID ) && ( n.Personal_Deposit_Accounts__c == 0.0 ) && ( n.Closed_Deposit_Accounts__c > 0.0 ) &&
                ( n.Most_Recent_Closed_Financial_Account__c != null ) && ( n.Sent_Closed_Account_Survey__c == null ) )
            {
                if ( ! ContactHelper.checkDuplicate ( ContactHelper.closedSurveyMap, n.Id ) )
                {
                    Automated_Email__c ae = new Automated_Email__c () ;
                    
                    ae.Contact_OR_Account__c = 'Contact' ;
                    ae.Contact__c = n.ID ;
                    ae.Is_A_Survey__c = 'Y' ;
                    ae.Email_Address__c = n.Email ;
                    ae.First_Name__c = n.FirstName ;
                    ae.Last_Name__c = n.LastName ;
                    ae.Template_Name__c = 'closed-account-survey' ;
                    ae.Merge_Var_Name_1__c = 'Survey' ;
                    ae.Merge_Var_Value_1__c = ContactHelper.getSurveyID () ;
                    ae.Merge_Var_Name_2__c = 'Related_Object_Field' ;
                    ae.Merge_Var_Value_2__c = 'Financial_Account__c' ;
                    ae.Merge_Var_Name_3__c = 'Related_Object_ID' ;
                    ae.Merge_Var_Value_4__c = n.Most_Recent_Closed_Financial_Account__c ;
                    
                    aeL.add ( ae ) ;
                    
                    n.Sent_Closed_Account_Survey__c = Date.today () ;
                }
            }
        }
    }
    
    /* -----------------------------------------------------------------------------------
	 * AFTER TRIGGER
	 * ----------------------------------------------------------------------------------- */
	if ( Trigger.isAfter )
	{
        /* -----------------------------------------------------------------------------------
         * SETUP
         * ----------------------------------------------------------------------------------- */
        list<Contact> rcL = new list<Contact> () ;
        List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> reallyLongNameList = new List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        map<ID,Contact> cMap = new map<ID,Contact> () ;
        
        /* -----------------------------------------------------------------------------------
         * LOOP
         * ----------------------------------------------------------------------------------- */
        for ( Contact n : Trigger.new )
        {
            ID oldAdvocateLevel = null ;
            boolean oldHazSocial = false ;
            boolean oldDirectDeposit = false ;
            boolean oldPopMoney = false ;
            boolean oldMobileRemoteDeposit = false ;
            boolean oldFundsTransfer = false ;
            
            if ( Trigger.isInsert )
            {
                //  --------------------------------------------------------------
                //  OneTransaction campaign
                //  --------------------------------------------------------------
                if ( String.isNotBlank ( n.OneTransaction_Focus__c ) )
                {
                    String key = 'MC OneTransaction ' + n.OneTransaction_Focus__c ;
                    
                    if ( xocM.containsKey ( key.toLowerCase () ) )
                    {
                        OneCampaign_Member__c ocm = new OneCampaign_Member__c () ;
                        ocm.Contact__c = n.ID ;
                        
                        ocm.Campaign__c = xocM.get ( key.toLowerCase () ).ID ;
                        ocmL.add ( ocm ) ;
                    }
                }
            }
            else if ( Trigger.isUpdate )
            {
                //  --------------------------------------------------------------
                //  Get old data for comparison
                //  --------------------------------------------------------------
                Contact o = Trigger.oldMap.get ( n.ID ) ;
                
                //  --------------------------------------------------------------
                //  Advocate level
                //  --------------------------------------------------------------
                oldAdvocateLevel = o.Advocate_Level__c ;
                
                //  --------------------------------------------------------------
                //  Social info
                //  --------------------------------------------------------------
                if ( String.isNotBlank ( o.Facebook_Handle__c ) )
                    oldHazSocial = true ;
                else if ( String.isNotBlank ( o.Twitter_Handle__c ) )
                    oldHazSocial = true ;
                else if ( String.isNotBlank ( o.Instagram_Handle__c ) )
                    oldHazSocial = true ;
                
                //  --------------------------------------------------------------
                //  Direct Deposit
                //  --------------------------------------------------------------
                oldDirectDeposit = o.Direct_Dep__c ;
                
                //  --------------------------------------------------------------
                //  Pop Money
                //  --------------------------------------------------------------
                oldPopMoney = o.PopMoney__c ;
                
                //  --------------------------------------------------------------
                //  Mobile Remote
                //  --------------------------------------------------------------
                oldMobileRemoteDeposit = o.Mobile_Remote_Deposit__c ;
                
                //  --------------------------------------------------------------
                //  Mobile Remote
                //  --------------------------------------------------------------
                oldFundsTransfer = o.Funds_Transfer__c ;
                
                //  --------------------------------------------------------------
                //  OneTransaction campaign
                //  --------------------------------------------------------------
                String key = 'MC OneTransaction ' + n.OneTransaction_Focus__c ;
                
                if ( ( String.isBlank ( o.OneTransaction_Focus__c ) ) && ( String.isNotBlank ( n.OneTransaction_Focus__c ) ) )
                {
                    if ( xocM.containsKey ( key.toLowerCase () ) )
                    {
                        OneCampaign_Member__c ocm = new OneCampaign_Member__c () ;
                        ocm.Contact__c = n.ID ;
                        
                        ocm.Campaign__c = xocM.get ( key.toLowerCase () ).ID ;
                        ocmL.add ( ocm ) ;
                    }
                }
                else if ( ( String.isNotBlank ( o.OneTransaction_Focus__c ) ) && ( String.isNotBlank ( n.OneTransaction_Focus__c ) ) &&
                         ( ! n.OneTransaction_Focus__c.equalsIgnoreCase ( o.OneTransaction_Focus__c ) ) )
                {
                    if ( xocM.containsKey ( key.toLowerCase () ) )
                    {
                        OneCampaign_Member__c ocm = new OneCampaign_Member__c () ;
                        ocm.Contact__c = n.ID ;
                        
                        ocm.Campaign__c = xocM.get ( key.toLowerCase () ).ID ;
                        ocmL.add ( ocm ) ;
                    }
                }
                
                //  --------------------------------------------------------------
                //  QUIT?!?
                //  --------------------------------------------------------------
                if ( ( n.Advocate_Level__c == alsQuitID ) && ( n.Advocate_Level__c != o.Advocate_Level__c ) )
                {
                    Automated_Email__c ae = new Automated_Email__c () ;
                    ae.Contact_OR_Account__c = 'Contact' ;
                    ae.Contact__c = n.ID ;
                    ae.Email_Address__c = n.Email ;
                    ae.First_Name__c = n.FirstName ;
                    ae.Last_Name__c = n.LastName ;
                    ae.Is_A_Survey__c = 'N' ;
                    ae.RecordTypeId = ContactHelper.getAutomatedEmailRTID () ;
                    ae.Template_Name__c = 'Advocate Non Participant Opt Out Confirmation' ;
                    
                    aeL.add ( ae ) ;
                }
            }
                
            // haz active account & referral code count = 0
            // OR
            // advocate date not null & referral code count = 0
            // OR
            // advocate level not null & referral code count = 0
            if ( rcL.size () < 2000 )
            {
                if ( ( n.Has_Active_Account__c == true ) && ( n.Advocate_Referral_Code_Count__c == 0 ) )
                    rcL.add ( n ) ;
                else if ( ( n.Advocate_Timestamp__c != null ) && ( n.Advocate_Referral_Code_Count__c == 0 ) )
                    rcL.add ( n ) ;
                else if ( ( n.Advocate_Level__c != null ) && ( n.Advocate_Referral_Code_Count__c == 0 ) )
                    rcL.add ( n ) ;
            }
        
            // ---------------------------------------------------------------------------------
            // Transactions
            // ---------------------------------------------------------------------------------
            boolean hazPoints = false ;
            
            decimal current = 0.0 ;
            if ( n.Advocate_POS_Trans_Current_Mult__c != null )
                current = n.Advocate_POS_Trans_Current_Mult__c ;
            
            decimal prior = 0.0 ;
            if ( n.Advocate_POS_Trans_Last_Mult__c != null )
                prior = n.Advocate_POS_Trans_Last_Mult__c ;
            
            //If the month rolls over, then we need to start awarding points again.
            if (current < prior) {
                prior=0;
            }
            
            decimal total = current - prior ;
            
            while ( total >= 1.0 )
            {
                hazPoints = true ;
                
                //Needs to be a while and not an if, so that if some how there are 200 points to be given in an one 
                //award, then we award in groups of 100.
                while ( total >= 10.0 )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'Transactions_10' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    total = total - 10.0 ;
                }
                
                //Needs to be equal to so that we award for 10 transactions.
                if ( total >= 1.0 )
                {
                    Integer thingy = total.intValue () ;
                    
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'Transactions_' + thingy ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    total = total - thingy ;
                }
            }
            
            if ( hazPoints == true )
            {
                if ( cMap.containsKey ( n.ID ) )
                {
                    Contact c = cMap.get ( n.ID ) ;
                    c.Advocate_POS_Trans_Last_Mult__c = n.Advocate_POS_Trans_Current_Mult__c ;
                    cMap.put ( n.ID, c ) ;
                }
                else
                {
                    Contact c = new Contact () ;
                    c.ID = n.ID ;
                    c.Advocate_POS_Trans_Last_Mult__c = n.Advocate_POS_Trans_Current_Mult__c ;
                    cMap.put ( c.ID, c ) ;
                }
            }
            
            // ---------------------------------------------------------------------------------
            // Accounts
            // ---------------------------------------------------------------------------------
            double d = 0.0 ;
            if ( n.Personal_Deposit_Accounts__c != null )
                d += n.Personal_Deposit_Accounts__c ;
            if ( n.Personal_CC_Accounts__c != null )
                d += n.Personal_CC_Accounts__c ;
            
            double l = 0.0 ;
            if ( n.Advocate_Deposit_Acct_Last_Awarded__c != null )
                l = n.Advocate_Deposit_Acct_Last_Awarded__c ;
            
            if ( ( d > l ) && ( l < 4.0 ) )
            {
                while (l < d) {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    if (l!=0) {
                        reallyLongName.requestType = 'Activity' ;
                    } else {
                        reallyLongName.requestType = 'Badge' ;
                    }
                    
                	l++;
                    
                    reallyLongName.requestName = 'New_Deposit_Account_' + ( l ).intValue () ;
                    
                    system.debug('reallyLongName.requestType:'+reallyLongName.requestType);
                    system.debug('reallyLongName.requestName:'+reallyLongName.requestName);
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                }
                
                if ( cMap.containsKey ( n.ID ) )
                {
                    Contact c = cMap.get ( n.ID ) ;
                    c.Advocate_Deposit_Acct_Last_Awarded__c = l ;
                    cMap.put ( n.ID, c ) ;
                }
                else
                {
                    Contact c = new Contact () ;
                    c.ID = n.ID ;
                    c.Advocate_Deposit_Acct_Last_Awarded__c = l ;
                    cMap.put ( c.ID, c ) ;
                }
            }
            
            // ---------------------------------------------------------------------------------
            // Anniversary
            // ---------------------------------------------------------------------------------
            if ( n.Years_as_a_Customer__c > n.Advocate_Anniversary_Awarded__c )
            {
                boolean hazBadge = false ;
                
                if ( n.Years_as_a_Customer__c >= 1 )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Badge' ;
                    reallyLongName.requestName = 'BankBlack_1' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    hazBadge = true ;
                }
                
                if ( n.Years_as_a_Customer__c >= 3 )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Badge' ;
                    reallyLongName.requestName = 'BankBlack_3' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    hazBadge = true ;
                }
                
                if ( n.Years_as_a_Customer__c >= 5 )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Badge' ;
                    reallyLongName.requestName = 'BankBlack_5' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    hazBadge = true ;
                }
                
				if ( hazBadge == true )
                {                    
                    if ( cMap.containsKey ( n.ID ) )
                    {
                        Contact c = cMap.get ( n.ID ) ;
                        c.Advocate_Anniversary_Awarded__c = n.Years_as_a_Customer__c ;
                        cMap.put ( n.ID, c ) ;
                    }
                    else
                    {
                        Contact c = new Contact () ;
                        c.ID = n.ID ;
                        c.Advocate_Anniversary_Awarded__c = n.Years_as_a_Customer__c ;
                        cMap.put ( c.ID, c ) ;
                    }
                }
            }
            
            // ---------------------------------------------------------------------------------
            // Advocate Sign up
            // ---------------------------------------------------------------------------------
            if ( ( oldAdvocateLevel == null ) && ( n.Advocate_Level__c != null ) )
            {
                AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                reallyLongName.ContactID = n.ID ;
                reallyLongName.requestType = 'Badge' ;
                reallyLongName.requestName = 'BankBlack_Challenge' ;
                
                reallyLongNameList.add ( reallyLongName ) ;
                
                if ( cMap.containsKey ( n.ID ) )
                {
                    Contact c = cMap.get ( n.ID ) ;
                    if ( n.Advocate_Timestamp__c == null )
                        c.Advocate_Timestamp__c = System.now ()  ;
                    c.Advocate__c = true ;
                    cMap.put ( n.ID, c ) ;
                }
                else
                {
                    Contact c = new Contact () ;
                    c.ID = n.ID ;
                    if ( n.Advocate_Timestamp__c == null )
                        c.Advocate_Timestamp__c = System.now ()  ;
                    c.Advocate__c = true ;
                    cMap.put ( c.ID, c ) ;
                }
            }
            
            // ---------------------------------------------------------------------------------
            // Social
            // ---------------------------------------------------------------------------------
            if ( ! oldHazSocial )
            {
                if ( ( String.isNotBlank ( n.Facebook_Handle__c ) ) || ( String.isNotBlank ( n.Twitter_Handle__c ) ) || ( String.isNotBlank ( n.Instagram_Handle__c ) ) )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Badge' ;
                    reallyLongName.requestName = 'Link_Up' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Direct Deposit
            //  -----------------------------------------------------------------------------------
            if ( ! oldDirectDeposit )
            {
                if ( ( n.Has_Active_Account__c == true ) && ( n.Direct_Deposit__c == true ) && ( n.Direct_Dep__c == false ) )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Badge' ;
                    reallyLongName.requestName = 'Direct_Satisfaction' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    if ( cMap.containsKey ( n.ID ) )
                    {
                        Contact c = cMap.get ( n.ID ) ;
                        c.Direct_Dep__c = true ;
                        cMap.put ( n.ID, c ) ;
                    }
                    else
                    {
                        Contact c = new Contact () ;
                        c.ID = n.ID ;
                        c.Direct_Dep__c = true ;
                        cMap.put ( c.ID, c ) ;
                    }
                }
            }

            //  -----------------------------------------------------------------------------------
            //  Pop Money
            //  -----------------------------------------------------------------------------------
            if ( oldPopMoney == false )
            {
                if ( ( n.Has_Active_Account__c == true ) && ( n.PopMoney__c == false ) && ( ( n.MTD_Pers_Pop_Trans__c > 0.0 ) || ( n.PM_Pers_Pop_Trans__c > 0.0 ) ) )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'PopMoney' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    if ( cMap.containsKey ( n.ID ) )
                    {
                        Contact c = cMap.get ( n.ID ) ;
                        c.PopMoney__c = true ;
                        cMap.put ( n.ID, c ) ;
                    }
                    else
                    {
                        Contact c = new Contact () ;
                        c.ID = n.ID ;
                        c.PopMoney__c = true ;
                        cMap.put ( c.ID, c ) ;
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Mobile Remote Deposit
            //  -----------------------------------------------------------------------------------
            if ( oldMobileRemoteDeposit == false )
            {
                if ( ( n.Has_Active_Account__c == true ) && ( n.Mobile_Remote_Deposit__c == false ) && ( ( n.MTD_Pers_Remote_Dep_Count__c > 0.0 ) || ( n.PM_Pers_Remote_Dep_Count__c > 0.0 ) ) )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'Mobile_Remote_Deposit_Capture' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    if ( cMap.containsKey ( n.ID ) )
                    {
                        Contact c = cMap.get ( n.ID ) ;
                        c.Mobile_Remote_Deposit__c = true ;
                        cMap.put ( n.ID, c ) ;
                    }
                    else
                    {
                        Contact c = new Contact () ;
                        c.ID = n.ID ;
                        c.Mobile_Remote_Deposit__c = true ;
                        cMap.put ( c.ID, c ) ;
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  Funds Transfer
            //  -----------------------------------------------------------------------------------
            if ( oldFundsTransfer == false )
            {
                if ( ( n.Has_Active_Account__c == true ) && ( n.Funds_Transfer__c == false ) && ( ( n.MTD_Pers_Funds_Trans__c > 0.0 ) || ( n.PM_Pers_Funds_Trans__c > 0.0 ) ) )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallyLongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    reallyLongName.ContactID = n.ID ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'Transfer_Funds' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                    
                    if ( cMap.containsKey ( n.ID ) )
                    {
                        Contact c = cMap.get ( n.ID ) ;
                        c.Funds_Transfer__c = true ;
                        cMap.put ( n.ID, c ) ;
                    }
                    else
                    {
                        Contact c = new Contact () ;
                        c.ID = n.ID ;
                        c.Funds_Transfer__c = true ;
                        cMap.put ( c.ID, c ) ;
                    }
                }
            }
            
            //  -----------------------------------------------------------------------------------
            //  END LOOP
            //  -----------------------------------------------------------------------------------
        }
        
        /* -----------------------------------------------------------------------------------
         * Referral codes
         * ----------------------------------------------------------------------------------- */
        if ( ( rcL != null ) && ( rcL.size () > 0 ) )
            ReferralCodeGenerator.generateReferralCodes ( rcL ) ;
        
        //  -----------------------------------------------------------------------------------
        //  Contacts FIRST LEST YE INFINITE LOOP!
        //  -----------------------------------------------------------------------------------
        try
        {
            update cMap.values () ;
        }
        catch ( DMLException e )
        {
            // ?
        }
        
        //  -----------------------------------------------------------------------------------
        //  Advocate
        //  -----------------------------------------------------------------------------------
        if ( ( reallyLongNameList != null ) && ( reallyLongNameList.size () > 0 ) )
            AdvocateProgramAwardInvocationFunction.advocateProgramAward ( reallyLongNameList ) ;
    }
    
    //  -----------------------------------------------------------------------------------
    //  Automated Email
    //  -----------------------------------------------------------------------------------
    if ( ( aeL != null ) && ( aeL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            ContactQueue cq = new ContactQueue ( aeL ) ;
            System.enqueueJob ( cq ) ;
        }
        else
        {
            try
            {
                insert aeL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
    
    //  -----------------------------------------------------------------------------------
    //  OneCampaigns
    //  -----------------------------------------------------------------------------------
    if ( ( ocmL != null ) && ( ocmL.size () > 0 ) )
    {
        if ( ! System.isQueueable () )
        {
            ContactQueue cq = new ContactQueue ( ocmL ) ;
            System.enqueueJob ( cq ) ;
        }
        else
        {
            try
            {
                insert ocmL ;
            }
            catch ( DMLException e )
            {
                // ?
            }
        }
    }
}