trigger OneApplicationAfterUpdate on one_application__c (before update, after update) 
{
    //  -----------------------------------------------------------------------------------
    //  Helper setup
    //  -----------------------------------------------------------------------------------
    if ( OneApplicationHelper.dMapUV == null )
    {
        OneApplicationHelper.dMapUV = new map<String, boolean> () ;
        OneApplicationHelper.dRanUV = false ;
    }
    
    if ( OneApplicationHelper.dMapD == null )
    {
        OneApplicationHelper.dMapD = new map<String, boolean> () ;
        OneApplicationHelper.dRanD = false ;
    }
    
    /* -----------------------------------------------------------------------------------
     * COUNTER
     * ----------------------------------------------------------------------------------- */
    private final Integer ACCOUNTS_PER_CALLOUT = 2 ;
    private final Integer UNITYVISA_PER_CALLOUT = 8 ;

    /* -----------------------------------------------------------------------------------
     * BEFORE TRIGGER
     * ----------------------------------------------------------------------------------- */
    if ( Trigger.isBefore )
    {
        /* -----------------------------------------------------------------------------------
         * creating the decision code map
         * ----------------------------------------------------------------------------------- */
        List<FIS_Decision_Code_Mapping__mdt> decisionCodeMapping = [SELECT Decision_Message__c,DeveloperName FROM FIS_Decision_Code_Mapping__mdt ];
        Map<String,String> decisionCodeMap = new Map<String,String>();
        for(FIS_Decision_Code_Mapping__mdt dcm : decisionCodeMapping){
            decisionCodeMap.put(dcm.DeveloperName, dcm.Decision_Message__c);
        }
        
        //  -----------------------------------------------------------------------------------
        //  Post Bureau
        //  -----------------------------------------------------------------------------------
        map<ID,boolean> pbM = new map<ID,boolean> () ;
        list<one_application__c> pbL = new list<one_application__c> () ;

        //  -----------------------------------------------------------------------------------
        //  DUPLICATE SETUP
        //  -----------------------------------------------------------------------------------
        list<one_application__c> dAPP = new list<one_application__c> () ;
        for ( one_application__c n : Trigger.new )
        {
            if ( String.isNotBlank ( n.Visitor_IP__c ) || 
                 ( String.isNotBlank ( n.Funding_Routing_Number__c ) && 
                   String.isNotBlank ( n.Funding_Account_Number__c ) && 
                   n.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) )
            {
                System.debug ( 'Adding ::' + n.ID + ' to duplicate check.' ) ;
                dAPP.add ( n ) ;
            }
        }
        
        if ( ( OneApplicationHelper.dMapUV.keyset ().size () == 0 ) && ( ! OneApplicationHelper.dRanUV ) )
        {
            OneApplicationHelper.dMapUV = one_utils.duplicateApplicationWarningChecker ( dAPP, 'UNITY_Visa' ) ;
            OneApplicationHelper.dRanUV = true ;
        }
        else
            System.debug ( 'UV dupe map already loaded' ) ;
        
        system.debug ( 'DMAP UV SIZE :: ' + OneApplicationHelper.dMapUV.keyset().size() ) ;
        
        if ( ( OneApplicationHelper.dMapD.keyset ().size () == 0 ) && ( ! OneApplicationHelper.dRanD ) )
        {
            OneApplicationHelper.dMapD = one_utils.duplicateApplicationWarningChecker ( dAPP, 'Deposits' ) ;
            OneApplicationHelper.dRanD = true ;
        }
        else
            System.debug ( 'D dupe map already loaded' ) ;
        
        system.debug ( 'DMAP D SIZE :: ' + OneApplicationHelper.dMapD.keyset().size() ) ;
        
        /* -----------------------------------------------------------------------------------
         * LOOP
         * ----------------------------------------------------------------------------------- */
        for ( one_application__c n : Trigger.new )
        {
            //  Sigh
            if ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) )
                n.Advocate_Referral_Code_Text__c = n.Advocate_Referral_Code_Text__c.toUpperCase () ;
                
            //  Set description based on code
            n.FIS_Decision__c = decisionCodeMap.get(n.FIS_Decision_Code__c);
            
            //  --------------------------------------------------------------
            //  Duplicate checks, UNITY Visa
            //  --------------------------------------------------------------
            if ( OneApplicationHelper.dMapUV.containsKey ( n.ID + '_IP' ) )
                n.Duplicate_IP_Address__c = OneApplicationHelper.dMapUV.get ( n.ID + '_IP' ) ;
                
            if ( OneApplicationHelper.dMapUV.containsKey ( n.ID + '_ACH' ) )
                n.Duplicate_Funding_Account__c = OneApplicationHelper.dMapUV.get ( n.ID + '_ACH' ) ;
            
            //  --------------------------------------------------------------
            //  Duplicate checks, Deposits
            //  --------------------------------------------------------------
            if ( OneApplicationHelper.dMapD.containsKey ( n.ID + '_IP' ) )
                n.Duplicate_IP_Address__c = OneApplicationHelper.dMapD.get ( n.ID + '_IP' ) ;
                
            if ( OneApplicationHelper.dMapD.containsKey ( n.ID + '_ACH' ) )
                n.Duplicate_Funding_Account__c = OneApplicationHelper.dMapD.get ( n.ID + '_ACH' ) ;
            
            //  --------------------------------------------------------------
            //  Get old data for comparison
            //  --------------------------------------------------------------
            one_application__c o = Trigger.oldMap.get ( n.ID ) ;
        
            //  -------------------------------------------------------------------------------------
            //  This section sets the Approval date for easier reporting
            //  -------------------------------------------------------------------------------------
            if ( String.isNotEmpty ( o.Application_Status__c ) && ( String.isNotEmpty ( n.Application_Status__c ) ) )
            {
                if ( ! o.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) &&
                       n.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) )
                {
                    n.Application_Approved_Date__c = System.now () ;
                }
            }
            
            //  -------------------------------------------------------------------------------------
            //  This section sets Upload Key for attachments if it doesn't already exist
            //  -------------------------------------------------------------------------------------
            if ( String.isBlank ( o.Upload_Attachment_key__c ) )
            {
                n.Upload_Attachment_key__c = one_utils.getRandomString ( 30 ) ;
            }
            
            //  -------------------------------------------------------------------------------------
            //  Sets up trial deposits if they don't exist
            //  -------------------------------------------------------------------------------------
            if ( o.Trial_Deposit_1__c == null )
            {
                n.Trial_Deposit_1__c = Math.round ( Math.random () * Integer.valueOf ( 10 ) ) + 1 ;
            }
            
            if ( o.Trial_Deposit_2__c == null )
            {
                n.Trial_Deposit_2__c = Math.round ( Math.random () * Integer.valueOf ( 10 ) ) + 1 ;
            }
            
            //  -------------------------------------------------------------------------------------
            //  Additional Information needed, set time & list
            //  -------------------------------------------------------------------------------------
            if ( ( o.Application_Processing_Status__c == null ) ||
                 ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
            {
                if ( ( n.Application_Processing_Status__c != null ) &&
                     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
                {
                    n.Additional_Doc_Needed_Datetime__c = System.now () ;

                    if ( String.isNotBlank ( n.FIS_Decision_Code__c ) )
                    {
                        String listDoc = one_utils.getMultiPicklistDocs ( n.FIS_Decision_Code__c ) ;
                        
                        if ( String.isNotBlank ( listDoc ) )
                        {
                            if ( String.isBlank ( n.Additional_Information_Required__c ) )
                            {
                                n.Additional_Information_Required__c = listDoc ;
                            }
                            else if ( String.isNotBlank ( o.FIS_Decision_Code__c ) )
                            {
                                if ( ! n.FIS_Decision_Code__c.equalsIgnoreCase ( o.FIS_Decision_Code__c ) )
                                {
                                    n.Additional_Information_Required__c = listDoc ;
                                }
                            }
                        }
                    }
                }
            }

            //  --------------------------------------------------------------
            //  Update date(s) for tracking porpoises
            //  --------------------------------------------------------------
            if ( String.isNotEmpty ( o.FIS_Decision_Code__c ) && ( String.isNotEmpty ( n.FIS_Decision_Code__c ) ) )
            {
                if ( ! n.FIS_Decision_Code__c.equalsIgnoreCase ( o.FIS_Decision_Code__c ) )
                {
                    n.Last_Application_Decision_Change__c = System.now () ;
                }
            }

            if ( String.isNotEmpty ( o.Application_Status__c ) && ( String.isNotEmpty ( n.Application_Status__c ) ) )
            {
                if ( ! n.Application_Status__c.equalsIgnoreCase ( o.Application_Status__c ) )
                {
                    n.Last_Application_Status_Change__c = System.now () ;
                }
            }
            
            if ( ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Received' ) ) &&
                ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Received' ) )
               )
            {
                n.Additional_Doc_Received_Datetime__c = System.now () ;
            }
            
            if ( ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Verified' ) ) &&
                ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Verified' ) )
               )
            {
                n.Additional_Doc_Verified_Datetime__c = System.now () ;
                n.Additional_Doc_Verified_User__c = UserInfo.getUserId () ;
            }
            
            if ( ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Ready for Review' ) ) &&
                ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Ready for Review' ) )
               )
            {
                n.Additional_Doc_ReadyReview_Datetime__c = System.now () ;
                n.Additional_Doc_ReadyReview_User__c = UserInfo.getUserId () ;
            }
            
            if ( ( String.isNotBlank ( n.Application_Processing_Status__c ) ) &&
                 ( String.isNotBlank ( o.Application_Processing_Status__c ) ) &&
                 ( ! n.Application_Processing_Status__c.equalsIgnoreCase ( o.Application_Processing_Status__c ) ) )
            {
                n.Application_Status_Last_Updated__c = System.now () ;
                n.Application_Status_Updated_By__c = UserInfo.getUserId () ;
            }
            
            //  --------------------------------------------------------------
            //  ACH Status checks
            //  --------------------------------------------------------------
            if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( String.isNotBlank ( n.Funding_Options__c ) ) )
            {
                 if ( n.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) )
                 {
                     if ( String.isBlank ( n.ACH_Funding_Status__c ) )
                     {
                        if ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) ||
                             ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) || 
                             ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) 
                        {
                            n.ACH_Funding_Status__c = 'Cleared to Originate' ;
                        }
                    }
                    else if ( String.isNotBlank ( n.ACH_Funding_Status__c ) )
                    {
                        if ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) ||
                             ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) || 
                             ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) 
                        {
                            if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
                            {
                                if ( ( n.Photo_ID_Override_Datetime__c != null ) &&
                                     ( n.Photo_ID_Override_User__c != null ) )
                                {
                                    n.ACH_Funding_Status__c = 'Cleared to Originate' ;
                                }
                            }
                        }
                    }
                 }
            }

            //  --------------------------------------------------------------
            //  Counteroffer ACH Status checks
            //  --------------------------------------------------------------
            if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && 
                 ( String.isNotBlank ( n.Funding_Options__c ) ) )
            {
                 if ( n.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) )
                 {
                     if ( String.isBlank ( n.ACH_Funding_Status__c ) )
                     {
                         if ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) || ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) )
                         {
                             n.ACH_Funding_Status__c = 'Cleared to Originate' ;
                         }
                     }
                 }
            }
            
            //  --------------------------------------------------------------
            //  UTM Formatting fixes
            //  --------------------------------------------------------------
            if ( String.isNotBlank ( n.UTM_Campaign__c ) )
            {
                if ( n.UTM_Campaign__c.contains ( '%20' ) )
                {
                    n.UTM_Campaign__c = n.UTM_Campaign__c.replace ( '%20', ' ' ) ; 
                }
            }
            
            if ( String.isNotBlank ( n.UTM_Content__c ) )
            {
                if ( n.UTM_Content__c.contains ( '%20' ) )
                {
                    n.UTM_Content__c = n.UTM_Content__c.replace ( '%20', ' ' ) ;
                } 
            }
            
            if ( String.isNotBlank ( n.UTM_Medium__c ) )
            {
                if ( n.UTM_Medium__c.contains ( '%20' ) )
                {
                    n.UTM_Medium__c = n.UTM_Medium__c.replace ( '%20', ' ' ) ;
                } 
            }
            
            if ( String.isNotBlank ( n.UTM_Source__c ) )
            {
                if ( n.UTM_Source__c.contains ( '%20' ) )
                {               
                    n.UTM_Source__c = n.UTM_Source__c.replace ( '%20', ' ' ) ;
                } 
            }
            
            if ( String.isNotBlank ( n.UTM_Term__c ) )
            {
                if ( n.UTM_Term__c.contains ( '%20' ) )
                {
                    n.UTM_Term__c = n.UTM_Term__c.replace ( '%20', ' ' ) ;
                } 
            }
            
            if ( String.isNotBlank ( n.UTM_VisitorID__c ) )
            {
                if ( n.UTM_VisitorID__c.contains ( '%20' ) )
                {
                    n.UTM_VisitorID__c = n.UTM_VisitorID__c.replace ( '%20', ' ' ) ;
                } 
            }
            
            //  --------------------------------------------------------------
            //  Post Bureau
            //  --------------------------------------------------------------
            if ( ( ( String.isBlank ( o.FIS_Decision_Code__c )  ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P960' ) ) ) )
            {
                if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) )  && ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P960' ) ) )
                {
                    if ( ! pbM.containsKey ( n.ID ) )
                    {
                        pbM.put ( n.ID, true ) ;
                        pbL.add ( n ) ;
                    }
                }
            }

            //  --------------------------------------------------------------
            //  Woids
            //  --------------------------------------------------------------
            if ( ( ( String.isBlank ( o.Void_Application_Reason__c ) ) && ( String.isNotBlank ( n.Void_Application_Reason__c ) ) ) ||
                   ( ( String.isNotBlank ( o.Void_Application_Reason__c ) ) && ( String.isNotBlank ( n.Void_Application_Reason__c ) && ( ! n.Void_Application_Reason__c.equalsIgnoreCase ( o.Void_Application_Reason__c ) ) ) ) )
            {
                n.FIS_Decision_Code__c = n.Void_Application_Reason__c.left ( 4 ) ;
                n.Void_Application_Datetime__c = System.now () ;
                n.Void_Application_User__c = System.UserInfo.getUserId () ;
            }
            
            else if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c.startsWithIgnoreCase ( 'V' ) ) )
            {
                n.Void_Application_Datetime__c = System.now () ;
                n.Void_Application_User__c = System.UserInfo.getUserId () ;
            }
            
            if ( ( String.isBlank ( n.Void_Application_Reason__c ) ) && ( String.isNotBlank ( o.Void_Application_Reason__c ) ) && ( o.Application_Status__c.equalsIgnoreCase ( 'VOID' ) ) )
            {
                n.FIS_Decision_Code__c = n.FIS_Prior_Decision_Code__c ;
                n.Void_Application_Datetime__c = null ;
                n.Void_Application_User__c = null ;
            }
            

            //  --------------------------------------------------------------
            //  Keep track of prior decision code
            //  --------------------------------------------------------------
            if ( String.isNotEmpty ( o.FIS_Decision_Code__c ) && ( String.isNotEmpty ( n.FIS_Decision_Code__c ) ) )
            {
                if ( ! n.FIS_Decision_Code__c.equalsIgnoreCase ( o.FIS_Decision_Code__c ) )
                {
                    n.FIS_Prior_Decision_Code__c = o.FIS_Decision_Code__c ;
                }
            }
            
            // ------------------------- END LOOP ------------------------- //
        }
        
        //  -----------------------------------------------------------------------------------
        //  Post Bureau update
        //  -----------------------------------------------------------------------------------
        map<String,boolean> dupeMap = LOSHelper.duplicateApplicationChecker ( pbL ) ;
        
        if ( ( pbM != null ) && ( pbM.keyset ().size () > 0 ) )
        {
            for ( one_application__c n : Trigger.new )
            {
                if ( pbM.containsKey ( n.ID ) )
                {
                    LOSHelper losh = new LOSHelper () ;
                    losh.setDuplicateApplicationMap ( dupeMap ) ;
                    
                    losh.setApplication ( n ) ;
                    losh.runEverything () ;
                }
            }
        }
        
        /* -----------------------------------------------------------------------------------
         * Advocate Program ?!?
         * ----------------------------------------------------------------------------------- */
        set<String> apS = new set<String> () ;
        
        for ( one_application__c n : Trigger.new )
        {
            //  --------------------------------------------------------------
            //  Get old data for comparison
            //  --------------------------------------------------------------
            one_application__c o = Trigger.oldMap.get ( n.ID ) ;
            
            if ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) )
            {
                if ( n.Advocate_Referral_Code__c == NULL )
                {
                    apS.add ( n.Advocate_Referral_Code_Text__c.toUpperCase () ) ;
                }
                
                if ( ( String.isNotBlank ( o.Advocate_Referral_Code_Text__c ) ) && ( ! n.Advocate_Referral_Code_Text__c.equalsIgnoreCase ( o.Advocate_Referral_Code_Text__c ) ) )
                {
                    apS.add ( n.Advocate_Referral_Code_Text__c.toUpperCase () ) ;
                }
            }
        }
        
        list<Referral_Code__c> rcL = null ;
        
        try
        {
            rcL = [
                SELECT ID, Name
                FROM Referral_Code__c
                WHERE Name IN :apS
            ] ;
        }
        catch ( DMLException e )
        {
            rcL = null ;
        }
        
        if ( ( rcL != null ) && ( rcL.size () > 0 ) )
        {
            map<String,ID> rcM = new map<String,ID> () ;
            
            for ( Referral_Code__c rc : rcL )
                rcM.put ( rc.Name, rc.ID ) ;
                
            for ( one_application__c n : Trigger.new )
            {
                //  Empty out referral code if current one is blank
                if ( String.isBlank ( n.Advocate_Referral_Code_Text__c ) )
                    n.Advocate_Referral_Code__c = null ;
            	else
                {
                    //  Place in thingy
                    if ( rcM.containsKey ( n.Advocate_Referral_Code_Text__c.toUpperCase () ) )
                        n.Advocate_Referral_Code__c = rcM.get ( n.Advocate_Referral_Code_Text__c ) ;
                }
            }
        }
    }
        
    /* -----------------------------------------------------------------------------------
     * AFTER TRIGGER
     * ----------------------------------------------------------------------------------- */
    if ( Trigger.isAfter )
    {
        //  -----------------------------------------------------------------------------------
        //  Email Helper setup
        //  -----------------------------------------------------------------------------------
        if ( EmailFunctionsHelper.approvalMap == null )
            EmailFunctionsHelper.approvalMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.fundingMap == null )
            EmailFunctionsHelper.fundingMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.additionalMap == null )
            EmailFunctionsHelper.additionalMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.trialMap == null )
            EmailFunctionsHelper.trialMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.warningMap == null )
            EmailFunctionsHelper.warningMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.denyMap == null )
            EmailFunctionsHelper.denyMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.withdrawalMap == null )
            EmailFunctionsHelper.withdrawalMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.declineIDVMap == null )
            EmailFunctionsHelper.declineIDVMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.declineOFACMap == null )
            EmailFunctionsHelper.declineOFACMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.declineFCRAMap == null )
            EmailFunctionsHelper.declineFCRAMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.achRejectionMap == null )
            EmailFunctionsHelper.achRejectionMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.counterOfferMap == null )
            EmailFunctionsHelper.counterOfferMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.notCitizenMap == null )
            EmailFunctionsHelper.notCitizenMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.outOfAreaMap == null )
            EmailFunctionsHelper.outOfAreaMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.underageMap == null )
            EmailFunctionsHelper.underageMap = new map<String, boolean> () ;
        
        if ( EmailFunctionsHelper.youngMap == null )
            EmailFunctionsHelper.youngMap = new map<String, boolean> () ;
        
        //  -----------------------------------------------------------------------------------
        //  Application Transaction Helper setup
        //  -----------------------------------------------------------------------------------
        if ( ApplicationTransactionHelper.refundMap == null )
            ApplicationTransactionHelper.refundMap = new map<String, boolean> () ;
        
        if ( ApplicationTransactionHelper.settleMap == null )
            ApplicationTransactionHelper.settleMap = new map<String, boolean> () ;

        if ( ApplicationTransactionHelper.voidMap == null )
            ApplicationTransactionHelper.voidMap = new map<String, boolean> () ;

        //  -----------------------------------------------------------------------------------
        //  Letter Helper Setup
        //  -----------------------------------------------------------------------------------
        if ( ApplicationLetterHelper.letterMap == null )
            ApplicationLetterHelper.letterMap = new map<String, boolean> () ;
        
        list<one_application__c> appLetterList = new list<one_application__c> () ;
        map<ID,string> appLetterMap = new map<ID,String> () ;

        //  -----------------------------------------------------------------------------------
        //  Setting Up
        //  -----------------------------------------------------------------------------------
        one_settings__c oS = one_settings__c.getInstance () ;
        
        List<String> cID = new List<String> () ;
        
        DateTime lastEmail = System.now () ;
        
        if ( ( os != null ) && ( oS.Last_Batch_Email_Sent__c != null ) )
            lastEmail = oS.Last_Batch_Email_Sent__c ;
        
        List<String> lFundingIDs = new List<String> () ;
        List<String> lApprovedIDs = new List<String> () ;
        List<String> lAdditionalInfoIDs = new List<String> () ;
        List<String> lTrialIDs = new List<String> () ;
        List<String> lWarningIDs = new List<String> () ;
        List<String> lDeniedIDs = new List<String> () ;
        List<String> lGeoLocationIDs = new List<String> () ;
        List<String> lWithdrawalIDs = new List<String> () ;
        List<String> lDeclineIDVIDs = new List<String> () ;
        List<String> lDeclineOFACIDs = new List<String> () ;     
        List<String> lDeclineFCRAIDs = new List<String> () ;
        List<String> lACHRejectIDs = new List<String> () ;
        List<String> lCounterOfferIDs = new List<String> () ;
        List<String> lNotCitizenIDs = new List<String> () ;
        List<String> lOutOfAreaIDs = new List<String> () ;
        List<String> lUnderageIDs = new List<String> () ;
        List<String> lYoungIDs = new List<String> () ;
        
        //  ---------------------------------------
        //  Advocate awards
        //  ---------------------------------------
        List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> reallyLongNameList = new List<AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest> () ;
        
        //  ---------------------------------------
        //  For COCC calls 
        //  ---------------------------------------
        List<ID> applicationsWaitingForCOCC = new List<Id>();
        
        //  ---------------------------------------
        //  For LOS calls 
        //  ---------------------------------------
        List<ID> applicationsWaitingForLOS = new List<Id>();
        
        //  -----------------------------------------------------------------------------------
        //  Application Transactions
        //  -----------------------------------------------------------------------------------
        Set<Id> applicationsToVoidTransactions = new Set<Id>();
        Set<Id> applicationsToCaptureTransactions = new Set<Id>();
        Map<String,Decimal> amountsMap = new Map<String,Decimal>();        
        
        //  -----------------------------------------------------------------------------------
        //  MAIN LOOP
        //  -----------------------------------------------------------------------------------
        for ( one_application__c n : Trigger.new )
        {
            one_application__c o = Trigger.oldMap.get ( n.ID ) ;

            //  --------------------------------------------------------------
            //  Trial Deposit emails, status change
            //  --------------------------------------------------------------
            if ( ( o.FIS_Decision_Code__c == null ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P501' ) ) )
            {
                if ( ( n.FIS_Decision_Code__c != null ) && ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P501' ) ) )
                {
                    if ( String.isNotBlank ( n.ACH_Funding_Status__c ) && 
                        ( ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) ) ||
                          ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Send Trial Deposit' ) ) ) )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lTrialIds.add ( n.ID ) ;
                    }
                }
            }

            //  --------------------------------------------------------------
            //  Trial Deposit emails after a certain time frame
            //  --------------------------------------------------------------
            boolean hazTrialDepositEmail = EmailFunctionsHelper.sendTimeBasedTrialEmail ( n, lastEmail ) ;
            
            if ( hazTrialDepositEmail )
            {
                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                    lTrialIDs.add ( n.Id ) ;
            }
            
            //  --------------------------------------------------------------
            //  Funding Emails after a certain time frame
            //  --------------------------------------------------------------
            boolean hazFundingEmail = EmailFunctionsHelper.sendTimeBasedFundingEmail ( n, lastEmail ) ;
            
            if ( hazFundingEmail )
            {
                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                    lFundingIDs.add ( n.Id ) ;
            }
            
            //  --------------------------------------------------------------
            //  Funding Emails on validation change
            //  --------------------------------------------------------------
            if ( ( n.RecordTypeID == NULL ) || ( n.RecordTypeId != Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
            {
                if ( ( o.FIS_Decision_Code__c == null ) ||
                     ( ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) ||
                       ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) || 
                       ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) )
                 {
                    if ( ( n.FIS_Decision_Code__c != null ) &&
                         ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) ||
                           ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) || 
                           ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) )
                     {
                        if ( ( String.isNotBlank ( o.ACH_Funding_Status__c ) ) && ( String.isNotBlank ( n.ACH_Funding_Status__c ) ) )
                        {
                            if ( ( ! o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) ) &&
                                 ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) ) )
                            {
                                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                    lTrialIDs.add ( n.Id ) ;
                            }
                            
                            if ( ( ! o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Cleared to Originate' ) ) &&
                                 ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Cleared to Originate' ) ) )
                            {
                                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                    lFundingIDs.add ( n.Id ) ;
                            }
                        }
                     }
                }
            }
            
            //  --------------------------------------------------------------
            //  Funding Emails on status change
            //  --------------------------------------------------------------
            if ( ( n.RecordTypeID == NULL ) || ( n.RecordTypeId != Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
            {
                if ( ( o.FIS_Decision_Code__c == null ) ||
                     ( ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) &&
                       ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) && 
                       ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) )
                 {
                    if ( ( n.FIS_Decision_Code__c != null ) &&
                         ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P012' ) ) ||
                           ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P212' ) ) || 
                           ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) ) )
                     {
                        if ( String.isNotBlank ( n.ACH_Funding_Status__c ) )
                        {
                            if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Cleared to Originate' ) )
                            {
                                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                    lFundingIDs.add ( n.Id ) ;
                            }
                            else if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
                            {
                                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                    lTrialIDs.add ( n.Id ) ;
                            }
                        }
                     }
                 }
            }
            
            //  --------------------------------------------------------------
            //  Funding Emails counteroffer, status change
            //  --------------------------------------------------------------
            if ( ( o.FIS_Decision_Code__c == null ) ||
                 ( ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) &&
                   ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) && 
                   ( ( o.Application_Processing_Status__c == null ) ||
                     ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Counter Offer Accepted' ) ) ) 
                 ) 
               )
             {
                if ( ( n.FIS_Decision_Code__c != null ) &&
                     ( ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) ||
                         ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) ) &&
                       ( ( n.Application_Processing_Status__c != null ) &&
                         ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Counter Offer Accepted' ) ) 
                       ) 
                      )
                    )
                 {
                    if ( String.isNotBlank ( n.ACH_Funding_Status__c ) )
                    {
                        if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Cleared to Originate' ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lFundingIDs.add ( n.Id ) ;
                        }
                        else if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lTrialIDs.add ( n.Id ) ;
                        }
                    }
                 }
             }
            
            //  --------------------------------------------------------------
            //  Funding Emails counteroffer, same status
            //  --------------------------------------------------------------
            if ( ( o.FIS_Decision_Code__c == null ) ||
                 ( ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) ||
                   ( o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) && 
                   ( ( o.Application_Processing_Status__c != null ) &&
                     ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Counter Offer Accepted' ) ) ) 
                 ) 
               )
             {
                if ( ( n.FIS_Decision_Code__c != null ) &&
                     ( ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) ||
                         ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) ) &&
                       ( ( n.Application_Processing_Status__c != null ) &&
                         ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Counter Offer Accepted' ) ) 
                       ) 
                      )
                    )
                 {
                    if ( String.isNotBlank ( n.ACH_Funding_Status__c ) )
                    {
                        if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Cleared to Originate' ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.fundingMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lFundingIDs.add ( n.Id ) ;
                        }
                        else if ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.trialMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lTrialIDs.add ( n.Id ) ;
                        }
                    }
                 }
             }
             
            //  --------------------------------------------------------------
            //  Counter offer emails - Requires additional step
            //  --------------------------------------------------------------
            if ( ( ( n.RecordTypeID != NULL ) && ( n.RecordTypeId == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) ) ||
                 ( LOSHelper.isNewLOS ( n ) ) )
            {
                if ( ( o.FIS_Decision_Code__c == null ) ||
                     ( ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) &&
                       ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) ) )
                {
                    if ( ( n.FIS_Decision_Code__c != null ) &&
                         ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P030' ) ) ||
                           ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P230' ) ) ) )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.counterOfferMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lCounterOfferIDs.add ( n.Id ) ;
                    }
                }
            }
            
            //  --------------------------------------------------------------
            //  Additional Info Emails on status change
            //  --------------------------------------------------------------
            if ( ( o.Application_Processing_Status__c == null ) ||
                 ( ! o.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
            {
                if ( ( n.Application_Processing_Status__c != null ) &&
                     ( n.Application_Processing_Status__c.equalsIgnoreCase ( 'Additional Documentation Needed' ) ) )
                {
                    System.debug ( 'Additional Info Email - Status change - ADD #1' ) ;
                    
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.additionalMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lAdditionalInfoIDs.add ( n.Id ) ;
                }
            }
             
/*          if ( ( o.FIS_Decision_Code__c == null ) || 
                 ( ! one_utils.codesAdditionalInfoMap.containsKey ( o.FIS_Decision_Code__c ) ) ||
                 ( ( one_utils.codesAdditionalInfoMap.containsKey ( o.FIS_Decision_Code__c ) ) && ( o.Is_Quiz_Processing_Status__c ) ) )
            {
                if ( ( n.FIS_Decision_Code__c != null ) && ( one_utils.codesAdditionalInfoMap.containsKey ( n.FIS_Decision_Code__c ) ) )
                {
                    if ( ! n.Is_Quiz_Processing_Status__c )
                    {
                        System.debug ( 'Additional Info Email - Status change - ADD #2' ) ;
                        if ( ( ! listHasElement ( lAdditionalInfoIDs, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lAdditionalInfoIDs.add ( n.Id ) ;
                    }
                }
            } */
            else
            //  --------------------------------------------------------------
            //  Additional Info Emails on status change, time Based
            //  --------------------------------------------------------------
            {
                boolean hazAdditionalInfoEmail = EmailFunctionsHelper.sendTimeBasedAdditionalInfoEmail ( o, lastEmail ) ;
                
                if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.additionalMap, o.Id ) ) && ( ! o.Invalid_Email_Address__c ) && hazAdditionalInfoEmail )
                    lAdditionalInfoIDs.add ( o.Id ) ;
            }
             
            //  --------------------------------------------------------------
            //  Approved Emails on status change
            //  --------------------------------------------------------------
            if ( ( n.RecordTypeID != NULL ) && ( n.RecordTypeId == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
            {
                if ( ( String.isBlank ( o.FIS_Decision_Code__c  ) ) || ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) )
                {
                    if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P089' ) ) )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.approvalMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lApprovedIds.add ( n.Id ) ;
                    }
                }
            }
            else
            {
                if ( ! o.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) &&
                       n.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) )
                {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.approvalMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lApprovedIds.add ( n.Id ) ;
                    
                    //  Add to list of contacts to flip record type to new one
                    cID.add ( n.Contact__c ) ;
                    
                    //  Do the same for co-applicant
                    if ( String.isNotBlank ( o.Co_Applicant_Contact__c ) )
                        cID.add ( o.Co_Applicant_Contact__c ) ;
                }
            }
            
            //  --------------------------------------------------------------
            //  Warning Emails on status change
            //  --------------------------------------------------------------
            if ( String.isNotBlank ( n.FIS_Decision_Code__c ) )
            {
                if ( ( ( o.FIS_Decision_Code__c == null ) ||
                     ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P064' ) &&
                       ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'P264' ) ) ) &&
                     ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P064' ) ||
                       n.FIS_Decision_Code__c.equalsIgnoreCase ( 'P264' ) ) )
                {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.warningMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lWarningIDs.add ( n.Id ) ;
                }
            }
            
            //  --------------------------------------------------------------
            //  Withdrawal Emails on status change - Only V002/V004 & not in LOS
            //  --------------------------------------------------------------
            if ( String.isBlank ( n.FIS_Application_ID__c ) )
            {
                if ( ( String.IsBlank ( o.FIS_Decision_Code__c ) ) ||
                     ( ( String.isNotBlank ( o.FIS_Decision_Code__c ) ) && 
                       ( ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'V002' ) ) && ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'V004' ) ) ) 
                     ) 
                   )
                {
                    if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && 
                         ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'V002' ) ) || ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'V004' ) ) ) 
                       )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.withdrawalMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lWithdrawalIDs.add ( n.ID ) ;
                    }
                }
            }

            //  --------------------------------------------------------------
            //  Decline Emails on status change
            //  --------------------------------------------------------------
            if ( String.isBlank ( n.FIS_Application_ID__c ) )
            {
                if ( ( String.IsBlank ( o.FIS_Decision_Code__c ) ) ||
                     ( ( String.isNotBlank ( o.FIS_Decision_Code__c ) ) && 
                       ( ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D004' ) ) && 
                         ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D006' ) ) && 
                         ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D501' ) ) &&  
                         ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D502' ) ) &&  
                         ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D503' ) ) &&  
                         ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D504' ) ) &&  
                         ( ! o.FIS_Decision_Code__c.equalsIgnoreCase ( 'D505' ) ) 
                       ) 
                     ) 
                   )
                {
                    if ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && 
                         ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D004' ) ) || ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D006' ) ) || ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D501' ) ) ) 
                       )
                    {
                        if ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D004' ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineOFACMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lDeclineOFACIDs.add ( n.ID ) ;
                        }
                        else if ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D006' ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineIDVMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lDeclineIDVIDs.add ( n.ID ) ;
                        }
                        else if ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D501' ) )
                        {
                            //  Trial deposit Decline is same as IDV decline - can't validate information.
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineIDVMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lDeclineIDVIDs.add ( n.Id ) ;
                        }
                        else if ( ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D502' ) ) ||
                                  ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D503' ) ) ||
                                  ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D504' ) ) ||
                                  ( n.FIS_Decision_Code__c.equalsIgnoreCase ( 'D505' ) ) )
                        {
                            //  One of our declines 
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineIDVMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lDeclineIDVIDs.add ( n.Id ) ;
                        }
                    }
                }
            }

            //  --------------------------------------------------------------
            //  FIS Denied Emails on status change - any and all 'D's !
            //  --------------------------------------------------------------
            if ( String.isNotBlank ( n.FIS_Decision_Code__c ) )
            {
                if ( String.isNotBlank ( n.FIS_Application_ID__c ) || ( n.RecordTypeId == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
                {
                    if ( ( n.FIS_Decision_Code__c.subString ( 0, 1 ).equalsIgnoreCase ( 'D' ) ) &&
                         ( ( n.Days_in_Status__c >= 2 ) && ( n.Days_in_Status__c <= 5 ) ) &&
                         ( n.Denial_Email_Sent__c == false )
                       )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.denyMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lDeniedIDs.add ( n.Id ) ;
                    }
                }
            }
            
            //  --------------------------------------------------------------
            //  Decline Emails on Qualifile - DEPOSIT ONRY
            //  --------------------------------------------------------------
            if ( ( n.RecordTypeID != NULL ) && ( n.RecordTypeId == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
            {
                if ( ( String.isBlank ( o.QualiFile_Product_Strategy__c ) ) ||
                     ( ( String.isNotBlank ( o.QualiFile_Product_Strategy__c ) ) && 
                       ( ! o.QualiFile_Product_Strategy__c.startswith ( 'D' ) ) ) )
                {
                    if ( ( String.isNotBlank ( n.QualiFile_Product_Strategy__c ) ) &&
                         ( n.QualiFile_Product_Strategy__c.startswith ( 'D' ) ) )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineFCRAMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lDeclineFCRAIDs.add ( n.ID ) ;
                    }
                }
                
                if ( String.isNotBlank ( n.QualiFile_Decision__c ) )
                {
                    if ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || ( ! o.FIS_Decision_Code__c.startsWith ( 'D' ) ) )
                    {
                        if ( ( String.IsNotBlank ( n.FIS_Decision_Code__C ) ) && ( n.FIS_Decision_Code__c.startsWith ( 'D' ) ) )
                        {
                            if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.declineFCRAMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                                lDeclineFCRAIDs.add ( n.ID ) ;
                        }
                    }
                }
            }

            //  --------------------------------------------------------------
            //  Ach Rejection Emails
            //  --------------------------------------------------------------
            if ( String.isNotBlank ( n.Funding_Options__c ) &&
                 String.isNotBlank ( o.ACH_Funding_Status__c ) && 
                 String.isNotBlank ( n.ACH_Funding_Status__c ) )
            {
                if ( ( n.Funding_Options__c.equalsIgnoreCase ( 'E- Check (ACH)' ) ) &&
                     ( ( o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Originated' ) || o.ACH_Funding_Status__c.equalsIgnoreCase ( 'Verify Trial Deposit' ) ) &&
                       ( n.ACH_Funding_Status__c.equalsIgnoreCase ( 'Rejected' ) ) ) )
                 {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.achRejectionMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lACHRejectIDs.add ( n.ID ) ;
                 }
            }
            
            //  --------------------------------------------------------------
            //  UNITY Visa weirdo never likely to hit emails
            //  --------------------------------------------------------------
            if ( ( n.RecordTypeID != NULL ) && ( n.RecordTypeID == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('UNITY Visa').RecordTypeId  ) )
            {
                //  ADV 401 - Not a citizen
                if ( ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || ( o.FIS_Decision_Code__c != 'D003') ) &&
                     ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c == 'D003') ) )
                {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.notCitizenMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lNotCitizenIDs.add ( n.ID ) ;
                }
                
                //  ADV 403 - Out of Area
                if ( ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || ( o.FIS_Decision_Code__c != 'D035') ) &&
                     ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c == 'D035') ) )
                {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.outOfAreaMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lOutOfAreaIDs.add ( n.ID ) ;
                }
                
                //  ADV 403 - Out of Area - OTHER
                if ( LOSHelper.isPreBureau ( n ) )
                {
                    if ( ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || ( o.FIS_Decision_Code__c != 'D008') ) &&
                         ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c == 'D008') ) )
                    {
                        if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.outOfAreaMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                            lOutOfAreaIDs.add ( n.ID ) ;
                    }
                }
                    
                //  ADV 405 - Underage
                if ( ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || ( o.FIS_Decision_Code__c != 'D002') ) &&
                     ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c == 'D002') ) )
                {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.underageMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lUnderageIDs.add ( n.ID ) ;
                }
                
                //  ADV 410 - Young Consumer
                /* ----- CANT HIT THIS ?!? ---
                if ( ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || ( o.FIS_Decision_Code__c != 'D002') ) &&
                     ( ( String.isNotBlank ( n.FIS_Decision_Code__c ) ) && ( n.FIS_Decision_Code__c == 'D002') ) )
                {
                    if ( ( ! EmailFunctionsHelper.checkDuplicate ( EmailFunctionsHelper.youngMap, n.Id ) ) && ( ! n.Invalid_Email_Address__c ) )
                        lYoungIDs.add ( n.ID ) ;
                }
                */
            }

            //  --------------------------------------------------------------
            //  Geolocating
            //  --------------------------------------------------------------
            if ( ( ( n.Location__Latitude__s == null ) || ( n.Location__Longitude__s == null ) ) && ( n.Unknown_Location__c == false ) )
            {
                lGeoLocationIDs.add ( n.Id ) ;
            }
            
            //  --------------------------------------------------------------
            //  Debit Card Application Transactions
            //  --------------------------------------------------------------
            if ( String.isNotBlank ( n.FIS_Decision_Code__c ) )
            {
                //  Debit card checks!
                if ( String.isNotBlank ( n.Funding_Options__c ) )
                {
                    if ( ( n.Funding_Options__c.equalsIgnoreCase ( 'Debit Card' ) ) )
                    {
                        if ( ( String.isBlank ( o.FIS_Decision_Code__c ) ) || 
                             ( ( String.isNotBlank ( o.FIS_Decision_Code__c ) ) && ( ! o.FIS_Decision_Code__c.startsWith ( 'D' ) ) && ( ! o.FIS_Decision_Code__c.startsWith ( 'V' ) ) ) )
                        {
                            if ( n.FIS_Decision_Code__c.startsWith ('D') || n.FIS_Decision_Code__c.startsWith ('V') )
                            {
                                //add application to the list of transactions to void
                                if ( ! ApplicationTransactionHelper.checkDuplicate ( ApplicationTransactionHelper.voidMap, n.Id ) )
                                    applicationsToVoidTransactions.add(n.Id);
                            }
                        }
                        
                        if ( n.FIS_Credit_Limit__c > 0 )
                        {
                            // UNITY Visa settlement
                            if ( ( n.RecordTypeId == null ) || ( n.RecordTypeId != Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
                            {
                                if ( String.isBlank ( o.FIS_Decision_Code__c ) ||
                                     ( (o.FIS_Decision_Code__c != 'P089' && 
                                       o.FIS_Decision_Code__c != 'P012' && 
                                       o.FIS_Decision_Code__c != 'P212' && 
                                       o.FIS_Decision_Code__c != 'P030' ) ||
                                       ( ( o.FIS_Decision_Code__c =='P089' || o.FIS_Decision_Code__c == 'P030' ) && (o.FIS_Credit_Limit__c==null || o.FIS_Credit_Limit__c==0) ) ) )
                                {
                                    if ( n.FIS_Decision_Code__c == 'P089' || 
                                         n.FIS_Decision_Code__c == 'P012' || 
                                         n.FIS_Decision_Code__c == 'P212' || 
                                         n.FIS_Decision_Code__c == 'P030' )
                                    {
                                        //add application to the list of transactions to capture
                                        if ( ! ApplicationTransactionHelper.checkDuplicate ( ApplicationTransactionHelper.settleMap, n.Id ) )
                                        {
                                            applicationsToCaptureTransactions.add(n.Id);
                                            amountsMap.put(String.valueOf(n.Id),n.FIS_Credit_Limit__c);
                                        }
                                    }
                                }
                            }
                            
                            // DEPOSITS settlement
                            else if ( ( n.RecordTypeId != null ) && ( n.RecordTypeId == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) )
                            {
                                if ( String.isBlank ( o.FIS_Decision_Code__c ) ||
                                     ( (o.FIS_Decision_Code__c != 'P089' && 
                                       o.FIS_Decision_Code__c != 'P012' && 
                                       o.FIS_Decision_Code__c != 'P212' ) ||
                                       (o.FIS_Decision_Code__c =='P089' && (o.FIS_Credit_Limit__c==null || o.FIS_Credit_Limit__c==0) ) ) )
                                {
                                    if ( n.FIS_Decision_Code__c == 'P089' || 
                                         n.FIS_Decision_Code__c == 'P012' || 
                                         n.FIS_Decision_Code__c == 'P212' )
                                    {
                                        //add application to the list of transactions to capture
                                        if ( ! ApplicationTransactionHelper.checkDuplicate ( ApplicationTransactionHelper.settleMap, n.Id ) )
                                        {
                                            applicationsToCaptureTransactions.add(n.Id);
                                            amountsMap.put(String.valueOf(n.Id),n.FIS_Credit_Limit__c);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            // DEPOSIT Applications waiting for COCC callout
            if (n.RecordTypeID == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('Deposits').RecordTypeId ) 
            {
                if ( String.isBlank ( o.FIS_Decision_Code__c ) || ( o.FIS_Decision_Code__c != 'P912' ) )
                {
                    if((n.FIS_Decision_Code__c == 'P912') )
                    {
                        if ( ( n.COCC_Status_Code__c == null ) ||
                             ( ( n.COCC_Status_Code__c != null ) && ( n.COCC_Status_Code__c != 200 ) ) )
                        {
                            applicationsWaitingForCOCC.add ( n.Id ) ; 
                        }
                    }
                }
            }
            
            // UNITY Visa Applications
            if ( n.RecordTypeID == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('UNITY Visa').RecordTypeId )
            {
                //  OLD WAY
                if ( String.isBlank ( o.Application_Status__c ) || 
                    ( ( ! o.Application_Status__c.equalsIgnoreCase ( 'PENDING ISSUANCE' ) ) && ( ! o.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) ) ) )
                {
                    if ( ( n.Application_Status__c.equalsIgnoreCase ( 'PENDING ISSUANCE' ) ) || ( n.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) ) )
                    {
                        //  CHeck to see if it's new path
                        if ( LOSHelper.isNewLOS ( n ) )
                        {
                            if ( ( String.isBlank ( n.RTDX_Response_Code__c ) ) ||
                                ( String.isNotBlank ( n.RTDX_Response_Code__c ) && ( ! n.RTDX_Response_Code__c.equalsIgnoreCase ( '00' ) ) ) )
                            {
                                if ( n.RTDX_Success_Datetime__c == null )
                                {
                                    System.debug ( 'ADDING TO RTDX queue' ) ;
                                    applicationsWaitingForLOS.add ( n.ID ) ;
                                }
                            }
                        }
                        //  Otherwise do old logic to push to COCC
                        else
                        {
                            if ( ( n.COCC_Status_Code__c == null ) ||
                                ( ( n.COCC_Status_Code__c != null ) && ( n.COCC_Status_Code__c != 200 ) ) )
                            {
                                if ( n.RTDX_Success_Datetime__c == null )
                                {
                                    System.debug ( 'ADDING TO COCC queue' ) ;
                                    applicationsWaitingForCOCC.add ( n.Id ) ; 
                                }
                            }
                        }
                    }
                }

                if ( LOSHelper.isNewLOS ( n ) )
                {
                    //  ADV 407 - Approved
                    if ( String.isBlank ( o.Application_Status__c ) || 
                        ( ( ! o.Application_Status__c.equalsIgnoreCase ( 'PENDING ISSUANCE' ) ) && ( ! o.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) ) ) )
                    {
                        if ( ( n.Application_Status__c.equalsIgnoreCase ( 'PENDING ISSUANCE' ) ) || ( n.Application_Status__c.equalsIgnoreCase ( 'APPROVED' ) ) )
                        {
                            if ( ( String.isBlank ( o.RTDX_Response_Code__c ) ) && ( String.isBlank ( n.RTDX_Response_Code__c ) ) )
                            {
                                appLetterList.add ( n ) ;
                                appLetterMap.put ( n.ID, 'ADV407' ) ;
                            }
                        }
                    }
                    
                    //  ADV 406 - Counteroffer
                    if ( String.isBlank ( o.FIS_Decision_Code__c ) || 
                         ( ( o.FIS_Decision_Code__c != 'P030' ) && ( o.FIS_Decision_Code__c != 'P230' ) ) )
                    {
                        if ( String.isNotBlank ( n.FIS_Decision_Code__c ) && 
                             ( ( n.FIS_Decision_Code__c == 'P030' || n.FIS_Decision_Code__c == 'P230' ) ) )
                        {
                            appLetterList.add ( n ) ;
                            appLetterMap.put ( n.ID, 'ADV406' ) ;
                        }
                    }
                    
                    //  ADV 411 - Declined
                    if ( String.isBlank ( o.FIS_Decision_Code__c ) ||
                        ( ( o.FIS_Decision_Code__c != 'D007' ) && ( o.FIS_Decision_Code__c != 'D207' ) && 
                          ( o.FIS_Decision_Code__c != 'D014' ) && ( o.FIS_Decision_Code__c != 'D214' ) && 
                          ( o.FIS_Decision_Code__c != 'D017' ) && ( o.FIS_Decision_Code__c != 'D217' ) && 
                          ( o.FIS_Decision_Code__c != 'D019' ) && ( o.FIS_Decision_Code__c != 'D219' ) ) )
                    {
                        if ( String.isNotBlank ( n.FIS_Decision_Code__c ) &&
                             ( n.FIS_Decision_Code__c == 'D007' || n.FIS_Decision_Code__c == 'D207' || 
                               n.FIS_Decision_Code__c == 'D014' || n.FIS_Decision_Code__c == 'D214' ||
                               n.FIS_Decision_Code__c == 'D017' || n.FIS_Decision_Code__c == 'D217' || 
                               n.FIS_Decision_Code__c == 'D019' || n.FIS_Decision_Code__c == 'D219' ) )
                        {
                            appLetterList.add ( n ) ;
                            appLetterMap.put ( n.ID, 'ADV411' ) ;
                        }
                    }
                    
                    // ADV 412 - Fraud
                    if ( String.isBlank ( o.FIS_Decision_Code__c ) ||
                        ( ( o.FIS_Decision_Code__c != 'D034' ) && ( o.FIS_Decision_Code__c != 'D234' ) ) )
                    {
                        if ( String.isNotBlank ( n.FIS_Decision_Code__c ) && 
                             ( ( n.FIS_Decision_Code__c == 'D034' || n.FIS_Decision_Code__c == 'D234' ) ) )
                        {
                            appLetterList.add ( n ) ;
                            appLetterMap.put ( n.ID, 'ADV412' ) ;
                        }
                    }
                    
                    //  ADV 411 - Decline, post-bureau
                    if ( LOSHelper.isPostBureau ( n ) )
                    {
                        if ( String.isBlank ( o.FIS_Decision_Code__c ) ||
                            ( ( o.FIS_Decision_Code__c != 'D008' ) && ( o.FIS_Decision_Code__c != 'D208' ) ) )
                        {
                            if ( String.isNotBlank ( n.FIS_Decision_Code__c ) && 
                                 ( ( n.FIS_Decision_Code__c == 'D008' || n.FIS_Decision_Code__c == 'D208' ) ) )
                            {
                                appLetterList.add ( n ) ;
                                appLetterMap.put ( n.ID, 'ADV411' ) ;
                            }
                        }
                    }
                }
            }
            
            // ------------------------- END LOOP ------------------------- //
        }
        
        //  -----------------------------------------------------------------------------------
        //  Letters - Create record from map
        //  -----------------------------------------------------------------------------------
        list<Automated_Email__c> aeL = new list<Automated_Email__c> () ;
        
        if ( ( appLetterList != null ) && ( appLetterList.size () > 0 ) )
        {
            list<id> idL = new list<id> () ;
            for ( one_application__c app : appLetterList )
                idL.add ( app.ID ) ;
                
            map<ID,creditchecker__Credit_Report__c> crM = ApplicationLetterHelper.getCreditReport ( idL ) ;
            
            for ( one_application__c app : appLetterList )
            {
                String xID = app.ID ;
                if ( ! ApplicationLetterHelper.checkDuplicate ( ApplicationLetterHelper.letterMap, xID ) )
                {
                    if ( appLetterMap.get ( app.ID ).equalsIgnoreCase ( 'ADV406' ) )
                    {
                        Automated_Email__c ae = ApplicationLetterHelper.createCounterofferLetter ( app, crM.get ( app.ID ) ) ;
                        aeL.add ( ae ) ;
                    }
                    else if ( appLetterMap.get ( app.ID ).equalsIgnoreCase ( 'ADV407' ) )
                    {
                        Automated_Email__c ae = ApplicationLetterHelper.createApprovalLetter ( app, crM.get ( app.ID ) ) ;
                        aeL.add ( ae ) ;
                    }
                    else if ( appLetterMap.get ( app.ID ).equalsIgnoreCase ( 'ADV411' ) )
                    {
                        Automated_Email__c ae = ApplicationLetterHelper.createDeclineLetter ( app, crM.get ( app.ID ) ) ;
                        aeL.add ( ae ) ;
                    }
                    else if ( appLetterMap.get ( app.ID ).equalsIgnoreCase ( 'ADV412' ) )
                    {
                        Automated_Email__c ae = ApplicationLetterHelper.createFraudLetter ( app, crM.get ( app.ID ) ) ;
                        aeL.add ( ae ) ;
                    }
                }
            }
            
            if ( ( aeL != null ) && ( aeL.size () > 0 ) )
            {
                try
                {
                    insert aeL ;
                }
                catch ( Exception e )
                {
                    System.debug ( OneUnitedUtilities.parseStackTrace ( e ) ) ;
                }
            }
        }

        //  -----------------------------------------------------------------------------------
        //  Combined Email list
        //  -----------------------------------------------------------------------------------
        System.debug ( 'Sending Emails? - A: ' + lApprovedIDs.size ()  + ' F: ' + lFundingIDs.size () + 
                                      ' I: ' + lAdditionalInfoIDs.size () + ' T: ' + lTrialIDs.size ()  + 
                                      ' W: ' + lWarningIDs.size () + ' D: ' + lDeniedIds.size () +
                                      ' W: ' + lWithdrawalIDs.size () +  ' V: ' + lDeclineIDVIds.size () +
                                      ' O: ' + lDeclineOFACIds.size () + ' F: ' + lDeclineFCRAIDs.size () +
                                      ' A: ' + lACHRejectIDs.size () + ' C: ' + lCounterOfferIDs.size () +
                                      ' C: ' + lNotCitizenIDs.size () + ' A: ' + lOutOfAreaIDs.size () +
                                      ' U: ' + lUnderageIDs.size () + ' Y: ' + lYoungIDs.size () ) ;
        
        Integer emailCount = lApprovedIDs.size () + lFundingIds.size () + lAdditionalInfoIDs.size () + lTrialIDs.size () + lWarningIDs.size () + lDeniedIds.size () + 
            lWithdrawalIDs.size () + lDeclineIDVIDs.size () + lDeclineOFACIDs.size () + lDeclineFCRAIDs.size () + lACHRejectIDs.size () + lCounterOfferIDs.size () + 
            lNotCitizenIDs.size () + lOutOfAreaIDs.size () + lUnderageIDs.size () + lYoungIDs.size () ;
        
        if ( emailCount > 0 )
        {
            if ( ! System.isBatch () )
            {
                //  This one is a future call
                ApplicationEmailFunctions.sendEmails ( lApprovedIDs, lFundingIDs, lAdditionalInfoIDs, lTrialIDs, lWarningIDs, 
                                                       lDeniedIDs, lWithdrawalIDs, lDeclineIDVIds, lDeclineOFACIds, lDeclineFCRAIDs, 
                                                       lACHRejectIDs, lCounterOfferIDs, lNotCitizenIDs, lOutOfAreaIDs, lUnderageIDs, lYoungIDs ) ;
            }
            else
            {
                //  Do nothing, we're in a batch, this will be handled externally
                System.debug ( 'In Batch, not sending emails!' ) ;
            }
        }

        //  --------------------------------------------------------------
        //  Update new record types
        //  --------------------------------------------------------------
        RecordType rt = [ SELECT ID FROM RecordType WHERE developername='UNITY_Visa' AND SObjectType='Contact' ] ;
        List<Contact> cL = [SELECT ID FROM Contact where ID=:cID] ;
        
        for ( Contact c : cL )
        {
            c.RecordTypeId = rt.Id ;
        }
        
        update cL ;
        
        //  --------------------------------------------------------------
        //  GeoLocation
        //  --------------------------------------------------------------
        if ( ( lGeoLocationIDs != null ) && ( lGeoLocationIDs.size () > 0 ) )
        {
            if ( ( ! system.isBatch() ) && ( ! system.isFuture () ) )
                GeoLocationUtilities.setApplicationLocations ( lGeoLocationIDs ) ;
        }
        
        ////////// VOIDING TRANSACTION FOR DECLINED APPLICATIONS /////////////////
        List<Application_Transaction__c> lappt = [
            SELECT Transaction_Ref_Num__c,Application__c 
            FROM Application_Transaction__c 
            WHERE Application__c IN :applicationsToVoidTransactions 
            AND Status__c != 'Voided' AND Status__c != 'Failed'
        ];
        
        if(lappt.size()>0){
            List<String> applicationsIds = new List<String>();
            for(Application_Transaction__c appt:lappt){
                applicationsIds.add(appt.Application__c);
                if(applicationsIds.size()==9){
                    runVoidUSAePayTransactionsQueueable queueable = new runVoidUSAePayTransactionsQueueable(applicationsIds,null,false);
                    System.enqueueJob(queueable);
                    applicationsIds = new List<String>();
                }
            }
            if(applicationsIds.size()>0){
                runVoidUSAePayTransactionsQueueable queueable2 = new runVoidUSAePayTransactionsQueueable(applicationsIds,null,false);
                System.enqueueJob(queueable2);
            }
        }
        
        ///////////////////////////////////////////////////////////////////////////
        ////////// CAPTURING TRANSACTION FOR APPROVED APPLICATIONS /////////////////
        List<Application_Transaction__c> lapptToCapture = [
            SELECT Transaction_Ref_Num__c,Application__c 
            FROM Application_Transaction__c 
            WHERE Application__c IN :applicationsToCaptureTransactions 
            AND Status__c != 'Settled' AND Status__c != 'Failed' AND Status__c != 'Voided'
        ];
        
        if(lapptToCapture.size()>0){
            List<String> applicationsIds = new List<String>();
            for(Application_Transaction__c apptC:lapptToCapture){
                applicationsIds.add(apptC.Application__c);
                if(applicationsIds.size()==9){
                    runVoidUSAePayTransactionsQueueable queueableCap = new runVoidUSAePayTransactionsQueueable(applicationsIds,amountsMap,true);
                    System.enqueueJob(queueableCap);
                    applicationsIds = new List<String>();
                }
            }
            if(applicationsIds.size()>0){
                runVoidUSAePayTransactionsQueueable queueableCap2 = new runVoidUSAePayTransactionsQueueable(applicationsIds,amountsMap,true);
                System.enqueueJob(queueableCap2);
            }
        }
        ///////////////////////////////////////////////////////////////////////////
        
        /////////////////// COCC CALL ////////////////////////
        List<String> applicationsIds = new List<String>();
        if(!System.isQueueable()){
            for(Id appId:applicationsWaitingForCOCC){
                applicationsIds.add(appId);
                if(applicationsIds.size()==ACCOUNTS_PER_CALLOUT ){
                    runCOCC_Callouts_Queueable queueableCOCC = new runCOCC_Callouts_Queueable(applicationsIds);
                    System.enqueueJob(queueableCOCC);
                    applicationsIds = new List<String>();
                }
            }
            if(applicationsIds.size()>0){
                runCOCC_Callouts_Queueable queueableCOCC2 = new runCOCC_Callouts_Queueable(applicationsIds);
                System.enqueueJob(queueableCOCC2);
            }
        }
        //////////////////////////////////////////////////////

        /////////////////// RTDX CALL ////////////////////////
        List<String> applicationsIds2 = new List<String>();
        System.debug ( 'QUEUE LIMITS ::' + Limits.getQueueableJobs () + ' of ' + Limits.getLimitQueueableJobs () ) ;
        if ( Limits.getLimitQueueableJobs () > Limits.getQueueableJobs () )
        {
            for(Id appId:applicationsWaitingForLOS){
                applicationsIds2.add(appId);
                if(applicationsIds2.size()==UNITYVISA_PER_CALLOUT ){
                    if ( Limits.getLimitQueueableJobs () > Limits.getQueueableJobs () )
                    {
                        runRTDXCalloutsQueueable queueableLOS = new runRTDXCalloutsQueueable(applicationsIds2);
                        System.enqueueJob(queueableLOS);
                        applicationsIds2 = new List<String>();
                    }
                }
            }
            if ( Limits.getLimitQueueableJobs () > Limits.getQueueableJobs () )
            {
                if(applicationsIds2.size()>0){
                    runRTDXCalloutsQueueable queueableLOS2 = new runRTDXCalloutsQueueable(applicationsIds2);
                    System.enqueueJob(queueableLOS2);
                }
            }
            else
            {
                System.debug ( 'Hit limit on additional queueable job(s) :: ' + Limits.getLimitQueueableJobs () ) ;
            }
        }
        else
        {
            System.debug ( 'Hit limit on queueable job :: ' + Limits.getLimitQueueableJobs () ) ;
        }
        //////////////////////////////////////////////////////

        /* -----------------------------------------------------------------------------------
         * Advocate Program ?!?
         * ----------------------------------------------------------------------------------- */
        list<Referral_Code_Application__c> rcaL = new list<Referral_Code_Application__c> () ;
        
        for ( one_application__c n : Trigger.new )
        {
            //  --------------------------------------------------------------
            //  Get old data for comparison
            //  --------------------------------------------------------------
            one_application__c o = Trigger.oldMap.get ( n.ID ) ;
            
            if ( String.isBlank ( o.Advocate_Referral_Code_Text__c ) && ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) ) )
            {
                if ( n.Advocate_Referral_Code__c != null )
                {
                    Referral_Code_Application__c rca = new Referral_Code_Application__c () ;
                    rca.Application__c = n.ID ;
                    rca.Advocate_Referral_Code__c = n.Advocate_Referral_Code__c ;
                    
                    rcaL.add ( rca ) ;
                }
            }
            else if ( ( String.isNotBlank ( o.Advocate_Referral_Code_Text__c ) ) && 
                      ( String.isNotBlank ( n.Advocate_Referral_Code_Text__c ) ) &&
                      ( ! o.Advocate_Referral_Code_Text__c.equalsIgnoreCase ( n.Advocate_Referral_Code_Text__c ) ) )
            {
                Referral_Code_Application__c rca = new Referral_Code_Application__c () ;
                rca.Application__c = n.ID ;
                rca.Advocate_Referral_Code__c = n.Advocate_Referral_Code__c ;
                
                rcaL.add ( rca ) ;
            }
            
            //  --------------------------------------------------------------
            //  Advocate #1
            //  --------------------------------------------------------------
            if ( ( ( o.Contact__c == null ) && ( n.Contact__c != null ) ) || ( o.Contact__c <> n.Contact__c ) )
            {
                if ( n.RecordTypeID == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('UNITY Visa').RecordTypeId )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallylongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    
                    reallyLongName.ContactID = n.Contact__c ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'We_Are_Family' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                }
                else
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallylongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    
                    reallyLongName.ContactID = n.Contact__c ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'Road_to_Financial_Freedom' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                }
            }
            
            //  --------------------------------------------------------------
            //  Advocate #2
            //  --------------------------------------------------------------
            if ( ( ( o.Co_Applicant_Contact__c == null ) && ( n.Co_Applicant_Contact__c != null ) ) || ( o.Co_Applicant_Contact__c <> n.Co_Applicant_Contact__c ) )
            {
                if ( n.RecordTypeID == Schema.SObjectType.one_application__c.RecordTypeInfosByName.get('UNITY Visa').RecordTypeId )
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallylongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    
                    reallyLongName.ContactID = n.Co_Applicant_Contact__c ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'We_Are_Family' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                }
                else
                {
                    AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest reallylongName = new AdvocateProgramAwardInvocationFunction.AdvocateProgramAwardRequest () ;
                    
                    reallyLongName.ContactID = n.Co_Applicant_Contact__c ;
                    reallyLongName.requestType = 'Activity' ;
                    reallyLongName.requestName = 'Road_to_Financial_Freedom' ;
                    
                    reallyLongNameList.add ( reallyLongName ) ;
                }
            }
        }
        
        //  -----------------------------------------------------------------------------------
        //  Add referrals
        //  -----------------------------------------------------------------------------------
        if ( ( rcaL != null ) && ( rcaL.size () > 0 ) )
            insert rcaL ;
        
        //  -----------------------------------------------------------------------------------
        //  Add advocacy
        //  -----------------------------------------------------------------------------------
        if ( ( reallyLongNameList != null ) && ( reallyLongNameList.size () > 0 ) )
            AdvocateProgramAwardInvocationFunction.advocateProgramAward ( reallyLongNameList ) ;
    }
}