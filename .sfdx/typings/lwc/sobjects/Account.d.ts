declare module "@salesforce/schema/Account.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Account.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Account.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Account.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Account.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Account.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Account.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Account.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Account.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/Account.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/Account.BillingStreet" {
  const BillingStreet:string;
  export default BillingStreet;
}
declare module "@salesforce/schema/Account.BillingCity" {
  const BillingCity:string;
  export default BillingCity;
}
declare module "@salesforce/schema/Account.BillingState" {
  const BillingState:string;
  export default BillingState;
}
declare module "@salesforce/schema/Account.BillingPostalCode" {
  const BillingPostalCode:string;
  export default BillingPostalCode;
}
declare module "@salesforce/schema/Account.BillingCountry" {
  const BillingCountry:string;
  export default BillingCountry;
}
declare module "@salesforce/schema/Account.BillingLatitude" {
  const BillingLatitude:number;
  export default BillingLatitude;
}
declare module "@salesforce/schema/Account.BillingLongitude" {
  const BillingLongitude:number;
  export default BillingLongitude;
}
declare module "@salesforce/schema/Account.BillingGeocodeAccuracy" {
  const BillingGeocodeAccuracy:string;
  export default BillingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.BillingAddress" {
  const BillingAddress:any;
  export default BillingAddress;
}
declare module "@salesforce/schema/Account.ShippingStreet" {
  const ShippingStreet:string;
  export default ShippingStreet;
}
declare module "@salesforce/schema/Account.ShippingCity" {
  const ShippingCity:string;
  export default ShippingCity;
}
declare module "@salesforce/schema/Account.ShippingState" {
  const ShippingState:string;
  export default ShippingState;
}
declare module "@salesforce/schema/Account.ShippingPostalCode" {
  const ShippingPostalCode:string;
  export default ShippingPostalCode;
}
declare module "@salesforce/schema/Account.ShippingCountry" {
  const ShippingCountry:string;
  export default ShippingCountry;
}
declare module "@salesforce/schema/Account.ShippingLatitude" {
  const ShippingLatitude:number;
  export default ShippingLatitude;
}
declare module "@salesforce/schema/Account.ShippingLongitude" {
  const ShippingLongitude:number;
  export default ShippingLongitude;
}
declare module "@salesforce/schema/Account.ShippingGeocodeAccuracy" {
  const ShippingGeocodeAccuracy:string;
  export default ShippingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.ShippingAddress" {
  const ShippingAddress:any;
  export default ShippingAddress;
}
declare module "@salesforce/schema/Account.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Account.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Account.AccountNumber" {
  const AccountNumber:string;
  export default AccountNumber;
}
declare module "@salesforce/schema/Account.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Account.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Account.Sic" {
  const Sic:string;
  export default Sic;
}
declare module "@salesforce/schema/Account.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Account.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Account.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Account.Ownership" {
  const Ownership:string;
  export default Ownership;
}
declare module "@salesforce/schema/Account.TickerSymbol" {
  const TickerSymbol:string;
  export default TickerSymbol;
}
declare module "@salesforce/schema/Account.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Account.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Account.Site" {
  const Site:string;
  export default Site;
}
declare module "@salesforce/schema/Account.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Account.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Account.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Account.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Account.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Account.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Account.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Account.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Account.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Account.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Account.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Account.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Account.IsCustomerPortal" {
  const IsCustomerPortal:boolean;
  export default IsCustomerPortal;
}
declare module "@salesforce/schema/Account.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Account.JigsawCompanyId" {
  const JigsawCompanyId:string;
  export default JigsawCompanyId;
}
declare module "@salesforce/schema/Account.AccountSource" {
  const AccountSource:string;
  export default AccountSource;
}
declare module "@salesforce/schema/Account.SicDesc" {
  const SicDesc:string;
  export default SicDesc;
}
declare module "@salesforce/schema/Account.BusinessStructure__c" {
  const BusinessStructure__c:string;
  export default BusinessStructure__c;
}
declare module "@salesforce/schema/Account.FederalTaxID__c" {
  const FederalTaxID__c:string;
  export default FederalTaxID__c;
}
declare module "@salesforce/schema/Account.BusinessType__c" {
  const BusinessType__c:string;
  export default BusinessType__c;
}
declare module "@salesforce/schema/Account.DBRating__c" {
  const DBRating__c:string;
  export default DBRating__c;
}
declare module "@salesforce/schema/Account.DBNumber__c" {
  const DBNumber__c:string;
  export default DBNumber__c;
}
declare module "@salesforce/schema/Account.GrossAnnualSales__c" {
  const GrossAnnualSales__c:number;
  export default GrossAnnualSales__c;
}
declare module "@salesforce/schema/Account.AssetSize__c" {
  const AssetSize__c:string;
  export default AssetSize__c;
}
declare module "@salesforce/schema/Account.BusinessStage__c" {
  const BusinessStage__c:string;
  export default BusinessStage__c;
}
declare module "@salesforce/schema/Account.PrimaryBankName__c" {
  const PrimaryBankName__c:string;
  export default PrimaryBankName__c;
}
declare module "@salesforce/schema/Account.InvestmentType__c" {
  const InvestmentType__c:string;
  export default InvestmentType__c;
}
declare module "@salesforce/schema/Account.DateBusinessStarted__c" {
  const DateBusinessStarted__c:string;
  export default DateBusinessStarted__c;
}
declare module "@salesforce/schema/Account.UploadID__c" {
  const UploadID__c:string;
  export default UploadID__c;
}
declare module "@salesforce/schema/Account.Marketing_Opt_Out__c" {
  const Marketing_Opt_Out__c:boolean;
  export default Marketing_Opt_Out__c;
}
declare module "@salesforce/schema/Account.ADDDATE__c" {
  const ADDDATE__c:any;
  export default ADDDATE__c;
}
declare module "@salesforce/schema/Account.Branch_Name__c" {
  const Branch_Name__c:string;
  export default Branch_Name__c;
}
declare module "@salesforce/schema/Account.Sales_Status__c" {
  const Sales_Status__c:string;
  export default Sales_Status__c;
}
declare module "@salesforce/schema/Account.Branch__r" {
  const Branch__r:any;
  export default Branch__r;
}
declare module "@salesforce/schema/Account.Branch__c" {
  const Branch__c:any;
  export default Branch__c;
}
declare module "@salesforce/schema/Account.Business_On_Line_Banking__c" {
  const Business_On_Line_Banking__c:string;
  export default Business_On_Line_Banking__c;
}
declare module "@salesforce/schema/Account.Business_On_Line_Bill_Pay__c" {
  const Business_On_Line_Bill_Pay__c:string;
  export default Business_On_Line_Bill_Pay__c;
}
declare module "@salesforce/schema/Account.CCM_Last_History_DateTi__c" {
  const CCM_Last_History_DateTi__c:string;
  export default CCM_Last_History_DateTi__c;
}
declare module "@salesforce/schema/Account.CIPRATINGCD__c" {
  const CIPRATINGCD__c:string;
  export default CIPRATINGCD__c;
}
declare module "@salesforce/schema/Account.CREDITSCORE__c" {
  const CREDITSCORE__c:number;
  export default CREDITSCORE__c;
}
declare module "@salesforce/schema/Account.Confirm_Verified_Address__c" {
  const Confirm_Verified_Address__c:string;
  export default Confirm_Verified_Address__c;
}
declare module "@salesforce/schema/Account.DATELASTMAINT__c" {
  const DATELASTMAINT__c:any;
  export default DATELASTMAINT__c;
}
declare module "@salesforce/schema/Account.FICO_Score__c" {
  const FICO_Score__c:string;
  export default FICO_Score__c;
}
declare module "@salesforce/schema/Account.Mothers_Maiden_Name__c" {
  const Mothers_Maiden_Name__c:string;
  export default Mothers_Maiden_Name__c;
}
declare module "@salesforce/schema/Account.NAICSCDDESC__c" {
  const NAICSCDDESC__c:string;
  export default NAICSCDDESC__c;
}
declare module "@salesforce/schema/Account.NAICSCD__c" {
  const NAICSCD__c:string;
  export default NAICSCD__c;
}
declare module "@salesforce/schema/Account.Nbr_of_Properties_Disburs__c" {
  const Nbr_of_Properties_Disburs__c:string;
  export default Nbr_of_Properties_Disburs__c;
}
declare module "@salesforce/schema/Account.Non_Sole_Prop_Cash_Manage__c" {
  const Non_Sole_Prop_Cash_Manage__c:string;
  export default Non_Sole_Prop_Cash_Manage__c;
}
declare module "@salesforce/schema/Account.Originating_Institution__c" {
  const Originating_Institution__c:string;
  export default Originating_Institution__c;
}
declare module "@salesforce/schema/Account.Partnership_Type__c" {
  const Partnership_Type__c:string;
  export default Partnership_Type__c;
}
declare module "@salesforce/schema/Account.RUNDATE__c" {
  const RUNDATE__c:any;
  export default RUNDATE__c;
}
declare module "@salesforce/schema/Account.Red_Flag_Bypass__c" {
  const Red_Flag_Bypass__c:string;
  export default Red_Flag_Bypass__c;
}
declare module "@salesforce/schema/Account.Rel_CD_Accounts__c" {
  const Rel_CD_Accounts__c:number;
  export default Rel_CD_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_CD_Balance__c" {
  const Rel_CD_Balance__c:number;
  export default Rel_CD_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_CD_CLS_O__c" {
  const Rel_CD_CLS_O__c:number;
  export default Rel_CD_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_CD_CLS__c" {
  const Rel_CD_CLS__c:number;
  export default Rel_CD_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Checking_Accounts__c" {
  const Rel_Checking_Accounts__c:number;
  export default Rel_Checking_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Checking_Balance__c" {
  const Rel_Checking_Balance__c:number;
  export default Rel_Checking_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Checking_CLS_O__c" {
  const Rel_Checking_CLS_O__c:number;
  export default Rel_Checking_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Checking_CLS__c" {
  const Rel_Checking_CLS__c:number;
  export default Rel_Checking_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Commercial_Balance__c" {
  const Rel_Commercial_Balance__c:number;
  export default Rel_Commercial_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Commercial_CLS_O__c" {
  const Rel_Commercial_CLS_O__c:number;
  export default Rel_Commercial_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Commercial_CLS__c" {
  const Rel_Commercial_CLS__c:number;
  export default Rel_Commercial_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Commercial_Loans__c" {
  const Rel_Commercial_Loans__c:number;
  export default Rel_Commercial_Loans__c;
}
declare module "@salesforce/schema/Account.Rel_Consumer_Accounts__c" {
  const Rel_Consumer_Accounts__c:number;
  export default Rel_Consumer_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Consumer_Balance__c" {
  const Rel_Consumer_Balance__c:number;
  export default Rel_Consumer_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Consumer_CLS_O__c" {
  const Rel_Consumer_CLS_O__c:number;
  export default Rel_Consumer_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Consumer_CLS__c" {
  const Rel_Consumer_CLS__c:number;
  export default Rel_Consumer_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Deposit_Accounts__c" {
  const Rel_Deposit_Accounts__c:number;
  export default Rel_Deposit_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Deposit_Balance__c" {
  const Rel_Deposit_Balance__c:number;
  export default Rel_Deposit_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Loan_Accounts__c" {
  const Rel_Loan_Accounts__c:number;
  export default Rel_Loan_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Loan_Balance__c" {
  const Rel_Loan_Balance__c:number;
  export default Rel_Loan_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Mortgage_Balance__c" {
  const Rel_Mortgage_Balance__c:number;
  export default Rel_Mortgage_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Mortgage_CLS_O__c" {
  const Rel_Mortgage_CLS_O__c:number;
  export default Rel_Mortgage_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Mortgage_CLS__c" {
  const Rel_Mortgage_CLS__c:number;
  export default Rel_Mortgage_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Mortgage_Loans__c" {
  const Rel_Mortgage_Loans__c:number;
  export default Rel_Mortgage_Loans__c;
}
declare module "@salesforce/schema/Account.Rel_Most_Trans_DOW_ATM__c" {
  const Rel_Most_Trans_DOW_ATM__c:string;
  export default Rel_Most_Trans_DOW_ATM__c;
}
declare module "@salesforce/schema/Account.Rel_Most_Trans_DOW_Bra__c" {
  const Rel_Most_Trans_DOW_Bra__c:string;
  export default Rel_Most_Trans_DOW_Bra__c;
}
declare module "@salesforce/schema/Account.Rel_Most_Trans_DOW_WWW__c" {
  const Rel_Most_Trans_DOW_WWW__c:string;
  export default Rel_Most_Trans_DOW_WWW__c;
}
declare module "@salesforce/schema/Account.Rel_Most_Trans_DOW__c" {
  const Rel_Most_Trans_DOW__c:string;
  export default Rel_Most_Trans_DOW__c;
}
declare module "@salesforce/schema/Account.Rel_Retire_Accounts__c" {
  const Rel_Retire_Accounts__c:number;
  export default Rel_Retire_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Retire_Balance__c" {
  const Rel_Retire_Balance__c:number;
  export default Rel_Retire_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Retire_CLS_O__c" {
  const Rel_Retire_CLS_O__c:number;
  export default Rel_Retire_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Retire_CLS__c" {
  const Rel_Retire_CLS__c:number;
  export default Rel_Retire_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Safe_Dep_Accounts__c" {
  const Rel_Safe_Dep_Accounts__c:number;
  export default Rel_Safe_Dep_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Safe_Dep_Balance__c" {
  const Rel_Safe_Dep_Balance__c:number;
  export default Rel_Safe_Dep_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Safe_Dep_CLS_O__c" {
  const Rel_Safe_Dep_CLS_O__c:number;
  export default Rel_Safe_Dep_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Safe_Dep_CLS__c" {
  const Rel_Safe_Dep_CLS__c:number;
  export default Rel_Safe_Dep_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Savings_Accounts__c" {
  const Rel_Savings_Accounts__c:number;
  export default Rel_Savings_Accounts__c;
}
declare module "@salesforce/schema/Account.Rel_Savings_Balance__c" {
  const Rel_Savings_Balance__c:number;
  export default Rel_Savings_Balance__c;
}
declare module "@salesforce/schema/Account.Rel_Savings_CLS_O__c" {
  const Rel_Savings_CLS_O__c:number;
  export default Rel_Savings_CLS_O__c;
}
declare module "@salesforce/schema/Account.Rel_Savings_CLS__c" {
  const Rel_Savings_CLS__c:number;
  export default Rel_Savings_CLS__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_ATM__c" {
  const Rel_Trans_ATM__c:number;
  export default Rel_Trans_ATM__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_Batch__c" {
  const Rel_Trans_Batch__c:number;
  export default Rel_Trans_Batch__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_Bra__c" {
  const Rel_Trans_Bra__c:number;
  export default Rel_Trans_Bra__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_VRU__c" {
  const Rel_Trans_VRU__c:number;
  export default Rel_Trans_VRU__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_WWW__c" {
  const Rel_Trans_WWW__c:number;
  export default Rel_Trans_WWW__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_on_Day_ATM__c" {
  const Rel_Trans_on_Day_ATM__c:number;
  export default Rel_Trans_on_Day_ATM__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_on_Day_Bra__c" {
  const Rel_Trans_on_Day_Bra__c:number;
  export default Rel_Trans_on_Day_Bra__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_on_Day_WWW__c" {
  const Rel_Trans_on_Day_WWW__c:number;
  export default Rel_Trans_on_Day_WWW__c;
}
declare module "@salesforce/schema/Account.Rel_Trans_on_Day__c" {
  const Rel_Trans_on_Day__c:number;
  export default Rel_Trans_on_Day__c;
}
declare module "@salesforce/schema/Account.Report_1099_Interest__c" {
  const Report_1099_Interest__c:boolean;
  export default Report_1099_Interest__c;
}
declare module "@salesforce/schema/Account.SICSUBCDDESC__c" {
  const SICSUBCDDESC__c:string;
  export default SICSUBCDDESC__c;
}
declare module "@salesforce/schema/Account.SICSUBCD__c" {
  const SICSUBCD__c:string;
  export default SICSUBCD__c;
}
declare module "@salesforce/schema/Account.Safe_Deposit_Box__c" {
  const Safe_Deposit_Box__c:string;
  export default Safe_Deposit_Box__c;
}
declare module "@salesforce/schema/Account.Self_Employed__c" {
  const Self_Employed__c:string;
  export default Self_Employed__c;
}
declare module "@salesforce/schema/Account.TAXEXEMPTYN__c" {
  const TAXEXEMPTYN__c:boolean;
  export default TAXEXEMPTYN__c;
}
declare module "@salesforce/schema/Account.Closed_Deposit_Accounts__c" {
  const Closed_Deposit_Accounts__c:number;
  export default Closed_Deposit_Accounts__c;
}
declare module "@salesforce/schema/Account.Closed_Deposit_Balance__c" {
  const Closed_Deposit_Balance__c:number;
  export default Closed_Deposit_Balance__c;
}
declare module "@salesforce/schema/Account.Closed_Loan_Accounts__c" {
  const Closed_Loan_Accounts__c:number;
  export default Closed_Loan_Accounts__c;
}
declare module "@salesforce/schema/Account.Closed_Loan_Balance__c" {
  const Closed_Loan_Balance__c:number;
  export default Closed_Loan_Balance__c;
}
declare module "@salesforce/schema/Account.Federal_Tax_ID_Masked__c" {
  const Federal_Tax_ID_Masked__c:string;
  export default Federal_Tax_ID_Masked__c;
}
declare module "@salesforce/schema/Account.Affiliate_ID__c" {
  const Affiliate_ID__c:string;
  export default Affiliate_ID__c;
}
declare module "@salesforce/schema/Account.CPA_Rate__c" {
  const CPA_Rate__c:number;
  export default CPA_Rate__c;
}
declare module "@salesforce/schema/Account.Created_Date__c" {
  const Created_Date__c:any;
  export default Created_Date__c;
}
declare module "@salesforce/schema/Account.Created_Year__c" {
  const Created_Year__c:number;
  export default Created_Year__c;
}
declare module "@salesforce/schema/Account.Transaction_at_Stocker__c" {
  const Transaction_at_Stocker__c:boolean;
  export default Transaction_at_Stocker__c;
}
declare module "@salesforce/schema/Account.Business_Email__c" {
  const Business_Email__c:string;
  export default Business_Email__c;
}
declare module "@salesforce/schema/Account.Referral_Tracking_Hold_Days__c" {
  const Referral_Tracking_Hold_Days__c:number;
  export default Referral_Tracking_Hold_Days__c;
}
declare module "@salesforce/schema/Account.Tracking_Pixel__c" {
  const Tracking_Pixel__c:boolean;
  export default Tracking_Pixel__c;
}
declare module "@salesforce/schema/Account.Available_for_Survey__c" {
  const Available_for_Survey__c:boolean;
  export default Available_for_Survey__c;
}
declare module "@salesforce/schema/Account.Last_Survey_Sent__c" {
  const Last_Survey_Sent__c:any;
  export default Last_Survey_Sent__c;
}
declare module "@salesforce/schema/Account.Accounts__c" {
  const Accounts__c:number;
  export default Accounts__c;
}
declare module "@salesforce/schema/Account.Had_Active_Account__c" {
  const Had_Active_Account__c:boolean;
  export default Had_Active_Account__c;
}
declare module "@salesforce/schema/Account.Any_Volunteer_Needs__c" {
  const Any_Volunteer_Needs__c:string;
  export default Any_Volunteer_Needs__c;
}
declare module "@salesforce/schema/Account.Call_Center_User__c" {
  const Call_Center_User__c:boolean;
  export default Call_Center_User__c;
}
declare module "@salesforce/schema/Account.Company_Mission_Statement__c" {
  const Company_Mission_Statement__c:string;
  export default Company_Mission_Statement__c;
}
declare module "@salesforce/schema/Account.Date_Of_Last_Quick_Profile_Update__c" {
  const Date_Of_Last_Quick_Profile_Update__c:any;
  export default Date_Of_Last_Quick_Profile_Update__c;
}
declare module "@salesforce/schema/Account.Date_of_Last_Full_Profile_Update__c" {
  const Date_of_Last_Full_Profile_Update__c:any;
  export default Date_of_Last_Full_Profile_Update__c;
}
declare module "@salesforce/schema/Account.Do_you_own_a_home__c" {
  const Do_you_own_a_home__c:string;
  export default Do_you_own_a_home__c;
}
declare module "@salesforce/schema/Account.Do_you_own_other_Real_Estate__c" {
  const Do_you_own_other_Real_Estate__c:string;
  export default Do_you_own_other_Real_Estate__c;
}
declare module "@salesforce/schema/Account.Facebook_Handle__c" {
  const Facebook_Handle__c:string;
  export default Facebook_Handle__c;
}
declare module "@salesforce/schema/Account.Growth_Plans__c" {
  const Growth_Plans__c:string;
  export default Growth_Plans__c;
}
declare module "@salesforce/schema/Account.Has_Active_Account__c" {
  const Has_Active_Account__c:boolean;
  export default Has_Active_Account__c;
}
declare module "@salesforce/schema/Account.Historic_Growth__c" {
  const Historic_Growth__c:string;
  export default Historic_Growth__c;
}
declare module "@salesforce/schema/Account.Instagram_Handle__c" {
  const Instagram_Handle__c:string;
  export default Instagram_Handle__c;
}
declare module "@salesforce/schema/Account.LinkedIn_Handle__c" {
  const LinkedIn_Handle__c:string;
  export default LinkedIn_Handle__c;
}
declare module "@salesforce/schema/Account.Long_Term_Financial_Goals__c" {
  const Long_Term_Financial_Goals__c:string;
  export default Long_Term_Financial_Goals__c;
}
declare module "@salesforce/schema/Account.Merchant_Bankcard_Processor__c" {
  const Merchant_Bankcard_Processor__c:string;
  export default Merchant_Bankcard_Processor__c;
}
declare module "@salesforce/schema/Account.Needs_Full_Profile__c" {
  const Needs_Full_Profile__c:boolean;
  export default Needs_Full_Profile__c;
}
declare module "@salesforce/schema/Account.Needs_Quick_Profile__c" {
  const Needs_Quick_Profile__c:boolean;
  export default Needs_Quick_Profile__c;
}
declare module "@salesforce/schema/Account.Net_Income__c" {
  const Net_Income__c:number;
  export default Net_Income__c;
}
declare module "@salesforce/schema/Account.Number_of_Locations__c" {
  const Number_of_Locations__c:number;
  export default Number_of_Locations__c;
}
declare module "@salesforce/schema/Account.Number_of_Years_in_Business__c" {
  const Number_of_Years_in_Business__c:number;
  export default Number_of_Years_in_Business__c;
}
declare module "@salesforce/schema/Account.Occupation__c" {
  const Occupation__c:string;
  export default Occupation__c;
}
declare module "@salesforce/schema/Account.Professional_Affiliations__c" {
  const Professional_Affiliations__c:string;
  export default Professional_Affiliations__c;
}
declare module "@salesforce/schema/Account.Savings_Plans_for_the_Future__c" {
  const Savings_Plans_for_the_Future__c:string;
  export default Savings_Plans_for_the_Future__c;
}
declare module "@salesforce/schema/Account.Short_Term_Financial_Goals__c" {
  const Short_Term_Financial_Goals__c:string;
  export default Short_Term_Financial_Goals__c;
}
declare module "@salesforce/schema/Account.Twitter_Handle__c" {
  const Twitter_Handle__c:string;
  export default Twitter_Handle__c;
}
declare module "@salesforce/schema/Account.Affiliated_with_any_Nonprofit_Orgs__c" {
  const Affiliated_with_any_Nonprofit_Orgs__c:string;
  export default Affiliated_with_any_Nonprofit_Orgs__c;
}
declare module "@salesforce/schema/Account.Closed_All_Accounts__c" {
  const Closed_All_Accounts__c:boolean;
  export default Closed_All_Accounts__c;
}
declare module "@salesforce/schema/Account.Attrition_Prediction__c" {
  const Attrition_Prediction__c:number;
  export default Attrition_Prediction__c;
}
declare module "@salesforce/schema/Account.geopointe__Geocode__r" {
  const geopointe__Geocode__r:any;
  export default geopointe__Geocode__r;
}
declare module "@salesforce/schema/Account.geopointe__Geocode__c" {
  const geopointe__Geocode__c:any;
  export default geopointe__Geocode__c;
}
declare module "@salesforce/schema/Account.creditchecker__Adverse_Alert__c" {
  const creditchecker__Adverse_Alert__c:number;
  export default creditchecker__Adverse_Alert__c;
}
declare module "@salesforce/schema/Account.creditchecker__Average_Score__c" {
  const creditchecker__Average_Score__c:number;
  export default creditchecker__Average_Score__c;
}
declare module "@salesforce/schema/Account.creditchecker__Bankruptcy_Count__c" {
  const creditchecker__Bankruptcy_Count__c:number;
  export default creditchecker__Bankruptcy_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Charge_Offs_Count__c" {
  const creditchecker__Charge_Offs_Count__c:number;
  export default creditchecker__Charge_Offs_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Collections_Count__c" {
  const creditchecker__Collections_Count__c:number;
  export default creditchecker__Collections_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Current_Adverse_Count__c" {
  const creditchecker__Current_Adverse_Count__c:number;
  export default creditchecker__Current_Adverse_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Date_Completed__c" {
  const creditchecker__Date_Completed__c:any;
  export default creditchecker__Date_Completed__c;
}
declare module "@salesforce/schema/Account.creditchecker__Debt_High_Credit__c" {
  const creditchecker__Debt_High_Credit__c:number;
  export default creditchecker__Debt_High_Credit__c;
}
declare module "@salesforce/schema/Account.creditchecker__Derogatory_Count__c" {
  const creditchecker__Derogatory_Count__c:number;
  export default creditchecker__Derogatory_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Disputes_Count__c" {
  const creditchecker__Disputes_Count__c:number;
  export default creditchecker__Disputes_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Most_Recent_Late__c" {
  const creditchecker__Most_Recent_Late__c:string;
  export default creditchecker__Most_Recent_Late__c;
}
declare module "@salesforce/schema/Account.creditchecker__Oldest_Date__c" {
  const creditchecker__Oldest_Date__c:string;
  export default creditchecker__Oldest_Date__c;
}
declare module "@salesforce/schema/Account.creditchecker__Previous_Adverse_Count__c" {
  const creditchecker__Previous_Adverse_Count__c:number;
  export default creditchecker__Previous_Adverse_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Public_Record_Count__c" {
  const creditchecker__Public_Record_Count__c:number;
  export default creditchecker__Public_Record_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Revolving_Credit_Utilization__c" {
  const creditchecker__Revolving_Credit_Utilization__c:number;
  export default creditchecker__Revolving_Credit_Utilization__c;
}
declare module "@salesforce/schema/Account.creditchecker__Secured_Debt__c" {
  const creditchecker__Secured_Debt__c:number;
  export default creditchecker__Secured_Debt__c;
}
declare module "@salesforce/schema/Account.creditchecker__Total_High_Credit__c" {
  const creditchecker__Total_High_Credit__c:number;
  export default creditchecker__Total_High_Credit__c;
}
declare module "@salesforce/schema/Account.creditchecker__Total_Inquiry_Count__c" {
  const creditchecker__Total_Inquiry_Count__c:number;
  export default creditchecker__Total_Inquiry_Count__c;
}
declare module "@salesforce/schema/Account.creditchecker__Total_Liability_Balance__c" {
  const creditchecker__Total_Liability_Balance__c:number;
  export default creditchecker__Total_Liability_Balance__c;
}
declare module "@salesforce/schema/Account.creditchecker__Total_Liability_PastDue__c" {
  const creditchecker__Total_Liability_PastDue__c:number;
  export default creditchecker__Total_Liability_PastDue__c;
}
declare module "@salesforce/schema/Account.creditchecker__Total_Liability_Payment__c" {
  const creditchecker__Total_Liability_Payment__c:number;
  export default creditchecker__Total_Liability_Payment__c;
}
declare module "@salesforce/schema/Account.creditchecker__Unsecured_Debt__c" {
  const creditchecker__Unsecured_Debt__c:number;
  export default creditchecker__Unsecured_Debt__c;
}
declare module "@salesforce/schema/Account.PPP_Loan_Applications__c" {
  const PPP_Loan_Applications__c:number;
  export default PPP_Loan_Applications__c;
}
