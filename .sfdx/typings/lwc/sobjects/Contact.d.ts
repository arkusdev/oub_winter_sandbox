declare module "@salesforce/schema/Contact.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Contact.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Contact.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Contact.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Contact.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Contact.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Contact.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Contact.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Contact.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Contact.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Contact.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Contact.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Contact.OtherStreet" {
  const OtherStreet:string;
  export default OtherStreet;
}
declare module "@salesforce/schema/Contact.OtherCity" {
  const OtherCity:string;
  export default OtherCity;
}
declare module "@salesforce/schema/Contact.OtherState" {
  const OtherState:string;
  export default OtherState;
}
declare module "@salesforce/schema/Contact.OtherPostalCode" {
  const OtherPostalCode:string;
  export default OtherPostalCode;
}
declare module "@salesforce/schema/Contact.OtherCountry" {
  const OtherCountry:string;
  export default OtherCountry;
}
declare module "@salesforce/schema/Contact.OtherLatitude" {
  const OtherLatitude:number;
  export default OtherLatitude;
}
declare module "@salesforce/schema/Contact.OtherLongitude" {
  const OtherLongitude:number;
  export default OtherLongitude;
}
declare module "@salesforce/schema/Contact.OtherGeocodeAccuracy" {
  const OtherGeocodeAccuracy:string;
  export default OtherGeocodeAccuracy;
}
declare module "@salesforce/schema/Contact.OtherAddress" {
  const OtherAddress:any;
  export default OtherAddress;
}
declare module "@salesforce/schema/Contact.MailingStreet" {
  const MailingStreet:string;
  export default MailingStreet;
}
declare module "@salesforce/schema/Contact.MailingCity" {
  const MailingCity:string;
  export default MailingCity;
}
declare module "@salesforce/schema/Contact.MailingState" {
  const MailingState:string;
  export default MailingState;
}
declare module "@salesforce/schema/Contact.MailingPostalCode" {
  const MailingPostalCode:string;
  export default MailingPostalCode;
}
declare module "@salesforce/schema/Contact.MailingCountry" {
  const MailingCountry:string;
  export default MailingCountry;
}
declare module "@salesforce/schema/Contact.MailingLatitude" {
  const MailingLatitude:number;
  export default MailingLatitude;
}
declare module "@salesforce/schema/Contact.MailingLongitude" {
  const MailingLongitude:number;
  export default MailingLongitude;
}
declare module "@salesforce/schema/Contact.MailingGeocodeAccuracy" {
  const MailingGeocodeAccuracy:string;
  export default MailingGeocodeAccuracy;
}
declare module "@salesforce/schema/Contact.MailingAddress" {
  const MailingAddress:any;
  export default MailingAddress;
}
declare module "@salesforce/schema/Contact.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Contact.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Contact.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/Contact.HomePhone" {
  const HomePhone:string;
  export default HomePhone;
}
declare module "@salesforce/schema/Contact.OtherPhone" {
  const OtherPhone:string;
  export default OtherPhone;
}
declare module "@salesforce/schema/Contact.AssistantPhone" {
  const AssistantPhone:string;
  export default AssistantPhone;
}
declare module "@salesforce/schema/Contact.ReportsTo" {
  const ReportsTo:any;
  export default ReportsTo;
}
declare module "@salesforce/schema/Contact.ReportsToId" {
  const ReportsToId:any;
  export default ReportsToId;
}
declare module "@salesforce/schema/Contact.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/Contact.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/Contact.Department" {
  const Department:string;
  export default Department;
}
declare module "@salesforce/schema/Contact.AssistantName" {
  const AssistantName:string;
  export default AssistantName;
}
declare module "@salesforce/schema/Contact.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Contact.Birthdate" {
  const Birthdate:any;
  export default Birthdate;
}
declare module "@salesforce/schema/Contact.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Contact.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Contact.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Contact.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/Contact.HasOptedOutOfFax" {
  const HasOptedOutOfFax:boolean;
  export default HasOptedOutOfFax;
}
declare module "@salesforce/schema/Contact.DoNotCall" {
  const DoNotCall:boolean;
  export default DoNotCall;
}
declare module "@salesforce/schema/Contact.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Contact.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Contact.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Contact.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Contact.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Contact.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Contact.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Contact.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Contact.LastCURequestDate" {
  const LastCURequestDate:any;
  export default LastCURequestDate;
}
declare module "@salesforce/schema/Contact.LastCUUpdateDate" {
  const LastCUUpdateDate:any;
  export default LastCUUpdateDate;
}
declare module "@salesforce/schema/Contact.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Contact.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Contact.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/Contact.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/Contact.IsEmailBounced" {
  const IsEmailBounced:boolean;
  export default IsEmailBounced;
}
declare module "@salesforce/schema/Contact.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Contact.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Contact.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/Contact.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/Contact.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/Contact.OwnershipPercentage__c" {
  const OwnershipPercentage__c:number;
  export default OwnershipPercentage__c;
}
declare module "@salesforce/schema/Contact.SocialSecurityNumber__c" {
  const SocialSecurityNumber__c:string;
  export default SocialSecurityNumber__c;
}
declare module "@salesforce/schema/Contact.UploadID__c" {
  const UploadID__c:string;
  export default UploadID__c;
}
declare module "@salesforce/schema/Contact.insight_update__c" {
  const insight_update__c:string;
  export default insight_update__c;
}
declare module "@salesforce/schema/Contact.BankAccountType__c" {
  const BankAccountType__c:string;
  export default BankAccountType__c;
}
declare module "@salesforce/schema/Contact.BestTimeToCall__c" {
  const BestTimeToCall__c:string;
  export default BestTimeToCall__c;
}
declare module "@salesforce/schema/Contact.BusinessStage__c" {
  const BusinessStage__c:string;
  export default BusinessStage__c;
}
declare module "@salesforce/schema/Contact.BusinessStructure__c" {
  const BusinessStructure__c:string;
  export default BusinessStructure__c;
}
declare module "@salesforce/schema/Contact.BusinessType__c" {
  const BusinessType__c:string;
  export default BusinessType__c;
}
declare module "@salesforce/schema/Contact.DateBusinessStarted__c" {
  const DateBusinessStarted__c:string;
  export default DateBusinessStarted__c;
}
declare module "@salesforce/schema/Contact.EventsList__c" {
  const EventsList__c:boolean;
  export default EventsList__c;
}
declare module "@salesforce/schema/Contact.GrossAnnualSales__c" {
  const GrossAnnualSales__c:number;
  export default GrossAnnualSales__c;
}
declare module "@salesforce/schema/Contact.Newsletter__c" {
  const Newsletter__c:boolean;
  export default Newsletter__c;
}
declare module "@salesforce/schema/Contact.NewsList__c" {
  const NewsList__c:boolean;
  export default NewsList__c;
}
declare module "@salesforce/schema/Contact.OneUnitedNewsletter__c" {
  const OneUnitedNewsletter__c:string;
  export default OneUnitedNewsletter__c;
}
declare module "@salesforce/schema/Contact.PrimaryBankName__c" {
  const PrimaryBankName__c:string;
  export default PrimaryBankName__c;
}
declare module "@salesforce/schema/Contact.ProductInterest__c" {
  const ProductInterest__c:string;
  export default ProductInterest__c;
}
declare module "@salesforce/schema/Contact.PromotionCode__c" {
  const PromotionCode__c:string;
  export default PromotionCode__c;
}
declare module "@salesforce/schema/Contact.WorkEmail__c" {
  const WorkEmail__c:string;
  export default WorkEmail__c;
}
declare module "@salesforce/schema/Contact.Years_with_Bank__c" {
  const Years_with_Bank__c:number;
  export default Years_with_Bank__c;
}
declare module "@salesforce/schema/Contact.Branch_Name__c" {
  const Branch_Name__c:string;
  export default Branch_Name__c;
}
declare module "@salesforce/schema/Contact.Rating__c" {
  const Rating__c:string;
  export default Rating__c;
}
declare module "@salesforce/schema/Contact.Sales_Status__c" {
  const Sales_Status__c:string;
  export default Sales_Status__c;
}
declare module "@salesforce/schema/Contact.Bad_Email_Address__c" {
  const Bad_Email_Address__c:boolean;
  export default Bad_Email_Address__c;
}
declare module "@salesforce/schema/Contact.e_Statements__c" {
  const e_Statements__c:boolean;
  export default e_Statements__c;
}
declare module "@salesforce/schema/Contact.OnlineBanking__c" {
  const OnlineBanking__c:boolean;
  export default OnlineBanking__c;
}
declare module "@salesforce/schema/Contact.Web_Source__c" {
  const Web_Source__c:string;
  export default Web_Source__c;
}
declare module "@salesforce/schema/Contact.Are_you_a_United_States_Citizen__c" {
  const Are_you_a_United_States_Citizen__c:string;
  export default Are_you_a_United_States_Citizen__c;
}
declare module "@salesforce/schema/Contact.Do_you_have_a_ChexSystems_record__c" {
  const Do_you_have_a_ChexSystems_record__c:string;
  export default Do_you_have_a_ChexSystems_record__c;
}
declare module "@salesforce/schema/Contact.Evening_Phone__c" {
  const Evening_Phone__c:string;
  export default Evening_Phone__c;
}
declare module "@salesforce/schema/Contact.Message__c" {
  const Message__c:string;
  export default Message__c;
}
declare module "@salesforce/schema/Contact.Organization__c" {
  const Organization__c:string;
  export default Organization__c;
}
declare module "@salesforce/schema/Contact.Referred_By__c" {
  const Referred_By__c:string;
  export default Referred_By__c;
}
declare module "@salesforce/schema/Contact.Website_Form__c" {
  const Website_Form__c:string;
  export default Website_Form__c;
}
declare module "@salesforce/schema/Contact.What_is_the_purpose_of_this_inquiry__c" {
  const What_is_the_purpose_of_this_inquiry__c:string;
  export default What_is_the_purpose_of_this_inquiry__c;
}
declare module "@salesforce/schema/Contact.What_is_your_intended_opening_deposit__c" {
  const What_is_your_intended_opening_deposit__c:string;
  export default What_is_your_intended_opening_deposit__c;
}
declare module "@salesforce/schema/Contact.What_product_are_you_interested_in__c" {
  const What_product_are_you_interested_in__c:string;
  export default What_product_are_you_interested_in__c;
}
declare module "@salesforce/schema/Contact.UTM_Campaign__c" {
  const UTM_Campaign__c:string;
  export default UTM_Campaign__c;
}
declare module "@salesforce/schema/Contact.UTM_Medium__c" {
  const UTM_Medium__c:string;
  export default UTM_Medium__c;
}
declare module "@salesforce/schema/Contact.UTM_Source__c" {
  const UTM_Source__c:string;
  export default UTM_Source__c;
}
declare module "@salesforce/schema/Contact.TaxID__c" {
  const TaxID__c:string;
  export default TaxID__c;
}
declare module "@salesforce/schema/Contact.UTM_Content__c" {
  const UTM_Content__c:string;
  export default UTM_Content__c;
}
declare module "@salesforce/schema/Contact.UTM_Term__c" {
  const UTM_Term__c:string;
  export default UTM_Term__c;
}
declare module "@salesforce/schema/Contact.UTM_VisitorID__c" {
  const UTM_VisitorID__c:string;
  export default UTM_VisitorID__c;
}
declare module "@salesforce/schema/Contact.OneUnited_Department__c" {
  const OneUnited_Department__c:string;
  export default OneUnited_Department__c;
}
declare module "@salesforce/schema/Contact.OneUnited_Position__c" {
  const OneUnited_Position__c:string;
  export default OneUnited_Position__c;
}
declare module "@salesforce/schema/Contact.ADDDATE__c" {
  const ADDDATE__c:any;
  export default ADDDATE__c;
}
declare module "@salesforce/schema/Contact.Loan_Referral_Date__c" {
  const Loan_Referral_Date__c:any;
  export default Loan_Referral_Date__c;
}
declare module "@salesforce/schema/Contact.MC_Online_Banking__c" {
  const MC_Online_Banking__c:string;
  export default MC_Online_Banking__c;
}
declare module "@salesforce/schema/Contact.MC_Salesforce_Type__c" {
  const MC_Salesforce_Type__c:string;
  export default MC_Salesforce_Type__c;
}
declare module "@salesforce/schema/Contact.Borrower_Credit_Quality__c" {
  const Borrower_Credit_Quality__c:string;
  export default Borrower_Credit_Quality__c;
}
declare module "@salesforce/schema/Contact.Branch__r" {
  const Branch__r:any;
  export default Branch__r;
}
declare module "@salesforce/schema/Contact.Branch__c" {
  const Branch__c:any;
  export default Branch__c;
}
declare module "@salesforce/schema/Contact.CRCD__c" {
  const CRCD__c:string;
  export default CRCD__c;
}
declare module "@salesforce/schema/Contact.CREDITREPORTCONSINFOCD__c" {
  const CREDITREPORTCONSINFOCD__c:string;
  export default CREDITREPORTCONSINFOCD__c;
}
declare module "@salesforce/schema/Contact.CUSTKEYWORD__c" {
  const CUSTKEYWORD__c:string;
  export default CUSTKEYWORD__c;
}
declare module "@salesforce/schema/Contact.Confirm_Verified_Address__c" {
  const Confirm_Verified_Address__c:string;
  export default Confirm_Verified_Address__c;
}
declare module "@salesforce/schema/Contact.DATEDEATH__c" {
  const DATEDEATH__c:any;
  export default DATEDEATH__c;
}
declare module "@salesforce/schema/Contact.DATELASTMAINT__c" {
  const DATELASTMAINT__c:any;
  export default DATELASTMAINT__c;
}
declare module "@salesforce/schema/Contact.Denied_BPY_Reason_DBPY__c" {
  const Denied_BPY_Reason_DBPY__c:string;
  export default Denied_BPY_Reason_DBPY__c;
}
declare module "@salesforce/schema/Contact.Denied_OLB_Reason_DOLB__c" {
  const Denied_OLB_Reason_DOLB__c:string;
  export default Denied_OLB_Reason_DOLB__c;
}
declare module "@salesforce/schema/Contact.EDUCLEVCD__c" {
  const EDUCLEVCD__c:string;
  export default EDUCLEVCD__c;
}
declare module "@salesforce/schema/Contact.Employer_EMPL__c" {
  const Employer_EMPL__c:string;
  export default Employer_EMPL__c;
}
declare module "@salesforce/schema/Contact.FICO_Score_PFIC__c" {
  const FICO_Score_PFIC__c:string;
  export default FICO_Score_PFIC__c;
}
declare module "@salesforce/schema/Contact.GRADDATE__c" {
  const GRADDATE__c:any;
  export default GRADDATE__c;
}
declare module "@salesforce/schema/Contact.INCOMELEVCD__c" {
  const INCOMELEVCD__c:string;
  export default INCOMELEVCD__c;
}
declare module "@salesforce/schema/Contact.Most_Trans_Day_of_Week_ATM__c" {
  const Most_Trans_Day_of_Week_ATM__c:string;
  export default Most_Trans_Day_of_Week_ATM__c;
}
declare module "@salesforce/schema/Contact.Most_Trans_Day_of_Week_Bran__c" {
  const Most_Trans_Day_of_Week_Bran__c:string;
  export default Most_Trans_Day_of_Week_Bran__c;
}
declare module "@salesforce/schema/Contact.Most_Trans_Day_of_Week_WWW__c" {
  const Most_Trans_Day_of_Week_WWW__c:string;
  export default Most_Trans_Day_of_Week_WWW__c;
}
declare module "@salesforce/schema/Contact.Most_Trans_Day_of_Week__c" {
  const Most_Trans_Day_of_Week__c:string;
  export default Most_Trans_Day_of_Week__c;
}
declare module "@salesforce/schema/Contact.Mothers_Maiden_Name_MMN__c" {
  const Mothers_Maiden_Name_MMN__c:string;
  export default Mothers_Maiden_Name_MMN__c;
}
declare module "@salesforce/schema/Contact.NBRDEPND__c" {
  const NBRDEPND__c:number;
  export default NBRDEPND__c;
}
declare module "@salesforce/schema/Contact.NICKNAME__c" {
  const NICKNAME__c:string;
  export default NICKNAME__c;
}
declare module "@salesforce/schema/Contact.OCCPTNCD__c" {
  const OCCPTNCD__c:string;
  export default OCCPTNCD__c;
}
declare module "@salesforce/schema/Contact.OWNRENT__c" {
  const OWNRENT__c:boolean;
  export default OWNRENT__c;
}
declare module "@salesforce/schema/Contact.Occupation_OCCU__c" {
  const Occupation_OCCU__c:string;
  export default Occupation_OCCU__c;
}
declare module "@salesforce/schema/Contact.On_Line_Banking_OBNK__c" {
  const On_Line_Banking_OBNK__c:string;
  export default On_Line_Banking_OBNK__c;
}
declare module "@salesforce/schema/Contact.On_Line_Bill_Pay_BLPY__c" {
  const On_Line_Bill_Pay_BLPY__c:string;
  export default On_Line_Bill_Pay_BLPY__c;
}
declare module "@salesforce/schema/Contact.Partnership_Type_PART__c" {
  const Partnership_Type_PART__c:string;
  export default Partnership_Type_PART__c;
}
declare module "@salesforce/schema/Contact.Personal_CD_Accounts__c" {
  const Personal_CD_Accounts__c:number;
  export default Personal_CD_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_CD_Balance__c" {
  const Personal_CD_Balance__c:number;
  export default Personal_CD_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_CD_CLSO__c" {
  const Personal_CD_CLSO__c:number;
  export default Personal_CD_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_CD_CLS__c" {
  const Personal_CD_CLS__c:number;
  export default Personal_CD_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Checking_Accounts__c" {
  const Personal_Checking_Accounts__c:number;
  export default Personal_Checking_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Checking_Balance__c" {
  const Personal_Checking_Balance__c:number;
  export default Personal_Checking_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Checking_CLSO__c" {
  const Personal_Checking_CLSO__c:number;
  export default Personal_Checking_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Checking_CLS__c" {
  const Personal_Checking_CLS__c:number;
  export default Personal_Checking_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Commercial_Balance__c" {
  const Personal_Commercial_Balance__c:number;
  export default Personal_Commercial_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Commercial_CLSO__c" {
  const Personal_Commercial_CLSO__c:number;
  export default Personal_Commercial_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Commercial_CLS__c" {
  const Personal_Commercial_CLS__c:number;
  export default Personal_Commercial_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Commercial_Loans__c" {
  const Personal_Commercial_Loans__c:number;
  export default Personal_Commercial_Loans__c;
}
declare module "@salesforce/schema/Contact.Personal_Consumer_Accounts__c" {
  const Personal_Consumer_Accounts__c:number;
  export default Personal_Consumer_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Consumer_Balance__c" {
  const Personal_Consumer_Balance__c:number;
  export default Personal_Consumer_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Consumer_CLSO__c" {
  const Personal_Consumer_CLSO__c:number;
  export default Personal_Consumer_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Consumer_CLS__c" {
  const Personal_Consumer_CLS__c:number;
  export default Personal_Consumer_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Deposit_Accounts__c" {
  const Personal_Deposit_Accounts__c:number;
  export default Personal_Deposit_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Deposit_Balance__c" {
  const Personal_Deposit_Balance__c:number;
  export default Personal_Deposit_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Loan_Accounts__c" {
  const Personal_Loan_Accounts__c:number;
  export default Personal_Loan_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Loan_Balance__c" {
  const Personal_Loan_Balance__c:number;
  export default Personal_Loan_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Mortgage_Balance__c" {
  const Personal_Mortgage_Balance__c:number;
  export default Personal_Mortgage_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Mortgage_CLSO__c" {
  const Personal_Mortgage_CLSO__c:number;
  export default Personal_Mortgage_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Mortgage_CLS__c" {
  const Personal_Mortgage_CLS__c:number;
  export default Personal_Mortgage_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Mortgage_Loans__c" {
  const Personal_Mortgage_Loans__c:number;
  export default Personal_Mortgage_Loans__c;
}
declare module "@salesforce/schema/Contact.Personal_Retire_Accounts__c" {
  const Personal_Retire_Accounts__c:number;
  export default Personal_Retire_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Retire_CLSO__c" {
  const Personal_Retire_CLSO__c:number;
  export default Personal_Retire_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Retire_CLS__c" {
  const Personal_Retire_CLS__c:number;
  export default Personal_Retire_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Retirement_Balance__c" {
  const Personal_Retirement_Balance__c:number;
  export default Personal_Retirement_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Safe_Dep_Accounts__c" {
  const Personal_Safe_Dep_Accounts__c:number;
  export default Personal_Safe_Dep_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Safe_Dep_Balance__c" {
  const Personal_Safe_Dep_Balance__c:number;
  export default Personal_Safe_Dep_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Safe_Dep_CLSO__c" {
  const Personal_Safe_Dep_CLSO__c:number;
  export default Personal_Safe_Dep_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Safe_Dep_CLS__c" {
  const Personal_Safe_Dep_CLS__c:number;
  export default Personal_Safe_Dep_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Savings_Accounts__c" {
  const Personal_Savings_Accounts__c:number;
  export default Personal_Savings_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_Savings_Balance__c" {
  const Personal_Savings_Balance__c:number;
  export default Personal_Savings_Balance__c;
}
declare module "@salesforce/schema/Contact.Personal_Savings_CLSO__c" {
  const Personal_Savings_CLSO__c:number;
  export default Personal_Savings_CLSO__c;
}
declare module "@salesforce/schema/Contact.Personal_Savings_CLS__c" {
  const Personal_Savings_CLS__c:number;
  export default Personal_Savings_CLS__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_Branch__c" {
  const Personal_Trans_Branch__c:number;
  export default Personal_Trans_Branch__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_VRU__c" {
  const Personal_Trans_VRU__c:number;
  export default Personal_Trans_VRU__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_WWW__c" {
  const Personal_Trans_WWW__c:number;
  export default Personal_Trans_WWW__c;
}
declare module "@salesforce/schema/Contact.Personal_Transactions_ATM__c" {
  const Personal_Transactions_ATM__c:number;
  export default Personal_Transactions_ATM__c;
}
declare module "@salesforce/schema/Contact.Personal_Transactions_Batch__c" {
  const Personal_Transactions_Batch__c:number;
  export default Personal_Transactions_Batch__c;
}
declare module "@salesforce/schema/Contact.Post_closing_audit_approv__c" {
  const Post_closing_audit_approv__c:string;
  export default Post_closing_audit_approv__c;
}
declare module "@salesforce/schema/Contact.Post_closing_documents_co__c" {
  const Post_closing_documents_co__c:string;
  export default Post_closing_documents_co__c;
}
declare module "@salesforce/schema/Contact.Post_closing_fields_audit__c" {
  const Post_closing_fields_audit__c:string;
  export default Post_closing_fields_audit__c;
}
declare module "@salesforce/schema/Contact.Red_Flag_Bypass_RFBP__c" {
  const Red_Flag_Bypass_RFBP__c:string;
  export default Red_Flag_Bypass_RFBP__c;
}
declare module "@salesforce/schema/Contact.SHORTNAME__c" {
  const SHORTNAME__c:string;
  export default SHORTNAME__c;
}
declare module "@salesforce/schema/Contact.SPOUSEPERSNBR__r" {
  const SPOUSEPERSNBR__r:any;
  export default SPOUSEPERSNBR__r;
}
declare module "@salesforce/schema/Contact.SPOUSEPERSNBR__c" {
  const SPOUSEPERSNBR__c:any;
  export default SPOUSEPERSNBR__c;
}
declare module "@salesforce/schema/Contact.SUFFIX__c" {
  const SUFFIX__c:string;
  export default SUFFIX__c;
}
declare module "@salesforce/schema/Contact.Safe_Deposit_Box_SAFE__c" {
  const Safe_Deposit_Box_SAFE__c:string;
  export default Safe_Deposit_Box_SAFE__c;
}
declare module "@salesforce/schema/Contact.Self_Employed_PSLF__c" {
  const Self_Employed_PSLF__c:string;
  export default Self_Employed_PSLF__c;
}
declare module "@salesforce/schema/Contact.Shared_Teller_Terminal_ID__c" {
  const Shared_Teller_Terminal_ID__c:string;
  export default Shared_Teller_Terminal_ID__c;
}
declare module "@salesforce/schema/Contact.Sole_Prop_Cash_Management__c" {
  const Sole_Prop_Cash_Management__c:string;
  export default Sole_Prop_Cash_Management__c;
}
declare module "@salesforce/schema/Contact.Transactions_on_Day_ATM__c" {
  const Transactions_on_Day_ATM__c:number;
  export default Transactions_on_Day_ATM__c;
}
declare module "@salesforce/schema/Contact.Transactions_on_Day_Branch__c" {
  const Transactions_on_Day_Branch__c:number;
  export default Transactions_on_Day_Branch__c;
}
declare module "@salesforce/schema/Contact.Transactions_on_Day_WWW__c" {
  const Transactions_on_Day_WWW__c:number;
  export default Transactions_on_Day_WWW__c;
}
declare module "@salesforce/schema/Contact.Transactions_on_Day__c" {
  const Transactions_on_Day__c:number;
  export default Transactions_on_Day__c;
}
declare module "@salesforce/schema/Contact.Credit_Score_Borrower__c" {
  const Credit_Score_Borrower__c:string;
  export default Credit_Score_Borrower__c;
}
declare module "@salesforce/schema/Contact.Credit_Score_Co_Borrower__c" {
  const Credit_Score_Co_Borrower__c:string;
  export default Credit_Score_Co_Borrower__c;
}
declare module "@salesforce/schema/Contact.Closed_Deposit_Accounts__c" {
  const Closed_Deposit_Accounts__c:number;
  export default Closed_Deposit_Accounts__c;
}
declare module "@salesforce/schema/Contact.Closed_Deposit_Balance__c" {
  const Closed_Deposit_Balance__c:number;
  export default Closed_Deposit_Balance__c;
}
declare module "@salesforce/schema/Contact.Closed_Loan_Accounts__c" {
  const Closed_Loan_Accounts__c:number;
  export default Closed_Loan_Accounts__c;
}
declare module "@salesforce/schema/Contact.Closed_Loan_Balance__c" {
  const Closed_Loan_Balance__c:number;
  export default Closed_Loan_Balance__c;
}
declare module "@salesforce/schema/Contact.Years_as_a_Customer__c" {
  const Years_as_a_Customer__c:number;
  export default Years_as_a_Customer__c;
}
declare module "@salesforce/schema/Contact.Created_Date__c" {
  const Created_Date__c:any;
  export default Created_Date__c;
}
declare module "@salesforce/schema/Contact.Created_Year__c" {
  const Created_Year__c:number;
  export default Created_Year__c;
}
declare module "@salesforce/schema/Contact.Personal_CC_Accounts__c" {
  const Personal_CC_Accounts__c:number;
  export default Personal_CC_Accounts__c;
}
declare module "@salesforce/schema/Contact.Personal_CC_Balances__c" {
  const Personal_CC_Balances__c:number;
  export default Personal_CC_Balances__c;
}
declare module "@salesforce/schema/Contact.Personal_CC_Available__c" {
  const Personal_CC_Available__c:number;
  export default Personal_CC_Available__c;
}
declare module "@salesforce/schema/Contact.Personal_CC_Limits__c" {
  const Personal_CC_Limits__c:number;
  export default Personal_CC_Limits__c;
}
declare module "@salesforce/schema/Contact.Transaction_at_Stocker__c" {
  const Transaction_at_Stocker__c:boolean;
  export default Transaction_at_Stocker__c;
}
declare module "@salesforce/schema/Contact.Top_Checking_Gift_Received__c" {
  const Top_Checking_Gift_Received__c:boolean;
  export default Top_Checking_Gift_Received__c;
}
declare module "@salesforce/schema/Contact.Top_Checking_Picture_Taken__c" {
  const Top_Checking_Picture_Taken__c:boolean;
  export default Top_Checking_Picture_Taken__c;
}
declare module "@salesforce/schema/Contact.New_Since_2014__c" {
  const New_Since_2014__c:boolean;
  export default New_Since_2014__c;
}
declare module "@salesforce/schema/Contact.Computed_Phone__c" {
  const Computed_Phone__c:string;
  export default Computed_Phone__c;
}
declare module "@salesforce/schema/Contact.Age__c" {
  const Age__c:number;
  export default Age__c;
}
declare module "@salesforce/schema/Contact.Available_for_Survey__c" {
  const Available_for_Survey__c:boolean;
  export default Available_for_Survey__c;
}
declare module "@salesforce/schema/Contact.Last_Survey_Sent__c" {
  const Last_Survey_Sent__c:any;
  export default Last_Survey_Sent__c;
}
declare module "@salesforce/schema/Contact.Date_Last_Deposit_App__c" {
  const Date_Last_Deposit_App__c:any;
  export default Date_Last_Deposit_App__c;
}
declare module "@salesforce/schema/Contact.LeadSource_Change_Date__c" {
  const LeadSource_Change_Date__c:any;
  export default LeadSource_Change_Date__c;
}
declare module "@salesforce/schema/Contact.DeDuplication_Date__c" {
  const DeDuplication_Date__c:any;
  export default DeDuplication_Date__c;
}
declare module "@salesforce/schema/Contact.IsDepositLead__c" {
  const IsDepositLead__c:boolean;
  export default IsDepositLead__c;
}
declare module "@salesforce/schema/Contact.Qualifile_Error_List__c" {
  const Qualifile_Error_List__c:boolean;
  export default Qualifile_Error_List__c;
}
declare module "@salesforce/schema/Contact.End_of_Last_Journey__c" {
  const End_of_Last_Journey__c:any;
  export default End_of_Last_Journey__c;
}
declare module "@salesforce/schema/Contact.Start_of_Last_Journey__c" {
  const Start_of_Last_Journey__c:any;
  export default Start_of_Last_Journey__c;
}
declare module "@salesforce/schema/Contact.eContacts__Business_Card__c" {
  const eContacts__Business_Card__c:string;
  export default eContacts__Business_Card__c;
}
declare module "@salesforce/schema/Contact.eContacts__Created_By_eContacts__c" {
  const eContacts__Created_By_eContacts__c:boolean;
  export default eContacts__Created_By_eContacts__c;
}
declare module "@salesforce/schema/Contact.eContacts__Location_Text__c" {
  const eContacts__Location_Text__c:string;
  export default eContacts__Location_Text__c;
}
declare module "@salesforce/schema/Contact.eContacts__Location__Latitude__s" {
  const eContacts__Location__Latitude__s:number;
  export default eContacts__Location__Latitude__s;
}
declare module "@salesforce/schema/Contact.eContacts__Location__Longitude__s" {
  const eContacts__Location__Longitude__s:number;
  export default eContacts__Location__Longitude__s;
}
declare module "@salesforce/schema/Contact.eContacts__Location__c" {
  const eContacts__Location__c:any;
  export default eContacts__Location__c;
}
declare module "@salesforce/schema/Contact.Date_Last_UNITY_Visa_App__c" {
  const Date_Last_UNITY_Visa_App__c:any;
  export default Date_Last_UNITY_Visa_App__c;
}
declare module "@salesforce/schema/Contact.IsUNITYVisaLead__c" {
  const IsUNITYVisaLead__c:boolean;
  export default IsUNITYVisaLead__c;
}
declare module "@salesforce/schema/Contact.Has_3rd_Party_Mortgage__c" {
  const Has_3rd_Party_Mortgage__c:boolean;
  export default Has_3rd_Party_Mortgage__c;
}
declare module "@salesforce/schema/Contact.et4ae5__HasOptedOutOfMobile__c" {
  const et4ae5__HasOptedOutOfMobile__c:boolean;
  export default et4ae5__HasOptedOutOfMobile__c;
}
declare module "@salesforce/schema/Contact.et4ae5__Mobile_Country_Code__c" {
  const et4ae5__Mobile_Country_Code__c:string;
  export default et4ae5__Mobile_Country_Code__c;
}
declare module "@salesforce/schema/Contact.Advocate_Text__c" {
  const Advocate_Text__c:string;
  export default Advocate_Text__c;
}
declare module "@salesforce/schema/Contact.Advocate_Timestamp__c" {
  const Advocate_Timestamp__c:any;
  export default Advocate_Timestamp__c;
}
declare module "@salesforce/schema/Contact.Advocate__c" {
  const Advocate__c:boolean;
  export default Advocate__c;
}
declare module "@salesforce/schema/Contact.Ambassador_Timestamp__c" {
  const Ambassador_Timestamp__c:any;
  export default Ambassador_Timestamp__c;
}
declare module "@salesforce/schema/Contact.Ambassador__c" {
  const Ambassador__c:boolean;
  export default Ambassador__c;
}
declare module "@salesforce/schema/Contact.Facebook_URL__c" {
  const Facebook_URL__c:string;
  export default Facebook_URL__c;
}
declare module "@salesforce/schema/Contact.Instagram_Handle__c" {
  const Instagram_Handle__c:string;
  export default Instagram_Handle__c;
}
declare module "@salesforce/schema/Contact.Twitter_Handle__c" {
  const Twitter_Handle__c:string;
  export default Twitter_Handle__c;
}
declare module "@salesforce/schema/Contact.Name_Email_Lookup__c" {
  const Name_Email_Lookup__c:string;
  export default Name_Email_Lookup__c;
}
declare module "@salesforce/schema/Contact.Contact_DeDupe_Attempt__c" {
  const Contact_DeDupe_Attempt__c:any;
  export default Contact_DeDupe_Attempt__c;
}
declare module "@salesforce/schema/Contact.Contacts__c" {
  const Contacts__c:number;
  export default Contacts__c;
}
declare module "@salesforce/schema/Contact.Most_Recent_Closed_Financial_Account__r" {
  const Most_Recent_Closed_Financial_Account__r:any;
  export default Most_Recent_Closed_Financial_Account__r;
}
declare module "@salesforce/schema/Contact.Most_Recent_Closed_Financial_Account__c" {
  const Most_Recent_Closed_Financial_Account__c:any;
  export default Most_Recent_Closed_Financial_Account__c;
}
declare module "@salesforce/schema/Contact.Sent_Closed_Account_Survey__c" {
  const Sent_Closed_Account_Survey__c:any;
  export default Sent_Closed_Account_Survey__c;
}
declare module "@salesforce/schema/Contact.Affiliated_with_any_Nonprofit_Orgs__c" {
  const Affiliated_with_any_Nonprofit_Orgs__c:string;
  export default Affiliated_with_any_Nonprofit_Orgs__c;
}
declare module "@salesforce/schema/Contact.Business_Owner__c" {
  const Business_Owner__c:string;
  export default Business_Owner__c;
}
declare module "@salesforce/schema/Contact.Call_Center_User__c" {
  const Call_Center_User__c:boolean;
  export default Call_Center_User__c;
}
declare module "@salesforce/schema/Contact.Date_of_Last_Quick_Profile_Update__c" {
  const Date_of_Last_Quick_Profile_Update__c:any;
  export default Date_of_Last_Quick_Profile_Update__c;
}
declare module "@salesforce/schema/Contact.Date_of_Last_full_Profile_Update__c" {
  const Date_of_Last_full_Profile_Update__c:any;
  export default Date_of_Last_full_Profile_Update__c;
}
declare module "@salesforce/schema/Contact.Do_you_own_a_home__c" {
  const Do_you_own_a_home__c:string;
  export default Do_you_own_a_home__c;
}
declare module "@salesforce/schema/Contact.Do_you_own_other_Real_Estate__c" {
  const Do_you_own_other_Real_Estate__c:string;
  export default Do_you_own_other_Real_Estate__c;
}
declare module "@salesforce/schema/Contact.Facebook_Handle__c" {
  const Facebook_Handle__c:string;
  export default Facebook_Handle__c;
}
declare module "@salesforce/schema/Contact.Gender__c" {
  const Gender__c:string;
  export default Gender__c;
}
declare module "@salesforce/schema/Contact.Has_Active_Account__c" {
  const Has_Active_Account__c:boolean;
  export default Has_Active_Account__c;
}
declare module "@salesforce/schema/Contact.LinkedIn_Handle__c" {
  const LinkedIn_Handle__c:string;
  export default LinkedIn_Handle__c;
}
declare module "@salesforce/schema/Contact.Long_Term_Financial_Goals__c" {
  const Long_Term_Financial_Goals__c:string;
  export default Long_Term_Financial_Goals__c;
}
declare module "@salesforce/schema/Contact.Marital_Status__c" {
  const Marital_Status__c:string;
  export default Marital_Status__c;
}
declare module "@salesforce/schema/Contact.Needs_Full_Profile__c" {
  const Needs_Full_Profile__c:boolean;
  export default Needs_Full_Profile__c;
}
declare module "@salesforce/schema/Contact.Needs_Quick_Profile__c" {
  const Needs_Quick_Profile__c:boolean;
  export default Needs_Quick_Profile__c;
}
declare module "@salesforce/schema/Contact.Savings_Plans_for_the_Future__c" {
  const Savings_Plans_for_the_Future__c:string;
  export default Savings_Plans_for_the_Future__c;
}
declare module "@salesforce/schema/Contact.Short_Term_Financial_Goals__c" {
  const Short_Term_Financial_Goals__c:string;
  export default Short_Term_Financial_Goals__c;
}
declare module "@salesforce/schema/Contact.Affiliated_with_Nonprofit_orgs__c" {
  const Affiliated_with_Nonprofit_orgs__c:string;
  export default Affiliated_with_Nonprofit_orgs__c;
}
declare module "@salesforce/schema/Contact.Employer__c" {
  const Employer__c:string;
  export default Employer__c;
}
declare module "@salesforce/schema/Contact.Occupation__c" {
  const Occupation__c:string;
  export default Occupation__c;
}
declare module "@salesforce/schema/Contact.Sales_Spoke_to_Customer__c" {
  const Sales_Spoke_to_Customer__c:any;
  export default Sales_Spoke_to_Customer__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_POS__c" {
  const Personal_Trans_POS__c:number;
  export default Personal_Trans_POS__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_POS_Amount__c" {
  const Personal_Trans_POS_Amount__c:number;
  export default Personal_Trans_POS_Amount__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_XDEP__c" {
  const Personal_Trans_XDEP__c:number;
  export default Personal_Trans_XDEP__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_XDEP_Amount__c" {
  const Personal_Trans_XDEP_Amount__c:number;
  export default Personal_Trans_XDEP_Amount__c;
}
declare module "@salesforce/schema/Contact.Personal_Funds_Trans__c" {
  const Personal_Funds_Trans__c:number;
  export default Personal_Funds_Trans__c;
}
declare module "@salesforce/schema/Contact.Personal_Funds_Trans_Amount__c" {
  const Personal_Funds_Trans_Amount__c:number;
  export default Personal_Funds_Trans_Amount__c;
}
declare module "@salesforce/schema/Contact.Personal_Pop_Trans__c" {
  const Personal_Pop_Trans__c:number;
  export default Personal_Pop_Trans__c;
}
declare module "@salesforce/schema/Contact.Personal_Pop_Trans_Amount__c" {
  const Personal_Pop_Trans_Amount__c:number;
  export default Personal_Pop_Trans_Amount__c;
}
declare module "@salesforce/schema/Contact.Mailchimp_Rating__c" {
  const Mailchimp_Rating__c:string;
  export default Mailchimp_Rating__c;
}
declare module "@salesforce/schema/Contact.DeathDate__c" {
  const DeathDate__c:any;
  export default DeathDate__c;
}
declare module "@salesforce/schema/Contact.Has_Valid_Email__c" {
  const Has_Valid_Email__c:boolean;
  export default Has_Valid_Email__c;
}
declare module "@salesforce/schema/Contact.Send_to_MarketingCloud__c" {
  const Send_to_MarketingCloud__c:boolean;
  export default Send_to_MarketingCloud__c;
}
declare module "@salesforce/schema/Contact.Direct_Deposit__c" {
  const Direct_Deposit__c:boolean;
  export default Direct_Deposit__c;
}
declare module "@salesforce/schema/Contact.Attrition_Prediction__c" {
  const Attrition_Prediction__c:number;
  export default Attrition_Prediction__c;
}
declare module "@salesforce/schema/Contact.Online_Banking_Active__c" {
  const Online_Banking_Active__c:boolean;
  export default Online_Banking_Active__c;
}
declare module "@salesforce/schema/Contact.Mobile_Banking_Active__c" {
  const Mobile_Banking_Active__c:boolean;
  export default Mobile_Banking_Active__c;
}
declare module "@salesforce/schema/Contact.New_Deposit_Journey_Start_Date__c" {
  const New_Deposit_Journey_Start_Date__c:any;
  export default New_Deposit_Journey_Start_Date__c;
}
declare module "@salesforce/schema/Contact.Deposit_Journey_Started_Recently__c" {
  const Deposit_Journey_Started_Recently__c:boolean;
  export default Deposit_Journey_Started_Recently__c;
}
declare module "@salesforce/schema/Contact.Active_Debit_Card__c" {
  const Active_Debit_Card__c:boolean;
  export default Active_Debit_Card__c;
}
declare module "@salesforce/schema/Contact.Active_ATM_Card__c" {
  const Active_ATM_Card__c:boolean;
  export default Active_ATM_Card__c;
}
declare module "@salesforce/schema/Contact.Card_Plastic_Type__c" {
  const Card_Plastic_Type__c:string;
  export default Card_Plastic_Type__c;
}
declare module "@salesforce/schema/Contact.Active_Card__c" {
  const Active_Card__c:boolean;
  export default Active_Card__c;
}
declare module "@salesforce/schema/Contact.Offer_Response_Date__c" {
  const Offer_Response_Date__c:any;
  export default Offer_Response_Date__c;
}
declare module "@salesforce/schema/Contact.Census_Tract_Number__c" {
  const Census_Tract_Number__c:string;
  export default Census_Tract_Number__c;
}
declare module "@salesforce/schema/Contact.Closed_All_Accounts__c" {
  const Closed_All_Accounts__c:boolean;
  export default Closed_All_Accounts__c;
}
declare module "@salesforce/schema/Contact.Had_Active_Account__c" {
  const Had_Active_Account__c:boolean;
  export default Had_Active_Account__c;
}
declare module "@salesforce/schema/Contact.Survey_Response_Rating__c" {
  const Survey_Response_Rating__c:number;
  export default Survey_Response_Rating__c;
}
declare module "@salesforce/schema/Contact.Survey_Response__r" {
  const Survey_Response__r:any;
  export default Survey_Response__r;
}
declare module "@salesforce/schema/Contact.Survey_Response__c" {
  const Survey_Response__c:any;
  export default Survey_Response__c;
}
declare module "@salesforce/schema/Contact.Needs_Phone_Call__c" {
  const Needs_Phone_Call__c:boolean;
  export default Needs_Phone_Call__c;
}
declare module "@salesforce/schema/Contact.Attrition_Test__c" {
  const Attrition_Test__c:number;
  export default Attrition_Test__c;
}
declare module "@salesforce/schema/Contact.Remote_Deposit_Capture_Active__c" {
  const Remote_Deposit_Capture_Active__c:boolean;
  export default Remote_Deposit_Capture_Active__c;
}
declare module "@salesforce/schema/Contact.Remote_Deposit_Amount__c" {
  const Remote_Deposit_Amount__c:number;
  export default Remote_Deposit_Amount__c;
}
declare module "@salesforce/schema/Contact.Remote_Deposit_Count__c" {
  const Remote_Deposit_Count__c:number;
  export default Remote_Deposit_Count__c;
}
declare module "@salesforce/schema/Contact.Add_Advocate_Referral_Code__c" {
  const Add_Advocate_Referral_Code__c:string;
  export default Add_Advocate_Referral_Code__c;
}
declare module "@salesforce/schema/Contact.Advocate_Level__r" {
  const Advocate_Level__r:any;
  export default Advocate_Level__r;
}
declare module "@salesforce/schema/Contact.Advocate_Level__c" {
  const Advocate_Level__c:any;
  export default Advocate_Level__c;
}
declare module "@salesforce/schema/Contact.Advocate_Referral_Code_Count__c" {
  const Advocate_Referral_Code_Count__c:number;
  export default Advocate_Referral_Code_Count__c;
}
declare module "@salesforce/schema/Contact.MTD_Total_Trans__c" {
  const MTD_Total_Trans__c:number;
  export default MTD_Total_Trans__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_ATM__c" {
  const MTD_Pers_Trans_ATM__c:number;
  export default MTD_Pers_Trans_ATM__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_Batch__c" {
  const MTD_Pers_Trans_Batch__c:number;
  export default MTD_Pers_Trans_Batch__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_Branch__c" {
  const MTD_Pers_Trans_Branch__c:number;
  export default MTD_Pers_Trans_Branch__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_WWW__c" {
  const MTD_Pers_Trans_WWW__c:number;
  export default MTD_Pers_Trans_WWW__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_VRU__c" {
  const MTD_Pers_Trans_VRU__c:number;
  export default MTD_Pers_Trans_VRU__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_POS__c" {
  const MTD_Pers_Trans_POS__c:number;
  export default MTD_Pers_Trans_POS__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_POS_Amt__c" {
  const MTD_Pers_Trans_POS_Amt__c:number;
  export default MTD_Pers_Trans_POS_Amt__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_XDEP__c" {
  const MTD_Pers_Trans_XDEP__c:number;
  export default MTD_Pers_Trans_XDEP__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_XDEP_Amt__c" {
  const MTD_Pers_Trans_XDEP_Amt__c:number;
  export default MTD_Pers_Trans_XDEP_Amt__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Funds_Trans__c" {
  const MTD_Pers_Funds_Trans__c:number;
  export default MTD_Pers_Funds_Trans__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Funds_Trans_Amt__c" {
  const MTD_Pers_Funds_Trans_Amt__c:number;
  export default MTD_Pers_Funds_Trans_Amt__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Pop_Trans__c" {
  const MTD_Pers_Pop_Trans__c:number;
  export default MTD_Pers_Pop_Trans__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Pop_Trans_Amt__c" {
  const MTD_Pers_Pop_Trans_Amt__c:number;
  export default MTD_Pers_Pop_Trans_Amt__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Remote_Dep_Count__c" {
  const MTD_Pers_Remote_Dep_Count__c:number;
  export default MTD_Pers_Remote_Dep_Count__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Remote_Dep_Amt__c" {
  const MTD_Pers_Remote_Dep_Amt__c:number;
  export default MTD_Pers_Remote_Dep_Amt__c;
}
declare module "@salesforce/schema/Contact.PM_Total_Trans__c" {
  const PM_Total_Trans__c:number;
  export default PM_Total_Trans__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_ATM__c" {
  const PM_Pers_Trans_ATM__c:number;
  export default PM_Pers_Trans_ATM__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_Batch__c" {
  const PM_Pers_Trans_Batch__c:number;
  export default PM_Pers_Trans_Batch__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_Branch__c" {
  const PM_Pers_Trans_Branch__c:number;
  export default PM_Pers_Trans_Branch__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_WWW__c" {
  const PM_Pers_Trans_WWW__c:number;
  export default PM_Pers_Trans_WWW__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_VRU__c" {
  const PM_Pers_Trans_VRU__c:number;
  export default PM_Pers_Trans_VRU__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_POS__c" {
  const PM_Pers_Trans_POS__c:number;
  export default PM_Pers_Trans_POS__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_POS_Amt__c" {
  const PM_Pers_Trans_POS_Amt__c:number;
  export default PM_Pers_Trans_POS_Amt__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_XDEP__c" {
  const PM_Pers_Trans_XDEP__c:number;
  export default PM_Pers_Trans_XDEP__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_XDEP_Amt__c" {
  const PM_Pers_Trans_XDEP_Amt__c:number;
  export default PM_Pers_Trans_XDEP_Amt__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Funds_Trans__c" {
  const PM_Pers_Funds_Trans__c:number;
  export default PM_Pers_Funds_Trans__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Funds_Trans_Amt__c" {
  const PM_Pers_Funds_Trans_Amt__c:number;
  export default PM_Pers_Funds_Trans_Amt__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Pop_Trans__c" {
  const PM_Pers_Pop_Trans__c:number;
  export default PM_Pers_Pop_Trans__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Pop_Trans_Amt__c" {
  const PM_Pers_Pop_Trans_Amt__c:number;
  export default PM_Pers_Pop_Trans_Amt__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Remote_Dep_Count__c" {
  const PM_Pers_Remote_Dep_Count__c:number;
  export default PM_Pers_Remote_Dep_Count__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Remote_Dep_Amt__c" {
  const PM_Pers_Remote_Dep_Amt__c:number;
  export default PM_Pers_Remote_Dep_Amt__c;
}
declare module "@salesforce/schema/Contact.Advocate_Level_Image__c" {
  const Advocate_Level_Image__c:string;
  export default Advocate_Level_Image__c;
}
declare module "@salesforce/schema/Contact.Advocate_Next_Level_Has_Reward__c" {
  const Advocate_Next_Level_Has_Reward__c:boolean;
  export default Advocate_Next_Level_Has_Reward__c;
}
declare module "@salesforce/schema/Contact.Reached_Next_Advocate_Level__c" {
  const Reached_Next_Advocate_Level__c:boolean;
  export default Reached_Next_Advocate_Level__c;
}
declare module "@salesforce/schema/Contact.Total_Advocate_Points__c" {
  const Total_Advocate_Points__c:number;
  export default Total_Advocate_Points__c;
}
declare module "@salesforce/schema/Contact.Advocate_Activity_Total_Points__c" {
  const Advocate_Activity_Total_Points__c:number;
  export default Advocate_Activity_Total_Points__c;
}
declare module "@salesforce/schema/Contact.Advocate_Badge_Total_Points__c" {
  const Advocate_Badge_Total_Points__c:number;
  export default Advocate_Badge_Total_Points__c;
}
declare module "@salesforce/schema/Contact.PPP_Loan_Applications__c" {
  const PPP_Loan_Applications__c:number;
  export default PPP_Loan_Applications__c;
}
declare module "@salesforce/schema/Contact.Award_Badges__c" {
  const Award_Badges__c:number;
  export default Award_Badges__c;
}
declare module "@salesforce/schema/Contact.Advocate_Anniversary_Awarded__c" {
  const Advocate_Anniversary_Awarded__c:number;
  export default Advocate_Anniversary_Awarded__c;
}
declare module "@salesforce/schema/Contact.Advocate_Deposit_Acct_Last_Awarded__c" {
  const Advocate_Deposit_Acct_Last_Awarded__c:number;
  export default Advocate_Deposit_Acct_Last_Awarded__c;
}
declare module "@salesforce/schema/Contact.Advocate_POS_Trans_Current_Mult__c" {
  const Advocate_POS_Trans_Current_Mult__c:number;
  export default Advocate_POS_Trans_Current_Mult__c;
}
declare module "@salesforce/schema/Contact.Advocate_POS_Trans_Last_Mult__c" {
  const Advocate_POS_Trans_Last_Mult__c:number;
  export default Advocate_POS_Trans_Last_Mult__c;
}
declare module "@salesforce/schema/Contact.Direct_Dep__c" {
  const Direct_Dep__c:boolean;
  export default Direct_Dep__c;
}
declare module "@salesforce/schema/Contact.Funds_Transfer__c" {
  const Funds_Transfer__c:boolean;
  export default Funds_Transfer__c;
}
declare module "@salesforce/schema/Contact.Mobile_Remote_Deposit__c" {
  const Mobile_Remote_Deposit__c:boolean;
  export default Mobile_Remote_Deposit__c;
}
declare module "@salesforce/schema/Contact.PopMoney__c" {
  const PopMoney__c:boolean;
  export default PopMoney__c;
}
declare module "@salesforce/schema/Contact.Referred_by_Referral_Code__r" {
  const Referred_by_Referral_Code__r:any;
  export default Referred_by_Referral_Code__r;
}
declare module "@salesforce/schema/Contact.Referred_by_Referral_Code__c" {
  const Referred_by_Referral_Code__c:any;
  export default Referred_by_Referral_Code__c;
}
declare module "@salesforce/schema/Contact.Advocate_Referral_Code_Rewards__c" {
  const Advocate_Referral_Code_Rewards__c:number;
  export default Advocate_Referral_Code_Rewards__c;
}
declare module "@salesforce/schema/Contact.Rewards_Achieved_Exceeded__c" {
  const Rewards_Achieved_Exceeded__c:boolean;
  export default Rewards_Achieved_Exceeded__c;
}
declare module "@salesforce/schema/Contact.Rewards_Achieved__c" {
  const Rewards_Achieved__c:number;
  export default Rewards_Achieved__c;
}
declare module "@salesforce/schema/Contact.Advocate_Move_Money_I__c" {
  const Advocate_Move_Money_I__c:number;
  export default Advocate_Move_Money_I__c;
}
declare module "@salesforce/schema/Contact.Advocate_Move_Money_I_Missing__c" {
  const Advocate_Move_Money_I_Missing__c:boolean;
  export default Advocate_Move_Money_I_Missing__c;
}
declare module "@salesforce/schema/Contact.geopointe__Geocode__r" {
  const geopointe__Geocode__r:any;
  export default geopointe__Geocode__r;
}
declare module "@salesforce/schema/Contact.geopointe__Geocode__c" {
  const geopointe__Geocode__c:any;
  export default geopointe__Geocode__c;
}
declare module "@salesforce/schema/Contact.Rewards_Achieved_Value__c" {
  const Rewards_Achieved_Value__c:number;
  export default Rewards_Achieved_Value__c;
}
declare module "@salesforce/schema/Contact.Total_Activities_Achieved__c" {
  const Total_Activities_Achieved__c:number;
  export default Total_Activities_Achieved__c;
}
declare module "@salesforce/schema/Contact.DeDuplication_Flag__c" {
  const DeDuplication_Flag__c:boolean;
  export default DeDuplication_Flag__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Adverse_Alert__c" {
  const creditchecker__Adverse_Alert__c:number;
  export default creditchecker__Adverse_Alert__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Average_Score__c" {
  const creditchecker__Average_Score__c:number;
  export default creditchecker__Average_Score__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Bankruptcy_Count__c" {
  const creditchecker__Bankruptcy_Count__c:number;
  export default creditchecker__Bankruptcy_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Charge_Offs_Count__c" {
  const creditchecker__Charge_Offs_Count__c:number;
  export default creditchecker__Charge_Offs_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Collections_Count__c" {
  const creditchecker__Collections_Count__c:number;
  export default creditchecker__Collections_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Current_Adverse_Count__c" {
  const creditchecker__Current_Adverse_Count__c:number;
  export default creditchecker__Current_Adverse_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Date_Completed__c" {
  const creditchecker__Date_Completed__c:any;
  export default creditchecker__Date_Completed__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Debt_High_Credit__c" {
  const creditchecker__Debt_High_Credit__c:number;
  export default creditchecker__Debt_High_Credit__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Derogatory_Count__c" {
  const creditchecker__Derogatory_Count__c:number;
  export default creditchecker__Derogatory_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Disputes_Count__c" {
  const creditchecker__Disputes_Count__c:number;
  export default creditchecker__Disputes_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Most_Recent_Late__c" {
  const creditchecker__Most_Recent_Late__c:string;
  export default creditchecker__Most_Recent_Late__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Oldest_Date__c" {
  const creditchecker__Oldest_Date__c:string;
  export default creditchecker__Oldest_Date__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Previous_Adverse_Count__c" {
  const creditchecker__Previous_Adverse_Count__c:number;
  export default creditchecker__Previous_Adverse_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Public_Record_Count__c" {
  const creditchecker__Public_Record_Count__c:number;
  export default creditchecker__Public_Record_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Revolving_Credit_Utilization__c" {
  const creditchecker__Revolving_Credit_Utilization__c:number;
  export default creditchecker__Revolving_Credit_Utilization__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Secured_Debt__c" {
  const creditchecker__Secured_Debt__c:number;
  export default creditchecker__Secured_Debt__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Total_High_Credit__c" {
  const creditchecker__Total_High_Credit__c:number;
  export default creditchecker__Total_High_Credit__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Total_Inquiry_Count__c" {
  const creditchecker__Total_Inquiry_Count__c:number;
  export default creditchecker__Total_Inquiry_Count__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Total_Liability_Balance__c" {
  const creditchecker__Total_Liability_Balance__c:number;
  export default creditchecker__Total_Liability_Balance__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Total_Liability_PastDue__c" {
  const creditchecker__Total_Liability_PastDue__c:number;
  export default creditchecker__Total_Liability_PastDue__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Total_Liability_Payment__c" {
  const creditchecker__Total_Liability_Payment__c:number;
  export default creditchecker__Total_Liability_Payment__c;
}
declare module "@salesforce/schema/Contact.creditchecker__Unsecured_Debt__c" {
  const creditchecker__Unsecured_Debt__c:number;
  export default creditchecker__Unsecured_Debt__c;
}
declare module "@salesforce/schema/Contact.Activation_Score__c" {
  const Activation_Score__c:number;
  export default Activation_Score__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_PDEP__c" {
  const Personal_Trans_PDEP__c:number;
  export default Personal_Trans_PDEP__c;
}
declare module "@salesforce/schema/Contact.Personal_Trans_PDEP_Amount__c" {
  const Personal_Trans_PDEP_Amount__c:number;
  export default Personal_Trans_PDEP_Amount__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_PDEP__c" {
  const MTD_Pers_Trans_PDEP__c:number;
  export default MTD_Pers_Trans_PDEP__c;
}
declare module "@salesforce/schema/Contact.MTD_Pers_Trans_PDEP_Amt__c" {
  const MTD_Pers_Trans_PDEP_Amt__c:number;
  export default MTD_Pers_Trans_PDEP_Amt__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_PDEP__c" {
  const PM_Pers_Trans_PDEP__c:number;
  export default PM_Pers_Trans_PDEP__c;
}
declare module "@salesforce/schema/Contact.PM_Pers_Trans_PDEP_Amt__c" {
  const PM_Pers_Trans_PDEP_Amt__c:number;
  export default PM_Pers_Trans_PDEP_Amt__c;
}
declare module "@salesforce/schema/Contact.GUUID__c" {
  const GUUID__c:string;
  export default GUUID__c;
}
declare module "@salesforce/schema/Contact.Attrition_Status__c" {
  const Attrition_Status__c:string;
  export default Attrition_Status__c;
}
declare module "@salesforce/schema/Contact.Likelyhood_to_Attrition__c" {
  const Likelyhood_to_Attrition__c:number;
  export default Likelyhood_to_Attrition__c;
}
declare module "@salesforce/schema/Contact.Attrition_Status_1st_Year__c" {
  const Attrition_Status_1st_Year__c:string;
  export default Attrition_Status_1st_Year__c;
}
declare module "@salesforce/schema/Contact.Likelyhood_to_Attri_1st_Year__c" {
  const Likelyhood_to_Attri_1st_Year__c:number;
  export default Likelyhood_to_Attri_1st_Year__c;
}
declare module "@salesforce/schema/Contact.SMS_Number__c" {
  const SMS_Number__c:string;
  export default SMS_Number__c;
}
declare module "@salesforce/schema/Contact.SMS_Phone__c" {
  const SMS_Phone__c:string;
  export default SMS_Phone__c;
}
