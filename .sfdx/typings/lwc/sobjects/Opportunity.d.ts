declare module "@salesforce/schema/Opportunity.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Opportunity.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Opportunity.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Opportunity.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Opportunity.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Opportunity.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Opportunity.StageName" {
  const StageName:string;
  export default StageName;
}
declare module "@salesforce/schema/Opportunity.Amount" {
  const Amount:number;
  export default Amount;
}
declare module "@salesforce/schema/Opportunity.Probability" {
  const Probability:number;
  export default Probability;
}
declare module "@salesforce/schema/Opportunity.ExpectedRevenue" {
  const ExpectedRevenue:number;
  export default ExpectedRevenue;
}
declare module "@salesforce/schema/Opportunity.TotalOpportunityQuantity" {
  const TotalOpportunityQuantity:number;
  export default TotalOpportunityQuantity;
}
declare module "@salesforce/schema/Opportunity.CloseDate" {
  const CloseDate:any;
  export default CloseDate;
}
declare module "@salesforce/schema/Opportunity.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Opportunity.NextStep" {
  const NextStep:string;
  export default NextStep;
}
declare module "@salesforce/schema/Opportunity.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Opportunity.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Opportunity.IsWon" {
  const IsWon:boolean;
  export default IsWon;
}
declare module "@salesforce/schema/Opportunity.ForecastCategory" {
  const ForecastCategory:string;
  export default ForecastCategory;
}
declare module "@salesforce/schema/Opportunity.ForecastCategoryName" {
  const ForecastCategoryName:string;
  export default ForecastCategoryName;
}
declare module "@salesforce/schema/Opportunity.HasOpportunityLineItem" {
  const HasOpportunityLineItem:boolean;
  export default HasOpportunityLineItem;
}
declare module "@salesforce/schema/Opportunity.Pricebook2" {
  const Pricebook2:any;
  export default Pricebook2;
}
declare module "@salesforce/schema/Opportunity.Pricebook2Id" {
  const Pricebook2Id:any;
  export default Pricebook2Id;
}
declare module "@salesforce/schema/Opportunity.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Opportunity.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Opportunity.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Opportunity.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Opportunity.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Opportunity.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Opportunity.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Opportunity.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Opportunity.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Opportunity.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Opportunity.FiscalQuarter" {
  const FiscalQuarter:number;
  export default FiscalQuarter;
}
declare module "@salesforce/schema/Opportunity.FiscalYear" {
  const FiscalYear:number;
  export default FiscalYear;
}
declare module "@salesforce/schema/Opportunity.Fiscal" {
  const Fiscal:string;
  export default Fiscal;
}
declare module "@salesforce/schema/Opportunity.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/Opportunity.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/Opportunity.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Opportunity.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Opportunity.HasOpenActivity" {
  const HasOpenActivity:boolean;
  export default HasOpenActivity;
}
declare module "@salesforce/schema/Opportunity.HasOverdueTask" {
  const HasOverdueTask:boolean;
  export default HasOverdueTask;
}
declare module "@salesforce/schema/Opportunity.Rate__c" {
  const Rate__c:number;
  export default Rate__c;
}
declare module "@salesforce/schema/Opportunity.TypeofLoan__c" {
  const TypeofLoan__c:string;
  export default TypeofLoan__c;
}
declare module "@salesforce/schema/Opportunity.ProposedCollateral__c" {
  const ProposedCollateral__c:string;
  export default ProposedCollateral__c;
}
declare module "@salesforce/schema/Opportunity.ProductInterest__c" {
  const ProductInterest__c:string;
  export default ProductInterest__c;
}
declare module "@salesforce/schema/Opportunity.PurposeofLoan__c" {
  const PurposeofLoan__c:string;
  export default PurposeofLoan__c;
}
declare module "@salesforce/schema/Opportunity.BankAccountType__c" {
  const BankAccountType__c:string;
  export default BankAccountType__c;
}
declare module "@salesforce/schema/Opportunity.InvestmentType__c" {
  const InvestmentType__c:string;
  export default InvestmentType__c;
}
declare module "@salesforce/schema/Opportunity.Term__c" {
  const Term__c:string;
  export default Term__c;
}
declare module "@salesforce/schema/Opportunity.UploadID__c" {
  const UploadID__c:string;
  export default UploadID__c;
}
declare module "@salesforce/schema/Opportunity.geopointe__Geocode__r" {
  const geopointe__Geocode__r:any;
  export default geopointe__Geocode__r;
}
declare module "@salesforce/schema/Opportunity.geopointe__Geocode__c" {
  const geopointe__Geocode__c:any;
  export default geopointe__Geocode__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Adverse_Alert__c" {
  const creditchecker__Adverse_Alert__c:number;
  export default creditchecker__Adverse_Alert__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Average_Score__c" {
  const creditchecker__Average_Score__c:number;
  export default creditchecker__Average_Score__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Bankruptcy_Count__c" {
  const creditchecker__Bankruptcy_Count__c:number;
  export default creditchecker__Bankruptcy_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Charge_Offs_Count__c" {
  const creditchecker__Charge_Offs_Count__c:number;
  export default creditchecker__Charge_Offs_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Collections_Count__c" {
  const creditchecker__Collections_Count__c:number;
  export default creditchecker__Collections_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Current_Adverse_Count__c" {
  const creditchecker__Current_Adverse_Count__c:number;
  export default creditchecker__Current_Adverse_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Date_Completed__c" {
  const creditchecker__Date_Completed__c:any;
  export default creditchecker__Date_Completed__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Debt_High_Credit__c" {
  const creditchecker__Debt_High_Credit__c:number;
  export default creditchecker__Debt_High_Credit__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__DeliveryInstallationStatus__c" {
  const creditchecker__DeliveryInstallationStatus__c:string;
  export default creditchecker__DeliveryInstallationStatus__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Derogatory_Count__c" {
  const creditchecker__Derogatory_Count__c:number;
  export default creditchecker__Derogatory_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Disputes_Count__c" {
  const creditchecker__Disputes_Count__c:number;
  export default creditchecker__Disputes_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Most_Recent_Late__c" {
  const creditchecker__Most_Recent_Late__c:string;
  export default creditchecker__Most_Recent_Late__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Oldest_Date__c" {
  const creditchecker__Oldest_Date__c:string;
  export default creditchecker__Oldest_Date__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Previous_Adverse_Count__c" {
  const creditchecker__Previous_Adverse_Count__c:number;
  export default creditchecker__Previous_Adverse_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Public_Record_Count__c" {
  const creditchecker__Public_Record_Count__c:number;
  export default creditchecker__Public_Record_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Revolving_Credit_Utilization__c" {
  const creditchecker__Revolving_Credit_Utilization__c:number;
  export default creditchecker__Revolving_Credit_Utilization__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Secured_Debt__c" {
  const creditchecker__Secured_Debt__c:number;
  export default creditchecker__Secured_Debt__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Total_High_Credit__c" {
  const creditchecker__Total_High_Credit__c:number;
  export default creditchecker__Total_High_Credit__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Total_Inquiry_Count__c" {
  const creditchecker__Total_Inquiry_Count__c:number;
  export default creditchecker__Total_Inquiry_Count__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Total_Liability_Balance__c" {
  const creditchecker__Total_Liability_Balance__c:number;
  export default creditchecker__Total_Liability_Balance__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Total_Liability_PastDue__c" {
  const creditchecker__Total_Liability_PastDue__c:number;
  export default creditchecker__Total_Liability_PastDue__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Total_Liability_Payment__c" {
  const creditchecker__Total_Liability_Payment__c:number;
  export default creditchecker__Total_Liability_Payment__c;
}
declare module "@salesforce/schema/Opportunity.creditchecker__Unsecured_Debt__c" {
  const creditchecker__Unsecured_Debt__c:number;
  export default creditchecker__Unsecured_Debt__c;
}
