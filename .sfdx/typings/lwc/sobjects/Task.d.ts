declare module "@salesforce/schema/Task.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Task.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Task.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Task.Who" {
  const Who:any;
  export default Who;
}
declare module "@salesforce/schema/Task.WhoId" {
  const WhoId:any;
  export default WhoId;
}
declare module "@salesforce/schema/Task.What" {
  const What:any;
  export default What;
}
declare module "@salesforce/schema/Task.WhatId" {
  const WhatId:any;
  export default WhatId;
}
declare module "@salesforce/schema/Task.WhoCount" {
  const WhoCount:number;
  export default WhoCount;
}
declare module "@salesforce/schema/Task.WhatCount" {
  const WhatCount:number;
  export default WhatCount;
}
declare module "@salesforce/schema/Task.Subject" {
  const Subject:string;
  export default Subject;
}
declare module "@salesforce/schema/Task.ActivityDate" {
  const ActivityDate:any;
  export default ActivityDate;
}
declare module "@salesforce/schema/Task.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/Task.Priority" {
  const Priority:string;
  export default Priority;
}
declare module "@salesforce/schema/Task.IsHighPriority" {
  const IsHighPriority:boolean;
  export default IsHighPriority;
}
declare module "@salesforce/schema/Task.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Task.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Task.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Task.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Task.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Task.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Task.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Task.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Task.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Task.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Task.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Task.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Task.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Task.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Task.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Task.IsArchived" {
  const IsArchived:boolean;
  export default IsArchived;
}
declare module "@salesforce/schema/Task.IsVisibleInSelfService" {
  const IsVisibleInSelfService:boolean;
  export default IsVisibleInSelfService;
}
declare module "@salesforce/schema/Task.CallDurationInSeconds" {
  const CallDurationInSeconds:number;
  export default CallDurationInSeconds;
}
declare module "@salesforce/schema/Task.CallType" {
  const CallType:string;
  export default CallType;
}
declare module "@salesforce/schema/Task.CallDisposition" {
  const CallDisposition:string;
  export default CallDisposition;
}
declare module "@salesforce/schema/Task.CallObject" {
  const CallObject:string;
  export default CallObject;
}
declare module "@salesforce/schema/Task.ReminderDateTime" {
  const ReminderDateTime:any;
  export default ReminderDateTime;
}
declare module "@salesforce/schema/Task.IsReminderSet" {
  const IsReminderSet:boolean;
  export default IsReminderSet;
}
declare module "@salesforce/schema/Task.RecurrenceActivity" {
  const RecurrenceActivity:any;
  export default RecurrenceActivity;
}
declare module "@salesforce/schema/Task.RecurrenceActivityId" {
  const RecurrenceActivityId:any;
  export default RecurrenceActivityId;
}
declare module "@salesforce/schema/Task.IsRecurrence" {
  const IsRecurrence:boolean;
  export default IsRecurrence;
}
declare module "@salesforce/schema/Task.RecurrenceStartDateOnly" {
  const RecurrenceStartDateOnly:any;
  export default RecurrenceStartDateOnly;
}
declare module "@salesforce/schema/Task.RecurrenceEndDateOnly" {
  const RecurrenceEndDateOnly:any;
  export default RecurrenceEndDateOnly;
}
declare module "@salesforce/schema/Task.RecurrenceTimeZoneSidKey" {
  const RecurrenceTimeZoneSidKey:string;
  export default RecurrenceTimeZoneSidKey;
}
declare module "@salesforce/schema/Task.RecurrenceType" {
  const RecurrenceType:string;
  export default RecurrenceType;
}
declare module "@salesforce/schema/Task.RecurrenceInterval" {
  const RecurrenceInterval:number;
  export default RecurrenceInterval;
}
declare module "@salesforce/schema/Task.RecurrenceDayOfWeekMask" {
  const RecurrenceDayOfWeekMask:number;
  export default RecurrenceDayOfWeekMask;
}
declare module "@salesforce/schema/Task.RecurrenceDayOfMonth" {
  const RecurrenceDayOfMonth:number;
  export default RecurrenceDayOfMonth;
}
declare module "@salesforce/schema/Task.RecurrenceInstance" {
  const RecurrenceInstance:string;
  export default RecurrenceInstance;
}
declare module "@salesforce/schema/Task.RecurrenceMonthOfYear" {
  const RecurrenceMonthOfYear:string;
  export default RecurrenceMonthOfYear;
}
declare module "@salesforce/schema/Task.RecurrenceRegeneratedType" {
  const RecurrenceRegeneratedType:string;
  export default RecurrenceRegeneratedType;
}
declare module "@salesforce/schema/Task.TaskSubtype" {
  const TaskSubtype:string;
  export default TaskSubtype;
}
declare module "@salesforce/schema/Task.CompletedDateTime" {
  const CompletedDateTime:any;
  export default CompletedDateTime;
}
declare module "@salesforce/schema/Task.Call_Reason__c" {
  const Call_Reason__c:string;
  export default Call_Reason__c;
}
declare module "@salesforce/schema/Task.Interested_in_New_Account__c" {
  const Interested_in_New_Account__c:string;
  export default Interested_in_New_Account__c;
}
declare module "@salesforce/schema/Task.New_Account_Balance__c" {
  const New_Account_Balance__c:number;
  export default New_Account_Balance__c;
}
declare module "@salesforce/schema/Task.Reason_Account_Closed__c" {
  const Reason_Account_Closed__c:string;
  export default Reason_Account_Closed__c;
}
declare module "@salesforce/schema/Task.First_Name__c" {
  const First_Name__c:string;
  export default First_Name__c;
}
declare module "@salesforce/schema/Task.Generate_Batch_Letter__c" {
  const Generate_Batch_Letter__c:boolean;
  export default Generate_Batch_Letter__c;
}
declare module "@salesforce/schema/Task.Last_Name__c" {
  const Last_Name__c:string;
  export default Last_Name__c;
}
declare module "@salesforce/schema/Task.Mailing_Address__c" {
  const Mailing_Address__c:string;
  export default Mailing_Address__c;
}
declare module "@salesforce/schema/Task.Mailing_City__c" {
  const Mailing_City__c:string;
  export default Mailing_City__c;
}
declare module "@salesforce/schema/Task.Mailing_State__c" {
  const Mailing_State__c:string;
  export default Mailing_State__c;
}
declare module "@salesforce/schema/Task.Mailing_Zip__c" {
  const Mailing_Zip__c:string;
  export default Mailing_Zip__c;
}
declare module "@salesforce/schema/Task.Send_Customer_Communication__c" {
  const Send_Customer_Communication__c:boolean;
  export default Send_Customer_Communication__c;
}
declare module "@salesforce/schema/Task.Template_ID__c" {
  const Template_ID__c:number;
  export default Template_ID__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Business__c" {
  const Financial_Priority_Business__c:boolean;
  export default Financial_Priority_Business__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Credit__c" {
  const Financial_Priority_Credit__c:boolean;
  export default Financial_Priority_Credit__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Home_Finance__c" {
  const Financial_Priority_Home_Finance__c:boolean;
  export default Financial_Priority_Home_Finance__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Literacy__c" {
  const Financial_Priority_Literacy__c:boolean;
  export default Financial_Priority_Literacy__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Online_Banking__c" {
  const Financial_Priority_Online_Banking__c:boolean;
  export default Financial_Priority_Online_Banking__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Retirement__c" {
  const Financial_Priority_Retirement__c:boolean;
  export default Financial_Priority_Retirement__c;
}
declare module "@salesforce/schema/Task.Financial_Priority_Savings__c" {
  const Financial_Priority_Savings__c:boolean;
  export default Financial_Priority_Savings__c;
}
declare module "@salesforce/schema/Task.Form_Email__c" {
  const Form_Email__c:string;
  export default Form_Email__c;
}
declare module "@salesforce/schema/Task.Form_Phone__c" {
  const Form_Phone__c:string;
  export default Form_Phone__c;
}
declare module "@salesforce/schema/Task.Are_you_a_Non_Profit__c" {
  const Are_you_a_Non_Profit__c:boolean;
  export default Are_you_a_Non_Profit__c;
}
declare module "@salesforce/schema/Task.Are_you_a_OneUnited_Bank_Customer__c" {
  const Are_you_a_OneUnited_Bank_Customer__c:boolean;
  export default Are_you_a_OneUnited_Bank_Customer__c;
}
declare module "@salesforce/schema/Task.Certificate_of_Liability_Insurance__c" {
  const Certificate_of_Liability_Insurance__c:boolean;
  export default Certificate_of_Liability_Insurance__c;
}
declare module "@salesforce/schema/Task.Food_being_Served__c" {
  const Food_being_Served__c:boolean;
  export default Food_being_Served__c;
}
declare module "@salesforce/schema/Task.Mission_Statement__c" {
  const Mission_Statement__c:string;
  export default Mission_Statement__c;
}
declare module "@salesforce/schema/Task.Name__c" {
  const Name__c:string;
  export default Name__c;
}
declare module "@salesforce/schema/Task.Number_of_People_Expected__c" {
  const Number_of_People_Expected__c:number;
  export default Number_of_People_Expected__c;
}
declare module "@salesforce/schema/Task.Organization_Tax_ID__c" {
  const Organization_Tax_ID__c:string;
  export default Organization_Tax_ID__c;
}
declare module "@salesforce/schema/Task.Other_Options_for_Dates__c" {
  const Other_Options_for_Dates__c:string;
  export default Other_Options_for_Dates__c;
}
declare module "@salesforce/schema/Task.Purpose_of_Meeting__c" {
  const Purpose_of_Meeting__c:string;
  export default Purpose_of_Meeting__c;
}
declare module "@salesforce/schema/Task.Refundable_Cleaning_Deposit__c" {
  const Refundable_Cleaning_Deposit__c:boolean;
  export default Refundable_Cleaning_Deposit__c;
}
declare module "@salesforce/schema/Task.Repeat_Meeting__c" {
  const Repeat_Meeting__c:string;
  export default Repeat_Meeting__c;
}
declare module "@salesforce/schema/Task.Website__c" {
  const Website__c:string;
  export default Website__c;
}
declare module "@salesforce/schema/Task.Owner_in_Department__c" {
  const Owner_in_Department__c:boolean;
  export default Owner_in_Department__c;
}
declare module "@salesforce/schema/Task.Owner_is_Direct_Report__c" {
  const Owner_is_Direct_Report__c:boolean;
  export default Owner_is_Direct_Report__c;
}
declare module "@salesforce/schema/Task.Received_Thank_you_Gift__c" {
  const Received_Thank_you_Gift__c:boolean;
  export default Received_Thank_you_Gift__c;
}
declare module "@salesforce/schema/Task.Took_Picture__c" {
  const Took_Picture__c:boolean;
  export default Took_Picture__c;
}
declare module "@salesforce/schema/Task.Days_Since_Created__c" {
  const Days_Since_Created__c:number;
  export default Days_Since_Created__c;
}
declare module "@salesforce/schema/Task.Owner_is_Branch_Manager__c" {
  const Owner_is_Branch_Manager__c:boolean;
  export default Owner_is_Branch_Manager__c;
}
declare module "@salesforce/schema/Task.SFDC_Test__c" {
  const SFDC_Test__c:boolean;
  export default SFDC_Test__c;
}
declare module "@salesforce/schema/Task.Email_Date_and_Time__c" {
  const Email_Date_and_Time__c:any;
  export default Email_Date_and_Time__c;
}
declare module "@salesforce/schema/Task.Scheduled_Email_Template__c" {
  const Scheduled_Email_Template__c:string;
  export default Scheduled_Email_Template__c;
}
declare module "@salesforce/schema/Task.Assigned_Too_Branch__c" {
  const Assigned_Too_Branch__c:string;
  export default Assigned_Too_Branch__c;
}
declare module "@salesforce/schema/Task.Campaign_Member__r" {
  const Campaign_Member__r:any;
  export default Campaign_Member__r;
}
declare module "@salesforce/schema/Task.Campaign_Member__c" {
  const Campaign_Member__c:any;
  export default Campaign_Member__c;
}
declare module "@salesforce/schema/Task.Offer_Response__r" {
  const Offer_Response__r:any;
  export default Offer_Response__r;
}
declare module "@salesforce/schema/Task.Offer_Response__c" {
  const Offer_Response__c:any;
  export default Offer_Response__c;
}
declare module "@salesforce/schema/Task.Assigned_To_Department__c" {
  const Assigned_To_Department__c:string;
  export default Assigned_To_Department__c;
}
declare module "@salesforce/schema/Task.Recorded_Call__c" {
  const Recorded_Call__c:string;
  export default Recorded_Call__c;
}
declare module "@salesforce/schema/Task.Relationship__r" {
  const Relationship__r:any;
  export default Relationship__r;
}
declare module "@salesforce/schema/Task.Relationship__c" {
  const Relationship__c:any;
  export default Relationship__c;
}
declare module "@salesforce/schema/Task.Survey_Response__r" {
  const Survey_Response__r:any;
  export default Survey_Response__r;
}
declare module "@salesforce/schema/Task.Survey_Response__c" {
  const Survey_Response__c:any;
  export default Survey_Response__c;
}
declare module "@salesforce/schema/Task.Product_Interest__c" {
  const Product_Interest__c:string;
  export default Product_Interest__c;
}
declare module "@salesforce/schema/Task.UTM_Campaign__c" {
  const UTM_Campaign__c:string;
  export default UTM_Campaign__c;
}
declare module "@salesforce/schema/Task.UTM_Content__c" {
  const UTM_Content__c:string;
  export default UTM_Content__c;
}
declare module "@salesforce/schema/Task.UTM_Medium__c" {
  const UTM_Medium__c:string;
  export default UTM_Medium__c;
}
declare module "@salesforce/schema/Task.UTM_Source__c" {
  const UTM_Source__c:string;
  export default UTM_Source__c;
}
declare module "@salesforce/schema/Task.UTM_Term__c" {
  const UTM_Term__c:string;
  export default UTM_Term__c;
}
declare module "@salesforce/schema/Task.UTM_VisitorID__c" {
  const UTM_VisitorID__c:string;
  export default UTM_VisitorID__c;
}
declare module "@salesforce/schema/Task.Total_Contacts__c" {
  const Total_Contacts__c:number;
  export default Total_Contacts__c;
}
declare module "@salesforce/schema/Task.Sales_Call_List__c" {
  const Sales_Call_List__c:string;
  export default Sales_Call_List__c;
}
declare module "@salesforce/schema/Task.Masked_Card_Number__c" {
  const Masked_Card_Number__c:string;
  export default Masked_Card_Number__c;
}
declare module "@salesforce/schema/Task.Wallet_Type__c" {
  const Wallet_Type__c:string;
  export default Wallet_Type__c;
}
