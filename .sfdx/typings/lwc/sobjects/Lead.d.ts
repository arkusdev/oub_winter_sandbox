declare module "@salesforce/schema/Lead.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Lead.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Lead.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Lead.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Lead.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Lead.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Lead.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Lead.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Lead.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Lead.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Lead.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/Lead.Company" {
  const Company:string;
  export default Company;
}
declare module "@salesforce/schema/Lead.Street" {
  const Street:string;
  export default Street;
}
declare module "@salesforce/schema/Lead.City" {
  const City:string;
  export default City;
}
declare module "@salesforce/schema/Lead.State" {
  const State:string;
  export default State;
}
declare module "@salesforce/schema/Lead.PostalCode" {
  const PostalCode:string;
  export default PostalCode;
}
declare module "@salesforce/schema/Lead.Country" {
  const Country:string;
  export default Country;
}
declare module "@salesforce/schema/Lead.Latitude" {
  const Latitude:number;
  export default Latitude;
}
declare module "@salesforce/schema/Lead.Longitude" {
  const Longitude:number;
  export default Longitude;
}
declare module "@salesforce/schema/Lead.GeocodeAccuracy" {
  const GeocodeAccuracy:string;
  export default GeocodeAccuracy;
}
declare module "@salesforce/schema/Lead.Address" {
  const Address:any;
  export default Address;
}
declare module "@salesforce/schema/Lead.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Lead.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/Lead.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Lead.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/Lead.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Lead.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Lead.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Lead.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Lead.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/Lead.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Lead.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Lead.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Lead.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Lead.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Lead.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Lead.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/Lead.IsConverted" {
  const IsConverted:boolean;
  export default IsConverted;
}
declare module "@salesforce/schema/Lead.ConvertedDate" {
  const ConvertedDate:any;
  export default ConvertedDate;
}
declare module "@salesforce/schema/Lead.ConvertedAccount" {
  const ConvertedAccount:any;
  export default ConvertedAccount;
}
declare module "@salesforce/schema/Lead.ConvertedAccountId" {
  const ConvertedAccountId:any;
  export default ConvertedAccountId;
}
declare module "@salesforce/schema/Lead.ConvertedContact" {
  const ConvertedContact:any;
  export default ConvertedContact;
}
declare module "@salesforce/schema/Lead.ConvertedContactId" {
  const ConvertedContactId:any;
  export default ConvertedContactId;
}
declare module "@salesforce/schema/Lead.ConvertedOpportunity" {
  const ConvertedOpportunity:any;
  export default ConvertedOpportunity;
}
declare module "@salesforce/schema/Lead.ConvertedOpportunityId" {
  const ConvertedOpportunityId:any;
  export default ConvertedOpportunityId;
}
declare module "@salesforce/schema/Lead.IsUnreadByOwner" {
  const IsUnreadByOwner:boolean;
  export default IsUnreadByOwner;
}
declare module "@salesforce/schema/Lead.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Lead.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Lead.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Lead.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Lead.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Lead.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Lead.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Lead.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Lead.DoNotCall" {
  const DoNotCall:boolean;
  export default DoNotCall;
}
declare module "@salesforce/schema/Lead.HasOptedOutOfFax" {
  const HasOptedOutOfFax:boolean;
  export default HasOptedOutOfFax;
}
declare module "@salesforce/schema/Lead.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Lead.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Lead.LastTransferDate" {
  const LastTransferDate:any;
  export default LastTransferDate;
}
declare module "@salesforce/schema/Lead.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Lead.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/Lead.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/Lead.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/Lead.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/Lead.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/Lead.BusinessStructure__c" {
  const BusinessStructure__c:string;
  export default BusinessStructure__c;
}
declare module "@salesforce/schema/Lead.DateBusinessStarted__c" {
  const DateBusinessStarted__c:string;
  export default DateBusinessStarted__c;
}
declare module "@salesforce/schema/Lead.BankAccountType__c" {
  const BankAccountType__c:string;
  export default BankAccountType__c;
}
declare module "@salesforce/schema/Lead.BusinessStage__c" {
  const BusinessStage__c:string;
  export default BusinessStage__c;
}
declare module "@salesforce/schema/Lead.PrimaryBankName__c" {
  const PrimaryBankName__c:string;
  export default PrimaryBankName__c;
}
declare module "@salesforce/schema/Lead.GrossAnnualSales__c" {
  const GrossAnnualSales__c:number;
  export default GrossAnnualSales__c;
}
declare module "@salesforce/schema/Lead.BusinessType__c" {
  const BusinessType__c:string;
  export default BusinessType__c;
}
declare module "@salesforce/schema/Lead.ProductInterest__c" {
  const ProductInterest__c:string;
  export default ProductInterest__c;
}
declare module "@salesforce/schema/Lead.UploadID__c" {
  const UploadID__c:string;
  export default UploadID__c;
}
declare module "@salesforce/schema/Lead.Tax_ID_Number__c" {
  const Tax_ID_Number__c:number;
  export default Tax_ID_Number__c;
}
declare module "@salesforce/schema/Lead.Work_Email__c" {
  const Work_Email__c:string;
  export default Work_Email__c;
}
declare module "@salesforce/schema/Lead.OneUnited_Newsletter__c" {
  const OneUnited_Newsletter__c:string;
  export default OneUnited_Newsletter__c;
}
declare module "@salesforce/schema/Lead.BestCallTime__c" {
  const BestCallTime__c:string;
  export default BestCallTime__c;
}
declare module "@salesforce/schema/Lead.News_List__c" {
  const News_List__c:boolean;
  export default News_List__c;
}
declare module "@salesforce/schema/Lead.Events_List__c" {
  const Events_List__c:boolean;
  export default Events_List__c;
}
declare module "@salesforce/schema/Lead.Newsletter__c" {
  const Newsletter__c:boolean;
  export default Newsletter__c;
}
declare module "@salesforce/schema/Lead.Promotion_Code__c" {
  const Promotion_Code__c:string;
  export default Promotion_Code__c;
}
declare module "@salesforce/schema/Lead.Bad_Email_Address__c" {
  const Bad_Email_Address__c:boolean;
  export default Bad_Email_Address__c;
}
declare module "@salesforce/schema/Lead.SFGA__Web_Source__c" {
  const SFGA__Web_Source__c:string;
  export default SFGA__Web_Source__c;
}
declare module "@salesforce/schema/Lead.SFGA__CorrelationID__c" {
  const SFGA__CorrelationID__c:string;
  export default SFGA__CorrelationID__c;
}
declare module "@salesforce/schema/Lead.SFGA__Correlation_Data__c" {
  const SFGA__Correlation_Data__c:string;
  export default SFGA__Correlation_Data__c;
}
declare module "@salesforce/schema/Lead.ProductSelection__c" {
  const ProductSelection__c:string;
  export default ProductSelection__c;
}
declare module "@salesforce/schema/Lead.Message__c" {
  const Message__c:string;
  export default Message__c;
}
declare module "@salesforce/schema/Lead.Do_you_have_a_ChexSystems_record__c" {
  const Do_you_have_a_ChexSystems_record__c:string;
  export default Do_you_have_a_ChexSystems_record__c;
}
declare module "@salesforce/schema/Lead.Are_you_a_United_States_Citizen__c" {
  const Are_you_a_United_States_Citizen__c:string;
  export default Are_you_a_United_States_Citizen__c;
}
declare module "@salesforce/schema/Lead.Referred_By__c" {
  const Referred_By__c:string;
  export default Referred_By__c;
}
declare module "@salesforce/schema/Lead.What_is_your_intended_opening_deposit__c" {
  const What_is_your_intended_opening_deposit__c:string;
  export default What_is_your_intended_opening_deposit__c;
}
declare module "@salesforce/schema/Lead.What_is_the_purpose_of_this_inquiry__c" {
  const What_is_the_purpose_of_this_inquiry__c:string;
  export default What_is_the_purpose_of_this_inquiry__c;
}
declare module "@salesforce/schema/Lead.Tax_ID__c" {
  const Tax_ID__c:string;
  export default Tax_ID__c;
}
declare module "@salesforce/schema/Lead.Organization__c" {
  const Organization__c:string;
  export default Organization__c;
}
declare module "@salesforce/schema/Lead.Website_Form__c" {
  const Website_Form__c:string;
  export default Website_Form__c;
}
declare module "@salesforce/schema/Lead.Evening_Phone__c" {
  const Evening_Phone__c:string;
  export default Evening_Phone__c;
}
declare module "@salesforce/schema/Lead.UTM_Campaign__c" {
  const UTM_Campaign__c:string;
  export default UTM_Campaign__c;
}
declare module "@salesforce/schema/Lead.UTM_Medium__c" {
  const UTM_Medium__c:string;
  export default UTM_Medium__c;
}
declare module "@salesforce/schema/Lead.UTM_Source__c" {
  const UTM_Source__c:string;
  export default UTM_Source__c;
}
declare module "@salesforce/schema/Lead.UTM_Content__c" {
  const UTM_Content__c:string;
  export default UTM_Content__c;
}
declare module "@salesforce/schema/Lead.UTM_Term__c" {
  const UTM_Term__c:string;
  export default UTM_Term__c;
}
declare module "@salesforce/schema/Lead.UTM_VisitorID__c" {
  const UTM_VisitorID__c:string;
  export default UTM_VisitorID__c;
}
declare module "@salesforce/schema/Lead.Created_Year__c" {
  const Created_Year__c:number;
  export default Created_Year__c;
}
declare module "@salesforce/schema/Lead.Loan_Referral_Date__c" {
  const Loan_Referral_Date__c:any;
  export default Loan_Referral_Date__c;
}
declare module "@salesforce/schema/Lead.MC_Salesforce_Type__c" {
  const MC_Salesforce_Type__c:string;
  export default MC_Salesforce_Type__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Business__c" {
  const Financial_Priority_Business__c:boolean;
  export default Financial_Priority_Business__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Credit__c" {
  const Financial_Priority_Credit__c:boolean;
  export default Financial_Priority_Credit__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Home_Finance__c" {
  const Financial_Priority_Home_Finance__c:boolean;
  export default Financial_Priority_Home_Finance__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Literacy__c" {
  const Financial_Priority_Literacy__c:boolean;
  export default Financial_Priority_Literacy__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Online_Banking__c" {
  const Financial_Priority_Online_Banking__c:boolean;
  export default Financial_Priority_Online_Banking__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Retirement__c" {
  const Financial_Priority_Retirement__c:boolean;
  export default Financial_Priority_Retirement__c;
}
declare module "@salesforce/schema/Lead.Financial_Priority_Savings__c" {
  const Financial_Priority_Savings__c:boolean;
  export default Financial_Priority_Savings__c;
}
declare module "@salesforce/schema/Lead.eContacts__Business_Card__c" {
  const eContacts__Business_Card__c:string;
  export default eContacts__Business_Card__c;
}
declare module "@salesforce/schema/Lead.eContacts__Created_By_eContacts__c" {
  const eContacts__Created_By_eContacts__c:boolean;
  export default eContacts__Created_By_eContacts__c;
}
declare module "@salesforce/schema/Lead.eContacts__Location_Text__c" {
  const eContacts__Location_Text__c:string;
  export default eContacts__Location_Text__c;
}
declare module "@salesforce/schema/Lead.eContacts__Location__Latitude__s" {
  const eContacts__Location__Latitude__s:number;
  export default eContacts__Location__Latitude__s;
}
declare module "@salesforce/schema/Lead.eContacts__Location__Longitude__s" {
  const eContacts__Location__Longitude__s:number;
  export default eContacts__Location__Longitude__s;
}
declare module "@salesforce/schema/Lead.eContacts__Location__c" {
  const eContacts__Location__c:any;
  export default eContacts__Location__c;
}
declare module "@salesforce/schema/Lead.Is_Deposit_Lead__c" {
  const Is_Deposit_Lead__c:boolean;
  export default Is_Deposit_Lead__c;
}
declare module "@salesforce/schema/Lead.LeadSource_Change_Date__c" {
  const LeadSource_Change_Date__c:any;
  export default LeadSource_Change_Date__c;
}
declare module "@salesforce/schema/Lead.Is_UNITY_Visa_Lead__c" {
  const Is_UNITY_Visa_Lead__c:boolean;
  export default Is_UNITY_Visa_Lead__c;
}
declare module "@salesforce/schema/Lead.et4ae5__HasOptedOutOfMobile__c" {
  const et4ae5__HasOptedOutOfMobile__c:boolean;
  export default et4ae5__HasOptedOutOfMobile__c;
}
declare module "@salesforce/schema/Lead.et4ae5__Mobile_Country_Code__c" {
  const et4ae5__Mobile_Country_Code__c:string;
  export default et4ae5__Mobile_Country_Code__c;
}
declare module "@salesforce/schema/Lead.Advocate_Text__c" {
  const Advocate_Text__c:string;
  export default Advocate_Text__c;
}
declare module "@salesforce/schema/Lead.Advocate_Timestamp__c" {
  const Advocate_Timestamp__c:any;
  export default Advocate_Timestamp__c;
}
declare module "@salesforce/schema/Lead.Advocate__c" {
  const Advocate__c:boolean;
  export default Advocate__c;
}
declare module "@salesforce/schema/Lead.Ambassador_Timestamp__c" {
  const Ambassador_Timestamp__c:any;
  export default Ambassador_Timestamp__c;
}
declare module "@salesforce/schema/Lead.Ambassador__c" {
  const Ambassador__c:boolean;
  export default Ambassador__c;
}
declare module "@salesforce/schema/Lead.Facebook_URL__c" {
  const Facebook_URL__c:string;
  export default Facebook_URL__c;
}
declare module "@salesforce/schema/Lead.Instagram_Handle__c" {
  const Instagram_Handle__c:string;
  export default Instagram_Handle__c;
}
declare module "@salesforce/schema/Lead.Twitter_Handle__c" {
  const Twitter_Handle__c:string;
  export default Twitter_Handle__c;
}
declare module "@salesforce/schema/Lead.DeDuplication_Date__c" {
  const DeDuplication_Date__c:any;
  export default DeDuplication_Date__c;
}
declare module "@salesforce/schema/Lead.Mailchimp_Rating__c" {
  const Mailchimp_Rating__c:string;
  export default Mailchimp_Rating__c;
}
declare module "@salesforce/schema/Lead.Has_Valid_Email__c" {
  const Has_Valid_Email__c:boolean;
  export default Has_Valid_Email__c;
}
declare module "@salesforce/schema/Lead.Send_to_MarketingCloud__c" {
  const Send_to_MarketingCloud__c:boolean;
  export default Send_to_MarketingCloud__c;
}
declare module "@salesforce/schema/Lead.leadcap__Facebook_Lead_ID__c" {
  const leadcap__Facebook_Lead_ID__c:string;
  export default leadcap__Facebook_Lead_ID__c;
}
declare module "@salesforce/schema/Lead.geopointe__Geocode__r" {
  const geopointe__Geocode__r:any;
  export default geopointe__Geocode__r;
}
declare module "@salesforce/schema/Lead.geopointe__Geocode__c" {
  const geopointe__Geocode__c:any;
  export default geopointe__Geocode__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Adverse_Alert__c" {
  const creditchecker__Adverse_Alert__c:number;
  export default creditchecker__Adverse_Alert__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Average_Score__c" {
  const creditchecker__Average_Score__c:number;
  export default creditchecker__Average_Score__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Bankruptcy_Count__c" {
  const creditchecker__Bankruptcy_Count__c:number;
  export default creditchecker__Bankruptcy_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Charge_Offs_Count__c" {
  const creditchecker__Charge_Offs_Count__c:number;
  export default creditchecker__Charge_Offs_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Collections_Count__c" {
  const creditchecker__Collections_Count__c:number;
  export default creditchecker__Collections_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Current_Adverse_Count__c" {
  const creditchecker__Current_Adverse_Count__c:number;
  export default creditchecker__Current_Adverse_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Date_Completed__c" {
  const creditchecker__Date_Completed__c:any;
  export default creditchecker__Date_Completed__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Debt_High_Credit__c" {
  const creditchecker__Debt_High_Credit__c:number;
  export default creditchecker__Debt_High_Credit__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Derogatory_Count__c" {
  const creditchecker__Derogatory_Count__c:number;
  export default creditchecker__Derogatory_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Disputes_Count__c" {
  const creditchecker__Disputes_Count__c:number;
  export default creditchecker__Disputes_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Most_Recent_Late__c" {
  const creditchecker__Most_Recent_Late__c:string;
  export default creditchecker__Most_Recent_Late__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Oldest_Date__c" {
  const creditchecker__Oldest_Date__c:string;
  export default creditchecker__Oldest_Date__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Previous_Adverse_Count__c" {
  const creditchecker__Previous_Adverse_Count__c:number;
  export default creditchecker__Previous_Adverse_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Public_Record_Count__c" {
  const creditchecker__Public_Record_Count__c:number;
  export default creditchecker__Public_Record_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Revolving_Credit_Utilization__c" {
  const creditchecker__Revolving_Credit_Utilization__c:number;
  export default creditchecker__Revolving_Credit_Utilization__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Secured_Debt__c" {
  const creditchecker__Secured_Debt__c:number;
  export default creditchecker__Secured_Debt__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Total_High_Credit__c" {
  const creditchecker__Total_High_Credit__c:number;
  export default creditchecker__Total_High_Credit__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Total_Inquiry_Count__c" {
  const creditchecker__Total_Inquiry_Count__c:number;
  export default creditchecker__Total_Inquiry_Count__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Total_Liability_Balance__c" {
  const creditchecker__Total_Liability_Balance__c:number;
  export default creditchecker__Total_Liability_Balance__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Total_Liability_PastDue__c" {
  const creditchecker__Total_Liability_PastDue__c:number;
  export default creditchecker__Total_Liability_PastDue__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Total_Liability_Payment__c" {
  const creditchecker__Total_Liability_Payment__c:number;
  export default creditchecker__Total_Liability_Payment__c;
}
declare module "@salesforce/schema/Lead.creditchecker__Unsecured_Debt__c" {
  const creditchecker__Unsecured_Debt__c:number;
  export default creditchecker__Unsecured_Debt__c;
}
declare module "@salesforce/schema/Lead.Locale__c" {
  const Locale__c:string;
  export default Locale__c;
}
declare module "@salesforce/schema/Lead.SMS_Number__c" {
  const SMS_Number__c:string;
  export default SMS_Number__c;
}
declare module "@salesforce/schema/Lead.SMS_Phone__c" {
  const SMS_Phone__c:string;
  export default SMS_Phone__c;
}
