declare module "@salesforce/schema/Case.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Case.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Case.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Case.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Case.CaseNumber" {
  const CaseNumber:string;
  export default CaseNumber;
}
declare module "@salesforce/schema/Case.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/Case.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/Case.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Case.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Case.Asset" {
  const Asset:any;
  export default Asset;
}
declare module "@salesforce/schema/Case.AssetId" {
  const AssetId:any;
  export default AssetId;
}
declare module "@salesforce/schema/Case.Source" {
  const Source:any;
  export default Source;
}
declare module "@salesforce/schema/Case.SourceId" {
  const SourceId:any;
  export default SourceId;
}
declare module "@salesforce/schema/Case.BusinessHours" {
  const BusinessHours:any;
  export default BusinessHours;
}
declare module "@salesforce/schema/Case.BusinessHoursId" {
  const BusinessHoursId:any;
  export default BusinessHoursId;
}
declare module "@salesforce/schema/Case.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/Case.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/Case.SuppliedName" {
  const SuppliedName:string;
  export default SuppliedName;
}
declare module "@salesforce/schema/Case.SuppliedEmail" {
  const SuppliedEmail:string;
  export default SuppliedEmail;
}
declare module "@salesforce/schema/Case.SuppliedPhone" {
  const SuppliedPhone:string;
  export default SuppliedPhone;
}
declare module "@salesforce/schema/Case.SuppliedCompany" {
  const SuppliedCompany:string;
  export default SuppliedCompany;
}
declare module "@salesforce/schema/Case.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Case.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Case.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Case.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/Case.Reason" {
  const Reason:string;
  export default Reason;
}
declare module "@salesforce/schema/Case.Origin" {
  const Origin:string;
  export default Origin;
}
declare module "@salesforce/schema/Case.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/Case.IsVisibleInSelfService" {
  const IsVisibleInSelfService:boolean;
  export default IsVisibleInSelfService;
}
declare module "@salesforce/schema/Case.Subject" {
  const Subject:string;
  export default Subject;
}
declare module "@salesforce/schema/Case.Priority" {
  const Priority:string;
  export default Priority;
}
declare module "@salesforce/schema/Case.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Case.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Case.ClosedDate" {
  const ClosedDate:any;
  export default ClosedDate;
}
declare module "@salesforce/schema/Case.IsEscalated" {
  const IsEscalated:boolean;
  export default IsEscalated;
}
declare module "@salesforce/schema/Case.HasCommentsUnreadByOwner" {
  const HasCommentsUnreadByOwner:boolean;
  export default HasCommentsUnreadByOwner;
}
declare module "@salesforce/schema/Case.HasSelfServiceComments" {
  const HasSelfServiceComments:boolean;
  export default HasSelfServiceComments;
}
declare module "@salesforce/schema/Case.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Case.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Case.IsClosedOnCreate" {
  const IsClosedOnCreate:boolean;
  export default IsClosedOnCreate;
}
declare module "@salesforce/schema/Case.IsSelfServiceClosed" {
  const IsSelfServiceClosed:boolean;
  export default IsSelfServiceClosed;
}
declare module "@salesforce/schema/Case.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Case.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Case.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Case.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Case.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Case.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Case.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Case.ContactPhone" {
  const ContactPhone:string;
  export default ContactPhone;
}
declare module "@salesforce/schema/Case.ContactMobile" {
  const ContactMobile:string;
  export default ContactMobile;
}
declare module "@salesforce/schema/Case.ContactEmail" {
  const ContactEmail:string;
  export default ContactEmail;
}
declare module "@salesforce/schema/Case.ContactFax" {
  const ContactFax:string;
  export default ContactFax;
}
declare module "@salesforce/schema/Case.Comments" {
  const Comments:string;
  export default Comments;
}
declare module "@salesforce/schema/Case.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Case.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Case.UploadID__c" {
  const UploadID__c:string;
  export default UploadID__c;
}
declare module "@salesforce/schema/Case.Lead__r" {
  const Lead__r:any;
  export default Lead__r;
}
declare module "@salesforce/schema/Case.Lead__c" {
  const Lead__c:any;
  export default Lead__c;
}
declare module "@salesforce/schema/Case.Days_Open__c" {
  const Days_Open__c:number;
  export default Days_Open__c;
}
declare module "@salesforce/schema/Case.Days_To_Complete__c" {
  const Days_To_Complete__c:number;
  export default Days_To_Complete__c;
}
declare module "@salesforce/schema/Case.Minutes_to_Complete__c" {
  const Minutes_to_Complete__c:number;
  export default Minutes_to_Complete__c;
}
declare module "@salesforce/schema/Case.Survey_Q1__c" {
  const Survey_Q1__c:string;
  export default Survey_Q1__c;
}
declare module "@salesforce/schema/Case.Survey_Q1_Freeform__c" {
  const Survey_Q1_Freeform__c:string;
  export default Survey_Q1_Freeform__c;
}
declare module "@salesforce/schema/Case.Survey_Q2__c" {
  const Survey_Q2__c:string;
  export default Survey_Q2__c;
}
declare module "@salesforce/schema/Case.Survey_Q3__c" {
  const Survey_Q3__c:string;
  export default Survey_Q3__c;
}
declare module "@salesforce/schema/Case.Survey_Q4__c" {
  const Survey_Q4__c:string;
  export default Survey_Q4__c;
}
declare module "@salesforce/schema/Case.Survey_Q5__c" {
  const Survey_Q5__c:string;
  export default Survey_Q5__c;
}
declare module "@salesforce/schema/Case.Survey_Q6__c" {
  const Survey_Q6__c:string;
  export default Survey_Q6__c;
}
declare module "@salesforce/schema/Case.Survey_Q7__c" {
  const Survey_Q7__c:string;
  export default Survey_Q7__c;
}
declare module "@salesforce/schema/Case.Survey_Q8__c" {
  const Survey_Q8__c:string;
  export default Survey_Q8__c;
}
declare module "@salesforce/schema/Case.Survey_Q9__c" {
  const Survey_Q9__c:string;
  export default Survey_Q9__c;
}
declare module "@salesforce/schema/Case.Survey_Q10_Freeform__c" {
  const Survey_Q10_Freeform__c:string;
  export default Survey_Q10_Freeform__c;
}
declare module "@salesforce/schema/Case.Survey_Q11__c" {
  const Survey_Q11__c:string;
  export default Survey_Q11__c;
}
declare module "@salesforce/schema/Case.Survey_Q12__c" {
  const Survey_Q12__c:string;
  export default Survey_Q12__c;
}
declare module "@salesforce/schema/Case.Survey_Q13__c" {
  const Survey_Q13__c:string;
  export default Survey_Q13__c;
}
declare module "@salesforce/schema/Case.Survey_Q14__c" {
  const Survey_Q14__c:string;
  export default Survey_Q14__c;
}
declare module "@salesforce/schema/Case.Survey_Q15__c" {
  const Survey_Q15__c:string;
  export default Survey_Q15__c;
}
declare module "@salesforce/schema/Case.Initial_Contact_SQ3__c" {
  const Initial_Contact_SQ3__c:string;
  export default Initial_Contact_SQ3__c;
}
declare module "@salesforce/schema/Case.Wait_Time_SQ4__c" {
  const Wait_Time_SQ4__c:number;
  export default Wait_Time_SQ4__c;
}
declare module "@salesforce/schema/Case.SQ_Courteous_Attentive_5__c" {
  const SQ_Courteous_Attentive_5__c:number;
  export default SQ_Courteous_Attentive_5__c;
}
declare module "@salesforce/schema/Case.SQ_Knowledgeable_Prof_6__c" {
  const SQ_Knowledgeable_Prof_6__c:number;
  export default SQ_Knowledgeable_Prof_6__c;
}
declare module "@salesforce/schema/Case.SQ_Easy_to_Understand_7__c" {
  const SQ_Easy_to_Understand_7__c:number;
  export default SQ_Easy_to_Understand_7__c;
}
declare module "@salesforce/schema/Case.SQ_Timely_Service_8__c" {
  const SQ_Timely_Service_8__c:number;
  export default SQ_Timely_Service_8__c;
}
declare module "@salesforce/schema/Case.SQ_Met_Service_Expectations_9__c" {
  const SQ_Met_Service_Expectations_9__c:number;
  export default SQ_Met_Service_Expectations_9__c;
}
declare module "@salesforce/schema/Case.New_Account_Ref_ID__c" {
  const New_Account_Ref_ID__c:number;
  export default New_Account_Ref_ID__c;
}
declare module "@salesforce/schema/Case.ID_Verified__c" {
  const ID_Verified__c:boolean;
  export default ID_Verified__c;
}
declare module "@salesforce/schema/Case.CIP_OFAC__c" {
  const CIP_OFAC__c:boolean;
  export default CIP_OFAC__c;
}
declare module "@salesforce/schema/Case.Account_Opened__c" {
  const Account_Opened__c:boolean;
  export default Account_Opened__c;
}
declare module "@salesforce/schema/Case.Online_Banking__c" {
  const Online_Banking__c:boolean;
  export default Online_Banking__c;
}
declare module "@salesforce/schema/Case.Deposit_Withdrawl__c" {
  const Deposit_Withdrawl__c:boolean;
  export default Deposit_Withdrawl__c;
}
declare module "@salesforce/schema/Case.Advance_Maturity_Date__c" {
  const Advance_Maturity_Date__c:boolean;
  export default Advance_Maturity_Date__c;
}
declare module "@salesforce/schema/Case.Exclude_Notices__c" {
  const Exclude_Notices__c:boolean;
  export default Exclude_Notices__c;
}
declare module "@salesforce/schema/Case.Contact_Date__c" {
  const Contact_Date__c:any;
  export default Contact_Date__c;
}
declare module "@salesforce/schema/Case.Morvision_ID__c" {
  const Morvision_ID__c:string;
  export default Morvision_ID__c;
}
declare module "@salesforce/schema/Case.Campaign__r" {
  const Campaign__r:any;
  export default Campaign__r;
}
declare module "@salesforce/schema/Case.Campaign__c" {
  const Campaign__c:any;
  export default Campaign__c;
}
declare module "@salesforce/schema/Case.Evening_Phone__c" {
  const Evening_Phone__c:string;
  export default Evening_Phone__c;
}
declare module "@salesforce/schema/Case.Branch_Manager__c" {
  const Branch_Manager__c:string;
  export default Branch_Manager__c;
}
declare module "@salesforce/schema/Case.one_Application__r" {
  const one_Application__r:any;
  export default one_Application__r;
}
declare module "@salesforce/schema/Case.one_Application__c" {
  const one_Application__c:any;
  export default one_Application__c;
}
declare module "@salesforce/schema/Case.Financial_Account__r" {
  const Financial_Account__r:any;
  export default Financial_Account__r;
}
declare module "@salesforce/schema/Case.Financial_Account__c" {
  const Financial_Account__c:any;
  export default Financial_Account__c;
}
declare module "@salesforce/schema/Case.Channel__c" {
  const Channel__c:string;
  export default Channel__c;
}
declare module "@salesforce/schema/Case.Priority_Flag__c" {
  const Priority_Flag__c:string;
  export default Priority_Flag__c;
}
declare module "@salesforce/schema/Case.Status_Flag__c" {
  const Status_Flag__c:string;
  export default Status_Flag__c;
}
declare module "@salesforce/schema/Case.Description_HTML__c" {
  const Description_HTML__c:string;
  export default Description_HTML__c;
}
declare module "@salesforce/schema/Case.geopointe__Geocode__r" {
  const geopointe__Geocode__r:any;
  export default geopointe__Geocode__r;
}
declare module "@salesforce/schema/Case.geopointe__Geocode__c" {
  const geopointe__Geocode__c:any;
  export default geopointe__Geocode__c;
}
declare module "@salesforce/schema/Case.Voicemail_Translation_Status__c" {
  const Voicemail_Translation_Status__c:string;
  export default Voicemail_Translation_Status__c;
}
declare module "@salesforce/schema/Case.Voicemail_Translation__c" {
  const Voicemail_Translation__c:string;
  export default Voicemail_Translation__c;
}
declare module "@salesforce/schema/Case.Attachment_Copy_to_Application__c" {
  const Attachment_Copy_to_Application__c:boolean;
  export default Attachment_Copy_to_Application__c;
}
declare module "@salesforce/schema/Case.Case_Topic__r" {
  const Case_Topic__r:any;
  export default Case_Topic__r;
}
declare module "@salesforce/schema/Case.Case_Topic__c" {
  const Case_Topic__c:any;
  export default Case_Topic__c;
}
declare module "@salesforce/schema/Case.Send_Topic_Classification_Email__c" {
  const Send_Topic_Classification_Email__c:boolean;
  export default Send_Topic_Classification_Email__c;
}
declare module "@salesforce/schema/Case.Additional_Email__c" {
  const Additional_Email__c:string;
  export default Additional_Email__c;
}
declare module "@salesforce/schema/Case.Additional_Name__c" {
  const Additional_Name__c:string;
  export default Additional_Name__c;
}
declare module "@salesforce/schema/Case.Additional_Phone__c" {
  const Additional_Phone__c:string;
  export default Additional_Phone__c;
}
declare module "@salesforce/schema/Case.Computed_Inbound_Phone__c" {
  const Computed_Inbound_Phone__c:string;
  export default Computed_Inbound_Phone__c;
}
declare module "@salesforce/schema/Case.Status_Change_Date__c" {
  const Status_Change_Date__c:any;
  export default Status_Change_Date__c;
}
declare module "@salesforce/schema/Case.amazonconnect__AC_Contact_Id__c" {
  const amazonconnect__AC_Contact_Id__c:string;
  export default amazonconnect__AC_Contact_Id__c;
}
