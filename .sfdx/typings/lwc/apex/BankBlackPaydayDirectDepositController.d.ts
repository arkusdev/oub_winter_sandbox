declare module "@salesforce/apex/BankBlackPaydayDirectDepositController.generateTicket" {
  export default function generateTicket(param: {info: any}): Promise<any>;
}
declare module "@salesforce/apex/BankBlackPaydayDirectDepositController.getFinancialAccountById" {
  export default function getFinancialAccountById(param: {faId: any}): Promise<any>;
}
declare module "@salesforce/apex/BankBlackPaydayDirectDepositController.getFinancialAccountByAcctNum" {
  export default function getFinancialAccountByAcctNum(param: {acctNum: any}): Promise<any>;
}
declare module "@salesforce/apex/BankBlackPaydayDirectDepositController.getExistingTicket" {
  export default function getExistingTicket(param: {contactId: any, faAcctNum: any}): Promise<any>;
}
