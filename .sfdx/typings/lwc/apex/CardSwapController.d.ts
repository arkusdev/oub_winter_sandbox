declare module "@salesforce/apex/CardSwapController.getSwapURL" {
  export default function getSwapURL(): Promise<any>;
}
