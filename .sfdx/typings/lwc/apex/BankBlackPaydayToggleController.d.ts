declare module "@salesforce/apex/BankBlackPaydayToggleController.createPaydayResponse" {
  export default function createPaydayResponse(param: {faId: any, check: any, commsOption: any, acctNbr: any}): Promise<any>;
}
declare module "@salesforce/apex/BankBlackPaydayToggleController.processToggleEven" {
  export default function processToggleEven(param: {faId: any}): Promise<any>;
}
declare module "@salesforce/apex/BankBlackPaydayToggleController.createPaydayCommsResponse" {
  export default function createPaydayCommsResponse(param: {faId: any, commsOption: any, acctNbr: any}): Promise<any>;
}
declare module "@salesforce/apex/BankBlackPaydayToggleController.processComms" {
  export default function processComms(param: {faId: any, pdrId: any}): Promise<any>;
}
