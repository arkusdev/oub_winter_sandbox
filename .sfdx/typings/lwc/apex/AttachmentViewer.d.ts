declare module "@salesforce/apex/AttachmentViewer.getAttachmentList" {
  export default function getAttachmentList(param: {xID: any}): Promise<any>;
}
declare module "@salesforce/apex/AttachmentViewer.getContentDocumentList" {
  export default function getContentDocumentList(param: {xID: any}): Promise<any>;
}
