declare module "@salesforce/apex/ApplicationEmail.getApplication" {
  export default function getApplication(param: {appID: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationEmail.getEmailStates" {
  export default function getEmailStates(param: {app: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationEmail.sendTrialDepositEmail" {
  export default function sendTrialDepositEmail(param: {app: any}): Promise<any>;
}
