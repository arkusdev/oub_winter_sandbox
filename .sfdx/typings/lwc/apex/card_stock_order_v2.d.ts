declare module "@salesforce/apex/card_stock_order_v2.getCards" {
  export default function getCards(param: {cardType: any}): Promise<any>;
}
declare module "@salesforce/apex/card_stock_order_v2.submitTicket" {
  export default function submitTicket(param: {info: any}): Promise<any>;
}
declare module "@salesforce/apex/card_stock_order_v2.getServices" {
  export default function getServices(param: {cardType: any}): Promise<any>;
}
