declare module "@salesforce/apex/BankBlackPaydayOptInController.getAllAcctInfo" {
  export default function getAllAcctInfo(param: {type: any}): Promise<any>;
}
