declare module "@salesforce/apex/ApplicationLOSOverride.getDeclineReasons" {
  export default function getDeclineReasons(): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.getApplication" {
  export default function getApplication(param: {appID: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.isPreBureauOverride" {
  export default function isPreBureauOverride(param: {app: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.overrideApplicationPreBureau" {
  export default function overrideApplicationPreBureau(param: {app: any, overrideReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.declineApplicationPreBureau" {
  export default function declineApplicationPreBureau(param: {app: any, declineReasons: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.isPostBureauOverride" {
  export default function isPostBureauOverride(param: {app: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.overrideApplicationPostBureau" {
  export default function overrideApplicationPostBureau(param: {app: any, overrideReason: any, creditLimit: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationLOSOverride.declineApplicationPostBureau" {
  export default function declineApplicationPostBureau(param: {app: any, declineReasons: any}): Promise<any>;
}
