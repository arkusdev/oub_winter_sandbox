declare module "@salesforce/apex/OneUnitedChatterController.getGroupID" {
  export default function getGroupID(): Promise<any>;
}
