declare module "@salesforce/apex/ApplicationOverride.getApplication" {
  export default function getApplication(param: {appID: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.getOverrides" {
  export default function getOverrides(param: {app: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.runMinfraudOverride" {
  export default function runMinfraudOverride(param: {app: any, overrideReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.declineApplicationMinfraud" {
  export default function declineApplicationMinfraud(param: {app: any, declineReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.runOUBOverride" {
  export default function runOUBOverride(param: {app: any, overrideReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.declineApplicationOUB" {
  export default function declineApplicationOUB(param: {app: any, declineReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.runOFACOverride" {
  export default function runOFACOverride(param: {app: any, overrideReason: any, whichApp: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.declineApplicationOFAC" {
  export default function declineApplicationOFAC(param: {app: any, declineReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.runIDVOverride" {
  export default function runIDVOverride(param: {app: any, overrideReason: any, whichApp: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.declineApplicationIDV" {
  export default function declineApplicationIDV(param: {app: any, declineReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.runQFOverride" {
  export default function runQFOverride(param: {app: any, overrideReason: any, whichApp: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.declineApplicationQF" {
  export default function declineApplicationQF(param: {app: any, declineReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.runPhotoOverride" {
  export default function runPhotoOverride(param: {app: any, overrideReason: any}): Promise<any>;
}
declare module "@salesforce/apex/ApplicationOverride.declineApplicationPhoto" {
  export default function declineApplicationPhoto(param: {app: any, declineReason: any}): Promise<any>;
}
