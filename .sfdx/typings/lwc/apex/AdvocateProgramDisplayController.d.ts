declare module "@salesforce/apex/AdvocateProgramDisplayController.getOverallProgress" {
  export default function getOverallProgress(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getCurrentLevel" {
  export default function getCurrentLevel(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getReferralInfo" {
  export default function getReferralInfo(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getAllLevels" {
  export default function getAllLevels(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getAllBadges" {
  export default function getAllBadges(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getMyBadges" {
  export default function getMyBadges(param: {recent: any}): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getMyPoints" {
  export default function getMyPoints(param: {recent: any}): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getAllPoints" {
  export default function getAllPoints(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getMyRewards" {
  export default function getMyRewards(param: {recent: any}): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getAllRewards" {
  export default function getAllRewards(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.getContact" {
  export default function getContact(): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.saveContact" {
  export default function saveContact(param: {con: any}): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.saveAttachment" {
  export default function saveAttachment(param: {base64Data: any, contentType: any}): Promise<any>;
}
declare module "@salesforce/apex/AdvocateProgramDisplayController.assignLevelIfEmtpy" {
  export default function assignLevelIfEmtpy(): Promise<any>;
}
