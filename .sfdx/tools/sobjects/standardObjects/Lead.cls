// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Lead {
    global Id Id;
    global Boolean IsDeleted;
    global Lead MasterRecord;
    global Id MasterRecordId;
    global String LastName;
    global String FirstName;
    global String Salutation;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Title;
    global String Company;
    global String Street;
    global String City;
    global String State;
    global String PostalCode;
    global String Country;
    global Double Latitude;
    global Double Longitude;
    global String GeocodeAccuracy;
    global Address Address;
    global String Phone;
    global String MobilePhone;
    global String Fax;
    global String Email;
    global String Website;
    global String PhotoUrl;
    global String Description;
    global String LeadSource;
    global String Status;
    global String Industry;
    global String Rating;
    global Decimal AnnualRevenue;
    global Integer NumberOfEmployees;
    global SObject Owner;
    global Id OwnerId;
    global Boolean HasOptedOutOfEmail;
    global Boolean IsConverted;
    global Date ConvertedDate;
    global Account ConvertedAccount;
    global Id ConvertedAccountId;
    global Contact ConvertedContact;
    global Id ConvertedContactId;
    global Opportunity ConvertedOpportunity;
    global Id ConvertedOpportunityId;
    global Boolean IsUnreadByOwner;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Boolean DoNotCall;
    global Boolean HasOptedOutOfFax;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Date LastTransferDate;
    global String Jigsaw;
    global String JigsawContactId;
    global String EmailBouncedReason;
    global Datetime EmailBouncedDate;
    global Individual Individual;
    global Id IndividualId;
    global String BusinessStructure__c;
    global String DateBusinessStarted__c;
    global String BankAccountType__c;
    global String BusinessStage__c;
    global String PrimaryBankName__c;
    global Decimal GrossAnnualSales__c;
    global String BusinessType__c;
    global String ProductInterest__c;
    global String UploadID__c;
    global Double Tax_ID_Number__c;
    global String Work_Email__c;
    global String OneUnited_Newsletter__c;
    global String BestCallTime__c;
    global Boolean News_List__c;
    global Boolean Events_List__c;
    global Boolean Newsletter__c;
    global String Promotion_Code__c;
    global Boolean Bad_Email_Address__c;
    global String SFGA__Web_Source__c;
    global String SFGA__CorrelationID__c;
    global String SFGA__Correlation_Data__c;
    global String ProductSelection__c;
    global String Message__c;
    global String Do_you_have_a_ChexSystems_record__c;
    global String Are_you_a_United_States_Citizen__c;
    global String Referred_By__c;
    global String What_is_your_intended_opening_deposit__c;
    global String What_is_the_purpose_of_this_inquiry__c;
    global String Tax_ID__c;
    global String Organization__c;
    global String Website_Form__c;
    global String Evening_Phone__c;
    global String UTM_Campaign__c;
    global String UTM_Medium__c;
    global String UTM_Source__c;
    global String UTM_Content__c;
    global String UTM_Term__c;
    global String UTM_VisitorID__c;
    global Double Created_Year__c;
    global Datetime Loan_Referral_Date__c;
    global String MC_Salesforce_Type__c;
    global Boolean Financial_Priority_Business__c;
    global Boolean Financial_Priority_Credit__c;
    global Boolean Financial_Priority_Home_Finance__c;
    global Boolean Financial_Priority_Literacy__c;
    global Boolean Financial_Priority_Online_Banking__c;
    global Boolean Financial_Priority_Retirement__c;
    global Boolean Financial_Priority_Savings__c;
    global String eContacts__Business_Card__c;
    global Boolean eContacts__Created_By_eContacts__c;
    global String eContacts__Location_Text__c;
    global Double eContacts__Location__Latitude__s;
    global Double eContacts__Location__Longitude__s;
    global Location eContacts__Location__c;
    global Boolean Is_Deposit_Lead__c;
    global Datetime LeadSource_Change_Date__c;
    global Boolean Is_UNITY_Visa_Lead__c;
    global Boolean et4ae5__HasOptedOutOfMobile__c;
    global String et4ae5__Mobile_Country_Code__c;
    global String Advocate_Text__c;
    global Datetime Advocate_Timestamp__c;
    global Boolean Advocate__c;
    global Datetime Ambassador_Timestamp__c;
    global Boolean Ambassador__c;
    global String Facebook_URL__c;
    global String Instagram_Handle__c;
    global String Twitter_Handle__c;
    global Date DeDuplication_Date__c;
    global String Mailchimp_Rating__c;
    global Boolean Has_Valid_Email__c;
    global Boolean Send_to_MarketingCloud__c;
    global String leadcap__Facebook_Lead_ID__c;
    /* Reference to the Geocode object holding all of mapping information for the record.
    */
    global geopointe__Geocode__c geopointe__Geocode__r;
    /* Reference to the Geocode object holding all of mapping information for the record.
    */
    global Id geopointe__Geocode__c;
    /* Count of adverse alerts. Please view the full information in the Alert related list.
    */
    global Double creditchecker__Adverse_Alert__c;
    /* Average of "Score" field from related "Score Model" records.
    */
    global Double creditchecker__Average_Score__c;
    global Double creditchecker__Bankruptcy_Count__c;
    /* A charge-off or chargeoff is the declaration by a creditor (usually a credit card account) that an amount of debt is unlikely to be collected.
    */
    global Double creditchecker__Charge_Offs_Count__c;
    global Double creditchecker__Collections_Count__c;
    /* Liability Current Adverse Count.
    */
    global Double creditchecker__Current_Adverse_Count__c;
    /* The date of completion marks the date of Credit Check completion
    */
    global Date creditchecker__Date_Completed__c;
    global Double creditchecker__Debt_High_Credit__c;
    global Double creditchecker__Derogatory_Count__c;
    global Double creditchecker__Disputes_Count__c;
    global String creditchecker__Most_Recent_Late__c;
    global String creditchecker__Oldest_Date__c;
    /* Liability Previous Adverse Count.
    */
    global Double creditchecker__Previous_Adverse_Count__c;
    global Double creditchecker__Public_Record_Count__c;
    /* Revolving Credit Utilization.
    */
    global Double creditchecker__Revolving_Credit_Utilization__c;
    global Double creditchecker__Secured_Debt__c;
    global Double creditchecker__Total_High_Credit__c;
    /* Total credit inquiries.
    */
    global Double creditchecker__Total_Inquiry_Count__c;
    global Double creditchecker__Total_Liability_Balance__c;
    global Double creditchecker__Total_Liability_PastDue__c;
    global Double creditchecker__Total_Liability_Payment__c;
    global Double creditchecker__Unsecured_Debt__c;
    global String Locale__c;
    global String SMS_Number__c;
    global String SMS_Phone__c;
    global List<AcceptedEventRelation> AcceptedEventRelations;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CampaignMember> CampaignMembers;
    global List<Case> Case__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DeclinedEventRelation> DeclinedEventRelations;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessageRelation> EmailMessageRelations;
    global List<EmailStatus> EmailStatuses;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<EventWhoRelation> EventWhoRelations;
    global List<LeadFeed> Feeds;
    global List<LeadHistory> Histories;
    global List<LeadShare> Shares;
    global List<ListEmailIndividualRecipient> ListEmailIndividualRecipients;
    global List<LiveChatTranscript> LiveChatTranscripts;
    global List<MWA1_Submission__c> R00N50000001ZGHoEAO;
    global List<NetworkActivityAudit> ParentEntities;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OneCampaign_Member__c> Campaign_Members__r;
    global List<OpenActivity> OpenActivities;
    global List<OutgoingEmailRelation> OutgoingEmailRelations;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SFGA__Ad_Group__c> R00N40000001HggvEAC;
    global List<SFGA__Google_Campaign__c> R00N40000001HggqEAC;
    global List<SFGA__Keyword__c> R00N40000001Hgh9EAC;
    global List<SFGA__Search_Phrase__c> R00N40000001HghFEAS;
    global List<SFGA__Text_Ad__c> R00N40000001Hgh4EAC;
    global List<SocialPersona> Personas;
    global List<SocialPost> Posts;
    global List<Survey_Response__c> Survey_Responses__r;
    global List<Task> Tasks;
    global List<TaskRelation> TaskRelations;
    global List<TaskWhoRelation> TaskWhoRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<UndecidedEventRelation> UndecidedEventRelations;
    global List<UserEmailPreferredPerson> PersonRecord;
    global List<Workshop_Attendee__c> One_United_Marketing_Members__r;
    global List<amazonconnect__Call_Campaign__c> amazonconnect__Call_Campaigns__r;
    global List<creditchecker__Credit_Report__c> creditchecker__Credit_Reports__r;
    global List<et4ae5__IndividualEmailResult__c> et4ae5__IndividualEmailResults__r;
    global List<et4ae5__SMSDefinition__c> et4ae5__SMSDefinitions__r;
    global List<et4ae5__SendDefinition__c> et4ae5__SendDefinitions__r;
    global List<geopointe__Check_In__c> geopointe__Check_Ins__r;
    global List<ActivityHistory> PrimaryWho;
    global List<AgentWork> WorkItem;
    global List<CampaignMember> LeadOrContact;
    global List<CampaignMemberChangeEvent> Lead;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> Who;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<LiveChatTranscriptChangeEvent> Lead;
    global List<OpenActivity> PrimaryWho;
    global List<OutgoingEmail> Who;
    global List<PendingServiceRouting> WorkItem;
    global List<TaskChangeEvent> Who;
    global List<TaskRelationChangeEvent> Relation;

    global Lead () 
    {
    }
}