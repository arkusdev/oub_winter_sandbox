// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Opportunity {
    global Id Id;
    global Boolean IsDeleted;
    global Account Account;
    global Id AccountId;
    global String Name;
    global String Description;
    global String StageName;
    global Decimal Amount;
    global Double Probability;
    global Decimal ExpectedRevenue;
    global Double TotalOpportunityQuantity;
    global Date CloseDate;
    global String Type;
    global String NextStep;
    global String LeadSource;
    global Boolean IsClosed;
    global Boolean IsWon;
    global String ForecastCategory;
    global String ForecastCategoryName;
    global Boolean HasOpportunityLineItem;
    global Pricebook2 Pricebook2;
    global Id Pricebook2Id;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Integer FiscalQuarter;
    global Integer FiscalYear;
    global String Fiscal;
    global Contact Contact;
    global Id ContactId;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Boolean HasOpenActivity;
    global Boolean HasOverdueTask;
    global Double Rate__c;
    global String TypeofLoan__c;
    global String ProposedCollateral__c;
    global String ProductInterest__c;
    global String PurposeofLoan__c;
    global String BankAccountType__c;
    global String InvestmentType__c;
    global String Term__c;
    global String UploadID__c;
    /* Reference to the Geocode object holding all of mapping information for the record.
    */
    global geopointe__Geocode__c geopointe__Geocode__r;
    /* Reference to the Geocode object holding all of mapping information for the record.
    */
    global Id geopointe__Geocode__c;
    /* Count of adverse alerts. Please view the full information in the Alert related list.
    */
    global Double creditchecker__Adverse_Alert__c;
    /* Average of "Score" field from related "Score Model" records.
    */
    global Double creditchecker__Average_Score__c;
    global Double creditchecker__Bankruptcy_Count__c;
    /* A charge-off or chargeoff is the declaration by a creditor (usually a credit card account) that an amount of debt is unlikely to be collected.
    */
    global Double creditchecker__Charge_Offs_Count__c;
    global Double creditchecker__Collections_Count__c;
    /* Liability Current Adverse Count.
    */
    global Double creditchecker__Current_Adverse_Count__c;
    /* The date of completion marks the date of Credit Check completion
    */
    global Date creditchecker__Date_Completed__c;
    global Double creditchecker__Debt_High_Credit__c;
    global String creditchecker__DeliveryInstallationStatus__c;
    global Double creditchecker__Derogatory_Count__c;
    global Double creditchecker__Disputes_Count__c;
    global String creditchecker__Most_Recent_Late__c;
    global String creditchecker__Oldest_Date__c;
    /* Liability Previous Adverse Count.
    */
    global Double creditchecker__Previous_Adverse_Count__c;
    global Double creditchecker__Public_Record_Count__c;
    /* Revolving Credit Utilization.
    */
    global Double creditchecker__Revolving_Credit_Utilization__c;
    global Double creditchecker__Secured_Debt__c;
    global Double creditchecker__Total_High_Credit__c;
    /* Total credit inquiries.
    */
    global Double creditchecker__Total_Inquiry_Count__c;
    global Double creditchecker__Total_Liability_Balance__c;
    global Double creditchecker__Total_Liability_PastDue__c;
    global Double creditchecker__Total_Liability_Payment__c;
    global Double creditchecker__Unsecured_Debt__c;
    global List<AccountPartner> AccountPartners;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<MWA1_Submission__c> R00N50000001ZGHuEAO;
    global List<NetworkActivityAudit> ParentEntities;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<OpportunityCompetitor> OpportunityCompetitors;
    global List<OpportunityContactRole> OpportunityContactRoles;
    global List<OpportunityFeed> Feeds;
    global List<OpportunityFieldHistory> Histories;
    global List<OpportunityHistory> OpportunityHistories;
    global List<OpportunityLineItem> OpportunityLineItems;
    global List<OpportunityPartner> OpportunityPartnersFrom;
    global List<OpportunityShare> Shares;
    global List<Partner> Partners;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SFDC_Project__c> R00N30000001F4t2EAC;
    global List<SFGA__Ad_Group__c> R00N40000001HggwEAC;
    global List<SFGA__Google_Campaign__c> R00N40000001HggrEAC;
    global List<SFGA__Keyword__c> R00N40000001HghAEAS;
    global List<SFGA__Search_Phrase__c> R00N40000001HghEEAS;
    global List<SFGA__Text_Ad__c> R00N40000001Hgh5EAC;
    global List<Task> Tasks;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<amazonconnect__Call_Campaign__c> amazonconnect__Call_Campaigns__r;
    global List<creditchecker__Credit_Report__c> creditchecker__Credit_Reports__r;
    global List<geopointe__Check_In__c> geopointe__Check_Ins__r;
    global List<AgentWork> WorkItem;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Lead> ConvertedOpportunity;
    global List<OpportunityContactRoleChangeEvent> Opportunity;
    global List<Opportunity__hd> Parent;
    global List<OutgoingEmail> RelatedTo;
    global List<PendingServiceRouting> WorkItem;
    global List<TaskChangeEvent> What;
    global List<TaskRelationChangeEvent> Relation;

    global Opportunity () 
    {
    }
}